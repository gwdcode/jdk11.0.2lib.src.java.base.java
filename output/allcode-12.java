        static double getVolatile(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getDoubleVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static double getOpaque(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getDoubleOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static double getAcquire(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getDoubleAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldInstanceReadOnly.class, Object.class, double.class);
    }

    static final class FieldInstanceReadWrite extends FieldInstanceReadOnly {

        FieldInstanceReadWrite(Class<?> receiverType, long fieldOffset) {
            super(receiverType, fieldOffset, FieldInstanceReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldInstanceReadWrite handle, Object holder, double value) {
            UNSAFE.putDouble(Objects.requireNonNull(handle.receiverType.cast(holder)),
                             handle.fieldOffset,
                             value);
        }

        @ForceInline
        static void setVolatile(FieldInstanceReadWrite handle, Object holder, double value) {
            UNSAFE.putDoubleVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                     handle.fieldOffset,
                                     value);
        }

        @ForceInline
        static void setOpaque(FieldInstanceReadWrite handle, Object holder, double value) {
            UNSAFE.putDoubleOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                   handle.fieldOffset,
                                   value);
        }

        @ForceInline
        static void setRelease(FieldInstanceReadWrite handle, Object holder, double value) {
            UNSAFE.putDoubleRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                    handle.fieldOffset,
                                    value);
        }

        @ForceInline
        static boolean compareAndSet(FieldInstanceReadWrite handle, Object holder, double expected, double value) {
            return UNSAFE.compareAndSetDouble(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static double compareAndExchange(FieldInstanceReadWrite handle, Object holder, double expected, double value) {
            return UNSAFE.compareAndExchangeDouble(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static double compareAndExchangeAcquire(FieldInstanceReadWrite handle, Object holder, double expected, double value) {
            return UNSAFE.compareAndExchangeDoubleAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static double compareAndExchangeRelease(FieldInstanceReadWrite handle, Object holder, double expected, double value) {
            return UNSAFE.compareAndExchangeDoubleRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldInstanceReadWrite handle, Object holder, double expected, double value) {
            return UNSAFE.weakCompareAndSetDoublePlain(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldInstanceReadWrite handle, Object holder, double expected, double value) {
            return UNSAFE.weakCompareAndSetDouble(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldInstanceReadWrite handle, Object holder, double expected, double value) {
            return UNSAFE.weakCompareAndSetDoubleAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldInstanceReadWrite handle, Object holder, double expected, double value) {
            return UNSAFE.weakCompareAndSetDoubleRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static double getAndSet(FieldInstanceReadWrite handle, Object holder, double value) {
            return UNSAFE.getAndSetDouble(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static double getAndSetAcquire(FieldInstanceReadWrite handle, Object holder, double value) {
            return UNSAFE.getAndSetDoubleAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static double getAndSetRelease(FieldInstanceReadWrite handle, Object holder, double value) {
            return UNSAFE.getAndSetDoubleRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static double getAndAdd(FieldInstanceReadWrite handle, Object holder, double value) {
            return UNSAFE.getAndAddDouble(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static double getAndAddAcquire(FieldInstanceReadWrite handle, Object holder, double value) {
            return UNSAFE.getAndAddDoubleAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static double getAndAddRelease(FieldInstanceReadWrite handle, Object holder, double value) {
            return UNSAFE.getAndAddDoubleRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }


        static final VarForm FORM = new VarForm(FieldInstanceReadWrite.class, Object.class, double.class);
    }


    static class FieldStaticReadOnly extends VarHandle {
        final Object base;
        final long fieldOffset;

        FieldStaticReadOnly(Object base, long fieldOffset) {
            this(base, fieldOffset, FieldStaticReadOnly.FORM);
        }

        protected FieldStaticReadOnly(Object base, long fieldOffset,
                                      VarForm form) {
            super(form);
            this.base = base;
            this.fieldOffset = fieldOffset;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(null, double.class);
        }

        @ForceInline
        static double get(FieldStaticReadOnly handle) {
            return UNSAFE.getDouble(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static double getVolatile(FieldStaticReadOnly handle) {
            return UNSAFE.getDoubleVolatile(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static double getOpaque(FieldStaticReadOnly handle) {
            return UNSAFE.getDoubleOpaque(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static double getAcquire(FieldStaticReadOnly handle) {
            return UNSAFE.getDoubleAcquire(handle.base,
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadOnly.class, null, double.class);
    }

    static final class FieldStaticReadWrite extends FieldStaticReadOnly {

        FieldStaticReadWrite(Object base, long fieldOffset) {
            super(base, fieldOffset, FieldStaticReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldStaticReadWrite handle, double value) {
            UNSAFE.putDouble(handle.base,
                             handle.fieldOffset,
                             value);
        }

        @ForceInline
        static void setVolatile(FieldStaticReadWrite handle, double value) {
            UNSAFE.putDoubleVolatile(handle.base,
                                     handle.fieldOffset,
                                     value);
        }

        @ForceInline
        static void setOpaque(FieldStaticReadWrite handle, double value) {
            UNSAFE.putDoubleOpaque(handle.base,
                                   handle.fieldOffset,
                                   value);
        }

        @ForceInline
        static void setRelease(FieldStaticReadWrite handle, double value) {
            UNSAFE.putDoubleRelease(handle.base,
                                    handle.fieldOffset,
                                    value);
        }

        @ForceInline
        static boolean compareAndSet(FieldStaticReadWrite handle, double expected, double value) {
            return UNSAFE.compareAndSetDouble(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }


        @ForceInline
        static double compareAndExchange(FieldStaticReadWrite handle, double expected, double value) {
            return UNSAFE.compareAndExchangeDouble(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static double compareAndExchangeAcquire(FieldStaticReadWrite handle, double expected, double value) {
            return UNSAFE.compareAndExchangeDoubleAcquire(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static double compareAndExchangeRelease(FieldStaticReadWrite handle, double expected, double value) {
            return UNSAFE.compareAndExchangeDoubleRelease(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldStaticReadWrite handle, double expected, double value) {
            return UNSAFE.weakCompareAndSetDoublePlain(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldStaticReadWrite handle, double expected, double value) {
            return UNSAFE.weakCompareAndSetDouble(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldStaticReadWrite handle, double expected, double value) {
            return UNSAFE.weakCompareAndSetDoubleAcquire(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldStaticReadWrite handle, double expected, double value) {
            return UNSAFE.weakCompareAndSetDoubleRelease(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static double getAndSet(FieldStaticReadWrite handle, double value) {
            return UNSAFE.getAndSetDouble(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static double getAndSetAcquire(FieldStaticReadWrite handle, double value) {
            return UNSAFE.getAndSetDoubleAcquire(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static double getAndSetRelease(FieldStaticReadWrite handle, double value) {
            return UNSAFE.getAndSetDoubleRelease(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static double getAndAdd(FieldStaticReadWrite handle, double value) {
            return UNSAFE.getAndAddDouble(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static double getAndAddAcquire(FieldStaticReadWrite handle, double value) {
            return UNSAFE.getAndAddDoubleAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static double getAndAddRelease(FieldStaticReadWrite handle, double value) {
            return UNSAFE.getAndAddDoubleRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadWrite.class, null, double.class);
    }


    static final class Array extends VarHandle {
        final int abase;
        final int ashift;

        Array(int abase, int ashift) {
            super(Array.FORM);
            this.abase = abase;
            this.ashift = ashift;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(double[].class, double.class, int.class);
        }


        @ForceInline
        static double get(Array handle, Object oarray, int index) {
            double[] array = (double[]) oarray;
            return array[index];
        }

        @ForceInline
        static void set(Array handle, Object oarray, int index, double value) {
            double[] array = (double[]) oarray;
            array[index] = value;
        }

        @ForceInline
        static double getVolatile(Array handle, Object oarray, int index) {
            double[] array = (double[]) oarray;
            return UNSAFE.getDoubleVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setVolatile(Array handle, Object oarray, int index, double value) {
            double[] array = (double[]) oarray;
            UNSAFE.putDoubleVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static double getOpaque(Array handle, Object oarray, int index) {
            double[] array = (double[]) oarray;
            return UNSAFE.getDoubleOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setOpaque(Array handle, Object oarray, int index, double value) {
            double[] array = (double[]) oarray;
            UNSAFE.putDoubleOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static double getAcquire(Array handle, Object oarray, int index) {
            double[] array = (double[]) oarray;
            return UNSAFE.getDoubleAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setRelease(Array handle, Object oarray, int index, double value) {
            double[] array = (double[]) oarray;
            UNSAFE.putDoubleRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static boolean compareAndSet(Array handle, Object oarray, int index, double expected, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.compareAndSetDouble(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static double compareAndExchange(Array handle, Object oarray, int index, double expected, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.compareAndExchangeDouble(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static double compareAndExchangeAcquire(Array handle, Object oarray, int index, double expected, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.compareAndExchangeDoubleAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static double compareAndExchangeRelease(Array handle, Object oarray, int index, double expected, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.compareAndExchangeDoubleRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(Array handle, Object oarray, int index, double expected, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.weakCompareAndSetDoublePlain(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSet(Array handle, Object oarray, int index, double expected, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.weakCompareAndSetDouble(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(Array handle, Object oarray, int index, double expected, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.weakCompareAndSetDoubleAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(Array handle, Object oarray, int index, double expected, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.weakCompareAndSetDoubleRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static double getAndSet(Array handle, Object oarray, int index, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.getAndSetDouble(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static double getAndSetAcquire(Array handle, Object oarray, int index, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.getAndSetDoubleAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static double getAndSetRelease(Array handle, Object oarray, int index, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.getAndSetDoubleRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static double getAndAdd(Array handle, Object oarray, int index, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.getAndAddDouble(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static double getAndAddAcquire(Array handle, Object oarray, int index, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.getAndAddDoubleAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static double getAndAddRelease(Array handle, Object oarray, int index, double value) {
            double[] array = (double[]) oarray;
            return UNSAFE.getAndAddDoubleRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        static final VarForm FORM = new VarForm(Array.class, double[].class, double.class, int.class);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\VarHandleFloats.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang.invoke;

import jdk.internal.util.Preconditions;
import jdk.internal.vm.annotation.ForceInline;

import java.util.Objects;

import static java.lang.invoke.MethodHandleStatics.UNSAFE;

// -- This file was mechanically generated: Do not edit! -- //

final class VarHandleFloats {

    static class FieldInstanceReadOnly extends VarHandle {
        final long fieldOffset;
        final Class<?> receiverType;

        FieldInstanceReadOnly(Class<?> receiverType, long fieldOffset) {
            this(receiverType, fieldOffset, FieldInstanceReadOnly.FORM);
        }

        protected FieldInstanceReadOnly(Class<?> receiverType, long fieldOffset,
                                        VarForm form) {
            super(form);
            this.fieldOffset = fieldOffset;
            this.receiverType = receiverType;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(receiverType, float.class);
        }

        @ForceInline
        static float get(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getFloat(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static float getVolatile(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getFloatVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static float getOpaque(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getFloatOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static float getAcquire(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getFloatAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldInstanceReadOnly.class, Object.class, float.class);
    }

    static final class FieldInstanceReadWrite extends FieldInstanceReadOnly {

        FieldInstanceReadWrite(Class<?> receiverType, long fieldOffset) {
            super(receiverType, fieldOffset, FieldInstanceReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldInstanceReadWrite handle, Object holder, float value) {
            UNSAFE.putFloat(Objects.requireNonNull(handle.receiverType.cast(holder)),
                             handle.fieldOffset,
                             value);
        }

        @ForceInline
        static void setVolatile(FieldInstanceReadWrite handle, Object holder, float value) {
            UNSAFE.putFloatVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                     handle.fieldOffset,
                                     value);
        }

        @ForceInline
        static void setOpaque(FieldInstanceReadWrite handle, Object holder, float value) {
            UNSAFE.putFloatOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                   handle.fieldOffset,
                                   value);
        }

        @ForceInline
        static void setRelease(FieldInstanceReadWrite handle, Object holder, float value) {
            UNSAFE.putFloatRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                    handle.fieldOffset,
                                    value);
        }

        @ForceInline
        static boolean compareAndSet(FieldInstanceReadWrite handle, Object holder, float expected, float value) {
            return UNSAFE.compareAndSetFloat(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static float compareAndExchange(FieldInstanceReadWrite handle, Object holder, float expected, float value) {
            return UNSAFE.compareAndExchangeFloat(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static float compareAndExchangeAcquire(FieldInstanceReadWrite handle, Object holder, float expected, float value) {
            return UNSAFE.compareAndExchangeFloatAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static float compareAndExchangeRelease(FieldInstanceReadWrite handle, Object holder, float expected, float value) {
            return UNSAFE.compareAndExchangeFloatRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldInstanceReadWrite handle, Object holder, float expected, float value) {
            return UNSAFE.weakCompareAndSetFloatPlain(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldInstanceReadWrite handle, Object holder, float expected, float value) {
            return UNSAFE.weakCompareAndSetFloat(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldInstanceReadWrite handle, Object holder, float expected, float value) {
            return UNSAFE.weakCompareAndSetFloatAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldInstanceReadWrite handle, Object holder, float expected, float value) {
            return UNSAFE.weakCompareAndSetFloatRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static float getAndSet(FieldInstanceReadWrite handle, Object holder, float value) {
            return UNSAFE.getAndSetFloat(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static float getAndSetAcquire(FieldInstanceReadWrite handle, Object holder, float value) {
            return UNSAFE.getAndSetFloatAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static float getAndSetRelease(FieldInstanceReadWrite handle, Object holder, float value) {
            return UNSAFE.getAndSetFloatRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static float getAndAdd(FieldInstanceReadWrite handle, Object holder, float value) {
            return UNSAFE.getAndAddFloat(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static float getAndAddAcquire(FieldInstanceReadWrite handle, Object holder, float value) {
            return UNSAFE.getAndAddFloatAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static float getAndAddRelease(FieldInstanceReadWrite handle, Object holder, float value) {
            return UNSAFE.getAndAddFloatRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }


        static final VarForm FORM = new VarForm(FieldInstanceReadWrite.class, Object.class, float.class);
    }


    static class FieldStaticReadOnly extends VarHandle {
        final Object base;
        final long fieldOffset;

        FieldStaticReadOnly(Object base, long fieldOffset) {
            this(base, fieldOffset, FieldStaticReadOnly.FORM);
        }

        protected FieldStaticReadOnly(Object base, long fieldOffset,
                                      VarForm form) {
            super(form);
            this.base = base;
            this.fieldOffset = fieldOffset;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(null, float.class);
        }

        @ForceInline
        static float get(FieldStaticReadOnly handle) {
            return UNSAFE.getFloat(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static float getVolatile(FieldStaticReadOnly handle) {
            return UNSAFE.getFloatVolatile(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static float getOpaque(FieldStaticReadOnly handle) {
            return UNSAFE.getFloatOpaque(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static float getAcquire(FieldStaticReadOnly handle) {
            return UNSAFE.getFloatAcquire(handle.base,
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadOnly.class, null, float.class);
    }

    static final class FieldStaticReadWrite extends FieldStaticReadOnly {

        FieldStaticReadWrite(Object base, long fieldOffset) {
            super(base, fieldOffset, FieldStaticReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldStaticReadWrite handle, float value) {
            UNSAFE.putFloat(handle.base,
                             handle.fieldOffset,
                             value);
        }

        @ForceInline
        static void setVolatile(FieldStaticReadWrite handle, float value) {
            UNSAFE.putFloatVolatile(handle.base,
                                     handle.fieldOffset,
                                     value);
        }

        @ForceInline
        static void setOpaque(FieldStaticReadWrite handle, float value) {
            UNSAFE.putFloatOpaque(handle.base,
                                   handle.fieldOffset,
                                   value);
        }

        @ForceInline
        static void setRelease(FieldStaticReadWrite handle, float value) {
            UNSAFE.putFloatRelease(handle.base,
                                    handle.fieldOffset,
                                    value);
        }

        @ForceInline
        static boolean compareAndSet(FieldStaticReadWrite handle, float expected, float value) {
            return UNSAFE.compareAndSetFloat(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }


        @ForceInline
        static float compareAndExchange(FieldStaticReadWrite handle, float expected, float value) {
            return UNSAFE.compareAndExchangeFloat(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static float compareAndExchangeAcquire(FieldStaticReadWrite handle, float expected, float value) {
            return UNSAFE.compareAndExchangeFloatAcquire(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static float compareAndExchangeRelease(FieldStaticReadWrite handle, float expected, float value) {
            return UNSAFE.compareAndExchangeFloatRelease(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldStaticReadWrite handle, float expected, float value) {
            return UNSAFE.weakCompareAndSetFloatPlain(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldStaticReadWrite handle, float expected, float value) {
            return UNSAFE.weakCompareAndSetFloat(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldStaticReadWrite handle, float expected, float value) {
            return UNSAFE.weakCompareAndSetFloatAcquire(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldStaticReadWrite handle, float expected, float value) {
            return UNSAFE.weakCompareAndSetFloatRelease(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static float getAndSet(FieldStaticReadWrite handle, float value) {
            return UNSAFE.getAndSetFloat(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static float getAndSetAcquire(FieldStaticReadWrite handle, float value) {
            return UNSAFE.getAndSetFloatAcquire(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static float getAndSetRelease(FieldStaticReadWrite handle, float value) {
            return UNSAFE.getAndSetFloatRelease(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static float getAndAdd(FieldStaticReadWrite handle, float value) {
            return UNSAFE.getAndAddFloat(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static float getAndAddAcquire(FieldStaticReadWrite handle, float value) {
            return UNSAFE.getAndAddFloatAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static float getAndAddRelease(FieldStaticReadWrite handle, float value) {
            return UNSAFE.getAndAddFloatRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadWrite.class, null, float.class);
    }


    static final class Array extends VarHandle {
        final int abase;
        final int ashift;

        Array(int abase, int ashift) {
            super(Array.FORM);
            this.abase = abase;
            this.ashift = ashift;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(float[].class, float.class, int.class);
        }


        @ForceInline
        static float get(Array handle, Object oarray, int index) {
            float[] array = (float[]) oarray;
            return array[index];
        }

        @ForceInline
        static void set(Array handle, Object oarray, int index, float value) {
            float[] array = (float[]) oarray;
            array[index] = value;
        }

        @ForceInline
        static float getVolatile(Array handle, Object oarray, int index) {
            float[] array = (float[]) oarray;
            return UNSAFE.getFloatVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setVolatile(Array handle, Object oarray, int index, float value) {
            float[] array = (float[]) oarray;
            UNSAFE.putFloatVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static float getOpaque(Array handle, Object oarray, int index) {
            float[] array = (float[]) oarray;
            return UNSAFE.getFloatOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setOpaque(Array handle, Object oarray, int index, float value) {
            float[] array = (float[]) oarray;
            UNSAFE.putFloatOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static float getAcquire(Array handle, Object oarray, int index) {
            float[] array = (float[]) oarray;
            return UNSAFE.getFloatAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setRelease(Array handle, Object oarray, int index, float value) {
            float[] array = (float[]) oarray;
            UNSAFE.putFloatRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static boolean compareAndSet(Array handle, Object oarray, int index, float expected, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.compareAndSetFloat(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static float compareAndExchange(Array handle, Object oarray, int index, float expected, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.compareAndExchangeFloat(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static float compareAndExchangeAcquire(Array handle, Object oarray, int index, float expected, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.compareAndExchangeFloatAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static float compareAndExchangeRelease(Array handle, Object oarray, int index, float expected, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.compareAndExchangeFloatRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(Array handle, Object oarray, int index, float expected, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.weakCompareAndSetFloatPlain(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSet(Array handle, Object oarray, int index, float expected, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.weakCompareAndSetFloat(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(Array handle, Object oarray, int index, float expected, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.weakCompareAndSetFloatAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(Array handle, Object oarray, int index, float expected, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.weakCompareAndSetFloatRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static float getAndSet(Array handle, Object oarray, int index, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.getAndSetFloat(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static float getAndSetAcquire(Array handle, Object oarray, int index, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.getAndSetFloatAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static float getAndSetRelease(Array handle, Object oarray, int index, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.getAndSetFloatRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static float getAndAdd(Array handle, Object oarray, int index, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.getAndAddFloat(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static float getAndAddAcquire(Array handle, Object oarray, int index, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.getAndAddFloatAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static float getAndAddRelease(Array handle, Object oarray, int index, float value) {
            float[] array = (float[]) oarray;
            return UNSAFE.getAndAddFloatRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        static final VarForm FORM = new VarForm(Array.class, float[].class, float.class, int.class);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\VarHandleGuards.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2014, 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang.invoke;

import jdk.internal.vm.annotation.ForceInline;

// This class is auto-generated by java.lang.invoke.VarHandles$GuardMethodGenerator. Do not edit.
final class VarHandleGuards {

    @ForceInline
    @LambdaForm.Compiled
    final static Object guard_L_L(VarHandle handle, Object arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            Object r = MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
            return ad.returnType.cast(r);
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LL_V(VarHandle handle, Object arg0, Object arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static Object guard_LL_L(VarHandle handle, Object arg0, Object arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            Object r = MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
            return ad.returnType.cast(r);
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LLL_Z(VarHandle handle, Object arg0, Object arg1, Object arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static Object guard_LLL_L(VarHandle handle, Object arg0, Object arg1, Object arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            Object r = MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
            return ad.returnType.cast(r);
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static int guard_L_I(VarHandle handle, Object arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (int) MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (int) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LI_V(VarHandle handle, Object arg0, int arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static int guard_LI_I(VarHandle handle, Object arg0, int arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (int) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (int) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LII_Z(VarHandle handle, Object arg0, int arg1, int arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static int guard_LII_I(VarHandle handle, Object arg0, int arg1, int arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (int) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (int) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static long guard_L_J(VarHandle handle, Object arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (long) MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (long) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LJ_V(VarHandle handle, Object arg0, long arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static long guard_LJ_J(VarHandle handle, Object arg0, long arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (long) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (long) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LJJ_Z(VarHandle handle, Object arg0, long arg1, long arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static long guard_LJJ_J(VarHandle handle, Object arg0, long arg1, long arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (long) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (long) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static float guard_L_F(VarHandle handle, Object arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (float) MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (float) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LF_V(VarHandle handle, Object arg0, float arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static float guard_LF_F(VarHandle handle, Object arg0, float arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (float) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (float) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LFF_Z(VarHandle handle, Object arg0, float arg1, float arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static float guard_LFF_F(VarHandle handle, Object arg0, float arg1, float arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (float) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (float) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static double guard_L_D(VarHandle handle, Object arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (double) MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (double) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LD_V(VarHandle handle, Object arg0, double arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static double guard_LD_D(VarHandle handle, Object arg0, double arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (double) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (double) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LDD_Z(VarHandle handle, Object arg0, double arg1, double arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static double guard_LDD_D(VarHandle handle, Object arg0, double arg1, double arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (double) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (double) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static Object guard__L(VarHandle handle, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            Object r = MethodHandle.linkToStatic(handle, handle.vform.getMemberName(ad.mode));
            return ad.returnType.cast(r);
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_L_V(VarHandle handle, Object arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LL_Z(VarHandle handle, Object arg0, Object arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static int guard__I(VarHandle handle, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (int) MethodHandle.linkToStatic(handle, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (int) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_I_V(VarHandle handle, int arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static int guard_I_I(VarHandle handle, int arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (int) MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (int) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_II_Z(VarHandle handle, int arg0, int arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static int guard_II_I(VarHandle handle, int arg0, int arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (int) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (int) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static long guard__J(VarHandle handle, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (long) MethodHandle.linkToStatic(handle, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (long) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_J_V(VarHandle handle, long arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static long guard_J_J(VarHandle handle, long arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (long) MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (long) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_JJ_Z(VarHandle handle, long arg0, long arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static long guard_JJ_J(VarHandle handle, long arg0, long arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (long) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (long) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static float guard__F(VarHandle handle, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (float) MethodHandle.linkToStatic(handle, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (float) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_F_V(VarHandle handle, float arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static float guard_F_F(VarHandle handle, float arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (float) MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (float) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_FF_Z(VarHandle handle, float arg0, float arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static float guard_FF_F(VarHandle handle, float arg0, float arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (float) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (float) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static double guard__D(VarHandle handle, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (double) MethodHandle.linkToStatic(handle, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (double) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_D_V(VarHandle handle, double arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static double guard_D_D(VarHandle handle, double arg0, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (double) MethodHandle.linkToStatic(handle, arg0, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (double) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_DD_Z(VarHandle handle, double arg0, double arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static double guard_DD_D(VarHandle handle, double arg0, double arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (double) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (double) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static Object guard_LI_L(VarHandle handle, Object arg0, int arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            Object r = MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
            return ad.returnType.cast(r);
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LIL_V(VarHandle handle, Object arg0, int arg1, Object arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static Object guard_LIL_L(VarHandle handle, Object arg0, int arg1, Object arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            Object r = MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
            return ad.returnType.cast(r);
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LILL_Z(VarHandle handle, Object arg0, int arg1, Object arg2, Object arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static Object guard_LILL_L(VarHandle handle, Object arg0, int arg1, Object arg2, Object arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            Object r = MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
            return ad.returnType.cast(r);
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LII_V(VarHandle handle, Object arg0, int arg1, int arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LIII_Z(VarHandle handle, Object arg0, int arg1, int arg2, int arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static int guard_LIII_I(VarHandle handle, Object arg0, int arg1, int arg2, int arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (int) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (int) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static long guard_LI_J(VarHandle handle, Object arg0, int arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (long) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (long) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LIJ_V(VarHandle handle, Object arg0, int arg1, long arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static long guard_LIJ_J(VarHandle handle, Object arg0, int arg1, long arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (long) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (long) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LIJJ_Z(VarHandle handle, Object arg0, int arg1, long arg2, long arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static long guard_LIJJ_J(VarHandle handle, Object arg0, int arg1, long arg2, long arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (long) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (long) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static float guard_LI_F(VarHandle handle, Object arg0, int arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (float) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (float) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LIF_V(VarHandle handle, Object arg0, int arg1, float arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static float guard_LIF_F(VarHandle handle, Object arg0, int arg1, float arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (float) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (float) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LIFF_Z(VarHandle handle, Object arg0, int arg1, float arg2, float arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static float guard_LIFF_F(VarHandle handle, Object arg0, int arg1, float arg2, float arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (float) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (float) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static double guard_LI_D(VarHandle handle, Object arg0, int arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (double) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (double) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LID_V(VarHandle handle, Object arg0, int arg1, double arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static double guard_LID_D(VarHandle handle, Object arg0, int arg1, double arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (double) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (double) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LIDD_Z(VarHandle handle, Object arg0, int arg1, double arg2, double arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static double guard_LIDD_D(VarHandle handle, Object arg0, int arg1, double arg2, double arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (double) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (double) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static int guard_LJ_I(VarHandle handle, Object arg0, long arg1, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (int) MethodHandle.linkToStatic(handle, arg0, arg1, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (int) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LJI_V(VarHandle handle, Object arg0, long arg1, int arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static int guard_LJI_I(VarHandle handle, Object arg0, long arg1, int arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (int) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (int) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LJII_Z(VarHandle handle, Object arg0, long arg1, int arg2, int arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static int guard_LJII_I(VarHandle handle, Object arg0, long arg1, int arg2, int arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (int) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (int) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static void guard_LJJ_V(VarHandle handle, Object arg0, long arg1, long arg2, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodTypeErased) {
            MethodHandle.linkToStatic(handle, arg0, arg1, arg2, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static boolean guard_LJJJ_Z(VarHandle handle, Object arg0, long arg1, long arg2, long arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (boolean) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (boolean) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

    @ForceInline
    @LambdaForm.Compiled
    final static long guard_LJJJ_J(VarHandle handle, Object arg0, long arg1, long arg2, long arg3, VarHandle.AccessDescriptor ad) throws Throwable {
        if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodTypeErased) {
            return (long) MethodHandle.linkToStatic(handle, arg0, arg1, arg2, arg3, handle.vform.getMemberName(ad.mode));
        }
        else {
            MethodHandle mh = handle.getMethodHandle(ad.mode);
            return (long) mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(handle, arg0, arg1, arg2, arg3);
        }
    }

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\VarHandleInts.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang.invoke;

import jdk.internal.util.Preconditions;
import jdk.internal.vm.annotation.ForceInline;

import java.util.Objects;

import static java.lang.invoke.MethodHandleStatics.UNSAFE;

// -- This file was mechanically generated: Do not edit! -- //

final class VarHandleInts {

    static class FieldInstanceReadOnly extends VarHandle {
        final long fieldOffset;
        final Class<?> receiverType;

        FieldInstanceReadOnly(Class<?> receiverType, long fieldOffset) {
            this(receiverType, fieldOffset, FieldInstanceReadOnly.FORM);
        }

        protected FieldInstanceReadOnly(Class<?> receiverType, long fieldOffset,
                                        VarForm form) {
            super(form);
            this.fieldOffset = fieldOffset;
            this.receiverType = receiverType;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(receiverType, int.class);
        }

        @ForceInline
        static int get(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getInt(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static int getVolatile(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getIntVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static int getOpaque(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getIntOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static int getAcquire(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getIntAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldInstanceReadOnly.class, Object.class, int.class);
    }

    static final class FieldInstanceReadWrite extends FieldInstanceReadOnly {

        FieldInstanceReadWrite(Class<?> receiverType, long fieldOffset) {
            super(receiverType, fieldOffset, FieldInstanceReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldInstanceReadWrite handle, Object holder, int value) {
            UNSAFE.putInt(Objects.requireNonNull(handle.receiverType.cast(holder)),
                             handle.fieldOffset,
                             value);
        }

        @ForceInline
        static void setVolatile(FieldInstanceReadWrite handle, Object holder, int value) {
            UNSAFE.putIntVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                     handle.fieldOffset,
                                     value);
        }

        @ForceInline
        static void setOpaque(FieldInstanceReadWrite handle, Object holder, int value) {
            UNSAFE.putIntOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                   handle.fieldOffset,
                                   value);
        }

        @ForceInline
        static void setRelease(FieldInstanceReadWrite handle, Object holder, int value) {
            UNSAFE.putIntRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                    handle.fieldOffset,
                                    value);
        }

        @ForceInline
        static boolean compareAndSet(FieldInstanceReadWrite handle, Object holder, int expected, int value) {
            return UNSAFE.compareAndSetInt(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static int compareAndExchange(FieldInstanceReadWrite handle, Object holder, int expected, int value) {
            return UNSAFE.compareAndExchangeInt(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static int compareAndExchangeAcquire(FieldInstanceReadWrite handle, Object holder, int expected, int value) {
            return UNSAFE.compareAndExchangeIntAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static int compareAndExchangeRelease(FieldInstanceReadWrite handle, Object holder, int expected, int value) {
            return UNSAFE.compareAndExchangeIntRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldInstanceReadWrite handle, Object holder, int expected, int value) {
            return UNSAFE.weakCompareAndSetIntPlain(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldInstanceReadWrite handle, Object holder, int expected, int value) {
            return UNSAFE.weakCompareAndSetInt(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldInstanceReadWrite handle, Object holder, int expected, int value) {
            return UNSAFE.weakCompareAndSetIntAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldInstanceReadWrite handle, Object holder, int expected, int value) {
            return UNSAFE.weakCompareAndSetIntRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static int getAndSet(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndSetInt(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static int getAndSetAcquire(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndSetIntAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static int getAndSetRelease(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndSetIntRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static int getAndAdd(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndAddInt(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndAddAcquire(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndAddIntAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndAddRelease(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndAddIntRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }


        @ForceInline
        static int getAndBitwiseOr(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndBitwiseOrInt(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseOrRelease(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndBitwiseOrIntRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseOrAcquire(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndBitwiseOrIntAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseAnd(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndBitwiseAndInt(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseAndRelease(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndBitwiseAndIntRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseAndAcquire(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndBitwiseAndIntAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseXor(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndBitwiseXorInt(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseXorRelease(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndBitwiseXorIntRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseXorAcquire(FieldInstanceReadWrite handle, Object holder, int value) {
            return UNSAFE.getAndBitwiseXorIntAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        static final VarForm FORM = new VarForm(FieldInstanceReadWrite.class, Object.class, int.class);
    }


    static class FieldStaticReadOnly extends VarHandle {
        final Object base;
        final long fieldOffset;

        FieldStaticReadOnly(Object base, long fieldOffset) {
            this(base, fieldOffset, FieldStaticReadOnly.FORM);
        }

        protected FieldStaticReadOnly(Object base, long fieldOffset,
                                      VarForm form) {
            super(form);
            this.base = base;
            this.fieldOffset = fieldOffset;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(null, int.class);
        }

        @ForceInline
        static int get(FieldStaticReadOnly handle) {
            return UNSAFE.getInt(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static int getVolatile(FieldStaticReadOnly handle) {
            return UNSAFE.getIntVolatile(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static int getOpaque(FieldStaticReadOnly handle) {
            return UNSAFE.getIntOpaque(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static int getAcquire(FieldStaticReadOnly handle) {
            return UNSAFE.getIntAcquire(handle.base,
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadOnly.class, null, int.class);
    }

    static final class FieldStaticReadWrite extends FieldStaticReadOnly {

        FieldStaticReadWrite(Object base, long fieldOffset) {
            super(base, fieldOffset, FieldStaticReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldStaticReadWrite handle, int value) {
            UNSAFE.putInt(handle.base,
                             handle.fieldOffset,
                             value);
        }

        @ForceInline
        static void setVolatile(FieldStaticReadWrite handle, int value) {
            UNSAFE.putIntVolatile(handle.base,
                                     handle.fieldOffset,
                                     value);
        }

        @ForceInline
        static void setOpaque(FieldStaticReadWrite handle, int value) {
            UNSAFE.putIntOpaque(handle.base,
                                   handle.fieldOffset,
                                   value);
        }

        @ForceInline
        static void setRelease(FieldStaticReadWrite handle, int value) {
            UNSAFE.putIntRelease(handle.base,
                                    handle.fieldOffset,
                                    value);
        }

        @ForceInline
        static boolean compareAndSet(FieldStaticReadWrite handle, int expected, int value) {
            return UNSAFE.compareAndSetInt(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }


        @ForceInline
        static int compareAndExchange(FieldStaticReadWrite handle, int expected, int value) {
            return UNSAFE.compareAndExchangeInt(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static int compareAndExchangeAcquire(FieldStaticReadWrite handle, int expected, int value) {
            return UNSAFE.compareAndExchangeIntAcquire(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static int compareAndExchangeRelease(FieldStaticReadWrite handle, int expected, int value) {
            return UNSAFE.compareAndExchangeIntRelease(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldStaticReadWrite handle, int expected, int value) {
            return UNSAFE.weakCompareAndSetIntPlain(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldStaticReadWrite handle, int expected, int value) {
            return UNSAFE.weakCompareAndSetInt(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldStaticReadWrite handle, int expected, int value) {
            return UNSAFE.weakCompareAndSetIntAcquire(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldStaticReadWrite handle, int expected, int value) {
            return UNSAFE.weakCompareAndSetIntRelease(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static int getAndSet(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndSetInt(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static int getAndSetAcquire(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndSetIntAcquire(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static int getAndSetRelease(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndSetIntRelease(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static int getAndAdd(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndAddInt(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndAddAcquire(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndAddIntAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndAddRelease(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndAddIntRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseOr(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndBitwiseOrInt(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseOrRelease(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndBitwiseOrIntRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseOrAcquire(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndBitwiseOrIntAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseAnd(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndBitwiseAndInt(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseAndRelease(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndBitwiseAndIntRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseAndAcquire(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndBitwiseAndIntAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseXor(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndBitwiseXorInt(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseXorRelease(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndBitwiseXorIntRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseXorAcquire(FieldStaticReadWrite handle, int value) {
            return UNSAFE.getAndBitwiseXorIntAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadWrite.class, null, int.class);
    }


    static final class Array extends VarHandle {
        final int abase;
        final int ashift;

        Array(int abase, int ashift) {
            super(Array.FORM);
            this.abase = abase;
            this.ashift = ashift;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(int[].class, int.class, int.class);
        }


        @ForceInline
        static int get(Array handle, Object oarray, int index) {
            int[] array = (int[]) oarray;
            return array[index];
        }

        @ForceInline
        static void set(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            array[index] = value;
        }

        @ForceInline
        static int getVolatile(Array handle, Object oarray, int index) {
            int[] array = (int[]) oarray;
            return UNSAFE.getIntVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setVolatile(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            UNSAFE.putIntVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static int getOpaque(Array handle, Object oarray, int index) {
            int[] array = (int[]) oarray;
            return UNSAFE.getIntOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setOpaque(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            UNSAFE.putIntOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static int getAcquire(Array handle, Object oarray, int index) {
            int[] array = (int[]) oarray;
            return UNSAFE.getIntAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setRelease(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            UNSAFE.putIntRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static boolean compareAndSet(Array handle, Object oarray, int index, int expected, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.compareAndSetInt(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static int compareAndExchange(Array handle, Object oarray, int index, int expected, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.compareAndExchangeInt(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static int compareAndExchangeAcquire(Array handle, Object oarray, int index, int expected, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.compareAndExchangeIntAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static int compareAndExchangeRelease(Array handle, Object oarray, int index, int expected, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.compareAndExchangeIntRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(Array handle, Object oarray, int index, int expected, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.weakCompareAndSetIntPlain(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSet(Array handle, Object oarray, int index, int expected, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.weakCompareAndSetInt(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(Array handle, Object oarray, int index, int expected, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.weakCompareAndSetIntAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(Array handle, Object oarray, int index, int expected, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.weakCompareAndSetIntRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static int getAndSet(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndSetInt(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static int getAndSetAcquire(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndSetIntAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static int getAndSetRelease(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndSetIntRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static int getAndAdd(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndAddInt(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static int getAndAddAcquire(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndAddIntAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static int getAndAddRelease(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndAddIntRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static int getAndBitwiseOr(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndBitwiseOrInt(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseOrRelease(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndBitwiseOrIntRelease(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseOrAcquire(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndBitwiseOrIntAcquire(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseAnd(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndBitwiseAndInt(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseAndRelease(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndBitwiseAndIntRelease(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseAndAcquire(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndBitwiseAndIntAcquire(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseXor(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndBitwiseXorInt(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseXorRelease(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndBitwiseXorIntRelease(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static int getAndBitwiseXorAcquire(Array handle, Object oarray, int index, int value) {
            int[] array = (int[]) oarray;
            return UNSAFE.getAndBitwiseXorIntAcquire(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        static final VarForm FORM = new VarForm(Array.class, int[].class, int.class, int.class);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\VarHandleLongs.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang.invoke;

import jdk.internal.util.Preconditions;
import jdk.internal.vm.annotation.ForceInline;

import java.util.Objects;

import static java.lang.invoke.MethodHandleStatics.UNSAFE;

// -- This file was mechanically generated: Do not edit! -- //

final class VarHandleLongs {

    static class FieldInstanceReadOnly extends VarHandle {
        final long fieldOffset;
        final Class<?> receiverType;

        FieldInstanceReadOnly(Class<?> receiverType, long fieldOffset) {
            this(receiverType, fieldOffset, FieldInstanceReadOnly.FORM);
        }

        protected FieldInstanceReadOnly(Class<?> receiverType, long fieldOffset,
                                        VarForm form) {
            super(form);
            this.fieldOffset = fieldOffset;
            this.receiverType = receiverType;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(receiverType, long.class);
        }

        @ForceInline
        static long get(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getLong(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static long getVolatile(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getLongVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static long getOpaque(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getLongOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static long getAcquire(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getLongAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldInstanceReadOnly.class, Object.class, long.class);
    }

    static final class FieldInstanceReadWrite extends FieldInstanceReadOnly {

        FieldInstanceReadWrite(Class<?> receiverType, long fieldOffset) {
            super(receiverType, fieldOffset, FieldInstanceReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldInstanceReadWrite handle, Object holder, long value) {
            UNSAFE.putLong(Objects.requireNonNull(handle.receiverType.cast(holder)),
                             handle.fieldOffset,
                             value);
        }

        @ForceInline
        static void setVolatile(FieldInstanceReadWrite handle, Object holder, long value) {
            UNSAFE.putLongVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                     handle.fieldOffset,
                                     value);
        }

        @ForceInline
        static void setOpaque(FieldInstanceReadWrite handle, Object holder, long value) {
            UNSAFE.putLongOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                   handle.fieldOffset,
                                   value);
        }

        @ForceInline
        static void setRelease(FieldInstanceReadWrite handle, Object holder, long value) {
            UNSAFE.putLongRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                    handle.fieldOffset,
                                    value);
        }

        @ForceInline
        static boolean compareAndSet(FieldInstanceReadWrite handle, Object holder, long expected, long value) {
            return UNSAFE.compareAndSetLong(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static long compareAndExchange(FieldInstanceReadWrite handle, Object holder, long expected, long value) {
            return UNSAFE.compareAndExchangeLong(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static long compareAndExchangeAcquire(FieldInstanceReadWrite handle, Object holder, long expected, long value) {
            return UNSAFE.compareAndExchangeLongAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static long compareAndExchangeRelease(FieldInstanceReadWrite handle, Object holder, long expected, long value) {
            return UNSAFE.compareAndExchangeLongRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldInstanceReadWrite handle, Object holder, long expected, long value) {
            return UNSAFE.weakCompareAndSetLongPlain(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldInstanceReadWrite handle, Object holder, long expected, long value) {
            return UNSAFE.weakCompareAndSetLong(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldInstanceReadWrite handle, Object holder, long expected, long value) {
            return UNSAFE.weakCompareAndSetLongAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldInstanceReadWrite handle, Object holder, long expected, long value) {
            return UNSAFE.weakCompareAndSetLongRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static long getAndSet(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndSetLong(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static long getAndSetAcquire(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndSetLongAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static long getAndSetRelease(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndSetLongRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static long getAndAdd(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndAddLong(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndAddAcquire(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndAddLongAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndAddRelease(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndAddLongRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }


        @ForceInline
        static long getAndBitwiseOr(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndBitwiseOrLong(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseOrRelease(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndBitwiseOrLongRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseOrAcquire(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndBitwiseOrLongAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseAnd(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndBitwiseAndLong(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseAndRelease(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndBitwiseAndLongRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseAndAcquire(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndBitwiseAndLongAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseXor(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndBitwiseXorLong(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseXorRelease(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndBitwiseXorLongRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseXorAcquire(FieldInstanceReadWrite handle, Object holder, long value) {
            return UNSAFE.getAndBitwiseXorLongAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        static final VarForm FORM = new VarForm(FieldInstanceReadWrite.class, Object.class, long.class);
    }


    static class FieldStaticReadOnly extends VarHandle {
        final Object base;
        final long fieldOffset;

        FieldStaticReadOnly(Object base, long fieldOffset) {
            this(base, fieldOffset, FieldStaticReadOnly.FORM);
        }

        protected FieldStaticReadOnly(Object base, long fieldOffset,
                                      VarForm form) {
            super(form);
            this.base = base;
            this.fieldOffset = fieldOffset;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(null, long.class);
        }

        @ForceInline
        static long get(FieldStaticReadOnly handle) {
            return UNSAFE.getLong(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static long getVolatile(FieldStaticReadOnly handle) {
            return UNSAFE.getLongVolatile(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static long getOpaque(FieldStaticReadOnly handle) {
            return UNSAFE.getLongOpaque(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static long getAcquire(FieldStaticReadOnly handle) {
            return UNSAFE.getLongAcquire(handle.base,
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadOnly.class, null, long.class);
    }

    static final class FieldStaticReadWrite extends FieldStaticReadOnly {

        FieldStaticReadWrite(Object base, long fieldOffset) {
            super(base, fieldOffset, FieldStaticReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldStaticReadWrite handle, long value) {
            UNSAFE.putLong(handle.base,
                             handle.fieldOffset,
                             value);
        }

        @ForceInline
        static void setVolatile(FieldStaticReadWrite handle, long value) {
            UNSAFE.putLongVolatile(handle.base,
                                     handle.fieldOffset,
                                     value);
        }

        @ForceInline
        static void setOpaque(FieldStaticReadWrite handle, long value) {
            UNSAFE.putLongOpaque(handle.base,
                                   handle.fieldOffset,
                                   value);
        }

        @ForceInline
        static void setRelease(FieldStaticReadWrite handle, long value) {
            UNSAFE.putLongRelease(handle.base,
                                    handle.fieldOffset,
                                    value);
        }

        @ForceInline
        static boolean compareAndSet(FieldStaticReadWrite handle, long expected, long value) {
            return UNSAFE.compareAndSetLong(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }


        @ForceInline
        static long compareAndExchange(FieldStaticReadWrite handle, long expected, long value) {
            return UNSAFE.compareAndExchangeLong(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static long compareAndExchangeAcquire(FieldStaticReadWrite handle, long expected, long value) {
            return UNSAFE.compareAndExchangeLongAcquire(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static long compareAndExchangeRelease(FieldStaticReadWrite handle, long expected, long value) {
            return UNSAFE.compareAndExchangeLongRelease(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldStaticReadWrite handle, long expected, long value) {
            return UNSAFE.weakCompareAndSetLongPlain(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldStaticReadWrite handle, long expected, long value) {
            return UNSAFE.weakCompareAndSetLong(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldStaticReadWrite handle, long expected, long value) {
            return UNSAFE.weakCompareAndSetLongAcquire(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldStaticReadWrite handle, long expected, long value) {
            return UNSAFE.weakCompareAndSetLongRelease(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static long getAndSet(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndSetLong(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static long getAndSetAcquire(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndSetLongAcquire(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static long getAndSetRelease(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndSetLongRelease(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static long getAndAdd(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndAddLong(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndAddAcquire(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndAddLongAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndAddRelease(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndAddLongRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseOr(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndBitwiseOrLong(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseOrRelease(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndBitwiseOrLongRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseOrAcquire(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndBitwiseOrLongAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseAnd(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndBitwiseAndLong(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseAndRelease(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndBitwiseAndLongRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseAndAcquire(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndBitwiseAndLongAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseXor(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndBitwiseXorLong(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseXorRelease(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndBitwiseXorLongRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseXorAcquire(FieldStaticReadWrite handle, long value) {
            return UNSAFE.getAndBitwiseXorLongAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadWrite.class, null, long.class);
    }


    static final class Array extends VarHandle {
        final int abase;
        final int ashift;

        Array(int abase, int ashift) {
            super(Array.FORM);
            this.abase = abase;
            this.ashift = ashift;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(long[].class, long.class, int.class);
        }


        @ForceInline
        static long get(Array handle, Object oarray, int index) {
            long[] array = (long[]) oarray;
            return array[index];
        }

        @ForceInline
        static void set(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            array[index] = value;
        }

        @ForceInline
        static long getVolatile(Array handle, Object oarray, int index) {
            long[] array = (long[]) oarray;
            return UNSAFE.getLongVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setVolatile(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            UNSAFE.putLongVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static long getOpaque(Array handle, Object oarray, int index) {
            long[] array = (long[]) oarray;
            return UNSAFE.getLongOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setOpaque(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            UNSAFE.putLongOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static long getAcquire(Array handle, Object oarray, int index) {
            long[] array = (long[]) oarray;
            return UNSAFE.getLongAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setRelease(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            UNSAFE.putLongRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static boolean compareAndSet(Array handle, Object oarray, int index, long expected, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.compareAndSetLong(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static long compareAndExchange(Array handle, Object oarray, int index, long expected, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.compareAndExchangeLong(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static long compareAndExchangeAcquire(Array handle, Object oarray, int index, long expected, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.compareAndExchangeLongAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static long compareAndExchangeRelease(Array handle, Object oarray, int index, long expected, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.compareAndExchangeLongRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(Array handle, Object oarray, int index, long expected, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.weakCompareAndSetLongPlain(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSet(Array handle, Object oarray, int index, long expected, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.weakCompareAndSetLong(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(Array handle, Object oarray, int index, long expected, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.weakCompareAndSetLongAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(Array handle, Object oarray, int index, long expected, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.weakCompareAndSetLongRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static long getAndSet(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndSetLong(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static long getAndSetAcquire(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndSetLongAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static long getAndSetRelease(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndSetLongRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static long getAndAdd(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndAddLong(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static long getAndAddAcquire(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndAddLongAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static long getAndAddRelease(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndAddLongRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static long getAndBitwiseOr(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndBitwiseOrLong(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseOrRelease(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndBitwiseOrLongRelease(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseOrAcquire(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndBitwiseOrLongAcquire(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseAnd(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndBitwiseAndLong(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseAndRelease(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndBitwiseAndLongRelease(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseAndAcquire(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndBitwiseAndLongAcquire(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseXor(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndBitwiseXorLong(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseXorRelease(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndBitwiseXorLongRelease(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static long getAndBitwiseXorAcquire(Array handle, Object oarray, int index, long value) {
            long[] array = (long[]) oarray;
            return UNSAFE.getAndBitwiseXorLongAcquire(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        static final VarForm FORM = new VarForm(Array.class, long[].class, long.class, int.class);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\VarHandleObjects.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang.invoke;

import jdk.internal.util.Preconditions;
import jdk.internal.vm.annotation.ForceInline;

import java.util.Objects;

import static java.lang.invoke.MethodHandleStatics.UNSAFE;

// -- This file was mechanically generated: Do not edit! -- //

final class VarHandleObjects {

    static class FieldInstanceReadOnly extends VarHandle {
        final long fieldOffset;
        final Class<?> receiverType;
        final Class<?> fieldType;

        FieldInstanceReadOnly(Class<?> receiverType, long fieldOffset, Class<?> fieldType) {
            this(receiverType, fieldOffset, fieldType, FieldInstanceReadOnly.FORM);
        }

        protected FieldInstanceReadOnly(Class<?> receiverType, long fieldOffset, Class<?> fieldType,
                                        VarForm form) {
            super(form);
            this.fieldOffset = fieldOffset;
            this.receiverType = receiverType;
            this.fieldType = fieldType;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(receiverType, fieldType);
        }

        @ForceInline
        static Object get(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getObject(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static Object getVolatile(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getObjectVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static Object getOpaque(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getObjectOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static Object getAcquire(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getObjectAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldInstanceReadOnly.class, Object.class, Object.class);
    }

    static final class FieldInstanceReadWrite extends FieldInstanceReadOnly {

        FieldInstanceReadWrite(Class<?> receiverType, long fieldOffset, Class<?> fieldType) {
            super(receiverType, fieldOffset, fieldType, FieldInstanceReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldInstanceReadWrite handle, Object holder, Object value) {
            UNSAFE.putObject(Objects.requireNonNull(handle.receiverType.cast(holder)),
                             handle.fieldOffset,
                             handle.fieldType.cast(value));
        }

        @ForceInline
        static void setVolatile(FieldInstanceReadWrite handle, Object holder, Object value) {
            UNSAFE.putObjectVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                     handle.fieldOffset,
                                     handle.fieldType.cast(value));
        }

        @ForceInline
        static void setOpaque(FieldInstanceReadWrite handle, Object holder, Object value) {
            UNSAFE.putObjectOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                   handle.fieldOffset,
                                   handle.fieldType.cast(value));
        }

        @ForceInline
        static void setRelease(FieldInstanceReadWrite handle, Object holder, Object value) {
            UNSAFE.putObjectRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                    handle.fieldOffset,
                                    handle.fieldType.cast(value));
        }

        @ForceInline
        static boolean compareAndSet(FieldInstanceReadWrite handle, Object holder, Object expected, Object value) {
            return UNSAFE.compareAndSetObject(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static Object compareAndExchange(FieldInstanceReadWrite handle, Object holder, Object expected, Object value) {
            return UNSAFE.compareAndExchangeObject(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static Object compareAndExchangeAcquire(FieldInstanceReadWrite handle, Object holder, Object expected, Object value) {
            return UNSAFE.compareAndExchangeObjectAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static Object compareAndExchangeRelease(FieldInstanceReadWrite handle, Object holder, Object expected, Object value) {
            return UNSAFE.compareAndExchangeObjectRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldInstanceReadWrite handle, Object holder, Object expected, Object value) {
            return UNSAFE.weakCompareAndSetObjectPlain(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldInstanceReadWrite handle, Object holder, Object expected, Object value) {
            return UNSAFE.weakCompareAndSetObject(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldInstanceReadWrite handle, Object holder, Object expected, Object value) {
            return UNSAFE.weakCompareAndSetObjectAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldInstanceReadWrite handle, Object holder, Object expected, Object value) {
            return UNSAFE.weakCompareAndSetObjectRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static Object getAndSet(FieldInstanceReadWrite handle, Object holder, Object value) {
            return UNSAFE.getAndSetObject(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          handle.fieldType.cast(value));
        }

        @ForceInline
        static Object getAndSetAcquire(FieldInstanceReadWrite handle, Object holder, Object value) {
            return UNSAFE.getAndSetObjectAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          handle.fieldType.cast(value));
        }

        @ForceInline
        static Object getAndSetRelease(FieldInstanceReadWrite handle, Object holder, Object value) {
            return UNSAFE.getAndSetObjectRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          handle.fieldType.cast(value));
        }

        static final VarForm FORM = new VarForm(FieldInstanceReadWrite.class, Object.class, Object.class);
    }


    static class FieldStaticReadOnly extends VarHandle {
        final Object base;
        final long fieldOffset;
        final Class<?> fieldType;

        FieldStaticReadOnly(Object base, long fieldOffset, Class<?> fieldType) {
            this(base, fieldOffset, fieldType, FieldStaticReadOnly.FORM);
        }

        protected FieldStaticReadOnly(Object base, long fieldOffset, Class<?> fieldType,
                                      VarForm form) {
            super(form);
            this.base = base;
            this.fieldOffset = fieldOffset;
            this.fieldType = fieldType;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(null, fieldType);
        }

        @ForceInline
        static Object get(FieldStaticReadOnly handle) {
            return UNSAFE.getObject(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static Object getVolatile(FieldStaticReadOnly handle) {
            return UNSAFE.getObjectVolatile(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static Object getOpaque(FieldStaticReadOnly handle) {
            return UNSAFE.getObjectOpaque(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static Object getAcquire(FieldStaticReadOnly handle) {
            return UNSAFE.getObjectAcquire(handle.base,
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadOnly.class, null, Object.class);
    }

    static final class FieldStaticReadWrite extends FieldStaticReadOnly {

        FieldStaticReadWrite(Object base, long fieldOffset, Class<?> fieldType) {
            super(base, fieldOffset, fieldType, FieldStaticReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldStaticReadWrite handle, Object value) {
            UNSAFE.putObject(handle.base,
                             handle.fieldOffset,
                             handle.fieldType.cast(value));
        }

        @ForceInline
        static void setVolatile(FieldStaticReadWrite handle, Object value) {
            UNSAFE.putObjectVolatile(handle.base,
                                     handle.fieldOffset,
                                     handle.fieldType.cast(value));
        }

        @ForceInline
        static void setOpaque(FieldStaticReadWrite handle, Object value) {
            UNSAFE.putObjectOpaque(handle.base,
                                   handle.fieldOffset,
                                   handle.fieldType.cast(value));
        }

        @ForceInline
        static void setRelease(FieldStaticReadWrite handle, Object value) {
            UNSAFE.putObjectRelease(handle.base,
                                    handle.fieldOffset,
                                    handle.fieldType.cast(value));
        }

        @ForceInline
        static boolean compareAndSet(FieldStaticReadWrite handle, Object expected, Object value) {
            return UNSAFE.compareAndSetObject(handle.base,
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }


        @ForceInline
        static Object compareAndExchange(FieldStaticReadWrite handle, Object expected, Object value) {
            return UNSAFE.compareAndExchangeObject(handle.base,
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static Object compareAndExchangeAcquire(FieldStaticReadWrite handle, Object expected, Object value) {
            return UNSAFE.compareAndExchangeObjectAcquire(handle.base,
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static Object compareAndExchangeRelease(FieldStaticReadWrite handle, Object expected, Object value) {
            return UNSAFE.compareAndExchangeObjectRelease(handle.base,
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldStaticReadWrite handle, Object expected, Object value) {
            return UNSAFE.weakCompareAndSetObjectPlain(handle.base,
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldStaticReadWrite handle, Object expected, Object value) {
            return UNSAFE.weakCompareAndSetObject(handle.base,
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldStaticReadWrite handle, Object expected, Object value) {
            return UNSAFE.weakCompareAndSetObjectAcquire(handle.base,
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldStaticReadWrite handle, Object expected, Object value) {
            return UNSAFE.weakCompareAndSetObjectRelease(handle.base,
                                               handle.fieldOffset,
                                               handle.fieldType.cast(expected),
                                               handle.fieldType.cast(value));
        }

        @ForceInline
        static Object getAndSet(FieldStaticReadWrite handle, Object value) {
            return UNSAFE.getAndSetObject(handle.base,
                                          handle.fieldOffset,
                                          handle.fieldType.cast(value));
        }

        @ForceInline
        static Object getAndSetAcquire(FieldStaticReadWrite handle, Object value) {
            return UNSAFE.getAndSetObjectAcquire(handle.base,
                                          handle.fieldOffset,
                                          handle.fieldType.cast(value));
        }

        @ForceInline
        static Object getAndSetRelease(FieldStaticReadWrite handle, Object value) {
            return UNSAFE.getAndSetObjectRelease(handle.base,
                                          handle.fieldOffset,
                                          handle.fieldType.cast(value));
        }

        static final VarForm FORM = new VarForm(FieldStaticReadWrite.class, null, Object.class);
    }


    static final class Array extends VarHandle {
        final int abase;
        final int ashift;
        final Class<?> arrayType;
        final Class<?> componentType;

        Array(int abase, int ashift, Class<?> arrayType) {
            super(Array.FORM);
            this.abase = abase;
            this.ashift = ashift;
            this.arrayType = arrayType;
            this.componentType = arrayType.getComponentType();
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(arrayType, arrayType.getComponentType(), int.class);
        }

        @ForceInline
        static Object runtimeTypeCheck(Array handle, Object[] oarray, Object value) {
            if (handle.arrayType == oarray.getClass()) {
                // Fast path: static array type same as argument array type
                return handle.componentType.cast(value);
            } else {
                // Slow path: check value against argument array component type
                return reflectiveTypeCheck(oarray, value);
            }
        }

        @ForceInline
        static Object reflectiveTypeCheck(Object[] oarray, Object value) {
            try {
                return oarray.getClass().getComponentType().cast(value);
            } catch (ClassCastException e) {
                throw new ArrayStoreException();
            }
        }

        @ForceInline
        static Object get(Array handle, Object oarray, int index) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return array[index];
        }

        @ForceInline
        static void set(Array handle, Object oarray, int index, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            array[index] = handle.componentType.cast(value);
        }

        @ForceInline
        static Object getVolatile(Array handle, Object oarray, int index) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.getObjectVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setVolatile(Array handle, Object oarray, int index, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            UNSAFE.putObjectVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static Object getOpaque(Array handle, Object oarray, int index) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.getObjectOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setOpaque(Array handle, Object oarray, int index, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            UNSAFE.putObjectOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static Object getAcquire(Array handle, Object oarray, int index) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.getObjectAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setRelease(Array handle, Object oarray, int index, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            UNSAFE.putObjectRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static boolean compareAndSet(Array handle, Object oarray, int index, Object expected, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.compareAndSetObject(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    handle.componentType.cast(expected),
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static Object compareAndExchange(Array handle, Object oarray, int index, Object expected, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.compareAndExchangeObject(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    handle.componentType.cast(expected),
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static Object compareAndExchangeAcquire(Array handle, Object oarray, int index, Object expected, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.compareAndExchangeObjectAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    handle.componentType.cast(expected),
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static Object compareAndExchangeRelease(Array handle, Object oarray, int index, Object expected, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.compareAndExchangeObjectRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    handle.componentType.cast(expected),
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(Array handle, Object oarray, int index, Object expected, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.weakCompareAndSetObjectPlain(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    handle.componentType.cast(expected),
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static boolean weakCompareAndSet(Array handle, Object oarray, int index, Object expected, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.weakCompareAndSetObject(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    handle.componentType.cast(expected),
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(Array handle, Object oarray, int index, Object expected, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.weakCompareAndSetObjectAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    handle.componentType.cast(expected),
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(Array handle, Object oarray, int index, Object expected, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.weakCompareAndSetObjectRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    handle.componentType.cast(expected),
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static Object getAndSet(Array handle, Object oarray, int index, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.getAndSetObject(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static Object getAndSetAcquire(Array handle, Object oarray, int index, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.getAndSetObjectAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    runtimeTypeCheck(handle, array, value));
        }

        @ForceInline
        static Object getAndSetRelease(Array handle, Object oarray, int index, Object value) {
            Object[] array = (Object[]) handle.arrayType.cast(oarray);
            return UNSAFE.getAndSetObjectRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    runtimeTypeCheck(handle, array, value));
        }

        static final VarForm FORM = new VarForm(Array.class, Object[].class, Object.class, int.class);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\VarHandles.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import static java.lang.invoke.MethodHandleStatics.UNSAFE;

final class VarHandles {

    static VarHandle makeFieldHandle(MemberName f, Class<?> refc, Class<?> type, boolean isWriteAllowedOnFinalFields) {
        if (!f.isStatic()) {
            long foffset = MethodHandleNatives.objectFieldOffset(f);
            if (!type.isPrimitive()) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleObjects.FieldInstanceReadOnly(refc, foffset, type)
                       : new VarHandleObjects.FieldInstanceReadWrite(refc, foffset, type);
            }
            else if (type == boolean.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleBooleans.FieldInstanceReadOnly(refc, foffset)
                       : new VarHandleBooleans.FieldInstanceReadWrite(refc, foffset);
            }
            else if (type == byte.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleBytes.FieldInstanceReadOnly(refc, foffset)
                       : new VarHandleBytes.FieldInstanceReadWrite(refc, foffset);
            }
            else if (type == short.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleShorts.FieldInstanceReadOnly(refc, foffset)
                       : new VarHandleShorts.FieldInstanceReadWrite(refc, foffset);
            }
            else if (type == char.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleChars.FieldInstanceReadOnly(refc, foffset)
                       : new VarHandleChars.FieldInstanceReadWrite(refc, foffset);
            }
            else if (type == int.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleInts.FieldInstanceReadOnly(refc, foffset)
                       : new VarHandleInts.FieldInstanceReadWrite(refc, foffset);
            }
            else if (type == long.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleLongs.FieldInstanceReadOnly(refc, foffset)
                       : new VarHandleLongs.FieldInstanceReadWrite(refc, foffset);
            }
            else if (type == float.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleFloats.FieldInstanceReadOnly(refc, foffset)
                       : new VarHandleFloats.FieldInstanceReadWrite(refc, foffset);
            }
            else if (type == double.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleDoubles.FieldInstanceReadOnly(refc, foffset)
                       : new VarHandleDoubles.FieldInstanceReadWrite(refc, foffset);
            }
            else {
                throw new UnsupportedOperationException();
            }
        }
        else {
            // TODO This is not lazy on first invocation
            // and might cause some circular initialization issues

            // Replace with something similar to direct method handles
            // where a barrier is used then elided after use

            if (UNSAFE.shouldBeInitialized(refc))
                UNSAFE.ensureClassInitialized(refc);

            Object base = MethodHandleNatives.staticFieldBase(f);
            long foffset = MethodHandleNatives.staticFieldOffset(f);
            if (!type.isPrimitive()) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleObjects.FieldStaticReadOnly(base, foffset, type)
                       : new VarHandleObjects.FieldStaticReadWrite(base, foffset, type);
            }
            else if (type == boolean.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleBooleans.FieldStaticReadOnly(base, foffset)
                       : new VarHandleBooleans.FieldStaticReadWrite(base, foffset);
            }
            else if (type == byte.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleBytes.FieldStaticReadOnly(base, foffset)
                       : new VarHandleBytes.FieldStaticReadWrite(base, foffset);
            }
            else if (type == short.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleShorts.FieldStaticReadOnly(base, foffset)
                       : new VarHandleShorts.FieldStaticReadWrite(base, foffset);
            }
            else if (type == char.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleChars.FieldStaticReadOnly(base, foffset)
                       : new VarHandleChars.FieldStaticReadWrite(base, foffset);
            }
            else if (type == int.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleInts.FieldStaticReadOnly(base, foffset)
                       : new VarHandleInts.FieldStaticReadWrite(base, foffset);
            }
            else if (type == long.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleLongs.FieldStaticReadOnly(base, foffset)
                       : new VarHandleLongs.FieldStaticReadWrite(base, foffset);
            }
            else if (type == float.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleFloats.FieldStaticReadOnly(base, foffset)
                       : new VarHandleFloats.FieldStaticReadWrite(base, foffset);
            }
            else if (type == double.class) {
                return f.isFinal() && !isWriteAllowedOnFinalFields
                       ? new VarHandleDoubles.FieldStaticReadOnly(base, foffset)
                       : new VarHandleDoubles.FieldStaticReadWrite(base, foffset);
            }
            else {
                throw new UnsupportedOperationException();
            }
        }
    }

    static VarHandle makeArrayElementHandle(Class<?> arrayClass) {
        if (!arrayClass.isArray())
            throw new IllegalArgumentException("not an array: " + arrayClass);

        Class<?> componentType = arrayClass.getComponentType();

        int aoffset = UNSAFE.arrayBaseOffset(arrayClass);
        int ascale = UNSAFE.arrayIndexScale(arrayClass);
        int ashift = 31 - Integer.numberOfLeadingZeros(ascale);

        if (!componentType.isPrimitive()) {
            return new VarHandleObjects.Array(aoffset, ashift, arrayClass);
        }
        else if (componentType == boolean.class) {
            return new VarHandleBooleans.Array(aoffset, ashift);
        }
        else if (componentType == byte.class) {
            return new VarHandleBytes.Array(aoffset, ashift);
        }
        else if (componentType == short.class) {
            return new VarHandleShorts.Array(aoffset, ashift);
        }
        else if (componentType == char.class) {
            return new VarHandleChars.Array(aoffset, ashift);
        }
        else if (componentType == int.class) {
            return new VarHandleInts.Array(aoffset, ashift);
        }
        else if (componentType == long.class) {
            return new VarHandleLongs.Array(aoffset, ashift);
        }
        else if (componentType == float.class) {
            return new VarHandleFloats.Array(aoffset, ashift);
        }
        else if (componentType == double.class) {
            return new VarHandleDoubles.Array(aoffset, ashift);
        }
        else {
            throw new UnsupportedOperationException();
        }
    }

    static VarHandle byteArrayViewHandle(Class<?> viewArrayClass,
                                         boolean be) {
        if (!viewArrayClass.isArray())
            throw new IllegalArgumentException("not an array: " + viewArrayClass);

        Class<?> viewComponentType = viewArrayClass.getComponentType();

        if (viewComponentType == long.class) {
            return new VarHandleByteArrayAsLongs.ArrayHandle(be);
        }
        else if (viewComponentType == int.class) {
            return new VarHandleByteArrayAsInts.ArrayHandle(be);
        }
        else if (viewComponentType == short.class) {
            return new VarHandleByteArrayAsShorts.ArrayHandle(be);
        }
        else if (viewComponentType == char.class) {
            return new VarHandleByteArrayAsChars.ArrayHandle(be);
        }
        else if (viewComponentType == double.class) {
            return new VarHandleByteArrayAsDoubles.ArrayHandle(be);
        }
        else if (viewComponentType == float.class) {
            return new VarHandleByteArrayAsFloats.ArrayHandle(be);
        }

        throw new UnsupportedOperationException();
    }

    static VarHandle makeByteBufferViewHandle(Class<?> viewArrayClass,
                                              boolean be) {
        if (!viewArrayClass.isArray())
            throw new IllegalArgumentException("not an array: " + viewArrayClass);

        Class<?> viewComponentType = viewArrayClass.getComponentType();

        if (viewComponentType == long.class) {
            return new VarHandleByteArrayAsLongs.ByteBufferHandle(be);
        }
        else if (viewComponentType == int.class) {
            return new VarHandleByteArrayAsInts.ByteBufferHandle(be);
        }
        else if (viewComponentType == short.class) {
            return new VarHandleByteArrayAsShorts.ByteBufferHandle(be);
        }
        else if (viewComponentType == char.class) {
            return new VarHandleByteArrayAsChars.ByteBufferHandle(be);
        }
        else if (viewComponentType == double.class) {
            return new VarHandleByteArrayAsDoubles.ByteBufferHandle(be);
        }
        else if (viewComponentType == float.class) {
            return new VarHandleByteArrayAsFloats.ByteBufferHandle(be);
        }

        throw new UnsupportedOperationException();
    }

//    /**
//     * A helper program to generate the VarHandleGuards class with a set of
//     * static guard methods each of which corresponds to a particular shape and
//     * performs a type check of the symbolic type descriptor with the VarHandle
//     * type descriptor before linking/invoking to the underlying operation as
//     * characterized by the operation member name on the VarForm of the
//     * VarHandle.
//     * <p>
//     * The generated class essentially encapsulates pre-compiled LambdaForms,
//     * one for each method, for the most set of common method signatures.
//     * This reduces static initialization costs, footprint costs, and circular
//     * dependencies that may arise if a class is generated per LambdaForm.
//     * <p>
//     * A maximum of L*T*S methods will be generated where L is the number of
//     * access modes kinds (or unique operation signatures) and T is the number
//     * of variable types and S is the number of shapes (such as instance field,
//     * static field, or array access).
//     * If there are 4 unique operation signatures, 5 basic types (Object, int,
//     * long, float, double), and 3 shapes then a maximum of 60 methods will be
//     * generated.  However, the number is likely to be less since there
//     * be duplicate signatures.
//     * <p>
//     * Each method is annotated with @LambdaForm.Compiled to inform the runtime
//     * that such methods should be treated as if a method of a class that is the
//     * result of compiling a LambdaForm.  Annotation of such methods is
//     * important for correct evaluation of certain assertions and method return
//     * type profiling in HotSpot.
//     */
//    public static class GuardMethodGenerator {
//
//        static final String GUARD_METHOD_SIG_TEMPLATE = "<RETURN> <NAME>_<SIGNATURE>(<PARAMS>)";
//
//        static final String GUARD_METHOD_TEMPLATE =
//                "@ForceInline\n" +
//                "@LambdaForm.Compiled\n" +
//                "final static <METHOD> throws Throwable {\n" +
//                "    if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodType) {\n" +
//                "        <RESULT_ERASED>MethodHandle.linkToStatic(<LINK_TO_STATIC_ARGS>);<RETURN_ERASED>\n" +
//                "    }\n" +
//                "    else {\n" +
//                "        MethodHandle mh = handle.getMethodHandle(ad.mode);\n" +
//                "        <RETURN>mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(<LINK_TO_INVOKER_ARGS>);\n" +
//                "    }\n" +
//                "}";
//
//        static final String GUARD_METHOD_TEMPLATE_V =
//                "@ForceInline\n" +
//                "@LambdaForm.Compiled\n" +
//                "final static <METHOD> throws Throwable {\n" +
//                "    if (handle.vform.methodType_table[ad.type] == ad.symbolicMethodType) {\n" +
//                "        MethodHandle.linkToStatic(<LINK_TO_STATIC_ARGS>);\n" +
//                "    }\n" +
//                "    else if (handle.vform.getMethodType_V(ad.type) == ad.symbolicMethodType) {\n" +
//                "        MethodHandle.linkToStatic(<LINK_TO_STATIC_ARGS>);\n" +
//                "    }\n" +
//                "    else {\n" +
//                "        MethodHandle mh = handle.getMethodHandle(ad.mode);\n" +
//                "        mh.asType(ad.symbolicMethodTypeInvoker).invokeBasic(<LINK_TO_INVOKER_ARGS>);\n" +
//                "    }\n" +
//                "}";
//
//        // A template for deriving the operations
//        // could be supported by annotating VarHandle directly with the
//        // operation kind and shape
//        interface VarHandleTemplate {
//            Object get();
//
//            void set(Object value);
//
//            boolean compareAndSet(Object actualValue, Object expectedValue);
//
//            Object compareAndExchange(Object actualValue, Object expectedValue);
//
//            Object getAndUpdate(Object value);
//        }
//
//        static class HandleType {
//            final Class<?> receiver;
//            final Class<?>[] intermediates;
//            final Class<?> value;
//
//            HandleType(Class<?> receiver, Class<?> value, Class<?>... intermediates) {
//                this.receiver = receiver;
//                this.intermediates = intermediates;
//                this.value = value;
//            }
//        }
//
//        /**
//         * @param args parameters
//         */
//        public static void main(String[] args) {
//            System.out.println("package java.lang.invoke;");
//            System.out.println();
//            System.out.println("import jdk.internal.vm.annotation.ForceInline;");
//            System.out.println();
//            System.out.println("// This class is auto-generated by " +
//                               GuardMethodGenerator.class.getName() +
//                               ". Do not edit.");
//            System.out.println("final class VarHandleGuards {");
//
//            System.out.println();
//
//            // Declare the stream of shapes
//            Stream<HandleType> hts = Stream.of(
//                    // Object->Object
//                    new HandleType(Object.class, Object.class),
//                    // Object->int
//                    new HandleType(Object.class, int.class),
//                    // Object->long
//                    new HandleType(Object.class, long.class),
//                    // Object->float
//                    new HandleType(Object.class, float.class),
//                    // Object->double
//                    new HandleType(Object.class, double.class),
//
//                    // <static>->Object
//                    new HandleType(null, Object.class),
//                    // <static>->int
//                    new HandleType(null, int.class),
//                    // <static>->long
//                    new HandleType(null, long.class),
//                    // <static>->float
//                    new HandleType(null, float.class),
//                    // <static>->double
//                    new HandleType(null, double.class),
//
//                    // Array[int]->Object
//                    new HandleType(Object.class, Object.class, int.class),
//                    // Array[int]->int
//                    new HandleType(Object.class, int.class, int.class),
//                    // Array[int]->long
//                    new HandleType(Object.class, long.class, int.class),
//                    // Array[int]->float
//                    new HandleType(Object.class, float.class, int.class),
//                    // Array[int]->double
//                    new HandleType(Object.class, double.class, int.class),
//
//                    // Array[long]->int
//                    new HandleType(Object.class, int.class, long.class),
//                    // Array[long]->long
//                    new HandleType(Object.class, long.class, long.class)
//            );
//
//            hts.flatMap(ht -> Stream.of(VarHandleTemplate.class.getMethods()).
//                    map(m -> generateMethodType(m, ht.receiver, ht.value, ht.intermediates))).
//                    distinct().
//                    map(mt -> generateMethod(mt)).
//                    forEach(s -> {
//                        System.out.println(s);
//                        System.out.println();
//                    });
//
//            System.out.println("}");
//        }
//
//        static MethodType generateMethodType(Method m, Class<?> receiver, Class<?> value, Class<?>... intermediates) {
//            Class<?> returnType = m.getReturnType() == Object.class
//                                  ? value : m.getReturnType();
//
//            List<Class<?>> params = new ArrayList<>();
//            if (receiver != null)
//                params.add(receiver);
//            for (int i = 0; i < intermediates.length; i++) {
//                params.add(intermediates[i]);
//            }
//            for (Parameter p : m.getParameters()) {
//                params.add(value);
//            }
//            return MethodType.methodType(returnType, params);
//        }
//
//        static String generateMethod(MethodType mt) {
//            Class<?> returnType = mt.returnType();
//
//            LinkedHashMap<String, Class<?>> params = new LinkedHashMap<>();
//            params.put("handle", VarHandle.class);
//            for (int i = 0; i < mt.parameterCount(); i++) {
//                params.put("arg" + i, mt.parameterType(i));
//            }
//            params.put("ad", VarHandle.AccessDescriptor.class);
//
//            // Generate method signature line
//            String RETURN = className(returnType);
//            String NAME = "guard";
//            String SIGNATURE = getSignature(mt);
//            String PARAMS = params.entrySet().stream().
//                    map(e -> className(e.getValue()) + " " + e.getKey()).
//                    collect(joining(", "));
//            String METHOD = GUARD_METHOD_SIG_TEMPLATE.
//                    replace("<RETURN>", RETURN).
//                    replace("<NAME>", NAME).
//                    replace("<SIGNATURE>", SIGNATURE).
//                    replace("<PARAMS>", PARAMS);
//
//            // Generate method
//            params.remove("ad");
//
//            List<String> LINK_TO_STATIC_ARGS = params.keySet().stream().
//                    collect(toList());
//            LINK_TO_STATIC_ARGS.add("handle.vform.getMemberName(ad.mode)");
//            List<String> LINK_TO_STATIC_ARGS_V = params.keySet().stream().
//                    collect(toList());
//            LINK_TO_STATIC_ARGS_V.add("handle.vform.getMemberName_V(ad.mode)");
//
//            List<String> LINK_TO_INVOKER_ARGS = params.keySet().stream().
//                    collect(toList());
//
//            RETURN = returnType == void.class
//                     ? ""
//                     : returnType == Object.class
//                       ? "return "
//                       : "return (" + returnType.getName() + ") ";
//
//            String RESULT_ERASED = returnType == void.class
//                                   ? ""
//                                   : returnType != Object.class
//                                     ? "return (" + returnType.getName() + ") "
//                                     : "Object r = ";
//
//            String RETURN_ERASED = returnType != Object.class
//                                   ? ""
//                                   : " return ad.returnType.cast(r);";
//
//            String template = returnType == void.class
//                              ? GUARD_METHOD_TEMPLATE_V
//                              : GUARD_METHOD_TEMPLATE;
//            return template.
//                    replace("<METHOD>", METHOD).
//                    replace("<NAME>", NAME).
//                    replaceAll("<RETURN>", RETURN).
//                    replace("<RESULT_ERASED>", RESULT_ERASED).
//                    replace("<RETURN_ERASED>", RETURN_ERASED).
//                    replaceAll("<LINK_TO_STATIC_ARGS>", LINK_TO_STATIC_ARGS.stream().
//                            collect(joining(", "))).
//                    replaceAll("<LINK_TO_STATIC_ARGS_V>", LINK_TO_STATIC_ARGS_V.stream().
//                            collect(joining(", "))).
//                    replace("<LINK_TO_INVOKER_ARGS>", LINK_TO_INVOKER_ARGS.stream().
//                            collect(joining(", ")))
//                    ;
//        }
//
//        static String className(Class<?> c) {
//            String n = c.getName();
//            if (n.startsWith("java.lang.")) {
//                n = n.replace("java.lang.", "");
//                if (n.startsWith("invoke.")) {
//                    n = n.replace("invoke.", "");
//                }
//            }
//            return n.replace('$', '.');
//        }
//
//        static String getSignature(MethodType m) {
//            StringBuilder sb = new StringBuilder(m.parameterCount() + 1);
//
//            for (int i = 0; i < m.parameterCount(); i++) {
//                Class<?> pt = m.parameterType(i);
//                sb.append(getCharType(pt));
//            }
//
//            sb.append('_').append(getCharType(m.returnType()));
//
//            return sb.toString();
//        }
//
//        static char getCharType(Class<?> pt) {
//            if (pt == void.class) {
//                return 'V';
//            }
//            else if (!pt.isPrimitive()) {
//                return 'L';
//            }
//            else if (pt == boolean.class) {
//                return 'Z';
//            }
//            else if (pt == int.class) {
//                return 'I';
//            }
//            else if (pt == long.class) {
//                return 'J';
//            }
//            else if (pt == float.class) {
//                return 'F';
//            }
//            else if (pt == double.class) {
//                return 'D';
//            }
//            else {
//                throw new IllegalStateException(pt.getName());
//            }
//        }
//    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\VarHandleShorts.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang.invoke;

import jdk.internal.util.Preconditions;
import jdk.internal.vm.annotation.ForceInline;

import java.util.Objects;

import static java.lang.invoke.MethodHandleStatics.UNSAFE;

// -- This file was mechanically generated: Do not edit! -- //

final class VarHandleShorts {

    static class FieldInstanceReadOnly extends VarHandle {
        final long fieldOffset;
        final Class<?> receiverType;

        FieldInstanceReadOnly(Class<?> receiverType, long fieldOffset) {
            this(receiverType, fieldOffset, FieldInstanceReadOnly.FORM);
        }

        protected FieldInstanceReadOnly(Class<?> receiverType, long fieldOffset,
                                        VarForm form) {
            super(form);
            this.fieldOffset = fieldOffset;
            this.receiverType = receiverType;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(receiverType, short.class);
        }

        @ForceInline
        static short get(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getShort(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static short getVolatile(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getShortVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static short getOpaque(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getShortOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        @ForceInline
        static short getAcquire(FieldInstanceReadOnly handle, Object holder) {
            return UNSAFE.getShortAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldInstanceReadOnly.class, Object.class, short.class);
    }

    static final class FieldInstanceReadWrite extends FieldInstanceReadOnly {

        FieldInstanceReadWrite(Class<?> receiverType, long fieldOffset) {
            super(receiverType, fieldOffset, FieldInstanceReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldInstanceReadWrite handle, Object holder, short value) {
            UNSAFE.putShort(Objects.requireNonNull(handle.receiverType.cast(holder)),
                             handle.fieldOffset,
                             value);
        }

        @ForceInline
        static void setVolatile(FieldInstanceReadWrite handle, Object holder, short value) {
            UNSAFE.putShortVolatile(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                     handle.fieldOffset,
                                     value);
        }

        @ForceInline
        static void setOpaque(FieldInstanceReadWrite handle, Object holder, short value) {
            UNSAFE.putShortOpaque(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                   handle.fieldOffset,
                                   value);
        }

        @ForceInline
        static void setRelease(FieldInstanceReadWrite handle, Object holder, short value) {
            UNSAFE.putShortRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                    handle.fieldOffset,
                                    value);
        }

        @ForceInline
        static boolean compareAndSet(FieldInstanceReadWrite handle, Object holder, short expected, short value) {
            return UNSAFE.compareAndSetShort(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static short compareAndExchange(FieldInstanceReadWrite handle, Object holder, short expected, short value) {
            return UNSAFE.compareAndExchangeShort(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static short compareAndExchangeAcquire(FieldInstanceReadWrite handle, Object holder, short expected, short value) {
            return UNSAFE.compareAndExchangeShortAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static short compareAndExchangeRelease(FieldInstanceReadWrite handle, Object holder, short expected, short value) {
            return UNSAFE.compareAndExchangeShortRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldInstanceReadWrite handle, Object holder, short expected, short value) {
            return UNSAFE.weakCompareAndSetShortPlain(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldInstanceReadWrite handle, Object holder, short expected, short value) {
            return UNSAFE.weakCompareAndSetShort(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldInstanceReadWrite handle, Object holder, short expected, short value) {
            return UNSAFE.weakCompareAndSetShortAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldInstanceReadWrite handle, Object holder, short expected, short value) {
            return UNSAFE.weakCompareAndSetShortRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static short getAndSet(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndSetShort(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static short getAndSetAcquire(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndSetShortAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static short getAndSetRelease(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndSetShortRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static short getAndAdd(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndAddShort(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndAddAcquire(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndAddShortAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndAddRelease(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndAddShortRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }


        @ForceInline
        static short getAndBitwiseOr(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndBitwiseOrShort(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseOrRelease(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndBitwiseOrShortRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseOrAcquire(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndBitwiseOrShortAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseAnd(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndBitwiseAndShort(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseAndRelease(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndBitwiseAndShortRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseAndAcquire(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndBitwiseAndShortAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseXor(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndBitwiseXorShort(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseXorRelease(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndBitwiseXorShortRelease(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseXorAcquire(FieldInstanceReadWrite handle, Object holder, short value) {
            return UNSAFE.getAndBitwiseXorShortAcquire(Objects.requireNonNull(handle.receiverType.cast(holder)),
                                       handle.fieldOffset,
                                       value);
        }

        static final VarForm FORM = new VarForm(FieldInstanceReadWrite.class, Object.class, short.class);
    }


    static class FieldStaticReadOnly extends VarHandle {
        final Object base;
        final long fieldOffset;

        FieldStaticReadOnly(Object base, long fieldOffset) {
            this(base, fieldOffset, FieldStaticReadOnly.FORM);
        }

        protected FieldStaticReadOnly(Object base, long fieldOffset,
                                      VarForm form) {
            super(form);
            this.base = base;
            this.fieldOffset = fieldOffset;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(null, short.class);
        }

        @ForceInline
        static short get(FieldStaticReadOnly handle) {
            return UNSAFE.getShort(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static short getVolatile(FieldStaticReadOnly handle) {
            return UNSAFE.getShortVolatile(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static short getOpaque(FieldStaticReadOnly handle) {
            return UNSAFE.getShortOpaque(handle.base,
                                 handle.fieldOffset);
        }

        @ForceInline
        static short getAcquire(FieldStaticReadOnly handle) {
            return UNSAFE.getShortAcquire(handle.base,
                                 handle.fieldOffset);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadOnly.class, null, short.class);
    }

    static final class FieldStaticReadWrite extends FieldStaticReadOnly {

        FieldStaticReadWrite(Object base, long fieldOffset) {
            super(base, fieldOffset, FieldStaticReadWrite.FORM);
        }

        @ForceInline
        static void set(FieldStaticReadWrite handle, short value) {
            UNSAFE.putShort(handle.base,
                             handle.fieldOffset,
                             value);
        }

        @ForceInline
        static void setVolatile(FieldStaticReadWrite handle, short value) {
            UNSAFE.putShortVolatile(handle.base,
                                     handle.fieldOffset,
                                     value);
        }

        @ForceInline
        static void setOpaque(FieldStaticReadWrite handle, short value) {
            UNSAFE.putShortOpaque(handle.base,
                                   handle.fieldOffset,
                                   value);
        }

        @ForceInline
        static void setRelease(FieldStaticReadWrite handle, short value) {
            UNSAFE.putShortRelease(handle.base,
                                    handle.fieldOffset,
                                    value);
        }

        @ForceInline
        static boolean compareAndSet(FieldStaticReadWrite handle, short expected, short value) {
            return UNSAFE.compareAndSetShort(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }


        @ForceInline
        static short compareAndExchange(FieldStaticReadWrite handle, short expected, short value) {
            return UNSAFE.compareAndExchangeShort(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static short compareAndExchangeAcquire(FieldStaticReadWrite handle, short expected, short value) {
            return UNSAFE.compareAndExchangeShortAcquire(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static short compareAndExchangeRelease(FieldStaticReadWrite handle, short expected, short value) {
            return UNSAFE.compareAndExchangeShortRelease(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(FieldStaticReadWrite handle, short expected, short value) {
            return UNSAFE.weakCompareAndSetShortPlain(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSet(FieldStaticReadWrite handle, short expected, short value) {
            return UNSAFE.weakCompareAndSetShort(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(FieldStaticReadWrite handle, short expected, short value) {
            return UNSAFE.weakCompareAndSetShortAcquire(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(FieldStaticReadWrite handle, short expected, short value) {
            return UNSAFE.weakCompareAndSetShortRelease(handle.base,
                                               handle.fieldOffset,
                                               expected,
                                               value);
        }

        @ForceInline
        static short getAndSet(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndSetShort(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static short getAndSetAcquire(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndSetShortAcquire(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static short getAndSetRelease(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndSetShortRelease(handle.base,
                                          handle.fieldOffset,
                                          value);
        }

        @ForceInline
        static short getAndAdd(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndAddShort(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndAddAcquire(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndAddShortAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndAddRelease(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndAddShortRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseOr(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndBitwiseOrShort(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseOrRelease(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndBitwiseOrShortRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseOrAcquire(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndBitwiseOrShortAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseAnd(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndBitwiseAndShort(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseAndRelease(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndBitwiseAndShortRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseAndAcquire(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndBitwiseAndShortAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseXor(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndBitwiseXorShort(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseXorRelease(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndBitwiseXorShortRelease(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseXorAcquire(FieldStaticReadWrite handle, short value) {
            return UNSAFE.getAndBitwiseXorShortAcquire(handle.base,
                                       handle.fieldOffset,
                                       value);
        }

        static final VarForm FORM = new VarForm(FieldStaticReadWrite.class, null, short.class);
    }


    static final class Array extends VarHandle {
        final int abase;
        final int ashift;

        Array(int abase, int ashift) {
            super(Array.FORM);
            this.abase = abase;
            this.ashift = ashift;
        }

        @Override
        final MethodType accessModeTypeUncached(AccessMode accessMode) {
            return accessMode.at.accessModeType(short[].class, short.class, int.class);
        }


        @ForceInline
        static short get(Array handle, Object oarray, int index) {
            short[] array = (short[]) oarray;
            return array[index];
        }

        @ForceInline
        static void set(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            array[index] = value;
        }

        @ForceInline
        static short getVolatile(Array handle, Object oarray, int index) {
            short[] array = (short[]) oarray;
            return UNSAFE.getShortVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setVolatile(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            UNSAFE.putShortVolatile(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static short getOpaque(Array handle, Object oarray, int index) {
            short[] array = (short[]) oarray;
            return UNSAFE.getShortOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setOpaque(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            UNSAFE.putShortOpaque(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static short getAcquire(Array handle, Object oarray, int index) {
            short[] array = (short[]) oarray;
            return UNSAFE.getShortAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase);
        }

        @ForceInline
        static void setRelease(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            UNSAFE.putShortRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static boolean compareAndSet(Array handle, Object oarray, int index, short expected, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.compareAndSetShort(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static short compareAndExchange(Array handle, Object oarray, int index, short expected, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.compareAndExchangeShort(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static short compareAndExchangeAcquire(Array handle, Object oarray, int index, short expected, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.compareAndExchangeShortAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static short compareAndExchangeRelease(Array handle, Object oarray, int index, short expected, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.compareAndExchangeShortRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetPlain(Array handle, Object oarray, int index, short expected, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.weakCompareAndSetShortPlain(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSet(Array handle, Object oarray, int index, short expected, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.weakCompareAndSetShort(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetAcquire(Array handle, Object oarray, int index, short expected, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.weakCompareAndSetShortAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static boolean weakCompareAndSetRelease(Array handle, Object oarray, int index, short expected, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.weakCompareAndSetShortRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    expected,
                    value);
        }

        @ForceInline
        static short getAndSet(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndSetShort(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static short getAndSetAcquire(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndSetShortAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static short getAndSetRelease(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndSetShortRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static short getAndAdd(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndAddShort(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static short getAndAddAcquire(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndAddShortAcquire(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static short getAndAddRelease(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndAddShortRelease(array,
                    (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                    value);
        }

        @ForceInline
        static short getAndBitwiseOr(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndBitwiseOrShort(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseOrRelease(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndBitwiseOrShortRelease(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseOrAcquire(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndBitwiseOrShortAcquire(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseAnd(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndBitwiseAndShort(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseAndRelease(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndBitwiseAndShortRelease(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseAndAcquire(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndBitwiseAndShortAcquire(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseXor(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndBitwiseXorShort(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseXorRelease(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndBitwiseXorShortRelease(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        @ForceInline
        static short getAndBitwiseXorAcquire(Array handle, Object oarray, int index, short value) {
            short[] array = (short[]) oarray;
            return UNSAFE.getAndBitwiseXorShortAcquire(array,
                                       (((long) Preconditions.checkIndex(index, array.length, AIOOBE_SUPPLIER)) << handle.ashift) + handle.abase,
                                       value);
        }

        static final VarForm FORM = new VarForm(Array.class, short[].class, short.class, int.class);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\VolatileCallSite.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2010, 2011, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

/**
 * A {@code VolatileCallSite} is a {@link CallSite} whose target acts like a volatile variable.
 * An {@code invokedynamic} instruction linked to a {@code VolatileCallSite} sees updates
 * to its call site target immediately, even if the update occurs in another thread.
 * There may be a performance penalty for such tight coupling between threads.
 * <p>
 * Unlike {@code MutableCallSite}, there is no
 * {@linkplain MutableCallSite#syncAll syncAll operation} on volatile
 * call sites, since every write to a volatile variable is implicitly
 * synchronized with reader threads.
 * <p>
 * In other respects, a {@code VolatileCallSite} is interchangeable
 * with {@code MutableCallSite}.
 * @see MutableCallSite
 * @author John Rose, JSR 292 EG
 * @since 1.7
 */
public class VolatileCallSite extends CallSite {
    /**
     * Creates a call site with a volatile binding to its target.
     * The initial target is set to a method handle
     * of the given type which will throw an {@code IllegalStateException} if called.
     * @param type the method type that this call site will have
     * @throws NullPointerException if the proposed type is null
     */
    public VolatileCallSite(MethodType type) {
        super(type);
    }

    /**
     * Creates a call site with a volatile binding to its target.
     * The target is set to the given value.
     * @param target the method handle that will be the initial target of the call site
     * @throws NullPointerException if the proposed target is null
     */
    public VolatileCallSite(MethodHandle target) {
        super(target);
    }

    /**
     * Returns the target method of the call site, which behaves
     * like a {@code volatile} field of the {@code VolatileCallSite}.
     * <p>
     * The interactions of {@code getTarget} with memory are the same
     * as of a read from a {@code volatile} field.
     * <p>
     * In particular, the current thread is required to issue a fresh
     * read of the target from memory, and must not fail to see
     * a recent update to the target by another thread.
     *
     * @return the linkage state of this call site, a method handle which can change over time
     * @see #setTarget
     */
    @Override public final MethodHandle getTarget() {
        return getTargetVolatile();
    }

    /**
     * Updates the target method of this call site, as a volatile variable.
     * The type of the new target must agree with the type of the old target.
     * <p>
     * The interactions with memory are the same as of a write to a volatile field.
     * In particular, any threads is guaranteed to see the updated target
     * the next time it calls {@code getTarget}.
     * @param newTarget the new target
     * @throws NullPointerException if the proposed new target is null
     * @throws WrongMethodTypeException if the proposed new target
     *         has a method type that differs from the previous target
     * @see #getTarget
     */
    @Override public void setTarget(MethodHandle newTarget) {
        checkTargetChange(getTargetVolatile(), newTarget);
        setTargetVolatile(newTarget);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final MethodHandle dynamicInvoker() {
        return makeDynamicInvoker();
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\WrongMethodTypeException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2008, 2012, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

/**
 * Thrown to indicate that code has attempted to call a method handle
 * via the wrong method type.  As with the bytecode representation of
 * normal Java method calls, method handle calls are strongly typed
 * to a specific type descriptor associated with a call site.
 * <p>
 * This exception may also be thrown when two method handles are
 * composed, and the system detects that their types cannot be
 * matched up correctly.  This amounts to an early evaluation
 * of the type mismatch, at method handle construction time,
 * instead of when the mismatched method handle is called.
 *
 * @author John Rose, JSR 292 EG
 * @since 1.7
 */
public class WrongMethodTypeException extends RuntimeException {
    private static final long serialVersionUID = 292L;

    /**
     * Constructs a {@code WrongMethodTypeException} with no detail message.
     */
    public WrongMethodTypeException() {
        super();
    }

    /**
     * Constructs a {@code WrongMethodTypeException} with the specified
     * detail message.
     *
     * @param s the detail message.
     */
    public WrongMethodTypeException(String s) {
        super(s);
    }

    /**
     * Constructs a {@code WrongMethodTypeException} with the specified
     * detail message and cause.
     *
     * @param s the detail message.
     * @param cause the cause of the exception, or null.
     */
    //FIXME: make this public in MR1
    /*non-public*/ WrongMethodTypeException(String s, Throwable cause) {
        super(s, cause);
    }

    /**
     * Constructs a {@code WrongMethodTypeException} with the specified
     * cause.
     *
     * @param cause the cause of the exception, or null.
     */
    //FIXME: make this public in MR1
    /*non-public*/ WrongMethodTypeException(Throwable cause) {
        super(cause);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\Iterable.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2003, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang;

import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

/**
 * Implementing this interface allows an object to be the target of the enhanced
 * {@code for} statement (sometimes called the "for-each loop" statement).
 *
 * @param <T> the type of elements returned by the iterator
 *
 * @since 1.5
 * @jls 14.14.2 The enhanced {@code for} statement
 */
public interface Iterable<T> {
    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    Iterator<T> iterator();

    /**
     * Performs the given action for each element of the {@code Iterable}
     * until all elements have been processed or the action throws an
     * exception.  Actions are performed in the order of iteration, if that
     * order is specified.  Exceptions thrown by the action are relayed to the
     * caller.
     * <p>
     * The behavior of this method is unspecified if the action performs
     * side-effects that modify the underlying source of elements, unless an
     * overriding class has specified a concurrent modification policy.
     *
     * @implSpec
     * <p>The default implementation behaves as if:
     * <pre>{@code
     *     for (T t : this)
     *         action.accept(t);
     * }</pre>
     *
     * @param action The action to be performed for each element
     * @throws NullPointerException if the specified action is null
     * @since 1.8
     */
    default void forEach(Consumer<? super T> action) {
        Objects.requireNonNull(action);
        for (T t : this) {
            action.accept(t);
        }
    }

    /**
     * Creates a {@link Spliterator} over the elements described by this
     * {@code Iterable}.
     *
     * @implSpec
     * The default implementation creates an
     * <em><a href="../util/Spliterator.html#binding">early-binding</a></em>
     * spliterator from the iterable's {@code Iterator}.  The spliterator
     * inherits the <em>fail-fast</em> properties of the iterable's iterator.
     *
     * @implNote
     * The default implementation should usually be overridden.  The
     * spliterator returned by the default implementation has poor splitting
     * capabilities, is unsized, and does not report any spliterator
     * characteristics. Implementing classes can nearly always provide a
     * better implementation.
     *
     * @return a {@code Spliterator} over the elements described by this
     * {@code Iterable}.
     * @since 1.8
     */
    default Spliterator<T> spliterator() {
        return Spliterators.spliteratorUnknownSize(iterator(), 0);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\LayerInstantiationException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown when creating a {@linkplain ModuleLayer module layer} fails.
 *
 * @see ModuleLayer
 * @since 9
 * @spec JPMS
 */
public class LayerInstantiationException extends RuntimeException {
    private static final long serialVersionUID = -906239691613568347L;

    /**
     * Constructs a {@code LayerInstantiationException} with no detail message.
     */
    public LayerInstantiationException() {
    }

    /**
     * Constructs a {@code LayerInstantiationException} with the given detail
     * message.
     *
     * @param msg
     *        The detail message; can be {@code null}
     */
    public LayerInstantiationException(String msg) {
        super(msg);
    }

    /**
     * Constructs a {@code LayerInstantiationException} with the given cause.
     *
     * @param cause
     *        The cause; can be {@code null}
     */
    public LayerInstantiationException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a {@code LayerInstantiationException} with the given detail
     * message and cause.
     *
     * @param msg
     *        The detail message; can be {@code null}
     * @param cause
     *        The cause; can be {@code null}
     */
    public LayerInstantiationException(String msg, Throwable cause) {
        super(msg, cause);
    }
}


【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\LinkageError.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1995, 2010, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Subclasses of {@code LinkageError} indicate that a class has
 * some dependency on another class; however, the latter class has
 * incompatibly changed after the compilation of the former class.
 *
 *
 * @author  Frank Yellin
 * @since   1.0
 */
public
class LinkageError extends Error {
    private static final long serialVersionUID = 3579600108157160122L;

    /**
     * Constructs a {@code LinkageError} with no detail message.
     */
    public LinkageError() {
        super();
    }

    /**
     * Constructs a {@code LinkageError} with the specified detail
     * message.
     *
     * @param   s   the detail message.
     */
    public LinkageError(String s) {
        super(s);
    }

    /**
     * Constructs a {@code LinkageError} with the specified detail
     * message and cause.
     *
     * @param s     the detail message.
     * @param cause the cause, may be {@code null}
     * @since 1.7
     */
    public LinkageError(String s, Throwable cause) {
        super(s, cause);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\LiveStackFrame.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang;

import java.lang.StackWalker.StackFrame;
import java.util.EnumSet;
import java.util.Set;

import static java.lang.StackWalker.ExtendedOption.LOCALS_AND_OPERANDS;

/**
 * <em>UNSUPPORTED</em> This interface is intended to be package-private
 * or move to an internal package.<p>
 *
 * {@code LiveStackFrame} represents a frame storing data and partial results.
 * Each frame has its own array of local variables (JVMS section 2.6.1),
 * its own operand stack (JVMS section 2.6.2) for a method invocation.
 *
 * @jvms 2.6 Frames
 */
/* package-private */
interface LiveStackFrame extends StackFrame {
    /**
     * Return the monitors held by this stack frame. This method returns
     * an empty array if no monitor is held by this stack frame.
     *
     * @return the monitors held by this stack frames
     */
    public Object[] getMonitors();

    /**
     * Gets the local variable array of this stack frame.
     *
     * <p>A single local variable can hold a value of type boolean, byte, char,
     * short, int, float, reference or returnAddress.  A pair of local variables
     * can hold a value of type long or double (JVMS section 2.6.1).  Primitive
     * locals are represented in the returned array as {@code PrimitiveSlot}s,
     * with longs and doubles occupying a pair of consecutive
     * {@code PrimitiveSlot}s.
     *
     * <p>The current VM implementation does not provide specific type
     * information for primitive locals.  This method simply returns the raw
     * contents of the VM's primitive locals on a best-effort basis, without
     * indicating a specific type.
     *
     * <p>The returned array may contain null entries for local variables that
     * are not live.
     *
     * @implNote
     * <p> The specific subclass of {@code PrimitiveSlot} will reflect the
     * underlying architecture, and will be either {@code PrimitiveSlot32} or
     * {@code PrimitiveSlot64}.
     *
     * <p>How a long or double value is stored in the pair of
     * {@code PrimitiveSlot}s can vary based on the underlying architecture and
     * VM implementation.  On 32-bit architectures, long/double values are split
     * between the two {@code PrimitiveSlot32}s.
     * On 64-bit architectures, the entire value may be stored in one of the
     * {@code PrimitiveSlot64}s, with the other {@code PrimitiveSlot64} being
     * unused.
     *
     * <p>The contents of the unused, high-order portion of a
     * {@code PrimitiveSlot64} (when storing a primitive other than a long or
     * double) is unspecified.  In particular, the unused bits are not
     * necessarily zeroed out.
     *
     * @return  the local variable array of this stack frame.
     */
    public Object[] getLocals();

    /**
     * Gets the operand stack of this stack frame.
     *
     * <p>
     * The 0-th element of the returned array represents the top of the operand stack.
     * This method returns an empty array if the operand stack is empty.
     *
     * <p>Each entry on the operand stack can hold a value of any Java Virtual
     * Machine Type.
     * For a value of primitive type, the element in the returned array is
     * a {@link PrimitiveSlot} object; otherwise, the element is the {@code Object}
     * on the operand stack.
     *
     * @return the operand stack of this stack frame.
     */
    public Object[] getStack();

    /**
     * <em>UNSUPPORTED</em> This interface is intended to be package-private
     * or moved to an internal package.<p>
     *
     * Represents a local variable or an entry on the operand stack whose value is
     * of primitive type.
     */
    public abstract class PrimitiveSlot {
        /**
         * Returns the size, in bytes, of the slot.
         */
        public abstract int size();

        /**
         * Returns the int value if this primitive value is of size 4
         * @return the int value if this primitive value is of size 4
         *
         * @throws UnsupportedOperationException if this primitive value is not
         * of size 4.
         */
        public int intValue() {
            throw new UnsupportedOperationException("this " + size() + "-byte primitive");
        }

        /**
         * Returns the long value if this primitive value is of size 8
         * @return the long value if this primitive value is of size 8
         *
         * @throws UnsupportedOperationException if this primitive value is not
         * of size 8.
         */
        public long longValue() {
            throw new UnsupportedOperationException("this " + size() + "-byte primitive");
        }
    }


    /**
     * Gets {@code StackWalker} that can get locals and operands.
     *
     * @throws SecurityException if the security manager is present and
     * denies access to {@code RuntimePermission("liveStackFrames")}
     */
    public static StackWalker getStackWalker() {
        return getStackWalker(EnumSet.noneOf(StackWalker.Option.class));
    }

    /**
     * Gets a {@code StackWalker} instance with the given options specifying
     * the stack frame information it can access, and which will traverse at most
     * the given {@code maxDepth} number of stack frames.  If no option is
     * specified, this {@code StackWalker} obtains the method name and
     * the class name with all
     * {@linkplain StackWalker.Option#SHOW_HIDDEN_FRAMES hidden frames} skipped.
     * The returned {@code StackWalker} can get locals and operands.
     *
     * @param options stack walk {@link StackWalker.Option options}
     *
     * @throws SecurityException if the security manager is present and
     * it denies access to {@code RuntimePermission("liveStackFrames")}; or
     * or if the given {@code options} contains
     * {@link StackWalker.Option#RETAIN_CLASS_REFERENCE Option.RETAIN_CLASS_REFERENCE}
     * and it denies access to {@code RuntimePermission("getStackWalkerWithClassReference")}.
     */
    public static StackWalker getStackWalker(Set<StackWalker.Option> options) {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new RuntimePermission("liveStackFrames"));
        }
        return StackWalker.newInstance(options, LOCALS_AND_OPERANDS);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\LiveStackFrameInfo.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang;

final class LiveStackFrameInfo extends StackFrameInfo implements LiveStackFrame {
    private static Object[] EMPTY_ARRAY = new Object[0];

    // These flags must match the values maintained in the VM
    private static final int MODE_INTERPRETED = 0x01;
    private static final int MODE_COMPILED    = 0x02;

    LiveStackFrameInfo(StackWalker walker) {
        super(walker);
    }

    // These fields are initialized by the VM if ExtendedOption.LOCALS_AND_OPERANDS is set
    private Object[] monitors = EMPTY_ARRAY;
    private Object[] locals = EMPTY_ARRAY;
    private Object[] operands = EMPTY_ARRAY;
    private int mode = 0;

    @Override
    public Object[] getMonitors() {
        return monitors;
    }

    @Override
    public Object[] getLocals() {
        return locals;
    }

    @Override
    public Object[] getStack() {
        return operands;
    }

    @Override
    public String toString() {
        StringBuilder retVal = new StringBuilder(super.toString());
        if (mode != 0) {
            retVal.append("(");
            if ((mode & MODE_INTERPRETED) == MODE_INTERPRETED) {
                retVal.append(" interpreted ");
            }
            if ((mode & MODE_COMPILED) == MODE_COMPILED) {
                retVal.append(" compiled ");
            }
            retVal.append(")");
        }
        return retVal.toString();
    }

    /*
     * Convert primitive value to {@code PrimitiveSlot} object to represent
     * a local variable or an element on the operand stack of primitive type.
     */

    static PrimitiveSlot asPrimitive(int value) {
        return new PrimitiveSlot32(value);
    }

    static PrimitiveSlot asPrimitive(long value) {
        return new PrimitiveSlot64(value);
    }

    private static class PrimitiveSlot32 extends PrimitiveSlot {
        final int value;
        PrimitiveSlot32(int value) {
            this.value = value;
        }

        @Override
        public int size() {
            return 4;
        }

        @Override
        public int intValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }

    private static class PrimitiveSlot64 extends PrimitiveSlot {
        final long value;
        PrimitiveSlot64(long value) {
            this.value = value;
        }

        @Override
        public int size() {
            return 8;
        }

        @Override
        public long longValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\Long.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1994, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.lang.annotation.Native;
import java.math.*;
import java.util.Objects;
import jdk.internal.HotSpotIntrinsicCandidate;

import static java.lang.String.COMPACT_STRINGS;
import static java.lang.String.LATIN1;
import static java.lang.String.UTF16;

/**
 * The {@code Long} class wraps a value of the primitive type {@code
 * long} in an object. An object of type {@code Long} contains a
 * single field whose type is {@code long}.
 *
 * <p> In addition, this class provides several methods for converting
 * a {@code long} to a {@code String} and a {@code String} to a {@code
 * long}, as well as other constants and methods useful when dealing
 * with a {@code long}.
 *
 * <p>Implementation note: The implementations of the "bit twiddling"
 * methods (such as {@link #highestOneBit(long) highestOneBit} and
 * {@link #numberOfTrailingZeros(long) numberOfTrailingZeros}) are
 * based on material from Henry S. Warren, Jr.'s <i>Hacker's
 * Delight</i>, (Addison Wesley, 2002).
 *
 * @author  Lee Boynton
 * @author  Arthur van Hoff
 * @author  Josh Bloch
 * @author  Joseph D. Darcy
 * @since   1.0
 */
public final class Long extends Number implements Comparable<Long> {
    /**
     * A constant holding the minimum value a {@code long} can
     * have, -2<sup>63</sup>.
     */
    @Native public static final long MIN_VALUE = 0x8000000000000000L;

    /**
     * A constant holding the maximum value a {@code long} can
     * have, 2<sup>63</sup>-1.
     */
    @Native public static final long MAX_VALUE = 0x7fffffffffffffffL;

    /**
     * The {@code Class} instance representing the primitive type
     * {@code long}.
     *
     * @since   1.1
     */
    @SuppressWarnings("unchecked")
    public static final Class<Long>     TYPE = (Class<Long>) Class.getPrimitiveClass("long");

    /**
     * Returns a string representation of the first argument in the
     * radix specified by the second argument.
     *
     * <p>If the radix is smaller than {@code Character.MIN_RADIX}
     * or larger than {@code Character.MAX_RADIX}, then the radix
     * {@code 10} is used instead.
     *
     * <p>If the first argument is negative, the first element of the
     * result is the ASCII minus sign {@code '-'}
     * ({@code '\u005Cu002d'}). If the first argument is not
     * negative, no sign character appears in the result.
     *
     * <p>The remaining characters of the result represent the magnitude
     * of the first argument. If the magnitude is zero, it is
     * represented by a single zero character {@code '0'}
     * ({@code '\u005Cu0030'}); otherwise, the first character of
     * the representation of the magnitude will not be the zero
     * character.  The following ASCII characters are used as digits:
     *
     * <blockquote>
     *   {@code 0123456789abcdefghijklmnopqrstuvwxyz}
     * </blockquote>
     *
     * These are {@code '\u005Cu0030'} through
     * {@code '\u005Cu0039'} and {@code '\u005Cu0061'} through
     * {@code '\u005Cu007a'}. If {@code radix} is
     * <var>N</var>, then the first <var>N</var> of these characters
     * are used as radix-<var>N</var> digits in the order shown. Thus,
     * the digits for hexadecimal (radix 16) are
     * {@code 0123456789abcdef}. If uppercase letters are
     * desired, the {@link java.lang.String#toUpperCase()} method may
     * be called on the result:
     *
     * <blockquote>
     *  {@code Long.toString(n, 16).toUpperCase()}
     * </blockquote>
     *
     * @param   i       a {@code long} to be converted to a string.
     * @param   radix   the radix to use in the string representation.
     * @return  a string representation of the argument in the specified radix.
     * @see     java.lang.Character#MAX_RADIX
     * @see     java.lang.Character#MIN_RADIX
     */
    public static String toString(long i, int radix) {
        if (radix < Character.MIN_RADIX || radix > Character.MAX_RADIX)
            radix = 10;
        if (radix == 10)
            return toString(i);

        if (COMPACT_STRINGS) {
            byte[] buf = new byte[65];
            int charPos = 64;
            boolean negative = (i < 0);

            if (!negative) {
                i = -i;
            }

            while (i <= -radix) {
                buf[charPos--] = (byte)Integer.digits[(int)(-(i % radix))];
                i = i / radix;
            }
            buf[charPos] = (byte)Integer.digits[(int)(-i)];

            if (negative) {
                buf[--charPos] = '-';
            }
            return StringLatin1.newString(buf, charPos, (65 - charPos));
        }
        return toStringUTF16(i, radix);
    }

    private static String toStringUTF16(long i, int radix) {
        byte[] buf = new byte[65 * 2];
        int charPos = 64;
        boolean negative = (i < 0);
        if (!negative) {
            i = -i;
        }
        while (i <= -radix) {
            StringUTF16.putChar(buf, charPos--, Integer.digits[(int)(-(i % radix))]);
            i = i / radix;
        }
        StringUTF16.putChar(buf, charPos, Integer.digits[(int)(-i)]);
        if (negative) {
            StringUTF16.putChar(buf, --charPos, '-');
        }
        return StringUTF16.newString(buf, charPos, (65 - charPos));
    }

    /**
     * Returns a string representation of the first argument as an
     * unsigned integer value in the radix specified by the second
     * argument.
     *
     * <p>If the radix is smaller than {@code Character.MIN_RADIX}
     * or larger than {@code Character.MAX_RADIX}, then the radix
     * {@code 10} is used instead.
     *
     * <p>Note that since the first argument is treated as an unsigned
     * value, no leading sign character is printed.
     *
     * <p>If the magnitude is zero, it is represented by a single zero
     * character {@code '0'} ({@code '\u005Cu0030'}); otherwise,
     * the first character of the representation of the magnitude will
     * not be the zero character.
     *
     * <p>The behavior of radixes and the characters used as digits
     * are the same as {@link #toString(long, int) toString}.
     *
     * @param   i       an integer to be converted to an unsigned string.
     * @param   radix   the radix to use in the string representation.
     * @return  an unsigned string representation of the argument in the specified radix.
     * @see     #toString(long, int)
     * @since 1.8
     */
    public static String toUnsignedString(long i, int radix) {
        if (i >= 0)
            return toString(i, radix);
        else {
            switch (radix) {
            case 2:
                return toBinaryString(i);

            case 4:
                return toUnsignedString0(i, 2);

            case 8:
                return toOctalString(i);

            case 10:
                /*
                 * We can get the effect of an unsigned division by 10
                 * on a long value by first shifting right, yielding a
                 * positive value, and then dividing by 5.  This
                 * allows the last digit and preceding digits to be
                 * isolated more quickly than by an initial conversion
                 * to BigInteger.
                 */
                long quot = (i >>> 1) / 5;
                long rem = i - quot * 10;
                return toString(quot) + rem;

            case 16:
                return toHexString(i);

            case 32:
                return toUnsignedString0(i, 5);

            default:
                return toUnsignedBigInteger(i).toString(radix);
            }
        }
    }

    /**
     * Return a BigInteger equal to the unsigned value of the
     * argument.
     */
    private static BigInteger toUnsignedBigInteger(long i) {
        if (i >= 0L)
            return BigInteger.valueOf(i);
        else {
            int upper = (int) (i >>> 32);
            int lower = (int) i;

            // return (upper << 32) + lower
            return (BigInteger.valueOf(Integer.toUnsignedLong(upper))).shiftLeft(32).
                add(BigInteger.valueOf(Integer.toUnsignedLong(lower)));
        }
    }

    /**
     * Returns a string representation of the {@code long}
     * argument as an unsigned integer in base&nbsp;16.
     *
     * <p>The unsigned {@code long} value is the argument plus
     * 2<sup>64</sup> if the argument is negative; otherwise, it is
     * equal to the argument.  This value is converted to a string of
     * ASCII digits in hexadecimal (base&nbsp;16) with no extra
     * leading {@code 0}s.
     *
     * <p>The value of the argument can be recovered from the returned
     * string {@code s} by calling {@link
     * Long#parseUnsignedLong(String, int) Long.parseUnsignedLong(s,
     * 16)}.
     *
     * <p>If the unsigned magnitude is zero, it is represented by a
     * single zero character {@code '0'} ({@code '\u005Cu0030'});
     * otherwise, the first character of the representation of the
     * unsigned magnitude will not be the zero character. The
     * following characters are used as hexadecimal digits:
     *
     * <blockquote>
     *  {@code 0123456789abcdef}
     * </blockquote>
     *
     * These are the characters {@code '\u005Cu0030'} through
     * {@code '\u005Cu0039'} and  {@code '\u005Cu0061'} through
     * {@code '\u005Cu0066'}.  If uppercase letters are desired,
     * the {@link java.lang.String#toUpperCase()} method may be called
     * on the result:
     *
     * <blockquote>
     *  {@code Long.toHexString(n).toUpperCase()}
     * </blockquote>
     *
     * @param   i   a {@code long} to be converted to a string.
     * @return  the string representation of the unsigned {@code long}
     *          value represented by the argument in hexadecimal
     *          (base&nbsp;16).
     * @see #parseUnsignedLong(String, int)
     * @see #toUnsignedString(long, int)
     * @since   1.0.2
     */
    public static String toHexString(long i) {
        return toUnsignedString0(i, 4);
    }

    /**
     * Returns a string representation of the {@code long}
     * argument as an unsigned integer in base&nbsp;8.
     *
     * <p>The unsigned {@code long} value is the argument plus
     * 2<sup>64</sup> if the argument is negative; otherwise, it is
     * equal to the argument.  This value is converted to a string of
     * ASCII digits in octal (base&nbsp;8) with no extra leading
     * {@code 0}s.
     *
     * <p>The value of the argument can be recovered from the returned
     * string {@code s} by calling {@link
     * Long#parseUnsignedLong(String, int) Long.parseUnsignedLong(s,
     * 8)}.
     *
     * <p>If the unsigned magnitude is zero, it is represented by a
     * single zero character {@code '0'} ({@code '\u005Cu0030'});
     * otherwise, the first character of the representation of the
     * unsigned magnitude will not be the zero character. The
     * following characters are used as octal digits:
     *
     * <blockquote>
     *  {@code 01234567}
     * </blockquote>
     *
     * These are the characters {@code '\u005Cu0030'} through
     * {@code '\u005Cu0037'}.
     *
     * @param   i   a {@code long} to be converted to a string.
     * @return  the string representation of the unsigned {@code long}
     *          value represented by the argument in octal (base&nbsp;8).
     * @see #parseUnsignedLong(String, int)
     * @see #toUnsignedString(long, int)
     * @since   1.0.2
     */
    public static String toOctalString(long i) {
        return toUnsignedString0(i, 3);
    }

    /**
     * Returns a string representation of the {@code long}
     * argument as an unsigned integer in base&nbsp;2.
     *
     * <p>The unsigned {@code long} value is the argument plus
     * 2<sup>64</sup> if the argument is negative; otherwise, it is
     * equal to the argument.  This value is converted to a string of
     * ASCII digits in binary (base&nbsp;2) with no extra leading
     * {@code 0}s.
     *
     * <p>The value of the argument can be recovered from the returned
     * string {@code s} by calling {@link
     * Long#parseUnsignedLong(String, int) Long.parseUnsignedLong(s,
     * 2)}.
     *
     * <p>If the unsigned magnitude is zero, it is represented by a
     * single zero character {@code '0'} ({@code '\u005Cu0030'});
     * otherwise, the first character of the representation of the
     * unsigned magnitude will not be the zero character. The
     * characters {@code '0'} ({@code '\u005Cu0030'}) and {@code
     * '1'} ({@code '\u005Cu0031'}) are used as binary digits.
     *
     * @param   i   a {@code long} to be converted to a string.
     * @return  the string representation of the unsigned {@code long}
     *          value represented by the argument in binary (base&nbsp;2).
     * @see #parseUnsignedLong(String, int)
     * @see #toUnsignedString(long, int)
     * @since   1.0.2
     */
    public static String toBinaryString(long i) {
        return toUnsignedString0(i, 1);
    }

    /**
     * Format a long (treated as unsigned) into a String.
     * @param val the value to format
     * @param shift the log2 of the base to format in (4 for hex, 3 for octal, 1 for binary)
     */
    static String toUnsignedString0(long val, int shift) {
        // assert shift > 0 && shift <=5 : "Illegal shift value";
        int mag = Long.SIZE - Long.numberOfLeadingZeros(val);
        int chars = Math.max(((mag + (shift - 1)) / shift), 1);
        if (COMPACT_STRINGS) {
            byte[] buf = new byte[chars];
            formatUnsignedLong0(val, shift, buf, 0, chars);
            return new String(buf, LATIN1);
        } else {
            byte[] buf = new byte[chars * 2];
            formatUnsignedLong0UTF16(val, shift, buf, 0, chars);
            return new String(buf, UTF16);
        }
    }

    /**
     * Format a long (treated as unsigned) into a character buffer. If
     * {@code len} exceeds the formatted ASCII representation of {@code val},
     * {@code buf} will be padded with leading zeroes.
     *
     * @param val the unsigned long to format
     * @param shift the log2 of the base to format in (4 for hex, 3 for octal, 1 for binary)
     * @param buf the character buffer to write to
     * @param offset the offset in the destination buffer to start at
     * @param len the number of characters to write
     */

    /** byte[]/LATIN1 version    */
    static void formatUnsignedLong0(long val, int shift, byte[] buf, int offset, int len) {
        int charPos = offset + len;
        int radix = 1 << shift;
        int mask = radix - 1;
        do {
            buf[--charPos] = (byte)Integer.digits[((int) val) & mask];
            val >>>= shift;
        } while (charPos > offset);
    }

    /** byte[]/UTF16 version    */
    private static void formatUnsignedLong0UTF16(long val, int shift, byte[] buf, int offset, int len) {
        int charPos = offset + len;
        int radix = 1 << shift;
        int mask = radix - 1;
        do {
            StringUTF16.putChar(buf, --charPos, Integer.digits[((int) val) & mask]);
            val >>>= shift;
        } while (charPos > offset);
    }

    static String fastUUID(long lsb, long msb) {
        if (COMPACT_STRINGS) {
            byte[] buf = new byte[36];
            formatUnsignedLong0(lsb,        4, buf, 24, 12);
            formatUnsignedLong0(lsb >>> 48, 4, buf, 19, 4);
            formatUnsignedLong0(msb,        4, buf, 14, 4);
            formatUnsignedLong0(msb >>> 16, 4, buf, 9,  4);
            formatUnsignedLong0(msb >>> 32, 4, buf, 0,  8);

            buf[23] = '-';
            buf[18] = '-';
            buf[13] = '-';
            buf[8]  = '-';

            return new String(buf, LATIN1);
        } else {
            byte[] buf = new byte[72];

            formatUnsignedLong0UTF16(lsb,        4, buf, 24, 12);
            formatUnsignedLong0UTF16(lsb >>> 48, 4, buf, 19, 4);
            formatUnsignedLong0UTF16(msb,        4, buf, 14, 4);
            formatUnsignedLong0UTF16(msb >>> 16, 4, buf, 9,  4);
            formatUnsignedLong0UTF16(msb >>> 32, 4, buf, 0,  8);

            StringUTF16.putChar(buf, 23, '-');
            StringUTF16.putChar(buf, 18, '-');
            StringUTF16.putChar(buf, 13, '-');
            StringUTF16.putChar(buf,  8, '-');

            return new String(buf, UTF16);
        }
    }

    /**
     * Returns a {@code String} object representing the specified
     * {@code long}.  The argument is converted to signed decimal
     * representation and returned as a string, exactly as if the
     * argument and the radix 10 were given as arguments to the {@link
     * #toString(long, int)} method.
     *
     * @param   i   a {@code long} to be converted.
     * @return  a string representation of the argument in base&nbsp;10.
     */
    public static String toString(long i) {
        int size = stringSize(i);
        if (COMPACT_STRINGS) {
            byte[] buf = new byte[size];
            getChars(i, size, buf);
            return new String(buf, LATIN1);
        } else {
            byte[] buf = new byte[size * 2];
            StringUTF16.getChars(i, size, buf);
            return new String(buf, UTF16);
        }
    }

    /**
     * Returns a string representation of the argument as an unsigned
     * decimal value.
     *
     * The argument is converted to unsigned decimal representation
     * and returned as a string exactly as if the argument and radix
     * 10 were given as arguments to the {@link #toUnsignedString(long,
     * int)} method.
     *
     * @param   i  an integer to be converted to an unsigned string.
     * @return  an unsigned string representation of the argument.
     * @see     #toUnsignedString(long, int)
     * @since 1.8
     */
    public static String toUnsignedString(long i) {
        return toUnsignedString(i, 10);
    }

    /**
     * Places characters representing the long i into the
     * character array buf. The characters are placed into
     * the buffer backwards starting with the least significant
     * digit at the specified index (exclusive), and working
     * backwards from there.
     *
     * @implNote This method converts positive inputs into negative
     * values, to cover the Long.MIN_VALUE case. Converting otherwise
     * (negative to positive) will expose -Long.MIN_VALUE that overflows
     * long.
     *
     * @param i     value to convert
     * @param index next index, after the least significant digit
     * @param buf   target buffer, Latin1-encoded
     * @return index of the most significant digit or minus sign, if present
     */
    static int getChars(long i, int index, byte[] buf) {
        long q;
        int r;
        int charPos = index;

        boolean negative = (i < 0);
        if (!negative) {
            i = -i;
        }

        // Get 2 digits/iteration using longs until quotient fits into an int
        while (i <= Integer.MIN_VALUE) {
            q = i / 100;
            r = (int)((q * 100) - i);
            i = q;
            buf[--charPos] = Integer.DigitOnes[r];
            buf[--charPos] = Integer.DigitTens[r];
        }

        // Get 2 digits/iteration using ints
        int q2;
        int i2 = (int)i;
        while (i2 <= -100) {
            q2 = i2 / 100;
            r  = (q2 * 100) - i2;
            i2 = q2;
            buf[--charPos] = Integer.DigitOnes[r];
            buf[--charPos] = Integer.DigitTens[r];
        }

        // We know there are at most two digits left at this point.
        q2 = i2 / 10;
        r  = (q2 * 10) - i2;
        buf[--charPos] = (byte)('0' + r);

        // Whatever left is the remaining digit.
        if (q2 < 0) {
            buf[--charPos] = (byte)('0' - q2);
        }

        if (negative) {
            buf[--charPos] = (byte)'-';
        }
        return charPos;
    }

    /**
     * Returns the string representation size for a given long value.
     *
     * @param x long value
     * @return string size
     *
     * @implNote There are other ways to compute this: e.g. binary search,
     * but values are biased heavily towards zero, and therefore linear search
     * wins. The iteration results are also routinely inlined in the generated
     * code after loop unrolling.
     */
    static int stringSize(long x) {
        int d = 1;
        if (x >= 0) {
            d = 0;
            x = -x;
        }
        long p = -10;
        for (int i = 1; i < 19; i++) {
            if (x > p)
                return i + d;
            p = 10 * p;
        }
        return 19 + d;
    }

    /**
     * Parses the string argument as a signed {@code long} in the
     * radix specified by the second argument. The characters in the
     * string must all be digits of the specified radix (as determined
     * by whether {@link java.lang.Character#digit(char, int)} returns
     * a nonnegative value), except that the first character may be an
     * ASCII minus sign {@code '-'} ({@code '\u005Cu002D'}) to
     * indicate a negative value or an ASCII plus sign {@code '+'}
     * ({@code '\u005Cu002B'}) to indicate a positive value. The
     * resulting {@code long} value is returned.
     *
     * <p>Note that neither the character {@code L}
     * ({@code '\u005Cu004C'}) nor {@code l}
     * ({@code '\u005Cu006C'}) is permitted to appear at the end
     * of the string as a type indicator, as would be permitted in
     * Java programming language source code - except that either
     * {@code L} or {@code l} may appear as a digit for a
     * radix greater than or equal to 22.
     *
     * <p>An exception of type {@code NumberFormatException} is
     * thrown if any of the following situations occurs:
     * <ul>
     *
     * <li>The first argument is {@code null} or is a string of
     * length zero.
     *
     * <li>The {@code radix} is either smaller than {@link
     * java.lang.Character#MIN_RADIX} or larger than {@link
     * java.lang.Character#MAX_RADIX}.
     *
     * <li>Any character of the string is not a digit of the specified
     * radix, except that the first character may be a minus sign
     * {@code '-'} ({@code '\u005Cu002d'}) or plus sign {@code
     * '+'} ({@code '\u005Cu002B'}) provided that the string is
     * longer than length 1.
     *
     * <li>The value represented by the string is not a value of type
     *      {@code long}.
     * </ul>
     *
     * <p>Examples:
     * <blockquote><pre>
     * parseLong("0", 10) returns 0L
     * parseLong("473", 10) returns 473L
     * parseLong("+42", 10) returns 42L
     * parseLong("-0", 10) returns 0L
     * parseLong("-FF", 16) returns -255L
     * parseLong("1100110", 2) returns 102L
     * parseLong("99", 8) throws a NumberFormatException
     * parseLong("Hazelnut", 10) throws a NumberFormatException
     * parseLong("Hazelnut", 36) returns 1356099454469L
     * </pre></blockquote>
     *
     * @param      s       the {@code String} containing the
     *                     {@code long} representation to be parsed.
     * @param      radix   the radix to be used while parsing {@code s}.
     * @return     the {@code long} represented by the string argument in
     *             the specified radix.
     * @throws     NumberFormatException  if the string does not contain a
     *             parsable {@code long}.
     */
    public static long parseLong(String s, int radix)
              throws NumberFormatException
    {
        if (s == null) {
            throw new NumberFormatException("null");
        }

        if (radix < Character.MIN_RADIX) {
            throw new NumberFormatException("radix " + radix +
                                            " less than Character.MIN_RADIX");
        }
        if (radix > Character.MAX_RADIX) {
            throw new NumberFormatException("radix " + radix +
                                            " greater than Character.MAX_RADIX");
        }

        boolean negative = false;
        int i = 0, len = s.length();
        long limit = -Long.MAX_VALUE;

        if (len > 0) {
            char firstChar = s.charAt(0);
            if (firstChar < '0') { // Possible leading "+" or "-"
                if (firstChar == '-') {
                    negative = true;
                    limit = Long.MIN_VALUE;
                } else if (firstChar != '+') {
                    throw NumberFormatException.forInputString(s);
                }

                if (len == 1) { // Cannot have lone "+" or "-"
                    throw NumberFormatException.forInputString(s);
                }
                i++;
            }
            long multmin = limit / radix;
            long result = 0;
            while (i < len) {
                // Accumulating negatively avoids surprises near MAX_VALUE
                int digit = Character.digit(s.charAt(i++),radix);
                if (digit < 0 || result < multmin) {
                    throw NumberFormatException.forInputString(s);
                }
                result *= radix;
                if (result < limit + digit) {
                    throw NumberFormatException.forInputString(s);
                }
                result -= digit;
            }
            return negative ? result : -result;
        } else {
            throw NumberFormatException.forInputString(s);
        }
    }

    /**
     * Parses the {@link CharSequence} argument as a signed {@code long} in
     * the specified {@code radix}, beginning at the specified
     * {@code beginIndex} and extending to {@code endIndex - 1}.
     *
     * <p>The method does not take steps to guard against the
     * {@code CharSequence} being mutated while parsing.
     *
     * @param      s   the {@code CharSequence} containing the {@code long}
     *                  representation to be parsed
     * @param      beginIndex   the beginning index, inclusive.
     * @param      endIndex     the ending index, exclusive.
     * @param      radix   the radix to be used while parsing {@code s}.
     * @return     the signed {@code long} represented by the subsequence in
     *             the specified radix.
     * @throws     NullPointerException  if {@code s} is null.
     * @throws     IndexOutOfBoundsException  if {@code beginIndex} is
     *             negative, or if {@code beginIndex} is greater than
     *             {@code endIndex} or if {@code endIndex} is greater than
     *             {@code s.length()}.
     * @throws     NumberFormatException  if the {@code CharSequence} does not
     *             contain a parsable {@code int} in the specified
     *             {@code radix}, or if {@code radix} is either smaller than
     *             {@link java.lang.Character#MIN_RADIX} or larger than
     *             {@link java.lang.Character#MAX_RADIX}.
     * @since  9
     */
    public static long parseLong(CharSequence s, int beginIndex, int endIndex, int radix)
                throws NumberFormatException {
        s = Objects.requireNonNull(s);

        if (beginIndex < 0 || beginIndex > endIndex || endIndex > s.length()) {
            throw new IndexOutOfBoundsException();
        }
        if (radix < Character.MIN_RADIX) {
            throw new NumberFormatException("radix " + radix +
                    " less than Character.MIN_RADIX");
        }
        if (radix > Character.MAX_RADIX) {
            throw new NumberFormatException("radix " + radix +
                    " greater than Character.MAX_RADIX");
        }

        boolean negative = false;
        int i = beginIndex;
        long limit = -Long.MAX_VALUE;

        if (i < endIndex) {
            char firstChar = s.charAt(i);
            if (firstChar < '0') { // Possible leading "+" or "-"
                if (firstChar == '-') {
                    negative = true;
                    limit = Long.MIN_VALUE;
                } else if (firstChar != '+') {
                    throw NumberFormatException.forCharSequence(s, beginIndex,
                            endIndex, i);
                }
                i++;
            }
            if (i >= endIndex) { // Cannot have lone "+", "-" or ""
                throw NumberFormatException.forCharSequence(s, beginIndex,
                        endIndex, i);
            }
            long multmin = limit / radix;
            long result = 0;
            while (i < endIndex) {
                // Accumulating negatively avoids surprises near MAX_VALUE
                int digit = Character.digit(s.charAt(i), radix);
                if (digit < 0 || result < multmin) {
                    throw NumberFormatException.forCharSequence(s, beginIndex,
                            endIndex, i);
                }
                result *= radix;
                if (result < limit + digit) {
                    throw NumberFormatException.forCharSequence(s, beginIndex,
                            endIndex, i);
                }
                i++;
                result -= digit;
            }
            return negative ? result : -result;
        } else {
            throw new NumberFormatException("");
        }
    }

    /**
     * Parses the string argument as a signed decimal {@code long}.
     * The characters in the string must all be decimal digits, except
     * that the first character may be an ASCII minus sign {@code '-'}
     * ({@code \u005Cu002D'}) to indicate a negative value or an
     * ASCII plus sign {@code '+'} ({@code '\u005Cu002B'}) to
     * indicate a positive value. The resulting {@code long} value is
     * returned, exactly as if the argument and the radix {@code 10}
     * were given as arguments to the {@link
     * #parseLong(java.lang.String, int)} method.
     *
     * <p>Note that neither the character {@code L}
     * ({@code '\u005Cu004C'}) nor {@code l}
     * ({@code '\u005Cu006C'}) is permitted to appear at the end
     * of the string as a type indicator, as would be permitted in
     * Java programming language source code.
     *
     * @param      s   a {@code String} containing the {@code long}
     *             representation to be parsed
     * @return     the {@code long} represented by the argument in
     *             decimal.
     * @throws     NumberFormatException  if the string does not contain a
     *             parsable {@code long}.
     */
    public static long parseLong(String s) throws NumberFormatException {
        return parseLong(s, 10);
    }

    /**
     * Parses the string argument as an unsigned {@code long} in the
     * radix specified by the second argument.  An unsigned integer
     * maps the values usually associated with negative numbers to
     * positive numbers larger than {@code MAX_VALUE}.
     *
     * The characters in the string must all be digits of the
     * specified radix (as determined by whether {@link
     * java.lang.Character#digit(char, int)} returns a nonnegative
     * value), except that the first character may be an ASCII plus
     * sign {@code '+'} ({@code '\u005Cu002B'}). The resulting
     * integer value is returned.
     *
     * <p>An exception of type {@code NumberFormatException} is
     * thrown if any of the following situations occurs:
     * <ul>
     * <li>The first argument is {@code null} or is a string of
     * length zero.
     *
     * <li>The radix is either smaller than
     * {@link java.lang.Character#MIN_RADIX} or
     * larger than {@link java.lang.Character#MAX_RADIX}.
     *
     * <li>Any character of the string is not a digit of the specified
     * radix, except that the first character may be a plus sign
     * {@code '+'} ({@code '\u005Cu002B'}) provided that the
     * string is longer than length 1.
     *
     * <li>The value represented by the string is larger than the
     * largest unsigned {@code long}, 2<sup>64</sup>-1.
     *
     * </ul>
     *
     *
     * @param      s   the {@code String} containing the unsigned integer
     *                  representation to be parsed
     * @param      radix   the radix to be used while parsing {@code s}.
     * @return     the unsigned {@code long} represented by the string
     *             argument in the specified radix.
     * @throws     NumberFormatException if the {@code String}
     *             does not contain a parsable {@code long}.
     * @since 1.8
     */
    public static long parseUnsignedLong(String s, int radix)
                throws NumberFormatException {
        if (s == null)  {
            throw new NumberFormatException("null");
        }

        int len = s.length();
        if (len > 0) {
            char firstChar = s.charAt(0);
            if (firstChar == '-') {
                throw new
                    NumberFormatException(String.format("Illegal leading minus sign " +
                                                       "on unsigned string %s.", s));
            } else {
                if (len <= 12 || // Long.MAX_VALUE in Character.MAX_RADIX is 13 digits
                    (radix == 10 && len <= 18) ) { // Long.MAX_VALUE in base 10 is 19 digits
                    return parseLong(s, radix);
                }

                // No need for range checks on len due to testing above.
                long first = parseLong(s, 0, len - 1, radix);
                int second = Character.digit(s.charAt(len - 1), radix);
                if (second < 0) {
                    throw new NumberFormatException("Bad digit at end of " + s);
                }
                long result = first * radix + second;

                /*
                 * Test leftmost bits of multiprecision extension of first*radix
                 * for overflow. The number of bits needed is defined by
                 * GUARD_BIT = ceil(log2(Character.MAX_RADIX)) + 1 = 7. Then
                 * int guard = radix*(int)(first >>> (64 - GUARD_BIT)) and
                 * overflow is tested by splitting guard in the ranges
                 * guard < 92, 92 <= guard < 128, and 128 <= guard, where
                 * 92 = 128 - Character.MAX_RADIX. Note that guard cannot take
                 * on a value which does not include a prime factor in the legal
                 * radix range.
                 */
                int guard = radix * (int) (first >>> 57);
                if (guard >= 128 ||
                    (result >= 0 && guard >= 128 - Character.MAX_RADIX)) {
                    /*
                     * For purposes of exposition, the programmatic statements
                     * below should be taken to be multi-precision, i.e., not
                     * subject to overflow.
                     *
                     * A) Condition guard >= 128:
                     * If guard >= 128 then first*radix >= 2^7 * 2^57 = 2^64
                     * hence always overflow.
                     *
                     * B) Condition guard < 92:
                     * Define left7 = first >>> 57.
                     * Given first = (left7 * 2^57) + (first & (2^57 - 1)) then
                     * result <= (radix*left7)*2^57 + radix*(2^57 - 1) + second.
                     * Thus if radix*left7 < 92, radix <= 36, and second < 36,
                     * then result < 92*2^57 + 36*(2^57 - 1) + 36 = 2^64 hence
                     * never overflow.
                     *
                     * C) Condition 92 <= guard < 128:
                     * first*radix + second >= radix*left7*2^57 + second
                     * so that first*radix + second >= 92*2^57 + 0 > 2^63
                     *
                     * D) Condition guard < 128:
                     * radix*first <= (radix*left7) * 2^57 + radix*(2^57 - 1)
                     * so
                     * radix*first + second <= (radix*left7) * 2^57 + radix*(2^57 - 1) + 36
                     * thus
                     * radix*first + second < 128 * 2^57 + 36*2^57 - radix + 36
                     * whence
                     * radix*first + second < 2^64 + 2^6*2^57 = 2^64 + 2^63
                     *
                     * E) Conditions C, D, and result >= 0:
                     * C and D combined imply the mathematical result
                     * 2^63 < first*radix + second < 2^64 + 2^63. The lower
                     * bound is therefore negative as a signed long, but the
                     * upper bound is too small to overflow again after the
                     * signed long overflows to positive above 2^64 - 1. Hence
                     * result >= 0 implies overflow given C and D.
                     */
                    throw new NumberFormatException(String.format("String value %s exceeds " +
                                                                  "range of unsigned long.", s));
                }
                return result;
            }
        } else {
            throw NumberFormatException.forInputString(s);
        }
    }

    /**
     * Parses the {@link CharSequence} argument as an unsigned {@code long} in
     * the specified {@code radix}, beginning at the specified
     * {@code beginIndex} and extending to {@code endIndex - 1}.
     *
     * <p>The method does not take steps to guard against the
     * {@code CharSequence} being mutated while parsing.
     *
     * @param      s   the {@code CharSequence} containing the unsigned
     *                 {@code long} representation to be parsed
     * @param      beginIndex   the beginning index, inclusive.
     * @param      endIndex     the ending index, exclusive.
     * @param      radix   the radix to be used while parsing {@code s}.
     * @return     the unsigned {@code long} represented by the subsequence in
     *             the specified radix.
     * @throws     NullPointerException  if {@code s} is null.
     * @throws     IndexOutOfBoundsException  if {@code beginIndex} is
     *             negative, or if {@code beginIndex} is greater than
     *             {@code endIndex} or if {@code endIndex} is greater than
     *             {@code s.length()}.
     * @throws     NumberFormatException  if the {@code CharSequence} does not
     *             contain a parsable unsigned {@code long} in the specified
     *             {@code radix}, or if {@code radix} is either smaller than
     *             {@link java.lang.Character#MIN_RADIX} or larger than
     *             {@link java.lang.Character#MAX_RADIX}.
     * @since  9
     */
    public static long parseUnsignedLong(CharSequence s, int beginIndex, int endIndex, int radix)
                throws NumberFormatException {
        s = Objects.requireNonNull(s);

        if (beginIndex < 0 || beginIndex > endIndex || endIndex > s.length()) {
            throw new IndexOutOfBoundsException();
        }
        int start = beginIndex, len = endIndex - beginIndex;

        if (len > 0) {
            char firstChar = s.charAt(start);
            if (firstChar == '-') {
                throw new NumberFormatException(String.format("Illegal leading minus sign " +
                        "on unsigned string %s.", s.subSequence(start, start + len)));
            } else {
                if (len <= 12 || // Long.MAX_VALUE in Character.MAX_RADIX is 13 digits
                    (radix == 10 && len <= 18) ) { // Long.MAX_VALUE in base 10 is 19 digits
                    return parseLong(s, start, start + len, radix);
                }

                // No need for range checks on end due to testing above.
                long first = parseLong(s, start, start + len - 1, radix);
                int second = Character.digit(s.charAt(start + len - 1), radix);
                if (second < 0) {
                    throw new NumberFormatException("Bad digit at end of " +
                            s.subSequence(start, start + len));
                }
                long result = first * radix + second;

                /*
                 * Test leftmost bits of multiprecision extension of first*radix
                 * for overflow. The number of bits needed is defined by
                 * GUARD_BIT = ceil(log2(Character.MAX_RADIX)) + 1 = 7. Then
                 * int guard = radix*(int)(first >>> (64 - GUARD_BIT)) and
                 * overflow is tested by splitting guard in the ranges
                 * guard < 92, 92 <= guard < 128, and 128 <= guard, where
                 * 92 = 128 - Character.MAX_RADIX. Note that guard cannot take
                 * on a value which does not include a prime factor in the legal
                 * radix range.
                 */
                int guard = radix * (int) (first >>> 57);
                if (guard >= 128 ||
                        (result >= 0 && guard >= 128 - Character.MAX_RADIX)) {
                    /*
                     * For purposes of exposition, the programmatic statements
                     * below should be taken to be multi-precision, i.e., not
                     * subject to overflow.
                     *
                     * A) Condition guard >= 128:
                     * If guard >= 128 then first*radix >= 2^7 * 2^57 = 2^64
                     * hence always overflow.
                     *
                     * B) Condition guard < 92:
                     * Define left7 = first >>> 57.
                     * Given first = (left7 * 2^57) + (first & (2^57 - 1)) then
                     * result <= (radix*left7)*2^57 + radix*(2^57 - 1) + second.
                     * Thus if radix*left7 < 92, radix <= 36, and second < 36,
                     * then result < 92*2^57 + 36*(2^57 - 1) + 36 = 2^64 hence
                     * never overflow.
                     *
                     * C) Condition 92 <= guard < 128:
                     * first*radix + second >= radix*left7*2^57 + second
                     * so that first*radix + second >= 92*2^57 + 0 > 2^63
                     *
                     * D) Condition guard < 128:
                     * radix*first <= (radix*left7) * 2^57 + radix*(2^57 - 1)
                     * so
                     * radix*first + second <= (radix*left7) * 2^57 + radix*(2^57 - 1) + 36
                     * thus
                     * radix*first + second < 128 * 2^57 + 36*2^57 - radix + 36
                     * whence
                     * radix*first + second < 2^64 + 2^6*2^57 = 2^64 + 2^63
                     *
                     * E) Conditions C, D, and result >= 0:
                     * C and D combined imply the mathematical result
                     * 2^63 < first*radix + second < 2^64 + 2^63. The lower
                     * bound is therefore negative as a signed long, but the
                     * upper bound is too small to overflow again after the
                     * signed long overflows to positive above 2^64 - 1. Hence
                     * result >= 0 implies overflow given C and D.
                     */
                    throw new NumberFormatException(String.format("String value %s exceeds " +
                            "range of unsigned long.", s.subSequence(start, start + len)));
                }
                return result;
            }
        } else {
            throw NumberFormatException.forInputString("");
        }
    }

    /**
     * Parses the string argument as an unsigned decimal {@code long}. The
     * characters in the string must all be decimal digits, except
     * that the first character may be an ASCII plus sign {@code
     * '+'} ({@code '\u005Cu002B'}). The resulting integer value
     * is returned, exactly as if the argument and the radix 10 were
     * given as arguments to the {@link
     * #parseUnsignedLong(java.lang.String, int)} method.
     *
     * @param s   a {@code String} containing the unsigned {@code long}
     *            representation to be parsed
     * @return    the unsigned {@code long} value represented by the decimal string argument
     * @throws    NumberFormatException  if the string does not contain a
     *            parsable unsigned integer.
     * @since 1.8
     */
    public static long parseUnsignedLong(String s) throws NumberFormatException {
        return parseUnsignedLong(s, 10);
    }

    /**
     * Returns a {@code Long} object holding the value
     * extracted from the specified {@code String} when parsed
     * with the radix given by the second argument.  The first
     * argument is interpreted as representing a signed
     * {@code long} in the radix specified by the second
     * argument, exactly as if the arguments were given to the {@link
     * #parseLong(java.lang.String, int)} method. The result is a
     * {@code Long} object that represents the {@code long}
     * value specified by the string.
     *
     * <p>In other words, this method returns a {@code Long} object equal
     * to the value of:
     *
     * <blockquote>
     *  {@code new Long(Long.parseLong(s, radix))}
     * </blockquote>
     *
     * @param      s       the string to be parsed
     * @param      radix   the radix to be used in interpreting {@code s}
     * @return     a {@code Long} object holding the value
     *             represented by the string argument in the specified
     *             radix.
     * @throws     NumberFormatException  If the {@code String} does not
     *             contain a parsable {@code long}.
     */
    public static Long valueOf(String s, int radix) throws NumberFormatException {
        return Long.valueOf(parseLong(s, radix));
    }

    /**
     * Returns a {@code Long} object holding the value
     * of the specified {@code String}. The argument is
     * interpreted as representing a signed decimal {@code long},
     * exactly as if the argument were given to the {@link
     * #parseLong(java.lang.String)} method. The result is a
     * {@code Long} object that represents the integer value
     * specified by the string.
     *
     * <p>In other words, this method returns a {@code Long} object
     * equal to the value of:
     *
     * <blockquote>
     *  {@code new Long(Long.parseLong(s))}
     * </blockquote>
     *
     * @param      s   the string to be parsed.
     * @return     a {@code Long} object holding the value
     *             represented by the string argument.
     * @throws     NumberFormatException  If the string cannot be parsed
     *             as a {@code long}.
     */
    public static Long valueOf(String s) throws NumberFormatException
    {
        return Long.valueOf(parseLong(s, 10));
    }

    private static class LongCache {
        private LongCache(){}

        static final Long cache[] = new Long[-(-128) + 127 + 1];

        static {
            for(int i = 0; i < cache.length; i++)
                cache[i] = new Long(i - 128);
        }
    }

    /**
     * Returns a {@code Long} instance representing the specified
     * {@code long} value.
     * If a new {@code Long} instance is not required, this method
     * should generally be used in preference to the constructor
     * {@link #Long(long)}, as this method is likely to yield
     * significantly better space and time performance by caching
     * frequently requested values.
     *
     * This method will always cache values in the range -128 to 127,
     * inclusive, and may cache other values outside of this range.
     *
     * @param  l a long value.
     * @return a {@code Long} instance representing {@code l}.
     * @since  1.5
     */
    @HotSpotIntrinsicCandidate
    public static Long valueOf(long l) {
        final int offset = 128;
        if (l >= -128 && l <= 127) { // will cache
            return LongCache.cache[(int)l + offset];
        }
        return new Long(l);
    }

    /**
     * Decodes a {@code String} into a {@code Long}.
     * Accepts decimal, hexadecimal, and octal numbers given by the
     * following grammar:
     *
     * <blockquote>
     * <dl>
     * <dt><i>DecodableString:</i>
     * <dd><i>Sign<sub>opt</sub> DecimalNumeral</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code 0x} <i>HexDigits</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code 0X} <i>HexDigits</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code #} <i>HexDigits</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code 0} <i>OctalDigits</i>
     *
     * <dt><i>Sign:</i>
     * <dd>{@code -}
     * <dd>{@code +}
     * </dl>
     * </blockquote>
     *
     * <i>DecimalNumeral</i>, <i>HexDigits</i>, and <i>OctalDigits</i>
     * are as defined in section 3.10.1 of
     * <cite>The Java&trade; Language Specification</cite>,
     * except that underscores are not accepted between digits.
     *
     * <p>The sequence of characters following an optional
     * sign and/or radix specifier ("{@code 0x}", "{@code 0X}",
     * "{@code #}", or leading zero) is parsed as by the {@code
     * Long.parseLong} method with the indicated radix (10, 16, or 8).
     * This sequence of characters must represent a positive value or
     * a {@link NumberFormatException} will be thrown.  The result is
     * negated if first character of the specified {@code String} is
     * the minus sign.  No whitespace characters are permitted in the
     * {@code String}.
     *
     * @param     nm the {@code String} to decode.
     * @return    a {@code Long} object holding the {@code long}
     *            value represented by {@code nm}
     * @throws    NumberFormatException  if the {@code String} does not
     *            contain a parsable {@code long}.
     * @see java.lang.Long#parseLong(String, int)
     * @since 1.2
     */
    public static Long decode(String nm) throws NumberFormatException {
        int radix = 10;
        int index = 0;
        boolean negative = false;
        Long result;

        if (nm.length() == 0)
            throw new NumberFormatException("Zero length string");
        char firstChar = nm.charAt(0);
        // Handle sign, if present
        if (firstChar == '-') {
            negative = true;
            index++;
        } else if (firstChar == '+')
            index++;

        // Handle radix specifier, if present
        if (nm.startsWith("0x", index) || nm.startsWith("0X", index)) {
            index += 2;
            radix = 16;
        }
        else if (nm.startsWith("#", index)) {
            index ++;
            radix = 16;
        }
        else if (nm.startsWith("0", index) && nm.length() > 1 + index) {
            index ++;
            radix = 8;
        }

        if (nm.startsWith("-", index) || nm.startsWith("+", index))
            throw new NumberFormatException("Sign character in wrong position");

        try {
            result = Long.valueOf(nm.substring(index), radix);
            result = negative ? Long.valueOf(-result.longValue()) : result;
        } catch (NumberFormatException e) {
            // If number is Long.MIN_VALUE, we'll end up here. The next line
            // handles this case, and causes any genuine format error to be
            // rethrown.
            String constant = negative ? ("-" + nm.substring(index))
                                       : nm.substring(index);
            result = Long.valueOf(constant, radix);
        }
        return result;
    }

    /**
     * The value of the {@code Long}.
     *
     * @serial
     */
    private final long value;

    /**
     * Constructs a newly allocated {@code Long} object that
     * represents the specified {@code long} argument.
     *
     * @param   value   the value to be represented by the
     *          {@code Long} object.
     *
     * @deprecated
     * It is rarely appropriate to use this constructor. The static factory
     * {@link #valueOf(long)} is generally a better choice, as it is
     * likely to yield significantly better space and time performance.
     */
    @Deprecated(since="9")
    public Long(long value) {
        this.value = value;
    }

    /**
     * Constructs a newly allocated {@code Long} object that
     * represents the {@code long} value indicated by the
     * {@code String} parameter. The string is converted to a
     * {@code long} value in exactly the manner used by the
     * {@code parseLong} method for radix 10.
     *
     * @param      s   the {@code String} to be converted to a
     *             {@code Long}.
     * @throws     NumberFormatException  if the {@code String} does not
     *             contain a parsable {@code long}.
     *
     * @deprecated
     * It is rarely appropriate to use this constructor.
     * Use {@link #parseLong(String)} to convert a string to a
     * {@code long} primitive, or use {@link #valueOf(String)}
     * to convert a string to a {@code Long} object.
     */
    @Deprecated(since="9")
    public Long(String s) throws NumberFormatException {
        this.value = parseLong(s, 10);
    }

    /**
     * Returns the value of this {@code Long} as a {@code byte} after
     * a narrowing primitive conversion.
     * @jls 5.1.3 Narrowing Primitive Conversions
     */
    public byte byteValue() {
        return (byte)value;
    }

    /**
     * Returns the value of this {@code Long} as a {@code short} after
     * a narrowing primitive conversion.
     * @jls 5.1.3 Narrowing Primitive Conversions
     */
    public short shortValue() {
        return (short)value;
    }

    /**
     * Returns the value of this {@code Long} as an {@code int} after
     * a narrowing primitive conversion.
     * @jls 5.1.3 Narrowing Primitive Conversions
     */
    public int intValue() {
        return (int)value;
    }

    /**
     * Returns the value of this {@code Long} as a
     * {@code long} value.
     */
    @HotSpotIntrinsicCandidate
    public long longValue() {
        return value;
    }

    /**
     * Returns the value of this {@code Long} as a {@code float} after
     * a widening primitive conversion.
     * @jls 5.1.2 Widening Primitive Conversions
     */
    public float floatValue() {
        return (float)value;
    }

    /**
     * Returns the value of this {@code Long} as a {@code double}
     * after a widening primitive conversion.
     * @jls 5.1.2 Widening Primitive Conversions
     */
    public double doubleValue() {
        return (double)value;
    }

    /**
     * Returns a {@code String} object representing this
     * {@code Long}'s value.  The value is converted to signed
     * decimal representation and returned as a string, exactly as if
     * the {@code long} value were given as an argument to the
     * {@link java.lang.Long#toString(long)} method.
     *
     * @return  a string representation of the value of this object in
     *          base&nbsp;10.
     */
    public String toString() {
        return toString(value);
    }

    /**
     * Returns a hash code for this {@code Long}. The result is
     * the exclusive OR of the two halves of the primitive
     * {@code long} value held by this {@code Long}
     * object. That is, the hashcode is the value of the expression:
     *
     * <blockquote>
     *  {@code (int)(this.longValue()^(this.longValue()>>>32))}
     * </blockquote>
     *
     * @return  a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Long.hashCode(value);
    }

    /**
     * Returns a hash code for a {@code long} value; compatible with
     * {@code Long.hashCode()}.
     *
     * @param value the value to hash
     * @return a hash code value for a {@code long} value.
     * @since 1.8
     */
    public static int hashCode(long value) {
        return (int)(value ^ (value >>> 32));
    }

    /**
     * Compares this object to the specified object.  The result is
     * {@code true} if and only if the argument is not
     * {@code null} and is a {@code Long} object that
     * contains the same {@code long} value as this object.
     *
     * @param   obj   the object to compare with.
     * @return  {@code true} if the objects are the same;
     *          {@code false} otherwise.
     */
    public boolean equals(Object obj) {
        if (obj instanceof Long) {
            return value == ((Long)obj).longValue();
        }
        return false;
    }

    /**
     * Determines the {@code long} value of the system property
     * with the specified name.
     *
     * <p>The first argument is treated as the name of a system
     * property.  System properties are accessible through the {@link
     * java.lang.System#getProperty(java.lang.String)} method. The
     * string value of this property is then interpreted as a {@code
     * long} value using the grammar supported by {@link Long#decode decode}
     * and a {@code Long} object representing this value is returned.
     *
     * <p>If there is no property with the specified name, if the
     * specified name is empty or {@code null}, or if the property
     * does not have the correct numeric format, then {@code null} is
     * returned.
     *
     * <p>In other words, this method returns a {@code Long} object
     * equal to the value of:
     *
     * <blockquote>
     *  {@code getLong(nm, null)}
     * </blockquote>
     *
     * @param   nm   property name.
     * @return  the {@code Long} value of the property.
     * @throws  SecurityException for the same reasons as
     *          {@link System#getProperty(String) System.getProperty}
     * @see     java.lang.System#getProperty(java.lang.String)
     * @see     java.lang.System#getProperty(java.lang.String, java.lang.String)
     */
    public static Long getLong(String nm) {
        return getLong(nm, null);
    }

    /**
     * Determines the {@code long} value of the system property
     * with the specified name.
     *
     * <p>The first argument is treated as the name of a system
     * property.  System properties are accessible through the {@link
     * java.lang.System#getProperty(java.lang.String)} method. The
     * string value of this property is then interpreted as a {@code
     * long} value using the grammar supported by {@link Long#decode decode}
     * and a {@code Long} object representing this value is returned.
     *
     * <p>The second argument is the default value. A {@code Long} object
     * that represents the value of the second argument is returned if there
     * is no property of the specified name, if the property does not have
     * the correct numeric format, or if the specified name is empty or null.
     *
     * <p>In other words, this method returns a {@code Long} object equal
     * to the value of:
     *
     * <blockquote>
     *  {@code getLong(nm, new Long(val))}
     * </blockquote>
     *
     * but in practice it may be implemented in a manner such as:
     *
     * <blockquote><pre>
     * Long result = getLong(nm, null);
     * return (result == null) ? new Long(val) : result;
     * </pre></blockquote>
     *
     * to avoid the unnecessary allocation of a {@code Long} object when
     * the default value is not needed.
     *
     * @param   nm    property name.
     * @param   val   default value.
     * @return  the {@code Long} value of the property.
     * @throws  SecurityException for the same reasons as
     *          {@link System#getProperty(String) System.getProperty}
     * @see     java.lang.System#getProperty(java.lang.String)
     * @see     java.lang.System#getProperty(java.lang.String, java.lang.String)
     */
    public static Long getLong(String nm, long val) {
        Long result = Long.getLong(nm, null);
        return (result == null) ? Long.valueOf(val) : result;
    }

    /**
     * Returns the {@code long} value of the system property with
     * the specified name.  The first argument is treated as the name
     * of a system property.  System properties are accessible through
     * the {@link java.lang.System#getProperty(java.lang.String)}
     * method. The string value of this property is then interpreted
     * as a {@code long} value, as per the
     * {@link Long#decode decode} method, and a {@code Long} object
     * representing this value is returned; in summary:
     *
     * <ul>
     * <li>If the property value begins with the two ASCII characters
     * {@code 0x} or the ASCII character {@code #}, not followed by
     * a minus sign, then the rest of it is parsed as a hexadecimal integer
     * exactly as for the method {@link #valueOf(java.lang.String, int)}
     * with radix 16.
     * <li>If the property value begins with the ASCII character
     * {@code 0} followed by another character, it is parsed as
     * an octal integer exactly as by the method {@link
     * #valueOf(java.lang.String, int)} with radix 8.
     * <li>Otherwise the property value is parsed as a decimal
     * integer exactly as by the method
     * {@link #valueOf(java.lang.String, int)} with radix 10.
     * </ul>
     *
     * <p>Note that, in every case, neither {@code L}
     * ({@code '\u005Cu004C'}) nor {@code l}
     * ({@code '\u005Cu006C'}) is permitted to appear at the end
     * of the property value as a type indicator, as would be
     * permitted in Java programming language source code.
     *
     * <p>The second argument is the default value. The default value is
     * returned if there is no property of the specified name, if the
     * property does not have the correct numeric format, or if the
     * specified name is empty or {@code null}.
     *
     * @param   nm   property name.
     * @param   val   default value.
     * @return  the {@code Long} value of the property.
     * @throws  SecurityException for the same reasons as
     *          {@link System#getProperty(String) System.getProperty}
     * @see     System#getProperty(java.lang.String)
     * @see     System#getProperty(java.lang.String, java.lang.String)
     */
    public static Long getLong(String nm, Long val) {
        String v = null;
        try {
            v = System.getProperty(nm);
        } catch (IllegalArgumentException | NullPointerException e) {
        }
        if (v != null) {
            try {
                return Long.decode(v);
            } catch (NumberFormatException e) {
            }
        }
        return val;
    }

    /**
     * Compares two {@code Long} objects numerically.
     *
     * @param   anotherLong   the {@code Long} to be compared.
     * @return  the value {@code 0} if this {@code Long} is
     *          equal to the argument {@code Long}; a value less than
     *          {@code 0} if this {@code Long} is numerically less
     *          than the argument {@code Long}; and a value greater
     *          than {@code 0} if this {@code Long} is numerically
     *           greater than the argument {@code Long} (signed
     *           comparison).
     * @since   1.2
     */
    public int compareTo(Long anotherLong) {
        return compare(this.value, anotherLong.value);
    }

    /**
     * Compares two {@code long} values numerically.
     * The value returned is identical to what would be returned by:
     * <pre>
     *    Long.valueOf(x).compareTo(Long.valueOf(y))
     * </pre>
     *
     * @param  x the first {@code long} to compare
     * @param  y the second {@code long} to compare
     * @return the value {@code 0} if {@code x == y};
     *         a value less than {@code 0} if {@code x < y}; and
     *         a value greater than {@code 0} if {@code x > y}
     * @since 1.7
     */
    public static int compare(long x, long y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

    /**
     * Compares two {@code long} values numerically treating the values
     * as unsigned.
     *
     * @param  x the first {@code long} to compare
     * @param  y the second {@code long} to compare
     * @return the value {@code 0} if {@code x == y}; a value less
     *         than {@code 0} if {@code x < y} as unsigned values; and
     *         a value greater than {@code 0} if {@code x > y} as
     *         unsigned values
     * @since 1.8
     */
    public static int compareUnsigned(long x, long y) {
        return compare(x + MIN_VALUE, y + MIN_VALUE);
    }


    /**
     * Returns the unsigned quotient of dividing the first argument by
     * the second where each argument and the result is interpreted as
     * an unsigned value.
     *
     * <p>Note that in two's complement arithmetic, the three other
     * basic arithmetic operations of add, subtract, and multiply are
     * bit-wise identical if the two operands are regarded as both
     * being signed or both being unsigned.  Therefore separate {@code
     * addUnsigned}, etc. methods are not provided.
     *
     * @param dividend the value to be divided
     * @param divisor the value doing the dividing
     * @return the unsigned quotient of the first argument divided by
     * the second argument
     * @see #remainderUnsigned
     * @since 1.8
     */
    public static long divideUnsigned(long dividend, long divisor) {
        if (divisor < 0L) { // signed comparison
            // Answer must be 0 or 1 depending on relative magnitude
            // of dividend and divisor.
            return (compareUnsigned(dividend, divisor)) < 0 ? 0L :1L;
        }

        if (dividend > 0) //  Both inputs non-negative
            return dividend/divisor;
        else {
            /*
             * For simple code, leveraging BigInteger.  Longer and faster
             * code written directly in terms of operations on longs is
             * possible; see "Hacker's Delight" for divide and remainder
             * algorithms.
             */
            return toUnsignedBigInteger(dividend).
                divide(toUnsignedBigInteger(divisor)).longValue();
        }
    }

    /**
     * Returns the unsigned remainder from dividing the first argument
     * by the second where each argument and the result is interpreted
     * as an unsigned value.
     *
     * @param dividend the value to be divided
     * @param divisor the value doing the dividing
     * @return the unsigned remainder of the first argument divided by
     * the second argument
     * @see #divideUnsigned
     * @since 1.8
     */
    public static long remainderUnsigned(long dividend, long divisor) {
        if (dividend > 0 && divisor > 0) { // signed comparisons
            return dividend % divisor;
        } else {
            if (compareUnsigned(dividend, divisor) < 0) // Avoid explicit check for 0 divisor
                return dividend;
            else
                return toUnsignedBigInteger(dividend).
                    remainder(toUnsignedBigInteger(divisor)).longValue();
        }
    }

    // Bit Twiddling

    /**
     * The number of bits used to represent a {@code long} value in two's
     * complement binary form.
     *
     * @since 1.5
     */
    @Native public static final int SIZE = 64;

    /**
     * The number of bytes used to represent a {@code long} value in two's
     * complement binary form.
     *
     * @since 1.8
     */
    public static final int BYTES = SIZE / Byte.SIZE;

    /**
     * Returns a {@code long} value with at most a single one-bit, in the
     * position of the highest-order ("leftmost") one-bit in the specified
     * {@code long} value.  Returns zero if the specified value has no
     * one-bits in its two's complement binary representation, that is, if it
     * is equal to zero.
     *
     * @param i the value whose highest one bit is to be computed
     * @return a {@code long} value with a single one-bit, in the position
     *     of the highest-order one-bit in the specified value, or zero if
     *     the specified value is itself equal to zero.
     * @since 1.5
     */
    public static long highestOneBit(long i) {
        return i & (MIN_VALUE >>> numberOfLeadingZeros(i));
    }

    /**
     * Returns a {@code long} value with at most a single one-bit, in the
     * position of the lowest-order ("rightmost") one-bit in the specified
     * {@code long} value.  Returns zero if the specified value has no
     * one-bits in its two's complement binary representation, that is, if it
     * is equal to zero.
     *
     * @param i the value whose lowest one bit is to be computed
     * @return a {@code long} value with a single one-bit, in the position
     *     of the lowest-order one-bit in the specified value, or zero if
     *     the specified value is itself equal to zero.
     * @since 1.5
     */
    public static long lowestOneBit(long i) {
        // HD, Section 2-1
        return i & -i;
    }

    /**
     * Returns the number of zero bits preceding the highest-order
     * ("leftmost") one-bit in the two's complement binary representation
     * of the specified {@code long} value.  Returns 64 if the
     * specified value has no one-bits in its two's complement representation,
     * in other words if it is equal to zero.
     *
     * <p>Note that this method is closely related to the logarithm base 2.
     * For all positive {@code long} values x:
     * <ul>
     * <li>floor(log<sub>2</sub>(x)) = {@code 63 - numberOfLeadingZeros(x)}
     * <li>ceil(log<sub>2</sub>(x)) = {@code 64 - numberOfLeadingZeros(x - 1)}
     * </ul>
     *
     * @param i the value whose number of leading zeros is to be computed
     * @return the number of zero bits preceding the highest-order
     *     ("leftmost") one-bit in the two's complement binary representation
     *     of the specified {@code long} value, or 64 if the value
     *     is equal to zero.
     * @since 1.5
     */
    @HotSpotIntrinsicCandidate
    public static int numberOfLeadingZeros(long i) {
        int x = (int)(i >>> 32);
        return x == 0 ? 32 + Integer.numberOfLeadingZeros((int)i)
                : Integer.numberOfLeadingZeros(x);
    }

    /**
     * Returns the number of zero bits following the lowest-order ("rightmost")
     * one-bit in the two's complement binary representation of the specified
     * {@code long} value.  Returns 64 if the specified value has no
     * one-bits in its two's complement representation, in other words if it is
     * equal to zero.
     *
     * @param i the value whose number of trailing zeros is to be computed
     * @return the number of zero bits following the lowest-order ("rightmost")
     *     one-bit in the two's complement binary representation of the
     *     specified {@code long} value, or 64 if the value is equal
     *     to zero.
     * @since 1.5
     */
    @HotSpotIntrinsicCandidate
    public static int numberOfTrailingZeros(long i) {
        // HD, Figure 5-14
        int x, y;
        if (i == 0) return 64;
        int n = 63;
        y = (int)i; if (y != 0) { n = n -32; x = y; } else x = (int)(i>>>32);
        y = x <<16; if (y != 0) { n = n -16; x = y; }
        y = x << 8; if (y != 0) { n = n - 8; x = y; }
        y = x << 4; if (y != 0) { n = n - 4; x = y; }
        y = x << 2; if (y != 0) { n = n - 2; x = y; }
        return n - ((x << 1) >>> 31);
    }

    /**
     * Returns the number of one-bits in the two's complement binary
     * representation of the specified {@code long} value.  This function is
     * sometimes referred to as the <i>population count</i>.
     *
     * @param i the value whose bits are to be counted
     * @return the number of one-bits in the two's complement binary
     *     representation of the specified {@code long} value.
     * @since 1.5
     */
     @HotSpotIntrinsicCandidate
     public static int bitCount(long i) {
        // HD, Figure 5-2
        i = i - ((i >>> 1) & 0x5555555555555555L);
        i = (i & 0x3333333333333333L) + ((i >>> 2) & 0x3333333333333333L);
        i = (i + (i >>> 4)) & 0x0f0f0f0f0f0f0f0fL;
        i = i + (i >>> 8);
        i = i + (i >>> 16);
        i = i + (i >>> 32);
        return (int)i & 0x7f;
     }

    /**
     * Returns the value obtained by rotating the two's complement binary
     * representation of the specified {@code long} value left by the
     * specified number of bits.  (Bits shifted out of the left hand, or
     * high-order, side reenter on the right, or low-order.)
     *
     * <p>Note that left rotation with a negative distance is equivalent to
     * right rotation: {@code rotateLeft(val, -distance) == rotateRight(val,
     * distance)}.  Note also that rotation by any multiple of 64 is a
     * no-op, so all but the last six bits of the rotation distance can be
     * ignored, even if the distance is negative: {@code rotateLeft(val,
     * distance) == rotateLeft(val, distance & 0x3F)}.
     *
     * @param i the value whose bits are to be rotated left
     * @param distance the number of bit positions to rotate left
     * @return the value obtained by rotating the two's complement binary
     *     representation of the specified {@code long} value left by the
     *     specified number of bits.
     * @since 1.5
     */
    public static long rotateLeft(long i, int distance) {
        return (i << distance) | (i >>> -distance);
    }

    /**
     * Returns the value obtained by rotating the two's complement binary
     * representation of the specified {@code long} value right by the
     * specified number of bits.  (Bits shifted out of the right hand, or
     * low-order, side reenter on the left, or high-order.)
     *
     * <p>Note that right rotation with a negative distance is equivalent to
     * left rotation: {@code rotateRight(val, -distance) == rotateLeft(val,
     * distance)}.  Note also that rotation by any multiple of 64 is a
     * no-op, so all but the last six bits of the rotation distance can be
     * ignored, even if the distance is negative: {@code rotateRight(val,
     * distance) == rotateRight(val, distance & 0x3F)}.
     *
     * @param i the value whose bits are to be rotated right
     * @param distance the number of bit positions to rotate right
     * @return the value obtained by rotating the two's complement binary
     *     representation of the specified {@code long} value right by the
     *     specified number of bits.
     * @since 1.5
     */
    public static long rotateRight(long i, int distance) {
        return (i >>> distance) | (i << -distance);
    }

    /**
     * Returns the value obtained by reversing the order of the bits in the
     * two's complement binary representation of the specified {@code long}
     * value.
     *
     * @param i the value to be reversed
     * @return the value obtained by reversing order of the bits in the
     *     specified {@code long} value.
     * @since 1.5
     */
    public static long reverse(long i) {
        // HD, Figure 7-1
        i = (i & 0x5555555555555555L) << 1 | (i >>> 1) & 0x5555555555555555L;
        i = (i & 0x3333333333333333L) << 2 | (i >>> 2) & 0x3333333333333333L;
        i = (i & 0x0f0f0f0f0f0f0f0fL) << 4 | (i >>> 4) & 0x0f0f0f0f0f0f0f0fL;

        return reverseBytes(i);
    }

    /**
     * Returns the signum function of the specified {@code long} value.  (The
     * return value is -1 if the specified value is negative; 0 if the
     * specified value is zero; and 1 if the specified value is positive.)
     *
     * @param i the value whose signum is to be computed
     * @return the signum function of the specified {@code long} value.
     * @since 1.5
     */
    public static int signum(long i) {
        // HD, Section 2-7
        return (int) ((i >> 63) | (-i >>> 63));
    }

    /**
     * Returns the value obtained by reversing the order of the bytes in the
     * two's complement representation of the specified {@code long} value.
     *
     * @param i the value whose bytes are to be reversed
     * @return the value obtained by reversing the bytes in the specified
     *     {@code long} value.
     * @since 1.5
     */
    @HotSpotIntrinsicCandidate
    public static long reverseBytes(long i) {
        i = (i & 0x00ff00ff00ff00ffL) << 8 | (i >>> 8) & 0x00ff00ff00ff00ffL;
        return (i << 48) | ((i & 0xffff0000L) << 16) |
            ((i >>> 16) & 0xffff0000L) | (i >>> 48);
    }

    /**
     * Adds two {@code long} values together as per the + operator.
     *
     * @param a the first operand
     * @param b the second operand
     * @return the sum of {@code a} and {@code b}
     * @see java.util.function.BinaryOperator
     * @since 1.8
     */
    public static long sum(long a, long b) {
        return a + b;
    }

    /**
     * Returns the greater of two {@code long} values
     * as if by calling {@link Math#max(long, long) Math.max}.
     *
     * @param a the first operand
     * @param b the second operand
     * @return the greater of {@code a} and {@code b}
     * @see java.util.function.BinaryOperator
     * @since 1.8
     */
    public static long max(long a, long b) {
        return Math.max(a, b);
    }

    /**
     * Returns the smaller of two {@code long} values
     * as if by calling {@link Math#min(long, long) Math.min}.
     *
     * @param a the first operand
     * @param b the second operand
     * @return the smaller of {@code a} and {@code b}
     * @see java.util.function.BinaryOperator
     * @since 1.8
     */
    public static long min(long a, long b) {
        return Math.min(a, b);
    }

    /** use serialVersionUID from JDK 1.0.2 for interoperability */
    @Native private static final long serialVersionUID = 4290774380558885855L;
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\Math.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1994, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.math.BigDecimal;
import java.util.Random;
import jdk.internal.math.FloatConsts;
import jdk.internal.math.DoubleConsts;
import jdk.internal.HotSpotIntrinsicCandidate;

/**
 * The class {@code Math} contains methods for performing basic
 * numeric operations such as the elementary exponential, logarithm,
 * square root, and trigonometric functions.
 *
 * <p>Unlike some of the numeric methods of class
 * {@code StrictMath}, all implementations of the equivalent
 * functions of class {@code Math} are not defined to return the
 * bit-for-bit same results.  This relaxation permits
 * better-performing implementations where strict reproducibility is
 * not required.
 *
 * <p>By default many of the {@code Math} methods simply call
 * the equivalent method in {@code StrictMath} for their
 * implementation.  Code generators are encouraged to use
 * platform-specific native libraries or microprocessor instructions,
 * where available, to provide higher-performance implementations of
 * {@code Math} methods.  Such higher-performance
 * implementations still must conform to the specification for
 * {@code Math}.
 *
 * <p>The quality of implementation specifications concern two
 * properties, accuracy of the returned result and monotonicity of the
 * method.  Accuracy of the floating-point {@code Math} methods is
 * measured in terms of <i>ulps</i>, units in the last place.  For a
 * given floating-point format, an {@linkplain #ulp(double) ulp} of a
 * specific real number value is the distance between the two
 * floating-point values bracketing that numerical value.  When
 * discussing the accuracy of a method as a whole rather than at a
 * specific argument, the number of ulps cited is for the worst-case
 * error at any argument.  If a method always has an error less than
 * 0.5 ulps, the method always returns the floating-point number
 * nearest the exact result; such a method is <i>correctly
 * rounded</i>.  A correctly rounded method is generally the best a
 * floating-point approximation can be; however, it is impractical for
 * many floating-point methods to be correctly rounded.  Instead, for
 * the {@code Math} class, a larger error bound of 1 or 2 ulps is
 * allowed for certain methods.  Informally, with a 1 ulp error bound,
 * when the exact result is a representable number, the exact result
 * should be returned as the computed result; otherwise, either of the
 * two floating-point values which bracket the exact result may be
 * returned.  For exact results large in magnitude, one of the
 * endpoints of the bracket may be infinite.  Besides accuracy at
 * individual arguments, maintaining proper relations between the
 * method at different arguments is also important.  Therefore, most
 * methods with more than 0.5 ulp errors are required to be
 * <i>semi-monotonic</i>: whenever the mathematical function is
 * non-decreasing, so is the floating-point approximation, likewise,
 * whenever the mathematical function is non-increasing, so is the
 * floating-point approximation.  Not all approximations that have 1
 * ulp accuracy will automatically meet the monotonicity requirements.
 *
 * <p>
 * The platform uses signed two's complement integer arithmetic with
 * int and long primitive types.  The developer should choose
 * the primitive type to ensure that arithmetic operations consistently
 * produce correct results, which in some cases means the operations
 * will not overflow the range of values of the computation.
 * The best practice is to choose the primitive type and algorithm to avoid
 * overflow. In cases where the size is {@code int} or {@code long} and
 * overflow errors need to be detected, the methods {@code addExact},
 * {@code subtractExact}, {@code multiplyExact}, and {@code toIntExact}
 * throw an {@code ArithmeticException} when the results overflow.
 * For other arithmetic operations such as divide, absolute value,
 * increment by one, decrement by one, and negation, overflow occurs only with
 * a specific minimum or maximum value and should be checked against
 * the minimum or maximum as appropriate.
 *
 * @author  unascribed
 * @author  Joseph D. Darcy
 * @since   1.0
 */

public final class Math {

    /**
     * Don't let anyone instantiate this class.
     */
    private Math() {}

    /**
     * The {@code double} value that is closer than any other to
     * <i>e</i>, the base of the natural logarithms.
     */
    public static final double E = 2.7182818284590452354;

    /**
     * The {@code double} value that is closer than any other to
     * <i>pi</i>, the ratio of the circumference of a circle to its
     * diameter.
     */
    public static final double PI = 3.14159265358979323846;

    /**
     * Constant by which to multiply an angular value in degrees to obtain an
     * angular value in radians.
     */
    private static final double DEGREES_TO_RADIANS = 0.017453292519943295;

    /**
     * Constant by which to multiply an angular value in radians to obtain an
     * angular value in degrees.
     */
    private static final double RADIANS_TO_DEGREES = 57.29577951308232;

    /**
     * Returns the trigonometric sine of an angle.  Special cases:
     * <ul><li>If the argument is NaN or an infinity, then the
     * result is NaN.
     * <li>If the argument is zero, then the result is a zero with the
     * same sign as the argument.</ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   a   an angle, in radians.
     * @return  the sine of the argument.
     */
    @HotSpotIntrinsicCandidate
    public static double sin(double a) {
        return StrictMath.sin(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the trigonometric cosine of an angle. Special cases:
     * <ul><li>If the argument is NaN or an infinity, then the
     * result is NaN.</ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   a   an angle, in radians.
     * @return  the cosine of the argument.
     */
    @HotSpotIntrinsicCandidate
    public static double cos(double a) {
        return StrictMath.cos(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the trigonometric tangent of an angle.  Special cases:
     * <ul><li>If the argument is NaN or an infinity, then the result
     * is NaN.
     * <li>If the argument is zero, then the result is a zero with the
     * same sign as the argument.</ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   a   an angle, in radians.
     * @return  the tangent of the argument.
     */
    @HotSpotIntrinsicCandidate
    public static double tan(double a) {
        return StrictMath.tan(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the arc sine of a value; the returned angle is in the
     * range -<i>pi</i>/2 through <i>pi</i>/2.  Special cases:
     * <ul><li>If the argument is NaN or its absolute value is greater
     * than 1, then the result is NaN.
     * <li>If the argument is zero, then the result is a zero with the
     * same sign as the argument.</ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   a   the value whose arc sine is to be returned.
     * @return  the arc sine of the argument.
     */
    public static double asin(double a) {
        return StrictMath.asin(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the arc cosine of a value; the returned angle is in the
     * range 0.0 through <i>pi</i>.  Special case:
     * <ul><li>If the argument is NaN or its absolute value is greater
     * than 1, then the result is NaN.</ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   a   the value whose arc cosine is to be returned.
     * @return  the arc cosine of the argument.
     */
    public static double acos(double a) {
        return StrictMath.acos(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the arc tangent of a value; the returned angle is in the
     * range -<i>pi</i>/2 through <i>pi</i>/2.  Special cases:
     * <ul><li>If the argument is NaN, then the result is NaN.
     * <li>If the argument is zero, then the result is a zero with the
     * same sign as the argument.</ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   a   the value whose arc tangent is to be returned.
     * @return  the arc tangent of the argument.
     */
    public static double atan(double a) {
        return StrictMath.atan(a); // default impl. delegates to StrictMath
    }

    /**
     * Converts an angle measured in degrees to an approximately
     * equivalent angle measured in radians.  The conversion from
     * degrees to radians is generally inexact.
     *
     * @param   angdeg   an angle, in degrees
     * @return  the measurement of the angle {@code angdeg}
     *          in radians.
     * @since   1.2
     */
    public static double toRadians(double angdeg) {
        return angdeg * DEGREES_TO_RADIANS;
    }

    /**
     * Converts an angle measured in radians to an approximately
     * equivalent angle measured in degrees.  The conversion from
     * radians to degrees is generally inexact; users should
     * <i>not</i> expect {@code cos(toRadians(90.0))} to exactly
     * equal {@code 0.0}.
     *
     * @param   angrad   an angle, in radians
     * @return  the measurement of the angle {@code angrad}
     *          in degrees.
     * @since   1.2
     */
    public static double toDegrees(double angrad) {
        return angrad * RADIANS_TO_DEGREES;
    }

    /**
     * Returns Euler's number <i>e</i> raised to the power of a
     * {@code double} value.  Special cases:
     * <ul><li>If the argument is NaN, the result is NaN.
     * <li>If the argument is positive infinity, then the result is
     * positive infinity.
     * <li>If the argument is negative infinity, then the result is
     * positive zero.</ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   a   the exponent to raise <i>e</i> to.
     * @return  the value <i>e</i><sup>{@code a}</sup>,
     *          where <i>e</i> is the base of the natural logarithms.
     */
    @HotSpotIntrinsicCandidate
    public static double exp(double a) {
        return StrictMath.exp(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the natural logarithm (base <i>e</i>) of a {@code double}
     * value.  Special cases:
     * <ul><li>If the argument is NaN or less than zero, then the result
     * is NaN.
     * <li>If the argument is positive infinity, then the result is
     * positive infinity.
     * <li>If the argument is positive zero or negative zero, then the
     * result is negative infinity.</ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   a   a value
     * @return  the value ln&nbsp;{@code a}, the natural logarithm of
     *          {@code a}.
     */
    @HotSpotIntrinsicCandidate
    public static double log(double a) {
        return StrictMath.log(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the base 10 logarithm of a {@code double} value.
     * Special cases:
     *
     * <ul><li>If the argument is NaN or less than zero, then the result
     * is NaN.
     * <li>If the argument is positive infinity, then the result is
     * positive infinity.
     * <li>If the argument is positive zero or negative zero, then the
     * result is negative infinity.
     * <li> If the argument is equal to 10<sup><i>n</i></sup> for
     * integer <i>n</i>, then the result is <i>n</i>.
     * </ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   a   a value
     * @return  the base 10 logarithm of  {@code a}.
     * @since 1.5
     */
    @HotSpotIntrinsicCandidate
    public static double log10(double a) {
        return StrictMath.log10(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the correctly rounded positive square root of a
     * {@code double} value.
     * Special cases:
     * <ul><li>If the argument is NaN or less than zero, then the result
     * is NaN.
     * <li>If the argument is positive infinity, then the result is positive
     * infinity.
     * <li>If the argument is positive zero or negative zero, then the
     * result is the same as the argument.</ul>
     * Otherwise, the result is the {@code double} value closest to
     * the true mathematical square root of the argument value.
     *
     * @param   a   a value.
     * @return  the positive square root of {@code a}.
     *          If the argument is NaN or less than zero, the result is NaN.
     */
    @HotSpotIntrinsicCandidate
    public static double sqrt(double a) {
        return StrictMath.sqrt(a); // default impl. delegates to StrictMath
                                   // Note that hardware sqrt instructions
                                   // frequently can be directly used by JITs
                                   // and should be much faster than doing
                                   // Math.sqrt in software.
    }


    /**
     * Returns the cube root of a {@code double} value.  For
     * positive finite {@code x}, {@code cbrt(-x) ==
     * -cbrt(x)}; that is, the cube root of a negative value is
     * the negative of the cube root of that value's magnitude.
     *
     * Special cases:
     *
     * <ul>
     *
     * <li>If the argument is NaN, then the result is NaN.
     *
     * <li>If the argument is infinite, then the result is an infinity
     * with the same sign as the argument.
     *
     * <li>If the argument is zero, then the result is a zero with the
     * same sign as the argument.
     *
     * </ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     *
     * @param   a   a value.
     * @return  the cube root of {@code a}.
     * @since 1.5
     */
    public static double cbrt(double a) {
        return StrictMath.cbrt(a);
    }

    /**
     * Computes the remainder operation on two arguments as prescribed
     * by the IEEE 754 standard.
     * The remainder value is mathematically equal to
     * <code>f1&nbsp;-&nbsp;f2</code>&nbsp;&times;&nbsp;<i>n</i>,
     * where <i>n</i> is the mathematical integer closest to the exact
     * mathematical value of the quotient {@code f1/f2}, and if two
     * mathematical integers are equally close to {@code f1/f2},
     * then <i>n</i> is the integer that is even. If the remainder is
     * zero, its sign is the same as the sign of the first argument.
     * Special cases:
     * <ul><li>If either argument is NaN, or the first argument is infinite,
     * or the second argument is positive zero or negative zero, then the
     * result is NaN.
     * <li>If the first argument is finite and the second argument is
     * infinite, then the result is the same as the first argument.</ul>
     *
     * @param   f1   the dividend.
     * @param   f2   the divisor.
     * @return  the remainder when {@code f1} is divided by
     *          {@code f2}.
     */
    public static double IEEEremainder(double f1, double f2) {
        return StrictMath.IEEEremainder(f1, f2); // delegate to StrictMath
    }

    /**
     * Returns the smallest (closest to negative infinity)
     * {@code double} value that is greater than or equal to the
     * argument and is equal to a mathematical integer. Special cases:
     * <ul><li>If the argument value is already equal to a
     * mathematical integer, then the result is the same as the
     * argument.  <li>If the argument is NaN or an infinity or
     * positive zero or negative zero, then the result is the same as
     * the argument.  <li>If the argument value is less than zero but
     * greater than -1.0, then the result is negative zero.</ul> Note
     * that the value of {@code Math.ceil(x)} is exactly the
     * value of {@code -Math.floor(-x)}.
     *
     *
     * @param   a   a value.
     * @return  the smallest (closest to negative infinity)
     *          floating-point value that is greater than or equal to
     *          the argument and is equal to a mathematical integer.
     */
    public static double ceil(double a) {
        return StrictMath.ceil(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the largest (closest to positive infinity)
     * {@code double} value that is less than or equal to the
     * argument and is equal to a mathematical integer. Special cases:
     * <ul><li>If the argument value is already equal to a
     * mathematical integer, then the result is the same as the
     * argument.  <li>If the argument is NaN or an infinity or
     * positive zero or negative zero, then the result is the same as
     * the argument.</ul>
     *
     * @param   a   a value.
     * @return  the largest (closest to positive infinity)
     *          floating-point value that less than or equal to the argument
     *          and is equal to a mathematical integer.
     */
    public static double floor(double a) {
        return StrictMath.floor(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the {@code double} value that is closest in value
     * to the argument and is equal to a mathematical integer. If two
     * {@code double} values that are mathematical integers are
     * equally close, the result is the integer value that is
     * even. Special cases:
     * <ul><li>If the argument value is already equal to a mathematical
     * integer, then the result is the same as the argument.
     * <li>If the argument is NaN or an infinity or positive zero or negative
     * zero, then the result is the same as the argument.</ul>
     *
     * @param   a   a {@code double} value.
     * @return  the closest floating-point value to {@code a} that is
     *          equal to a mathematical integer.
     */
    public static double rint(double a) {
        return StrictMath.rint(a); // default impl. delegates to StrictMath
    }

    /**
     * Returns the angle <i>theta</i> from the conversion of rectangular
     * coordinates ({@code x},&nbsp;{@code y}) to polar
     * coordinates (r,&nbsp;<i>theta</i>).
     * This method computes the phase <i>theta</i> by computing an arc tangent
     * of {@code y/x} in the range of -<i>pi</i> to <i>pi</i>. Special
     * cases:
     * <ul><li>If either argument is NaN, then the result is NaN.
     * <li>If the first argument is positive zero and the second argument
     * is positive, or the first argument is positive and finite and the
     * second argument is positive infinity, then the result is positive
     * zero.
     * <li>If the first argument is negative zero and the second argument
     * is positive, or the first argument is negative and finite and the
     * second argument is positive infinity, then the result is negative zero.
     * <li>If the first argument is positive zero and the second argument
     * is negative, or the first argument is positive and finite and the
     * second argument is negative infinity, then the result is the
     * {@code double} value closest to <i>pi</i>.
     * <li>If the first argument is negative zero and the second argument
     * is negative, or the first argument is negative and finite and the
     * second argument is negative infinity, then the result is the
     * {@code double} value closest to -<i>pi</i>.
     * <li>If the first argument is positive and the second argument is
     * positive zero or negative zero, or the first argument is positive
     * infinity and the second argument is finite, then the result is the
     * {@code double} value closest to <i>pi</i>/2.
     * <li>If the first argument is negative and the second argument is
     * positive zero or negative zero, or the first argument is negative
     * infinity and the second argument is finite, then the result is the
     * {@code double} value closest to -<i>pi</i>/2.
     * <li>If both arguments are positive infinity, then the result is the
     * {@code double} value closest to <i>pi</i>/4.
     * <li>If the first argument is positive infinity and the second argument
     * is negative infinity, then the result is the {@code double}
     * value closest to 3*<i>pi</i>/4.
     * <li>If the first argument is negative infinity and the second argument
     * is positive infinity, then the result is the {@code double} value
     * closest to -<i>pi</i>/4.
     * <li>If both arguments are negative infinity, then the result is the
     * {@code double} value closest to -3*<i>pi</i>/4.</ul>
     *
     * <p>The computed result must be within 2 ulps of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   y   the ordinate coordinate
     * @param   x   the abscissa coordinate
     * @return  the <i>theta</i> component of the point
     *          (<i>r</i>,&nbsp;<i>theta</i>)
     *          in polar coordinates that corresponds to the point
     *          (<i>x</i>,&nbsp;<i>y</i>) in Cartesian coordinates.
     */
    @HotSpotIntrinsicCandidate
    public static double atan2(double y, double x) {
        return StrictMath.atan2(y, x); // default impl. delegates to StrictMath
    }

    /**
     * Returns the value of the first argument raised to the power of the
     * second argument. Special cases:
     *
     * <ul><li>If the second argument is positive or negative zero, then the
     * result is 1.0.
     * <li>If the second argument is 1.0, then the result is the same as the
     * first argument.
     * <li>If the second argument is NaN, then the result is NaN.
     * <li>If the first argument is NaN and the second argument is nonzero,
     * then the result is NaN.
     *
     * <li>If
     * <ul>
     * <li>the absolute value of the first argument is greater than 1
     * and the second argument is positive infinity, or
     * <li>the absolute value of the first argument is less than 1 and
     * the second argument is negative infinity,
     * </ul>
     * then the result is positive infinity.
     *
     * <li>If
     * <ul>
     * <li>the absolute value of the first argument is greater than 1 and
     * the second argument is negative infinity, or
     * <li>the absolute value of the
     * first argument is less than 1 and the second argument is positive
     * infinity,
     * </ul>
     * then the result is positive zero.
     *
     * <li>If the absolute value of the first argument equals 1 and the
     * second argument is infinite, then the result is NaN.
     *
     * <li>If
     * <ul>
     * <li>the first argument is positive zero and the second argument
     * is greater than zero, or
     * <li>the first argument is positive infinity and the second
     * argument is less than zero,
     * </ul>
     * then the result is positive zero.
     *
     * <li>If
     * <ul>
     * <li>the first argument is positive zero and the second argument
     * is less than zero, or
     * <li>the first argument is positive infinity and the second
     * argument is greater than zero,
     * </ul>
     * then the result is positive infinity.
     *
     * <li>If
     * <ul>
     * <li>the first argument is negative zero and the second argument
     * is greater than zero but not a finite odd integer, or
     * <li>the first argument is negative infinity and the second
     * argument is less than zero but not a finite odd integer,
     * </ul>
     * then the result is positive zero.
     *
     * <li>If
     * <ul>
     * <li>the first argument is negative zero and the second argument
     * is a positive finite odd integer, or
     * <li>the first argument is negative infinity and the second
     * argument is a negative finite odd integer,
     * </ul>
     * then the result is negative zero.
     *
     * <li>If
     * <ul>
     * <li>the first argument is negative zero and the second argument
     * is less than zero but not a finite odd integer, or
     * <li>the first argument is negative infinity and the second
     * argument is greater than zero but not a finite odd integer,
     * </ul>
     * then the result is positive infinity.
     *
     * <li>If
     * <ul>
     * <li>the first argument is negative zero and the second argument
     * is a negative finite odd integer, or
     * <li>the first argument is negative infinity and the second
     * argument is a positive finite odd integer,
     * </ul>
     * then the result is negative infinity.
     *
     * <li>If the first argument is finite and less than zero
     * <ul>
     * <li> if the second argument is a finite even integer, the
     * result is equal to the result of raising the absolute value of
     * the first argument to the power of the second argument
     *
     * <li>if the second argument is a finite odd integer, the result
     * is equal to the negative of the result of raising the absolute
     * value of the first argument to the power of the second
     * argument
     *
     * <li>if the second argument is finite and not an integer, then
     * the result is NaN.
     * </ul>
     *
     * <li>If both arguments are integers, then the result is exactly equal
     * to the mathematical result of raising the first argument to the power
     * of the second argument if that result can in fact be represented
     * exactly as a {@code double} value.</ul>
     *
     * <p>(In the foregoing descriptions, a floating-point value is
     * considered to be an integer if and only if it is finite and a
     * fixed point of the method {@link #ceil ceil} or,
     * equivalently, a fixed point of the method {@link #floor
     * floor}. A value is a fixed point of a one-argument
     * method if and only if the result of applying the method to the
     * value is equal to the value.)
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   a   the base.
     * @param   b   the exponent.
     * @return  the value {@code a}<sup>{@code b}</sup>.
     */
    @HotSpotIntrinsicCandidate
    public static double pow(double a, double b) {
        return StrictMath.pow(a, b); // default impl. delegates to StrictMath
    }

    /**
     * Returns the closest {@code int} to the argument, with ties
     * rounding to positive infinity.
     *
     * <p>
     * Special cases:
     * <ul><li>If the argument is NaN, the result is 0.
     * <li>If the argument is negative infinity or any value less than or
     * equal to the value of {@code Integer.MIN_VALUE}, the result is
     * equal to the value of {@code Integer.MIN_VALUE}.
     * <li>If the argument is positive infinity or any value greater than or
     * equal to the value of {@code Integer.MAX_VALUE}, the result is
     * equal to the value of {@code Integer.MAX_VALUE}.</ul>
     *
     * @param   a   a floating-point value to be rounded to an integer.
     * @return  the value of the argument rounded to the nearest
     *          {@code int} value.
     * @see     java.lang.Integer#MAX_VALUE
     * @see     java.lang.Integer#MIN_VALUE
     */
    public static int round(float a) {
        int intBits = Float.floatToRawIntBits(a);
        int biasedExp = (intBits & FloatConsts.EXP_BIT_MASK)
                >> (FloatConsts.SIGNIFICAND_WIDTH - 1);
        int shift = (FloatConsts.SIGNIFICAND_WIDTH - 2
                + FloatConsts.EXP_BIAS) - biasedExp;
        if ((shift & -32) == 0) { // shift >= 0 && shift < 32
            // a is a finite number such that pow(2,-32) <= ulp(a) < 1
            int r = ((intBits & FloatConsts.SIGNIF_BIT_MASK)
                    | (FloatConsts.SIGNIF_BIT_MASK + 1));
            if (intBits < 0) {
                r = -r;
            }
            // In the comments below each Java expression evaluates to the value
            // the corresponding mathematical expression:
            // (r) evaluates to a / ulp(a)
            // (r >> shift) evaluates to floor(a * 2)
            // ((r >> shift) + 1) evaluates to floor((a + 1/2) * 2)
            // (((r >> shift) + 1) >> 1) evaluates to floor(a + 1/2)
            return ((r >> shift) + 1) >> 1;
        } else {
            // a is either
            // - a finite number with abs(a) < exp(2,FloatConsts.SIGNIFICAND_WIDTH-32) < 1/2
            // - a finite number with ulp(a) >= 1 and hence a is a mathematical integer
            // - an infinity or NaN
            return (int) a;
        }
    }

    /**
     * Returns the closest {@code long} to the argument, with ties
     * rounding to positive infinity.
     *
     * <p>Special cases:
     * <ul><li>If the argument is NaN, the result is 0.
     * <li>If the argument is negative infinity or any value less than or
     * equal to the value of {@code Long.MIN_VALUE}, the result is
     * equal to the value of {@code Long.MIN_VALUE}.
     * <li>If the argument is positive infinity or any value greater than or
     * equal to the value of {@code Long.MAX_VALUE}, the result is
     * equal to the value of {@code Long.MAX_VALUE}.</ul>
     *
     * @param   a   a floating-point value to be rounded to a
     *          {@code long}.
     * @return  the value of the argument rounded to the nearest
     *          {@code long} value.
     * @see     java.lang.Long#MAX_VALUE
     * @see     java.lang.Long#MIN_VALUE
     */
    public static long round(double a) {
        long longBits = Double.doubleToRawLongBits(a);
        long biasedExp = (longBits & DoubleConsts.EXP_BIT_MASK)
                >> (DoubleConsts.SIGNIFICAND_WIDTH - 1);
        long shift = (DoubleConsts.SIGNIFICAND_WIDTH - 2
                + DoubleConsts.EXP_BIAS) - biasedExp;
        if ((shift & -64) == 0) { // shift >= 0 && shift < 64
            // a is a finite number such that pow(2,-64) <= ulp(a) < 1
            long r = ((longBits & DoubleConsts.SIGNIF_BIT_MASK)
                    | (DoubleConsts.SIGNIF_BIT_MASK + 1));
            if (longBits < 0) {
                r = -r;
            }
            // In the comments below each Java expression evaluates to the value
            // the corresponding mathematical expression:
            // (r) evaluates to a / ulp(a)
            // (r >> shift) evaluates to floor(a * 2)
            // ((r >> shift) + 1) evaluates to floor((a + 1/2) * 2)
            // (((r >> shift) + 1) >> 1) evaluates to floor(a + 1/2)
            return ((r >> shift) + 1) >> 1;
        } else {
            // a is either
            // - a finite number with abs(a) < exp(2,DoubleConsts.SIGNIFICAND_WIDTH-64) < 1/2
            // - a finite number with ulp(a) >= 1 and hence a is a mathematical integer
            // - an infinity or NaN
            return (long) a;
        }
    }

    private static final class RandomNumberGeneratorHolder {
        static final Random randomNumberGenerator = new Random();
    }

    /**
     * Returns a {@code double} value with a positive sign, greater
     * than or equal to {@code 0.0} and less than {@code 1.0}.
     * Returned values are chosen pseudorandomly with (approximately)
     * uniform distribution from that range.
     *
     * <p>When this method is first called, it creates a single new
     * pseudorandom-number generator, exactly as if by the expression
     *
     * <blockquote>{@code new java.util.Random()}</blockquote>
     *
     * This new pseudorandom-number generator is used thereafter for
     * all calls to this method and is used nowhere else.
     *
     * <p>This method is properly synchronized to allow correct use by
     * more than one thread. However, if many threads need to generate
     * pseudorandom numbers at a great rate, it may reduce contention
     * for each thread to have its own pseudorandom-number generator.
     *
     * @apiNote
     * As the largest {@code double} value less than {@code 1.0}
     * is {@code Math.nextDown(1.0)}, a value {@code x} in the closed range
     * {@code [x1,x2]} where {@code x1<=x2} may be defined by the statements
     *
     * <blockquote><pre>{@code
     * double f = Math.random()/Math.nextDown(1.0);
     * double x = x1*(1.0 - f) + x2*f;
     * }</pre></blockquote>
     *
     * @return  a pseudorandom {@code double} greater than or equal
     * to {@code 0.0} and less than {@code 1.0}.
     * @see #nextDown(double)
     * @see Random#nextDouble()
     */
    public static double random() {
        return RandomNumberGeneratorHolder.randomNumberGenerator.nextDouble();
    }

    /**
     * Returns the sum of its arguments,
     * throwing an exception if the result overflows an {@code int}.
     *
     * @param x the first value
     * @param y the second value
     * @return the result
     * @throws ArithmeticException if the result overflows an int
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static int addExact(int x, int y) {
        int r = x + y;
        // HD 2-12 Overflow iff both arguments have the opposite sign of the result
        if (((x ^ r) & (y ^ r)) < 0) {
            throw new ArithmeticException("integer overflow");
        }
        return r;
    }

    /**
     * Returns the sum of its arguments,
     * throwing an exception if the result overflows a {@code long}.
     *
     * @param x the first value
     * @param y the second value
     * @return the result
     * @throws ArithmeticException if the result overflows a long
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static long addExact(long x, long y) {
        long r = x + y;
        // HD 2-12 Overflow iff both arguments have the opposite sign of the result
        if (((x ^ r) & (y ^ r)) < 0) {
            throw new ArithmeticException("long overflow");
        }
        return r;
    }

    /**
     * Returns the difference of the arguments,
     * throwing an exception if the result overflows an {@code int}.
     *
     * @param x the first value
     * @param y the second value to subtract from the first
     * @return the result
     * @throws ArithmeticException if the result overflows an int
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static int subtractExact(int x, int y) {
        int r = x - y;
        // HD 2-12 Overflow iff the arguments have different signs and
        // the sign of the result is different from the sign of x
        if (((x ^ y) & (x ^ r)) < 0) {
            throw new ArithmeticException("integer overflow");
        }
        return r;
    }

    /**
     * Returns the difference of the arguments,
     * throwing an exception if the result overflows a {@code long}.
     *
     * @param x the first value
     * @param y the second value to subtract from the first
     * @return the result
     * @throws ArithmeticException if the result overflows a long
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static long subtractExact(long x, long y) {
        long r = x - y;
        // HD 2-12 Overflow iff the arguments have different signs and
        // the sign of the result is different from the sign of x
        if (((x ^ y) & (x ^ r)) < 0) {
            throw new ArithmeticException("long overflow");
        }
        return r;
    }

    /**
     * Returns the product of the arguments,
     * throwing an exception if the result overflows an {@code int}.
     *
     * @param x the first value
     * @param y the second value
     * @return the result
     * @throws ArithmeticException if the result overflows an int
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static int multiplyExact(int x, int y) {
        long r = (long)x * (long)y;
        if ((int)r != r) {
            throw new ArithmeticException("integer overflow");
        }
        return (int)r;
    }

    /**
     * Returns the product of the arguments, throwing an exception if the result
     * overflows a {@code long}.
     *
     * @param x the first value
     * @param y the second value
     * @return the result
     * @throws ArithmeticException if the result overflows a long
     * @since 9
     */
    public static long multiplyExact(long x, int y) {
        return multiplyExact(x, (long)y);
    }

    /**
     * Returns the product of the arguments,
     * throwing an exception if the result overflows a {@code long}.
     *
     * @param x the first value
     * @param y the second value
     * @return the result
     * @throws ArithmeticException if the result overflows a long
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static long multiplyExact(long x, long y) {
        long r = x * y;
        long ax = Math.abs(x);
        long ay = Math.abs(y);
        if (((ax | ay) >>> 31 != 0)) {
            // Some bits greater than 2^31 that might cause overflow
            // Check the result using the divide operator
            // and check for the special case of Long.MIN_VALUE * -1
           if (((y != 0) && (r / y != x)) ||
               (x == Long.MIN_VALUE && y == -1)) {
                throw new ArithmeticException("long overflow");
            }
        }
        return r;
    }

    /**
     * Returns the argument incremented by one, throwing an exception if the
     * result overflows an {@code int}.
     *
     * @param a the value to increment
     * @return the result
     * @throws ArithmeticException if the result overflows an int
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static int incrementExact(int a) {
        if (a == Integer.MAX_VALUE) {
            throw new ArithmeticException("integer overflow");
        }

        return a + 1;
    }

    /**
     * Returns the argument incremented by one, throwing an exception if the
     * result overflows a {@code long}.
     *
     * @param a the value to increment
     * @return the result
     * @throws ArithmeticException if the result overflows a long
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static long incrementExact(long a) {
        if (a == Long.MAX_VALUE) {
            throw new ArithmeticException("long overflow");
        }

        return a + 1L;
    }

    /**
     * Returns the argument decremented by one, throwing an exception if the
     * result overflows an {@code int}.
     *
     * @param a the value to decrement
     * @return the result
     * @throws ArithmeticException if the result overflows an int
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static int decrementExact(int a) {
        if (a == Integer.MIN_VALUE) {
            throw new ArithmeticException("integer overflow");
        }

        return a - 1;
    }

    /**
     * Returns the argument decremented by one, throwing an exception if the
     * result overflows a {@code long}.
     *
     * @param a the value to decrement
     * @return the result
     * @throws ArithmeticException if the result overflows a long
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static long decrementExact(long a) {
        if (a == Long.MIN_VALUE) {
            throw new ArithmeticException("long overflow");
        }

        return a - 1L;
    }

    /**
     * Returns the negation of the argument, throwing an exception if the
     * result overflows an {@code int}.
     *
     * @param a the value to negate
     * @return the result
     * @throws ArithmeticException if the result overflows an int
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static int negateExact(int a) {
        if (a == Integer.MIN_VALUE) {
            throw new ArithmeticException("integer overflow");
        }

        return -a;
    }

    /**
     * Returns the negation of the argument, throwing an exception if the
     * result overflows a {@code long}.
     *
     * @param a the value to negate
     * @return the result
     * @throws ArithmeticException if the result overflows a long
     * @since 1.8
     */
    @HotSpotIntrinsicCandidate
    public static long negateExact(long a) {
        if (a == Long.MIN_VALUE) {
            throw new ArithmeticException("long overflow");
        }

        return -a;
    }

    /**
     * Returns the value of the {@code long} argument;
     * throwing an exception if the value overflows an {@code int}.
     *
     * @param value the long value
     * @return the argument as an int
     * @throws ArithmeticException if the {@code argument} overflows an int
     * @since 1.8
     */
    public static int toIntExact(long value) {
        if ((int)value != value) {
            throw new ArithmeticException("integer overflow");
        }
        return (int)value;
    }

    /**
     * Returns the exact mathematical product of the arguments.
     *
     * @param x the first value
     * @param y the second value
     * @return the result
     * @since 9
     */
    public static long multiplyFull(int x, int y) {
        return (long)x * (long)y;
    }

    /**
     * Returns as a {@code long} the most significant 64 bits of the 128-bit
     * product of two 64-bit factors.
     *
     * @param x the first value
     * @param y the second value
     * @return the result
     * @since 9
     */
    @HotSpotIntrinsicCandidate
    public static long multiplyHigh(long x, long y) {
        if (x < 0 || y < 0) {
            // Use technique from section 8-2 of Henry S. Warren, Jr.,
            // Hacker's Delight (2nd ed.) (Addison Wesley, 2013), 173-174.
            long x1 = x >> 32;
            long x2 = x & 0xFFFFFFFFL;
            long y1 = y >> 32;
            long y2 = y & 0xFFFFFFFFL;
            long z2 = x2 * y2;
            long t = x1 * y2 + (z2 >>> 32);
            long z1 = t & 0xFFFFFFFFL;
            long z0 = t >> 32;
            z1 += x2 * y1;
            return x1 * y1 + z0 + (z1 >> 32);
        } else {
            // Use Karatsuba technique with two base 2^32 digits.
            long x1 = x >>> 32;
            long y1 = y >>> 32;
            long x2 = x & 0xFFFFFFFFL;
            long y2 = y & 0xFFFFFFFFL;
            long A = x1 * y1;
            long B = x2 * y2;
            long C = (x1 + x2) * (y1 + y2);
            long K = C - A - B;
            return (((B >>> 32) + K) >>> 32) + A;
        }
    }

    /**
     * Returns the largest (closest to positive infinity)
     * {@code int} value that is less than or equal to the algebraic quotient.
     * There is one special case, if the dividend is the
     * {@linkplain Integer#MIN_VALUE Integer.MIN_VALUE} and the divisor is {@code -1},
     * then integer overflow occurs and
     * the result is equal to {@code Integer.MIN_VALUE}.
     * <p>
     * Normal integer division operates under the round to zero rounding mode
     * (truncation).  This operation instead acts under the round toward
     * negative infinity (floor) rounding mode.
     * The floor rounding mode gives different results from truncation
     * when the exact result is negative.
     * <ul>
     *   <li>If the signs of the arguments are the same, the results of
     *       {@code floorDiv} and the {@code /} operator are the same.  <br>
     *       For example, {@code floorDiv(4, 3) == 1} and {@code (4 / 3) == 1}.</li>
     *   <li>If the signs of the arguments are different,  the quotient is negative and
     *       {@code floorDiv} returns the integer less than or equal to the quotient
     *       and the {@code /} operator returns the integer closest to zero.<br>
     *       For example, {@code floorDiv(-4, 3) == -2},
     *       whereas {@code (-4 / 3) == -1}.
     *   </li>
     * </ul>
     *
     * @param x the dividend
     * @param y the divisor
     * @return the largest (closest to positive infinity)
     * {@code int} value that is less than or equal to the algebraic quotient.
     * @throws ArithmeticException if the divisor {@code y} is zero
     * @see #floorMod(int, int)
     * @see #floor(double)
     * @since 1.8
     */
    public static int floorDiv(int x, int y) {
        int r = x / y;
        // if the signs are different and modulo not zero, round down
        if ((x ^ y) < 0 && (r * y != x)) {
            r--;
        }
        return r;
    }

    /**
     * Returns the largest (closest to positive infinity)
     * {@code long} value that is less than or equal to the algebraic quotient.
     * There is one special case, if the dividend is the
     * {@linkplain Long#MIN_VALUE Long.MIN_VALUE} and the divisor is {@code -1},
     * then integer overflow occurs and
     * the result is equal to {@code Long.MIN_VALUE}.
     * <p>
     * Normal integer division operates under the round to zero rounding mode
     * (truncation).  This operation instead acts under the round toward
     * negative infinity (floor) rounding mode.
     * The floor rounding mode gives different results from truncation
     * when the exact result is negative.
     * <p>
     * For examples, see {@link #floorDiv(int, int)}.
     *
     * @param x the dividend
     * @param y the divisor
     * @return the largest (closest to positive infinity)
     * {@code int} value that is less than or equal to the algebraic quotient.
     * @throws ArithmeticException if the divisor {@code y} is zero
     * @see #floorMod(long, int)
     * @see #floor(double)
     * @since 9
     */
    public static long floorDiv(long x, int y) {
        return floorDiv(x, (long)y);
    }

    /**
     * Returns the largest (closest to positive infinity)
     * {@code long} value that is less than or equal to the algebraic quotient.
     * There is one special case, if the dividend is the
     * {@linkplain Long#MIN_VALUE Long.MIN_VALUE} and the divisor is {@code -1},
     * then integer overflow occurs and
     * the result is equal to {@code Long.MIN_VALUE}.
     * <p>
     * Normal integer division operates under the round to zero rounding mode
     * (truncation).  This operation instead acts under the round toward
     * negative infinity (floor) rounding mode.
     * The floor rounding mode gives different results from truncation
     * when the exact result is negative.
     * <p>
     * For examples, see {@link #floorDiv(int, int)}.
     *
     * @param x the dividend
     * @param y the divisor
     * @return the largest (closest to positive infinity)
     * {@code long} value that is less than or equal to the algebraic quotient.
     * @throws ArithmeticException if the divisor {@code y} is zero
     * @see #floorMod(long, long)
     * @see #floor(double)
     * @since 1.8
     */
    public static long floorDiv(long x, long y) {
        long r = x / y;
        // if the signs are different and modulo not zero, round down
        if ((x ^ y) < 0 && (r * y != x)) {
            r--;
        }
        return r;
    }

    /**
     * Returns the floor modulus of the {@code int} arguments.
     * <p>
     * The floor modulus is {@code x - (floorDiv(x, y) * y)},
     * has the same sign as the divisor {@code y}, and
     * is in the range of {@code -abs(y) < r < +abs(y)}.
     *
     * <p>
     * The relationship between {@code floorDiv} and {@code floorMod} is such that:
     * <ul>
     *   <li>{@code floorDiv(x, y) * y + floorMod(x, y) == x}
     * </ul>
     * <p>
     * The difference in values between {@code floorMod} and
     * the {@code %} operator is due to the difference between
     * {@code floorDiv} that returns the integer less than or equal to the quotient
     * and the {@code /} operator that returns the integer closest to zero.
     * <p>
     * Examples:
     * <ul>
     *   <li>If the signs of the arguments are the same, the results
     *       of {@code floorMod} and the {@code %} operator are the same.  <br>
     *       <ul>
     *       <li>{@code floorMod(4, 3) == 1}; &nbsp; and {@code (4 % 3) == 1}</li>
     *       </ul>
     *   <li>If the signs of the arguments are different, the results differ from the {@code %} operator.<br>
     *      <ul>
     *      <li>{@code floorMod(+4, -3) == -2}; &nbsp; and {@code (+4 % -3) == +1} </li>
     *      <li>{@code floorMod(-4, +3) == +2}; &nbsp; and {@code (-4 % +3) == -1} </li>
     *      <li>{@code floorMod(-4, -3) == -1}; &nbsp; and {@code (-4 % -3) == -1 } </li>
     *      </ul>
     *   </li>
     * </ul>
     * <p>
     * If the signs of arguments are unknown and a positive modulus
     * is needed it can be computed as {@code (floorMod(x, y) + abs(y)) % abs(y)}.
     *
     * @param x the dividend
     * @param y the divisor
     * @return the floor modulus {@code x - (floorDiv(x, y) * y)}
     * @throws ArithmeticException if the divisor {@code y} is zero
     * @see #floorDiv(int, int)
     * @since 1.8
     */
    public static int floorMod(int x, int y) {
        return x - floorDiv(x, y) * y;
    }

    /**
     * Returns the floor modulus of the {@code long} and {@code int} arguments.
     * <p>
     * The floor modulus is {@code x - (floorDiv(x, y) * y)},
     * has the same sign as the divisor {@code y}, and
     * is in the range of {@code -abs(y) < r < +abs(y)}.
     *
     * <p>
     * The relationship between {@code floorDiv} and {@code floorMod} is such that:
     * <ul>
     *   <li>{@code floorDiv(x, y) * y + floorMod(x, y) == x}
     * </ul>
     * <p>
     * For examples, see {@link #floorMod(int, int)}.
     *
     * @param x the dividend
     * @param y the divisor
     * @return the floor modulus {@code x - (floorDiv(x, y) * y)}
     * @throws ArithmeticException if the divisor {@code y} is zero
     * @see #floorDiv(long, int)
     * @since 9
     */
    public static int floorMod(long x, int y) {
        // Result cannot overflow the range of int.
        return (int)(x - floorDiv(x, y) * y);
    }

    /**
     * Returns the floor modulus of the {@code long} arguments.
     * <p>
     * The floor modulus is {@code x - (floorDiv(x, y) * y)},
     * has the same sign as the divisor {@code y}, and
     * is in the range of {@code -abs(y) < r < +abs(y)}.
     *
     * <p>
     * The relationship between {@code floorDiv} and {@code floorMod} is such that:
     * <ul>
     *   <li>{@code floorDiv(x, y) * y + floorMod(x, y) == x}
     * </ul>
     * <p>
     * For examples, see {@link #floorMod(int, int)}.
     *
     * @param x the dividend
     * @param y the divisor
     * @return the floor modulus {@code x - (floorDiv(x, y) * y)}
     * @throws ArithmeticException if the divisor {@code y} is zero
     * @see #floorDiv(long, long)
     * @since 1.8
     */
    public static long floorMod(long x, long y) {
        return x - floorDiv(x, y) * y;
    }

    /**
     * Returns the absolute value of an {@code int} value.
     * If the argument is not negative, the argument is returned.
     * If the argument is negative, the negation of the argument is returned.
     *
     * <p>Note that if the argument is equal to the value of
     * {@link Integer#MIN_VALUE}, the most negative representable
     * {@code int} value, the result is that same value, which is
     * negative.
     *
     * @param   a   the argument whose absolute value is to be determined
     * @return  the absolute value of the argument.
     */
    public static int abs(int a) {
        return (a < 0) ? -a : a;
    }

    /**
     * Returns the absolute value of a {@code long} value.
     * If the argument is not negative, the argument is returned.
     * If the argument is negative, the negation of the argument is returned.
     *
     * <p>Note that if the argument is equal to the value of
     * {@link Long#MIN_VALUE}, the most negative representable
     * {@code long} value, the result is that same value, which
     * is negative.
     *
     * @param   a   the argument whose absolute value is to be determined
     * @return  the absolute value of the argument.
     */
    public static long abs(long a) {
        return (a < 0) ? -a : a;
    }

    /**
     * Returns the absolute value of a {@code float} value.
     * If the argument is not negative, the argument is returned.
     * If the argument is negative, the negation of the argument is returned.
     * Special cases:
     * <ul><li>If the argument is positive zero or negative zero, the
     * result is positive zero.
     * <li>If the argument is infinite, the result is positive infinity.
     * <li>If the argument is NaN, the result is NaN.</ul>
     *
     * @apiNote As implied by the above, one valid implementation of
     * this method is given by the expression below which computes a
     * {@code float} with the same exponent and significand as the
     * argument but with a guaranteed zero sign bit indicating a
     * positive value:<br>
     * {@code Float.intBitsToFloat(0x7fffffff & Float.floatToRawIntBits(a))}
     *
     * @param   a   the argument whose absolute value is to be determined
     * @return  the absolute value of the argument.
     */
    public static float abs(float a) {
        return (a <= 0.0F) ? 0.0F - a : a;
    }

    /**
     * Returns the absolute value of a {@code double} value.
     * If the argument is not negative, the argument is returned.
     * If the argument is negative, the negation of the argument is returned.
     * Special cases:
     * <ul><li>If the argument is positive zero or negative zero, the result
     * is positive zero.
     * <li>If the argument is infinite, the result is positive infinity.
     * <li>If the argument is NaN, the result is NaN.</ul>
     *
     * @apiNote As implied by the above, one valid implementation of
     * this method is given by the expression below which computes a
     * {@code double} with the same exponent and significand as the
     * argument but with a guaranteed zero sign bit indicating a
     * positive value:<br>
     * {@code Double.longBitsToDouble((Double.doubleToRawLongBits(a)<<1)>>>1)}
     *
     * @param   a   the argument whose absolute value is to be determined
     * @return  the absolute value of the argument.
     */
    @HotSpotIntrinsicCandidate
    public static double abs(double a) {
        return (a <= 0.0D) ? 0.0D - a : a;
    }

    /**
     * Returns the greater of two {@code int} values. That is, the
     * result is the argument closer to the value of
     * {@link Integer#MAX_VALUE}. If the arguments have the same value,
     * the result is that same value.
     *
     * @param   a   an argument.
     * @param   b   another argument.
     * @return  the larger of {@code a} and {@code b}.
     */
    @HotSpotIntrinsicCandidate
    public static int max(int a, int b) {
        return (a >= b) ? a : b;
    }

    /**
     * Returns the greater of two {@code long} values. That is, the
     * result is the argument closer to the value of
     * {@link Long#MAX_VALUE}. If the arguments have the same value,
     * the result is that same value.
     *
     * @param   a   an argument.
     * @param   b   another argument.
     * @return  the larger of {@code a} and {@code b}.
     */
    public static long max(long a, long b) {
        return (a >= b) ? a : b;
    }

    // Use raw bit-wise conversions on guaranteed non-NaN arguments.
    private static final long negativeZeroFloatBits  = Float.floatToRawIntBits(-0.0f);
    private static final long negativeZeroDoubleBits = Double.doubleToRawLongBits(-0.0d);

    /**
     * Returns the greater of two {@code float} values.  That is,
