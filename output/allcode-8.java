     *
     * @param      s   the {@code String} containing the integer
     *                  representation to be parsed
     * @param      radix   the radix to be used while parsing {@code s}.
     * @return     the integer represented by the string argument in the
     *             specified radix.
     * @exception  NumberFormatException if the {@code String}
     *             does not contain a parsable {@code int}.
     */
    public static int parseInt(String s, int radix)
                throws NumberFormatException
    {
        /*
         * WARNING: This method may be invoked early during VM initialization
         * before IntegerCache is initialized. Care must be taken to not use
         * the valueOf method.
         */

        if (s == null) {
            throw new NumberFormatException("null");
        }

        if (radix < Character.MIN_RADIX) {
            throw new NumberFormatException("radix " + radix +
                                            " less than Character.MIN_RADIX");
        }

        if (radix > Character.MAX_RADIX) {
            throw new NumberFormatException("radix " + radix +
                                            " greater than Character.MAX_RADIX");
        }

        boolean negative = false;
        int i = 0, len = s.length();
        int limit = -Integer.MAX_VALUE;

        if (len > 0) {
            char firstChar = s.charAt(0);
            if (firstChar < '0') { // Possible leading "+" or "-"
                if (firstChar == '-') {
                    negative = true;
                    limit = Integer.MIN_VALUE;
                } else if (firstChar != '+') {
                    throw NumberFormatException.forInputString(s);
                }

                if (len == 1) { // Cannot have lone "+" or "-"
                    throw NumberFormatException.forInputString(s);
                }
                i++;
            }
            int multmin = limit / radix;
            int result = 0;
            while (i < len) {
                // Accumulating negatively avoids surprises near MAX_VALUE
                int digit = Character.digit(s.charAt(i++), radix);
                if (digit < 0 || result < multmin) {
                    throw NumberFormatException.forInputString(s);
                }
                result *= radix;
                if (result < limit + digit) {
                    throw NumberFormatException.forInputString(s);
                }
                result -= digit;
            }
            return negative ? result : -result;
        } else {
            throw NumberFormatException.forInputString(s);
        }
    }

    /**
     * Parses the {@link CharSequence} argument as a signed {@code int} in the
     * specified {@code radix}, beginning at the specified {@code beginIndex}
     * and extending to {@code endIndex - 1}.
     *
     * <p>The method does not take steps to guard against the
     * {@code CharSequence} being mutated while parsing.
     *
     * @param      s   the {@code CharSequence} containing the {@code int}
     *                  representation to be parsed
     * @param      beginIndex   the beginning index, inclusive.
     * @param      endIndex     the ending index, exclusive.
     * @param      radix   the radix to be used while parsing {@code s}.
     * @return     the signed {@code int} represented by the subsequence in
     *             the specified radix.
     * @throws     NullPointerException  if {@code s} is null.
     * @throws     IndexOutOfBoundsException  if {@code beginIndex} is
     *             negative, or if {@code beginIndex} is greater than
     *             {@code endIndex} or if {@code endIndex} is greater than
     *             {@code s.length()}.
     * @throws     NumberFormatException  if the {@code CharSequence} does not
     *             contain a parsable {@code int} in the specified
     *             {@code radix}, or if {@code radix} is either smaller than
     *             {@link java.lang.Character#MIN_RADIX} or larger than
     *             {@link java.lang.Character#MAX_RADIX}.
     * @since  9
     */
    public static int parseInt(CharSequence s, int beginIndex, int endIndex, int radix)
                throws NumberFormatException {
        s = Objects.requireNonNull(s);

        if (beginIndex < 0 || beginIndex > endIndex || endIndex > s.length()) {
            throw new IndexOutOfBoundsException();
        }
        if (radix < Character.MIN_RADIX) {
            throw new NumberFormatException("radix " + radix +
                                            " less than Character.MIN_RADIX");
        }
        if (radix > Character.MAX_RADIX) {
            throw new NumberFormatException("radix " + radix +
                                            " greater than Character.MAX_RADIX");
        }

        boolean negative = false;
        int i = beginIndex;
        int limit = -Integer.MAX_VALUE;

        if (i < endIndex) {
            char firstChar = s.charAt(i);
            if (firstChar < '0') { // Possible leading "+" or "-"
                if (firstChar == '-') {
                    negative = true;
                    limit = Integer.MIN_VALUE;
                } else if (firstChar != '+') {
                    throw NumberFormatException.forCharSequence(s, beginIndex,
                            endIndex, i);
                }
                i++;
                if (i == endIndex) { // Cannot have lone "+" or "-"
                    throw NumberFormatException.forCharSequence(s, beginIndex,
                            endIndex, i);
                }
            }
            int multmin = limit / radix;
            int result = 0;
            while (i < endIndex) {
                // Accumulating negatively avoids surprises near MAX_VALUE
                int digit = Character.digit(s.charAt(i), radix);
                if (digit < 0 || result < multmin) {
                    throw NumberFormatException.forCharSequence(s, beginIndex,
                            endIndex, i);
                }
                result *= radix;
                if (result < limit + digit) {
                    throw NumberFormatException.forCharSequence(s, beginIndex,
                            endIndex, i);
                }
                i++;
                result -= digit;
            }
            return negative ? result : -result;
        } else {
            throw NumberFormatException.forInputString("");
        }
    }

    /**
     * Parses the string argument as a signed decimal integer. The
     * characters in the string must all be decimal digits, except
     * that the first character may be an ASCII minus sign {@code '-'}
     * ({@code '\u005Cu002D'}) to indicate a negative value or an
     * ASCII plus sign {@code '+'} ({@code '\u005Cu002B'}) to
     * indicate a positive value. The resulting integer value is
     * returned, exactly as if the argument and the radix 10 were
     * given as arguments to the {@link #parseInt(java.lang.String,
     * int)} method.
     *
     * @param s    a {@code String} containing the {@code int}
     *             representation to be parsed
     * @return     the integer value represented by the argument in decimal.
     * @exception  NumberFormatException  if the string does not contain a
     *               parsable integer.
     */
    public static int parseInt(String s) throws NumberFormatException {
        return parseInt(s,10);
    }

    /**
     * Parses the string argument as an unsigned integer in the radix
     * specified by the second argument.  An unsigned integer maps the
     * values usually associated with negative numbers to positive
     * numbers larger than {@code MAX_VALUE}.
     *
     * The characters in the string must all be digits of the
     * specified radix (as determined by whether {@link
     * java.lang.Character#digit(char, int)} returns a nonnegative
     * value), except that the first character may be an ASCII plus
     * sign {@code '+'} ({@code '\u005Cu002B'}). The resulting
     * integer value is returned.
     *
     * <p>An exception of type {@code NumberFormatException} is
     * thrown if any of the following situations occurs:
     * <ul>
     * <li>The first argument is {@code null} or is a string of
     * length zero.
     *
     * <li>The radix is either smaller than
     * {@link java.lang.Character#MIN_RADIX} or
     * larger than {@link java.lang.Character#MAX_RADIX}.
     *
     * <li>Any character of the string is not a digit of the specified
     * radix, except that the first character may be a plus sign
     * {@code '+'} ({@code '\u005Cu002B'}) provided that the
     * string is longer than length 1.
     *
     * <li>The value represented by the string is larger than the
     * largest unsigned {@code int}, 2<sup>32</sup>-1.
     *
     * </ul>
     *
     *
     * @param      s   the {@code String} containing the unsigned integer
     *                  representation to be parsed
     * @param      radix   the radix to be used while parsing {@code s}.
     * @return     the integer represented by the string argument in the
     *             specified radix.
     * @throws     NumberFormatException if the {@code String}
     *             does not contain a parsable {@code int}.
     * @since 1.8
     */
    public static int parseUnsignedInt(String s, int radix)
                throws NumberFormatException {
        if (s == null)  {
            throw new NumberFormatException("null");
        }

        int len = s.length();
        if (len > 0) {
            char firstChar = s.charAt(0);
            if (firstChar == '-') {
                throw new
                    NumberFormatException(String.format("Illegal leading minus sign " +
                                                       "on unsigned string %s.", s));
            } else {
                if (len <= 5 || // Integer.MAX_VALUE in Character.MAX_RADIX is 6 digits
                    (radix == 10 && len <= 9) ) { // Integer.MAX_VALUE in base 10 is 10 digits
                    return parseInt(s, radix);
                } else {
                    long ell = Long.parseLong(s, radix);
                    if ((ell & 0xffff_ffff_0000_0000L) == 0) {
                        return (int) ell;
                    } else {
                        throw new
                            NumberFormatException(String.format("String value %s exceeds " +
                                                                "range of unsigned int.", s));
                    }
                }
            }
        } else {
            throw NumberFormatException.forInputString(s);
        }
    }

    /**
     * Parses the {@link CharSequence} argument as an unsigned {@code int} in
     * the specified {@code radix}, beginning at the specified
     * {@code beginIndex} and extending to {@code endIndex - 1}.
     *
     * <p>The method does not take steps to guard against the
     * {@code CharSequence} being mutated while parsing.
     *
     * @param      s   the {@code CharSequence} containing the unsigned
     *                 {@code int} representation to be parsed
     * @param      beginIndex   the beginning index, inclusive.
     * @param      endIndex     the ending index, exclusive.
     * @param      radix   the radix to be used while parsing {@code s}.
     * @return     the unsigned {@code int} represented by the subsequence in
     *             the specified radix.
     * @throws     NullPointerException  if {@code s} is null.
     * @throws     IndexOutOfBoundsException  if {@code beginIndex} is
     *             negative, or if {@code beginIndex} is greater than
     *             {@code endIndex} or if {@code endIndex} is greater than
     *             {@code s.length()}.
     * @throws     NumberFormatException  if the {@code CharSequence} does not
     *             contain a parsable unsigned {@code int} in the specified
     *             {@code radix}, or if {@code radix} is either smaller than
     *             {@link java.lang.Character#MIN_RADIX} or larger than
     *             {@link java.lang.Character#MAX_RADIX}.
     * @since  9
     */
    public static int parseUnsignedInt(CharSequence s, int beginIndex, int endIndex, int radix)
                throws NumberFormatException {
        s = Objects.requireNonNull(s);

        if (beginIndex < 0 || beginIndex > endIndex || endIndex > s.length()) {
            throw new IndexOutOfBoundsException();
        }
        int start = beginIndex, len = endIndex - beginIndex;

        if (len > 0) {
            char firstChar = s.charAt(start);
            if (firstChar == '-') {
                throw new
                    NumberFormatException(String.format("Illegal leading minus sign " +
                                                       "on unsigned string %s.", s));
            } else {
                if (len <= 5 || // Integer.MAX_VALUE in Character.MAX_RADIX is 6 digits
                        (radix == 10 && len <= 9)) { // Integer.MAX_VALUE in base 10 is 10 digits
                    return parseInt(s, start, start + len, radix);
                } else {
                    long ell = Long.parseLong(s, start, start + len, radix);
                    if ((ell & 0xffff_ffff_0000_0000L) == 0) {
                        return (int) ell;
                    } else {
                        throw new
                            NumberFormatException(String.format("String value %s exceeds " +
                                                                "range of unsigned int.", s));
                    }
                }
            }
        } else {
            throw new NumberFormatException("");
        }
    }

    /**
     * Parses the string argument as an unsigned decimal integer. The
     * characters in the string must all be decimal digits, except
     * that the first character may be an ASCII plus sign {@code
     * '+'} ({@code '\u005Cu002B'}). The resulting integer value
     * is returned, exactly as if the argument and the radix 10 were
     * given as arguments to the {@link
     * #parseUnsignedInt(java.lang.String, int)} method.
     *
     * @param s   a {@code String} containing the unsigned {@code int}
     *            representation to be parsed
     * @return    the unsigned integer value represented by the argument in decimal.
     * @throws    NumberFormatException  if the string does not contain a
     *            parsable unsigned integer.
     * @since 1.8
     */
    public static int parseUnsignedInt(String s) throws NumberFormatException {
        return parseUnsignedInt(s, 10);
    }

    /**
     * Returns an {@code Integer} object holding the value
     * extracted from the specified {@code String} when parsed
     * with the radix given by the second argument. The first argument
     * is interpreted as representing a signed integer in the radix
     * specified by the second argument, exactly as if the arguments
     * were given to the {@link #parseInt(java.lang.String, int)}
     * method. The result is an {@code Integer} object that
     * represents the integer value specified by the string.
     *
     * <p>In other words, this method returns an {@code Integer}
     * object equal to the value of:
     *
     * <blockquote>
     *  {@code new Integer(Integer.parseInt(s, radix))}
     * </blockquote>
     *
     * @param      s   the string to be parsed.
     * @param      radix the radix to be used in interpreting {@code s}
     * @return     an {@code Integer} object holding the value
     *             represented by the string argument in the specified
     *             radix.
     * @exception NumberFormatException if the {@code String}
     *            does not contain a parsable {@code int}.
     */
    public static Integer valueOf(String s, int radix) throws NumberFormatException {
        return Integer.valueOf(parseInt(s,radix));
    }

    /**
     * Returns an {@code Integer} object holding the
     * value of the specified {@code String}. The argument is
     * interpreted as representing a signed decimal integer, exactly
     * as if the argument were given to the {@link
     * #parseInt(java.lang.String)} method. The result is an
     * {@code Integer} object that represents the integer value
     * specified by the string.
     *
     * <p>In other words, this method returns an {@code Integer}
     * object equal to the value of:
     *
     * <blockquote>
     *  {@code new Integer(Integer.parseInt(s))}
     * </blockquote>
     *
     * @param      s   the string to be parsed.
     * @return     an {@code Integer} object holding the value
     *             represented by the string argument.
     * @exception  NumberFormatException  if the string cannot be parsed
     *             as an integer.
     */
    public static Integer valueOf(String s) throws NumberFormatException {
        return Integer.valueOf(parseInt(s, 10));
    }

    /**
     * Cache to support the object identity semantics of autoboxing for values between
     * -128 and 127 (inclusive) as required by JLS.
     *
     * The cache is initialized on first usage.  The size of the cache
     * may be controlled by the {@code -XX:AutoBoxCacheMax=<size>} option.
     * During VM initialization, java.lang.Integer.IntegerCache.high property
     * may be set and saved in the private system properties in the
     * jdk.internal.misc.VM class.
     */

    private static class IntegerCache {
        static final int low = -128;
        static final int high;
        static final Integer cache[];

        static {
            // high value may be configured by property
            int h = 127;
            String integerCacheHighPropValue =
                VM.getSavedProperty("java.lang.Integer.IntegerCache.high");
            if (integerCacheHighPropValue != null) {
                try {
                    int i = parseInt(integerCacheHighPropValue);
                    i = Math.max(i, 127);
                    // Maximum array size is Integer.MAX_VALUE
                    h = Math.min(i, Integer.MAX_VALUE - (-low) -1);
                } catch( NumberFormatException nfe) {
                    // If the property cannot be parsed into an int, ignore it.
                }
            }
            high = h;

            cache = new Integer[(high - low) + 1];
            int j = low;
            for(int k = 0; k < cache.length; k++)
                cache[k] = new Integer(j++);

            // range [-128, 127] must be interned (JLS7 5.1.7)
            assert IntegerCache.high >= 127;
        }

        private IntegerCache() {}
    }

    /**
     * Returns an {@code Integer} instance representing the specified
     * {@code int} value.  If a new {@code Integer} instance is not
     * required, this method should generally be used in preference to
     * the constructor {@link #Integer(int)}, as this method is likely
     * to yield significantly better space and time performance by
     * caching frequently requested values.
     *
     * This method will always cache values in the range -128 to 127,
     * inclusive, and may cache other values outside of this range.
     *
     * @param  i an {@code int} value.
     * @return an {@code Integer} instance representing {@code i}.
     * @since  1.5
     */
    @HotSpotIntrinsicCandidate
    public static Integer valueOf(int i) {
        if (i >= IntegerCache.low && i <= IntegerCache.high)
            return IntegerCache.cache[i + (-IntegerCache.low)];
        return new Integer(i);
    }

    /**
     * The value of the {@code Integer}.
     *
     * @serial
     */
    private final int value;

    /**
     * Constructs a newly allocated {@code Integer} object that
     * represents the specified {@code int} value.
     *
     * @param   value   the value to be represented by the
     *                  {@code Integer} object.
     *
     * @deprecated
     * It is rarely appropriate to use this constructor. The static factory
     * {@link #valueOf(int)} is generally a better choice, as it is
     * likely to yield significantly better space and time performance.
     */
    @Deprecated(since="9")
    public Integer(int value) {
        this.value = value;
    }

    /**
     * Constructs a newly allocated {@code Integer} object that
     * represents the {@code int} value indicated by the
     * {@code String} parameter. The string is converted to an
     * {@code int} value in exactly the manner used by the
     * {@code parseInt} method for radix 10.
     *
     * @param   s   the {@code String} to be converted to an {@code Integer}.
     * @throws      NumberFormatException if the {@code String} does not
     *              contain a parsable integer.
     *
     * @deprecated
     * It is rarely appropriate to use this constructor.
     * Use {@link #parseInt(String)} to convert a string to a
     * {@code int} primitive, or use {@link #valueOf(String)}
     * to convert a string to an {@code Integer} object.
     */
    @Deprecated(since="9")
    public Integer(String s) throws NumberFormatException {
        this.value = parseInt(s, 10);
    }

    /**
     * Returns the value of this {@code Integer} as a {@code byte}
     * after a narrowing primitive conversion.
     * @jls 5.1.3 Narrowing Primitive Conversions
     */
    public byte byteValue() {
        return (byte)value;
    }

    /**
     * Returns the value of this {@code Integer} as a {@code short}
     * after a narrowing primitive conversion.
     * @jls 5.1.3 Narrowing Primitive Conversions
     */
    public short shortValue() {
        return (short)value;
    }

    /**
     * Returns the value of this {@code Integer} as an
     * {@code int}.
     */
    @HotSpotIntrinsicCandidate
    public int intValue() {
        return value;
    }

    /**
     * Returns the value of this {@code Integer} as a {@code long}
     * after a widening primitive conversion.
     * @jls 5.1.2 Widening Primitive Conversions
     * @see Integer#toUnsignedLong(int)
     */
    public long longValue() {
        return (long)value;
    }

    /**
     * Returns the value of this {@code Integer} as a {@code float}
     * after a widening primitive conversion.
     * @jls 5.1.2 Widening Primitive Conversions
     */
    public float floatValue() {
        return (float)value;
    }

    /**
     * Returns the value of this {@code Integer} as a {@code double}
     * after a widening primitive conversion.
     * @jls 5.1.2 Widening Primitive Conversions
     */
    public double doubleValue() {
        return (double)value;
    }

    /**
     * Returns a {@code String} object representing this
     * {@code Integer}'s value. The value is converted to signed
     * decimal representation and returned as a string, exactly as if
     * the integer value were given as an argument to the {@link
     * java.lang.Integer#toString(int)} method.
     *
     * @return  a string representation of the value of this object in
     *          base&nbsp;10.
     */
    public String toString() {
        return toString(value);
    }

    /**
     * Returns a hash code for this {@code Integer}.
     *
     * @return  a hash code value for this object, equal to the
     *          primitive {@code int} value represented by this
     *          {@code Integer} object.
     */
    @Override
    public int hashCode() {
        return Integer.hashCode(value);
    }

    /**
     * Returns a hash code for an {@code int} value; compatible with
     * {@code Integer.hashCode()}.
     *
     * @param value the value to hash
     * @since 1.8
     *
     * @return a hash code value for an {@code int} value.
     */
    public static int hashCode(int value) {
        return value;
    }

    /**
     * Compares this object to the specified object.  The result is
     * {@code true} if and only if the argument is not
     * {@code null} and is an {@code Integer} object that
     * contains the same {@code int} value as this object.
     *
     * @param   obj   the object to compare with.
     * @return  {@code true} if the objects are the same;
     *          {@code false} otherwise.
     */
    public boolean equals(Object obj) {
        if (obj instanceof Integer) {
            return value == ((Integer)obj).intValue();
        }
        return false;
    }

    /**
     * Determines the integer value of the system property with the
     * specified name.
     *
     * <p>The first argument is treated as the name of a system
     * property.  System properties are accessible through the {@link
     * java.lang.System#getProperty(java.lang.String)} method. The
     * string value of this property is then interpreted as an integer
     * value using the grammar supported by {@link Integer#decode decode} and
     * an {@code Integer} object representing this value is returned.
     *
     * <p>If there is no property with the specified name, if the
     * specified name is empty or {@code null}, or if the property
     * does not have the correct numeric format, then {@code null} is
     * returned.
     *
     * <p>In other words, this method returns an {@code Integer}
     * object equal to the value of:
     *
     * <blockquote>
     *  {@code getInteger(nm, null)}
     * </blockquote>
     *
     * @param   nm   property name.
     * @return  the {@code Integer} value of the property.
     * @throws  SecurityException for the same reasons as
     *          {@link System#getProperty(String) System.getProperty}
     * @see     java.lang.System#getProperty(java.lang.String)
     * @see     java.lang.System#getProperty(java.lang.String, java.lang.String)
     */
    public static Integer getInteger(String nm) {
        return getInteger(nm, null);
    }

    /**
     * Determines the integer value of the system property with the
     * specified name.
     *
     * <p>The first argument is treated as the name of a system
     * property.  System properties are accessible through the {@link
     * java.lang.System#getProperty(java.lang.String)} method. The
     * string value of this property is then interpreted as an integer
     * value using the grammar supported by {@link Integer#decode decode} and
     * an {@code Integer} object representing this value is returned.
     *
     * <p>The second argument is the default value. An {@code Integer} object
     * that represents the value of the second argument is returned if there
     * is no property of the specified name, if the property does not have
     * the correct numeric format, or if the specified name is empty or
     * {@code null}.
     *
     * <p>In other words, this method returns an {@code Integer} object
     * equal to the value of:
     *
     * <blockquote>
     *  {@code getInteger(nm, new Integer(val))}
     * </blockquote>
     *
     * but in practice it may be implemented in a manner such as:
     *
     * <blockquote><pre>
     * Integer result = getInteger(nm, null);
     * return (result == null) ? new Integer(val) : result;
     * </pre></blockquote>
     *
     * to avoid the unnecessary allocation of an {@code Integer}
     * object when the default value is not needed.
     *
     * @param   nm   property name.
     * @param   val   default value.
     * @return  the {@code Integer} value of the property.
     * @throws  SecurityException for the same reasons as
     *          {@link System#getProperty(String) System.getProperty}
     * @see     java.lang.System#getProperty(java.lang.String)
     * @see     java.lang.System#getProperty(java.lang.String, java.lang.String)
     */
    public static Integer getInteger(String nm, int val) {
        Integer result = getInteger(nm, null);
        return (result == null) ? Integer.valueOf(val) : result;
    }

    /**
     * Returns the integer value of the system property with the
     * specified name.  The first argument is treated as the name of a
     * system property.  System properties are accessible through the
     * {@link java.lang.System#getProperty(java.lang.String)} method.
     * The string value of this property is then interpreted as an
     * integer value, as per the {@link Integer#decode decode} method,
     * and an {@code Integer} object representing this value is
     * returned; in summary:
     *
     * <ul><li>If the property value begins with the two ASCII characters
     *         {@code 0x} or the ASCII character {@code #}, not
     *      followed by a minus sign, then the rest of it is parsed as a
     *      hexadecimal integer exactly as by the method
     *      {@link #valueOf(java.lang.String, int)} with radix 16.
     * <li>If the property value begins with the ASCII character
     *     {@code 0} followed by another character, it is parsed as an
     *     octal integer exactly as by the method
     *     {@link #valueOf(java.lang.String, int)} with radix 8.
     * <li>Otherwise, the property value is parsed as a decimal integer
     * exactly as by the method {@link #valueOf(java.lang.String, int)}
     * with radix 10.
     * </ul>
     *
     * <p>The second argument is the default value. The default value is
     * returned if there is no property of the specified name, if the
     * property does not have the correct numeric format, or if the
     * specified name is empty or {@code null}.
     *
     * @param   nm   property name.
     * @param   val   default value.
     * @return  the {@code Integer} value of the property.
     * @throws  SecurityException for the same reasons as
     *          {@link System#getProperty(String) System.getProperty}
     * @see     System#getProperty(java.lang.String)
     * @see     System#getProperty(java.lang.String, java.lang.String)
     */
    public static Integer getInteger(String nm, Integer val) {
        String v = null;
        try {
            v = System.getProperty(nm);
        } catch (IllegalArgumentException | NullPointerException e) {
        }
        if (v != null) {
            try {
                return Integer.decode(v);
            } catch (NumberFormatException e) {
            }
        }
        return val;
    }

    /**
     * Decodes a {@code String} into an {@code Integer}.
     * Accepts decimal, hexadecimal, and octal numbers given
     * by the following grammar:
     *
     * <blockquote>
     * <dl>
     * <dt><i>DecodableString:</i>
     * <dd><i>Sign<sub>opt</sub> DecimalNumeral</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code 0x} <i>HexDigits</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code 0X} <i>HexDigits</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code #} <i>HexDigits</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code 0} <i>OctalDigits</i>
     *
     * <dt><i>Sign:</i>
     * <dd>{@code -}
     * <dd>{@code +}
     * </dl>
     * </blockquote>
     *
     * <i>DecimalNumeral</i>, <i>HexDigits</i>, and <i>OctalDigits</i>
     * are as defined in section 3.10.1 of
     * <cite>The Java&trade; Language Specification</cite>,
     * except that underscores are not accepted between digits.
     *
     * <p>The sequence of characters following an optional
     * sign and/or radix specifier ("{@code 0x}", "{@code 0X}",
     * "{@code #}", or leading zero) is parsed as by the {@code
     * Integer.parseInt} method with the indicated radix (10, 16, or
     * 8).  This sequence of characters must represent a positive
     * value or a {@link NumberFormatException} will be thrown.  The
     * result is negated if first character of the specified {@code
     * String} is the minus sign.  No whitespace characters are
     * permitted in the {@code String}.
     *
     * @param     nm the {@code String} to decode.
     * @return    an {@code Integer} object holding the {@code int}
     *             value represented by {@code nm}
     * @exception NumberFormatException  if the {@code String} does not
     *            contain a parsable integer.
     * @see java.lang.Integer#parseInt(java.lang.String, int)
     */
    public static Integer decode(String nm) throws NumberFormatException {
        int radix = 10;
        int index = 0;
        boolean negative = false;
        Integer result;

        if (nm.length() == 0)
            throw new NumberFormatException("Zero length string");
        char firstChar = nm.charAt(0);
        // Handle sign, if present
        if (firstChar == '-') {
            negative = true;
            index++;
        } else if (firstChar == '+')
            index++;

        // Handle radix specifier, if present
        if (nm.startsWith("0x", index) || nm.startsWith("0X", index)) {
            index += 2;
            radix = 16;
        }
        else if (nm.startsWith("#", index)) {
            index ++;
            radix = 16;
        }
        else if (nm.startsWith("0", index) && nm.length() > 1 + index) {
            index ++;
            radix = 8;
        }

        if (nm.startsWith("-", index) || nm.startsWith("+", index))
            throw new NumberFormatException("Sign character in wrong position");

        try {
            result = Integer.valueOf(nm.substring(index), radix);
            result = negative ? Integer.valueOf(-result.intValue()) : result;
        } catch (NumberFormatException e) {
            // If number is Integer.MIN_VALUE, we'll end up here. The next line
            // handles this case, and causes any genuine format error to be
            // rethrown.
            String constant = negative ? ("-" + nm.substring(index))
                                       : nm.substring(index);
            result = Integer.valueOf(constant, radix);
        }
        return result;
    }

    /**
     * Compares two {@code Integer} objects numerically.
     *
     * @param   anotherInteger   the {@code Integer} to be compared.
     * @return  the value {@code 0} if this {@code Integer} is
     *          equal to the argument {@code Integer}; a value less than
     *          {@code 0} if this {@code Integer} is numerically less
     *          than the argument {@code Integer}; and a value greater
     *          than {@code 0} if this {@code Integer} is numerically
     *           greater than the argument {@code Integer} (signed
     *           comparison).
     * @since   1.2
     */
    public int compareTo(Integer anotherInteger) {
        return compare(this.value, anotherInteger.value);
    }

    /**
     * Compares two {@code int} values numerically.
     * The value returned is identical to what would be returned by:
     * <pre>
     *    Integer.valueOf(x).compareTo(Integer.valueOf(y))
     * </pre>
     *
     * @param  x the first {@code int} to compare
     * @param  y the second {@code int} to compare
     * @return the value {@code 0} if {@code x == y};
     *         a value less than {@code 0} if {@code x < y}; and
     *         a value greater than {@code 0} if {@code x > y}
     * @since 1.7
     */
    public static int compare(int x, int y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

    /**
     * Compares two {@code int} values numerically treating the values
     * as unsigned.
     *
     * @param  x the first {@code int} to compare
     * @param  y the second {@code int} to compare
     * @return the value {@code 0} if {@code x == y}; a value less
     *         than {@code 0} if {@code x < y} as unsigned values; and
     *         a value greater than {@code 0} if {@code x > y} as
     *         unsigned values
     * @since 1.8
     */
    public static int compareUnsigned(int x, int y) {
        return compare(x + MIN_VALUE, y + MIN_VALUE);
    }

    /**
     * Converts the argument to a {@code long} by an unsigned
     * conversion.  In an unsigned conversion to a {@code long}, the
     * high-order 32 bits of the {@code long} are zero and the
     * low-order 32 bits are equal to the bits of the integer
     * argument.
     *
     * Consequently, zero and positive {@code int} values are mapped
     * to a numerically equal {@code long} value and negative {@code
     * int} values are mapped to a {@code long} value equal to the
     * input plus 2<sup>32</sup>.
     *
     * @param  x the value to convert to an unsigned {@code long}
     * @return the argument converted to {@code long} by an unsigned
     *         conversion
     * @since 1.8
     */
    public static long toUnsignedLong(int x) {
        return ((long) x) & 0xffffffffL;
    }

    /**
     * Returns the unsigned quotient of dividing the first argument by
     * the second where each argument and the result is interpreted as
     * an unsigned value.
     *
     * <p>Note that in two's complement arithmetic, the three other
     * basic arithmetic operations of add, subtract, and multiply are
     * bit-wise identical if the two operands are regarded as both
     * being signed or both being unsigned.  Therefore separate {@code
     * addUnsigned}, etc. methods are not provided.
     *
     * @param dividend the value to be divided
     * @param divisor the value doing the dividing
     * @return the unsigned quotient of the first argument divided by
     * the second argument
     * @see #remainderUnsigned
     * @since 1.8
     */
    public static int divideUnsigned(int dividend, int divisor) {
        // In lieu of tricky code, for now just use long arithmetic.
        return (int)(toUnsignedLong(dividend) / toUnsignedLong(divisor));
    }

    /**
     * Returns the unsigned remainder from dividing the first argument
     * by the second where each argument and the result is interpreted
     * as an unsigned value.
     *
     * @param dividend the value to be divided
     * @param divisor the value doing the dividing
     * @return the unsigned remainder of the first argument divided by
     * the second argument
     * @see #divideUnsigned
     * @since 1.8
     */
    public static int remainderUnsigned(int dividend, int divisor) {
        // In lieu of tricky code, for now just use long arithmetic.
        return (int)(toUnsignedLong(dividend) % toUnsignedLong(divisor));
    }


    // Bit twiddling

    /**
     * The number of bits used to represent an {@code int} value in two's
     * complement binary form.
     *
     * @since 1.5
     */
    @Native public static final int SIZE = 32;

    /**
     * The number of bytes used to represent an {@code int} value in two's
     * complement binary form.
     *
     * @since 1.8
     */
    public static final int BYTES = SIZE / Byte.SIZE;

    /**
     * Returns an {@code int} value with at most a single one-bit, in the
     * position of the highest-order ("leftmost") one-bit in the specified
     * {@code int} value.  Returns zero if the specified value has no
     * one-bits in its two's complement binary representation, that is, if it
     * is equal to zero.
     *
     * @param i the value whose highest one bit is to be computed
     * @return an {@code int} value with a single one-bit, in the position
     *     of the highest-order one-bit in the specified value, or zero if
     *     the specified value is itself equal to zero.
     * @since 1.5
     */
    public static int highestOneBit(int i) {
        return i & (MIN_VALUE >>> numberOfLeadingZeros(i));
    }

    /**
     * Returns an {@code int} value with at most a single one-bit, in the
     * position of the lowest-order ("rightmost") one-bit in the specified
     * {@code int} value.  Returns zero if the specified value has no
     * one-bits in its two's complement binary representation, that is, if it
     * is equal to zero.
     *
     * @param i the value whose lowest one bit is to be computed
     * @return an {@code int} value with a single one-bit, in the position
     *     of the lowest-order one-bit in the specified value, or zero if
     *     the specified value is itself equal to zero.
     * @since 1.5
     */
    public static int lowestOneBit(int i) {
        // HD, Section 2-1
        return i & -i;
    }

    /**
     * Returns the number of zero bits preceding the highest-order
     * ("leftmost") one-bit in the two's complement binary representation
     * of the specified {@code int} value.  Returns 32 if the
     * specified value has no one-bits in its two's complement representation,
     * in other words if it is equal to zero.
     *
     * <p>Note that this method is closely related to the logarithm base 2.
     * For all positive {@code int} values x:
     * <ul>
     * <li>floor(log<sub>2</sub>(x)) = {@code 31 - numberOfLeadingZeros(x)}
     * <li>ceil(log<sub>2</sub>(x)) = {@code 32 - numberOfLeadingZeros(x - 1)}
     * </ul>
     *
     * @param i the value whose number of leading zeros is to be computed
     * @return the number of zero bits preceding the highest-order
     *     ("leftmost") one-bit in the two's complement binary representation
     *     of the specified {@code int} value, or 32 if the value
     *     is equal to zero.
     * @since 1.5
     */
    @HotSpotIntrinsicCandidate
    public static int numberOfLeadingZeros(int i) {
        // HD, Count leading 0's
        if (i <= 0)
            return i == 0 ? 32 : 0;
        int n = 31;
        if (i >= 1 << 16) { n -= 16; i >>>= 16; }
        if (i >= 1 <<  8) { n -=  8; i >>>=  8; }
        if (i >= 1 <<  4) { n -=  4; i >>>=  4; }
        if (i >= 1 <<  2) { n -=  2; i >>>=  2; }
        return n - (i >>> 1);
    }

    /**
     * Returns the number of zero bits following the lowest-order ("rightmost")
     * one-bit in the two's complement binary representation of the specified
     * {@code int} value.  Returns 32 if the specified value has no
     * one-bits in its two's complement representation, in other words if it is
     * equal to zero.
     *
     * @param i the value whose number of trailing zeros is to be computed
     * @return the number of zero bits following the lowest-order ("rightmost")
     *     one-bit in the two's complement binary representation of the
     *     specified {@code int} value, or 32 if the value is equal
     *     to zero.
     * @since 1.5
     */
    @HotSpotIntrinsicCandidate
    public static int numberOfTrailingZeros(int i) {
        // HD, Figure 5-14
        int y;
        if (i == 0) return 32;
        int n = 31;
        y = i <<16; if (y != 0) { n = n -16; i = y; }
        y = i << 8; if (y != 0) { n = n - 8; i = y; }
        y = i << 4; if (y != 0) { n = n - 4; i = y; }
        y = i << 2; if (y != 0) { n = n - 2; i = y; }
        return n - ((i << 1) >>> 31);
    }

    /**
     * Returns the number of one-bits in the two's complement binary
     * representation of the specified {@code int} value.  This function is
     * sometimes referred to as the <i>population count</i>.
     *
     * @param i the value whose bits are to be counted
     * @return the number of one-bits in the two's complement binary
     *     representation of the specified {@code int} value.
     * @since 1.5
     */
    @HotSpotIntrinsicCandidate
    public static int bitCount(int i) {
        // HD, Figure 5-2
        i = i - ((i >>> 1) & 0x55555555);
        i = (i & 0x33333333) + ((i >>> 2) & 0x33333333);
        i = (i + (i >>> 4)) & 0x0f0f0f0f;
        i = i + (i >>> 8);
        i = i + (i >>> 16);
        return i & 0x3f;
    }

    /**
     * Returns the value obtained by rotating the two's complement binary
     * representation of the specified {@code int} value left by the
     * specified number of bits.  (Bits shifted out of the left hand, or
     * high-order, side reenter on the right, or low-order.)
     *
     * <p>Note that left rotation with a negative distance is equivalent to
     * right rotation: {@code rotateLeft(val, -distance) == rotateRight(val,
     * distance)}.  Note also that rotation by any multiple of 32 is a
     * no-op, so all but the last five bits of the rotation distance can be
     * ignored, even if the distance is negative: {@code rotateLeft(val,
     * distance) == rotateLeft(val, distance & 0x1F)}.
     *
     * @param i the value whose bits are to be rotated left
     * @param distance the number of bit positions to rotate left
     * @return the value obtained by rotating the two's complement binary
     *     representation of the specified {@code int} value left by the
     *     specified number of bits.
     * @since 1.5
     */
    public static int rotateLeft(int i, int distance) {
        return (i << distance) | (i >>> -distance);
    }

    /**
     * Returns the value obtained by rotating the two's complement binary
     * representation of the specified {@code int} value right by the
     * specified number of bits.  (Bits shifted out of the right hand, or
     * low-order, side reenter on the left, or high-order.)
     *
     * <p>Note that right rotation with a negative distance is equivalent to
     * left rotation: {@code rotateRight(val, -distance) == rotateLeft(val,
     * distance)}.  Note also that rotation by any multiple of 32 is a
     * no-op, so all but the last five bits of the rotation distance can be
     * ignored, even if the distance is negative: {@code rotateRight(val,
     * distance) == rotateRight(val, distance & 0x1F)}.
     *
     * @param i the value whose bits are to be rotated right
     * @param distance the number of bit positions to rotate right
     * @return the value obtained by rotating the two's complement binary
     *     representation of the specified {@code int} value right by the
     *     specified number of bits.
     * @since 1.5
     */
    public static int rotateRight(int i, int distance) {
        return (i >>> distance) | (i << -distance);
    }

    /**
     * Returns the value obtained by reversing the order of the bits in the
     * two's complement binary representation of the specified {@code int}
     * value.
     *
     * @param i the value to be reversed
     * @return the value obtained by reversing order of the bits in the
     *     specified {@code int} value.
     * @since 1.5
     */
    public static int reverse(int i) {
        // HD, Figure 7-1
        i = (i & 0x55555555) << 1 | (i >>> 1) & 0x55555555;
        i = (i & 0x33333333) << 2 | (i >>> 2) & 0x33333333;
        i = (i & 0x0f0f0f0f) << 4 | (i >>> 4) & 0x0f0f0f0f;

        return reverseBytes(i);
    }

    /**
     * Returns the signum function of the specified {@code int} value.  (The
     * return value is -1 if the specified value is negative; 0 if the
     * specified value is zero; and 1 if the specified value is positive.)
     *
     * @param i the value whose signum is to be computed
     * @return the signum function of the specified {@code int} value.
     * @since 1.5
     */
    public static int signum(int i) {
        // HD, Section 2-7
        return (i >> 31) | (-i >>> 31);
    }

    /**
     * Returns the value obtained by reversing the order of the bytes in the
     * two's complement representation of the specified {@code int} value.
     *
     * @param i the value whose bytes are to be reversed
     * @return the value obtained by reversing the bytes in the specified
     *     {@code int} value.
     * @since 1.5
     */
    @HotSpotIntrinsicCandidate
    public static int reverseBytes(int i) {
        return (i << 24)            |
               ((i & 0xff00) << 8)  |
               ((i >>> 8) & 0xff00) |
               (i >>> 24);
    }

    /**
     * Adds two integers together as per the + operator.
     *
     * @param a the first operand
     * @param b the second operand
     * @return the sum of {@code a} and {@code b}
     * @see java.util.function.BinaryOperator
     * @since 1.8
     */
    public static int sum(int a, int b) {
        return a + b;
    }

    /**
     * Returns the greater of two {@code int} values
     * as if by calling {@link Math#max(int, int) Math.max}.
     *
     * @param a the first operand
     * @param b the second operand
     * @return the greater of {@code a} and {@code b}
     * @see java.util.function.BinaryOperator
     * @since 1.8
     */
    public static int max(int a, int b) {
        return Math.max(a, b);
    }

    /**
     * Returns the smaller of two {@code int} values
     * as if by calling {@link Math#min(int, int) Math.min}.
     *
     * @param a the first operand
     * @param b the second operand
     * @return the smaller of {@code a} and {@code b}
     * @see java.util.function.BinaryOperator
     * @since 1.8
     */
    public static int min(int a, int b) {
        return Math.min(a, b);
    }

    /** use serialVersionUID from JDK 1.0.2 for interoperability */
    @Native private static final long serialVersionUID = 1360826667806852920L;
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\InternalError.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1994, 2011, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown to indicate some unexpected internal error has occurred in
 * the Java Virtual Machine.
 *
 * @author  unascribed
 * @since   1.0
 */
public class InternalError extends VirtualMachineError {
    private static final long serialVersionUID = -9062593416125562365L;

    /**
     * Constructs an <code>InternalError</code> with no detail message.
     */
    public InternalError() {
        super();
    }

    /**
     * Constructs an <code>InternalError</code> with the specified
     * detail message.
     *
     * @param   message   the detail message.
     */
    public InternalError(String message) {
        super(message);
    }


    /**
     * Constructs an {@code InternalError} with the specified detail
     * message and cause.  <p>Note that the detail message associated
     * with {@code cause} is <i>not</i> automatically incorporated in
     * this error's detail message.
     *
     * @param  message the detail message (which is saved for later retrieval
     *         by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A {@code null} value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     * @since  1.8
     */
    public InternalError(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an {@code InternalError} with the specified cause
     * and a detail message of {@code (cause==null ? null :
     * cause.toString())} (which typically contains the class and
     * detail message of {@code cause}).
     *
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A {@code null} value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     * @since  1.8
     */
    public InternalError(Throwable cause) {
        super(cause);
    }

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\InterruptedException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown when a thread is waiting, sleeping, or otherwise occupied,
 * and the thread is interrupted, either before or during the activity.
 * Occasionally a method may wish to test whether the current
 * thread has been interrupted, and if so, to immediately throw
 * this exception.  The following code can be used to achieve
 * this effect:
 * <pre>
 *  if (Thread.interrupted())  // Clears interrupted status!
 *      throw new InterruptedException();
 * </pre>
 *
 * @author  Frank Yellin
 * @see     java.lang.Object#wait()
 * @see     java.lang.Object#wait(long)
 * @see     java.lang.Object#wait(long, int)
 * @see     java.lang.Thread#sleep(long)
 * @see     java.lang.Thread#interrupt()
 * @see     java.lang.Thread#interrupted()
 * @since   1.0
 */
public
class InterruptedException extends Exception {
    private static final long serialVersionUID = 6700697376100628473L;

    /**
     * Constructs an <code>InterruptedException</code> with no detail  message.
     */
    public InterruptedException() {
        super();
    }

    /**
     * Constructs an <code>InterruptedException</code> with the
     * specified detail message.
     *
     * @param   s   the detail message.
     */
    public InterruptedException(String s) {
        super(s);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\AbstractConstantGroup.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import java.util.*;
import jdk.internal.vm.annotation.Stable;

import static java.lang.invoke.MethodHandleStatics.rangeCheck1;
import static java.lang.invoke.MethodHandleStatics.rangeCheck2;

/** Utility class for implementing ConstantGroup. */
/*non-public*/
abstract class AbstractConstantGroup implements ConstantGroup {
    /** The size of this constant group, set permanently by the constructor. */
    protected final int size;

    /** The constructor requires the size of the constant group being represented.
     * @param size the size of this constant group, set permanently by the constructor
     */
    AbstractConstantGroup(int size) {
        this.size = size;
    }

    @Override public final int size() {
        return size;
    }

    public abstract Object get(int index) throws LinkageError;

    public abstract Object get(int index, Object ifNotPresent);

    public abstract boolean isPresent(int index);

    // Do not override equals or hashCode, since this type is stateful.

    /**
     * Produce a string using the non-resolving list view,
     * where unresolved elements are presented as asterisks.
     * @return {@code this.asList("*").toString()}
     */
    @Override public String toString() {
        return asList("*").toString();
    }

    static class AsIterator implements Iterator<Object> {
        private final ConstantGroup self;
        private final int end;
        private final boolean resolving;
        private final Object ifNotPresent;

        // Mutable state:
        private int index;

        private AsIterator(ConstantGroup self, int start, int end,
                         boolean resolving, Object ifNotPresent) {
            this.self = self;
            this.end = end;
            this.index = start;
            this.resolving = resolving;
            this.ifNotPresent = ifNotPresent;
        }
        AsIterator(ConstantGroup self, int start, int end) {
            this(self, start, end, true, null);
        }
        AsIterator(ConstantGroup self, int start, int end,
                 Object ifNotPresent) {
            this(self, start, end, false, ifNotPresent);
        }

        @Override
        public boolean hasNext() {
            return index < end;
        }

        @Override
        public Object next() {
            int i = bumpIndex();
            if (resolving)
                return self.get(i);
            else
                return self.get(i, ifNotPresent);
        }

        private int bumpIndex() {
            int i = index;
            if (i >= end)  throw new NoSuchElementException();
            index = i+1;
            return i;
        }
    }

    static class SubGroup extends AbstractConstantGroup {
        private final ConstantGroup self;  // the real CG
        private final int offset;  // offset within myself
        SubGroup(ConstantGroup self, int start, int end) {
            super(end - start);
            this.self = self;
            this.offset = start;
            rangeCheck2(start, end, size);
        }

        private int mapIndex(int index) {
            return rangeCheck1(index, size) + offset;
        }

        @Override
        public Object get(int index) {
            return self.get(mapIndex(index));
        }

        @Override
        public Object get(int index, Object ifNotPresent) {
            return self.get(mapIndex(index), ifNotPresent);
        }

        @Override
        public boolean isPresent(int index) {
            return self.isPresent(mapIndex(index));
        }

        @Override
        public ConstantGroup subGroup(int start, int end) {
            rangeCheck2(start, end, size);
            return new SubGroup(self, offset + start, offset + end);
        }

        @Override
        public List<Object> asList() {
            return new AsList(self, offset, offset + size);
        }

        @Override
        public List<Object> asList(Object ifNotPresent) {
            return new AsList(self, offset, offset + size, ifNotPresent);
        }

        @Override
        public int copyConstants(int start, int end,
                                  Object[] buf, int pos) throws LinkageError {
            rangeCheck2(start, end, size);
            return self.copyConstants(offset + start, offset + end,
                                      buf, pos);
        }

        @Override
        public int copyConstants(int start, int end,
                                  Object[] buf, int pos,
                                  Object ifNotPresent) {
            rangeCheck2(start, end, size);
            return self.copyConstants(offset + start, offset + end,
                                      buf, pos, ifNotPresent);
        }
    }

    static class AsList extends AbstractList<Object> {
        private final ConstantGroup self;
        private final int size;
        private final int offset;
        private final boolean resolving;
        private final Object ifNotPresent;

        private AsList(ConstantGroup self, int start, int end,
                       boolean resolving, Object ifNotPresent) {
            this.self = self;
            this.size = end - start;
            this.offset = start;
            this.resolving = resolving;
            this.ifNotPresent = ifNotPresent;
            rangeCheck2(start, end, self.size());
        }
        AsList(ConstantGroup self, int start, int end) {
            this(self, start, end, true, null);
        }
        AsList(ConstantGroup self, int start, int end,
               Object ifNotPresent) {
            this(self, start, end, false, ifNotPresent);
        }

        private int mapIndex(int index) {
            return rangeCheck1(index, size) + offset;
        }

        @Override public final int size() {
            return size;
        }

        @Override public Object get(int index) {
            if (resolving)
                return self.get(mapIndex(index));
            else
                return self.get(mapIndex(index), ifNotPresent);
        }

        @Override
        public Iterator<Object> iterator() {
            if (resolving)
                return new AsIterator(self, offset, offset + size);
            else
                return new AsIterator(self, offset, offset + size, ifNotPresent);
        }

        @Override public List<Object> subList(int start, int end) {
            rangeCheck2(start, end, size);
            return new AsList(self, offset + start, offset + end,
                              resolving, ifNotPresent);
        }

        @Override public Object[] toArray() {
            return toArray(new Object[size]);
        }
        @Override public <T> T[] toArray(T[] a) {
            int pad = a.length - size;
            if (pad < 0) {
                pad = 0;
                a = Arrays.copyOf(a, size);
            }
            if (resolving)
                self.copyConstants(offset, offset + size, a, 0);
            else
                self.copyConstants(offset, offset + size, a, 0,
                                   ifNotPresent);
            if (pad > 0)  a[size] = null;
            return a;
        }
    }

    static abstract
    class WithCache extends AbstractConstantGroup {
        @Stable final Object[] cache;

        WithCache(int size) {
            super(size);
            // It is caller's responsibility to initialize the cache.
            // Initial contents are all-null, which means nothing is present.
            cache = new Object[size];
        }

        void initializeCache(List<Object> cacheContents, Object ifNotPresent) {
            // Replace ifNotPresent with NOT_PRESENT,
            // and null with RESOLVED_TO_NULL.
            // Then forget about the user-provided ifNotPresent.
            for (int i = 0; i < cache.length; i++) {
                Object x = cacheContents.get(i);
                if (x == ifNotPresent)
                    continue;  // leave the null in place
                if (x == null)
                    x = RESOLVED_TO_NULL;
                cache[i] = x;
            }
        }

        @Override public Object get(int i) {
            Object x = cache[i];
            // @Stable array must use null for sentinel
            if (x == null)  x = fillCache(i);
            return unwrapNull(x);
        }

        @Override public Object get(int i, Object ifNotAvailable) {
            Object x = cache[i];
            // @Stable array must use null for sentinel
            if (x == null)  return ifNotAvailable;
            return unwrapNull(x);
        }

        @Override
        public boolean isPresent(int i) {
            return cache[i] != null;
        }

        /** hook for local subclasses */
        Object fillCache(int i) {
            throw new NoSuchElementException("constant group does not contain element #"+i);
        }

        /// routines for mapping between null sentinel and true resolved null

        static Object wrapNull(Object x) {
            return x == null ? RESOLVED_TO_NULL : x;
        }

        static Object unwrapNull(Object x) {
            assert(x != null);
            return x == RESOLVED_TO_NULL ? null : x;
        }

        // secret sentinel for an actual null resolved value, in the cache
        static final Object RESOLVED_TO_NULL = new Object();

        // secret sentinel for a "hole" in the cache:
        static final Object NOT_PRESENT = new Object();

    }

    /** Skeleton implementation of BootstrapCallInfo. */
    static
    class BSCIWithCache<T> extends WithCache implements BootstrapCallInfo<T> {
        private final MethodHandle bsm;
        private final String name;
        private final T type;

        @Override public String toString() {
            return bsm+"/"+name+":"+type+super.toString();
        }

        BSCIWithCache(MethodHandle bsm, String name, T type, int size) {
            super(size);
            this.type = type;
            this.bsm = bsm;
            this.name = name;
            assert(type instanceof Class || type instanceof MethodType);
        }

        @Override public MethodHandle bootstrapMethod() { return bsm; }
        @Override public String invocationName() { return name; }
        @Override public T invocationType() { return type; }
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\AbstractValidatingLambdaMetafactory.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2012, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang.invoke;

import sun.invoke.util.Wrapper;

import static java.lang.invoke.MethodHandleInfo.*;
import static sun.invoke.util.Wrapper.forPrimitiveType;
import static sun.invoke.util.Wrapper.forWrapperType;
import static sun.invoke.util.Wrapper.isWrapperType;

/**
 * Abstract implementation of a lambda metafactory which provides parameter
 * unrolling and input validation.
 *
 * @see LambdaMetafactory
 */
/* package */ abstract class AbstractValidatingLambdaMetafactory {

    /*
     * For context, the comments for the following fields are marked in quotes
     * with their values, given this program:
     * interface II<T> {  Object foo(T x); }
     * interface JJ<R extends Number> extends II<R> { }
     * class CC {  String impl(int i) { return "impl:"+i; }}
     * class X {
     *     public static void main(String[] args) {
     *         JJ<Integer> iii = (new CC())::impl;
     *         System.out.printf(">>> %s\n", iii.foo(44));
     * }}
     */
    final Class<?> targetClass;               // The class calling the meta-factory via invokedynamic "class X"
    final MethodType invokedType;             // The type of the invoked method "(CC)II"
    final Class<?> samBase;                   // The type of the returned instance "interface JJ"
    final String samMethodName;               // Name of the SAM method "foo"
    final MethodType samMethodType;           // Type of the SAM method "(Object)Object"
    final MethodHandle implMethod;            // Raw method handle for the implementation method
    final MethodType implMethodType;          // Type of the implMethod MethodHandle "(CC,int)String"
    final MethodHandleInfo implInfo;          // Info about the implementation method handle "MethodHandleInfo[5 CC.impl(int)String]"
    final int implKind;                       // Invocation kind for implementation "5"=invokevirtual
    final boolean implIsInstanceMethod;       // Is the implementation an instance method "true"
    final Class<?> implClass;                 // Class for referencing the implementation method "class CC"
    final MethodType instantiatedMethodType;  // Instantiated erased functional interface method type "(Integer)Object"
    final boolean isSerializable;             // Should the returned instance be serializable
    final Class<?>[] markerInterfaces;        // Additional marker interfaces to be implemented
    final MethodType[] additionalBridges;     // Signatures of additional methods to bridge


    /**
     * Meta-factory constructor.
     *
     * @param caller Stacked automatically by VM; represents a lookup context
     *               with the accessibility privileges of the caller.
     * @param invokedType Stacked automatically by VM; the signature of the
     *                    invoked method, which includes the expected static
     *                    type of the returned lambda object, and the static
     *                    types of the captured arguments for the lambda.  In
     *                    the event that the implementation method is an
     *                    instance method, the first argument in the invocation
     *                    signature will correspond to the receiver.
     * @param samMethodName Name of the method in the functional interface to
     *                      which the lambda or method reference is being
     *                      converted, represented as a String.
     * @param samMethodType Type of the method in the functional interface to
     *                      which the lambda or method reference is being
     *                      converted, represented as a MethodType.
     * @param implMethod The implementation method which should be called
     *                   (with suitable adaptation of argument types, return
     *                   types, and adjustment for captured arguments) when
     *                   methods of the resulting functional interface instance
     *                   are invoked.
     * @param instantiatedMethodType The signature of the primary functional
     *                               interface method after type variables are
     *                               substituted with their instantiation from
     *                               the capture site
     * @param isSerializable Should the lambda be made serializable?  If set,
     *                       either the target type or one of the additional SAM
     *                       types must extend {@code Serializable}.
     * @param markerInterfaces Additional interfaces which the lambda object
     *                       should implement.
     * @param additionalBridges Method types for additional signatures to be
     *                          bridged to the implementation method
     * @throws LambdaConversionException If any of the meta-factory protocol
     * invariants are violated
     */
    AbstractValidatingLambdaMetafactory(MethodHandles.Lookup caller,
                                       MethodType invokedType,
                                       String samMethodName,
                                       MethodType samMethodType,
                                       MethodHandle implMethod,
                                       MethodType instantiatedMethodType,
                                       boolean isSerializable,
                                       Class<?>[] markerInterfaces,
                                       MethodType[] additionalBridges)
            throws LambdaConversionException {
        if ((caller.lookupModes() & MethodHandles.Lookup.PRIVATE) == 0) {
            throw new LambdaConversionException(String.format(
                    "Invalid caller: %s",
                    caller.lookupClass().getName()));
        }
        this.targetClass = caller.lookupClass();
        this.invokedType = invokedType;

        this.samBase = invokedType.returnType();

        this.samMethodName = samMethodName;
        this.samMethodType  = samMethodType;

        this.implMethod = implMethod;
        this.implMethodType = implMethod.type();
        this.implInfo = caller.revealDirect(implMethod);
        switch (implInfo.getReferenceKind()) {
            case REF_invokeVirtual:
            case REF_invokeInterface:
                this.implClass = implMethodType.parameterType(0);
                // reference kind reported by implInfo may not match implMethodType's first param
                // Example: implMethodType is (Cloneable)String, implInfo is for Object.toString
                this.implKind = implClass.isInterface() ? REF_invokeInterface : REF_invokeVirtual;
                this.implIsInstanceMethod = true;
                break;
            case REF_invokeSpecial:
                // JDK-8172817: should use referenced class here, but we don't know what it was
                this.implClass = implInfo.getDeclaringClass();
                this.implKind = REF_invokeSpecial;
                this.implIsInstanceMethod = true;
                break;
            case REF_invokeStatic:
            case REF_newInvokeSpecial:
                // JDK-8172817: should use referenced class here for invokestatic, but we don't know what it was
                this.implClass = implInfo.getDeclaringClass();
                this.implKind = implInfo.getReferenceKind();
                this.implIsInstanceMethod = false;
                break;
            default:
                throw new LambdaConversionException(String.format("Unsupported MethodHandle kind: %s", implInfo));
        }

        this.instantiatedMethodType = instantiatedMethodType;
        this.isSerializable = isSerializable;
        this.markerInterfaces = markerInterfaces;
        this.additionalBridges = additionalBridges;

        if (samMethodName.isEmpty() ||
                samMethodName.indexOf('.') >= 0 ||
                samMethodName.indexOf(';') >= 0 ||
                samMethodName.indexOf('[') >= 0 ||
                samMethodName.indexOf('/') >= 0 ||
                samMethodName.indexOf('<') >= 0 ||
                samMethodName.indexOf('>') >= 0) {
            throw new LambdaConversionException(String.format(
                    "Method name '%s' is not legal",
                    samMethodName));
        }

        if (!samBase.isInterface()) {
            throw new LambdaConversionException(String.format(
                    "Functional interface %s is not an interface",
                    samBase.getName()));
        }

        for (Class<?> c : markerInterfaces) {
            if (!c.isInterface()) {
                throw new LambdaConversionException(String.format(
                        "Marker interface %s is not an interface",
                        c.getName()));
            }
        }
    }

    /**
     * Build the CallSite.
     *
     * @return a CallSite, which, when invoked, will return an instance of the
     * functional interface
     * @throws ReflectiveOperationException
     */
    abstract CallSite buildCallSite()
            throws LambdaConversionException;

    /**
     * Check the meta-factory arguments for errors
     * @throws LambdaConversionException if there are improper conversions
     */
    void validateMetafactoryArgs() throws LambdaConversionException {
        // Check arity: captured + SAM == impl
        final int implArity = implMethodType.parameterCount();
        final int capturedArity = invokedType.parameterCount();
        final int samArity = samMethodType.parameterCount();
        final int instantiatedArity = instantiatedMethodType.parameterCount();
        if (implArity != capturedArity + samArity) {
            throw new LambdaConversionException(
                    String.format("Incorrect number of parameters for %s method %s; %d captured parameters, %d functional interface method parameters, %d implementation parameters",
                                  implIsInstanceMethod ? "instance" : "static", implInfo,
                                  capturedArity, samArity, implArity));
        }
        if (instantiatedArity != samArity) {
            throw new LambdaConversionException(
                    String.format("Incorrect number of parameters for %s method %s; %d instantiated parameters, %d functional interface method parameters",
                                  implIsInstanceMethod ? "instance" : "static", implInfo,
                                  instantiatedArity, samArity));
        }
        for (MethodType bridgeMT : additionalBridges) {
            if (bridgeMT.parameterCount() != samArity) {
                throw new LambdaConversionException(
                        String.format("Incorrect number of parameters for bridge signature %s; incompatible with %s",
                                      bridgeMT, samMethodType));
            }
        }

        // If instance: first captured arg (receiver) must be subtype of class where impl method is defined
        final int capturedStart; // index of first non-receiver capture parameter in implMethodType
        final int samStart; // index of first non-receiver sam parameter in implMethodType
        if (implIsInstanceMethod) {
            final Class<?> receiverClass;

            // implementation is an instance method, adjust for receiver in captured variables / SAM arguments
            if (capturedArity == 0) {
                // receiver is function parameter
                capturedStart = 0;
                samStart = 1;
                receiverClass = instantiatedMethodType.parameterType(0);
            } else {
                // receiver is a captured variable
                capturedStart = 1;
                samStart = capturedArity;
                receiverClass = invokedType.parameterType(0);
            }

            // check receiver type
            if (!implClass.isAssignableFrom(receiverClass)) {
                throw new LambdaConversionException(
                        String.format("Invalid receiver type %s; not a subtype of implementation type %s",
                                      receiverClass, implClass));
            }
        } else {
            // no receiver
            capturedStart = 0;
            samStart = capturedArity;
        }

        // Check for exact match on non-receiver captured arguments
        for (int i=capturedStart; i<capturedArity; i++) {
            Class<?> implParamType = implMethodType.parameterType(i);
            Class<?> capturedParamType = invokedType.parameterType(i);
            if (!capturedParamType.equals(implParamType)) {
                throw new LambdaConversionException(
                        String.format("Type mismatch in captured lambda parameter %d: expecting %s, found %s",
                                      i, capturedParamType, implParamType));
            }
        }
        // Check for adaptation match on non-receiver SAM arguments
        for (int i=samStart; i<implArity; i++) {
            Class<?> implParamType = implMethodType.parameterType(i);
            Class<?> instantiatedParamType = instantiatedMethodType.parameterType(i - capturedArity);
            if (!isAdaptableTo(instantiatedParamType, implParamType, true)) {
                throw new LambdaConversionException(
                        String.format("Type mismatch for lambda argument %d: %s is not convertible to %s",
                                      i, instantiatedParamType, implParamType));
            }
        }

        // Adaptation match: return type
        Class<?> expectedType = instantiatedMethodType.returnType();
        Class<?> actualReturnType = implMethodType.returnType();
        if (!isAdaptableToAsReturn(actualReturnType, expectedType)) {
            throw new LambdaConversionException(
                    String.format("Type mismatch for lambda return: %s is not convertible to %s",
                                  actualReturnType, expectedType));
        }

        // Check descriptors of generated methods
        checkDescriptor(samMethodType);
        for (MethodType bridgeMT : additionalBridges) {
            checkDescriptor(bridgeMT);
        }
    }

    /** Validate that the given descriptor's types are compatible with {@code instantiatedMethodType} **/
    private void checkDescriptor(MethodType descriptor) throws LambdaConversionException {
        for (int i = 0; i < instantiatedMethodType.parameterCount(); i++) {
            Class<?> instantiatedParamType = instantiatedMethodType.parameterType(i);
            Class<?> descriptorParamType = descriptor.parameterType(i);
            if (!descriptorParamType.isAssignableFrom(instantiatedParamType)) {
                String msg = String.format("Type mismatch for instantiated parameter %d: %s is not a subtype of %s",
                                           i, instantiatedParamType, descriptorParamType);
                throw new LambdaConversionException(msg);
            }
        }

        Class<?> instantiatedReturnType = instantiatedMethodType.returnType();
        Class<?> descriptorReturnType = descriptor.returnType();
        if (!isAdaptableToAsReturnStrict(instantiatedReturnType, descriptorReturnType)) {
            String msg = String.format("Type mismatch for lambda expected return: %s is not convertible to %s",
                                       instantiatedReturnType, descriptorReturnType);
            throw new LambdaConversionException(msg);
        }
    }

    /**
     * Check type adaptability for parameter types.
     * @param fromType Type to convert from
     * @param toType Type to convert to
     * @param strict If true, do strict checks, else allow that fromType may be parameterized
     * @return True if 'fromType' can be passed to an argument of 'toType'
     */
    private boolean isAdaptableTo(Class<?> fromType, Class<?> toType, boolean strict) {
        if (fromType.equals(toType)) {
            return true;
        }
        if (fromType.isPrimitive()) {
            Wrapper wfrom = forPrimitiveType(fromType);
            if (toType.isPrimitive()) {
                // both are primitive: widening
                Wrapper wto = forPrimitiveType(toType);
                return wto.isConvertibleFrom(wfrom);
            } else {
                // from primitive to reference: boxing
                return toType.isAssignableFrom(wfrom.wrapperType());
            }
        } else {
            if (toType.isPrimitive()) {
                // from reference to primitive: unboxing
                Wrapper wfrom;
                if (isWrapperType(fromType) && (wfrom = forWrapperType(fromType)).primitiveType().isPrimitive()) {
                    // fromType is a primitive wrapper; unbox+widen
                    Wrapper wto = forPrimitiveType(toType);
                    return wto.isConvertibleFrom(wfrom);
                } else {
                    // must be convertible to primitive
                    return !strict;
                }
            } else {
                // both are reference types: fromType should be a superclass of toType.
                return !strict || toType.isAssignableFrom(fromType);
            }
        }
    }

    /**
     * Check type adaptability for return types --
     * special handling of void type) and parameterized fromType
     * @return True if 'fromType' can be converted to 'toType'
     */
    private boolean isAdaptableToAsReturn(Class<?> fromType, Class<?> toType) {
        return toType.equals(void.class)
               || !fromType.equals(void.class) && isAdaptableTo(fromType, toType, false);
    }
    private boolean isAdaptableToAsReturnStrict(Class<?> fromType, Class<?> toType) {
        if (fromType.equals(void.class) || toType.equals(void.class)) return fromType.equals(toType);
        else return isAdaptableTo(fromType, toType, true);
    }


    /*********** Logging support -- for debugging only, uncomment as needed
    static final Executor logPool = Executors.newSingleThreadExecutor();
    protected static void log(final String s) {
        MethodHandleProxyLambdaMetafactory.logPool.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println(s);
            }
        });
    }

    protected static void log(final String s, final Throwable e) {
        MethodHandleProxyLambdaMetafactory.logPool.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println(s);
                e.printStackTrace(System.out);
            }
        });
    }
    ***********************/

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\BootstrapCallInfo.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import java.lang.invoke.MethodHandles.Lookup;

/**
 * An interface providing full static information about a particular
 * call to a
 * <a href="package-summary.html#bsm">bootstrap method</a> of an
 * dynamic call site or dynamic constant.
 * This information includes the method itself, the associated
 * name and type, and any associated static arguments.
 * <p>
 * If a bootstrap method declares exactly two arguments, and is
 * not of variable arity, then it is fed only two arguments by
 * the JVM, the {@linkplain Lookup lookup object} and an instance
 * of {@code BootstrapCallInfo} which supplies the rest of the
 * information about the call.
 * <p>
 * The API for accessing the static arguments allows the bootstrap
 * method to reorder the resolution (in the constant pool) of the
 * static arguments, and to catch errors resulting from the resolution.
 * This mode of evaluation <em>pulls</em> bootstrap parameters from
 * the JVM under control of the bootstrap method, as opposed to
 * the JVM <em>pushing</em> parameters to a bootstrap method
 * by resolving them all before the bootstrap method is called.
 * @apiNote
 * <p>
 * The {@linkplain Lookup lookup object} is <em>not</em> included in this
 * bundle of information, so as not to obscure the access control
 * logic of the program.
 * In cases where there are many thousands of parameters, it may
 * be preferable to pull their resolved values, either singly or in
 * batches, rather than wait until all of them have been resolved
 * before a constant or call site can be used.
 * <p>
 * A push mode bootstrap method can be adapted to a pull mode
 * bootstrap method, and vice versa.  For example, this generic
 * adapter pops a push-mode bootstrap method from the beginning
 * of the static argument list, eagerly resolves all the remaining
 * static arguments, and invokes the popped method in push mode.
 * The callee has no way of telling that it was not called directly
 * from the JVM.
 * <blockquote><pre>{@code
static Object genericBSM(Lookup lookup, BootstrapCallInfo<Object> bsci)
    throws Throwable {
  ArrayList<Object> args = new ArrayList<>();
  args.add(lookup);
  args.add(bsci.invocationName());
  args.add(bsci.invocationType());
  MethodHandle bsm = (MethodHandle) bsci.get(0);
  List<Object> restOfArgs = bsci.asList().subList(1, bsci.size();
  // the next line eagerly resolves all remaining static arguments:
  args.addAll(restOfArgs);
  return bsm.invokeWithArguments(args);
}
 * }</pre></blockquote>
 *
 * <p>
 * In the other direction, here is a combinator which pops
 * a pull-mode bootstrap method from the beginning of a list of
 * static argument values (already resolved), reformats all of
 * the arguments into a pair of a lookup and a {@code BootstrapCallInfo},
 * and invokes the popped method.  Again the callee has no way of
 * telling it was not called directly by the JVM, except that
 * all of the constant values will appear as resolved.
 * Put another way, if any constant fails to resolve, the
 * callee will not be able to catch the resulting error,
 * since the error will be thrown by the JVM before the
 * bootstrap method is entered.
 * <blockquote><pre>{@code
static Object genericBSM(Lookup lookup, String name, Object type,
                         MethodHandle bsm, Object... args)
    throws Throwable {
  ConstantGroup cons = ConstantGroup.makeConstantGroup(Arrays.asList(args));
  BootstrapCallInfo<Object> bsci = makeBootstrapCallInfo(bsm, name, type, cons);
  return bsm.invoke(lookup, bsci);
}
 * }</pre></blockquote>
 *
 * @since 1.10
 */
// public
interface BootstrapCallInfo<T> extends ConstantGroup {
    /** Returns the bootstrap method for this call.
     * @return the bootstrap method
     */
    MethodHandle bootstrapMethod();

    /** Returns the method name or constant name for this call.
     * @return the method name or constant name
     */
    String invocationName();

    /** Returns the method type or constant type for this call.
     * @return the method type or constant type
     */
    T invocationType();

    /**
     * Make a new bootstrap call descriptor with the given components.
     * @param bsm bootstrap method
     * @param name invocation name
     * @param type invocation type
     * @param constants the additional static arguments for the bootstrap method
     * @param <T> the type of the invocation type, either {@link MethodHandle} or {@link Class}
     * @return a new bootstrap call descriptor with the given components
     */
    static <T> BootstrapCallInfo<T> makeBootstrapCallInfo(MethodHandle bsm,
                                                          String name,
                                                          T type,
                                                          ConstantGroup constants) {
        AbstractConstantGroup.BSCIWithCache<T> bsci = new AbstractConstantGroup.BSCIWithCache<>(bsm, name, type, constants.size());
        final Object NP = AbstractConstantGroup.BSCIWithCache.NOT_PRESENT;
        bsci.initializeCache(constants.asList(NP), NP);
        return bsci;
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\BootstrapMethodInvoker.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang.invoke;

import sun.invoke.util.Wrapper;

import java.lang.invoke.AbstractConstantGroup.BSCIWithCache;
import java.util.Arrays;

import static java.lang.invoke.BootstrapCallInfo.makeBootstrapCallInfo;
import static java.lang.invoke.ConstantGroup.makeConstantGroup;
import static java.lang.invoke.MethodHandleNatives.*;
import static java.lang.invoke.MethodHandleStatics.TRACE_METHOD_LINKAGE;
import static java.lang.invoke.MethodHandles.Lookup;
import static java.lang.invoke.MethodHandles.Lookup.IMPL_LOOKUP;

final class BootstrapMethodInvoker {

    /**
     * Factored code for invoking a bootstrap method for invokedynamic
     * or a dynamic constant.
     * @param resultType the expected return type (either CallSite or a constant type)
     * @param bootstrapMethod the BSM to call
     * @param name the method name or constant name
     * @param type the method type or constant type
     * @param info information passed up from the JVM, to derive static arguments
     * @param callerClass the class containing the resolved method call or constant load
     * @param <T> the expected return type
     * @return the expected value, either a CallSite or a constant value
     */
    static <T> T invoke(Class<T> resultType,
                        MethodHandle bootstrapMethod,
                        // Callee information:
                        String name, Object type,
                        // Extra arguments for BSM, if any:
                        Object info,
                        // Caller information:
                        Class<?> callerClass) {
        MethodHandles.Lookup caller = IMPL_LOOKUP.in(callerClass);
        Object result;
        boolean pullMode = isPullModeBSM(bootstrapMethod);  // default value is false
        boolean vmIsPushing = !staticArgumentsPulled(info); // default value is true
        MethodHandle pullModeBSM;
        // match the VM with the BSM
        if (vmIsPushing) {
            // VM is pushing arguments at us
            pullModeBSM = null;
            if (pullMode) {
                bootstrapMethod = pushMePullYou(bootstrapMethod, true);
            }
        } else {
            // VM wants us to pull args from it
            pullModeBSM = pullMode ? bootstrapMethod :
                    pushMePullYou(bootstrapMethod, false);
            bootstrapMethod = null;
        }
        try {
            // As an optimization we special case various known BSMs,
            // such as LambdaMetafactory::metafactory and
            // StringConcatFactory::makeConcatWithConstants.
            //
            // By providing static type information or even invoking
            // exactly, we avoid emitting code to perform runtime
            // checking.
            info = maybeReBox(info);
            if (info == null) {
                // VM is allowed to pass up a null meaning no BSM args
                result = invoke(bootstrapMethod, caller, name, type);
            }
            else if (!info.getClass().isArray()) {
                // VM is allowed to pass up a single BSM arg directly

                // Call to StringConcatFactory::makeConcatWithConstants
                // with empty constant arguments?
                if (isStringConcatFactoryBSM(bootstrapMethod.type())) {
                    result = (CallSite)bootstrapMethod
                            .invokeExact(caller, name, (MethodType)type,
                                         (String)info, new Object[0]);
                } else {
                    result = invoke(bootstrapMethod, caller, name, type, info);
                }
            }
            else if (info.getClass() == int[].class) {
                // VM is allowed to pass up a pair {argc, index}
                // referring to 'argc' BSM args at some place 'index'
                // in the guts of the VM (associated with callerClass).
                // The format of this index pair is private to the
                // handshake between the VM and this class only.
                // This supports "pulling" of arguments.
                // The VM is allowed to do this for any reason.
                // The code in this method makes up for any mismatches.
                BootstrapCallInfo<Object> bsci
                    = new VM_BSCI<>(bootstrapMethod, name, type, caller, (int[])info);
                // Pull-mode API is (Lookup, BootstrapCallInfo) -> Object
                result = pullModeBSM.invoke(caller, bsci);
            }
            else {
                // VM is allowed to pass up a full array of resolved BSM args
                Object[] argv = (Object[]) info;
                maybeReBoxElements(argv);

                MethodType bsmType = bootstrapMethod.type();
                if (isLambdaMetafactoryIndyBSM(bsmType) && argv.length == 3) {
                    result = (CallSite)bootstrapMethod
                            .invokeExact(caller, name, (MethodType)type, (MethodType)argv[0],
                                    (MethodHandle)argv[1], (MethodType)argv[2]);
                } else if (isLambdaMetafactoryCondyBSM(bsmType) && argv.length == 3) {
                    result = bootstrapMethod
                            .invokeExact(caller, name, (Class<?>)type, (MethodType)argv[0],
                                    (MethodHandle)argv[1], (MethodType)argv[2]);
                } else if (isStringConcatFactoryBSM(bsmType) && argv.length >= 1) {
                    String recipe = (String)argv[0];
                    Object[] shiftedArgs = Arrays.copyOfRange(argv, 1, argv.length);
                    result = (CallSite)bootstrapMethod.invokeExact(caller, name, (MethodType)type, recipe, shiftedArgs);
                } else if (isLambdaMetafactoryAltMetafactoryBSM(bsmType)) {
                    result = (CallSite)bootstrapMethod.invokeExact(caller, name, (MethodType)type, argv);
                } else {
                    switch (argv.length) {
                        case 0:
                            result = invoke(bootstrapMethod, caller, name, type);
                            break;
                        case 1:
                            result = invoke(bootstrapMethod, caller, name, type,
                                            argv[0]);
                            break;
                        case 2:
                            result = invoke(bootstrapMethod, caller, name, type,
                                            argv[0], argv[1]);
                            break;
                        case 3:
                            result = invoke(bootstrapMethod, caller, name, type,
                                            argv[0], argv[1], argv[2]);
                            break;
                        case 4:
                            result = invoke(bootstrapMethod, caller, name, type,
                                            argv[0], argv[1], argv[2], argv[3]);
                            break;
                        case 5:
                            result = invoke(bootstrapMethod, caller, name, type,
                                            argv[0], argv[1], argv[2], argv[3], argv[4]);
                            break;
                        case 6:
                            result = invoke(bootstrapMethod, caller, name, type,
                                            argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
                            break;
                        default:
                            result = invokeWithManyArguments(bootstrapMethod, caller, name, type, argv);
                    }
                }
            }
            if (resultType.isPrimitive()) {
                // Non-reference conversions are more than just plain casts.
                // By pushing the value through a funnel of the form (T x)->x,
                // the boxed result can be widened as needed.  See MH::asType.
                MethodHandle funnel = MethodHandles.identity(resultType);
                result = funnel.invoke(result);
                // Now it is the wrapper type for resultType.
                resultType = Wrapper.asWrapperType(resultType);
            }
            return resultType.cast(result);
        }
        catch (Error e) {
            // Pass through an Error, including BootstrapMethodError, any other
            // form of linkage error, such as IllegalAccessError if the bootstrap
            // method is inaccessible, or say ThreadDeath/OutOfMemoryError
            // See the "Linking Exceptions" section for the invokedynamic
            // instruction in JVMS 6.5.
            throw e;
        }
        catch (Throwable ex) {
            // Wrap anything else in BootstrapMethodError
            throw new BootstrapMethodError("bootstrap method initialization exception", ex);
        }
    }

    // If we don't provide static type information for type, we'll generate runtime
    // checks. Let's try not to...

    private static Object invoke(MethodHandle bootstrapMethod, Lookup caller,
                                 String name, Object type) throws Throwable {
        if (type instanceof Class) {
            return bootstrapMethod.invoke(caller, name, (Class<?>)type);
        } else {
            return bootstrapMethod.invoke(caller, name, (MethodType)type);
        }
    }

    private static Object invoke(MethodHandle bootstrapMethod, Lookup caller,
                                 String name, Object type, Object arg0) throws Throwable {
        if (type instanceof Class) {
            return bootstrapMethod.invoke(caller, name, (Class<?>)type, arg0);
        } else {
            return bootstrapMethod.invoke(caller, name, (MethodType)type, arg0);
        }
    }

    private static Object invoke(MethodHandle bootstrapMethod, Lookup caller, String name,
                                 Object type, Object arg0, Object arg1) throws Throwable {
        if (type instanceof Class) {
            return bootstrapMethod.invoke(caller, name, (Class<?>)type, arg0, arg1);
        } else {
            return bootstrapMethod.invoke(caller, name, (MethodType)type, arg0, arg1);
        }
    }

    private static Object invoke(MethodHandle bootstrapMethod, Lookup caller, String name,
                                 Object type, Object arg0, Object arg1,
                                 Object arg2) throws Throwable {
        if (type instanceof Class) {
            return bootstrapMethod.invoke(caller, name, (Class<?>)type, arg0, arg1, arg2);
        } else {
            return bootstrapMethod.invoke(caller, name, (MethodType)type, arg0, arg1, arg2);
        }
    }

    private static Object invoke(MethodHandle bootstrapMethod, Lookup caller, String name,
                                 Object type, Object arg0, Object arg1,
                                 Object arg2, Object arg3) throws Throwable {
        if (type instanceof Class) {
            return bootstrapMethod.invoke(caller, name, (Class<?>)type, arg0, arg1, arg2, arg3);
        } else {
            return bootstrapMethod.invoke(caller, name, (MethodType)type, arg0, arg1, arg2, arg3);
        }
    }

    private static Object invoke(MethodHandle bootstrapMethod, Lookup caller,
                                 String name, Object type, Object arg0, Object arg1,
                                 Object arg2, Object arg3, Object arg4) throws Throwable {
        if (type instanceof Class) {
            return bootstrapMethod.invoke(caller, name, (Class<?>)type, arg0, arg1, arg2, arg3, arg4);
        } else {
            return bootstrapMethod.invoke(caller, name, (MethodType)type, arg0, arg1, arg2, arg3, arg4);
        }
    }

    private static Object invoke(MethodHandle bootstrapMethod, Lookup caller,
                                 String name, Object type, Object arg0, Object arg1,
                                 Object arg2, Object arg3, Object arg4, Object arg5) throws Throwable {
        if (type instanceof Class) {
            return bootstrapMethod.invoke(caller, name, (Class<?>)type, arg0, arg1, arg2, arg3, arg4, arg5);
        } else {
            return bootstrapMethod.invoke(caller, name, (MethodType)type, arg0, arg1, arg2, arg3, arg4, arg5);
        }
    }

    private static Object invokeWithManyArguments(MethodHandle bootstrapMethod, Lookup caller,
                                                  String name, Object type, Object[] argv) throws Throwable {
        final int NON_SPREAD_ARG_COUNT = 3;  // (caller, name, type)
        final int MAX_SAFE_SIZE = MethodType.MAX_MH_ARITY / 2 - NON_SPREAD_ARG_COUNT;
        if (argv.length >= MAX_SAFE_SIZE) {
            // to be on the safe side, use invokeWithArguments which handles jumbo lists
            Object[] newargv = new Object[NON_SPREAD_ARG_COUNT + argv.length];
            newargv[0] = caller;
            newargv[1] = name;
            newargv[2] = type;
            System.arraycopy(argv, 0, newargv, NON_SPREAD_ARG_COUNT, argv.length);
            return bootstrapMethod.invokeWithArguments(newargv);
        } else {
            MethodType invocationType = MethodType.genericMethodType(NON_SPREAD_ARG_COUNT + argv.length);
            MethodHandle typedBSM = bootstrapMethod.asType(invocationType);
            MethodHandle spreader = invocationType.invokers().spreadInvoker(NON_SPREAD_ARG_COUNT);
            return spreader.invokeExact(typedBSM, (Object) caller, (Object) name, type, argv);
        }
    }

    private static final MethodType LMF_INDY_MT = MethodType.methodType(CallSite.class,
            Lookup.class, String.class, MethodType.class, MethodType.class, MethodHandle.class, MethodType.class);

    private static final MethodType LMF_ALT_MT = MethodType.methodType(CallSite.class,
            Lookup.class, String.class, MethodType.class, Object[].class);

    private static final MethodType LMF_CONDY_MT = MethodType.methodType(Object.class,
            Lookup.class, String.class, Class.class, MethodType.class, MethodHandle.class, MethodType.class);

    private static final MethodType SCF_MT = MethodType.methodType(CallSite.class,
            Lookup.class, String.class, MethodType.class, String.class, Object[].class);

    /**
     * @return true iff the BSM method type exactly matches
     *         {@see java.lang.invoke.StringConcatFactory#makeConcatWithConstants(MethodHandles.Lookup,
     *                 String,MethodType,String,Object...))}
     */
    private static boolean isStringConcatFactoryBSM(MethodType bsmType) {
        return bsmType == SCF_MT;
    }

    /**
     * @return true iff the BSM method type exactly matches
     *         {@see java.lang.invoke.LambdaMetafactory#metafactory(
     *          MethodHandles.Lookup,String,Class,MethodType,MethodHandle,MethodType)}
     */
    private static boolean isLambdaMetafactoryCondyBSM(MethodType bsmType) {
        return bsmType == LMF_CONDY_MT;
    }

    /**
     * @return true iff the BSM method type exactly matches
     *         {@see java.lang.invoke.LambdaMetafactory#metafactory(
     *          MethodHandles.Lookup,String,MethodType,MethodType,MethodHandle,MethodType)}
     */
    private static boolean isLambdaMetafactoryIndyBSM(MethodType bsmType) {
        return bsmType == LMF_INDY_MT;
    }

    /**
     * @return true iff the BSM method type exactly matches
     *         {@see java.lang.invoke.LambdaMetafactory#altMetafactory(
     *          MethodHandles.Lookup,String,MethodType,Object[])}
     */
    private static boolean isLambdaMetafactoryAltMetafactoryBSM(MethodType bsmType) {
        return bsmType == LMF_ALT_MT;
    }

    /** The JVM produces java.lang.Integer values to box
     *  CONSTANT_Integer boxes but does not intern them.
     *  Let's intern them.  This is slightly wrong for
     *  a {@code CONSTANT_Dynamic} which produces an
     *  un-interned integer (e.g., {@code new Integer(0)}).
     */
    private static Object maybeReBox(Object x) {
        if (x instanceof Integer) {
            int xi = (int) x;
            if (xi == (byte) xi)
                x = xi;  // must rebox; see JLS 5.1.7
        }
        return x;
    }

    private static void maybeReBoxElements(Object[] xa) {
        for (int i = 0; i < xa.length; i++) {
            xa[i] = maybeReBox(xa[i]);
        }
    }

    /** Canonical VM-aware implementation of BootstrapCallInfo.
     * Knows how to dig into the JVM for lazily resolved (pull-mode) constants.
     */
    private static final class VM_BSCI<T> extends BSCIWithCache<T> {
        private final int[] indexInfo;
        private final Class<?> caller;  // for index resolution only

        VM_BSCI(MethodHandle bsm, String name, T type,
                Lookup lookup, int[] indexInfo) {
            super(bsm, name, type, indexInfo[0]);
            if (!lookup.hasPrivateAccess())  //D.I.D.
                throw new AssertionError("bad Lookup object");
            this.caller = lookup.lookupClass();
            this.indexInfo = indexInfo;
            // scoop up all the easy stuff right away:
            prefetchIntoCache(0, size());
        }

        @Override Object fillCache(int i) {
            Object[] buf = { null };
            copyConstants(i, i+1, buf, 0);
            Object res = wrapNull(buf[0]);
            cache[i] = res;
            int next = i + 1;
            if (next < cache.length && cache[next] == null)
                maybePrefetchIntoCache(next, false);  // try to prefetch
            return res;
        }

        @Override public int copyConstants(int start, int end,
                                           Object[] buf, int pos) {
            int i = start, bufi = pos;
            while (i < end) {
                Object x = cache[i];
                if (x == null)  break;
                buf[bufi++] = unwrapNull(x);
                i++;
            }
            // give up at first null and grab the rest in one big block
            if (i >= end)  return i;
            Object[] temp = new Object[end - i];
            if (TRACE_METHOD_LINKAGE) {
                System.out.println("resolving more BSM arguments: " +
                        Arrays.asList(caller.getSimpleName(), Arrays.toString(indexInfo), i, end));
            }
            copyOutBootstrapArguments(caller, indexInfo,
                                      i, end, temp, 0,
                                      true, null);
            for (Object x : temp) {
                x = maybeReBox(x);
                buf[bufi++] = x;
                cache[i++] = wrapNull(x);
            }
            if (end < cache.length && cache[end] == null)
                maybePrefetchIntoCache(end, true);  // try to prefetch
            return i;
        }

        private static final int MIN_PF = 4;
        private void maybePrefetchIntoCache(int i, boolean bulk) {
            int len = cache.length;
            assert(0 <= i && i <= len);
            int pfLimit = i;
            if (bulk)  pfLimit += i;  // exponential prefetch expansion
            // try to prefetch at least MIN_PF elements
            if (pfLimit < i + MIN_PF)  pfLimit = i + MIN_PF;
            if (pfLimit > len || pfLimit < 0)  pfLimit = len;
            // stop prefetching where cache is more full than empty
            int empty = 0, nonEmpty = 0, lastEmpty = i;
            for (int j = i; j < pfLimit; j++) {
                if (cache[j] == null) {
                    empty++;
                    lastEmpty = j;
                } else {
                    nonEmpty++;
                    if (nonEmpty > empty) {
                        pfLimit = lastEmpty + 1;
                        break;
                    }
                    if (pfLimit < len)  pfLimit++;
                }
            }
            if (bulk && empty < MIN_PF && pfLimit < len)
                return;  // not worth the effort
            prefetchIntoCache(i, pfLimit);
        }

        private void prefetchIntoCache(int i, int pfLimit) {
            if (pfLimit <= i)  return;  // corner case
            Object[] temp = new Object[pfLimit - i];
            if (TRACE_METHOD_LINKAGE) {
                System.out.println("prefetching BSM arguments: " +
                        Arrays.asList(caller.getSimpleName(), Arrays.toString(indexInfo), i, pfLimit));
            }
            copyOutBootstrapArguments(caller, indexInfo,
                                      i, pfLimit, temp, 0,
                                      false, NOT_PRESENT);
            for (Object x : temp) {
                if (x != NOT_PRESENT && cache[i] == null) {
                    cache[i] = wrapNull(maybeReBox(x));
                }
                i++;
            }
        }
    }

    /*non-public*/ static final
    class PushAdapter {
        // skeleton for push-mode BSM which wraps a pull-mode BSM:
        static Object pushToBootstrapMethod(MethodHandle pullModeBSM,
                                            MethodHandles.Lookup lookup, String name, Object type,
                                            Object... arguments) throws Throwable {
            ConstantGroup cons = makeConstantGroup(Arrays.asList(arguments));
            BootstrapCallInfo<?> bsci = makeBootstrapCallInfo(pullModeBSM, name, type, cons);
            if (TRACE_METHOD_LINKAGE)
                System.out.println("pull-mode BSM gets pushed arguments from fake BSCI");
            return pullModeBSM.invoke(lookup, bsci);
        }

        static final MethodHandle MH_pushToBootstrapMethod;
        static {
            final Class<?> THIS_CLASS = PushAdapter.class;
            try {
                MH_pushToBootstrapMethod = IMPL_LOOKUP
                    .findStatic(THIS_CLASS, "pushToBootstrapMethod",
                                MethodType.methodType(Object.class, MethodHandle.class,
                                        Lookup.class, String.class, Object.class, Object[].class));
            } catch (Throwable ex) {
                throw new InternalError(ex);
            }
        }
    }

    /*non-public*/ static final
    class PullAdapter {
        // skeleton for pull-mode BSM which wraps a push-mode BSM:
        static Object pullFromBootstrapMethod(MethodHandle pushModeBSM,
                                              MethodHandles.Lookup lookup,
                                              BootstrapCallInfo<?> bsci)
                throws Throwable {
            int argc = bsci.size();
            switch (argc) {
                case 0:
                    return pushModeBSM.invoke(lookup, bsci.invocationName(), bsci.invocationType());
                case 1:
                    return pushModeBSM.invoke(lookup, bsci.invocationName(), bsci.invocationType(),
                            bsci.get(0));
                case 2:
                    return pushModeBSM.invoke(lookup, bsci.invocationName(), bsci.invocationType(),
                            bsci.get(0), bsci.get(1));
                case 3:
                    return pushModeBSM.invoke(lookup, bsci.invocationName(), bsci.invocationType(),
                            bsci.get(0), bsci.get(1), bsci.get(2));
                case 4:
                    return pushModeBSM.invoke(lookup, bsci.invocationName(), bsci.invocationType(),
                            bsci.get(0), bsci.get(1), bsci.get(2), bsci.get(3));
                case 5:
                    return pushModeBSM.invoke(lookup, bsci.invocationName(), bsci.invocationType(),
                            bsci.get(0), bsci.get(1), bsci.get(2), bsci.get(3), bsci.get(4));
                case 6:
                    return pushModeBSM.invoke(lookup, bsci.invocationName(), bsci.invocationType(),
                            bsci.get(0), bsci.get(1), bsci.get(2), bsci.get(3), bsci.get(4), bsci.get(5));
                default:
                    final int NON_SPREAD_ARG_COUNT = 3;  // (lookup, name, type)
                    final int MAX_SAFE_SIZE = MethodType.MAX_MH_ARITY / 2 - NON_SPREAD_ARG_COUNT;
                    if (argc >= MAX_SAFE_SIZE) {
                        // to be on the safe side, use invokeWithArguments which handles jumbo lists
                        Object[] newargv = new Object[NON_SPREAD_ARG_COUNT + argc];
                        newargv[0] = lookup;
                        newargv[1] = bsci.invocationName();
                        newargv[2] = bsci.invocationType();
                        bsci.copyConstants(0, argc, newargv, NON_SPREAD_ARG_COUNT);
                        return pushModeBSM.invokeWithArguments(newargv);
                    }
                    MethodType invocationType = MethodType.genericMethodType(NON_SPREAD_ARG_COUNT + argc);
                    MethodHandle typedBSM = pushModeBSM.asType(invocationType);
                    MethodHandle spreader = invocationType.invokers().spreadInvoker(NON_SPREAD_ARG_COUNT);
                    Object[] argv = new Object[argc];
                    bsci.copyConstants(0, argc, argv, 0);
                    return spreader.invokeExact(typedBSM, (Object) lookup, (Object) bsci.invocationName(), bsci.invocationType(), argv);
                }
        }

        static final MethodHandle MH_pullFromBootstrapMethod;

        static {
            final Class<?> THIS_CLASS = PullAdapter.class;
            try {
                MH_pullFromBootstrapMethod = IMPL_LOOKUP
                    .findStatic(THIS_CLASS, "pullFromBootstrapMethod",
                                MethodType.methodType(Object.class, MethodHandle.class,
                                        Lookup.class, BootstrapCallInfo.class));
            } catch (Throwable ex) {
                throw new InternalError(ex);
            }
        }
    }

    /** Given a push-mode BSM (taking one argument) convert it to a
     *  pull-mode BSM (taking N pre-resolved arguments).
     *  This method is used when, in fact, the JVM is passing up
     *  pre-resolved arguments, but the BSM is expecting lazy stuff.
     *  Or, when goToPushMode is true, do the reverse transform.
     *  (The two transforms are exactly inverse.)
     */
    static MethodHandle pushMePullYou(MethodHandle bsm, boolean goToPushMode) {
        if (TRACE_METHOD_LINKAGE) {
            System.out.println("converting BSM of type " + bsm.type() + " to "
                    + (goToPushMode ? "push mode" : "pull mode"));
        }
        assert(isPullModeBSM(bsm) == goToPushMode); // there must be a change
        if (goToPushMode) {
            return PushAdapter.MH_pushToBootstrapMethod.bindTo(bsm).withVarargs(true);
        } else {
            return PullAdapter.MH_pullFromBootstrapMethod.bindTo(bsm).withVarargs(false);
        }
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\BoundMethodHandle.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2008, 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import jdk.internal.vm.annotation.Stable;
import sun.invoke.util.ValueConversions;

import java.util.ArrayList;
import java.util.List;

import static java.lang.invoke.LambdaForm.BasicType;
import static java.lang.invoke.LambdaForm.BasicType.*;
import static java.lang.invoke.LambdaForm.BasicType.V_TYPE_NUM;
import static java.lang.invoke.LambdaForm.BasicType.V_TYPE_NUM;
import static java.lang.invoke.LambdaForm.BasicType.V_TYPE_NUM;
import static java.lang.invoke.MethodHandles.Lookup.IMPL_LOOKUP;
import static java.lang.invoke.MethodHandleNatives.Constants.*;
import static java.lang.invoke.MethodHandleStatics.newInternalError;
import static java.lang.invoke.MethodHandleStatics.uncaughtException;

/**
 * The flavor of method handle which emulates an invoke instruction
 * on a predetermined argument.  The JVM dispatches to the correct method
 * when the handle is created, not when it is invoked.
 *
 * All bound arguments are encapsulated in dedicated species.
 */
/*non-public*/ abstract class BoundMethodHandle extends MethodHandle {

    /*non-public*/ BoundMethodHandle(MethodType type, LambdaForm form) {
        super(type, form);
        assert(speciesData() == speciesDataFor(form));
    }

    //
    // BMH API and internals
    //

    static BoundMethodHandle bindSingle(MethodType type, LambdaForm form, BasicType xtype, Object x) {
        // for some type signatures, there exist pre-defined concrete BMH classes
        try {
            switch (xtype) {
            case L_TYPE:
                return bindSingle(type, form, x);  // Use known fast path.
            case I_TYPE:
                return (BoundMethodHandle) SPECIALIZER.topSpecies().extendWith(I_TYPE_NUM).factory().invokeBasic(type, form, ValueConversions.widenSubword(x));
            case J_TYPE:
                return (BoundMethodHandle) SPECIALIZER.topSpecies().extendWith(J_TYPE_NUM).factory().invokeBasic(type, form, (long) x);
            case F_TYPE:
                return (BoundMethodHandle) SPECIALIZER.topSpecies().extendWith(F_TYPE_NUM).factory().invokeBasic(type, form, (float) x);
            case D_TYPE:
                return (BoundMethodHandle) SPECIALIZER.topSpecies().extendWith(D_TYPE_NUM).factory().invokeBasic(type, form, (double) x);
            default : throw newInternalError("unexpected xtype: " + xtype);
            }
        } catch (Throwable t) {
            throw uncaughtException(t);
        }
    }

    /*non-public*/
    LambdaFormEditor editor() {
        return form.editor();
    }

    static BoundMethodHandle bindSingle(MethodType type, LambdaForm form, Object x) {
        return Species_L.make(type, form, x);
    }

    @Override // there is a default binder in the super class, for 'L' types only
    /*non-public*/
    BoundMethodHandle bindArgumentL(int pos, Object value) {
        return editor().bindArgumentL(this, pos, value);
    }

    /*non-public*/
    BoundMethodHandle bindArgumentI(int pos, int value) {
        return editor().bindArgumentI(this, pos, value);
    }
    /*non-public*/
    BoundMethodHandle bindArgumentJ(int pos, long value) {
        return editor().bindArgumentJ(this, pos, value);
    }
    /*non-public*/
    BoundMethodHandle bindArgumentF(int pos, float value) {
        return editor().bindArgumentF(this, pos, value);
    }
    /*non-public*/
    BoundMethodHandle bindArgumentD(int pos, double value) {
        return editor().bindArgumentD(this, pos, value);
    }
    @Override
    BoundMethodHandle rebind() {
        if (!tooComplex()) {
            return this;
        }
        return makeReinvoker(this);
    }

    private boolean tooComplex() {
        return (fieldCount() > FIELD_COUNT_THRESHOLD ||
                form.expressionCount() > FORM_EXPRESSION_THRESHOLD);
    }
    private static final int FIELD_COUNT_THRESHOLD = 12;      // largest convenient BMH field count
    private static final int FORM_EXPRESSION_THRESHOLD = 24;  // largest convenient BMH expression count

    /**
     * A reinvoker MH has this form:
     * {@code lambda (bmh, arg*) { thismh = bmh[0]; invokeBasic(thismh, arg*) }}
     */
    static BoundMethodHandle makeReinvoker(MethodHandle target) {
        LambdaForm form = DelegatingMethodHandle.makeReinvokerForm(
                target, MethodTypeForm.LF_REBIND,
                Species_L.BMH_SPECIES, Species_L.BMH_SPECIES.getterFunction(0));
        return Species_L.make(target.type(), form, target);
    }

    /**
     * Return the {@link BoundMethodHandle.SpeciesData} instance representing this BMH species. All subclasses must provide a
     * static field containing this value, and they must accordingly implement this method.
     */
    /*non-public*/ abstract BoundMethodHandle.SpeciesData speciesData();

    /*non-public*/ static BoundMethodHandle.SpeciesData speciesDataFor(LambdaForm form) {
        Object c = form.names[0].constraint;
        if (c instanceof SpeciesData) {
            return (SpeciesData) c;
        }
        // if there is no BMH constraint, then use the null constraint
        return SPECIALIZER.topSpecies();
    }

    /**
     * Return the number of fields in this BMH.  Equivalent to speciesData().fieldCount().
     */
    /*non-public*/ final int fieldCount() { return speciesData().fieldCount(); }

    @Override
    Object internalProperties() {
        return "\n& BMH="+internalValues();
    }

    @Override
    final String internalValues() {
        int count = fieldCount();
        if (count == 1) {
            return "[" + arg(0) + "]";
        }
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < count; ++i) {
            sb.append("\n  ").append(i).append(": ( ").append(arg(i)).append(" )");
        }
        return sb.append("\n]").toString();
    }

    /*non-public*/ final Object arg(int i) {
        try {
            Class<?> fieldType = speciesData().fieldTypes().get(i);
            switch (BasicType.basicType(fieldType)) {
                case L_TYPE: return          speciesData().getter(i).invokeBasic(this);
                case I_TYPE: return (int)    speciesData().getter(i).invokeBasic(this);
                case J_TYPE: return (long)   speciesData().getter(i).invokeBasic(this);
                case F_TYPE: return (float)  speciesData().getter(i).invokeBasic(this);
                case D_TYPE: return (double) speciesData().getter(i).invokeBasic(this);
            }
        } catch (Throwable ex) {
            throw uncaughtException(ex);
        }
        throw new InternalError("unexpected type: " + speciesData().key()+"."+i);
    }

    //
    // cloning API
    //

    /*non-public*/ abstract BoundMethodHandle copyWith(MethodType mt, LambdaForm lf);
    /*non-public*/ abstract BoundMethodHandle copyWithExtendL(MethodType mt, LambdaForm lf, Object narg);
    /*non-public*/ abstract BoundMethodHandle copyWithExtendI(MethodType mt, LambdaForm lf, int    narg);
    /*non-public*/ abstract BoundMethodHandle copyWithExtendJ(MethodType mt, LambdaForm lf, long   narg);
    /*non-public*/ abstract BoundMethodHandle copyWithExtendF(MethodType mt, LambdaForm lf, float  narg);
    /*non-public*/ abstract BoundMethodHandle copyWithExtendD(MethodType mt, LambdaForm lf, double narg);

    //
    // concrete BMH classes required to close bootstrap loops
    //

    private  // make it private to force users to access the enclosing class first
    static final class Species_L extends BoundMethodHandle {

        final Object argL0;

        private Species_L(MethodType mt, LambdaForm lf, Object argL0) {
            super(mt, lf);
            this.argL0 = argL0;
        }

        @Override
        /*non-public*/ SpeciesData speciesData() {
            return BMH_SPECIES;
        }

        /*non-public*/ static @Stable SpeciesData BMH_SPECIES;

        /*non-public*/ static BoundMethodHandle make(MethodType mt, LambdaForm lf, Object argL0) {
            return new Species_L(mt, lf, argL0);
        }
        @Override
        /*non-public*/ final BoundMethodHandle copyWith(MethodType mt, LambdaForm lf) {
            return new Species_L(mt, lf, argL0);
        }
        @Override
        /*non-public*/ final BoundMethodHandle copyWithExtendL(MethodType mt, LambdaForm lf, Object narg) {
            try {
                return (BoundMethodHandle) BMH_SPECIES.extendWith(L_TYPE_NUM).factory().invokeBasic(mt, lf, argL0, narg);
            } catch (Throwable ex) {
                throw uncaughtException(ex);
            }
        }
        @Override
        /*non-public*/ final BoundMethodHandle copyWithExtendI(MethodType mt, LambdaForm lf, int narg) {
            try {
                return (BoundMethodHandle) BMH_SPECIES.extendWith(I_TYPE_NUM).factory().invokeBasic(mt, lf, argL0, narg);
            } catch (Throwable ex) {
                throw uncaughtException(ex);
            }
        }
        @Override
        /*non-public*/ final BoundMethodHandle copyWithExtendJ(MethodType mt, LambdaForm lf, long narg) {
            try {
                return (BoundMethodHandle) BMH_SPECIES.extendWith(J_TYPE_NUM).factory().invokeBasic(mt, lf, argL0, narg);
            } catch (Throwable ex) {
                throw uncaughtException(ex);
            }
        }
        @Override
        /*non-public*/ final BoundMethodHandle copyWithExtendF(MethodType mt, LambdaForm lf, float narg) {
            try {
                return (BoundMethodHandle) BMH_SPECIES.extendWith(F_TYPE_NUM).factory().invokeBasic(mt, lf, argL0, narg);
            } catch (Throwable ex) {
                throw uncaughtException(ex);
            }
        }
        @Override
        /*non-public*/ final BoundMethodHandle copyWithExtendD(MethodType mt, LambdaForm lf, double narg) {
            try {
                return (BoundMethodHandle) BMH_SPECIES.extendWith(D_TYPE_NUM).factory().invokeBasic(mt, lf, argL0, narg);
            } catch (Throwable ex) {
                throw uncaughtException(ex);
            }
        }
    }

    //
    // BMH species meta-data
    //

    /*non-public*/
    static final class SpeciesData extends ClassSpecializer<BoundMethodHandle, String, SpeciesData>.SpeciesData {
        // This array is filled in lazily, as new species come into being over time.
        @Stable final private SpeciesData[] extensions = new SpeciesData[ARG_TYPE_LIMIT];

        public SpeciesData(Specializer outer, String key) {
            outer.super(key);
        }

        @Override
        protected String deriveClassName() {
            String typeString = deriveTypeString();
            if (typeString.isEmpty()) {
                return SimpleMethodHandle.class.getName();
            }
            return BoundMethodHandle.class.getName() + "$Species_" + typeString;
        }

        @Override
        protected List<Class<?>> deriveFieldTypes(String key) {
            ArrayList<Class<?>> types = new ArrayList<>(key.length());
            for (int i = 0; i < key.length(); i++) {
                types.add(basicType(key.charAt(i)).basicTypeClass());
            }
            return types;
        }

        @Override
        protected String deriveTypeString() {
            // (If/when we have to add nominal types, just inherit the more complex default.)
            return key();
        }

        @Override
        protected MethodHandle deriveTransformHelper(MemberName transform, int whichtm) {
            if (whichtm == Specializer.TN_COPY_NO_EXTEND) {
                return factory();
            } else if (whichtm < ARG_TYPE_LIMIT) {
                return extendWith((byte) whichtm).factory();
            } else {
                throw newInternalError("bad transform");
            }
        }

        @Override
        protected <X> List<X> deriveTransformHelperArguments(MemberName transform, int whichtm, List<X> args, List<X> fields) {
            assert(verifyTHAargs(transform, whichtm, args, fields));
            // The rule is really simple:  Keep the first two arguments
            // the same, then put in the fields, then put any other argument.
            args.addAll(2, fields);
            return args;
        }

        private boolean verifyTHAargs(MemberName transform, int whichtm, List<?> args, List<?> fields) {
            assert(transform == Specializer.BMH_TRANSFORMS.get(whichtm));
            assert(args.size() == transform.getMethodType().parameterCount());
            assert(fields.size() == this.fieldCount());
            final int MH_AND_LF = 2;
            if (whichtm == Specializer.TN_COPY_NO_EXTEND) {
                assert(transform.getMethodType().parameterCount() == MH_AND_LF);
            } else if (whichtm < ARG_TYPE_LIMIT) {
                assert(transform.getMethodType().parameterCount() == MH_AND_LF+1);
                final BasicType type = basicType((byte) whichtm);
                assert(transform.getParameterTypes()[MH_AND_LF] == type.basicTypeClass());
            } else {
                return false;
            }
            return true;
        }

        /*non-public*/ SpeciesData extendWith(byte typeNum) {
            SpeciesData sd = extensions[typeNum];
            if (sd != null)  return sd;
            sd = SPECIALIZER.findSpecies(key() + BasicType.basicType(typeNum).basicTypeChar());
            extensions[typeNum] = sd;
            return sd;
        }
    }

    /*non-public*/
    static final Specializer SPECIALIZER = new Specializer();
    static {
        SimpleMethodHandle.BMH_SPECIES = BoundMethodHandle.SPECIALIZER.findSpecies("");
        Species_L.BMH_SPECIES = BoundMethodHandle.SPECIALIZER.findSpecies("L");
    }

    /*non-public*/
    static final class Specializer extends ClassSpecializer<BoundMethodHandle, String, SpeciesData> {

        private static final MemberName SPECIES_DATA_ACCESSOR;

        static {
            try {
                SPECIES_DATA_ACCESSOR = IMPL_LOOKUP.resolveOrFail(REF_invokeVirtual, BoundMethodHandle.class,
                        "speciesData", MethodType.methodType(BoundMethodHandle.SpeciesData.class));
            } catch (ReflectiveOperationException ex) {
                throw newInternalError("Bootstrap link error", ex);
            }
        }

        private Specializer() {
            super(  // Reified type parameters:
                    BoundMethodHandle.class, String.class, BoundMethodHandle.SpeciesData.class,
                    // Principal constructor type:
                    MethodType.methodType(void.class, MethodType.class, LambdaForm.class),
                    // Required linkage between class and species:
                    SPECIES_DATA_ACCESSOR,
                    "BMH_SPECIES",
                    BMH_TRANSFORMS);
        }

        @Override
        protected String topSpeciesKey() {
            return "";
        }

        @Override
        protected BoundMethodHandle.SpeciesData newSpeciesData(String key) {
            return new BoundMethodHandle.SpeciesData(this, key);
        }

        static final List<MemberName> BMH_TRANSFORMS;
        static final int TN_COPY_NO_EXTEND = V_TYPE_NUM;
        static {
            final Class<BoundMethodHandle> BMH = BoundMethodHandle.class;
            // copyWithExtendLIJFD + copyWith
            try {
                BMH_TRANSFORMS = List.of(
                        IMPL_LOOKUP.resolveOrFail(REF_invokeVirtual, BMH, "copyWithExtendL", MethodType.methodType(BMH, MethodType.class, LambdaForm.class, Object.class)),
                        IMPL_LOOKUP.resolveOrFail(REF_invokeVirtual, BMH, "copyWithExtendI", MethodType.methodType(BMH, MethodType.class, LambdaForm.class, int.class)),
                        IMPL_LOOKUP.resolveOrFail(REF_invokeVirtual, BMH, "copyWithExtendJ", MethodType.methodType(BMH, MethodType.class, LambdaForm.class, long.class)),
                        IMPL_LOOKUP.resolveOrFail(REF_invokeVirtual, BMH, "copyWithExtendF", MethodType.methodType(BMH, MethodType.class, LambdaForm.class, float.class)),
                        IMPL_LOOKUP.resolveOrFail(REF_invokeVirtual, BMH, "copyWithExtendD", MethodType.methodType(BMH, MethodType.class, LambdaForm.class, double.class)),
                        IMPL_LOOKUP.resolveOrFail(REF_invokeVirtual, BMH, "copyWith", MethodType.methodType(BMH, MethodType.class, LambdaForm.class))
                );
            } catch (ReflectiveOperationException ex) {
                throw newInternalError("Failed resolving copyWith methods", ex);
            }

            // as it happens, there is one transform per BasicType including V_TYPE
            assert(BMH_TRANSFORMS.size() == TYPE_LIMIT);
        }

        /**
         * Generation of concrete BMH classes.
         *
         * A concrete BMH species is fit for binding a number of values adhering to a
         * given type pattern. Reference types are erased.
         *
         * BMH species are cached by type pattern.
         *
         * A BMH species has a number of fields with the concrete (possibly erased) types of
         * bound values. Setters are provided as an API in BMH. Getters are exposed as MHs,
         * which can be included as names in lambda forms.
         */
        class Factory extends ClassSpecializer<BoundMethodHandle, String, BoundMethodHandle.SpeciesData>.Factory {
            @Override
            protected String chooseFieldName(Class<?> type, int index) {
                return "arg" + super.chooseFieldName(type, index);
            }
        }

        @Override
        protected Factory makeFactory() {
            return new Factory();
        }
      }

    static SpeciesData speciesData_L()      { return Species_L.BMH_SPECIES; }
    static SpeciesData speciesData_LL()     { return SPECIALIZER.findSpecies("LL"); }
    static SpeciesData speciesData_LLL()    { return SPECIALIZER.findSpecies("LLL"); }
    static SpeciesData speciesData_LLLL()   { return SPECIALIZER.findSpecies("LLLL"); }
    static SpeciesData speciesData_LLLLL()  { return SPECIALIZER.findSpecies("LLLLL"); }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\CallSite.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2008, 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import static java.lang.invoke.MethodHandleStatics.*;
import static java.lang.invoke.MethodHandles.Lookup.IMPL_LOOKUP;

import jdk.internal.vm.annotation.Stable;

/**
 * A {@code CallSite} is a holder for a variable {@link MethodHandle},
 * which is called its {@code target}.
 * An {@code invokedynamic} instruction linked to a {@code CallSite} delegates
 * all calls to the site's current target.
 * A {@code CallSite} may be associated with several {@code invokedynamic}
 * instructions, or it may be "free floating", associated with none.
 * In any case, it may be invoked through an associated method handle
 * called its {@linkplain #dynamicInvoker dynamic invoker}.
 * <p>
 * {@code CallSite} is an abstract class which does not allow
 * direct subclassing by users.  It has three immediate,
 * concrete subclasses that may be either instantiated or subclassed.
 * <ul>
 * <li>If a mutable target is not required, an {@code invokedynamic} instruction
 * may be permanently bound by means of a {@linkplain ConstantCallSite constant call site}.
 * <li>If a mutable target is required which has volatile variable semantics,
 * because updates to the target must be immediately and reliably witnessed by other threads,
 * a {@linkplain VolatileCallSite volatile call site} may be used.
 * <li>Otherwise, if a mutable target is required,
 * a {@linkplain MutableCallSite mutable call site} may be used.
 * </ul>
 * <p>
 * A non-constant call site may be <em>relinked</em> by changing its target.
 * The new target must have the same {@linkplain MethodHandle#type() type}
 * as the previous target.
 * Thus, though a call site can be relinked to a series of
 * successive targets, it cannot change its type.
 * <p>
 * Here is a sample use of call sites and bootstrap methods which links every
 * dynamic call site to print its arguments:
<blockquote><pre>{@code
static void test() throws Throwable {
    // THE FOLLOWING LINE IS PSEUDOCODE FOR A JVM INSTRUCTION
    InvokeDynamic[#bootstrapDynamic].baz("baz arg", 2, 3.14);
}
private static void printArgs(Object... args) {
  System.out.println(java.util.Arrays.deepToString(args));
}
private static final MethodHandle printArgs;
static {
  MethodHandles.Lookup lookup = MethodHandles.lookup();
  Class thisClass = lookup.lookupClass();  // (who am I?)
  printArgs = lookup.findStatic(thisClass,
      "printArgs", MethodType.methodType(void.class, Object[].class));
}
private static CallSite bootstrapDynamic(MethodHandles.Lookup caller, String name, MethodType type) {
  // ignore caller and name, but match the type:
  return new ConstantCallSite(printArgs.asType(type));
}
}</pre></blockquote>
 * @author John Rose, JSR 292 EG
 * @since 1.7
 */
abstract
public class CallSite {

    // The actual payload of this call site:
    /*package-private*/
    MethodHandle target;    // Note: This field is known to the JVM.  Do not change.

    /**
     * Make a blank call site object with the given method type.
     * An initial target method is supplied which will throw
     * an {@link IllegalStateException} if called.
     * <p>
     * Before this {@code CallSite} object is returned from a bootstrap method,
     * it is usually provided with a more useful target method,
     * via a call to {@link CallSite#setTarget(MethodHandle) setTarget}.
     * @throws NullPointerException if the proposed type is null
     */
    /*package-private*/
    CallSite(MethodType type) {
        target = makeUninitializedCallSite(type);
    }

    /**
     * Make a call site object equipped with an initial target method handle.
     * @param target the method handle which will be the initial target of the call site
     * @throws NullPointerException if the proposed target is null
     */
    /*package-private*/
    CallSite(MethodHandle target) {
        target.type();  // null check
        this.target = target;
    }

    /**
     * Make a call site object equipped with an initial target method handle.
     * @param targetType the desired type of the call site
     * @param createTargetHook a hook which will bind the call site to the target method handle
     * @throws WrongMethodTypeException if the hook cannot be invoked on the required arguments,
     *         or if the target returned by the hook is not of the given {@code targetType}
     * @throws NullPointerException if the hook returns a null value
     * @throws ClassCastException if the hook returns something other than a {@code MethodHandle}
     * @throws Throwable anything else thrown by the hook function
     */
    /*package-private*/
    CallSite(MethodType targetType, MethodHandle createTargetHook) throws Throwable {
        this(targetType);
        ConstantCallSite selfCCS = (ConstantCallSite) this;
        MethodHandle boundTarget = (MethodHandle) createTargetHook.invokeWithArguments(selfCCS);
        checkTargetChange(this.target, boundTarget);
        this.target = boundTarget;
    }

    /**
     * {@code CallSite} dependency context.
     * JVM uses CallSite.context to store nmethod dependencies on the call site target.
     */
    private final MethodHandleNatives.CallSiteContext context = MethodHandleNatives.CallSiteContext.make(this);

    /**
     * Returns the type of this call site's target.
     * Although targets may change, any call site's type is permanent, and can never change to an unequal type.
     * The {@code setTarget} method enforces this invariant by refusing any new target that does
     * not have the previous target's type.
     * @return the type of the current target, which is also the type of any future target
     */
    public MethodType type() {
        // warning:  do not call getTarget here, because CCS.getTarget can throw IllegalStateException
        return target.type();
    }

    /**
     * Returns the target method of the call site, according to the
     * behavior defined by this call site's specific class.
     * The immediate subclasses of {@code CallSite} document the
     * class-specific behaviors of this method.
     *
     * @return the current linkage state of the call site, its target method handle
     * @see ConstantCallSite
     * @see VolatileCallSite
     * @see #setTarget
     * @see ConstantCallSite#getTarget
     * @see MutableCallSite#getTarget
     * @see VolatileCallSite#getTarget
     */
    public abstract MethodHandle getTarget();

    /**
     * Updates the target method of this call site, according to the
     * behavior defined by this call site's specific class.
     * The immediate subclasses of {@code CallSite} document the
     * class-specific behaviors of this method.
     * <p>
     * The type of the new target must be {@linkplain MethodType#equals equal to}
     * the type of the old target.
     *
     * @param newTarget the new target
     * @throws NullPointerException if the proposed new target is null
     * @throws WrongMethodTypeException if the proposed new target
     *         has a method type that differs from the previous target
     * @see CallSite#getTarget
     * @see ConstantCallSite#setTarget
     * @see MutableCallSite#setTarget
     * @see VolatileCallSite#setTarget
     */
    public abstract void setTarget(MethodHandle newTarget);

    void checkTargetChange(MethodHandle oldTarget, MethodHandle newTarget) {
        MethodType oldType = oldTarget.type();
        MethodType newType = newTarget.type();  // null check!
        if (!newType.equals(oldType))
            throw wrongTargetType(newTarget, oldType);
    }

    private static WrongMethodTypeException wrongTargetType(MethodHandle target, MethodType type) {
        return new WrongMethodTypeException(String.valueOf(target)+" should be of type "+type);
    }

    /**
     * Produces a method handle equivalent to an invokedynamic instruction
     * which has been linked to this call site.
     * <p>
     * This method is equivalent to the following code:
     * <blockquote><pre>{@code
     * MethodHandle getTarget, invoker, result;
     * getTarget = MethodHandles.publicLookup().bind(this, "getTarget", MethodType.methodType(MethodHandle.class));
     * invoker = MethodHandles.exactInvoker(this.type());
     * result = MethodHandles.foldArguments(invoker, getTarget)
     * }</pre></blockquote>
     *
     * @return a method handle which always invokes this call site's current target
     */
    public abstract MethodHandle dynamicInvoker();

    /*non-public*/ MethodHandle makeDynamicInvoker() {
        MethodHandle getTarget = getTargetHandle().bindArgumentL(0, this);
        MethodHandle invoker = MethodHandles.exactInvoker(this.type());
        return MethodHandles.foldArguments(invoker, getTarget);
    }

    private static @Stable MethodHandle GET_TARGET;
    private static MethodHandle getTargetHandle() {
        MethodHandle handle = GET_TARGET;
        if (handle != null) {
            return handle;
        }
        try {
            return GET_TARGET = IMPL_LOOKUP.
                    findVirtual(CallSite.class, "getTarget",
                                MethodType.methodType(MethodHandle.class));
        } catch (ReflectiveOperationException e) {
            throw newInternalError(e);
        }
    }

    private static @Stable MethodHandle THROW_UCS;
    private static MethodHandle uninitializedCallSiteHandle() {
        MethodHandle handle = THROW_UCS;
        if (handle != null) {
            return handle;
        }
        try {
            return THROW_UCS = IMPL_LOOKUP.
                findStatic(CallSite.class, "uninitializedCallSite",
                           MethodType.methodType(Object.class, Object[].class));
        } catch (ReflectiveOperationException e) {
            throw newInternalError(e);
        }
    }

    /** This guy is rolled into the default target if a MethodType is supplied to the constructor. */
    private static Object uninitializedCallSite(Object... ignore) {
        throw new IllegalStateException("uninitialized call site");
    }

    private MethodHandle makeUninitializedCallSite(MethodType targetType) {
        MethodType basicType = targetType.basicType();
        MethodHandle invoker = basicType.form().cachedMethodHandle(MethodTypeForm.MH_UNINIT_CS);
        if (invoker == null) {
            invoker = uninitializedCallSiteHandle().asType(basicType);
            invoker = basicType.form().setCachedMethodHandle(MethodTypeForm.MH_UNINIT_CS, invoker);
        }
        // unchecked view is OK since no values will be received or returned
        return invoker.viewAsType(targetType, false);
    }

    // unsafe stuff:
    private static @Stable long TARGET_OFFSET;
    private static long getTargetOffset() {
        long offset = TARGET_OFFSET;
        if (offset > 0) {
            return offset;
        }
        offset = TARGET_OFFSET = UNSAFE.objectFieldOffset(CallSite.class, "target");
        assert(offset > 0);
        return offset;
    }

    /*package-private*/
    void setTargetNormal(MethodHandle newTarget) {
        MethodHandleNatives.setCallSiteTargetNormal(this, newTarget);
    }
    /*package-private*/
    MethodHandle getTargetVolatile() {
        return (MethodHandle) UNSAFE.getObjectVolatile(this, getTargetOffset());
    }
    /*package-private*/
    void setTargetVolatile(MethodHandle newTarget) {
        MethodHandleNatives.setCallSiteTargetVolatile(this, newTarget);
    }

    // this implements the upcall from the JVM, MethodHandleNatives.linkCallSite:
    static CallSite makeSite(MethodHandle bootstrapMethod,
                             // Callee information:
                             String name, MethodType type,
                             // Extra arguments for BSM, if any:
                             Object info,
                             // Caller information:
                             Class<?> callerClass) {
        CallSite site;
        try {
            Object binding = BootstrapMethodInvoker.invoke(
                    CallSite.class, bootstrapMethod, name, type, info, callerClass);
            if (binding instanceof CallSite) {
                site = (CallSite) binding;
            } else {
                // See the "Linking Exceptions" section for the invokedynamic
                // instruction in JVMS 6.5.
                // Throws a runtime exception defining the cause that is then
                // in the "catch (Throwable ex)" a few lines below wrapped in
                // BootstrapMethodError
                throw new ClassCastException("CallSite bootstrap method failed to produce an instance of CallSite");
            }
            if (!site.getTarget().type().equals(type)) {
                // See the "Linking Exceptions" section for the invokedynamic
                // instruction in JVMS 6.5.
                // Throws a runtime exception defining the cause that is then
                // in the "catch (Throwable ex)" a few lines below wrapped in
                // BootstrapMethodError
                throw wrongTargetType(site.getTarget(), type);
            }
        } catch (Error e) {
            // Pass through an Error, including BootstrapMethodError, any other
            // form of linkage error, such as IllegalAccessError if the bootstrap
            // method is inaccessible, or say ThreadDeath/OutOfMemoryError
            // See the "Linking Exceptions" section for the invokedynamic
            // instruction in JVMS 6.5.
            throw e;
        } catch (Throwable ex) {
            // Wrap anything else in BootstrapMethodError
            throw new BootstrapMethodError("CallSite bootstrap method initialization exception", ex);
        }
        return site;
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\ClassSpecializer.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import jdk.internal.loader.BootLoader;
import jdk.internal.org.objectweb.asm.ClassWriter;
import jdk.internal.org.objectweb.asm.FieldVisitor;
import jdk.internal.org.objectweb.asm.MethodVisitor;
import jdk.internal.vm.annotation.Stable;
import sun.invoke.util.BytecodeName;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import static java.lang.invoke.LambdaForm.*;
import static java.lang.invoke.MethodHandleNatives.Constants.REF_getStatic;
import static java.lang.invoke.MethodHandleNatives.Constants.REF_putStatic;
import static java.lang.invoke.MethodHandleStatics.*;
import static java.lang.invoke.MethodHandles.Lookup.IMPL_LOOKUP;
import static jdk.internal.org.objectweb.asm.Opcodes.*;

/**
 * Class specialization code.
 * @param <T> top class under which species classes are created.
 * @param <K> key which identifies individual specializations.
 * @param <S> species data type.
 */
/*non-public*/
abstract class ClassSpecializer<T,K,S extends ClassSpecializer<T,K,S>.SpeciesData> {
    private final Class<T> topClass;
    private final Class<K> keyType;
    private final Class<S> metaType;
    private final MemberName sdAccessor;
    private final String sdFieldName;
    private final List<MemberName> transformMethods;
    private final MethodType baseConstructorType;
    private final S topSpecies;
    private final ConcurrentHashMap<K, Object> cache = new ConcurrentHashMap<>();
    private final Factory factory;
    private @Stable boolean topClassIsSuper;

    /** Return the top type mirror, for type {@code T} */
    public final Class<T> topClass() { return topClass; }

    /** Return the key type mirror, for type {@code K} */
    public final Class<K> keyType() { return keyType; }

    /** Return the species metadata type mirror, for type {@code S} */
    public final Class<S> metaType() { return metaType; }

    /** Report the leading arguments (if any) required by every species factory.
     * Every species factory adds its own field types as additional arguments,
     * but these arguments always come first, in every factory method.
     */
    protected MethodType baseConstructorType() { return baseConstructorType; }

    /** Return the trivial species for the null sequence of arguments. */
    protected final S topSpecies() { return topSpecies; }

    /** Return the list of transform methods originally given at creation of this specializer. */
    protected final List<MemberName> transformMethods() { return transformMethods; }

    /** Return the factory object used to build and load concrete species code. */
    protected final Factory factory() { return factory; }

    /**
     * Constructor for this class specializer.
     * @param topClass type mirror for T
     * @param keyType type mirror for K
     * @param metaType type mirror for S
     * @param baseConstructorType principal constructor type
     * @param sdAccessor the method used to get the speciesData
     * @param sdFieldName the name of the species data field, inject the speciesData object
     * @param transformMethods optional list of transformMethods
     */
    protected ClassSpecializer(Class<T> topClass,
                               Class<K> keyType,
                               Class<S> metaType,
                               MethodType baseConstructorType,
                               MemberName sdAccessor,
                               String sdFieldName,
                               List<MemberName> transformMethods) {
        this.topClass = topClass;
        this.keyType = keyType;
        this.metaType = metaType;
        this.sdAccessor = sdAccessor;
        this.transformMethods = List.copyOf(transformMethods);
        this.sdFieldName = sdFieldName;
        this.baseConstructorType = baseConstructorType.changeReturnType(void.class);
        this.factory = makeFactory();
        K tsk = topSpeciesKey();
        S topSpecies = null;
        if (tsk != null && topSpecies == null) {
            // if there is a key, build the top species if needed:
            topSpecies = findSpecies(tsk);
        }
        this.topSpecies = topSpecies;
    }

    // Utilities for subclass constructors:
    protected static <T> Constructor<T> reflectConstructor(Class<T> defc, Class<?>... ptypes) {
        try {
            return defc.getDeclaredConstructor(ptypes);
        } catch (NoSuchMethodException ex) {
            throw newIAE(defc.getName()+"("+MethodType.methodType(void.class, ptypes)+")", ex);
        }
    }

    protected static Field reflectField(Class<?> defc, String name) {
        try {
            return defc.getDeclaredField(name);
        } catch (NoSuchFieldException ex) {
            throw newIAE(defc.getName()+"."+name, ex);
        }
    }

    private static RuntimeException newIAE(String message, Throwable cause) {
        return new IllegalArgumentException(message, cause);
    }

    private static final Function<Object, Object> CREATE_RESERVATION = new Function<>() {
        @Override
        public Object apply(Object key) {
            return new Object();
        }
    };

    public final S findSpecies(K key) {
        // Note:  Species instantiation may throw VirtualMachineError because of
        // code cache overflow.  If this happens the species bytecode may be
        // loaded but not linked to its species metadata (with MH's etc).
        // That will cause a throw out of Factory.loadSpecies.
        //
        // In a later attempt to get the same species, the already-loaded
        // class will be present in the system dictionary, causing an
        // error when the species generator tries to reload it.
        // We try to detect this case and link the pre-existing code.
        //
        // Although it would be better to start fresh by loading a new
        // copy, we have to salvage the previously loaded but broken code.
        // (As an alternative, we might spin a new class with a new name,
        // or use the anonymous class mechanism.)
        //
        // In the end, as long as everybody goes through this findSpecies method,
        // it will ensure only one SpeciesData will be set successfully on a
        // concrete class if ever.
        // The concrete class is published via SpeciesData instance
        // returned here only after the class and species data are linked together.
        Object speciesDataOrReservation = cache.computeIfAbsent(key, CREATE_RESERVATION);
        // Separating the creation of a placeholder SpeciesData instance above
        // from the loading and linking a real one below ensures we can never
        // accidentally call computeIfAbsent recursively.
        S speciesData;
        if (speciesDataOrReservation.getClass() == Object.class) {
            synchronized (speciesDataOrReservation) {
                Object existingSpeciesData = cache.get(key);
                if (existingSpeciesData == speciesDataOrReservation) { // won the race
                    // create a new SpeciesData...
                    speciesData = newSpeciesData(key);
                    // load and link it...
                    speciesData = factory.loadSpecies(speciesData);
                    if (!cache.replace(key, existingSpeciesData, speciesData)) {
                        throw newInternalError("Concurrent loadSpecies");
                    }
                } else { // lost the race; the retrieved existingSpeciesData is the final
                    speciesData = metaType.cast(existingSpeciesData);
                }
            }
        } else {
            speciesData = metaType.cast(speciesDataOrReservation);
        }
        assert(speciesData != null && speciesData.isResolved());
        return speciesData;
    }

    /**
     * Meta-data wrapper for concrete subtypes of the top class.
     * Each concrete subtype corresponds to a given sequence of basic field types (LIJFD).
     * The fields are immutable; their values are fully specified at object construction.
     * Each species supplies an array of getter functions which may be used in lambda forms.
     * A concrete value is always constructed from the full tuple of its field values,
     * accompanied by the required constructor parameters.
     * There *may* also be transforms which cloning a species instance and
     * either replace a constructor parameter or add one or more new field values.
     * The shortest possible species has zero fields.
     * Subtypes are not interrelated among themselves by subtyping, even though
     * it would appear that a shorter species could serve as a supertype of a
     * longer one which extends it.
     */
    public abstract class SpeciesData {
        // Bootstrapping requires circular relations Class -> SpeciesData -> Class
        // Therefore, we need non-final links in the chain.  Use @Stable fields.
        private final K key;
        private final List<Class<?>> fieldTypes;
        @Stable private Class<? extends T> speciesCode;
        @Stable private List<MethodHandle> factories;
        @Stable private List<MethodHandle> getters;
        @Stable private List<LambdaForm.NamedFunction> nominalGetters;
        @Stable private final MethodHandle[] transformHelpers = new MethodHandle[transformMethods.size()];

        protected SpeciesData(K key) {
            this.key = keyType.cast(Objects.requireNonNull(key));
            List<Class<?>> types = deriveFieldTypes(key);
            this.fieldTypes = List.copyOf(types);
        }

        public final K key() {
            return key;
        }

        protected final List<Class<?>> fieldTypes() {
            return fieldTypes;
        }

        protected final int fieldCount() {
            return fieldTypes.size();
        }

        protected ClassSpecializer<T,K,S> outer() {
            return ClassSpecializer.this;
        }

        protected final boolean isResolved() {
            return speciesCode != null && factories != null && !factories.isEmpty();
        }

        @Override public String toString() {
            return metaType.getSimpleName() + "[" + key.toString() + " => " + (isResolved() ? speciesCode.getSimpleName() : "UNRESOLVED") + "]";
        }

        @Override
        public int hashCode() {
            return key.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof ClassSpecializer.SpeciesData)) {
                return false;
            }
            @SuppressWarnings("rawtypes")
            ClassSpecializer.SpeciesData that = (ClassSpecializer.SpeciesData) obj;
            return this.outer() == that.outer() && this.key.equals(that.key);
        }

        /** Throws NPE if this species is not yet resolved. */
        protected final Class<? extends T> speciesCode() {
            return Objects.requireNonNull(speciesCode);
        }

        /**
         * Return a {@link MethodHandle} which can get the indexed field of this species.
         * The return type is the type of the species field it accesses.
         * The argument type is the {@code fieldHolder} class of this species.
         */
        protected MethodHandle getter(int i) {
            return getters.get(i);
        }

        /**
         * Return a {@link LambdaForm.Name} containing a {@link LambdaForm.NamedFunction} that
         * represents a MH bound to a generic invoker, which in turn forwards to the corresponding
         * getter.
         */
        protected LambdaForm.NamedFunction getterFunction(int i) {
            LambdaForm.NamedFunction nf = nominalGetters.get(i);
            assert(nf.memberDeclaringClassOrNull() == speciesCode());
            assert(nf.returnType() == BasicType.basicType(fieldTypes.get(i)));
            return nf;
        }

        protected List<LambdaForm.NamedFunction> getterFunctions() {
            return nominalGetters;
        }

        protected List<MethodHandle> getters() {
            return getters;
        }

        protected MethodHandle factory() {
            return factories.get(0);
        }

        protected MethodHandle transformHelper(int whichtm) {
            MethodHandle mh = transformHelpers[whichtm];
            if (mh != null)  return mh;
            mh = deriveTransformHelper(transformMethods().get(whichtm), whichtm);
            // Do a little type checking before we start using the MH.
            // (It will be called with invokeBasic, so this is our only chance.)
            final MethodType mt = transformHelperType(whichtm);
            mh = mh.asType(mt);
            return transformHelpers[whichtm] = mh;
        }

        private final MethodType transformHelperType(int whichtm) {
            MemberName tm = transformMethods().get(whichtm);
            ArrayList<Class<?>> args = new ArrayList<>();
            ArrayList<Class<?>> fields = new ArrayList<>();
            Collections.addAll(args, tm.getParameterTypes());
            fields.addAll(fieldTypes());
            List<Class<?>> helperArgs = deriveTransformHelperArguments(tm, whichtm, args, fields);
            return MethodType.methodType(tm.getReturnType(), helperArgs);
        }

        // Hooks for subclasses:

        /**
         * Given a key, derive the list of field types, which all instances of this
         * species must store.
         */
        protected abstract List<Class<?>> deriveFieldTypes(K key);

        /**
         * Given the index of a method in the transforms list, supply a factory
         * method that takes the arguments of the transform, plus the local fields,
         * and produce a value of the required type.
         * You can override this to return null or throw if there are no transforms.
         * This method exists so that the transforms can be "grown" lazily.
         * This is necessary if the transform *adds* a field to an instance,
         * which sometimtes requires the creation, on the fly, of an extended species.
         * This method is only called once for any particular parameter.
         * The species caches the result in a private array.
         *
         * @param transform the transform being implemented
         * @param whichtm the index of that transform in the original list of transforms
         * @return the method handle which creates a new result from a mix of transform
         * arguments and field values
         */
        protected abstract MethodHandle deriveTransformHelper(MemberName transform, int whichtm);

        /**
         * During code generation, this method is called once per transform to determine
         * what is the mix of arguments to hand to the transform-helper.  The bytecode
         * which marshals these arguments is open-coded in the species-specific transform.
         * The two lists are of opaque objects, which you shouldn't do anything with besides
         * reordering them into the output list.  (They are both mutable, to make editing
         * easier.)  The imputed types of the args correspond to the transform's parameter
         * list, while the imputed types of the fields correspond to the species field types.
         * After code generation, this method may be called occasionally by error-checking code.
         *
         * @param transform the transform being implemented
         * @param whichtm the index of that transform in the original list of transforms
         * @param args a list of opaque objects representing the incoming transform arguments
         * @param fields a list of opaque objects representing the field values of the receiver
         * @param <X> the common element type of the various lists
         * @return a new list
         */
        protected abstract <X> List<X> deriveTransformHelperArguments(MemberName transform, int whichtm,
                                                                      List<X> args, List<X> fields);

        /** Given a key, generate the name of the class which implements the species for that key.
         * This algorithm must be stable.
         *
         * @return class name, which by default is {@code outer().topClass().getName() + "$Species_" + deriveTypeString(key)}
         */
        protected String deriveClassName() {
            return outer().topClass().getName() + "$Species_" + deriveTypeString();
        }

        /**
         * Default implementation collects basic type characters,
         * plus possibly type names, if some types don't correspond
         * to basic types.
         *
         * @return a string suitable for use in a class name
         */
        protected String deriveTypeString() {
            List<Class<?>> types = fieldTypes();
            StringBuilder buf = new StringBuilder();
            StringBuilder end = new StringBuilder();
            for (Class<?> type : types) {
                BasicType basicType = BasicType.basicType(type);
                if (basicType.basicTypeClass() == type) {
                    buf.append(basicType.basicTypeChar());
                } else {
                    buf.append('V');
                    end.append(classSig(type));
                }
            }
            String typeString;
            if (end.length() > 0) {
                typeString = BytecodeName.toBytecodeName(buf.append("_").append(end).toString());
            } else {
                typeString = buf.toString();
            }
            return LambdaForm.shortenSignature(typeString);
        }

        /**
         * Report what immediate super-class to use for the concrete class of this species.
         * Normally this is {@code topClass}, but if that is an interface, the factory must override.
         * The super-class must provide a constructor which takes the {@code baseConstructorType} arguments, if any.
         * This hook also allows the code generator to use more than one canned supertype for species.
         *
         * @return the super-class of the class to be generated
         */
        protected Class<? extends T> deriveSuperClass() {
            final Class<T> topc = topClass();
            if (!topClassIsSuper) {
                try {
                    final Constructor<T> con = reflectConstructor(topc, baseConstructorType().parameterArray());
                    if (!topc.isInterface() && !Modifier.isPrivate(con.getModifiers())) {
                        topClassIsSuper = true;
                    }
                } catch (Exception|InternalError ex) {
                    // fall through...
                }
                if (!topClassIsSuper) {
                    throw newInternalError("must override if the top class cannot serve as a super class");
                }
            }
            return topc;
        }
    }

    protected abstract S newSpeciesData(K key);

    protected K topSpeciesKey() {
        return null;  // null means don't report a top species
    }

    /**
     * Code generation support for instances.
     * Subclasses can modify the behavior.
     */
    public class Factory {
        /**
         * Get a concrete subclass of the top class for a given combination of bound types.
         *
         * @param speciesData the species requiring the class, not yet linked
         * @return a linked version of the same species
         */
        S loadSpecies(S speciesData) {
            String className = speciesData.deriveClassName();
            assert(className.indexOf('/') < 0) : className;
            Class<?> salvage = null;
            try {
                salvage = BootLoader.loadClassOrNull(className);
                if (TRACE_RESOLVE && salvage != null) {
                    // Used by jlink species pregeneration plugin, see
                    // jdk.tools.jlink.internal.plugins.GenerateJLIClassesPlugin
                    System.out.println("[SPECIES_RESOLVE] " + className + " (salvaged)");
                }
            } catch (Error ex) {
                if (TRACE_RESOLVE) {
                    System.out.println("[SPECIES_FRESOLVE] " + className + " (Error) " + ex.getMessage());
                }
            }
            final Class<? extends T> speciesCode;
            if (salvage != null) {
                speciesCode = salvage.asSubclass(topClass());
                linkSpeciesDataToCode(speciesData, speciesCode);
                linkCodeToSpeciesData(speciesCode, speciesData, true);
            } else {
                // Not pregenerated, generate the class
                try {
                    speciesCode = generateConcreteSpeciesCode(className, speciesData);
                    if (TRACE_RESOLVE) {
                        // Used by jlink species pregeneration plugin, see
                        // jdk.tools.jlink.internal.plugins.GenerateJLIClassesPlugin
                        System.out.println("[SPECIES_RESOLVE] " + className + " (generated)");
                    }
                    // This operation causes a lot of churn:
                    linkSpeciesDataToCode(speciesData, speciesCode);
                    // This operation commits the relation, but causes little churn:
                    linkCodeToSpeciesData(speciesCode, speciesData, false);
                } catch (Error ex) {
                    if (TRACE_RESOLVE) {
                        System.out.println("[SPECIES_RESOLVE] " + className + " (Error #2)" );
                    }
                    // We can get here if there is a race condition loading a class.
                    // Or maybe we are out of resources.  Back out of the CHM.get and retry.
                    throw ex;
                }
            }

            if (!speciesData.isResolved()) {
                throw newInternalError("bad species class linkage for " + className + ": " + speciesData);
            }
            assert(speciesData == loadSpeciesDataFromCode(speciesCode));
            return speciesData;
        }

        /**
         * Generate a concrete subclass of the top class for a given combination of bound types.
         *
         * A concrete species subclass roughly matches the following schema:
         *
         * <pre>
         * class Species_[[types]] extends [[T]] {
         *     final [[S]] speciesData() { return ... }
         *     static [[T]] make([[fields]]) { return ... }
         *     [[fields]]
         *     final [[T]] transform([[args]]) { return ... }
         * }
         * </pre>
         *
         * The {@code [[types]]} signature is precisely the key for the species.
         *
         * The {@code [[fields]]} section consists of one field definition per character in
         * the type signature, adhering to the naming schema described in the definition of
         * {@link #chooseFieldName}.
         *
         * For example, a concrete species for two references and one integral bound value
         * has a shape like the following:
         *
         * <pre>
         * class TopClass { ... private static
         * final class Species_LLI extends TopClass {
         *     final Object argL0;
         *     final Object argL1;
         *     final int argI2;
         *     private Species_LLI(CT ctarg, ..., Object argL0, Object argL1, int argI2) {
         *         super(ctarg, ...);
         *         this.argL0 = argL0;
         *         this.argL1 = argL1;
         *         this.argI2 = argI2;
         *     }
         *     final SpeciesData speciesData() { return BMH_SPECIES; }
         *     &#64;Stable static SpeciesData BMH_SPECIES; // injected afterwards
         *     static TopClass make(CT ctarg, ..., Object argL0, Object argL1, int argI2) {
         *         return new Species_LLI(ctarg, ..., argL0, argL1, argI2);
         *     }
         *     final TopClass copyWith(CT ctarg, ...) {
         *         return new Species_LLI(ctarg, ..., argL0, argL1, argI2);
         *     }
         *     // two transforms, for the sake of illustration:
         *     final TopClass copyWithExtendL(CT ctarg, ..., Object narg) {
         *         return BMH_SPECIES.transform(L_TYPE).invokeBasic(ctarg, ..., argL0, argL1, argI2, narg);
         *     }
         *     final TopClass copyWithExtendI(CT ctarg, ..., int narg) {
         *         return BMH_SPECIES.transform(I_TYPE).invokeBasic(ctarg, ..., argL0, argL1, argI2, narg);
         *     }
         * }
         * </pre>
         *
         * @param className of the species
         * @param speciesData what species we are generating
         * @return the generated concrete TopClass class
         */
        Class<? extends T> generateConcreteSpeciesCode(String className, ClassSpecializer<T,K,S>.SpeciesData speciesData) {
            byte[] classFile = generateConcreteSpeciesCodeFile(className, speciesData);

            // load class
            InvokerBytecodeGenerator.maybeDump(classBCName(className), classFile);
            Class<?> speciesCode;

            ClassLoader cl = topClass().getClassLoader();
            ProtectionDomain pd = null;
            if (cl != null) {
                pd = AccessController.doPrivileged(
                        new PrivilegedAction<>() {
                            @Override
                            public ProtectionDomain run() {
                                return topClass().getProtectionDomain();
                            }
                        });
            }
            try {
                speciesCode = UNSAFE.defineClass(className, classFile, 0, classFile.length, cl, pd);
            } catch (Exception ex) {
                throw newInternalError(ex);
            }

            return speciesCode.asSubclass(topClass());
        }

        // These are named like constants because there is only one per specialization scheme:
        private final String SPECIES_DATA = classBCName(metaType);
        private final String SPECIES_DATA_SIG = classSig(SPECIES_DATA);
        private final String SPECIES_DATA_NAME = sdAccessor.getName();
        private final int SPECIES_DATA_MODS = sdAccessor.getModifiers();
        private final List<String> TRANSFORM_NAMES;  // derived from transformMethods
        private final List<MethodType> TRANSFORM_TYPES;
        private final List<Integer> TRANSFORM_MODS;
        {
            // Tear apart transformMethods to get the names, types, and modifiers.
            List<String> tns = new ArrayList<>();
            List<MethodType> tts = new ArrayList<>();
            List<Integer> tms = new ArrayList<>();
            for (int i = 0; i < transformMethods.size(); i++) {
                MemberName tm = transformMethods.get(i);
                tns.add(tm.getName());
                final MethodType tt = tm.getMethodType();
                tts.add(tt);
                tms.add(tm.getModifiers());
            }
            TRANSFORM_NAMES = List.of(tns.toArray(new String[0]));
            TRANSFORM_TYPES = List.of(tts.toArray(new MethodType[0]));
            TRANSFORM_MODS = List.of(tms.toArray(new Integer[0]));
        }
        private static final int ACC_PPP = ACC_PUBLIC | ACC_PRIVATE | ACC_PROTECTED;

        /*non-public*/ byte[] generateConcreteSpeciesCodeFile(String className0, ClassSpecializer<T,K,S>.SpeciesData speciesData) {
            final String className = classBCName(className0);
            final String superClassName = classBCName(speciesData.deriveSuperClass());

            final ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES);
            final int NOT_ACC_PUBLIC = 0;  // not ACC_PUBLIC
            cw.visit(V1_6, NOT_ACC_PUBLIC + ACC_FINAL + ACC_SUPER, className, null, superClassName, null);

            final String sourceFile = className.substring(className.lastIndexOf('.')+1);
            cw.visitSource(sourceFile, null);

            // emit static types and BMH_SPECIES fields
            FieldVisitor fw = cw.visitField(NOT_ACC_PUBLIC + ACC_STATIC, sdFieldName, SPECIES_DATA_SIG, null, null);
            fw.visitAnnotation(STABLE_SIG, true);
            fw.visitEnd();

            // handy holder for dealing with groups of typed values (ctor arguments and fields)
            class Var {
                final int index;
                final String name;
                final Class<?> type;
                final String desc;
                final BasicType basicType;
                final int slotIndex;
                Var(int index, int slotIndex) {
                    this.index = index;
                    this.slotIndex = slotIndex;
                    name = null; type = null; desc = null;
                    basicType = BasicType.V_TYPE;
                }
                Var(String name, Class<?> type, Var prev) {
                    int slotIndex = prev.nextSlotIndex();
                    int index = prev.nextIndex();
                    if (name == null)  name = "x";
                    if (name.endsWith("#"))
                        name = name.substring(0, name.length()-1) + index;
                    assert(!type.equals(void.class));
                    String desc = classSig(type);
                    BasicType basicType = BasicType.basicType(type);
                    this.index = index;
                    this.name = name;
                    this.type = type;
                    this.desc = desc;
                    this.basicType = basicType;
                    this.slotIndex = slotIndex;
                }
                Var lastOf(List<Var> vars) {
                    int n = vars.size();
                    return (n == 0 ? this : vars.get(n-1));
                }
                <X> List<Var> fromTypes(List<X> types) {
                    Var prev = this;
                    ArrayList<Var> result = new ArrayList<>(types.size());
                    int i = 0;
                    for (X x : types) {
                        String vn = name;
                        Class<?> vt;
                        if (x instanceof Class) {
                            vt = (Class<?>) x;
                            // make the names friendlier if debugging
                            assert((vn = vn + "_" + (i++)) != null);
                        } else {
                            @SuppressWarnings("unchecked")
                            Var v = (Var) x;
                            vn = v.name;
                            vt = v.type;
                        }
                        prev = new Var(vn, vt, prev);
                        result.add(prev);
                    }
                    return result;
                }

                int slotSize() { return basicType.basicTypeSlots(); }
                int nextIndex() { return index + (slotSize() == 0 ? 0 : 1); }
                int nextSlotIndex() { return slotIndex >= 0 ? slotIndex + slotSize() : slotIndex; }
                boolean isInHeap() { return slotIndex < 0; }
                void emitVarInstruction(int asmop, MethodVisitor mv) {
                    if (asmop == ALOAD)
                        asmop = typeLoadOp(basicType.basicTypeChar());
                    else
                        throw new AssertionError("bad op="+asmop+" for desc="+desc);
                    mv.visitVarInsn(asmop, slotIndex);
                }
                public void emitFieldInsn(int asmop, MethodVisitor mv) {
                    mv.visitFieldInsn(asmop, className, name, desc);
                }
            }

            final Var NO_THIS = new Var(0, 0),
                    AFTER_THIS = new Var(0, 1),
                    IN_HEAP = new Var(0, -1);

            // figure out the field types
            final List<Class<?>> fieldTypes = speciesData.fieldTypes();
            final List<Var> fields = new ArrayList<>(fieldTypes.size());
            {
                Var nextF = IN_HEAP;
                for (Class<?> ft : fieldTypes) {
                    String fn = chooseFieldName(ft, nextF.nextIndex());
                    nextF = new Var(fn, ft, nextF);
                    fields.add(nextF);
                }
            }

            // emit bound argument fields
            for (Var field : fields) {
                cw.visitField(ACC_FINAL, field.name, field.desc, null, null).visitEnd();
            }

            MethodVisitor mv;

            // emit implementation of speciesData()
            mv = cw.visitMethod((SPECIES_DATA_MODS & ACC_PPP) + ACC_FINAL,
                    SPECIES_DATA_NAME, "()" + SPECIES_DATA_SIG, null, null);
            mv.visitCode();
            mv.visitFieldInsn(GETSTATIC, className, sdFieldName, SPECIES_DATA_SIG);
            mv.visitInsn(ARETURN);
            mv.visitMaxs(0, 0);
            mv.visitEnd();

            // figure out the constructor arguments
            MethodType superCtorType = ClassSpecializer.this.baseConstructorType();
            MethodType thisCtorType = superCtorType.appendParameterTypes(fieldTypes);

            // emit constructor
            {
                mv = cw.visitMethod(ACC_PRIVATE,
                        "<init>", methodSig(thisCtorType), null, null);
                mv.visitCode();
                mv.visitVarInsn(ALOAD, 0); // this

                final List<Var> ctorArgs = AFTER_THIS.fromTypes(superCtorType.parameterList());
                for (Var ca : ctorArgs) {
                    ca.emitVarInstruction(ALOAD, mv);
                }

                // super(ca...)
                mv.visitMethodInsn(INVOKESPECIAL, superClassName,
                        "<init>", methodSig(superCtorType), false);

                // store down fields
                Var lastFV = AFTER_THIS.lastOf(ctorArgs);
                for (Var f : fields) {
                    // this.argL1 = argL1
                    mv.visitVarInsn(ALOAD, 0);  // this
                    lastFV = new Var(f.name, f.type, lastFV);
                    lastFV.emitVarInstruction(ALOAD, mv);
                    f.emitFieldInsn(PUTFIELD, mv);
                }

                mv.visitInsn(RETURN);
                mv.visitMaxs(0, 0);
                mv.visitEnd();
            }

            // emit make()  ...factory method wrapping constructor
            {
                MethodType ftryType = thisCtorType.changeReturnType(topClass());
                mv = cw.visitMethod(NOT_ACC_PUBLIC + ACC_STATIC,
                        "make", methodSig(ftryType), null, null);
                mv.visitCode();
                // make instance
                mv.visitTypeInsn(NEW, className);
                mv.visitInsn(DUP);
                // load factory method arguments:  ctarg... and arg...
                for (Var v : NO_THIS.fromTypes(ftryType.parameterList())) {
                    v.emitVarInstruction(ALOAD, mv);
                }

                // finally, invoke the constructor and return
                mv.visitMethodInsn(INVOKESPECIAL, className,
                        "<init>", methodSig(thisCtorType), false);
                mv.visitInsn(ARETURN);
                mv.visitMaxs(0, 0);
                mv.visitEnd();
            }

            // For each transform, emit the customized override of the transform method.
            // This method mixes together some incoming arguments (from the transform's
            // static type signature) with the field types themselves, and passes
            // the resulting mish-mosh of values to a method handle produced by
            // the species itself.  (Typically this method handle is the factory
            // method of this species or a related one.)
            for (int whichtm = 0; whichtm < TRANSFORM_NAMES.size(); whichtm++) {
                final String     TNAME = TRANSFORM_NAMES.get(whichtm);
                final MethodType TTYPE = TRANSFORM_TYPES.get(whichtm);
                final int        TMODS = TRANSFORM_MODS.get(whichtm);
                mv = cw.visitMethod((TMODS & ACC_PPP) | ACC_FINAL,
                        TNAME, TTYPE.toMethodDescriptorString(), null, E_THROWABLE);
                mv.visitCode();
                // return a call to the corresponding "transform helper", something like this:
                //   MY_SPECIES.transformHelper(whichtm).invokeBasic(ctarg, ..., argL0, ..., xarg)
                mv.visitFieldInsn(GETSTATIC, className,
                        sdFieldName, SPECIES_DATA_SIG);
                emitIntConstant(whichtm, mv);
                mv.visitMethodInsn(INVOKEVIRTUAL, SPECIES_DATA,
                        "transformHelper", "(I)" + MH_SIG, false);

                List<Var> targs = AFTER_THIS.fromTypes(TTYPE.parameterList());
                List<Var> tfields = new ArrayList<>(fields);
                // mix them up and load them for the transform helper:
                List<Var> helperArgs = speciesData.deriveTransformHelperArguments(transformMethods.get(whichtm), whichtm, targs, tfields);
                List<Class<?>> helperTypes = new ArrayList<>(helperArgs.size());
                for (Var ha : helperArgs) {
                    helperTypes.add(ha.basicType.basicTypeClass());
                    if (ha.isInHeap()) {
                        assert(tfields.contains(ha));
                        mv.visitVarInsn(ALOAD, 0);
                        ha.emitFieldInsn(GETFIELD, mv);
                    } else {
                        assert(targs.contains(ha));
                        ha.emitVarInstruction(ALOAD, mv);
                    }
                }

                // jump into the helper (which is probably a factory method)
                final Class<?> rtype = TTYPE.returnType();
                final BasicType rbt = BasicType.basicType(rtype);
                MethodType invokeBasicType = MethodType.methodType(rbt.basicTypeClass(), helperTypes);
                mv.visitMethodInsn(INVOKEVIRTUAL, MH,
                        "invokeBasic", methodSig(invokeBasicType), false);
                if (rbt == BasicType.L_TYPE) {
                    mv.visitTypeInsn(CHECKCAST, classBCName(rtype));
                    mv.visitInsn(ARETURN);
                } else {
                    throw newInternalError("NYI: transform of type "+rtype);
                }
                mv.visitMaxs(0, 0);
                mv.visitEnd();
            }

            cw.visitEnd();

            return cw.toByteArray();
        }

        private int typeLoadOp(char t) {
            switch (t) {
            case 'L': return ALOAD;
            case 'I': return ILOAD;
            case 'J': return LLOAD;
            case 'F': return FLOAD;
            case 'D': return DLOAD;
            default : throw newInternalError("unrecognized type " + t);
            }
        }

        private void emitIntConstant(int con, MethodVisitor mv) {
            if (ICONST_M1 - ICONST_0 <= con && con <= ICONST_5 - ICONST_0)
                mv.visitInsn(ICONST_0 + con);
            else if (con == (byte) con)
                mv.visitIntInsn(BIPUSH, con);
            else if (con == (short) con)
                mv.visitIntInsn(SIPUSH, con);
            else {
                mv.visitLdcInsn(con);
            }

        }

        //
        // Getter MH generation.
        //

        private MethodHandle findGetter(Class<?> speciesCode, List<Class<?>> types, int index) {
            Class<?> fieldType = types.get(index);
            String fieldName = chooseFieldName(fieldType, index);
            try {
                return IMPL_LOOKUP.findGetter(speciesCode, fieldName, fieldType);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw newInternalError(e);
            }
        }

        private List<MethodHandle> findGetters(Class<?> speciesCode, List<Class<?>> types) {
            MethodHandle[] mhs = new MethodHandle[types.size()];
            for (int i = 0; i < mhs.length; ++i) {
                mhs[i] = findGetter(speciesCode, types, i);
                assert(mhs[i].internalMemberName().getDeclaringClass() == speciesCode);
            }
            return List.of(mhs);
        }

        private List<MethodHandle> findFactories(Class<? extends T> speciesCode, List<Class<?>> types) {
            MethodHandle[] mhs = new MethodHandle[1];
            mhs[0] = findFactory(speciesCode, types);
            return List.of(mhs);
        }

        List<LambdaForm.NamedFunction> makeNominalGetters(List<Class<?>> types, List<MethodHandle> getters) {
            LambdaForm.NamedFunction[] nfs = new LambdaForm.NamedFunction[types.size()];
            for (int i = 0; i < nfs.length; ++i) {
                nfs[i] = new LambdaForm.NamedFunction(getters.get(i));
            }
            return List.of(nfs);
        }

        //
        // Auxiliary methods.
        //

        protected void linkSpeciesDataToCode(ClassSpecializer<T,K,S>.SpeciesData speciesData, Class<? extends T> speciesCode) {
            speciesData.speciesCode = speciesCode.asSubclass(topClass);
            final List<Class<?>> types = speciesData.fieldTypes;
            speciesData.factories = this.findFactories(speciesCode, types);
            speciesData.getters = this.findGetters(speciesCode, types);
            speciesData.nominalGetters = this.makeNominalGetters(types, speciesData.getters);
        }

        private Field reflectSDField(Class<? extends T> speciesCode) {
            final Field field = reflectField(speciesCode, sdFieldName);
            assert(field.getType() == metaType);
            assert(Modifier.isStatic(field.getModifiers()));
            return field;
        }

        private S readSpeciesDataFromCode(Class<? extends T> speciesCode) {
            try {
                MemberName sdField = IMPL_LOOKUP.resolveOrFail(REF_getStatic, speciesCode, sdFieldName, metaType);
                Object base = MethodHandleNatives.staticFieldBase(sdField);
                long offset = MethodHandleNatives.staticFieldOffset(sdField);
                UNSAFE.loadFence();
                return metaType.cast(UNSAFE.getObject(base, offset));
            } catch (Error err) {
                throw err;
            } catch (Exception ex) {
                throw newInternalError("Failed to load speciesData from speciesCode: " + speciesCode.getName(), ex);
            } catch (Throwable t) {
                throw uncaughtException(t);
            }
        }

        protected S loadSpeciesDataFromCode(Class<? extends T> speciesCode) {
            if (speciesCode == topClass()) {
                return topSpecies;
            }
            S result = readSpeciesDataFromCode(speciesCode);
            if (result.outer() != ClassSpecializer.this) {
                throw newInternalError("wrong class");
            }
            return result;
        }

        protected void linkCodeToSpeciesData(Class<? extends T> speciesCode, ClassSpecializer<T,K,S>.SpeciesData speciesData, boolean salvage) {
            try {
                assert(readSpeciesDataFromCode(speciesCode) == null ||
                    (salvage && readSpeciesDataFromCode(speciesCode).equals(speciesData)));

                MemberName sdField = IMPL_LOOKUP.resolveOrFail(REF_putStatic, speciesCode, sdFieldName, metaType);
                Object base = MethodHandleNatives.staticFieldBase(sdField);
                long offset = MethodHandleNatives.staticFieldOffset(sdField);
                UNSAFE.storeFence();
                UNSAFE.putObject(base, offset, speciesData);
                UNSAFE.storeFence();
            } catch (Error err) {
                throw err;
            } catch (Exception ex) {
                throw newInternalError("Failed to link speciesData to speciesCode: " + speciesCode.getName(), ex);
            } catch (Throwable t) {
                throw uncaughtException(t);
            }
        }

        /**
         * Field names in concrete species classes adhere to this pattern:
         * type + index, where type is a single character (L, I, J, F, D).
         * The factory subclass can customize this.
         * The name is purely cosmetic, since it applies to a private field.
         */
        protected String chooseFieldName(Class<?> type, int index) {
            BasicType bt = BasicType.basicType(type);
            return "" + bt.basicTypeChar() + index;
        }

        MethodHandle findFactory(Class<? extends T> speciesCode, List<Class<?>> types) {
            final MethodType type = baseConstructorType().changeReturnType(topClass()).appendParameterTypes(types);
            try {
                return IMPL_LOOKUP.findStatic(speciesCode, "make", type);
            } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | TypeNotPresentException e) {
                throw newInternalError(e);
            }
        }
    }

    /** Hook that virtualizes the Factory class, allowing subclasses to extend it. */
    protected Factory makeFactory() {
        return new Factory();
    }


    // Other misc helpers:
    private static final String MH = "java/lang/invoke/MethodHandle";
    private static final String MH_SIG = "L" + MH + ";";
    private static final String STABLE = "jdk/internal/vm/annotation/Stable";
    private static final String STABLE_SIG = "L" + STABLE + ";";
    private static final String[] E_THROWABLE = new String[] { "java/lang/Throwable" };
    static {
        assert(MH_SIG.equals(classSig(MethodHandle.class)));
        assert(MH.equals(classBCName(MethodHandle.class)));
    }

    static String methodSig(MethodType mt) {
        return mt.toMethodDescriptorString();
    }
    static String classSig(Class<?> cls) {
        if (cls.isPrimitive() || cls.isArray())
            return MethodType.methodType(cls).toMethodDescriptorString().substring(2);
        return classSig(classBCName(cls));
    }
    static String classSig(String bcName) {
        assert(bcName.indexOf('.') < 0);
        assert(!bcName.endsWith(";"));
        assert(!bcName.startsWith("["));
        return "L" + bcName + ";";
    }
    static String classBCName(Class<?> cls) {
        return classBCName(className(cls));
    }
    static String classBCName(String str) {
        assert(str.indexOf('/') < 0) : str;
        return str.replace('.', '/');
    }
    static String className(Class<?> cls) {
        assert(!cls.isArray() && !cls.isPrimitive());
        return cls.getName();
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\ConstantBootstraps.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2017, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang.invoke;

import sun.invoke.util.Wrapper;

import static java.lang.invoke.MethodHandleNatives.mapLookupExceptionToError;
import static java.util.Objects.requireNonNull;

/**
 * Bootstrap methods for dynamically-computed constants.
 *
 * <p>The bootstrap methods in this class will throw a
 * {@code NullPointerException} for any reference argument that is {@code null},
 * unless the argument is specified to be unused or specified to accept a
 * {@code null} value.
 *
 * @since 11
 */
public final class ConstantBootstraps {
    // implements the upcall from the JVM, MethodHandleNatives.linkDynamicConstant:
    /*non-public*/
    static Object makeConstant(MethodHandle bootstrapMethod,
                               // Callee information:
                               String name, Class<?> type,
                               // Extra arguments for BSM, if any:
                               Object info,
                               // Caller information:
                               Class<?> callerClass) {
        // Restrict bootstrap methods to those whose first parameter is Lookup
        // The motivation here is, in the future, to possibly support BSMs
        // that do not accept the meta-data of lookup/name/type, thereby
        // allowing the co-opting of existing methods to be used as BSMs as
        // long as the static arguments can be passed as method arguments
        MethodType mt = bootstrapMethod.type();
        if (mt.parameterCount() < 2 ||
            !MethodHandles.Lookup.class.isAssignableFrom(mt.parameterType(0))) {
            throw new BootstrapMethodError(
                    "Invalid bootstrap method declared for resolving a dynamic constant: " + bootstrapMethod);
        }

        // BSMI.invoke handles all type checking and exception translation.
        // If type is not a reference type, the JVM is expecting a boxed
        // version, and will manage unboxing on the other side.
        return BootstrapMethodInvoker.invoke(
                type, bootstrapMethod, name, type, info, callerClass);
    }

    /**
     * Returns a {@code null} object reference for the reference type specified
     * by {@code type}.
     *
     * @param lookup unused
     * @param name unused
     * @param type a reference type
     * @return a {@code null} value
     * @throws IllegalArgumentException if {@code type} is not a reference type
     */
    public static Object nullConstant(MethodHandles.Lookup lookup, String name, Class<?> type) {
        if (requireNonNull(type).isPrimitive()) {
            throw new IllegalArgumentException(String.format("not reference: %s", type));
        }

        return null;
    }

    /**
     * Returns a {@link Class} mirror for the primitive type whose type
     * descriptor is specified by {@code name}.
     *
     * @param lookup unused
     * @param name the descriptor (JVMS 4.3) of the desired primitive type
     * @param type the required result type (must be {@code Class.class})
     * @return the {@link Class} mirror
     * @throws IllegalArgumentException if the name is not a descriptor for a
     * primitive type or the type is not {@code Class.class}
     */
    public static Class<?> primitiveClass(MethodHandles.Lookup lookup, String name, Class<?> type) {
        requireNonNull(name);
        requireNonNull(type);
        if (type != Class.class) {
            throw new IllegalArgumentException();
        }
        if (name.length() == 0 || name.length() > 1) {
            throw new IllegalArgumentException(String.format("not primitive: %s", name));
        }

        return Wrapper.forPrimitiveType(name.charAt(0)).primitiveType();
    }

    /**
     * Returns an {@code enum} constant of the type specified by {@code type}
     * with the name specified by {@code name}.
     *
     * @param lookup the lookup context describing the class performing the
     * operation (normally stacked by the JVM)
     * @param name the name of the constant to return, which must exactly match
     * an enum constant in the specified type.
     * @param type the {@code Class} object describing the enum type for which
     * a constant is to be returned
     * @param <E> The enum type for which a constant value is to be returned
     * @return the enum constant of the specified enum type with the
     * specified name
     * @throws IllegalAccessError if the declaring class or the field is not
     * accessible to the class performing the operation
     * @throws IllegalArgumentException if the specified enum type has
     * no constant with the specified name, or the specified
     * class object does not represent an enum type
     * @see Enum#valueOf(Class, String)
     */
    public static <E extends Enum<E>> E enumConstant(MethodHandles.Lookup lookup, String name, Class<E> type) {
        requireNonNull(lookup);
        requireNonNull(name);
        requireNonNull(type);
        validateClassAccess(lookup, type);

        return Enum.valueOf(type, name);
    }

    /**
     * Returns the value of a static final field.
     *
     * @param lookup the lookup context describing the class performing the
     * operation (normally stacked by the JVM)
     * @param name the name of the field
     * @param type the type of the field
     * @param declaringClass the class in which the field is declared
     * @return the value of the field
     * @throws IllegalAccessError if the declaring class or the field is not
     * accessible to the class performing the operation
     * @throws NoSuchFieldError if the specified field does not exist
     * @throws IncompatibleClassChangeError if the specified field is not
     * {@code final}
     */
    public static Object getStaticFinal(MethodHandles.Lookup lookup, String name, Class<?> type,
                                        Class<?> declaringClass) {
        requireNonNull(lookup);
        requireNonNull(name);
        requireNonNull(type);
        requireNonNull(declaringClass);

        MethodHandle mh;
        try {
            mh = lookup.findStaticGetter(declaringClass, name, type);
            MemberName member = mh.internalMemberName();
            if (!member.isFinal()) {
                throw new IncompatibleClassChangeError("not a final field: " + name);
            }
        }
        catch (ReflectiveOperationException ex) {
            throw mapLookupExceptionToError(ex);
        }

        // Since mh is a handle to a static field only instances of
        // VirtualMachineError are anticipated to be thrown, such as a
        // StackOverflowError or an InternalError from the j.l.invoke code
        try {
            return mh.invoke();
        }
        catch (RuntimeException | Error e) {
            throw e;
        }
        catch (Throwable e) {
            throw new LinkageError("Unexpected throwable", e);
        }
    }

    /**
     * Returns the value of a static final field declared in the class which
     * is the same as the field's type (or, for primitive-valued fields,
     * declared in the wrapper class.)  This is a simplified form of
     * {@link #getStaticFinal(MethodHandles.Lookup, String, Class, Class)}
     * for the case where a class declares distinguished constant instances of
     * itself.
     *
     * @param lookup the lookup context describing the class performing the
     * operation (normally stacked by the JVM)
     * @param name the name of the field
     * @param type the type of the field
     * @return the value of the field
     * @throws IllegalAccessError if the declaring class or the field is not
     * accessible to the class performing the operation
     * @throws NoSuchFieldError if the specified field does not exist
     * @throws IncompatibleClassChangeError if the specified field is not
     * {@code final}
     * @see #getStaticFinal(MethodHandles.Lookup, String, Class, Class)
     */
    public static Object getStaticFinal(MethodHandles.Lookup lookup, String name, Class<?> type) {
        requireNonNull(type);

        Class<?> declaring = type.isPrimitive()
                             ? Wrapper.forPrimitiveType(type).wrapperType()
                             : type;
        return getStaticFinal(lookup, name, type, declaring);
    }


    /**
     * Returns the result of invoking a method handle with the provided
     * arguments.
     * <p>
     * This method behaves as if the method handle to be invoked is the result
     * of adapting the given method handle, via {@link MethodHandle#asType}, to
     * adjust the return type to the desired type.
     *
     * @param lookup unused
     * @param name unused
     * @param type the desired type of the value to be returned, which must be
     * compatible with the return type of the method handle
     * @param handle the method handle to be invoked
     * @param args the arguments to pass to the method handle, as if with
     * {@link MethodHandle#invokeWithArguments}.  Each argument may be
     * {@code null}.
     * @return the result of invoking the method handle
     * @throws WrongMethodTypeException if the handle's method type cannot be
     * adjusted to take the given number of arguments, or if the handle's return
     * type cannot be adjusted to the desired type
     * @throws ClassCastException if an argument or the result produced by
     * invoking the handle cannot be converted by reference casting
     * @throws Throwable anything thrown by the method handle invocation
     */
    public static Object invoke(MethodHandles.Lookup lookup, String name, Class<?> type,
                                MethodHandle handle, Object... args) throws Throwable {
        requireNonNull(type);
        requireNonNull(handle);
        requireNonNull(args);

        if (type != handle.type().returnType()) {
            // Adjust the return type of the handle to be invoked while
            // preserving variable arity if present
            handle = handle.asType(handle.type().changeReturnType(type)).
                    withVarargs(handle.isVarargsCollector());
        }

        return handle.invokeWithArguments(args);
    }

    /**
     * Finds a {@link VarHandle} for an instance field.
     *
     * @param lookup the lookup context describing the class performing the
     * operation (normally stacked by the JVM)
     * @param name the name of the field
     * @param type the required result type (must be {@code Class<VarHandle>})
     * @param declaringClass the class in which the field is declared
     * @param fieldType the type of the field
     * @return the {@link VarHandle}
     * @throws IllegalAccessError if the declaring class or the field is not
     * accessible to the class performing the operation
     * @throws NoSuchFieldError if the specified field does not exist
     * @throws IllegalArgumentException if the type is not {@code VarHandle}
     */
    public static VarHandle fieldVarHandle(MethodHandles.Lookup lookup, String name, Class<VarHandle> type,
                                           Class<?> declaringClass, Class<?> fieldType) {
        requireNonNull(lookup);
        requireNonNull(name);
        requireNonNull(type);
        requireNonNull(declaringClass);
        requireNonNull(fieldType);
        if (type != VarHandle.class) {
            throw new IllegalArgumentException();
        }

        try {
            return lookup.findVarHandle(declaringClass, name, fieldType);
        }
        catch (ReflectiveOperationException e) {
            throw mapLookupExceptionToError(e);
        }
    }

    /**
     * Finds a {@link VarHandle} for a static field.
     *
     * @param lookup the lookup context describing the class performing the
     * operation (normally stacked by the JVM)
     * @param name the name of the field
     * @param type the required result type (must be {@code Class<VarHandle>})
     * @param declaringClass the class in which the field is declared
     * @param fieldType the type of the field
     * @return the {@link VarHandle}
     * @throws IllegalAccessError if the declaring class or the field is not
     * accessible to the class performing the operation
     * @throws NoSuchFieldError if the specified field does not exist
     * @throws IllegalArgumentException if the type is not {@code VarHandle}
     */
    public static VarHandle staticFieldVarHandle(MethodHandles.Lookup lookup, String name, Class<VarHandle> type,
                                                 Class<?> declaringClass, Class<?> fieldType) {
        requireNonNull(lookup);
        requireNonNull(name);
        requireNonNull(type);
        requireNonNull(declaringClass);
        requireNonNull(fieldType);
        if (type != VarHandle.class) {
            throw new IllegalArgumentException();
        }

        try {
            return lookup.findStaticVarHandle(declaringClass, name, fieldType);
        }
        catch (ReflectiveOperationException e) {
            throw mapLookupExceptionToError(e);
        }
    }

    /**
     * Finds a {@link VarHandle} for an array type.
     *
     * @param lookup the lookup context describing the class performing the
     * operation (normally stacked by the JVM)
     * @param name unused
     * @param type the required result type (must be {@code Class<VarHandle>})
     * @param arrayClass the type of the array
     * @return the {@link VarHandle}
     * @throws IllegalAccessError if the component type of the array is not
     * accessible to the class performing the operation
     * @throws IllegalArgumentException if the type is not {@code VarHandle}
     */
    public static VarHandle arrayVarHandle(MethodHandles.Lookup lookup, String name, Class<VarHandle> type,
                                           Class<?> arrayClass) {
        requireNonNull(lookup);
        requireNonNull(type);
        requireNonNull(arrayClass);
        if (type != VarHandle.class) {
            throw new IllegalArgumentException();
        }

        return MethodHandles.arrayElementVarHandle(validateClassAccess(lookup, arrayClass));
    }

    private static <T> Class<T> validateClassAccess(MethodHandles.Lookup lookup, Class<T> type) {
        try {
            lookup.accessClass(type);
            return type;
        }
        catch (ReflectiveOperationException ex) {
            throw mapLookupExceptionToError(ex);
        }
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\ConstantCallSite.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2010, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

/**
 * A {@code ConstantCallSite} is a {@link CallSite} whose target is permanent, and can never be changed.
 * An {@code invokedynamic} instruction linked to a {@code ConstantCallSite} is permanently
 * bound to the call site's target.
 * @author John Rose, JSR 292 EG
 * @since 1.7
 */
public class ConstantCallSite extends CallSite {
    private final boolean isFrozen;

    /**
     * Creates a call site with a permanent target.
     * @param target the target to be permanently associated with this call site
     * @throws NullPointerException if the proposed target is null
     */
    public ConstantCallSite(MethodHandle target) {
        super(target);
        isFrozen = true;
    }

    /**
     * Creates a call site with a permanent target, possibly bound to the call site itself.
     * <p>
     * During construction of the call site, the {@code createTargetHook} is invoked to
     * produce the actual target, as if by a call of the form
     * {@code (MethodHandle) createTargetHook.invoke(this)}.
     * <p>
     * Note that user code cannot perform such an action directly in a subclass constructor,
     * since the target must be fixed before the {@code ConstantCallSite} constructor returns.
     * <p>
     * The hook is said to bind the call site to a target method handle,
     * and a typical action would be {@code someTarget.bindTo(this)}.
     * However, the hook is free to take any action whatever,
     * including ignoring the call site and returning a constant target.
     * <p>
     * The result returned by the hook must be a method handle of exactly
     * the same type as the call site.
     * <p>
     * While the hook is being called, the new {@code ConstantCallSite}
     * object is in a partially constructed state.
     * In this state,
     * a call to {@code getTarget}, or any other attempt to use the target,
     * will result in an {@code IllegalStateException}.
     * It is legal at all times to obtain the call site's type using the {@code type} method.
     *
     * @param targetType the type of the method handle to be permanently associated with this call site
     * @param createTargetHook a method handle to invoke (on the call site) to produce the call site's target
     * @throws WrongMethodTypeException if the hook cannot be invoked on the required arguments,
     *         or if the target returned by the hook is not of the given {@code targetType}
     * @throws NullPointerException if the hook returns a null value
     * @throws ClassCastException if the hook returns something other than a {@code MethodHandle}
     * @throws Throwable anything else thrown by the hook function
     */
    protected ConstantCallSite(MethodType targetType, MethodHandle createTargetHook) throws Throwable {
        super(targetType, createTargetHook);
        isFrozen = true;
    }

    /**
     * Returns the target method of the call site, which behaves
     * like a {@code final} field of the {@code ConstantCallSite}.
     * That is, the target is always the original value passed
     * to the constructor call which created this instance.
     *
     * @return the immutable linkage state of this call site, a constant method handle
     * @throws IllegalStateException if the {@code ConstantCallSite} constructor has not completed
     */
    @Override public final MethodHandle getTarget() {
        if (!isFrozen)  throw new IllegalStateException();
        return target;
    }

    /**
     * Always throws an {@link UnsupportedOperationException}.
     * This kind of call site cannot change its target.
     * @param ignore a new target proposed for the call site, which is ignored
     * @throws UnsupportedOperationException because this kind of call site cannot change its target
     */
    @Override public final void setTarget(MethodHandle ignore) {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns this call site's permanent target.
     * Since that target will never change, this is a correct implementation
     * of {@link CallSite#dynamicInvoker CallSite.dynamicInvoker}.
     * @return the immutable linkage state of this call site, a constant method handle
     * @throws IllegalStateException if the {@code ConstantCallSite} constructor has not completed
     */
    @Override
    public final MethodHandle dynamicInvoker() {
        return getTarget();
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\ConstantGroup.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.IntFunction;

/**
 * An ordered sequence of constants, some of which may not yet
 * be present.  This type is used by {@link BootstrapCallInfo}
 * to represent the sequence of bootstrap arguments associated
 * with a bootstrap method, without forcing their immediate
 * resolution.
 * <p>
 * If you use the
 * {@linkplain ConstantGroup#get(int) simple get method},
 * the constant will be resolved, if this has not already
 * happened.  An occasional side effect of resolution is a
 * {@code LinkageError}, which happens if the system
 * could not resolve the constant in question.
 * <p>
 * In order to peek at a constant without necessarily
 * resolving it, use the
 * {@linkplain ConstantGroup#get(int,Object)
 * non-throwing get method}.
 * This method will never throw a resolution error.
 * Instead, if the resolution would result in an error,
 * or if the implementation elects not to attempt
 * resolution at this point, then the method will
 * return the user-supplied sentinel value.
 * <p>
 * To iterate through the constants, resolving as you go,
 * use the iterator provided on the {@link List}-typed view.
 * If you supply a sentinel, resolution will be suppressed.
 * <p>
 * Typically the constant is drawn from a constant pool entry
 * in the virtual machine. Constant pool entries undergo a
 * one-time state transition from unresolved to resolved,
 * with a permanently recorded result.  Usually that result
 * is the desired constant value, but it may also be an error.
 * In any case, the results displayed by a {@code ConstantGroup}
 * are stable in the same way.  If a query to a particular
 * constant in a {@code ConstantGroup} throws an exception once,
 * it will throw the same kind of exception forever after.
 * If the query returns a constant value once, it will return
 * the same value forever after.
 * <p>
 * The only possible change in the status of a constant is
 * from the unresolved to the resolved state, and that
 * happens exactly once.  A constant will never revert to
 * an unlinked state.  However, from the point of view of
 * this interface, constants may appear to spontaneously
 * resolve.  This is so because constant pools are global
 * structures shared across threads, and because
 * prefetching of some constants may occur, there are no
 * strong guarantees when the virtual machine may resolve
 * constants.
 * <p>
 * When choosing sentinel values, be aware that a constant
 * pool which has {@code CONSTANT_Dynamic} entries
 * can contain potentially any representable value,
 * and arbitrary implementations of {@code ConstantGroup}
 * are also free to produce arbitrary values.
 * This means some obvious choices for sentinel values,
 * such as {@code null}, may sometimes fail to distinguish
 * a resolved from an unresolved constant in the group.
 * The most reliable sentinel is a privately created object,
 * or perhaps the {@code ConstantGroup} itself.
 * @since 1.10
 */
// public
interface ConstantGroup {
    /// Access

    /**
     * Returns the number of constants in this group.
     * This value never changes, for any particular group.
     * @return the number of constants in this group
     */
    int size();

    /**
     * Returns the selected constant, resolving it if necessary.
     * Throws a linkage error if resolution proves impossible.
     * @param index which constant to select
     * @return the selected constant
     * @throws LinkageError if the selected constant needs resolution and cannot be resolved
     */
    Object get(int index) throws LinkageError;

    /**
     * Returns the selected constant,
     * or the given sentinel value if there is none available.
     * If the constant cannot be resolved, the sentinel will be returned.
     * If the constant can (perhaps) be resolved, but has not yet been resolved,
     * then the sentinel <em>may</em> be returned, at the implementation's discretion.
     * To force resolution (and a possible exception), call {@link #get(int)}.
     * @param index the selected constant
     * @param ifNotPresent the sentinel value to return if the constant is not present
     * @return the selected constant, if available, else the sentinel value
     */
    Object get(int index, Object ifNotPresent);

    /**
     * Returns an indication of whether a constant may be available.
     * If it returns {@code true}, it will always return true in the future,
     * and a call to {@link #get(int)} will never throw an exception.
     * <p>
     * After a normal return from {@link #get(int)} or a present
     * value is reported from {@link #get(int,Object)}, this method
     * must always return true.
     * <p>
     * If this method returns {@code false}, nothing in particular
     *  can be inferred, since the query only concerns the internal
     * logic of the {@code ConstantGroup} object which ensures that
     a successful * query to a constant will always remain successful.
     * The only way to force a permanent decision about whether
     * a constant is available is to call {@link #get(int)} and
     * be ready for an exception if the constant is unavailable.
     * @param index the selected constant
     * @return {@code true} if the selected constant is known by
     *     this object to be present, {@code false} if it is known
     *     not to be present or
     */
    boolean isPresent(int index);

    /// Views

    /**
     * Create a view on this group as a {@link List} view.
     * Any request for a constant through this view will
     * force resolution.
     * @return a {@code List} view on this group which will force resolution
     */
    default List<Object> asList() {
        return new AbstractConstantGroup.AsList(this, 0, size());
    }

    /**
     * Create a view on this group as a {@link List} view.
     * Any request for a constant through this view will
     * return the given sentinel value, if the corresponding
     * call to {@link #get(int,Object)} would do so.
     * @param ifNotPresent the sentinel value to return if a constant is not present
     * @return a {@code List} view on this group which will not force resolution
     */
    default List<Object> asList(Object ifNotPresent) {
        return new AbstractConstantGroup.AsList(this, 0, size(), ifNotPresent);
    }

    /**
     * Create a view on a sub-sequence of this group.
     * @param start the index to begin the view
     * @param end the index to end the view
     * @return a view on the selected sub-group
     */
    default ConstantGroup subGroup(int start, int end) {
        return new AbstractConstantGroup.SubGroup(this, start, end);
    }

    /// Bulk operations

    /**
     * Copy a sequence of constant values into a given buffer.
     * This is equivalent to {@code end-offset} separate calls to {@code get},
     * for each index in the range from {@code offset} up to but not including {@code end}.
     * For the first constant that cannot be resolved,
     * a {@code LinkageError} is thrown, but only after
     * preceding constant value have been stored.
     * @param start index of first constant to retrieve
     * @param end limiting index of constants to retrieve
     * @param buf array to receive the requested values
     * @param pos position in the array to offset storing the values
     * @return the limiting index, {@code end}
     * @throws LinkageError if a constant cannot be resolved
     */
    default int copyConstants(int start, int end,
                              Object[] buf, int pos)
            throws LinkageError
    {
        int bufBase = pos - start;  // buf[bufBase + i] = get(i)
        for (int i = start; i < end; i++) {
            buf[bufBase + i] = get(i);
        }
        return end;
    }

    /**
     * Copy a sequence of constant values into a given buffer.
     * This is equivalent to {@code end-offset} separate calls to {@code get},
     * for each index in the range from {@code offset} up to but not including {@code end}.
     * Any constants that cannot be resolved are replaced by the
     * given sentinel value.
     * @param start index of first constant to retrieve
     * @param end limiting index of constants to retrieve
     * @param buf array to receive the requested values
     * @param pos position in the array to offset storing the values
     * @param ifNotPresent sentinel value to store if a value is not available
     * @return the limiting index, {@code end}
     * @throws LinkageError if {@code resolve} is true and a constant cannot be resolved
     */
    default int copyConstants(int start, int end,
                              Object[] buf, int pos,
                              Object ifNotPresent) {
        int bufBase = pos - start;  // buf[bufBase + i] = get(i)
        for (int i = start; i < end; i++) {
            buf[bufBase + i] = get(i, ifNotPresent);
        }
        return end;
    }

    /**
     * Make a new constant group with the given constants.
     * The value of {@code ifNotPresent} may be any reference.
     * If this value is encountered as an element of the
     * {@code constants} list, the new constant group will
     * regard that element of the list as logically missing.
     * If the new constant group is called upon to resolve
     * a missing element of the group, it will refer to the
     * given {@code constantProvider}, by calling it on the
     * index of the missing element.
     * The {@code constantProvider} must be stable, in the sense
     * that the outcome of calling it on the same index twice
     * will produce equivalent results.
     * If {@code constantProvider} is the null reference, then
     * it will be treated as if it were a function which raises
     * {@link NoSuchElementException}.
     * @param constants the elements of this constant group
     * @param ifNotPresent sentinel value provided instead of a missing constant
     * @param constantProvider function to call when a missing constant is resolved
     * @return a new constant group with the given constants and resolution behavior
     */
    static ConstantGroup makeConstantGroup(List<Object> constants,
                                           Object ifNotPresent,
                                           IntFunction<Object> constantProvider) {
        class Impl extends AbstractConstantGroup.WithCache {
            Impl() {
                super(constants.size());
                initializeCache(constants, ifNotPresent);
            }
            @Override
            Object fillCache(int index) {
                if (constantProvider == null)  super.fillCache(index);
                return constantProvider.apply(index);
            }
        }
        return new Impl();
    }

    /**
     * Make a new constant group with the given constant values.
     * The constants will be copied from the given list into the
     * new constant group, forcing resolution if any are missing.
     * @param constants the constants of this constant group
     * @return a new constant group with the given constants
     */
    static ConstantGroup makeConstantGroup(List<Object> constants) {
        final Object NP = AbstractConstantGroup.WithCache.NOT_PRESENT;
        assert(!constants.contains(NP));  // secret value
        return makeConstantGroup(constants, NP, null);
    }

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\DelegatingMethodHandle.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import java.util.Arrays;
import static java.lang.invoke.LambdaForm.*;
import static java.lang.invoke.LambdaForm.Kind.*;
import static java.lang.invoke.MethodHandleNatives.Constants.REF_invokeVirtual;
import static java.lang.invoke.MethodHandleStatics.*;

/**
 * A method handle whose invocation behavior is determined by a target.
 * The delegating MH itself can hold extra "intentions" beyond the simple behavior.
 * @author jrose
 */
/*non-public*/
abstract class DelegatingMethodHandle extends MethodHandle {
    protected DelegatingMethodHandle(MethodHandle target) {
        this(target.type(), target);
    }

    protected DelegatingMethodHandle(MethodType type, MethodHandle target) {
        super(type, chooseDelegatingForm(target));
    }

    protected DelegatingMethodHandle(MethodType type, LambdaForm form) {
        super(type, form);
    }

    /** Define this to extract the delegated target which supplies the invocation behavior. */
    protected abstract MethodHandle getTarget();

    @Override
    abstract MethodHandle asTypeUncached(MethodType newType);

    @Override
    MemberName internalMemberName() {
        return getTarget().internalMemberName();
    }

    @Override
    boolean isInvokeSpecial() {
        return getTarget().isInvokeSpecial();
    }

    @Override
    Class<?> internalCallerClass() {
        return getTarget().internalCallerClass();
    }

    @Override
    MethodHandle copyWith(MethodType mt, LambdaForm lf) {
        // FIXME: rethink 'copyWith' protocol; it is too low-level for use on all MHs
        throw newIllegalArgumentException("do not use this");
    }

    @Override
    String internalProperties() {
        return "\n& Class="+getClass().getSimpleName()+
               "\n& Target="+getTarget().debugString();
    }

    @Override
    BoundMethodHandle rebind() {
        return getTarget().rebind();
    }

    private static LambdaForm chooseDelegatingForm(MethodHandle target) {
        if (target instanceof SimpleMethodHandle)
            return target.internalForm();  // no need for an indirection
        return makeReinvokerForm(target, MethodTypeForm.LF_DELEGATE, DelegatingMethodHandle.class, NF_getTarget);
    }

    static LambdaForm makeReinvokerForm(MethodHandle target,
                                        int whichCache,
                                        Object constraint,
                                        NamedFunction getTargetFn) {
        // No pre-action needed.
        return makeReinvokerForm(target, whichCache, constraint, true, getTargetFn, null);
    }
    /** Create a LF which simply reinvokes a target of the given basic type. */
    static LambdaForm makeReinvokerForm(MethodHandle target,
                                        int whichCache,
                                        Object constraint,
                                        boolean forceInline,
                                        NamedFunction getTargetFn,
                                        NamedFunction preActionFn) {
        MethodType mtype = target.type().basicType();
        Kind kind = whichKind(whichCache);
        boolean customized = (whichCache < 0 ||
                mtype.parameterSlotCount() > MethodType.MAX_MH_INVOKER_ARITY);
        boolean hasPreAction = (preActionFn != null);
        LambdaForm form;
        if (!customized) {
            form = mtype.form().cachedLambdaForm(whichCache);
            if (form != null)  return form;
        }
        final int THIS_DMH    = 0;
        final int ARG_BASE    = 1;
        final int ARG_LIMIT   = ARG_BASE + mtype.parameterCount();
        int nameCursor = ARG_LIMIT;
        final int PRE_ACTION   = hasPreAction ? nameCursor++ : -1;
        final int NEXT_MH     = customized ? -1 : nameCursor++;
        final int REINVOKE    = nameCursor++;
        LambdaForm.Name[] names = LambdaForm.arguments(nameCursor - ARG_LIMIT, mtype.invokerType());
        assert(names.length == nameCursor);
        names[THIS_DMH] = names[THIS_DMH].withConstraint(constraint);
        Object[] targetArgs;
        if (hasPreAction) {
            names[PRE_ACTION] = new LambdaForm.Name(preActionFn, names[THIS_DMH]);
        }
        if (customized) {
            targetArgs = Arrays.copyOfRange(names, ARG_BASE, ARG_LIMIT, Object[].class);
            names[REINVOKE] = new LambdaForm.Name(target, targetArgs);  // the invoker is the target itself
        } else {
            names[NEXT_MH] = new LambdaForm.Name(getTargetFn, names[THIS_DMH]);
            targetArgs = Arrays.copyOfRange(names, THIS_DMH, ARG_LIMIT, Object[].class);
            targetArgs[0] = names[NEXT_MH];  // overwrite this MH with next MH
            names[REINVOKE] = new LambdaForm.Name(mtype, targetArgs);
        }
        form = new LambdaForm(ARG_LIMIT, names, forceInline, kind);
        if (!customized) {
            form = mtype.form().setCachedLambdaForm(whichCache, form);
        }
        return form;
    }

    private static Kind whichKind(int whichCache) {
        switch(whichCache) {
            case MethodTypeForm.LF_REBIND:   return BOUND_REINVOKER;
            case MethodTypeForm.LF_DELEGATE: return DELEGATE;
            default:                         return REINVOKER;
        }
    }

    static final NamedFunction NF_getTarget;
    static {
        try {
            MemberName member = new MemberName(DelegatingMethodHandle.class, "getTarget",
                    MethodType.methodType(MethodHandle.class), REF_invokeVirtual);
            NF_getTarget = new NamedFunction(
                    MemberName.getFactory()
                            .resolveOrFail(REF_invokeVirtual, member, DelegatingMethodHandle.class, NoSuchMethodException.class));
        } catch (ReflectiveOperationException ex) {
            throw newInternalError(ex);
        }
        // The Holder class will contain pre-generated DelegatingMethodHandles resolved
        // speculatively using MemberName.getFactory().resolveOrNull. However, that
        // doesn't initialize the class, which subtly breaks inlining etc. By forcing
        // initialization of the Holder class we avoid these issues.
        UNSAFE.ensureClassInitialized(Holder.class);
    }

    /* Placeholder class for DelegatingMethodHandles generated ahead of time */
    final class Holder {}
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\DirectMethodHandle.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2008, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import jdk.internal.misc.Unsafe;
import jdk.internal.vm.annotation.ForceInline;
import jdk.internal.vm.annotation.Stable;
import sun.invoke.util.ValueConversions;
import sun.invoke.util.VerifyAccess;
import sun.invoke.util.VerifyType;
import sun.invoke.util.Wrapper;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Objects;

import static java.lang.invoke.LambdaForm.*;
import static java.lang.invoke.LambdaForm.Kind.*;
import static java.lang.invoke.MethodHandleNatives.Constants.*;
import static java.lang.invoke.MethodHandleStatics.UNSAFE;
import static java.lang.invoke.MethodHandleStatics.newInternalError;
import static java.lang.invoke.MethodTypeForm.*;

/**
 * The flavor of method handle which implements a constant reference
 * to a class member.
 * @author jrose
 */
class DirectMethodHandle extends MethodHandle {
    final MemberName member;

    // Constructors and factory methods in this class *must* be package scoped or private.
    private DirectMethodHandle(MethodType mtype, LambdaForm form, MemberName member) {
        super(mtype, form);
        if (!member.isResolved())  throw new InternalError();

        if (member.getDeclaringClass().isInterface() &&
            member.getReferenceKind() == REF_invokeInterface &&
            member.isMethod() && !member.isAbstract()) {
            // Check for corner case: invokeinterface of Object method
            MemberName m = new MemberName(Object.class, member.getName(), member.getMethodType(), member.getReferenceKind());
            m = MemberName.getFactory().resolveOrNull(m.getReferenceKind(), m, null);
            if (m != null && m.isPublic()) {
                assert(member.getReferenceKind() == m.getReferenceKind());  // else this.form is wrong
                member = m;
            }
        }

        this.member = member;
    }

    // Factory methods:
    static DirectMethodHandle make(byte refKind, Class<?> refc, MemberName member, Class<?> callerClass) {
        MethodType mtype = member.getMethodOrFieldType();
        if (!member.isStatic()) {
            if (!member.getDeclaringClass().isAssignableFrom(refc) || member.isConstructor())
                throw new InternalError(member.toString());
            mtype = mtype.insertParameterTypes(0, refc);
        }
        if (!member.isField()) {
            // refKind reflects the original type of lookup via findSpecial or
            // findVirtual etc.
            switch (refKind) {
                case REF_invokeSpecial: {
                    member = member.asSpecial();
                    // if caller is an interface we need to adapt to get the
                    // receiver check inserted
                    if (callerClass == null) {
                        throw new InternalError("callerClass must not be null for REF_invokeSpecial");
                    }
                    LambdaForm lform = preparedLambdaForm(member, callerClass.isInterface());
                    return new Special(mtype, lform, member, callerClass);
                }
                case REF_invokeInterface: {
                    // for interfaces we always need the receiver typecheck,
                    // so we always pass 'true' to ensure we adapt if needed
                    // to include the REF_invokeSpecial case
                    LambdaForm lform = preparedLambdaForm(member, true);
                    return new Interface(mtype, lform, member, refc);
                }
                default: {
                    LambdaForm lform = preparedLambdaForm(member);
                    return new DirectMethodHandle(mtype, lform, member);
                }
            }
        } else {
            LambdaForm lform = preparedFieldLambdaForm(member);
            if (member.isStatic()) {
                long offset = MethodHandleNatives.staticFieldOffset(member);
                Object base = MethodHandleNatives.staticFieldBase(member);
                return new StaticAccessor(mtype, lform, member, base, offset);
            } else {
                long offset = MethodHandleNatives.objectFieldOffset(member);
                assert(offset == (int)offset);
                return new Accessor(mtype, lform, member, (int)offset);
            }
        }
    }
    static DirectMethodHandle make(Class<?> refc, MemberName member) {
        byte refKind = member.getReferenceKind();
        if (refKind == REF_invokeSpecial)
            refKind =  REF_invokeVirtual;
        return make(refKind, refc, member, null /* no callerClass context */);
    }
    static DirectMethodHandle make(MemberName member) {
        if (member.isConstructor())
            return makeAllocator(member);
        return make(member.getDeclaringClass(), member);
    }
    private static DirectMethodHandle makeAllocator(MemberName ctor) {
        assert(ctor.isConstructor() && ctor.getName().equals("<init>"));
        Class<?> instanceClass = ctor.getDeclaringClass();
        ctor = ctor.asConstructor();
        assert(ctor.isConstructor() && ctor.getReferenceKind() == REF_newInvokeSpecial) : ctor;
        MethodType mtype = ctor.getMethodType().changeReturnType(instanceClass);
        LambdaForm lform = preparedLambdaForm(ctor);
        MemberName init = ctor.asSpecial();
        assert(init.getMethodType().returnType() == void.class);
        return new Constructor(mtype, lform, ctor, init, instanceClass);
    }

    @Override
    BoundMethodHandle rebind() {
        return BoundMethodHandle.makeReinvoker(this);
    }

    @Override
    MethodHandle copyWith(MethodType mt, LambdaForm lf) {
        assert(this.getClass() == DirectMethodHandle.class);  // must override in subclasses
        return new DirectMethodHandle(mt, lf, member);
    }

    @Override
    String internalProperties() {
        return "\n& DMH.MN="+internalMemberName();
    }

    //// Implementation methods.
    @Override
    @ForceInline
    MemberName internalMemberName() {
        return member;
    }

    private static final MemberName.Factory IMPL_NAMES = MemberName.getFactory();

    /**
     * Create a LF which can invoke the given method.
     * Cache and share this structure among all methods with
     * the same basicType and refKind.
     */
    private static LambdaForm preparedLambdaForm(MemberName m, boolean adaptToSpecialIfc) {
        assert(m.isInvocable()) : m;  // call preparedFieldLambdaForm instead
        MethodType mtype = m.getInvocationType().basicType();
        assert(!m.isMethodHandleInvoke()) : m;
        int which;
        // MemberName.getReferenceKind represents the JVM optimized form of the call
        // as distinct from the "kind" passed to DMH.make which represents the original
        // bytecode-equivalent request. Specifically private/final methods that use a direct
        // call have getReferenceKind adapted to REF_invokeSpecial, even though the actual
        // invocation mode may be invokevirtual or invokeinterface.
        switch (m.getReferenceKind()) {
        case REF_invokeVirtual:    which = LF_INVVIRTUAL;    break;
        case REF_invokeStatic:     which = LF_INVSTATIC;     break;
        case REF_invokeSpecial:    which = LF_INVSPECIAL;    break;
        case REF_invokeInterface:  which = LF_INVINTERFACE;  break;
        case REF_newInvokeSpecial: which = LF_NEWINVSPECIAL; break;
        default:  throw new InternalError(m.toString());
        }
        if (which == LF_INVSTATIC && shouldBeInitialized(m)) {
            // precompute the barrier-free version:
            preparedLambdaForm(mtype, which);
            which = LF_INVSTATIC_INIT;
        }
        if (which == LF_INVSPECIAL && adaptToSpecialIfc) {
            which = LF_INVSPECIAL_IFC;
        }
        LambdaForm lform = preparedLambdaForm(mtype, which);
        maybeCompile(lform, m);
        assert(lform.methodType().dropParameterTypes(0, 1)
                .equals(m.getInvocationType().basicType()))
                : Arrays.asList(m, m.getInvocationType().basicType(), lform, lform.methodType());
        return lform;
    }

    private static LambdaForm preparedLambdaForm(MemberName m) {
        return preparedLambdaForm(m, false);
    }

    private static LambdaForm preparedLambdaForm(MethodType mtype, int which) {
        LambdaForm lform = mtype.form().cachedLambdaForm(which);
        if (lform != null)  return lform;
        lform = makePreparedLambdaForm(mtype, which);
        return mtype.form().setCachedLambdaForm(which, lform);
    }

    static LambdaForm makePreparedLambdaForm(MethodType mtype, int which) {
        boolean needsInit = (which == LF_INVSTATIC_INIT);
        boolean doesAlloc = (which == LF_NEWINVSPECIAL);
        boolean needsReceiverCheck = (which == LF_INVINTERFACE ||
                                      which == LF_INVSPECIAL_IFC);

        String linkerName;
        LambdaForm.Kind kind;
        switch (which) {
        case LF_INVVIRTUAL:    linkerName = "linkToVirtual";   kind = DIRECT_INVOKE_VIRTUAL;     break;
        case LF_INVSTATIC:     linkerName = "linkToStatic";    kind = DIRECT_INVOKE_STATIC;      break;
        case LF_INVSTATIC_INIT:linkerName = "linkToStatic";    kind = DIRECT_INVOKE_STATIC_INIT; break;
        case LF_INVSPECIAL_IFC:linkerName = "linkToSpecial";   kind = DIRECT_INVOKE_SPECIAL_IFC; break;
        case LF_INVSPECIAL:    linkerName = "linkToSpecial";   kind = DIRECT_INVOKE_SPECIAL;     break;
        case LF_INVINTERFACE:  linkerName = "linkToInterface"; kind = DIRECT_INVOKE_INTERFACE;   break;
        case LF_NEWINVSPECIAL: linkerName = "linkToSpecial";   kind = DIRECT_NEW_INVOKE_SPECIAL; break;
        default:  throw new InternalError("which="+which);
        }

        MethodType mtypeWithArg = mtype.appendParameterTypes(MemberName.class);
        if (doesAlloc)
            mtypeWithArg = mtypeWithArg
                    .insertParameterTypes(0, Object.class)  // insert newly allocated obj
                    .changeReturnType(void.class);          // <init> returns void
        MemberName linker = new MemberName(MethodHandle.class, linkerName, mtypeWithArg, REF_invokeStatic);
        try {
            linker = IMPL_NAMES.resolveOrFail(REF_invokeStatic, linker, null, NoSuchMethodException.class);
        } catch (ReflectiveOperationException ex) {
            throw newInternalError(ex);
        }
        final int DMH_THIS    = 0;
        final int ARG_BASE    = 1;
        final int ARG_LIMIT   = ARG_BASE + mtype.parameterCount();
        int nameCursor = ARG_LIMIT;
        final int NEW_OBJ     = (doesAlloc ? nameCursor++ : -1);
        final int GET_MEMBER  = nameCursor++;
        final int CHECK_RECEIVER = (needsReceiverCheck ? nameCursor++ : -1);
        final int LINKER_CALL = nameCursor++;
        Name[] names = arguments(nameCursor - ARG_LIMIT, mtype.invokerType());
        assert(names.length == nameCursor);
        if (doesAlloc) {
            // names = { argx,y,z,... new C, init method }
            names[NEW_OBJ] = new Name(getFunction(NF_allocateInstance), names[DMH_THIS]);
            names[GET_MEMBER] = new Name(getFunction(NF_constructorMethod), names[DMH_THIS]);
        } else if (needsInit) {
            names[GET_MEMBER] = new Name(getFunction(NF_internalMemberNameEnsureInit), names[DMH_THIS]);
        } else {
            names[GET_MEMBER] = new Name(getFunction(NF_internalMemberName), names[DMH_THIS]);
        }
        assert(findDirectMethodHandle(names[GET_MEMBER]) == names[DMH_THIS]);
        Object[] outArgs = Arrays.copyOfRange(names, ARG_BASE, GET_MEMBER+1, Object[].class);
        if (needsReceiverCheck) {
            names[CHECK_RECEIVER] = new Name(getFunction(NF_checkReceiver), names[DMH_THIS], names[ARG_BASE]);
            outArgs[0] = names[CHECK_RECEIVER];
        }
        assert(outArgs[outArgs.length-1] == names[GET_MEMBER]);  // look, shifted args!
        int result = LAST_RESULT;
        if (doesAlloc) {
            assert(outArgs[outArgs.length-2] == names[NEW_OBJ]);  // got to move this one
            System.arraycopy(outArgs, 0, outArgs, 1, outArgs.length-2);
            outArgs[0] = names[NEW_OBJ];
            result = NEW_OBJ;
        }
        names[LINKER_CALL] = new Name(linker, outArgs);
        LambdaForm lform = new LambdaForm(ARG_LIMIT, names, result, kind);

        // This is a tricky bit of code.  Don't send it through the LF interpreter.
        lform.compileToBytecode();
        return lform;
    }

    /* assert */ static Object findDirectMethodHandle(Name name) {
        if (name.function.equals(getFunction(NF_internalMemberName)) ||
            name.function.equals(getFunction(NF_internalMemberNameEnsureInit)) ||
            name.function.equals(getFunction(NF_constructorMethod))) {
            assert(name.arguments.length == 1);
            return name.arguments[0];
        }
        return null;
    }

    private static void maybeCompile(LambdaForm lform, MemberName m) {
        if (lform.vmentry == null && VerifyAccess.isSamePackage(m.getDeclaringClass(), MethodHandle.class))
            // Help along bootstrapping...
            lform.compileToBytecode();
    }

    /** Static wrapper for DirectMethodHandle.internalMemberName. */
    @ForceInline
    /*non-public*/ static Object internalMemberName(Object mh) {
        return ((DirectMethodHandle)mh).member;
    }

    /** Static wrapper for DirectMethodHandle.internalMemberName.
     * This one also forces initialization.
     */
    /*non-public*/ static Object internalMemberNameEnsureInit(Object mh) {
        DirectMethodHandle dmh = (DirectMethodHandle)mh;
        dmh.ensureInitialized();
        return dmh.member;
    }

    /*non-public*/ static
    boolean shouldBeInitialized(MemberName member) {
        switch (member.getReferenceKind()) {
        case REF_invokeStatic:
        case REF_getStatic:
        case REF_putStatic:
        case REF_newInvokeSpecial:
            break;
        default:
            // No need to initialize the class on this kind of member.
            return false;
        }
        Class<?> cls = member.getDeclaringClass();
        if (cls == ValueConversions.class ||
            cls == MethodHandleImpl.class ||
            cls == Invokers.class) {
            // These guys have lots of <clinit> DMH creation but we know
            // the MHs will not be used until the system is booted.
            return false;
        }
        if (VerifyAccess.isSamePackage(MethodHandle.class, cls) ||
            VerifyAccess.isSamePackage(ValueConversions.class, cls)) {
            // It is a system class.  It is probably in the process of
            // being initialized, but we will help it along just to be safe.
            if (UNSAFE.shouldBeInitialized(cls)) {
                UNSAFE.ensureClassInitialized(cls);
            }
            return false;
        }
        return UNSAFE.shouldBeInitialized(cls);
    }

    private static class EnsureInitialized extends ClassValue<WeakReference<Thread>> {
        @Override
        protected WeakReference<Thread> computeValue(Class<?> type) {
            UNSAFE.ensureClassInitialized(type);
            if (UNSAFE.shouldBeInitialized(type))
                // If the previous call didn't block, this can happen.
                // We are executing inside <clinit>.
                return new WeakReference<>(Thread.currentThread());
            return null;
        }
        static final EnsureInitialized INSTANCE = new EnsureInitialized();
    }

    private void ensureInitialized() {
        if (checkInitialized(member)) {
            // The coast is clear.  Delete the <clinit> barrier.
            if (member.isField())
                updateForm(preparedFieldLambdaForm(member));
            else
                updateForm(preparedLambdaForm(member));
        }
    }
    private static boolean checkInitialized(MemberName member) {
        Class<?> defc = member.getDeclaringClass();
        WeakReference<Thread> ref = EnsureInitialized.INSTANCE.get(defc);
        if (ref == null) {
            return true;  // the final state
        }
        Thread clinitThread = ref.get();
        // Somebody may still be running defc.<clinit>.
        if (clinitThread == Thread.currentThread()) {
            // If anybody is running defc.<clinit>, it is this thread.
            if (UNSAFE.shouldBeInitialized(defc))
                // Yes, we are running it; keep the barrier for now.
                return false;
        } else {
            // We are in a random thread.  Block.
            UNSAFE.ensureClassInitialized(defc);
        }
        assert(!UNSAFE.shouldBeInitialized(defc));
        // put it into the final state
        EnsureInitialized.INSTANCE.remove(defc);
        return true;
    }

    /*non-public*/ static void ensureInitialized(Object mh) {
        ((DirectMethodHandle)mh).ensureInitialized();
    }

    /** This subclass represents invokespecial instructions. */
    static class Special extends DirectMethodHandle {
        private final Class<?> caller;
        private Special(MethodType mtype, LambdaForm form, MemberName member, Class<?> caller) {
            super(mtype, form, member);
            this.caller = caller;
        }
        @Override
        boolean isInvokeSpecial() {
            return true;
        }
        @Override
        MethodHandle copyWith(MethodType mt, LambdaForm lf) {
            return new Special(mt, lf, member, caller);
        }
        Object checkReceiver(Object recv) {
            if (!caller.isInstance(recv)) {
                String msg = String.format("Receiver class %s is not a subclass of caller class %s",
                                           recv.getClass().getName(), caller.getName());
                throw new IncompatibleClassChangeError(msg);
            }
            return recv;
        }
    }

    /** This subclass represents invokeinterface instructions. */
    static class Interface extends DirectMethodHandle {
        private final Class<?> refc;
        private Interface(MethodType mtype, LambdaForm form, MemberName member, Class<?> refc) {
            super(mtype, form, member);
            assert refc.isInterface() : refc;
            this.refc = refc;
        }
        @Override
        MethodHandle copyWith(MethodType mt, LambdaForm lf) {
            return new Interface(mt, lf, member, refc);
        }
        @Override
        Object checkReceiver(Object recv) {
            if (!refc.isInstance(recv)) {
                String msg = String.format("Receiver class %s does not implement the requested interface %s",
                                           recv.getClass().getName(), refc.getName());
                throw new IncompatibleClassChangeError(msg);
            }
            return recv;
        }
    }

    /** Used for interface receiver type checks, by Interface and Special modes. */
    Object checkReceiver(Object recv) {
        throw new InternalError("Should only be invoked on a subclass");
    }


    /** This subclass handles constructor references. */
    static class Constructor extends DirectMethodHandle {
        final MemberName initMethod;
        final Class<?>   instanceClass;

        private Constructor(MethodType mtype, LambdaForm form, MemberName constructor,
                            MemberName initMethod, Class<?> instanceClass) {
            super(mtype, form, constructor);
            this.initMethod = initMethod;
            this.instanceClass = instanceClass;
            assert(initMethod.isResolved());
        }
        @Override
        MethodHandle copyWith(MethodType mt, LambdaForm lf) {
            return new Constructor(mt, lf, member, initMethod, instanceClass);
        }
    }

    /*non-public*/ static Object constructorMethod(Object mh) {
        Constructor dmh = (Constructor)mh;
        return dmh.initMethod;
    }

    /*non-public*/ static Object allocateInstance(Object mh) throws InstantiationException {
        Constructor dmh = (Constructor)mh;
        return UNSAFE.allocateInstance(dmh.instanceClass);
    }

    /** This subclass handles non-static field references. */
    static class Accessor extends DirectMethodHandle {
        final Class<?> fieldType;
        final int      fieldOffset;
        private Accessor(MethodType mtype, LambdaForm form, MemberName member,
                         int fieldOffset) {
            super(mtype, form, member);
            this.fieldType   = member.getFieldType();
            this.fieldOffset = fieldOffset;
        }

        @Override Object checkCast(Object obj) {
            return fieldType.cast(obj);
        }
        @Override
        MethodHandle copyWith(MethodType mt, LambdaForm lf) {
            return new Accessor(mt, lf, member, fieldOffset);
        }
    }

    @ForceInline
    /*non-public*/ static long fieldOffset(Object accessorObj) {
        // Note: We return a long because that is what Unsafe.getObject likes.
        // We store a plain int because it is more compact.
        return ((Accessor)accessorObj).fieldOffset;
    }

    @ForceInline
    /*non-public*/ static Object checkBase(Object obj) {
        // Note that the object's class has already been verified,
        // since the parameter type of the Accessor method handle
        // is either member.getDeclaringClass or a subclass.
        // This was verified in DirectMethodHandle.make.
        // Therefore, the only remaining check is for null.
        // Since this check is *not* guaranteed by Unsafe.getInt
        // and its siblings, we need to make an explicit one here.
        return Objects.requireNonNull(obj);
    }

    /** This subclass handles static field references. */
    static class StaticAccessor extends DirectMethodHandle {
        private final Class<?> fieldType;
        private final Object   staticBase;
        private final long     staticOffset;

        private StaticAccessor(MethodType mtype, LambdaForm form, MemberName member,
                               Object staticBase, long staticOffset) {
            super(mtype, form, member);
            this.fieldType    = member.getFieldType();
            this.staticBase   = staticBase;
            this.staticOffset = staticOffset;
        }

        @Override Object checkCast(Object obj) {
            return fieldType.cast(obj);
        }
        @Override
        MethodHandle copyWith(MethodType mt, LambdaForm lf) {
            return new StaticAccessor(mt, lf, member, staticBase, staticOffset);
        }
    }

    @ForceInline
    /*non-public*/ static Object nullCheck(Object obj) {
        return Objects.requireNonNull(obj);
    }

    @ForceInline
    /*non-public*/ static Object staticBase(Object accessorObj) {
        return ((StaticAccessor)accessorObj).staticBase;
    }

    @ForceInline
    /*non-public*/ static long staticOffset(Object accessorObj) {
        return ((StaticAccessor)accessorObj).staticOffset;
    }

    @ForceInline
    /*non-public*/ static Object checkCast(Object mh, Object obj) {
        return ((DirectMethodHandle) mh).checkCast(obj);
    }

    Object checkCast(Object obj) {
        return member.getReturnType().cast(obj);
    }

    // Caching machinery for field accessors:
    static final byte
            AF_GETFIELD        = 0,
            AF_PUTFIELD        = 1,
            AF_GETSTATIC       = 2,
            AF_PUTSTATIC       = 3,
            AF_GETSTATIC_INIT  = 4,
            AF_PUTSTATIC_INIT  = 5,
            AF_LIMIT           = 6;
    // Enumerate the different field kinds using Wrapper,
    // with an extra case added for checked references.
    static final int
            FT_LAST_WRAPPER    = Wrapper.COUNT-1,
            FT_UNCHECKED_REF   = Wrapper.OBJECT.ordinal(),
            FT_CHECKED_REF     = FT_LAST_WRAPPER+1,
            FT_LIMIT           = FT_LAST_WRAPPER+2;
    private static int afIndex(byte formOp, boolean isVolatile, int ftypeKind) {
        return ((formOp * FT_LIMIT * 2)
                + (isVolatile ? FT_LIMIT : 0)
                + ftypeKind);
    }
    @Stable
    private static final LambdaForm[] ACCESSOR_FORMS
            = new LambdaForm[afIndex(AF_LIMIT, false, 0)];
    static int ftypeKind(Class<?> ftype) {
        if (ftype.isPrimitive())
            return Wrapper.forPrimitiveType(ftype).ordinal();
        else if (VerifyType.isNullReferenceConversion(Object.class, ftype))
            return FT_UNCHECKED_REF;
        else
            return FT_CHECKED_REF;
    }

    /**
     * Create a LF which can access the given field.
     * Cache and share this structure among all fields with
     * the same basicType and refKind.
     */
    private static LambdaForm preparedFieldLambdaForm(MemberName m) {
        Class<?> ftype = m.getFieldType();
        boolean isVolatile = m.isVolatile();
        byte formOp;
        switch (m.getReferenceKind()) {
        case REF_getField:      formOp = AF_GETFIELD;    break;
        case REF_putField:      formOp = AF_PUTFIELD;    break;
        case REF_getStatic:     formOp = AF_GETSTATIC;   break;
        case REF_putStatic:     formOp = AF_PUTSTATIC;   break;
        default:  throw new InternalError(m.toString());
        }
        if (shouldBeInitialized(m)) {
            // precompute the barrier-free version:
            preparedFieldLambdaForm(formOp, isVolatile, ftype);
            assert((AF_GETSTATIC_INIT - AF_GETSTATIC) ==
                   (AF_PUTSTATIC_INIT - AF_PUTSTATIC));
            formOp += (AF_GETSTATIC_INIT - AF_GETSTATIC);
        }
        LambdaForm lform = preparedFieldLambdaForm(formOp, isVolatile, ftype);
        maybeCompile(lform, m);
        assert(lform.methodType().dropParameterTypes(0, 1)
                .equals(m.getInvocationType().basicType()))
                : Arrays.asList(m, m.getInvocationType().basicType(), lform, lform.methodType());
        return lform;
    }
    private static LambdaForm preparedFieldLambdaForm(byte formOp, boolean isVolatile, Class<?> ftype) {
        int ftypeKind = ftypeKind(ftype);
        int afIndex = afIndex(formOp, isVolatile, ftypeKind);
        LambdaForm lform = ACCESSOR_FORMS[afIndex];
        if (lform != null)  return lform;
        lform = makePreparedFieldLambdaForm(formOp, isVolatile, ftypeKind);
        ACCESSOR_FORMS[afIndex] = lform;  // don't bother with a CAS
        return lform;
    }

    private static final Wrapper[] ALL_WRAPPERS = Wrapper.values();

    private static Kind getFieldKind(boolean isGetter, boolean isVolatile, Wrapper wrapper) {
        if (isGetter) {
            if (isVolatile) {
                switch (wrapper) {
                    case BOOLEAN: return GET_BOOLEAN_VOLATILE;
                    case BYTE:    return GET_BYTE_VOLATILE;
                    case SHORT:   return GET_SHORT_VOLATILE;
                    case CHAR:    return GET_CHAR_VOLATILE;
                    case INT:     return GET_INT_VOLATILE;
                    case LONG:    return GET_LONG_VOLATILE;
                    case FLOAT:   return GET_FLOAT_VOLATILE;
                    case DOUBLE:  return GET_DOUBLE_VOLATILE;
                    case OBJECT:  return GET_OBJECT_VOLATILE;
                }
            } else {
                switch (wrapper) {
                    case BOOLEAN: return GET_BOOLEAN;
                    case BYTE:    return GET_BYTE;
                    case SHORT:   return GET_SHORT;
                    case CHAR:    return GET_CHAR;
                    case INT:     return GET_INT;
                    case LONG:    return GET_LONG;
                    case FLOAT:   return GET_FLOAT;
                    case DOUBLE:  return GET_DOUBLE;
                    case OBJECT:  return GET_OBJECT;
                }
            }
        } else {
            if (isVolatile) {
                switch (wrapper) {
                    case BOOLEAN: return PUT_BOOLEAN_VOLATILE;
                    case BYTE:    return PUT_BYTE_VOLATILE;
                    case SHORT:   return PUT_SHORT_VOLATILE;
                    case CHAR:    return PUT_CHAR_VOLATILE;
                    case INT:     return PUT_INT_VOLATILE;
                    case LONG:    return PUT_LONG_VOLATILE;
                    case FLOAT:   return PUT_FLOAT_VOLATILE;
                    case DOUBLE:  return PUT_DOUBLE_VOLATILE;
                    case OBJECT:  return PUT_OBJECT_VOLATILE;
                }
            } else {
                switch (wrapper) {
                    case BOOLEAN: return PUT_BOOLEAN;
                    case BYTE:    return PUT_BYTE;
                    case SHORT:   return PUT_SHORT;
                    case CHAR:    return PUT_CHAR;
                    case INT:     return PUT_INT;
                    case LONG:    return PUT_LONG;
                    case FLOAT:   return PUT_FLOAT;
                    case DOUBLE:  return PUT_DOUBLE;
                    case OBJECT:  return PUT_OBJECT;
                }
            }
        }
        throw new AssertionError("Invalid arguments");
    }

    static LambdaForm makePreparedFieldLambdaForm(byte formOp, boolean isVolatile, int ftypeKind) {
        boolean isGetter  = (formOp & 1) == (AF_GETFIELD & 1);
        boolean isStatic  = (formOp >= AF_GETSTATIC);
        boolean needsInit = (formOp >= AF_GETSTATIC_INIT);
        boolean needsCast = (ftypeKind == FT_CHECKED_REF);
        Wrapper fw = (needsCast ? Wrapper.OBJECT : ALL_WRAPPERS[ftypeKind]);
        Class<?> ft = fw.primitiveType();
        assert(ftypeKind(needsCast ? String.class : ft) == ftypeKind);

        // getObject, putIntVolatile, etc.
        Kind kind = getFieldKind(isGetter, isVolatile, fw);

        MethodType linkerType;
        if (isGetter)
            linkerType = MethodType.methodType(ft, Object.class, long.class);
        else
            linkerType = MethodType.methodType(void.class, Object.class, long.class, ft);
        MemberName linker = new MemberName(Unsafe.class, kind.methodName, linkerType, REF_invokeVirtual);
        try {
            linker = IMPL_NAMES.resolveOrFail(REF_invokeVirtual, linker, null, NoSuchMethodException.class);
        } catch (ReflectiveOperationException ex) {
            throw newInternalError(ex);
        }

        // What is the external type of the lambda form?
        MethodType mtype;
        if (isGetter)
            mtype = MethodType.methodType(ft);
        else
            mtype = MethodType.methodType(void.class, ft);
        mtype = mtype.basicType();  // erase short to int, etc.
        if (!isStatic)
            mtype = mtype.insertParameterTypes(0, Object.class);
        final int DMH_THIS  = 0;
        final int ARG_BASE  = 1;
        final int ARG_LIMIT = ARG_BASE + mtype.parameterCount();
        // if this is for non-static access, the base pointer is stored at this index:
        final int OBJ_BASE  = isStatic ? -1 : ARG_BASE;
        // if this is for write access, the value to be written is stored at this index:
        final int SET_VALUE  = isGetter ? -1 : ARG_LIMIT - 1;
        int nameCursor = ARG_LIMIT;
        final int F_HOLDER  = (isStatic ? nameCursor++ : -1);  // static base if any
        final int F_OFFSET  = nameCursor++;  // Either static offset or field offset.
        final int OBJ_CHECK = (OBJ_BASE >= 0 ? nameCursor++ : -1);
        final int U_HOLDER  = nameCursor++;  // UNSAFE holder
        final int INIT_BAR  = (needsInit ? nameCursor++ : -1);
        final int PRE_CAST  = (needsCast && !isGetter ? nameCursor++ : -1);
        final int LINKER_CALL = nameCursor++;
        final int POST_CAST = (needsCast && isGetter ? nameCursor++ : -1);
        final int RESULT    = nameCursor-1;  // either the call or the cast
        Name[] names = arguments(nameCursor - ARG_LIMIT, mtype.invokerType());
        if (needsInit)
            names[INIT_BAR] = new Name(getFunction(NF_ensureInitialized), names[DMH_THIS]);
        if (needsCast && !isGetter)
            names[PRE_CAST] = new Name(getFunction(NF_checkCast), names[DMH_THIS], names[SET_VALUE]);
        Object[] outArgs = new Object[1 + linkerType.parameterCount()];
        assert(outArgs.length == (isGetter ? 3 : 4));
        outArgs[0] = names[U_HOLDER] = new Name(getFunction(NF_UNSAFE));
        if (isStatic) {
            outArgs[1] = names[F_HOLDER]  = new Name(getFunction(NF_staticBase), names[DMH_THIS]);
            outArgs[2] = names[F_OFFSET]  = new Name(getFunction(NF_staticOffset), names[DMH_THIS]);
        } else {
            outArgs[1] = names[OBJ_CHECK] = new Name(getFunction(NF_checkBase), names[OBJ_BASE]);
            outArgs[2] = names[F_OFFSET]  = new Name(getFunction(NF_fieldOffset), names[DMH_THIS]);
        }
        if (!isGetter) {
            outArgs[3] = (needsCast ? names[PRE_CAST] : names[SET_VALUE]);
        }
        for (Object a : outArgs)  assert(a != null);
        names[LINKER_CALL] = new Name(linker, outArgs);
        if (needsCast && isGetter)
            names[POST_CAST] = new Name(getFunction(NF_checkCast), names[DMH_THIS], names[LINKER_CALL]);
        for (Name n : names)  assert(n != null);

        LambdaForm form;
        if (needsCast || needsInit) {
            // can't use the pre-generated form when casting and/or initializing
            form = new LambdaForm(ARG_LIMIT, names, RESULT);
        } else {
            form = new LambdaForm(ARG_LIMIT, names, RESULT, kind);
        }

        if (LambdaForm.debugNames()) {
            // add some detail to the lambdaForm debugname,
            // significant only for debugging
            StringBuilder nameBuilder = new StringBuilder(kind.methodName);
            if (isStatic) {
                nameBuilder.append("Static");
            } else {
                nameBuilder.append("Field");
            }
            if (needsCast) {
                nameBuilder.append("Cast");
            }
            if (needsInit) {
                nameBuilder.append("Init");
            }
            LambdaForm.associateWithDebugName(form, nameBuilder.toString());
        }
        return form;
    }

    /**
     * Pre-initialized NamedFunctions for bootstrapping purposes.
     */
    static final byte NF_internalMemberName = 0,
            NF_internalMemberNameEnsureInit = 1,
            NF_ensureInitialized = 2,
            NF_fieldOffset = 3,
            NF_checkBase = 4,
            NF_staticBase = 5,
            NF_staticOffset = 6,
            NF_checkCast = 7,
            NF_allocateInstance = 8,
            NF_constructorMethod = 9,
            NF_UNSAFE = 10,
            NF_checkReceiver = 11,
            NF_LIMIT = 12;

    private static final @Stable NamedFunction[] NFS = new NamedFunction[NF_LIMIT];

    private static NamedFunction getFunction(byte func) {
        NamedFunction nf = NFS[func];
        if (nf != null) {
            return nf;
        }
        // Each nf must be statically invocable or we get tied up in our bootstraps.
        nf = NFS[func] = createFunction(func);
        assert(InvokerBytecodeGenerator.isStaticallyInvocable(nf));
        return nf;
    }

    private static final MethodType OBJ_OBJ_TYPE = MethodType.methodType(Object.class, Object.class);

    private static final MethodType LONG_OBJ_TYPE = MethodType.methodType(long.class, Object.class);

    private static NamedFunction createFunction(byte func) {
        try {
            switch (func) {
                case NF_internalMemberName:
                    return getNamedFunction("internalMemberName", OBJ_OBJ_TYPE);
                case NF_internalMemberNameEnsureInit:
                    return getNamedFunction("internalMemberNameEnsureInit", OBJ_OBJ_TYPE);
                case NF_ensureInitialized:
                    return getNamedFunction("ensureInitialized", MethodType.methodType(void.class, Object.class));
                case NF_fieldOffset:
                    return getNamedFunction("fieldOffset", LONG_OBJ_TYPE);
                case NF_checkBase:
                    return getNamedFunction("checkBase", OBJ_OBJ_TYPE);
                case NF_staticBase:
                    return getNamedFunction("staticBase", OBJ_OBJ_TYPE);
                case NF_staticOffset:
                    return getNamedFunction("staticOffset", LONG_OBJ_TYPE);
                case NF_checkCast:
                    return getNamedFunction("checkCast", MethodType.methodType(Object.class, Object.class, Object.class));
                case NF_allocateInstance:
                    return getNamedFunction("allocateInstance", OBJ_OBJ_TYPE);
                case NF_constructorMethod:
                    return getNamedFunction("constructorMethod", OBJ_OBJ_TYPE);
                case NF_UNSAFE:
                    MemberName member = new MemberName(MethodHandleStatics.class, "UNSAFE", Unsafe.class, REF_getField);
                    return new NamedFunction(
                            MemberName.getFactory()
                                    .resolveOrFail(REF_getField, member, DirectMethodHandle.class, NoSuchMethodException.class));
                case NF_checkReceiver:
                    member = new MemberName(DirectMethodHandle.class, "checkReceiver", OBJ_OBJ_TYPE, REF_invokeVirtual);
                    return new NamedFunction(
                        MemberName.getFactory()
                            .resolveOrFail(REF_invokeVirtual, member, DirectMethodHandle.class, NoSuchMethodException.class));
                default:
                    throw newInternalError("Unknown function: " + func);
            }
        } catch (ReflectiveOperationException ex) {
            throw newInternalError(ex);
        }
    }

    private static NamedFunction getNamedFunction(String name, MethodType type)
        throws ReflectiveOperationException
    {
        MemberName member = new MemberName(DirectMethodHandle.class, name, type, REF_invokeStatic);
        return new NamedFunction(
            MemberName.getFactory()
                .resolveOrFail(REF_invokeStatic, member, DirectMethodHandle.class, NoSuchMethodException.class));
    }

    static {
        // The Holder class will contain pre-generated DirectMethodHandles resolved
        // speculatively using MemberName.getFactory().resolveOrNull. However, that
        // doesn't initialize the class, which subtly breaks inlining etc. By forcing
        // initialization of the Holder class we avoid these issues.
        UNSAFE.ensureClassInitialized(Holder.class);
    }

    /* Placeholder class for DirectMethodHandles generated ahead of time */
    final class Holder {}
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\GenerateJLIClassesHelper.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import jdk.internal.org.objectweb.asm.ClassWriter;
import jdk.internal.org.objectweb.asm.Opcodes;
import sun.invoke.util.Wrapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Helper class to assist the GenerateJLIClassesPlugin to get access to
 * generate classes ahead of time.
 */
class GenerateJLIClassesHelper {

    static byte[] generateBasicFormsClassBytes(String className) {
        ArrayList<LambdaForm> forms = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        HashSet<String> dedupSet = new HashSet<>();
        for (LambdaForm.BasicType type : LambdaForm.BasicType.values()) {
            LambdaForm zero = LambdaForm.zeroForm(type);
            String name = zero.kind.defaultLambdaName
                   + "_" + zero.returnType().basicTypeChar();
            if (dedupSet.add(name)) {
                names.add(name);
                forms.add(zero);
            }

            LambdaForm identity = LambdaForm.identityForm(type);
            name = identity.kind.defaultLambdaName
                   + "_" + identity.returnType().basicTypeChar();
            if (dedupSet.add(name)) {
                names.add(name);
                forms.add(identity);
            }
        }
        return generateCodeBytesForLFs(className,
                names.toArray(new String[0]),
                forms.toArray(new LambdaForm[0]));
    }

    static byte[] generateDirectMethodHandleHolderClassBytes(String className,
            MethodType[] methodTypes, int[] types) {
        ArrayList<LambdaForm> forms = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        for (int i = 0; i < methodTypes.length; i++) {
            LambdaForm form = DirectMethodHandle
                    .makePreparedLambdaForm(methodTypes[i], types[i]);
            forms.add(form);
            names.add(form.kind.defaultLambdaName);
        }
        for (Wrapper wrapper : Wrapper.values()) {
            if (wrapper == Wrapper.VOID) {
                continue;
            }
            for (byte b = DirectMethodHandle.AF_GETFIELD; b < DirectMethodHandle.AF_LIMIT; b++) {
                int ftype = DirectMethodHandle.ftypeKind(wrapper.primitiveType());
                LambdaForm form = DirectMethodHandle
                        .makePreparedFieldLambdaForm(b, /*isVolatile*/false, ftype);
                if (form.kind != LambdaForm.Kind.GENERIC) {
                    forms.add(form);
                    names.add(form.kind.defaultLambdaName);
                }
                // volatile
                form = DirectMethodHandle
                        .makePreparedFieldLambdaForm(b, /*isVolatile*/true, ftype);
                if (form.kind != LambdaForm.Kind.GENERIC) {
                    forms.add(form);
                    names.add(form.kind.defaultLambdaName);
                }
            }
        }
        return generateCodeBytesForLFs(className,
                names.toArray(new String[0]),
                forms.toArray(new LambdaForm[0]));
    }

    static byte[] generateDelegatingMethodHandleHolderClassBytes(String className,
            MethodType[] methodTypes) {

        HashSet<MethodType> dedupSet = new HashSet<>();
        ArrayList<LambdaForm> forms = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        for (int i = 0; i < methodTypes.length; i++) {
            // generate methods representing the DelegatingMethodHandle
            if (dedupSet.add(methodTypes[i])) {
                // reinvokers are variant with the associated SpeciesData
                // and shape of the target LF, but we can easily pregenerate
                // the basic reinvokers associated with Species_L. Ultimately we
                // may want to consider pregenerating more of these, which will
                // require an even more complex naming scheme
                LambdaForm reinvoker = makeReinvokerFor(methodTypes[i]);
                forms.add(reinvoker);
                String speciesSig = BoundMethodHandle.speciesDataFor(reinvoker).key();
                assert(speciesSig.equals("L"));
                names.add(reinvoker.kind.defaultLambdaName + "_" + speciesSig);

                LambdaForm delegate = makeDelegateFor(methodTypes[i]);
                forms.add(delegate);
                names.add(delegate.kind.defaultLambdaName);
            }
        }
        return generateCodeBytesForLFs(className,
                names.toArray(new String[0]),
                forms.toArray(new LambdaForm[0]));
    }

    static byte[] generateInvokersHolderClassBytes(String className,
            MethodType[] invokerMethodTypes, MethodType[] callSiteMethodTypes) {

        HashSet<MethodType> dedupSet = new HashSet<>();
        ArrayList<LambdaForm> forms = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        int[] types = {
            MethodTypeForm.LF_EX_LINKER,
            MethodTypeForm.LF_EX_INVOKER,
            MethodTypeForm.LF_GEN_LINKER,
            MethodTypeForm.LF_GEN_INVOKER
        };

        for (int i = 0; i < invokerMethodTypes.length; i++) {
            // generate methods representing invokers of the specified type
            if (dedupSet.add(invokerMethodTypes[i])) {
                for (int type : types) {
                    LambdaForm invokerForm = Invokers.invokeHandleForm(invokerMethodTypes[i],
                            /*customized*/false, type);
                    forms.add(invokerForm);
                    names.add(invokerForm.kind.defaultLambdaName);
                }
            }
        }

        dedupSet = new HashSet<>();
        for (int i = 0; i < callSiteMethodTypes.length; i++) {
            // generate methods representing invokers of the specified type
            if (dedupSet.add(callSiteMethodTypes[i])) {
                LambdaForm callSiteForm = Invokers.callSiteForm(callSiteMethodTypes[i], true);
                forms.add(callSiteForm);
                names.add(callSiteForm.kind.defaultLambdaName);

                LambdaForm methodHandleForm = Invokers.callSiteForm(callSiteMethodTypes[i], false);
                forms.add(methodHandleForm);
                names.add(methodHandleForm.kind.defaultLambdaName);
            }
        }

        return generateCodeBytesForLFs(className,
                names.toArray(new String[0]),
                forms.toArray(new LambdaForm[0]));
    }

    /*
     * Generate customized code for a set of LambdaForms of specified types into
     * a class with a specified name.
     */
    private static byte[] generateCodeBytesForLFs(String className,
            String[] names, LambdaForm[] forms) {

        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES);
        cw.visit(Opcodes.V1_8, Opcodes.ACC_PRIVATE + Opcodes.ACC_FINAL + Opcodes.ACC_SUPER,
                className, null, InvokerBytecodeGenerator.INVOKER_SUPER_NAME, null);
        cw.visitSource(className.substring(className.lastIndexOf('/') + 1), null);

        for (int i = 0; i < forms.length; i++) {
            addMethod(className, names[i], forms[i],
                    forms[i].methodType(), cw);
        }
        return cw.toByteArray();
    }

    private static void addMethod(String className, String methodName, LambdaForm form,
            MethodType type, ClassWriter cw) {
        InvokerBytecodeGenerator g
                = new InvokerBytecodeGenerator(className, methodName, form, type);
        g.setClassWriter(cw);
        g.addMethod();
    }

    private static LambdaForm makeReinvokerFor(MethodType type) {
        MethodHandle emptyHandle = MethodHandles.empty(type);
        return DelegatingMethodHandle.makeReinvokerForm(emptyHandle,
                MethodTypeForm.LF_REBIND,
                BoundMethodHandle.speciesData_L(),
                BoundMethodHandle.speciesData_L().getterFunction(0));
    }

    private static LambdaForm makeDelegateFor(MethodType type) {
        MethodHandle handle = MethodHandles.empty(type);
        return DelegatingMethodHandle.makeReinvokerForm(
                handle,
                MethodTypeForm.LF_DELEGATE,
                DelegatingMethodHandle.class,
                DelegatingMethodHandle.NF_getTarget);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    static Map.Entry<String, byte[]> generateConcreteBMHClassBytes(final String types) {
        for (char c : types.toCharArray()) {
            if ("LIJFD".indexOf(c) < 0) {
                throw new IllegalArgumentException("All characters must "
                        + "correspond to a basic field type: LIJFD");
            }
        }
        final BoundMethodHandle.SpeciesData species = BoundMethodHandle.SPECIALIZER.findSpecies(types);
        final String className = species.speciesCode().getName();
        final ClassSpecializer.Factory factory = BoundMethodHandle.SPECIALIZER.factory();
        final byte[] code = factory.generateConcreteSpeciesCodeFile(className, species);
        return Map.entry(className.replace('.', '/'), code);
    }

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\InfoFromMemberName.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2012, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import java.security.*;
import java.lang.reflect.*;
import java.lang.invoke.MethodHandleNatives.Constants;
import java.lang.invoke.MethodHandles.Lookup;
import static java.lang.invoke.MethodHandleStatics.*;

/*
 * Auxiliary to MethodHandleInfo, wants to nest in MethodHandleInfo but must be non-public.
 */
/*non-public*/
final
class InfoFromMemberName implements MethodHandleInfo {
    private final MemberName member;
    private final int referenceKind;

    InfoFromMemberName(Lookup lookup, MemberName member, byte referenceKind) {
        assert(member.isResolved() || member.isMethodHandleInvoke() || member.isVarHandleMethodInvoke());
        assert(member.referenceKindIsConsistentWith(referenceKind));
        this.member = member;
        this.referenceKind = referenceKind;
    }

    @Override
    public Class<?> getDeclaringClass() {
        return member.getDeclaringClass();
    }

    @Override
    public String getName() {
        return member.getName();
    }

    @Override
    public MethodType getMethodType() {
        return member.getMethodOrFieldType();
    }

    @Override
    public int getModifiers() {
        return member.getModifiers();
    }

    @Override
    public int getReferenceKind() {
        return referenceKind;
    }

    @Override
    public String toString() {
        return MethodHandleInfo.toString(getReferenceKind(), getDeclaringClass(), getName(), getMethodType());
    }

    @Override
    public <T extends Member> T reflectAs(Class<T> expected, Lookup lookup) {
        if ((member.isMethodHandleInvoke() || member.isVarHandleMethodInvoke())
            && !member.isVarargs()) {
            // This member is an instance of a signature-polymorphic method, which cannot be reflected
            // A method handle invoker can come in either of two forms:
            // A generic placeholder (present in the source code, and varargs)
            // and a signature-polymorphic instance (synthetic and not varargs).
            // For more information see comments on {@link MethodHandleNatives#linkMethod}.
            throw new IllegalArgumentException("cannot reflect signature polymorphic method");
        }
        Member mem = AccessController.doPrivileged(new PrivilegedAction<>() {
                public Member run() {
                    try {
                        return reflectUnchecked();
                    } catch (ReflectiveOperationException ex) {
                        throw new IllegalArgumentException(ex);
                    }
                }
            });
        try {
            Class<?> defc = getDeclaringClass();
            byte refKind = (byte) getReferenceKind();
            lookup.checkAccess(refKind, defc, convertToMemberName(refKind, mem));
        } catch (IllegalAccessException ex) {
            throw new IllegalArgumentException(ex);
        }
        return expected.cast(mem);
    }

    private Member reflectUnchecked() throws ReflectiveOperationException {
        byte refKind = (byte) getReferenceKind();
        Class<?> defc = getDeclaringClass();
        boolean isPublic = Modifier.isPublic(getModifiers());
        if (MethodHandleNatives.refKindIsMethod(refKind)) {
            if (isPublic)
                return defc.getMethod(getName(), getMethodType().parameterArray());
            else
                return defc.getDeclaredMethod(getName(), getMethodType().parameterArray());
        } else if (MethodHandleNatives.refKindIsConstructor(refKind)) {
            if (isPublic)
                return defc.getConstructor(getMethodType().parameterArray());
            else
                return defc.getDeclaredConstructor(getMethodType().parameterArray());
        } else if (MethodHandleNatives.refKindIsField(refKind)) {
            if (isPublic)
                return defc.getField(getName());
            else
                return defc.getDeclaredField(getName());
        } else {
            throw new IllegalArgumentException("referenceKind="+refKind);
        }
    }

    private static MemberName convertToMemberName(byte refKind, Member mem) throws IllegalAccessException {
        if (mem instanceof Method) {
            boolean wantSpecial = (refKind == REF_invokeSpecial);
            return new MemberName((Method) mem, wantSpecial);
        } else if (mem instanceof Constructor) {
            return new MemberName((Constructor) mem);
        } else if (mem instanceof Field) {
            boolean isSetter = (refKind == REF_putField || refKind == REF_putStatic);
            return new MemberName((Field) mem, isSetter);
        }
        throw new InternalError(mem.getClass().getName());
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\InjectedProfile.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import java.lang.annotation.*;

/**
 * Internal marker for some methods in the JSR 292 implementation.
 */
/*non-public*/
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
@Retention(RetentionPolicy.RUNTIME)
@interface InjectedProfile {
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\InnerClassLambdaMetafactory.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2012, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import jdk.internal.org.objectweb.asm.*;
import sun.invoke.util.BytecodeDescriptor;
import jdk.internal.misc.Unsafe;
import sun.security.action.GetPropertyAction;

import java.io.FilePermission;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.LinkedHashSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.PropertyPermission;
import java.util.Set;

import static jdk.internal.org.objectweb.asm.Opcodes.*;

/**
 * Lambda metafactory implementation which dynamically creates an
 * inner-class-like class per lambda callsite.
 *
 * @see LambdaMetafactory
 */
/* package */ final class InnerClassLambdaMetafactory extends AbstractValidatingLambdaMetafactory {
    private static final Unsafe UNSAFE = Unsafe.getUnsafe();

    private static final int CLASSFILE_VERSION = 52;
    private static final String METHOD_DESCRIPTOR_VOID = Type.getMethodDescriptor(Type.VOID_TYPE);
    private static final String JAVA_LANG_OBJECT = "java/lang/Object";
    private static final String NAME_CTOR = "<init>";
    private static final String NAME_FACTORY = "get$Lambda";

    //Serialization support
    private static final String NAME_SERIALIZED_LAMBDA = "java/lang/invoke/SerializedLambda";
    private static final String NAME_NOT_SERIALIZABLE_EXCEPTION = "java/io/NotSerializableException";
    private static final String DESCR_METHOD_WRITE_REPLACE = "()Ljava/lang/Object;";
    private static final String DESCR_METHOD_WRITE_OBJECT = "(Ljava/io/ObjectOutputStream;)V";
    private static final String DESCR_METHOD_READ_OBJECT = "(Ljava/io/ObjectInputStream;)V";
    private static final String NAME_METHOD_WRITE_REPLACE = "writeReplace";
    private static final String NAME_METHOD_READ_OBJECT = "readObject";
    private static final String NAME_METHOD_WRITE_OBJECT = "writeObject";

    private static final String DESCR_CLASS = "Ljava/lang/Class;";
    private static final String DESCR_STRING = "Ljava/lang/String;";
    private static final String DESCR_OBJECT = "Ljava/lang/Object;";
    private static final String DESCR_CTOR_SERIALIZED_LAMBDA
            = "(" + DESCR_CLASS + DESCR_STRING + DESCR_STRING + DESCR_STRING + "I"
            + DESCR_STRING + DESCR_STRING + DESCR_STRING + DESCR_STRING + "[" + DESCR_OBJECT + ")V";

    private static final String DESCR_CTOR_NOT_SERIALIZABLE_EXCEPTION = "(Ljava/lang/String;)V";
    private static final String[] SER_HOSTILE_EXCEPTIONS = new String[] {NAME_NOT_SERIALIZABLE_EXCEPTION};


    private static final String[] EMPTY_STRING_ARRAY = new String[0];

    // Used to ensure that each spun class name is unique
    private static final AtomicInteger counter = new AtomicInteger(0);

    // For dumping generated classes to disk, for debugging purposes
    private static final ProxyClassesDumper dumper;

    static {
        final String key = "jdk.internal.lambda.dumpProxyClasses";
        String path = GetPropertyAction.privilegedGetProperty(key);
        dumper = (null == path) ? null : ProxyClassesDumper.getInstance(path);
    }

    // See context values in AbstractValidatingLambdaMetafactory
    private final String implMethodClassName;        // Name of type containing implementation "CC"
    private final String implMethodName;             // Name of implementation method "impl"
    private final String implMethodDesc;             // Type descriptor for implementation methods "(I)Ljava/lang/String;"
    private final MethodType constructorType;        // Generated class constructor type "(CC)void"
    private final ClassWriter cw;                    // ASM class writer
    private final String[] argNames;                 // Generated names for the constructor arguments
    private final String[] argDescs;                 // Type descriptors for the constructor arguments
    private final String lambdaClassName;            // Generated name for the generated class "X$$Lambda$1"

    /**
     * General meta-factory constructor, supporting both standard cases and
     * allowing for uncommon options such as serialization or bridging.
     *
     * @param caller Stacked automatically by VM; represents a lookup context
     *               with the accessibility privileges of the caller.
     * @param invokedType Stacked automatically by VM; the signature of the
     *                    invoked method, which includes the expected static
     *                    type of the returned lambda object, and the static
     *                    types of the captured arguments for the lambda.  In
     *                    the event that the implementation method is an
     *                    instance method, the first argument in the invocation
     *                    signature will correspond to the receiver.
     * @param samMethodName Name of the method in the functional interface to
     *                      which the lambda or method reference is being
     *                      converted, represented as a String.
     * @param samMethodType Type of the method in the functional interface to
     *                      which the lambda or method reference is being
     *                      converted, represented as a MethodType.
     * @param implMethod The implementation method which should be called (with
     *                   suitable adaptation of argument types, return types,
     *                   and adjustment for captured arguments) when methods of
     *                   the resulting functional interface instance are invoked.
     * @param instantiatedMethodType The signature of the primary functional
     *                               interface method after type variables are
     *                               substituted with their instantiation from
     *                               the capture site
     * @param isSerializable Should the lambda be made serializable?  If set,
     *                       either the target type or one of the additional SAM
     *                       types must extend {@code Serializable}.
     * @param markerInterfaces Additional interfaces which the lambda object
     *                       should implement.
     * @param additionalBridges Method types for additional signatures to be
     *                          bridged to the implementation method
     * @throws LambdaConversionException If any of the meta-factory protocol
     * invariants are violated
     */
    public InnerClassLambdaMetafactory(MethodHandles.Lookup caller,
                                       MethodType invokedType,
                                       String samMethodName,
                                       MethodType samMethodType,
                                       MethodHandle implMethod,
                                       MethodType instantiatedMethodType,
                                       boolean isSerializable,
                                       Class<?>[] markerInterfaces,
                                       MethodType[] additionalBridges)
            throws LambdaConversionException {
        super(caller, invokedType, samMethodName, samMethodType,
              implMethod, instantiatedMethodType,
              isSerializable, markerInterfaces, additionalBridges);
        implMethodClassName = implClass.getName().replace('.', '/');
        implMethodName = implInfo.getName();
        implMethodDesc = implInfo.getMethodType().toMethodDescriptorString();
        constructorType = invokedType.changeReturnType(Void.TYPE);
        lambdaClassName = targetClass.getName().replace('.', '/') + "$$Lambda$" + counter.incrementAndGet();
        cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        int parameterCount = invokedType.parameterCount();
        if (parameterCount > 0) {
            argNames = new String[parameterCount];
            argDescs = new String[parameterCount];
            for (int i = 0; i < parameterCount; i++) {
                argNames[i] = "arg$" + (i + 1);
                argDescs[i] = BytecodeDescriptor.unparse(invokedType.parameterType(i));
            }
        } else {
            argNames = argDescs = EMPTY_STRING_ARRAY;
        }
    }

    /**
     * Build the CallSite. Generate a class file which implements the functional
     * interface, define the class, if there are no parameters create an instance
     * of the class which the CallSite will return, otherwise, generate handles
     * which will call the class' constructor.
     *
     * @return a CallSite, which, when invoked, will return an instance of the
     * functional interface
     * @throws ReflectiveOperationException
     * @throws LambdaConversionException If properly formed functional interface
     * is not found
     */
    @Override
    CallSite buildCallSite() throws LambdaConversionException {
        final Class<?> innerClass = spinInnerClass();
        if (invokedType.parameterCount() == 0) {
            final Constructor<?>[] ctrs = AccessController.doPrivileged(
                    new PrivilegedAction<>() {
                @Override
                public Constructor<?>[] run() {
                    Constructor<?>[] ctrs = innerClass.getDeclaredConstructors();
                    if (ctrs.length == 1) {
                        // The lambda implementing inner class constructor is private, set
                        // it accessible (by us) before creating the constant sole instance
                        ctrs[0].setAccessible(true);
                    }
                    return ctrs;
                }
                    });
            if (ctrs.length != 1) {
                throw new LambdaConversionException("Expected one lambda constructor for "
                        + innerClass.getCanonicalName() + ", got " + ctrs.length);
            }

            try {
                Object inst = ctrs[0].newInstance();
                return new ConstantCallSite(MethodHandles.constant(samBase, inst));
            }
            catch (ReflectiveOperationException e) {
                throw new LambdaConversionException("Exception instantiating lambda object", e);
            }
        } else {
            try {
                UNSAFE.ensureClassInitialized(innerClass);
                return new ConstantCallSite(
                        MethodHandles.Lookup.IMPL_LOOKUP
                             .findStatic(innerClass, NAME_FACTORY, invokedType));
            }
            catch (ReflectiveOperationException e) {
                throw new LambdaConversionException("Exception finding constructor", e);
            }
        }
    }

    /**
     * Generate a class file which implements the functional
     * interface, define and return the class.
     *
     * @implNote The class that is generated does not include signature
     * information for exceptions that may be present on the SAM method.
     * This is to reduce classfile size, and is harmless as checked exceptions
     * are erased anyway, no one will ever compile against this classfile,
     * and we make no guarantees about the reflective properties of lambda
     * objects.
     *
     * @return a Class which implements the functional interface
     * @throws LambdaConversionException If properly formed functional interface
     * is not found
     */
    private Class<?> spinInnerClass() throws LambdaConversionException {
        String[] interfaces;
        String samIntf = samBase.getName().replace('.', '/');
        boolean accidentallySerializable = !isSerializable && Serializable.class.isAssignableFrom(samBase);
        if (markerInterfaces.length == 0) {
            interfaces = new String[]{samIntf};
        } else {
            // Assure no duplicate interfaces (ClassFormatError)
            Set<String> itfs = new LinkedHashSet<>(markerInterfaces.length + 1);
            itfs.add(samIntf);
            for (Class<?> markerInterface : markerInterfaces) {
                itfs.add(markerInterface.getName().replace('.', '/'));
                accidentallySerializable |= !isSerializable && Serializable.class.isAssignableFrom(markerInterface);
            }
            interfaces = itfs.toArray(new String[itfs.size()]);
        }

        cw.visit(CLASSFILE_VERSION, ACC_SUPER + ACC_FINAL + ACC_SYNTHETIC,
                 lambdaClassName, null,
                 JAVA_LANG_OBJECT, interfaces);

        // Generate final fields to be filled in by constructor
        for (int i = 0; i < argDescs.length; i++) {
            FieldVisitor fv = cw.visitField(ACC_PRIVATE + ACC_FINAL,
                                            argNames[i],
                                            argDescs[i],
                                            null, null);
            fv.visitEnd();
        }

        generateConstructor();

        if (invokedType.parameterCount() != 0) {
            generateFactory();
        }

        // Forward the SAM method
        MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, samMethodName,
                                          samMethodType.toMethodDescriptorString(), null, null);
        mv.visitAnnotation("Ljava/lang/invoke/LambdaForm$Hidden;", true);
        new ForwardingMethodGenerator(mv).generate(samMethodType);

        // Forward the bridges
        if (additionalBridges != null) {
            for (MethodType mt : additionalBridges) {
                mv = cw.visitMethod(ACC_PUBLIC|ACC_BRIDGE, samMethodName,
                                    mt.toMethodDescriptorString(), null, null);
                mv.visitAnnotation("Ljava/lang/invoke/LambdaForm$Hidden;", true);
                new ForwardingMethodGenerator(mv).generate(mt);
            }
        }

        if (isSerializable)
            generateSerializationFriendlyMethods();
        else if (accidentallySerializable)
            generateSerializationHostileMethods();

        cw.visitEnd();

        // Define the generated class in this VM.

        final byte[] classBytes = cw.toByteArray();

        // If requested, dump out to a file for debugging purposes
        if (dumper != null) {
            AccessController.doPrivileged(new PrivilegedAction<>() {
                @Override
                public Void run() {
                    dumper.dumpClass(lambdaClassName, classBytes);
                    return null;
                }
            }, null,
            new FilePermission("<<ALL FILES>>", "read, write"),
            // createDirectories may need it
            new PropertyPermission("user.dir", "read"));
        }

        return UNSAFE.defineAnonymousClass(targetClass, classBytes, null);
    }

    /**
     * Generate the factory method for the class
     */
    private void generateFactory() {
        MethodVisitor m = cw.visitMethod(ACC_PRIVATE | ACC_STATIC, NAME_FACTORY, invokedType.toMethodDescriptorString(), null, null);
        m.visitCode();
        m.visitTypeInsn(NEW, lambdaClassName);
        m.visitInsn(Opcodes.DUP);
        int parameterCount = invokedType.parameterCount();
        for (int typeIndex = 0, varIndex = 0; typeIndex < parameterCount; typeIndex++) {
            Class<?> argType = invokedType.parameterType(typeIndex);
            m.visitVarInsn(getLoadOpcode(argType), varIndex);
            varIndex += getParameterSize(argType);
        }
        m.visitMethodInsn(INVOKESPECIAL, lambdaClassName, NAME_CTOR, constructorType.toMethodDescriptorString(), false);
        m.visitInsn(ARETURN);
        m.visitMaxs(-1, -1);
        m.visitEnd();
    }

    /**
     * Generate the constructor for the class
     */
    private void generateConstructor() {
        // Generate constructor
        MethodVisitor ctor = cw.visitMethod(ACC_PRIVATE, NAME_CTOR,
                                            constructorType.toMethodDescriptorString(), null, null);
        ctor.visitCode();
        ctor.visitVarInsn(ALOAD, 0);
        ctor.visitMethodInsn(INVOKESPECIAL, JAVA_LANG_OBJECT, NAME_CTOR,
                             METHOD_DESCRIPTOR_VOID, false);
        int parameterCount = invokedType.parameterCount();
        for (int i = 0, lvIndex = 0; i < parameterCount; i++) {
            ctor.visitVarInsn(ALOAD, 0);
            Class<?> argType = invokedType.parameterType(i);
            ctor.visitVarInsn(getLoadOpcode(argType), lvIndex + 1);
            lvIndex += getParameterSize(argType);
            ctor.visitFieldInsn(PUTFIELD, lambdaClassName, argNames[i], argDescs[i]);
        }
        ctor.visitInsn(RETURN);
        // Maxs computed by ClassWriter.COMPUTE_MAXS, these arguments ignored
        ctor.visitMaxs(-1, -1);
        ctor.visitEnd();
    }

    /**
     * Generate a writeReplace method that supports serialization
     */
    private void generateSerializationFriendlyMethods() {
        TypeConvertingMethodAdapter mv
                = new TypeConvertingMethodAdapter(
                    cw.visitMethod(ACC_PRIVATE + ACC_FINAL,
                    NAME_METHOD_WRITE_REPLACE, DESCR_METHOD_WRITE_REPLACE,
                    null, null));

        mv.visitCode();
        mv.visitTypeInsn(NEW, NAME_SERIALIZED_LAMBDA);
        mv.visitInsn(DUP);
        mv.visitLdcInsn(Type.getType(targetClass));
        mv.visitLdcInsn(invokedType.returnType().getName().replace('.', '/'));
        mv.visitLdcInsn(samMethodName);
        mv.visitLdcInsn(samMethodType.toMethodDescriptorString());
        mv.visitLdcInsn(implInfo.getReferenceKind());
        mv.visitLdcInsn(implInfo.getDeclaringClass().getName().replace('.', '/'));
        mv.visitLdcInsn(implInfo.getName());
        mv.visitLdcInsn(implInfo.getMethodType().toMethodDescriptorString());
        mv.visitLdcInsn(instantiatedMethodType.toMethodDescriptorString());
        mv.iconst(argDescs.length);
        mv.visitTypeInsn(ANEWARRAY, JAVA_LANG_OBJECT);
        for (int i = 0; i < argDescs.length; i++) {
            mv.visitInsn(DUP);
            mv.iconst(i);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitFieldInsn(GETFIELD, lambdaClassName, argNames[i], argDescs[i]);
            mv.boxIfTypePrimitive(Type.getType(argDescs[i]));
            mv.visitInsn(AASTORE);
        }
        mv.visitMethodInsn(INVOKESPECIAL, NAME_SERIALIZED_LAMBDA, NAME_CTOR,
                DESCR_CTOR_SERIALIZED_LAMBDA, false);
        mv.visitInsn(ARETURN);
        // Maxs computed by ClassWriter.COMPUTE_MAXS, these arguments ignored
        mv.visitMaxs(-1, -1);
        mv.visitEnd();
    }

    /**
     * Generate a readObject/writeObject method that is hostile to serialization
     */
    private void generateSerializationHostileMethods() {
        MethodVisitor mv = cw.visitMethod(ACC_PRIVATE + ACC_FINAL,
                                          NAME_METHOD_WRITE_OBJECT, DESCR_METHOD_WRITE_OBJECT,
                                          null, SER_HOSTILE_EXCEPTIONS);
        mv.visitCode();
        mv.visitTypeInsn(NEW, NAME_NOT_SERIALIZABLE_EXCEPTION);
        mv.visitInsn(DUP);
        mv.visitLdcInsn("Non-serializable lambda");
        mv.visitMethodInsn(INVOKESPECIAL, NAME_NOT_SERIALIZABLE_EXCEPTION, NAME_CTOR,
                           DESCR_CTOR_NOT_SERIALIZABLE_EXCEPTION, false);
        mv.visitInsn(ATHROW);
        mv.visitMaxs(-1, -1);
        mv.visitEnd();

        mv = cw.visitMethod(ACC_PRIVATE + ACC_FINAL,
                            NAME_METHOD_READ_OBJECT, DESCR_METHOD_READ_OBJECT,
                            null, SER_HOSTILE_EXCEPTIONS);
        mv.visitCode();
        mv.visitTypeInsn(NEW, NAME_NOT_SERIALIZABLE_EXCEPTION);
        mv.visitInsn(DUP);
        mv.visitLdcInsn("Non-serializable lambda");
        mv.visitMethodInsn(INVOKESPECIAL, NAME_NOT_SERIALIZABLE_EXCEPTION, NAME_CTOR,
                           DESCR_CTOR_NOT_SERIALIZABLE_EXCEPTION, false);
        mv.visitInsn(ATHROW);
        mv.visitMaxs(-1, -1);
        mv.visitEnd();
    }

    /**
     * This class generates a method body which calls the lambda implementation
     * method, converting arguments, as needed.
     */
    private class ForwardingMethodGenerator extends TypeConvertingMethodAdapter {

        ForwardingMethodGenerator(MethodVisitor mv) {
            super(mv);
        }

        void generate(MethodType methodType) {
            visitCode();

            if (implKind == MethodHandleInfo.REF_newInvokeSpecial) {
                visitTypeInsn(NEW, implMethodClassName);
                visitInsn(DUP);
            }
            for (int i = 0; i < argNames.length; i++) {
                visitVarInsn(ALOAD, 0);
                visitFieldInsn(GETFIELD, lambdaClassName, argNames[i], argDescs[i]);
            }

            convertArgumentTypes(methodType);

            // Invoke the method we want to forward to
            visitMethodInsn(invocationOpcode(), implMethodClassName,
                            implMethodName, implMethodDesc,
                            implClass.isInterface());

            // Convert the return value (if any) and return it
            // Note: if adapting from non-void to void, the 'return'
            // instruction will pop the unneeded result
            Class<?> implReturnClass = implMethodType.returnType();
            Class<?> samReturnClass = methodType.returnType();
            convertType(implReturnClass, samReturnClass, samReturnClass);
            visitInsn(getReturnOpcode(samReturnClass));
            // Maxs computed by ClassWriter.COMPUTE_MAXS,these arguments ignored
            visitMaxs(-1, -1);
            visitEnd();
        }

        private void convertArgumentTypes(MethodType samType) {
            int lvIndex = 0;
            int samParametersLength = samType.parameterCount();
            int captureArity = invokedType.parameterCount();
            for (int i = 0; i < samParametersLength; i++) {
                Class<?> argType = samType.parameterType(i);
                visitVarInsn(getLoadOpcode(argType), lvIndex + 1);
                lvIndex += getParameterSize(argType);
                convertType(argType, implMethodType.parameterType(captureArity + i), instantiatedMethodType.parameterType(i));
            }
        }

        private int invocationOpcode() throws InternalError {
            switch (implKind) {
                case MethodHandleInfo.REF_invokeStatic:
                    return INVOKESTATIC;
                case MethodHandleInfo.REF_newInvokeSpecial:
                    return INVOKESPECIAL;
                 case MethodHandleInfo.REF_invokeVirtual:
                    return INVOKEVIRTUAL;
                case MethodHandleInfo.REF_invokeInterface:
                    return INVOKEINTERFACE;
                case MethodHandleInfo.REF_invokeSpecial:
                    return INVOKESPECIAL;
                default:
                    throw new InternalError("Unexpected invocation kind: " + implKind);
            }
        }
    }

    static int getParameterSize(Class<?> c) {
        if (c == Void.TYPE) {
            return 0;
        } else if (c == Long.TYPE || c == Double.TYPE) {
            return 2;
        }
        return 1;
    }

    static int getLoadOpcode(Class<?> c) {
        if(c == Void.TYPE) {
            throw new InternalError("Unexpected void type of load opcode");
        }
        return ILOAD + getOpcodeOffset(c);
    }

    static int getReturnOpcode(Class<?> c) {
        if(c == Void.TYPE) {
            return RETURN;
        }
        return IRETURN + getOpcodeOffset(c);
    }

    private static int getOpcodeOffset(Class<?> c) {
        if (c.isPrimitive()) {
            if (c == Long.TYPE) {
                return 1;
            } else if (c == Float.TYPE) {
                return 2;
            } else if (c == Double.TYPE) {
                return 3;
            }
            return 0;
        } else {
            return 4;
        }
    }

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\InvokeDynamic.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2008, 2011, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

/**
 * This is a place-holder class.  Some HotSpot implementations need to see it.
 */
final class InvokeDynamic {
    private InvokeDynamic() { throw new InternalError(); }  // do not instantiate
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\InvokerBytecodeGenerator.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2012, 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import jdk.internal.org.objectweb.asm.ClassWriter;
import jdk.internal.org.objectweb.asm.Label;
import jdk.internal.org.objectweb.asm.MethodVisitor;
import jdk.internal.org.objectweb.asm.Opcodes;
import jdk.internal.org.objectweb.asm.Type;
import sun.invoke.util.VerifyAccess;
import sun.invoke.util.VerifyType;
import sun.invoke.util.Wrapper;
import sun.reflect.misc.ReflectUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Stream;

import static java.lang.invoke.LambdaForm.BasicType;
import static java.lang.invoke.LambdaForm.BasicType.*;
import static java.lang.invoke.LambdaForm.*;
import static java.lang.invoke.MethodHandleNatives.Constants.*;
import static java.lang.invoke.MethodHandleStatics.*;

/**
 * Code generation backend for LambdaForm.
 * <p>
 * @author John Rose, JSR 292 EG
 */
class InvokerBytecodeGenerator {
    /** Define class names for convenience. */
    private static final String MH      = "java/lang/invoke/MethodHandle";
    private static final String MHI     = "java/lang/invoke/MethodHandleImpl";
    private static final String LF      = "java/lang/invoke/LambdaForm";
    private static final String LFN     = "java/lang/invoke/LambdaForm$Name";
    private static final String CLS     = "java/lang/Class";
    private static final String OBJ     = "java/lang/Object";
    private static final String OBJARY  = "[Ljava/lang/Object;";

    private static final String LOOP_CLAUSES = MHI + "$LoopClauses";
    private static final String MHARY2       = "[[L" + MH + ";";

    private static final String LF_SIG  = "L" + LF + ";";
    private static final String LFN_SIG = "L" + LFN + ";";
    private static final String LL_SIG  = "(L" + OBJ + ";)L" + OBJ + ";";
    private static final String LLV_SIG = "(L" + OBJ + ";L" + OBJ + ";)V";
    private static final String CLASS_PREFIX = LF + "$";
    private static final String SOURCE_PREFIX = "LambdaForm$";

    /** Name of its super class*/
    static final String INVOKER_SUPER_NAME = OBJ;

    /** Name of new class */
    private final String className;

    private final LambdaForm lambdaForm;
    private final String     invokerName;
    private final MethodType invokerType;

    /** Info about local variables in compiled lambda form */
    private int[]       localsMap;    // index
    private Class<?>[]  localClasses; // type

    /** ASM bytecode generation. */
    private ClassWriter cw;
    private MethodVisitor mv;

    /** Single element internal class name lookup cache. */
    private Class<?> lastClass;
    private String lastInternalName;

    private static final MemberName.Factory MEMBERNAME_FACTORY = MemberName.getFactory();
    private static final Class<?> HOST_CLASS = LambdaForm.class;

    /** Main constructor; other constructors delegate to this one. */
    private InvokerBytecodeGenerator(LambdaForm lambdaForm, int localsMapSize,
                                     String className, String invokerName, MethodType invokerType) {
        int p = invokerName.indexOf('.');
        if (p > -1) {
            className = invokerName.substring(0, p);
            invokerName = invokerName.substring(p + 1);
        }
        if (DUMP_CLASS_FILES) {
            className = makeDumpableClassName(className);
        }
        this.className  = className;
        this.lambdaForm = lambdaForm;
        this.invokerName = invokerName;
        this.invokerType = invokerType;
        this.localsMap = new int[localsMapSize+1]; // last entry of localsMap is count of allocated local slots
        this.localClasses = new Class<?>[localsMapSize+1];
    }

    /** For generating LambdaForm interpreter entry points. */
    private InvokerBytecodeGenerator(String className, String invokerName, MethodType invokerType) {
        this(null, invokerType.parameterCount(),
             className, invokerName, invokerType);
        // Create an array to map name indexes to locals indexes.
        for (int i = 0; i < localsMap.length; i++) {
            localsMap[i] = invokerType.parameterSlotCount() - invokerType.parameterSlotDepth(i);
        }
    }

    /** For generating customized code for a single LambdaForm. */
    private InvokerBytecodeGenerator(String className, LambdaForm form, MethodType invokerType) {
        this(className, form.lambdaName(), form, invokerType);
    }

    /** For generating customized code for a single LambdaForm. */
    InvokerBytecodeGenerator(String className, String invokerName,
            LambdaForm form, MethodType invokerType) {
        this(form, form.names.length,
             className, invokerName, invokerType);
        // Create an array to map name indexes to locals indexes.
        Name[] names = form.names;
        for (int i = 0, index = 0; i < localsMap.length; i++) {
            localsMap[i] = index;
            if (i < names.length) {
                BasicType type = names[i].type();
                index += type.basicTypeSlots();
            }
        }
    }

    /** instance counters for dumped classes */
    private static final HashMap<String,Integer> DUMP_CLASS_FILES_COUNTERS;
    /** debugging flag for saving generated class files */
    private static final File DUMP_CLASS_FILES_DIR;

    static {
        if (DUMP_CLASS_FILES) {
            DUMP_CLASS_FILES_COUNTERS = new HashMap<>();
            try {
                File dumpDir = new File("DUMP_CLASS_FILES");
                if (!dumpDir.exists()) {
                    dumpDir.mkdirs();
                }
                DUMP_CLASS_FILES_DIR = dumpDir;
                System.out.println("Dumping class files to "+DUMP_CLASS_FILES_DIR+"/...");
            } catch (Exception e) {
                throw newInternalError(e);
            }
        } else {
            DUMP_CLASS_FILES_COUNTERS = null;
            DUMP_CLASS_FILES_DIR = null;
        }
    }

    private void maybeDump(final byte[] classFile) {
        if (DUMP_CLASS_FILES) {
            maybeDump(CLASS_PREFIX + className, classFile);
        }
    }

    // Also used from BoundMethodHandle
    static void maybeDump(final String className, final byte[] classFile) {
        if (DUMP_CLASS_FILES) {
            java.security.AccessController.doPrivileged(
            new java.security.PrivilegedAction<>() {
                public Void run() {
                    try {
                        String dumpName = className.replace('.','/');
                        File dumpFile = new File(DUMP_CLASS_FILES_DIR, dumpName+".class");
                        System.out.println("dump: " + dumpFile);
                        dumpFile.getParentFile().mkdirs();
                        FileOutputStream file = new FileOutputStream(dumpFile);
                        file.write(classFile);
                        file.close();
                        return null;
                    } catch (IOException ex) {
                        throw newInternalError(ex);
                    }
                }
            });
        }
    }

    private static String makeDumpableClassName(String className) {
        Integer ctr;
        synchronized (DUMP_CLASS_FILES_COUNTERS) {
            ctr = DUMP_CLASS_FILES_COUNTERS.get(className);
            if (ctr == null)  ctr = 0;
            DUMP_CLASS_FILES_COUNTERS.put(className, ctr+1);
        }
        String sfx = ctr.toString();
        while (sfx.length() < 3)
            sfx = "0"+sfx;
        className += sfx;
        return className;
    }

    class CpPatch {
        final int index;
        final Object value;
        CpPatch(int index, Object value) {
            this.index = index;
            this.value = value;
        }
        public String toString() {
            return "CpPatch/index="+index+",value="+value;
        }
    }

    private final ArrayList<CpPatch> cpPatches = new ArrayList<>();

    private int cph = 0;  // for counting constant placeholders

    String constantPlaceholder(Object arg) {
        String cpPlaceholder = "CONSTANT_PLACEHOLDER_" + cph++;
        if (DUMP_CLASS_FILES) cpPlaceholder += " <<" + debugString(arg) + ">>";
        // TODO check if arg is already in the constant pool
        // insert placeholder in CP and remember the patch
        int index = cw.newConst((Object) cpPlaceholder);
        cpPatches.add(new CpPatch(index, arg));
        return cpPlaceholder;
    }

    Object[] cpPatches(byte[] classFile) {
        int size = getConstantPoolSize(classFile);
        Object[] res = new Object[size];
        for (CpPatch p : cpPatches) {
            if (p.index >= size)
                throw new InternalError("in cpool["+size+"]: "+p+"\n"+Arrays.toString(Arrays.copyOf(classFile, 20)));
            res[p.index] = p.value;
        }
        return res;
    }

    private static String debugString(Object arg) {
        if (arg instanceof MethodHandle) {
            MethodHandle mh = (MethodHandle) arg;
            MemberName member = mh.internalMemberName();
            if (member != null)
                return member.toString();
            return mh.debugString();
        }
        return arg.toString();
    }

    /**
     * Extract the number of constant pool entries from a given class file.
     *
     * @param classFile the bytes of the class file in question.
     * @return the number of entries in the constant pool.
     */
    private static int getConstantPoolSize(byte[] classFile) {
        // The first few bytes:
        // u4 magic;
        // u2 minor_version;
        // u2 major_version;
        // u2 constant_pool_count;
        return ((classFile[8] & 0xFF) << 8) | (classFile[9] & 0xFF);
    }

    /**
     * Extract the MemberName of a newly-defined method.
     */
    private MemberName loadMethod(byte[] classFile) {
        Class<?> invokerClass = loadAndInitializeInvokerClass(classFile, cpPatches(classFile));
        return resolveInvokerMember(invokerClass, invokerName, invokerType);
    }

    /**
     * Define a given class as anonymous class in the runtime system.
     */
    private static Class<?> loadAndInitializeInvokerClass(byte[] classBytes, Object[] patches) {
        Class<?> invokerClass = UNSAFE.defineAnonymousClass(HOST_CLASS, classBytes, patches);
        UNSAFE.ensureClassInitialized(invokerClass);  // Make sure the class is initialized; VM might complain.
        return invokerClass;
    }

    private static MemberName resolveInvokerMember(Class<?> invokerClass, String name, MethodType type) {
        MemberName member = new MemberName(invokerClass, name, type, REF_invokeStatic);
        try {
            member = MEMBERNAME_FACTORY.resolveOrFail(REF_invokeStatic, member, HOST_CLASS, ReflectiveOperationException.class);
        } catch (ReflectiveOperationException e) {
            throw newInternalError(e);
        }
        return member;
    }

    /**
     * Set up class file generation.
     */
    private ClassWriter classFilePrologue() {
        final int NOT_ACC_PUBLIC = 0;  // not ACC_PUBLIC
        cw = new ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES);
        cw.visit(Opcodes.V1_8, NOT_ACC_PUBLIC + Opcodes.ACC_FINAL + Opcodes.ACC_SUPER,
                CLASS_PREFIX + className, null, INVOKER_SUPER_NAME, null);
        cw.visitSource(SOURCE_PREFIX + className, null);
        return cw;
    }

    private void methodPrologue() {
        String invokerDesc = invokerType.toMethodDescriptorString();
        mv = cw.visitMethod(Opcodes.ACC_STATIC, invokerName, invokerDesc, null, null);
    }

    /**
     * Tear down class file generation.
     */
    private void methodEpilogue() {
        mv.visitMaxs(0, 0);
        mv.visitEnd();
    }

    /*
     * Low-level emit helpers.
     */
    private void emitConst(Object con) {
        if (con == null) {
            mv.visitInsn(Opcodes.ACONST_NULL);
            return;
        }
        if (con instanceof Integer) {
            emitIconstInsn((int) con);
            return;
        }
        if (con instanceof Byte) {
            emitIconstInsn((byte)con);
            return;
        }
        if (con instanceof Short) {
            emitIconstInsn((short)con);
            return;
        }
        if (con instanceof Character) {
            emitIconstInsn((char)con);
            return;
        }
        if (con instanceof Long) {
            long x = (long) con;
            short sx = (short)x;
            if (x == sx) {
                if (sx >= 0 && sx <= 1) {
                    mv.visitInsn(Opcodes.LCONST_0 + (int) sx);
                } else {
                    emitIconstInsn((int) x);
                    mv.visitInsn(Opcodes.I2L);
                }
                return;
            }
        }
        if (con instanceof Float) {
            float x = (float) con;
            short sx = (short)x;
            if (x == sx) {
                if (sx >= 0 && sx <= 2) {
                    mv.visitInsn(Opcodes.FCONST_0 + (int) sx);
                } else {
                    emitIconstInsn((int) x);
                    mv.visitInsn(Opcodes.I2F);
                }
                return;
            }
        }
        if (con instanceof Double) {
            double x = (double) con;
            short sx = (short)x;
            if (x == sx) {
                if (sx >= 0 && sx <= 1) {
                    mv.visitInsn(Opcodes.DCONST_0 + (int) sx);
                } else {
                    emitIconstInsn((int) x);
                    mv.visitInsn(Opcodes.I2D);
                }
                return;
            }
        }
        if (con instanceof Boolean) {
            emitIconstInsn((boolean) con ? 1 : 0);
            return;
        }
        // fall through:
        mv.visitLdcInsn(con);
    }

    private void emitIconstInsn(final int cst) {
        if (cst >= -1 && cst <= 5) {
            mv.visitInsn(Opcodes.ICONST_0 + cst);
        } else if (cst >= Byte.MIN_VALUE && cst <= Byte.MAX_VALUE) {
            mv.visitIntInsn(Opcodes.BIPUSH, cst);
        } else if (cst >= Short.MIN_VALUE && cst <= Short.MAX_VALUE) {
            mv.visitIntInsn(Opcodes.SIPUSH, cst);
        } else {
            mv.visitLdcInsn(cst);
        }
    }

    /*
     * NOTE: These load/store methods use the localsMap to find the correct index!
     */
    private void emitLoadInsn(BasicType type, int index) {
        int opcode = loadInsnOpcode(type);
        mv.visitVarInsn(opcode, localsMap[index]);
    }

    private int loadInsnOpcode(BasicType type) throws InternalError {
        switch (type) {
            case I_TYPE: return Opcodes.ILOAD;
            case J_TYPE: return Opcodes.LLOAD;
            case F_TYPE: return Opcodes.FLOAD;
            case D_TYPE: return Opcodes.DLOAD;
            case L_TYPE: return Opcodes.ALOAD;
            default:
                throw new InternalError("unknown type: " + type);
        }
    }
    private void emitAloadInsn(int index) {
        emitLoadInsn(L_TYPE, index);
    }

    private void emitStoreInsn(BasicType type, int index) {
        int opcode = storeInsnOpcode(type);
        mv.visitVarInsn(opcode, localsMap[index]);
    }

    private int storeInsnOpcode(BasicType type) throws InternalError {
        switch (type) {
            case I_TYPE: return Opcodes.ISTORE;
            case J_TYPE: return Opcodes.LSTORE;
            case F_TYPE: return Opcodes.FSTORE;
            case D_TYPE: return Opcodes.DSTORE;
            case L_TYPE: return Opcodes.ASTORE;
            default:
                throw new InternalError("unknown type: " + type);
        }
    }
    private void emitAstoreInsn(int index) {
        emitStoreInsn(L_TYPE, index);
    }

    private byte arrayTypeCode(Wrapper elementType) {
        switch (elementType) {
            case BOOLEAN: return Opcodes.T_BOOLEAN;
            case BYTE:    return Opcodes.T_BYTE;
            case CHAR:    return Opcodes.T_CHAR;
            case SHORT:   return Opcodes.T_SHORT;
            case INT:     return Opcodes.T_INT;
            case LONG:    return Opcodes.T_LONG;
            case FLOAT:   return Opcodes.T_FLOAT;
            case DOUBLE:  return Opcodes.T_DOUBLE;
            case OBJECT:  return 0; // in place of Opcodes.T_OBJECT
            default:      throw new InternalError();
        }
    }

    private int arrayInsnOpcode(byte tcode, int aaop) throws InternalError {
        assert(aaop == Opcodes.AASTORE || aaop == Opcodes.AALOAD);
        int xas;
        switch (tcode) {
            case Opcodes.T_BOOLEAN: xas = Opcodes.BASTORE; break;
            case Opcodes.T_BYTE:    xas = Opcodes.BASTORE; break;
            case Opcodes.T_CHAR:    xas = Opcodes.CASTORE; break;
            case Opcodes.T_SHORT:   xas = Opcodes.SASTORE; break;
            case Opcodes.T_INT:     xas = Opcodes.IASTORE; break;
            case Opcodes.T_LONG:    xas = Opcodes.LASTORE; break;
            case Opcodes.T_FLOAT:   xas = Opcodes.FASTORE; break;
            case Opcodes.T_DOUBLE:  xas = Opcodes.DASTORE; break;
            case 0:                 xas = Opcodes.AASTORE; break;
            default:      throw new InternalError();
        }
        return xas - Opcodes.AASTORE + aaop;
    }

    /**
     * Emit a boxing call.
     *
     * @param wrapper primitive type class to box.
     */
    private void emitBoxing(Wrapper wrapper) {
        String owner = "java/lang/" + wrapper.wrapperType().getSimpleName();
        String name  = "valueOf";
        String desc  = "(" + wrapper.basicTypeChar() + ")L" + owner + ";";
        mv.visitMethodInsn(Opcodes.INVOKESTATIC, owner, name, desc, false);
    }

    /**
     * Emit an unboxing call (plus preceding checkcast).
     *
     * @param wrapper wrapper type class to unbox.
     */
    private void emitUnboxing(Wrapper wrapper) {
        String owner = "java/lang/" + wrapper.wrapperType().getSimpleName();
        String name  = wrapper.primitiveSimpleName() + "Value";
        String desc  = "()" + wrapper.basicTypeChar();
        emitReferenceCast(wrapper.wrapperType(), null);
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, owner, name, desc, false);
    }

    /**
     * Emit an implicit conversion for an argument which must be of the given pclass.
     * This is usually a no-op, except when pclass is a subword type or a reference other than Object or an interface.
     *
     * @param ptype type of value present on stack
     * @param pclass type of value required on stack
     * @param arg compile-time representation of value on stack (Node, constant) or null if none
     */
    private void emitImplicitConversion(BasicType ptype, Class<?> pclass, Object arg) {
        assert(basicType(pclass) == ptype);  // boxing/unboxing handled by caller
        if (pclass == ptype.basicTypeClass() && ptype != L_TYPE)
            return;   // nothing to do
        switch (ptype) {
            case L_TYPE:
                if (VerifyType.isNullConversion(Object.class, pclass, false)) {
                    if (PROFILE_LEVEL > 0)
                        emitReferenceCast(Object.class, arg);
                    return;
                }
                emitReferenceCast(pclass, arg);
                return;
            case I_TYPE:
                if (!VerifyType.isNullConversion(int.class, pclass, false))
                    emitPrimCast(ptype.basicTypeWrapper(), Wrapper.forPrimitiveType(pclass));
                return;
        }
        throw newInternalError("bad implicit conversion: tc="+ptype+": "+pclass);
    }

    /** Update localClasses type map.  Return true if the information is already present. */
    private boolean assertStaticType(Class<?> cls, Name n) {
        int local = n.index();
        Class<?> aclass = localClasses[local];
        if (aclass != null && (aclass == cls || cls.isAssignableFrom(aclass))) {
            return true;  // type info is already present
        } else if (aclass == null || aclass.isAssignableFrom(cls)) {
            localClasses[local] = cls;  // type info can be improved
        }
        return false;
    }

    private void emitReferenceCast(Class<?> cls, Object arg) {
        Name writeBack = null;  // local to write back result
        if (arg instanceof Name) {
            Name n = (Name) arg;
            if (lambdaForm.useCount(n) > 1) {
                // This guy gets used more than once.
                writeBack = n;
                if (assertStaticType(cls, n)) {
                    return; // this cast was already performed
                }
            }
        }
        if (isStaticallyNameable(cls)) {
            String sig = getInternalName(cls);
            mv.visitTypeInsn(Opcodes.CHECKCAST, sig);
        } else {
            mv.visitLdcInsn(constantPlaceholder(cls));
            mv.visitTypeInsn(Opcodes.CHECKCAST, CLS);
            mv.visitInsn(Opcodes.SWAP);
            mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, CLS, "cast", LL_SIG, false);
            if (Object[].class.isAssignableFrom(cls))
                mv.visitTypeInsn(Opcodes.CHECKCAST, OBJARY);
            else if (PROFILE_LEVEL > 0)
                mv.visitTypeInsn(Opcodes.CHECKCAST, OBJ);
        }
        if (writeBack != null) {
            mv.visitInsn(Opcodes.DUP);
            emitAstoreInsn(writeBack.index());
        }
    }

    /**
     * Emits an actual return instruction conforming to the given return type.
     */
    private void emitReturnInsn(BasicType type) {
        int opcode;
        switch (type) {
        case I_TYPE:  opcode = Opcodes.IRETURN;  break;
        case J_TYPE:  opcode = Opcodes.LRETURN;  break;
        case F_TYPE:  opcode = Opcodes.FRETURN;  break;
        case D_TYPE:  opcode = Opcodes.DRETURN;  break;
        case L_TYPE:  opcode = Opcodes.ARETURN;  break;
        case V_TYPE:  opcode = Opcodes.RETURN;   break;
        default:
            throw new InternalError("unknown return type: " + type);
        }
        mv.visitInsn(opcode);
    }

    private String getInternalName(Class<?> c) {
        if (c == Object.class)             return OBJ;
        else if (c == Object[].class)      return OBJARY;
        else if (c == Class.class)         return CLS;
        else if (c == MethodHandle.class)  return MH;
        assert(VerifyAccess.isTypeVisible(c, Object.class)) : c.getName();

        if (c == lastClass) {
            return lastInternalName;
        }
        lastClass = c;
        return lastInternalName = c.getName().replace('.', '/');
    }

    private static MemberName resolveFrom(String name, MethodType type, Class<?> holder) {
        MemberName member = new MemberName(holder, name, type, REF_invokeStatic);
        MemberName resolvedMember = MemberName.getFactory().resolveOrNull(REF_invokeStatic, member, holder);
        if (TRACE_RESOLVE) {
            System.out.println("[LF_RESOLVE] " + holder.getName() + " " + name + " " +
                    shortenSignature(basicTypeSignature(type)) + (resolvedMember != null ? " (success)" : " (fail)") );
        }
        return resolvedMember;
    }

    private static MemberName lookupPregenerated(LambdaForm form, MethodType invokerType) {
        if (form.customized != null) {
            // No pre-generated version for customized LF
            return null;
        }
        String name = form.kind.methodName;
        switch (form.kind) {
            case BOUND_REINVOKER: {
                name = name + "_" + BoundMethodHandle.speciesDataFor(form).key();
                return resolveFrom(name, invokerType, DelegatingMethodHandle.Holder.class);
            }
            case DELEGATE:                  return resolveFrom(name, invokerType, DelegatingMethodHandle.Holder.class);
            case ZERO:                      // fall-through
            case IDENTITY: {
                name = name + "_" + form.returnType().basicTypeChar();
                return resolveFrom(name, invokerType, LambdaForm.Holder.class);
            }
            case EXACT_INVOKER:             // fall-through
            case EXACT_LINKER:              // fall-through
            case LINK_TO_CALL_SITE:         // fall-through
            case LINK_TO_TARGET_METHOD:     // fall-through
            case GENERIC_INVOKER:           // fall-through
            case GENERIC_LINKER:            return resolveFrom(name, invokerType.basicType(), Invokers.Holder.class);
            case GET_OBJECT:                // fall-through
            case GET_BOOLEAN:               // fall-through
            case GET_BYTE:                  // fall-through
            case GET_CHAR:                  // fall-through
            case GET_SHORT:                 // fall-through
            case GET_INT:                   // fall-through
            case GET_LONG:                  // fall-through
            case GET_FLOAT:                 // fall-through
            case GET_DOUBLE:                // fall-through
            case PUT_OBJECT:                // fall-through
            case PUT_BOOLEAN:               // fall-through
            case PUT_BYTE:                  // fall-through
            case PUT_CHAR:                  // fall-through
            case PUT_SHORT:                 // fall-through
            case PUT_INT:                   // fall-through
            case PUT_LONG:                  // fall-through
            case PUT_FLOAT:                 // fall-through
            case PUT_DOUBLE:                // fall-through
            case DIRECT_NEW_INVOKE_SPECIAL: // fall-through
            case DIRECT_INVOKE_INTERFACE:   // fall-through
            case DIRECT_INVOKE_SPECIAL:     // fall-through
            case DIRECT_INVOKE_SPECIAL_IFC: // fall-through
            case DIRECT_INVOKE_STATIC:      // fall-through
            case DIRECT_INVOKE_STATIC_INIT: // fall-through
            case DIRECT_INVOKE_VIRTUAL:     return resolveFrom(name, invokerType, DirectMethodHandle.Holder.class);
        }
        return null;
    }

    /**
     * Generate customized bytecode for a given LambdaForm.
     */
    static MemberName generateCustomizedCode(LambdaForm form, MethodType invokerType) {
        MemberName pregenerated = lookupPregenerated(form, invokerType);
        if (pregenerated != null)  return pregenerated; // pre-generated bytecode

        InvokerBytecodeGenerator g = new InvokerBytecodeGenerator("MH", form, invokerType);
        return g.loadMethod(g.generateCustomizedCodeBytes());
    }

    /** Generates code to check that actual receiver and LambdaForm matches */
    private boolean checkActualReceiver() {
        // Expects MethodHandle on the stack and actual receiver MethodHandle in slot #0
        mv.visitInsn(Opcodes.DUP);
        mv.visitVarInsn(Opcodes.ALOAD, localsMap[0]);
        mv.visitMethodInsn(Opcodes.INVOKESTATIC, MHI, "assertSame", LLV_SIG, false);
        return true;
    }

    static String className(String cn) {
        assert checkClassName(cn): "Class not found: " + cn;
        return cn;
    }

    static boolean checkClassName(String cn) {
        Type tp = Type.getType(cn);
        // additional sanity so only valid "L;" descriptors work
        if (tp.getSort() != Type.OBJECT) {
            return false;
        }
        try {
            Class<?> c = Class.forName(tp.getClassName(), false, null);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    static final String  LF_HIDDEN_SIG = className("Ljava/lang/invoke/LambdaForm$Hidden;");
    static final String  LF_COMPILED_SIG = className("Ljava/lang/invoke/LambdaForm$Compiled;");
    static final String  FORCEINLINE_SIG = className("Ljdk/internal/vm/annotation/ForceInline;");
    static final String  DONTINLINE_SIG = className("Ljdk/internal/vm/annotation/DontInline;");
    static final String  INJECTEDPROFILE_SIG = className("Ljava/lang/invoke/InjectedProfile;");

    /**
     * Generate an invoker method for the passed {@link LambdaForm}.
     */
    private byte[] generateCustomizedCodeBytes() {
        classFilePrologue();
        addMethod();
        bogusMethod(lambdaForm);

        final byte[] classFile = toByteArray();
        maybeDump(classFile);
        return classFile;
    }

    void setClassWriter(ClassWriter cw) {
        this.cw = cw;
    }

    void addMethod() {
        methodPrologue();

        // Suppress this method in backtraces displayed to the user.
        mv.visitAnnotation(LF_HIDDEN_SIG, true);

        // Mark this method as a compiled LambdaForm
        mv.visitAnnotation(LF_COMPILED_SIG, true);

        if (lambdaForm.forceInline) {
            // Force inlining of this invoker method.
            mv.visitAnnotation(FORCEINLINE_SIG, true);
        } else {
            mv.visitAnnotation(DONTINLINE_SIG, true);
        }

        constantPlaceholder(lambdaForm); // keep LambdaForm instance & its compiled form lifetime tightly coupled.

        if (lambdaForm.customized != null) {
            // Since LambdaForm is customized for a particular MethodHandle, it's safe to substitute
            // receiver MethodHandle (at slot #0) with an embedded constant and use it instead.
            // It enables more efficient code generation in some situations, since embedded constants
            // are compile-time constants for JIT compiler.
            mv.visitLdcInsn(constantPlaceholder(lambdaForm.customized));
            mv.visitTypeInsn(Opcodes.CHECKCAST, MH);
            assert(checkActualReceiver()); // expects MethodHandle on top of the stack
            mv.visitVarInsn(Opcodes.ASTORE, localsMap[0]);
        }

        // iterate over the form's names, generating bytecode instructions for each
        // start iterating at the first name following the arguments
        Name onStack = null;
        for (int i = lambdaForm.arity; i < lambdaForm.names.length; i++) {
            Name name = lambdaForm.names[i];

            emitStoreResult(onStack);
            onStack = name;  // unless otherwise modified below
            MethodHandleImpl.Intrinsic intr = name.function.intrinsicName();
            switch (intr) {
                case SELECT_ALTERNATIVE:
                    assert lambdaForm.isSelectAlternative(i);
                    if (PROFILE_GWT) {
                        assert(name.arguments[0] instanceof Name &&
                                ((Name)name.arguments[0]).refersTo(MethodHandleImpl.class, "profileBoolean"));
                        mv.visitAnnotation(INJECTEDPROFILE_SIG, true);
                    }
                    onStack = emitSelectAlternative(name, lambdaForm.names[i+1]);
                    i++;  // skip MH.invokeBasic of the selectAlternative result
                    continue;
                case GUARD_WITH_CATCH:
                    assert lambdaForm.isGuardWithCatch(i);
                    onStack = emitGuardWithCatch(i);
                    i += 2; // jump to the end of GWC idiom
                    continue;
                case TRY_FINALLY:
                    assert lambdaForm.isTryFinally(i);
                    onStack = emitTryFinally(i);
                    i += 2; // jump to the end of the TF idiom
                    continue;
                case LOOP:
                    assert lambdaForm.isLoop(i);
                    onStack = emitLoop(i);
                    i += 2; // jump to the end of the LOOP idiom
                    continue;
                case NEW_ARRAY:
                    Class<?> rtype = name.function.methodType().returnType();
                    if (isStaticallyNameable(rtype)) {
                        emitNewArray(name);
                        continue;
                    }
                    break;
                case ARRAY_LOAD:
                    emitArrayLoad(name);
                    continue;
                case ARRAY_STORE:
                    emitArrayStore(name);
                    continue;
                case ARRAY_LENGTH:
                    emitArrayLength(name);
                    continue;
                case IDENTITY:
                    assert(name.arguments.length == 1);
                    emitPushArguments(name, 0);
                    continue;
                case ZERO:
                    assert(name.arguments.length == 0);
                    emitConst(name.type.basicTypeWrapper().zero());
                    continue;
                case NONE:
                    // no intrinsic associated
                    break;
                default:
                    throw newInternalError("Unknown intrinsic: "+intr);
            }

            MemberName member = name.function.member();
            if (isStaticallyInvocable(member)) {
                emitStaticInvoke(member, name);
            } else {
                emitInvoke(name);
            }
        }

        // return statement
        emitReturn(onStack);

        methodEpilogue();
    }

    /*
     * @throws BytecodeGenerationException if something goes wrong when
     *         generating the byte code
     */
    private byte[] toByteArray() {
        try {
            return cw.toByteArray();
        } catch (RuntimeException e) {
            throw new BytecodeGenerationException(e);
        }
    }

    @SuppressWarnings("serial")
    static final class BytecodeGenerationException extends RuntimeException {
        BytecodeGenerationException(Exception cause) {
            super(cause);
        }
    }

    void emitArrayLoad(Name name)   { emitArrayOp(name, Opcodes.AALOAD);      }
    void emitArrayStore(Name name)  { emitArrayOp(name, Opcodes.AASTORE);     }
    void emitArrayLength(Name name) { emitArrayOp(name, Opcodes.ARRAYLENGTH); }

    void emitArrayOp(Name name, int arrayOpcode) {
        assert arrayOpcode == Opcodes.AALOAD || arrayOpcode == Opcodes.AASTORE || arrayOpcode == Opcodes.ARRAYLENGTH;
        Class<?> elementType = name.function.methodType().parameterType(0).getComponentType();
        assert elementType != null;
        emitPushArguments(name, 0);
        if (arrayOpcode != Opcodes.ARRAYLENGTH && elementType.isPrimitive()) {
            Wrapper w = Wrapper.forPrimitiveType(elementType);
            arrayOpcode = arrayInsnOpcode(arrayTypeCode(w), arrayOpcode);
        }
        mv.visitInsn(arrayOpcode);
    }

    /**
     * Emit an invoke for the given name.
     */
    void emitInvoke(Name name) {
        assert(!name.isLinkerMethodInvoke());  // should use the static path for these
        if (true) {
            // push receiver
            MethodHandle target = name.function.resolvedHandle();
            assert(target != null) : name.exprString();
            mv.visitLdcInsn(constantPlaceholder(target));
            emitReferenceCast(MethodHandle.class, target);
        } else {
            // load receiver
            emitAloadInsn(0);
            emitReferenceCast(MethodHandle.class, null);
            mv.visitFieldInsn(Opcodes.GETFIELD, MH, "form", LF_SIG);
            mv.visitFieldInsn(Opcodes.GETFIELD, LF, "names", LFN_SIG);
            // TODO more to come
        }

        // push arguments
        emitPushArguments(name, 0);

        // invocation
        MethodType type = name.function.methodType();
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, MH, "invokeBasic", type.basicType().toMethodDescriptorString(), false);
    }

    private static Class<?>[] STATICALLY_INVOCABLE_PACKAGES = {
        // Sample classes from each package we are willing to bind to statically:
        java.lang.Object.class,
        java.util.Arrays.class,
        jdk.internal.misc.Unsafe.class
        //MethodHandle.class already covered
    };

    static boolean isStaticallyInvocable(NamedFunction ... functions) {
        for (NamedFunction nf : functions) {
            if (!isStaticallyInvocable(nf.member())) {
                return false;
            }
        }
        return true;
    }

    static boolean isStaticallyInvocable(Name name) {
        return isStaticallyInvocable(name.function.member());
    }

    static boolean isStaticallyInvocable(MemberName member) {
        if (member == null)  return false;
        if (member.isConstructor())  return false;
        Class<?> cls = member.getDeclaringClass();
        // Fast-path non-private members declared by MethodHandles, which is a common
        // case
        if (MethodHandle.class.isAssignableFrom(cls) && !member.isPrivate()) {
            assert(isStaticallyInvocableType(member.getMethodOrFieldType()));
            return true;
        }
        if (cls.isArray() || cls.isPrimitive())
            return false;  // FIXME
        if (cls.isAnonymousClass() || cls.isLocalClass())
            return false;  // inner class of some sort
        if (cls.getClassLoader() != MethodHandle.class.getClassLoader())
            return false;  // not on BCP
        if (ReflectUtil.isVMAnonymousClass(cls)) // FIXME: switch to supported API once it is added
            return false;
        if (!isStaticallyInvocableType(member.getMethodOrFieldType()))
            return false;
        if (!member.isPrivate() && VerifyAccess.isSamePackage(MethodHandle.class, cls))
            return true;   // in java.lang.invoke package
        if (member.isPublic() && isStaticallyNameable(cls))
            return true;
        return false;
    }

    private static boolean isStaticallyInvocableType(MethodType mtype) {
        if (!isStaticallyNameable(mtype.returnType()))
            return false;
        for (Class<?> ptype : mtype.parameterArray())
            if (!isStaticallyNameable(ptype))
                return false;
        return true;
    }

    static boolean isStaticallyNameable(Class<?> cls) {
        if (cls == Object.class)
            return true;
        if (MethodHandle.class.isAssignableFrom(cls)) {
            assert(!ReflectUtil.isVMAnonymousClass(cls));
            return true;
        }
        while (cls.isArray())
            cls = cls.getComponentType();
        if (cls.isPrimitive())
            return true;  // int[].class, for example
        if (ReflectUtil.isVMAnonymousClass(cls)) // FIXME: switch to supported API once it is added
            return false;
        // could use VerifyAccess.isClassAccessible but the following is a safe approximation
        if (cls.getClassLoader() != Object.class.getClassLoader())
            return false;
        if (VerifyAccess.isSamePackage(MethodHandle.class, cls))
            return true;
        if (!Modifier.isPublic(cls.getModifiers()))
            return false;
        for (Class<?> pkgcls : STATICALLY_INVOCABLE_PACKAGES) {
            if (VerifyAccess.isSamePackage(pkgcls, cls))
                return true;
        }
        return false;
    }

    void emitStaticInvoke(Name name) {
        emitStaticInvoke(name.function.member(), name);
    }

    /**
     * Emit an invoke for the given name, using the MemberName directly.
     */
    void emitStaticInvoke(MemberName member, Name name) {
        assert(member.equals(name.function.member()));
        Class<?> defc = member.getDeclaringClass();
        String cname = getInternalName(defc);
        String mname = member.getName();
        String mtype;
        byte refKind = member.getReferenceKind();
        if (refKind == REF_invokeSpecial) {
            // in order to pass the verifier, we need to convert this to invokevirtual in all cases
            assert(member.canBeStaticallyBound()) : member;
            refKind = REF_invokeVirtual;
        }

        assert(!(member.getDeclaringClass().isInterface() && refKind == REF_invokeVirtual));

        // push arguments
        emitPushArguments(name, 0);

        // invocation
        if (member.isMethod()) {
            mtype = member.getMethodType().toMethodDescriptorString();
            mv.visitMethodInsn(refKindOpcode(refKind), cname, mname, mtype,
                               member.getDeclaringClass().isInterface());
        } else {
            mtype = MethodType.toFieldDescriptorString(member.getFieldType());
            mv.visitFieldInsn(refKindOpcode(refKind), cname, mname, mtype);
        }
        // Issue a type assertion for the result, so we can avoid casts later.
        if (name.type == L_TYPE) {
            Class<?> rtype = member.getInvocationType().returnType();
            assert(!rtype.isPrimitive());
            if (rtype != Object.class && !rtype.isInterface()) {
                assertStaticType(rtype, name);
            }
        }
    }

    void emitNewArray(Name name) throws InternalError {
        Class<?> rtype = name.function.methodType().returnType();
        if (name.arguments.length == 0) {
            // The array will be a constant.
            Object emptyArray;
            try {
                emptyArray = name.function.resolvedHandle().invoke();
            } catch (Throwable ex) {
                throw uncaughtException(ex);
            }
            assert(java.lang.reflect.Array.getLength(emptyArray) == 0);
            assert(emptyArray.getClass() == rtype);  // exact typing
            mv.visitLdcInsn(constantPlaceholder(emptyArray));
            emitReferenceCast(rtype, emptyArray);
            return;
        }
        Class<?> arrayElementType = rtype.getComponentType();
        assert(arrayElementType != null);
        emitIconstInsn(name.arguments.length);
        int xas = Opcodes.AASTORE;
        if (!arrayElementType.isPrimitive()) {
            mv.visitTypeInsn(Opcodes.ANEWARRAY, getInternalName(arrayElementType));
        } else {
            byte tc = arrayTypeCode(Wrapper.forPrimitiveType(arrayElementType));
            xas = arrayInsnOpcode(tc, xas);
            mv.visitIntInsn(Opcodes.NEWARRAY, tc);
        }
        // store arguments
        for (int i = 0; i < name.arguments.length; i++) {
            mv.visitInsn(Opcodes.DUP);
            emitIconstInsn(i);
            emitPushArgument(name, i);
            mv.visitInsn(xas);
        }
        // the array is left on the stack
        assertStaticType(rtype, name);
    }
    int refKindOpcode(byte refKind) {
        switch (refKind) {
        case REF_invokeVirtual:      return Opcodes.INVOKEVIRTUAL;
        case REF_invokeStatic:       return Opcodes.INVOKESTATIC;
        case REF_invokeSpecial:      return Opcodes.INVOKESPECIAL;
        case REF_invokeInterface:    return Opcodes.INVOKEINTERFACE;
        case REF_getField:           return Opcodes.GETFIELD;
        case REF_putField:           return Opcodes.PUTFIELD;
        case REF_getStatic:          return Opcodes.GETSTATIC;
        case REF_putStatic:          return Opcodes.PUTSTATIC;
        }
        throw new InternalError("refKind="+refKind);
    }

    /**
     * Emit bytecode for the selectAlternative idiom.
     *
     * The pattern looks like (Cf. MethodHandleImpl.makeGuardWithTest):
     * <blockquote><pre>{@code
     *   Lambda(a0:L,a1:I)=>{
     *     t2:I=foo.test(a1:I);
     *     t3:L=MethodHandleImpl.selectAlternative(t2:I,(MethodHandle(int)int),(MethodHandle(int)int));
     *     t4:I=MethodHandle.invokeBasic(t3:L,a1:I);t4:I}
     * }</pre></blockquote>
     */
    private Name emitSelectAlternative(Name selectAlternativeName, Name invokeBasicName) {
        assert isStaticallyInvocable(invokeBasicName);

        Name receiver = (Name) invokeBasicName.arguments[0];

        Label L_fallback = new Label();
        Label L_done     = new Label();

        // load test result
        emitPushArgument(selectAlternativeName, 0);

        // if_icmpne L_fallback
        mv.visitJumpInsn(Opcodes.IFEQ, L_fallback);

        // invoke selectAlternativeName.arguments[1]
        Class<?>[] preForkClasses = localClasses.clone();
        emitPushArgument(selectAlternativeName, 1);  // get 2nd argument of selectAlternative
        emitAstoreInsn(receiver.index());  // store the MH in the receiver slot
        emitStaticInvoke(invokeBasicName);

        // goto L_done
        mv.visitJumpInsn(Opcodes.GOTO, L_done);

        // L_fallback:
        mv.visitLabel(L_fallback);

        // invoke selectAlternativeName.arguments[2]
        System.arraycopy(preForkClasses, 0, localClasses, 0, preForkClasses.length);
        emitPushArgument(selectAlternativeName, 2);  // get 3rd argument of selectAlternative
        emitAstoreInsn(receiver.index());  // store the MH in the receiver slot
        emitStaticInvoke(invokeBasicName);

        // L_done:
        mv.visitLabel(L_done);
        // for now do not bother to merge typestate; just reset to the dominator state
        System.arraycopy(preForkClasses, 0, localClasses, 0, preForkClasses.length);

        return invokeBasicName;  // return what's on stack
    }

    /**
      * Emit bytecode for the guardWithCatch idiom.
      *
      * The pattern looks like (Cf. MethodHandleImpl.makeGuardWithCatch):
      * <blockquote><pre>{@code
      *  guardWithCatch=Lambda(a0:L,a1:L,a2:L,a3:L,a4:L,a5:L,a6:L,a7:L)=>{
      *    t8:L=MethodHandle.invokeBasic(a4:L,a6:L,a7:L);
      *    t9:L=MethodHandleImpl.guardWithCatch(a1:L,a2:L,a3:L,t8:L);
      *   t10:I=MethodHandle.invokeBasic(a5:L,t9:L);t10:I}
      * }</pre></blockquote>
      *
      * It is compiled into bytecode equivalent of the following code:
      * <blockquote><pre>{@code
      *  try {
      *      return a1.invokeBasic(a6, a7);
      *  } catch (Throwable e) {
      *      if (!a2.isInstance(e)) throw e;
      *      return a3.invokeBasic(ex, a6, a7);
      *  }}
      */
    private Name emitGuardWithCatch(int pos) {
        Name args    = lambdaForm.names[pos];
        Name invoker = lambdaForm.names[pos+1];
        Name result  = lambdaForm.names[pos+2];

        Label L_startBlock = new Label();
        Label L_endBlock = new Label();
        Label L_handler = new Label();
        Label L_done = new Label();

        Class<?> returnType = result.function.resolvedHandle().type().returnType();
        MethodType type = args.function.resolvedHandle().type()
                              .dropParameterTypes(0,1)
                              .changeReturnType(returnType);

        mv.visitTryCatchBlock(L_startBlock, L_endBlock, L_handler, "java/lang/Throwable");

        // Normal case
        mv.visitLabel(L_startBlock);
        // load target
        emitPushArgument(invoker, 0);
        emitPushArguments(args, 1); // skip 1st argument: method handle
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, MH, "invokeBasic", type.basicType().toMethodDescriptorString(), false);
        mv.visitLabel(L_endBlock);
        mv.visitJumpInsn(Opcodes.GOTO, L_done);

        // Exceptional case
        mv.visitLabel(L_handler);

        // Check exception's type
        mv.visitInsn(Opcodes.DUP);
        // load exception class
        emitPushArgument(invoker, 1);
        mv.visitInsn(Opcodes.SWAP);
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Class", "isInstance", "(Ljava/lang/Object;)Z", false);
        Label L_rethrow = new Label();
        mv.visitJumpInsn(Opcodes.IFEQ, L_rethrow);

        // Invoke catcher
        // load catcher
        emitPushArgument(invoker, 2);
        mv.visitInsn(Opcodes.SWAP);
        emitPushArguments(args, 1); // skip 1st argument: method handle
        MethodType catcherType = type.insertParameterTypes(0, Throwable.class);
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, MH, "invokeBasic", catcherType.basicType().toMethodDescriptorString(), false);
        mv.visitJumpInsn(Opcodes.GOTO, L_done);

        mv.visitLabel(L_rethrow);
        mv.visitInsn(Opcodes.ATHROW);

        mv.visitLabel(L_done);

        return result;
    }

    /**
     * Emit bytecode for the tryFinally idiom.
     * <p>
     * The pattern looks like (Cf. MethodHandleImpl.makeTryFinally):
     * <blockquote><pre>{@code
     * // a0: BMH
     * // a1: target, a2: cleanup
     * // a3: box, a4: unbox
     * // a5 (and following): arguments
     * tryFinally=Lambda(a0:L,a1:L,a2:L,a3:L,a4:L,a5:L)=>{
     *   t6:L=MethodHandle.invokeBasic(a3:L,a5:L);         // box the arguments into an Object[]
     *   t7:L=MethodHandleImpl.tryFinally(a1:L,a2:L,t6:L); // call the tryFinally executor
     *   t8:L=MethodHandle.invokeBasic(a4:L,t7:L);t8:L}    // unbox the result; return the result
     * }</pre></blockquote>
     * <p>
     * It is compiled into bytecode equivalent to the following code:
     * <blockquote><pre>{@code
     * Throwable t;
     * Object r;
     * try {
     *     r = a1.invokeBasic(a5);
     * } catch (Throwable thrown) {
     *     t = thrown;
     *     throw t;
     * } finally {
     *     r = a2.invokeBasic(t, r, a5);
     * }
     * return r;
     * }</pre></blockquote>
     * <p>
     * Specifically, the bytecode will have the following form (the stack effects are given for the beginnings of
     * blocks, and for the situations after executing the given instruction - the code will have a slightly different
     * shape if the return type is {@code void}):
     * <blockquote><pre>{@code
     * TRY:                 (--)
     *                      load target                             (-- target)
     *                      load args                               (-- args... target)
     *                      INVOKEVIRTUAL MethodHandle.invokeBasic  (depends)
     * FINALLY_NORMAL:      (-- r)
     *                      load cleanup                            (-- cleanup r)
     *                      SWAP                                    (-- r cleanup)
     *                      ACONST_NULL                             (-- t r cleanup)
     *                      SWAP                                    (-- r t cleanup)
     *                      load args                               (-- args... r t cleanup)
     *                      INVOKEVIRTUAL MethodHandle.invokeBasic  (-- r)
     *                      GOTO DONE
     * CATCH:               (-- t)
     *                      DUP                                     (-- t t)
     * FINALLY_EXCEPTIONAL: (-- t t)
     *                      load cleanup                            (-- cleanup t t)
     *                      SWAP                                    (-- t cleanup t)
     *                      load default for r                      (-- r t cleanup t)
     *                      load args                               (-- args... r t cleanup t)
     *                      INVOKEVIRTUAL MethodHandle.invokeBasic  (-- r t)
     *                      POP                                     (-- t)
     *                      ATHROW
     * DONE:                (-- r)
     * }</pre></blockquote>
     */
    private Name emitTryFinally(int pos) {
        Name args    = lambdaForm.names[pos];
        Name invoker = lambdaForm.names[pos+1];
        Name result  = lambdaForm.names[pos+2];

        Label lFrom = new Label();
        Label lTo = new Label();
        Label lCatch = new Label();
        Label lDone = new Label();

        Class<?> returnType = result.function.resolvedHandle().type().returnType();
        boolean isNonVoid = returnType != void.class;
        MethodType type = args.function.resolvedHandle().type()
                .dropParameterTypes(0,1)
                .changeReturnType(returnType);
        MethodType cleanupType = type.insertParameterTypes(0, Throwable.class);
        if (isNonVoid) {
            cleanupType = cleanupType.insertParameterTypes(1, returnType);
        }
        String cleanupDesc = cleanupType.basicType().toMethodDescriptorString();

        // exception handler table
        mv.visitTryCatchBlock(lFrom, lTo, lCatch, "java/lang/Throwable");

        // TRY:
        mv.visitLabel(lFrom);
        emitPushArgument(invoker, 0); // load target
        emitPushArguments(args, 1); // load args (skip 0: method handle)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, MH, "invokeBasic", type.basicType().toMethodDescriptorString(), false);
        mv.visitLabel(lTo);

        // FINALLY_NORMAL:
        emitPushArgument(invoker, 1); // load cleanup
        if (isNonVoid) {
            mv.visitInsn(Opcodes.SWAP);
        }
        mv.visitInsn(Opcodes.ACONST_NULL);
        if (isNonVoid) {
            mv.visitInsn(Opcodes.SWAP);
        }
        emitPushArguments(args, 1); // load args (skip 0: method handle)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, MH, "invokeBasic", cleanupDesc, false);
        mv.visitJumpInsn(Opcodes.GOTO, lDone);

        // CATCH:
        mv.visitLabel(lCatch);
        mv.visitInsn(Opcodes.DUP);

        // FINALLY_EXCEPTIONAL:
        emitPushArgument(invoker, 1); // load cleanup
        mv.visitInsn(Opcodes.SWAP);
        if (isNonVoid) {
            emitZero(BasicType.basicType(returnType)); // load default for result
        }
        emitPushArguments(args, 1); // load args (skip 0: method handle)
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, MH, "invokeBasic", cleanupDesc, false);
        if (isNonVoid) {
            mv.visitInsn(Opcodes.POP);
        }
        mv.visitInsn(Opcodes.ATHROW);

        // DONE:
        mv.visitLabel(lDone);

        return result;
    }

    /**
     * Emit bytecode for the loop idiom.
     * <p>
     * The pattern looks like (Cf. MethodHandleImpl.loop):
     * <blockquote><pre>{@code
     * // a0: BMH
     * // a1: LoopClauses (containing an array of arrays: inits, steps, preds, finis)
     * // a2: box, a3: unbox
     * // a4 (and following): arguments
     * loop=Lambda(a0:L,a1:L,a2:L,a3:L,a4:L)=>{
     *   t5:L=MethodHandle.invokeBasic(a2:L,a4:L);          // box the arguments into an Object[]
     *   t6:L=MethodHandleImpl.loop(bt:L,a1:L,t5:L);        // call the loop executor (with supplied types in bt)
     *   t7:L=MethodHandle.invokeBasic(a3:L,t6:L);t7:L}     // unbox the result; return the result
     * }</pre></blockquote>
     * <p>
     * It is compiled into bytecode equivalent to the code seen in {@link MethodHandleImpl#loop(BasicType[],
     * MethodHandleImpl.LoopClauses, Object...)}, with the difference that no arrays
     * will be used for local state storage. Instead, the local state will be mapped to actual stack slots.
     * <p>
     * Bytecode generation applies an unrolling scheme to enable better bytecode generation regarding local state type
     * handling. The generated bytecode will have the following form ({@code void} types are ignored for convenience).
     * Assume there are {@code C} clauses in the loop.
     * <blockquote><pre>{@code
     * PREINIT: ALOAD_1
     *          CHECKCAST LoopClauses
     *          GETFIELD LoopClauses.clauses
     *          ASTORE clauseDataIndex          // place the clauses 2-dimensional array on the stack
     * INIT:    (INIT_SEQ for clause 1)
     *          ...
     *          (INIT_SEQ for clause C)
     * LOOP:    (LOOP_SEQ for clause 1)
     *          ...
     *          (LOOP_SEQ for clause C)
     *          GOTO LOOP
     * DONE:    ...
     * }</pre></blockquote>
     * <p>
     * The {@code INIT_SEQ_x} sequence for clause {@code x} (with {@code x} ranging from {@code 0} to {@code C-1}) has
     * the following shape. Assume slot {@code vx} is used to hold the state for clause {@code x}.
     * <blockquote><pre>{@code
     * INIT_SEQ_x:  ALOAD clauseDataIndex
     *              ICONST_0
     *              AALOAD      // load the inits array
     *              ICONST x
     *              AALOAD      // load the init handle for clause x
     *              load args
     *              INVOKEVIRTUAL MethodHandle.invokeBasic
     *              store vx
     * }</pre></blockquote>
     * <p>
     * The {@code LOOP_SEQ_x} sequence for clause {@code x} (with {@code x} ranging from {@code 0} to {@code C-1}) has
     * the following shape. Again, assume slot {@code vx} is used to hold the state for clause {@code x}.
     * <blockquote><pre>{@code
     * LOOP_SEQ_x:  ALOAD clauseDataIndex
     *              ICONST_1
     *              AALOAD              // load the steps array
     *              ICONST x
     *              AALOAD              // load the step handle for clause x
     *              load locals
     *              load args
     *              INVOKEVIRTUAL MethodHandle.invokeBasic
     *              store vx
     *              ALOAD clauseDataIndex
     *              ICONST_2
     *              AALOAD              // load the preds array
     *              ICONST x
     *              AALOAD              // load the pred handle for clause x
     *              load locals
     *              load args
     *              INVOKEVIRTUAL MethodHandle.invokeBasic
     *              IFNE LOOP_SEQ_x+1   // predicate returned false -> jump to next clause
     *              ALOAD clauseDataIndex
     *              ICONST_3
     *              AALOAD              // load the finis array
     *              ICONST x
     *              AALOAD              // load the fini handle for clause x
     *              load locals
     *              load args
     *              INVOKEVIRTUAL MethodHandle.invokeBasic
     *              GOTO DONE           // jump beyond end of clauses to return from loop
     * }</pre></blockquote>
     */
    private Name emitLoop(int pos) {
        Name args    = lambdaForm.names[pos];
        Name invoker = lambdaForm.names[pos+1];
        Name result  = lambdaForm.names[pos+2];

        // extract clause and loop-local state types
        // find the type info in the loop invocation
        BasicType[] loopClauseTypes = (BasicType[]) invoker.arguments[0];
        Class<?>[] loopLocalStateTypes = Stream.of(loopClauseTypes).
                filter(bt -> bt != BasicType.V_TYPE).map(BasicType::basicTypeClass).toArray(Class<?>[]::new);
        Class<?>[] localTypes = new Class<?>[loopLocalStateTypes.length + 1];
        localTypes[0] = MethodHandleImpl.LoopClauses.class;
        System.arraycopy(loopLocalStateTypes, 0, localTypes, 1, loopLocalStateTypes.length);

        final int clauseDataIndex = extendLocalsMap(localTypes);
        final int firstLoopStateIndex = clauseDataIndex + 1;

        Class<?> returnType = result.function.resolvedHandle().type().returnType();
        MethodType loopType = args.function.resolvedHandle().type()
                .dropParameterTypes(0,1)
                .changeReturnType(returnType);
        MethodType loopHandleType = loopType.insertParameterTypes(0, loopLocalStateTypes);
        MethodType predType = loopHandleType.changeReturnType(boolean.class);
        MethodType finiType = loopHandleType;

        final int nClauses = loopClauseTypes.length;

        // indices to invoker arguments to load method handle arrays
        final int inits = 1;
        final int steps = 2;
        final int preds = 3;
        final int finis = 4;

        Label lLoop = new Label();
        Label lDone = new Label();
        Label lNext;

        // PREINIT:
        emitPushArgument(MethodHandleImpl.LoopClauses.class, invoker.arguments[1]);
        mv.visitFieldInsn(Opcodes.GETFIELD, LOOP_CLAUSES, "clauses", MHARY2);
        emitAstoreInsn(clauseDataIndex);

        // INIT:
        for (int c = 0, state = 0; c < nClauses; ++c) {
            MethodType cInitType = loopType.changeReturnType(loopClauseTypes[c].basicTypeClass());
            emitLoopHandleInvoke(invoker, inits, c, args, false, cInitType, loopLocalStateTypes, clauseDataIndex,
                    firstLoopStateIndex);
            if (cInitType.returnType() != void.class) {
                emitStoreInsn(BasicType.basicType(cInitType.returnType()), firstLoopStateIndex + state);
                ++state;
            }
        }

        // LOOP:
        mv.visitLabel(lLoop);

        for (int c = 0, state = 0; c < nClauses; ++c) {
            lNext = new Label();

            MethodType stepType = loopHandleType.changeReturnType(loopClauseTypes[c].basicTypeClass());
            boolean isVoid = stepType.returnType() == void.class;

            // invoke loop step
            emitLoopHandleInvoke(invoker, steps, c, args, true, stepType, loopLocalStateTypes, clauseDataIndex,
                    firstLoopStateIndex);
            if (!isVoid) {
                emitStoreInsn(BasicType.basicType(stepType.returnType()), firstLoopStateIndex + state);
                ++state;
            }

            // invoke loop predicate
            emitLoopHandleInvoke(invoker, preds, c, args, true, predType, loopLocalStateTypes, clauseDataIndex,
                    firstLoopStateIndex);
            mv.visitJumpInsn(Opcodes.IFNE, lNext);

            // invoke fini
            emitLoopHandleInvoke(invoker, finis, c, args, true, finiType, loopLocalStateTypes, clauseDataIndex,
                    firstLoopStateIndex);
            mv.visitJumpInsn(Opcodes.GOTO, lDone);

            // this is the beginning of the next loop clause
            mv.visitLabel(lNext);
        }

        mv.visitJumpInsn(Opcodes.GOTO, lLoop);

        // DONE:
        mv.visitLabel(lDone);

        return result;
    }

    private int extendLocalsMap(Class<?>[] types) {
        int firstSlot = localsMap.length - 1;
        localsMap = Arrays.copyOf(localsMap, localsMap.length + types.length);
        localClasses = Arrays.copyOf(localClasses, localClasses.length + types.length);
        System.arraycopy(types, 0, localClasses, firstSlot, types.length);
        int index = localsMap[firstSlot - 1] + 1;
        int lastSlots = 0;
        for (int i = 0; i < types.length; ++i) {
            localsMap[firstSlot + i] = index;
            lastSlots = BasicType.basicType(localClasses[firstSlot + i]).basicTypeSlots();
            index += lastSlots;
        }
        localsMap[localsMap.length - 1] = index - lastSlots;
        return firstSlot;
    }

    private void emitLoopHandleInvoke(Name holder, int handles, int clause, Name args, boolean pushLocalState,
                                      MethodType type, Class<?>[] loopLocalStateTypes, int clauseDataSlot,
                                      int firstLoopStateSlot) {
        // load handle for clause
        emitPushClauseArray(clauseDataSlot, handles);
        emitIconstInsn(clause);
        mv.visitInsn(Opcodes.AALOAD);
        // load loop state (preceding the other arguments)
        if (pushLocalState) {
            for (int s = 0; s < loopLocalStateTypes.length; ++s) {
                emitLoadInsn(BasicType.basicType(loopLocalStateTypes[s]), firstLoopStateSlot + s);
            }
        }
        // load loop args (skip 0: method handle)
        emitPushArguments(args, 1);
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, MH, "invokeBasic", type.toMethodDescriptorString(), false);
    }

    private void emitPushClauseArray(int clauseDataSlot, int which) {
        emitAloadInsn(clauseDataSlot);
        emitIconstInsn(which - 1);
        mv.visitInsn(Opcodes.AALOAD);
    }

    private void emitZero(BasicType type) {
        switch (type) {
            case I_TYPE: mv.visitInsn(Opcodes.ICONST_0); break;
            case J_TYPE: mv.visitInsn(Opcodes.LCONST_0); break;
            case F_TYPE: mv.visitInsn(Opcodes.FCONST_0); break;
            case D_TYPE: mv.visitInsn(Opcodes.DCONST_0); break;
            case L_TYPE: mv.visitInsn(Opcodes.ACONST_NULL); break;
            default: throw new InternalError("unknown type: " + type);
        }
    }

    private void emitPushArguments(Name args, int start) {
        MethodType type = args.function.methodType();
        for (int i = start; i < args.arguments.length; i++) {
            emitPushArgument(type.parameterType(i), args.arguments[i]);
        }
    }

    private void emitPushArgument(Name name, int paramIndex) {
        Object arg = name.arguments[paramIndex];
        Class<?> ptype = name.function.methodType().parameterType(paramIndex);
        emitPushArgument(ptype, arg);
    }

    private void emitPushArgument(Class<?> ptype, Object arg) {
        BasicType bptype = basicType(ptype);
        if (arg instanceof Name) {
            Name n = (Name) arg;
            emitLoadInsn(n.type, n.index());
            emitImplicitConversion(n.type, ptype, n);
        } else if ((arg == null || arg instanceof String) && bptype == L_TYPE) {
            emitConst(arg);
        } else {
            if (Wrapper.isWrapperType(arg.getClass()) && bptype != L_TYPE) {
                emitConst(arg);
            } else {
                mv.visitLdcInsn(constantPlaceholder(arg));
                emitImplicitConversion(L_TYPE, ptype, arg);
            }
        }
    }

    /**
     * Store the name to its local, if necessary.
     */
    private void emitStoreResult(Name name) {
        if (name != null && name.type != V_TYPE) {
            // non-void: actually assign
            emitStoreInsn(name.type, name.index());
        }
    }

    /**
     * Emits a return statement from a LF invoker. If required, the result type is cast to the correct return type.
     */
    private void emitReturn(Name onStack) {
        // return statement
        Class<?> rclass = invokerType.returnType();
        BasicType rtype = lambdaForm.returnType();
        assert(rtype == basicType(rclass));  // must agree
        if (rtype == V_TYPE) {
            // void
            mv.visitInsn(Opcodes.RETURN);
            // it doesn't matter what rclass is; the JVM will discard any value
        } else {
            LambdaForm.Name rn = lambdaForm.names[lambdaForm.result];

            // put return value on the stack if it is not already there
            if (rn != onStack) {
                emitLoadInsn(rtype, lambdaForm.result);
            }

            emitImplicitConversion(rtype, rclass, rn);

            // generate actual return statement
            emitReturnInsn(rtype);
        }
    }

    /**
     * Emit a type conversion bytecode casting from "from" to "to".
     */
    private void emitPrimCast(Wrapper from, Wrapper to) {
        // Here's how.
        // -   indicates forbidden
        // <-> indicates implicit
        //      to ----> boolean  byte     short    char     int      long     float    double
        // from boolean    <->        -        -        -        -        -        -        -
        //      byte        -       <->       i2s      i2c      <->      i2l      i2f      i2d
        //      short       -       i2b       <->      i2c      <->      i2l      i2f      i2d
        //      char        -       i2b       i2s      <->      <->      i2l      i2f      i2d
        //      int         -       i2b       i2s      i2c      <->      i2l      i2f      i2d
        //      long        -     l2i,i2b   l2i,i2s  l2i,i2c    l2i      <->      l2f      l2d
        //      float       -     f2i,i2b   f2i,i2s  f2i,i2c    f2i      f2l      <->      f2d
        //      double      -     d2i,i2b   d2i,i2s  d2i,i2c    d2i      d2l      d2f      <->
        if (from == to) {
            // no cast required, should be dead code anyway
            return;
        }
        if (from.isSubwordOrInt()) {
            // cast from {byte,short,char,int} to anything
            emitI2X(to);
        } else {
            // cast from {long,float,double} to anything
            if (to.isSubwordOrInt()) {
                // cast to {byte,short,char,int}
                emitX2I(from);
                if (to.bitWidth() < 32) {
                    // targets other than int require another conversion
                    emitI2X(to);
                }
            } else {
                // cast to {long,float,double} - this is verbose
                boolean error = false;
                switch (from) {
                case LONG:
                    switch (to) {
                    case FLOAT:   mv.visitInsn(Opcodes.L2F);  break;
                    case DOUBLE:  mv.visitInsn(Opcodes.L2D);  break;
                    default:      error = true;               break;
                    }
                    break;
                case FLOAT:
                    switch (to) {
                    case LONG :   mv.visitInsn(Opcodes.F2L);  break;
                    case DOUBLE:  mv.visitInsn(Opcodes.F2D);  break;
                    default:      error = true;               break;
                    }
                    break;
                case DOUBLE:
                    switch (to) {
                    case LONG :   mv.visitInsn(Opcodes.D2L);  break;
                    case FLOAT:   mv.visitInsn(Opcodes.D2F);  break;
                    default:      error = true;               break;
                    }
                    break;
                default:
                    error = true;
                    break;
                }
                if (error) {
                    throw new IllegalStateException("unhandled prim cast: " + from + "2" + to);
                }
            }
        }
    }

    private void emitI2X(Wrapper type) {
        switch (type) {
        case BYTE:    mv.visitInsn(Opcodes.I2B);  break;
        case SHORT:   mv.visitInsn(Opcodes.I2S);  break;
        case CHAR:    mv.visitInsn(Opcodes.I2C);  break;
        case INT:     /* naught */                break;
        case LONG:    mv.visitInsn(Opcodes.I2L);  break;
        case FLOAT:   mv.visitInsn(Opcodes.I2F);  break;
        case DOUBLE:  mv.visitInsn(Opcodes.I2D);  break;
        case BOOLEAN:
            // For compatibility with ValueConversions and explicitCastArguments:
            mv.visitInsn(Opcodes.ICONST_1);
            mv.visitInsn(Opcodes.IAND);
            break;
        default:   throw new InternalError("unknown type: " + type);
        }
    }

    private void emitX2I(Wrapper type) {
        switch (type) {
        case LONG:    mv.visitInsn(Opcodes.L2I);  break;
        case FLOAT:   mv.visitInsn(Opcodes.F2I);  break;
        case DOUBLE:  mv.visitInsn(Opcodes.D2I);  break;
        default:      throw new InternalError("unknown type: " + type);
        }
    }

    /**
     * Generate bytecode for a LambdaForm.vmentry which calls interpretWithArguments.
     */
    static MemberName generateLambdaFormInterpreterEntryPoint(MethodType mt) {
        assert(isValidSignature(basicTypeSignature(mt)));
        String name = "interpret_"+basicTypeChar(mt.returnType());
        MethodType type = mt;  // includes leading argument
        type = type.changeParameterType(0, MethodHandle.class);
        InvokerBytecodeGenerator g = new InvokerBytecodeGenerator("LFI", name, type);
        return g.loadMethod(g.generateLambdaFormInterpreterEntryPointBytes());
    }

    private byte[] generateLambdaFormInterpreterEntryPointBytes() {
        classFilePrologue();
        methodPrologue();

        // Suppress this method in backtraces displayed to the user.
        mv.visitAnnotation(LF_HIDDEN_SIG, true);

        // Don't inline the interpreter entry.
        mv.visitAnnotation(DONTINLINE_SIG, true);

        // create parameter array
        emitIconstInsn(invokerType.parameterCount());
        mv.visitTypeInsn(Opcodes.ANEWARRAY, "java/lang/Object");

        // fill parameter array
        for (int i = 0; i < invokerType.parameterCount(); i++) {
            Class<?> ptype = invokerType.parameterType(i);
            mv.visitInsn(Opcodes.DUP);
            emitIconstInsn(i);
            emitLoadInsn(basicType(ptype), i);
            // box if primitive type
            if (ptype.isPrimitive()) {
                emitBoxing(Wrapper.forPrimitiveType(ptype));
            }
            mv.visitInsn(Opcodes.AASTORE);
        }
        // invoke
        emitAloadInsn(0);
        mv.visitFieldInsn(Opcodes.GETFIELD, MH, "form", "Ljava/lang/invoke/LambdaForm;");
        mv.visitInsn(Opcodes.SWAP);  // swap form and array; avoid local variable
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, LF, "interpretWithArguments", "([Ljava/lang/Object;)Ljava/lang/Object;", false);

        // maybe unbox
        Class<?> rtype = invokerType.returnType();
        if (rtype.isPrimitive() && rtype != void.class) {
            emitUnboxing(Wrapper.forPrimitiveType(rtype));
        }

        // return statement
        emitReturnInsn(basicType(rtype));

        methodEpilogue();
        bogusMethod(invokerType);

        final byte[] classFile = cw.toByteArray();
        maybeDump(classFile);
        return classFile;
    }

    /**
     * Generate bytecode for a NamedFunction invoker.
     */
    static MemberName generateNamedFunctionInvoker(MethodTypeForm typeForm) {
        MethodType invokerType = NamedFunction.INVOKER_METHOD_TYPE;
        String invokerName = "invoke_" + shortenSignature(basicTypeSignature(typeForm.erasedType()));
        InvokerBytecodeGenerator g = new InvokerBytecodeGenerator("NFI", invokerName, invokerType);
        return g.loadMethod(g.generateNamedFunctionInvokerImpl(typeForm));
    }

    private byte[] generateNamedFunctionInvokerImpl(MethodTypeForm typeForm) {
        MethodType dstType = typeForm.erasedType();
        classFilePrologue();
        methodPrologue();

        // Suppress this method in backtraces displayed to the user.
        mv.visitAnnotation(LF_HIDDEN_SIG, true);

        // Force inlining of this invoker method.
        mv.visitAnnotation(FORCEINLINE_SIG, true);

        // Load receiver
        emitAloadInsn(0);

        // Load arguments from array
        for (int i = 0; i < dstType.parameterCount(); i++) {
            emitAloadInsn(1);
            emitIconstInsn(i);
            mv.visitInsn(Opcodes.AALOAD);

            // Maybe unbox
            Class<?> dptype = dstType.parameterType(i);
            if (dptype.isPrimitive()) {
                Wrapper dstWrapper = Wrapper.forBasicType(dptype);
                Wrapper srcWrapper = dstWrapper.isSubwordOrInt() ? Wrapper.INT : dstWrapper;  // narrow subword from int
                emitUnboxing(srcWrapper);
                emitPrimCast(srcWrapper, dstWrapper);
            }
        }

        // Invoke
        String targetDesc = dstType.basicType().toMethodDescriptorString();
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, MH, "invokeBasic", targetDesc, false);

        // Box primitive types
        Class<?> rtype = dstType.returnType();
        if (rtype != void.class && rtype.isPrimitive()) {
            Wrapper srcWrapper = Wrapper.forBasicType(rtype);
            Wrapper dstWrapper = srcWrapper.isSubwordOrInt() ? Wrapper.INT : srcWrapper;  // widen subword to int
            // boolean casts not allowed
            emitPrimCast(srcWrapper, dstWrapper);
            emitBoxing(dstWrapper);
        }

        // If the return type is void we return a null reference.
        if (rtype == void.class) {
            mv.visitInsn(Opcodes.ACONST_NULL);
        }
        emitReturnInsn(L_TYPE);  // NOTE: NamedFunction invokers always return a reference value.

        methodEpilogue();
        bogusMethod(dstType);

        final byte[] classFile = cw.toByteArray();
        maybeDump(classFile);
        return classFile;
    }

    /**
     * Emit a bogus method that just loads some string constants. This is to get the constants into the constant pool
     * for debugging purposes.
     */
    private void bogusMethod(Object os) {
        if (DUMP_CLASS_FILES) {
            mv = cw.visitMethod(Opcodes.ACC_STATIC, "dummy", "()V", null, null);
            mv.visitLdcInsn(os.toString());
            mv.visitInsn(Opcodes.POP);
            mv.visitInsn(Opcodes.RETURN);
            mv.visitMaxs(0, 0);
            mv.visitEnd();
        }
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\invoke\Invokers.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2008, 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.invoke;

import jdk.internal.vm.annotation.DontInline;
import jdk.internal.vm.annotation.ForceInline;
import jdk.internal.vm.annotation.Stable;

import java.lang.reflect.Array;
import java.util.Arrays;

import static java.lang.invoke.MethodHandleStatics.*;
import static java.lang.invoke.MethodHandleNatives.Constants.*;
import static java.lang.invoke.MethodHandles.Lookup.IMPL_LOOKUP;
import static java.lang.invoke.LambdaForm.*;
import static java.lang.invoke.LambdaForm.Kind.*;

/**
 * Construction and caching of often-used invokers.
 * @author jrose
 */
class Invokers {
    // exact type (sans leading target MH) for the outgoing call
    private final MethodType targetType;

    // Cached adapter information:
    private final @Stable MethodHandle[] invokers = new MethodHandle[INV_LIMIT];
    // Indexes into invokers:
    static final int
            INV_EXACT          =  0,  // MethodHandles.exactInvoker
            INV_GENERIC        =  1,  // MethodHandles.invoker (generic invocation)
            INV_BASIC          =  2,  // MethodHandles.basicInvoker
            INV_LIMIT          =  3;

    /** Compute and cache information common to all collecting adapters
     *  that implement members of the erasure-family of the given erased type.
     */
    /*non-public*/ Invokers(MethodType targetType) {
        this.targetType = targetType;
    }

    /*non-public*/ MethodHandle exactInvoker() {
        MethodHandle invoker = cachedInvoker(INV_EXACT);
        if (invoker != null)  return invoker;
        invoker = makeExactOrGeneralInvoker(true);
        return setCachedInvoker(INV_EXACT, invoker);
    }

    /*non-public*/ MethodHandle genericInvoker() {
        MethodHandle invoker = cachedInvoker(INV_GENERIC);
        if (invoker != null)  return invoker;
        invoker = makeExactOrGeneralInvoker(false);
        return setCachedInvoker(INV_GENERIC, invoker);
    }

    /*non-public*/ MethodHandle basicInvoker() {
        MethodHandle invoker = cachedInvoker(INV_BASIC);
        if (invoker != null)  return invoker;
        MethodType basicType = targetType.basicType();
        if (basicType != targetType) {
            // double cache; not used significantly
            return setCachedInvoker(INV_BASIC, basicType.invokers().basicInvoker());
        }
        invoker = basicType.form().cachedMethodHandle(MethodTypeForm.MH_BASIC_INV);
        if (invoker == null) {
            MemberName method = invokeBasicMethod(basicType);
            invoker = DirectMethodHandle.make(method);
            assert(checkInvoker(invoker));
            invoker = basicType.form().setCachedMethodHandle(MethodTypeForm.MH_BASIC_INV, invoker);
        }
        return setCachedInvoker(INV_BASIC, invoker);
    }

    /*non-public*/ MethodHandle varHandleMethodInvoker(VarHandle.AccessMode ak) {
        // TODO cache invoker
        return makeVarHandleMethodInvoker(ak, false);
    }

    /*non-public*/ MethodHandle varHandleMethodExactInvoker(VarHandle.AccessMode ak) {
        // TODO cache invoker
        return makeVarHandleMethodInvoker(ak, true);
    }

    private MethodHandle cachedInvoker(int idx) {
        return invokers[idx];
    }

    private synchronized MethodHandle setCachedInvoker(int idx, final MethodHandle invoker) {
        // Simulate a CAS, to avoid racy duplication of results.
        MethodHandle prev = invokers[idx];
        if (prev != null)  return prev;
        return invokers[idx] = invoker;
    }

    private MethodHandle makeExactOrGeneralInvoker(boolean isExact) {
        MethodType mtype = targetType;
        MethodType invokerType = mtype.invokerType();
        int which = (isExact ? MethodTypeForm.LF_EX_INVOKER : MethodTypeForm.LF_GEN_INVOKER);
        LambdaForm lform = invokeHandleForm(mtype, false, which);
        MethodHandle invoker = BoundMethodHandle.bindSingle(invokerType, lform, mtype);
        String whichName = (isExact ? "invokeExact" : "invoke");
        invoker = invoker.withInternalMemberName(MemberName.makeMethodHandleInvoke(whichName, mtype), false);
        assert(checkInvoker(invoker));
        maybeCompileToBytecode(invoker);
        return invoker;
    }

    private MethodHandle makeVarHandleMethodInvoker(VarHandle.AccessMode ak, boolean isExact) {
        MethodType mtype = targetType;
        MethodType invokerType = mtype.insertParameterTypes(0, VarHandle.class);

        LambdaForm lform = varHandleMethodInvokerHandleForm(ak, mtype, isExact);
        VarHandle.AccessDescriptor ad = new VarHandle.AccessDescriptor(mtype, ak.at.ordinal(), ak.ordinal());
        MethodHandle invoker = BoundMethodHandle.bindSingle(invokerType, lform, ad);

        invoker = invoker.withInternalMemberName(MemberName.makeVarHandleMethodInvoke(ak.methodName(), mtype), false);
        assert(checkVarHandleInvoker(invoker));

        maybeCompileToBytecode(invoker);
        return invoker;
    }

    /** If the target type seems to be common enough, eagerly compile the invoker to bytecodes. */
    private void maybeCompileToBytecode(MethodHandle invoker) {
        final int EAGER_COMPILE_ARITY_LIMIT = 10;
        if (targetType == targetType.erase() &&
            targetType.parameterCount() < EAGER_COMPILE_ARITY_LIMIT) {
            invoker.form.compileToBytecode();
        }
    }

    // This next one is called from LambdaForm.NamedFunction.<init>.
    /*non-public*/ static MemberName invokeBasicMethod(MethodType basicType) {
        assert(basicType == basicType.basicType());
        try {
            //Lookup.findVirtual(MethodHandle.class, name, type);
            return IMPL_LOOKUP.resolveOrFail(REF_invokeVirtual, MethodHandle.class, "invokeBasic", basicType);
        } catch (ReflectiveOperationException ex) {
            throw newInternalError("JVM cannot find invoker for "+basicType, ex);
        }
    }

    private boolean checkInvoker(MethodHandle invoker) {
        assert(targetType.invokerType().equals(invoker.type()))
                : java.util.Arrays.asList(targetType, targetType.invokerType(), invoker);
        assert(invoker.internalMemberName() == null ||
               invoker.internalMemberName().getMethodType().equals(targetType));
        assert(!invoker.isVarargsCollector());
        return true;
    }

    private boolean checkVarHandleInvoker(MethodHandle invoker) {
        MethodType invokerType = targetType.insertParameterTypes(0, VarHandle.class);
        assert(invokerType.equals(invoker.type()))
                : java.util.Arrays.asList(targetType, invokerType, invoker);
        assert(invoker.internalMemberName() == null ||
               invoker.internalMemberName().getMethodType().equals(targetType));
        assert(!invoker.isVarargsCollector());
        return true;
    }

    /**
     * Find or create an invoker which passes unchanged a given number of arguments
     * and spreads the rest from a trailing array argument.
     * The invoker target type is the post-spread type {@code (TYPEOF(uarg*), TYPEOF(sarg*))=>RT}.
     * All the {@code sarg}s must have a common type {@code C}.  (If there are none, {@code Object} is assumed.}
     * @param leadingArgCount the number of unchanged (non-spread) arguments
     * @return {@code invoker.invokeExact(mh, uarg*, C[]{sarg*}) := (RT)mh.invoke(uarg*, sarg*)}
     */
    /*non-public*/ MethodHandle spreadInvoker(int leadingArgCount) {
        int spreadArgCount = targetType.parameterCount() - leadingArgCount;
        MethodType postSpreadType = targetType;
        Class<?> argArrayType = impliedRestargType(postSpreadType, leadingArgCount);
        if (postSpreadType.parameterSlotCount() <= MethodType.MAX_MH_INVOKER_ARITY) {
            return genericInvoker().asSpreader(argArrayType, spreadArgCount);
        }
        // Cannot build a generic invoker here of type ginvoker.invoke(mh, a*[254]).
        // Instead, factor sinvoker.invoke(mh, a) into ainvoker.invoke(filter(mh), a)
        // where filter(mh) == mh.asSpreader(Object[], spreadArgCount)
        MethodType preSpreadType = postSpreadType
            .replaceParameterTypes(leadingArgCount, postSpreadType.parameterCount(), argArrayType);
        MethodHandle arrayInvoker = MethodHandles.invoker(preSpreadType);
        MethodHandle makeSpreader = MethodHandles.insertArguments(Lazy.MH_asSpreader, 1, argArrayType, spreadArgCount);
        return MethodHandles.filterArgument(arrayInvoker, 0, makeSpreader);
    }

    private static Class<?> impliedRestargType(MethodType restargType, int fromPos) {
        if (restargType.isGeneric())  return Object[].class;  // can be nothing else
        int maxPos = restargType.parameterCount();
        if (fromPos >= maxPos)  return Object[].class;  // reasonable default
        Class<?> argType = restargType.parameterType(fromPos);
        for (int i = fromPos+1; i < maxPos; i++) {
            if (argType != restargType.parameterType(i))
                throw newIllegalArgumentException("need homogeneous rest arguments", restargType);
        }
        if (argType == Object.class)  return Object[].class;
        return Array.newInstance(argType, 0).getClass();
    }

    public String toString() {
        return "Invokers"+targetType;
    }

    static MemberName methodHandleInvokeLinkerMethod(String name,
                                                     MethodType mtype,
                                                     Object[] appendixResult) {
        int which;
        switch (name) {
            case "invokeExact":  which = MethodTypeForm.LF_EX_LINKER; break;
            case "invoke":       which = MethodTypeForm.LF_GEN_LINKER; break;
            default:             throw new InternalError("not invoker: "+name);
        }
        LambdaForm lform;
        if (mtype.parameterSlotCount() <= MethodType.MAX_MH_ARITY - MH_LINKER_ARG_APPENDED) {
            lform = invokeHandleForm(mtype, false, which);
            appendixResult[0] = mtype;
        } else {
            lform = invokeHandleForm(mtype, true, which);
        }
        return lform.vmentry;
    }

    // argument count to account for trailing "appendix value" (typically the mtype)
    private static final int MH_LINKER_ARG_APPENDED = 1;

    /** Returns an adapter for invokeExact or generic invoke, as a MH or constant pool linker.
     * If !customized, caller is responsible for supplying, during adapter execution,
     * a copy of the exact mtype.  This is because the adapter might be generalized to
     * a basic type.
     * @param mtype the caller's method type (either basic or full-custom)
     * @param customized whether to use a trailing appendix argument (to carry the mtype)
     * @param which bit-encoded 0x01 whether it is a CP adapter ("linker") or MHs.invoker value ("invoker");
     *                          0x02 whether it is for invokeExact or generic invoke
     */
    static LambdaForm invokeHandleForm(MethodType mtype, boolean customized, int which) {
        boolean isCached;
        if (!customized) {
            mtype = mtype.basicType();  // normalize Z to I, String to Object, etc.
            isCached = true;
        } else {
            isCached = false;  // maybe cache if mtype == mtype.basicType()
        }
        boolean isLinker, isGeneric;
        Kind kind;
        switch (which) {
        case MethodTypeForm.LF_EX_LINKER:   isLinker = true;  isGeneric = false; kind = EXACT_LINKER; break;
        case MethodTypeForm.LF_EX_INVOKER:  isLinker = false; isGeneric = false; kind = EXACT_INVOKER; break;
        case MethodTypeForm.LF_GEN_LINKER:  isLinker = true;  isGeneric = true;  kind = GENERIC_LINKER; break;
        case MethodTypeForm.LF_GEN_INVOKER: isLinker = false; isGeneric = true;  kind = GENERIC_INVOKER; break;
        default: throw new InternalError();
        }
        LambdaForm lform;
        if (isCached) {
            lform = mtype.form().cachedLambdaForm(which);
            if (lform != null)  return lform;
        }
        // exactInvokerForm (Object,Object)Object
        //   link with java.lang.invoke.MethodHandle.invokeBasic(MethodHandle,Object,Object)Object/invokeSpecial
        final int THIS_MH      = 0;
        final int CALL_MH      = THIS_MH + (isLinker ? 0 : 1);
        final int ARG_BASE     = CALL_MH + 1;
        final int OUTARG_LIMIT = ARG_BASE + mtype.parameterCount();
        final int INARG_LIMIT  = OUTARG_LIMIT + (isLinker && !customized ? 1 : 0);
        int nameCursor = OUTARG_LIMIT;
        final int MTYPE_ARG    = customized ? -1 : nameCursor++;  // might be last in-argument
        final int CHECK_TYPE   = nameCursor++;
        final int CHECK_CUSTOM = (CUSTOMIZE_THRESHOLD >= 0) ? nameCursor++ : -1;
        final int LINKER_CALL  = nameCursor++;
        MethodType invokerFormType = mtype.invokerType();
        if (isLinker) {
            if (!customized)
                invokerFormType = invokerFormType.appendParameterTypes(MemberName.class);
        } else {
            invokerFormType = invokerFormType.invokerType();
        }
        Name[] names = arguments(nameCursor - INARG_LIMIT, invokerFormType);
        assert(names.length == nameCursor)
                : Arrays.asList(mtype, customized, which, nameCursor, names.length);
        if (MTYPE_ARG >= INARG_LIMIT) {
            assert(names[MTYPE_ARG] == null);
            BoundMethodHandle.SpeciesData speciesData = BoundMethodHandle.speciesData_L();
            names[THIS_MH] = names[THIS_MH].withConstraint(speciesData);
            NamedFunction getter = speciesData.getterFunction(0);
            names[MTYPE_ARG] = new Name(getter, names[THIS_MH]);
            // else if isLinker, then MTYPE is passed in from the caller (e.g., the JVM)
        }

        // Make the final call.  If isGeneric, then prepend the result of type checking.
        MethodType outCallType = mtype.basicType();
        Object[] outArgs = Arrays.copyOfRange(names, CALL_MH, OUTARG_LIMIT, Object[].class);
        Object mtypeArg = (customized ? mtype : names[MTYPE_ARG]);
        if (!isGeneric) {
            names[CHECK_TYPE] = new Name(getFunction(NF_checkExactType), names[CALL_MH], mtypeArg);
            // mh.invokeExact(a*):R => checkExactType(mh, TYPEOF(a*:R)); mh.invokeBasic(a*)
        } else {
            names[CHECK_TYPE] = new Name(getFunction(NF_checkGenericType), names[CALL_MH], mtypeArg);
            // mh.invokeGeneric(a*):R => checkGenericType(mh, TYPEOF(a*:R)).invokeBasic(a*)
            outArgs[0] = names[CHECK_TYPE];
        }
        if (CHECK_CUSTOM != -1) {
            names[CHECK_CUSTOM] = new Name(getFunction(NF_checkCustomized), outArgs[0]);
        }
        names[LINKER_CALL] = new Name(outCallType, outArgs);
        if (customized) {
            lform = new LambdaForm(INARG_LIMIT, names);
        } else {
            lform = new LambdaForm(INARG_LIMIT, names, kind);
        }
        if (isLinker)
            lform.compileToBytecode();  // JVM needs a real methodOop
        if (isCached)
            lform = mtype.form().setCachedLambdaForm(which, lform);
        return lform;
    }


    static MemberName varHandleInvokeLinkerMethod(VarHandle.AccessMode ak, MethodType mtype) {
        LambdaForm lform;
        if (mtype.parameterSlotCount() <= MethodType.MAX_MH_ARITY - MH_LINKER_ARG_APPENDED) {
            lform = varHandleMethodGenericLinkerHandleForm(ak, mtype);
        } else {
            // TODO
            throw newInternalError("Unsupported parameter slot count " + mtype.parameterSlotCount());
        }
        return lform.vmentry;
    }

    private static LambdaForm varHandleMethodGenericLinkerHandleForm(VarHandle.AccessMode ak,
            MethodType mtype) {
        // TODO Cache form?

        final int THIS_VH      = 0;
        final int ARG_BASE     = THIS_VH + 1;
        final int ARG_LIMIT = ARG_BASE + mtype.parameterCount();
        int nameCursor = ARG_LIMIT;
        final int VAD_ARG      = nameCursor++;
        final int CHECK_TYPE   = nameCursor++;
        final int CHECK_CUSTOM = (CUSTOMIZE_THRESHOLD >= 0) ? nameCursor++ : -1;
        final int LINKER_CALL  = nameCursor++;

        Name[] names = new Name[LINKER_CALL + 1];
        names[THIS_VH] = argument(THIS_VH, BasicType.basicType(Object.class));
        for (int i = 0; i < mtype.parameterCount(); i++) {
            names[ARG_BASE + i] = argument(ARG_BASE + i, BasicType.basicType(mtype.parameterType(i)));
        }
        names[VAD_ARG] = new Name(ARG_LIMIT, BasicType.basicType(Object.class));

        names[CHECK_TYPE] = new Name(getFunction(NF_checkVarHandleGenericType), names[THIS_VH], names[VAD_ARG]);

        Object[] outArgs = new Object[ARG_LIMIT + 1];
        outArgs[0] = names[CHECK_TYPE];
        for (int i = 0; i < ARG_LIMIT; i++) {
            outArgs[i + 1] = names[i];
        }

        if (CHECK_CUSTOM != -1) {
            names[CHECK_CUSTOM] = new Name(getFunction(NF_checkCustomized), outArgs[0]);
        }

        MethodType outCallType = mtype.insertParameterTypes(0, VarHandle.class)
                .basicType();
        names[LINKER_CALL] = new Name(outCallType, outArgs);
        LambdaForm lform = new LambdaForm(ARG_LIMIT + 1, names, VARHANDLE_LINKER);
        if (LambdaForm.debugNames()) {
            String name = ak.methodName() + ":VarHandle_invoke_MT_" +
                    shortenSignature(basicTypeSignature(mtype));
            LambdaForm.associateWithDebugName(lform, name);
        }
        lform.compileToBytecode();
        return lform;
    }

    private static LambdaForm varHandleMethodInvokerHandleForm(VarHandle.AccessMode ak,
            MethodType mtype, boolean isExact) {
        // TODO Cache form?

        final int THIS_MH      = 0;
        final int CALL_VH      = THIS_MH + 1;
        final int ARG_BASE     = CALL_VH + 1;
        final int ARG_LIMIT = ARG_BASE + mtype.parameterCount();
        int nameCursor = ARG_LIMIT;
        final int VAD_ARG      = nameCursor++;
        final int CHECK_TYPE   = nameCursor++;
        final int LINKER_CALL  = nameCursor++;

        Name[] names = new Name[LINKER_CALL + 1];
        names[THIS_MH] = argument(THIS_MH, BasicType.basicType(Object.class));
        names[CALL_VH] = argument(CALL_VH, BasicType.basicType(Object.class));
        for (int i = 0; i < mtype.parameterCount(); i++) {
            names[ARG_BASE + i] = argument(ARG_BASE + i, BasicType.basicType(mtype.parameterType(i)));
        }

        BoundMethodHandle.SpeciesData speciesData = BoundMethodHandle.speciesData_L();
        names[THIS_MH] = names[THIS_MH].withConstraint(speciesData);

        NamedFunction getter = speciesData.getterFunction(0);
        names[VAD_ARG] = new Name(getter, names[THIS_MH]);

        if (isExact) {
            names[CHECK_TYPE] = new Name(getFunction(NF_checkVarHandleExactType), names[CALL_VH], names[VAD_ARG]);
        } else {
            names[CHECK_TYPE] = new Name(getFunction(NF_checkVarHandleGenericType), names[CALL_VH], names[VAD_ARG]);
        }
        Object[] outArgs = new Object[ARG_LIMIT];
        outArgs[0] = names[CHECK_TYPE];
        for (int i = 1; i < ARG_LIMIT; i++) {
            outArgs[i] = names[i];
        }

        MethodType outCallType = mtype.insertParameterTypes(0, VarHandle.class)
                .basicType();
        names[LINKER_CALL] = new Name(outCallType, outArgs);
        Kind kind = isExact ? VARHANDLE_EXACT_INVOKER : VARHANDLE_INVOKER;
        LambdaForm lform = new LambdaForm(ARG_LIMIT, names, kind);
        if (LambdaForm.debugNames()) {
            String name = ak.methodName() +
                    (isExact ? ":VarHandle_exactInvoker_" : ":VarHandle_invoker_") +
                    shortenSignature(basicTypeSignature(mtype));
            LambdaForm.associateWithDebugName(lform, name);
        }
        lform.prepare();
        return lform;
    }

    /*non-public*/ static
    @ForceInline
    MethodHandle checkVarHandleGenericType(VarHandle handle, VarHandle.AccessDescriptor ad) {
        // Test for exact match on invoker types
        // TODO match with erased types and add cast of return value to lambda form
        MethodHandle mh = handle.getMethodHandle(ad.mode);
        if (mh.type() == ad.symbolicMethodTypeInvoker) {
            return mh;
        }
        else {
            return mh.asType(ad.symbolicMethodTypeInvoker);
        }
    }

    /*non-public*/ static
    @ForceInline
    MethodHandle checkVarHandleExactType(VarHandle handle, VarHandle.AccessDescriptor ad) {
        MethodHandle mh = handle.getMethodHandle(ad.mode);
        MethodType mt = mh.type();
        if (mt != ad.symbolicMethodTypeInvoker) {
            throw newWrongMethodTypeException(mt, ad.symbolicMethodTypeInvoker);
        }
        return mh;
    }

    /*non-public*/ static
    WrongMethodTypeException newWrongMethodTypeException(MethodType actual, MethodType expected) {
        // FIXME: merge with JVM logic for throwing WMTE
        return new WrongMethodTypeException("expected "+expected+" but found "+actual);
    }

    /** Static definition of MethodHandle.invokeExact checking code. */
    /*non-public*/ static
    @ForceInline
    void checkExactType(MethodHandle mh, MethodType expected) {
        MethodType actual = mh.type();
        if (actual != expected)
            throw newWrongMethodTypeException(expected, actual);
    }

    /** Static definition of MethodHandle.invokeGeneric checking code.
     * Directly returns the type-adjusted MH to invoke, as follows:
     * {@code (R)MH.invoke(a*) => MH.asType(TYPEOF(a*:R)).invokeBasic(a*)}
     */
    /*non-public*/ static
    @ForceInline
    MethodHandle checkGenericType(MethodHandle mh,  MethodType expected) {
        return mh.asType(expected);
        /* Maybe add more paths here.  Possible optimizations:
         * for (R)MH.invoke(a*),
         * let MT0 = TYPEOF(a*:R), MT1 = MH.type
