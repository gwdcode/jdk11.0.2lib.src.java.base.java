     * the result is the argument closer to positive infinity. If the
     * arguments have the same value, the result is that same
     * value. If either value is NaN, then the result is NaN.  Unlike
     * the numerical comparison operators, this method considers
     * negative zero to be strictly smaller than positive zero. If one
     * argument is positive zero and the other negative zero, the
     * result is positive zero.
     *
     * @param   a   an argument.
     * @param   b   another argument.
     * @return  the larger of {@code a} and {@code b}.
     */
    public static float max(float a, float b) {
        if (a != a)
            return a;   // a is NaN
        if ((a == 0.0f) &&
            (b == 0.0f) &&
            (Float.floatToRawIntBits(a) == negativeZeroFloatBits)) {
            // Raw conversion ok since NaN can't map to -0.0.
            return b;
        }
        return (a >= b) ? a : b;
    }

    /**
     * Returns the greater of two {@code double} values.  That
     * is, the result is the argument closer to positive infinity. If
     * the arguments have the same value, the result is that same
     * value. If either value is NaN, then the result is NaN.  Unlike
     * the numerical comparison operators, this method considers
     * negative zero to be strictly smaller than positive zero. If one
     * argument is positive zero and the other negative zero, the
     * result is positive zero.
     *
     * @param   a   an argument.
     * @param   b   another argument.
     * @return  the larger of {@code a} and {@code b}.
     */
    public static double max(double a, double b) {
        if (a != a)
            return a;   // a is NaN
        if ((a == 0.0d) &&
            (b == 0.0d) &&
            (Double.doubleToRawLongBits(a) == negativeZeroDoubleBits)) {
            // Raw conversion ok since NaN can't map to -0.0.
            return b;
        }
        return (a >= b) ? a : b;
    }

    /**
     * Returns the smaller of two {@code int} values. That is,
     * the result the argument closer to the value of
     * {@link Integer#MIN_VALUE}.  If the arguments have the same
     * value, the result is that same value.
     *
     * @param   a   an argument.
     * @param   b   another argument.
     * @return  the smaller of {@code a} and {@code b}.
     */
    @HotSpotIntrinsicCandidate
    public static int min(int a, int b) {
        return (a <= b) ? a : b;
    }

    /**
     * Returns the smaller of two {@code long} values. That is,
     * the result is the argument closer to the value of
     * {@link Long#MIN_VALUE}. If the arguments have the same
     * value, the result is that same value.
     *
     * @param   a   an argument.
     * @param   b   another argument.
     * @return  the smaller of {@code a} and {@code b}.
     */
    public static long min(long a, long b) {
        return (a <= b) ? a : b;
    }

    /**
     * Returns the smaller of two {@code float} values.  That is,
     * the result is the value closer to negative infinity. If the
     * arguments have the same value, the result is that same
     * value. If either value is NaN, then the result is NaN.  Unlike
     * the numerical comparison operators, this method considers
     * negative zero to be strictly smaller than positive zero.  If
     * one argument is positive zero and the other is negative zero,
     * the result is negative zero.
     *
     * @param   a   an argument.
     * @param   b   another argument.
     * @return  the smaller of {@code a} and {@code b}.
     */
    public static float min(float a, float b) {
        if (a != a)
            return a;   // a is NaN
        if ((a == 0.0f) &&
            (b == 0.0f) &&
            (Float.floatToRawIntBits(b) == negativeZeroFloatBits)) {
            // Raw conversion ok since NaN can't map to -0.0.
            return b;
        }
        return (a <= b) ? a : b;
    }

    /**
     * Returns the smaller of two {@code double} values.  That
     * is, the result is the value closer to negative infinity. If the
     * arguments have the same value, the result is that same
     * value. If either value is NaN, then the result is NaN.  Unlike
     * the numerical comparison operators, this method considers
     * negative zero to be strictly smaller than positive zero. If one
     * argument is positive zero and the other is negative zero, the
     * result is negative zero.
     *
     * @param   a   an argument.
     * @param   b   another argument.
     * @return  the smaller of {@code a} and {@code b}.
     */
    public static double min(double a, double b) {
        if (a != a)
            return a;   // a is NaN
        if ((a == 0.0d) &&
            (b == 0.0d) &&
            (Double.doubleToRawLongBits(b) == negativeZeroDoubleBits)) {
            // Raw conversion ok since NaN can't map to -0.0.
            return b;
        }
        return (a <= b) ? a : b;
    }

    /**
     * Returns the fused multiply add of the three arguments; that is,
     * returns the exact product of the first two arguments summed
     * with the third argument and then rounded once to the nearest
     * {@code double}.
     *
     * The rounding is done using the {@linkplain
     * java.math.RoundingMode#HALF_EVEN round to nearest even
     * rounding mode}.
     *
     * In contrast, if {@code a * b + c} is evaluated as a regular
     * floating-point expression, two rounding errors are involved,
     * the first for the multiply operation, the second for the
     * addition operation.
     *
     * <p>Special cases:
     * <ul>
     * <li> If any argument is NaN, the result is NaN.
     *
     * <li> If one of the first two arguments is infinite and the
     * other is zero, the result is NaN.
     *
     * <li> If the exact product of the first two arguments is infinite
     * (in other words, at least one of the arguments is infinite and
     * the other is neither zero nor NaN) and the third argument is an
     * infinity of the opposite sign, the result is NaN.
     *
     * </ul>
     *
     * <p>Note that {@code fma(a, 1.0, c)} returns the same
     * result as ({@code a + c}).  However,
     * {@code fma(a, b, +0.0)} does <em>not</em> always return the
     * same result as ({@code a * b}) since
     * {@code fma(-0.0, +0.0, +0.0)} is {@code +0.0} while
     * ({@code -0.0 * +0.0}) is {@code -0.0}; {@code fma(a, b, -0.0)} is
     * equivalent to ({@code a * b}) however.
     *
     * @apiNote This method corresponds to the fusedMultiplyAdd
     * operation defined in IEEE 754-2008.
     *
     * @param a a value
     * @param b a value
     * @param c a value
     *
     * @return (<i>a</i>&nbsp;&times;&nbsp;<i>b</i>&nbsp;+&nbsp;<i>c</i>)
     * computed, as if with unlimited range and precision, and rounded
     * once to the nearest {@code double} value
     *
     * @since 9
     */
    @HotSpotIntrinsicCandidate
    public static double fma(double a, double b, double c) {
        /*
         * Infinity and NaN arithmetic is not quite the same with two
         * roundings as opposed to just one so the simple expression
         * "a * b + c" cannot always be used to compute the correct
         * result.  With two roundings, the product can overflow and
         * if the addend is infinite, a spurious NaN can be produced
         * if the infinity from the overflow and the infinite addend
         * have opposite signs.
         */

        // First, screen for and handle non-finite input values whose
        // arithmetic is not supported by BigDecimal.
        if (Double.isNaN(a) || Double.isNaN(b) || Double.isNaN(c)) {
            return Double.NaN;
        } else { // All inputs non-NaN
            boolean infiniteA = Double.isInfinite(a);
            boolean infiniteB = Double.isInfinite(b);
            boolean infiniteC = Double.isInfinite(c);
            double result;

            if (infiniteA || infiniteB || infiniteC) {
                if (infiniteA && b == 0.0 ||
                    infiniteB && a == 0.0 ) {
                    return Double.NaN;
                }
                // Store product in a double field to cause an
                // overflow even if non-strictfp evaluation is being
                // used.
                double product = a * b;
                if (Double.isInfinite(product) && !infiniteA && !infiniteB) {
                    // Intermediate overflow; might cause a
                    // spurious NaN if added to infinite c.
                    assert Double.isInfinite(c);
                    return c;
                } else {
                    result = product + c;
                    assert !Double.isFinite(result);
                    return result;
                }
            } else { // All inputs finite
                BigDecimal product = (new BigDecimal(a)).multiply(new BigDecimal(b));
                if (c == 0.0) { // Positive or negative zero
                    // If the product is an exact zero, use a
                    // floating-point expression to compute the sign
                    // of the zero final result. The product is an
                    // exact zero if and only if at least one of a and
                    // b is zero.
                    if (a == 0.0 || b == 0.0) {
                        return a * b + c;
                    } else {
                        // The sign of a zero addend doesn't matter if
                        // the product is nonzero. The sign of a zero
                        // addend is not factored in the result if the
                        // exact product is nonzero but underflows to
                        // zero; see IEEE-754 2008 section 6.3 "The
                        // sign bit".
                        return product.doubleValue();
                    }
                } else {
                    return product.add(new BigDecimal(c)).doubleValue();
                }
            }
        }
    }

    /**
     * Returns the fused multiply add of the three arguments; that is,
     * returns the exact product of the first two arguments summed
     * with the third argument and then rounded once to the nearest
     * {@code float}.
     *
     * The rounding is done using the {@linkplain
     * java.math.RoundingMode#HALF_EVEN round to nearest even
     * rounding mode}.
     *
     * In contrast, if {@code a * b + c} is evaluated as a regular
     * floating-point expression, two rounding errors are involved,
     * the first for the multiply operation, the second for the
     * addition operation.
     *
     * <p>Special cases:
     * <ul>
     * <li> If any argument is NaN, the result is NaN.
     *
     * <li> If one of the first two arguments is infinite and the
     * other is zero, the result is NaN.
     *
     * <li> If the exact product of the first two arguments is infinite
     * (in other words, at least one of the arguments is infinite and
     * the other is neither zero nor NaN) and the third argument is an
     * infinity of the opposite sign, the result is NaN.
     *
     * </ul>
     *
     * <p>Note that {@code fma(a, 1.0f, c)} returns the same
     * result as ({@code a + c}).  However,
     * {@code fma(a, b, +0.0f)} does <em>not</em> always return the
     * same result as ({@code a * b}) since
     * {@code fma(-0.0f, +0.0f, +0.0f)} is {@code +0.0f} while
     * ({@code -0.0f * +0.0f}) is {@code -0.0f}; {@code fma(a, b, -0.0f)} is
     * equivalent to ({@code a * b}) however.
     *
     * @apiNote This method corresponds to the fusedMultiplyAdd
     * operation defined in IEEE 754-2008.
     *
     * @param a a value
     * @param b a value
     * @param c a value
     *
     * @return (<i>a</i>&nbsp;&times;&nbsp;<i>b</i>&nbsp;+&nbsp;<i>c</i>)
     * computed, as if with unlimited range and precision, and rounded
     * once to the nearest {@code float} value
     *
     * @since 9
     */
    @HotSpotIntrinsicCandidate
    public static float fma(float a, float b, float c) {
        /*
         *  Since the double format has more than twice the precision
         *  of the float format, the multiply of a * b is exact in
         *  double. The add of c to the product then incurs one
         *  rounding error. Since the double format moreover has more
         *  than (2p + 2) precision bits compared to the p bits of the
         *  float format, the two roundings of (a * b + c), first to
         *  the double format and then secondarily to the float format,
         *  are equivalent to rounding the intermediate result directly
         *  to the float format.
         *
         * In terms of strictfp vs default-fp concerns related to
         * overflow and underflow, since
         *
         * (Float.MAX_VALUE * Float.MAX_VALUE) << Double.MAX_VALUE
         * (Float.MIN_VALUE * Float.MIN_VALUE) >> Double.MIN_VALUE
         *
         * neither the multiply nor add will overflow or underflow in
         * double. Therefore, it is not necessary for this method to
         * be declared strictfp to have reproducible
         * behavior. However, it is necessary to explicitly store down
         * to a float variable to avoid returning a value in the float
         * extended value set.
         */
        float result = (float)(((double) a * (double) b ) + (double) c);
        return result;
    }

    /**
     * Returns the size of an ulp of the argument.  An ulp, unit in
     * the last place, of a {@code double} value is the positive
     * distance between this floating-point value and the {@code
     * double} value next larger in magnitude.  Note that for non-NaN
     * <i>x</i>, <code>ulp(-<i>x</i>) == ulp(<i>x</i>)</code>.
     *
     * <p>Special Cases:
     * <ul>
     * <li> If the argument is NaN, then the result is NaN.
     * <li> If the argument is positive or negative infinity, then the
     * result is positive infinity.
     * <li> If the argument is positive or negative zero, then the result is
     * {@code Double.MIN_VALUE}.
     * <li> If the argument is &plusmn;{@code Double.MAX_VALUE}, then
     * the result is equal to 2<sup>971</sup>.
     * </ul>
     *
     * @param d the floating-point value whose ulp is to be returned
     * @return the size of an ulp of the argument
     * @author Joseph D. Darcy
     * @since 1.5
     */
    public static double ulp(double d) {
        int exp = getExponent(d);

        switch(exp) {
        case Double.MAX_EXPONENT + 1:       // NaN or infinity
            return Math.abs(d);

        case Double.MIN_EXPONENT - 1:       // zero or subnormal
            return Double.MIN_VALUE;

        default:
            assert exp <= Double.MAX_EXPONENT && exp >= Double.MIN_EXPONENT;

            // ulp(x) is usually 2^(SIGNIFICAND_WIDTH-1)*(2^ilogb(x))
            exp = exp - (DoubleConsts.SIGNIFICAND_WIDTH-1);
            if (exp >= Double.MIN_EXPONENT) {
                return powerOfTwoD(exp);
            }
            else {
                // return a subnormal result; left shift integer
                // representation of Double.MIN_VALUE appropriate
                // number of positions
                return Double.longBitsToDouble(1L <<
                (exp - (Double.MIN_EXPONENT - (DoubleConsts.SIGNIFICAND_WIDTH-1)) ));
            }
        }
    }

    /**
     * Returns the size of an ulp of the argument.  An ulp, unit in
     * the last place, of a {@code float} value is the positive
     * distance between this floating-point value and the {@code
     * float} value next larger in magnitude.  Note that for non-NaN
     * <i>x</i>, <code>ulp(-<i>x</i>) == ulp(<i>x</i>)</code>.
     *
     * <p>Special Cases:
     * <ul>
     * <li> If the argument is NaN, then the result is NaN.
     * <li> If the argument is positive or negative infinity, then the
     * result is positive infinity.
     * <li> If the argument is positive or negative zero, then the result is
     * {@code Float.MIN_VALUE}.
     * <li> If the argument is &plusmn;{@code Float.MAX_VALUE}, then
     * the result is equal to 2<sup>104</sup>.
     * </ul>
     *
     * @param f the floating-point value whose ulp is to be returned
     * @return the size of an ulp of the argument
     * @author Joseph D. Darcy
     * @since 1.5
     */
    public static float ulp(float f) {
        int exp = getExponent(f);

        switch(exp) {
        case Float.MAX_EXPONENT+1:        // NaN or infinity
            return Math.abs(f);

        case Float.MIN_EXPONENT-1:        // zero or subnormal
            return Float.MIN_VALUE;

        default:
            assert exp <= Float.MAX_EXPONENT && exp >= Float.MIN_EXPONENT;

            // ulp(x) is usually 2^(SIGNIFICAND_WIDTH-1)*(2^ilogb(x))
            exp = exp - (FloatConsts.SIGNIFICAND_WIDTH-1);
            if (exp >= Float.MIN_EXPONENT) {
                return powerOfTwoF(exp);
            } else {
                // return a subnormal result; left shift integer
                // representation of FloatConsts.MIN_VALUE appropriate
                // number of positions
                return Float.intBitsToFloat(1 <<
                (exp - (Float.MIN_EXPONENT - (FloatConsts.SIGNIFICAND_WIDTH-1)) ));
            }
        }
    }

    /**
     * Returns the signum function of the argument; zero if the argument
     * is zero, 1.0 if the argument is greater than zero, -1.0 if the
     * argument is less than zero.
     *
     * <p>Special Cases:
     * <ul>
     * <li> If the argument is NaN, then the result is NaN.
     * <li> If the argument is positive zero or negative zero, then the
     *      result is the same as the argument.
     * </ul>
     *
     * @param d the floating-point value whose signum is to be returned
     * @return the signum function of the argument
     * @author Joseph D. Darcy
     * @since 1.5
     */
    public static double signum(double d) {
        return (d == 0.0 || Double.isNaN(d))?d:copySign(1.0, d);
    }

    /**
     * Returns the signum function of the argument; zero if the argument
     * is zero, 1.0f if the argument is greater than zero, -1.0f if the
     * argument is less than zero.
     *
     * <p>Special Cases:
     * <ul>
     * <li> If the argument is NaN, then the result is NaN.
     * <li> If the argument is positive zero or negative zero, then the
     *      result is the same as the argument.
     * </ul>
     *
     * @param f the floating-point value whose signum is to be returned
     * @return the signum function of the argument
     * @author Joseph D. Darcy
     * @since 1.5
     */
    public static float signum(float f) {
        return (f == 0.0f || Float.isNaN(f))?f:copySign(1.0f, f);
    }

    /**
     * Returns the hyperbolic sine of a {@code double} value.
     * The hyperbolic sine of <i>x</i> is defined to be
     * (<i>e<sup>x</sup>&nbsp;-&nbsp;e<sup>-x</sup></i>)/2
     * where <i>e</i> is {@linkplain Math#E Euler's number}.
     *
     * <p>Special cases:
     * <ul>
     *
     * <li>If the argument is NaN, then the result is NaN.
     *
     * <li>If the argument is infinite, then the result is an infinity
     * with the same sign as the argument.
     *
     * <li>If the argument is zero, then the result is a zero with the
     * same sign as the argument.
     *
     * </ul>
     *
     * <p>The computed result must be within 2.5 ulps of the exact result.
     *
     * @param   x The number whose hyperbolic sine is to be returned.
     * @return  The hyperbolic sine of {@code x}.
     * @since 1.5
     */
    public static double sinh(double x) {
        return StrictMath.sinh(x);
    }

    /**
     * Returns the hyperbolic cosine of a {@code double} value.
     * The hyperbolic cosine of <i>x</i> is defined to be
     * (<i>e<sup>x</sup>&nbsp;+&nbsp;e<sup>-x</sup></i>)/2
     * where <i>e</i> is {@linkplain Math#E Euler's number}.
     *
     * <p>Special cases:
     * <ul>
     *
     * <li>If the argument is NaN, then the result is NaN.
     *
     * <li>If the argument is infinite, then the result is positive
     * infinity.
     *
     * <li>If the argument is zero, then the result is {@code 1.0}.
     *
     * </ul>
     *
     * <p>The computed result must be within 2.5 ulps of the exact result.
     *
     * @param   x The number whose hyperbolic cosine is to be returned.
     * @return  The hyperbolic cosine of {@code x}.
     * @since 1.5
     */
    public static double cosh(double x) {
        return StrictMath.cosh(x);
    }

    /**
     * Returns the hyperbolic tangent of a {@code double} value.
     * The hyperbolic tangent of <i>x</i> is defined to be
     * (<i>e<sup>x</sup>&nbsp;-&nbsp;e<sup>-x</sup></i>)/(<i>e<sup>x</sup>&nbsp;+&nbsp;e<sup>-x</sup></i>),
     * in other words, {@linkplain Math#sinh
     * sinh(<i>x</i>)}/{@linkplain Math#cosh cosh(<i>x</i>)}.  Note
     * that the absolute value of the exact tanh is always less than
     * 1.
     *
     * <p>Special cases:
     * <ul>
     *
     * <li>If the argument is NaN, then the result is NaN.
     *
     * <li>If the argument is zero, then the result is a zero with the
     * same sign as the argument.
     *
     * <li>If the argument is positive infinity, then the result is
     * {@code +1.0}.
     *
     * <li>If the argument is negative infinity, then the result is
     * {@code -1.0}.
     *
     * </ul>
     *
     * <p>The computed result must be within 2.5 ulps of the exact result.
     * The result of {@code tanh} for any finite input must have
     * an absolute value less than or equal to 1.  Note that once the
     * exact result of tanh is within 1/2 of an ulp of the limit value
     * of &plusmn;1, correctly signed &plusmn;{@code 1.0} should
     * be returned.
     *
     * @param   x The number whose hyperbolic tangent is to be returned.
     * @return  The hyperbolic tangent of {@code x}.
     * @since 1.5
     */
    public static double tanh(double x) {
        return StrictMath.tanh(x);
    }

    /**
     * Returns sqrt(<i>x</i><sup>2</sup>&nbsp;+<i>y</i><sup>2</sup>)
     * without intermediate overflow or underflow.
     *
     * <p>Special cases:
     * <ul>
     *
     * <li> If either argument is infinite, then the result
     * is positive infinity.
     *
     * <li> If either argument is NaN and neither argument is infinite,
     * then the result is NaN.
     *
     * </ul>
     *
     * <p>The computed result must be within 1 ulp of the exact
     * result.  If one parameter is held constant, the results must be
     * semi-monotonic in the other parameter.
     *
     * @param x a value
     * @param y a value
     * @return sqrt(<i>x</i><sup>2</sup>&nbsp;+<i>y</i><sup>2</sup>)
     * without intermediate overflow or underflow
     * @since 1.5
     */
    public static double hypot(double x, double y) {
        return StrictMath.hypot(x, y);
    }

    /**
     * Returns <i>e</i><sup>x</sup>&nbsp;-1.  Note that for values of
     * <i>x</i> near 0, the exact sum of
     * {@code expm1(x)}&nbsp;+&nbsp;1 is much closer to the true
     * result of <i>e</i><sup>x</sup> than {@code exp(x)}.
     *
     * <p>Special cases:
     * <ul>
     * <li>If the argument is NaN, the result is NaN.
     *
     * <li>If the argument is positive infinity, then the result is
     * positive infinity.
     *
     * <li>If the argument is negative infinity, then the result is
     * -1.0.
     *
     * <li>If the argument is zero, then the result is a zero with the
     * same sign as the argument.
     *
     * </ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.  The result of
     * {@code expm1} for any finite input must be greater than or
     * equal to {@code -1.0}.  Note that once the exact result of
     * <i>e</i><sup>{@code x}</sup>&nbsp;-&nbsp;1 is within 1/2
     * ulp of the limit value -1, {@code -1.0} should be
     * returned.
     *
     * @param   x   the exponent to raise <i>e</i> to in the computation of
     *              <i>e</i><sup>{@code x}</sup>&nbsp;-1.
     * @return  the value <i>e</i><sup>{@code x}</sup>&nbsp;-&nbsp;1.
     * @since 1.5
     */
    public static double expm1(double x) {
        return StrictMath.expm1(x);
    }

    /**
     * Returns the natural logarithm of the sum of the argument and 1.
     * Note that for small values {@code x}, the result of
     * {@code log1p(x)} is much closer to the true result of ln(1
     * + {@code x}) than the floating-point evaluation of
     * {@code log(1.0+x)}.
     *
     * <p>Special cases:
     *
     * <ul>
     *
     * <li>If the argument is NaN or less than -1, then the result is
     * NaN.
     *
     * <li>If the argument is positive infinity, then the result is
     * positive infinity.
     *
     * <li>If the argument is negative one, then the result is
     * negative infinity.
     *
     * <li>If the argument is zero, then the result is a zero with the
     * same sign as the argument.
     *
     * </ul>
     *
     * <p>The computed result must be within 1 ulp of the exact result.
     * Results must be semi-monotonic.
     *
     * @param   x   a value
     * @return the value ln({@code x}&nbsp;+&nbsp;1), the natural
     * log of {@code x}&nbsp;+&nbsp;1
     * @since 1.5
     */
    public static double log1p(double x) {
        return StrictMath.log1p(x);
    }

    /**
     * Returns the first floating-point argument with the sign of the
     * second floating-point argument.  Note that unlike the {@link
     * StrictMath#copySign(double, double) StrictMath.copySign}
     * method, this method does not require NaN {@code sign}
     * arguments to be treated as positive values; implementations are
     * permitted to treat some NaN arguments as positive and other NaN
     * arguments as negative to allow greater performance.
     *
     * @param magnitude  the parameter providing the magnitude of the result
     * @param sign   the parameter providing the sign of the result
     * @return a value with the magnitude of {@code magnitude}
     * and the sign of {@code sign}.
     * @since 1.6
     */
    public static double copySign(double magnitude, double sign) {
        return Double.longBitsToDouble((Double.doubleToRawLongBits(sign) &
                                        (DoubleConsts.SIGN_BIT_MASK)) |
                                       (Double.doubleToRawLongBits(magnitude) &
                                        (DoubleConsts.EXP_BIT_MASK |
                                         DoubleConsts.SIGNIF_BIT_MASK)));
    }

    /**
     * Returns the first floating-point argument with the sign of the
     * second floating-point argument.  Note that unlike the {@link
     * StrictMath#copySign(float, float) StrictMath.copySign}
     * method, this method does not require NaN {@code sign}
     * arguments to be treated as positive values; implementations are
     * permitted to treat some NaN arguments as positive and other NaN
     * arguments as negative to allow greater performance.
     *
     * @param magnitude  the parameter providing the magnitude of the result
     * @param sign   the parameter providing the sign of the result
     * @return a value with the magnitude of {@code magnitude}
     * and the sign of {@code sign}.
     * @since 1.6
     */
    public static float copySign(float magnitude, float sign) {
        return Float.intBitsToFloat((Float.floatToRawIntBits(sign) &
                                     (FloatConsts.SIGN_BIT_MASK)) |
                                    (Float.floatToRawIntBits(magnitude) &
                                     (FloatConsts.EXP_BIT_MASK |
                                      FloatConsts.SIGNIF_BIT_MASK)));
    }

    /**
     * Returns the unbiased exponent used in the representation of a
     * {@code float}.  Special cases:
     *
     * <ul>
     * <li>If the argument is NaN or infinite, then the result is
     * {@link Float#MAX_EXPONENT} + 1.
     * <li>If the argument is zero or subnormal, then the result is
     * {@link Float#MIN_EXPONENT} -1.
     * </ul>
     * @param f a {@code float} value
     * @return the unbiased exponent of the argument
     * @since 1.6
     */
    public static int getExponent(float f) {
        /*
         * Bitwise convert f to integer, mask out exponent bits, shift
         * to the right and then subtract out float's bias adjust to
         * get true exponent value
         */
        return ((Float.floatToRawIntBits(f) & FloatConsts.EXP_BIT_MASK) >>
                (FloatConsts.SIGNIFICAND_WIDTH - 1)) - FloatConsts.EXP_BIAS;
    }

    /**
     * Returns the unbiased exponent used in the representation of a
     * {@code double}.  Special cases:
     *
     * <ul>
     * <li>If the argument is NaN or infinite, then the result is
     * {@link Double#MAX_EXPONENT} + 1.
     * <li>If the argument is zero or subnormal, then the result is
     * {@link Double#MIN_EXPONENT} -1.
     * </ul>
     * @param d a {@code double} value
     * @return the unbiased exponent of the argument
     * @since 1.6
     */
    public static int getExponent(double d) {
        /*
         * Bitwise convert d to long, mask out exponent bits, shift
         * to the right and then subtract out double's bias adjust to
         * get true exponent value.
         */
        return (int)(((Double.doubleToRawLongBits(d) & DoubleConsts.EXP_BIT_MASK) >>
                      (DoubleConsts.SIGNIFICAND_WIDTH - 1)) - DoubleConsts.EXP_BIAS);
    }

    /**
     * Returns the floating-point number adjacent to the first
     * argument in the direction of the second argument.  If both
     * arguments compare as equal the second argument is returned.
     *
     * <p>
     * Special cases:
     * <ul>
     * <li> If either argument is a NaN, then NaN is returned.
     *
     * <li> If both arguments are signed zeros, {@code direction}
     * is returned unchanged (as implied by the requirement of
     * returning the second argument if the arguments compare as
     * equal).
     *
     * <li> If {@code start} is
     * &plusmn;{@link Double#MIN_VALUE} and {@code direction}
     * has a value such that the result should have a smaller
     * magnitude, then a zero with the same sign as {@code start}
     * is returned.
     *
     * <li> If {@code start} is infinite and
     * {@code direction} has a value such that the result should
     * have a smaller magnitude, {@link Double#MAX_VALUE} with the
     * same sign as {@code start} is returned.
     *
     * <li> If {@code start} is equal to &plusmn;
     * {@link Double#MAX_VALUE} and {@code direction} has a
     * value such that the result should have a larger magnitude, an
     * infinity with same sign as {@code start} is returned.
     * </ul>
     *
     * @param start  starting floating-point value
     * @param direction value indicating which of
     * {@code start}'s neighbors or {@code start} should
     * be returned
     * @return The floating-point number adjacent to {@code start} in the
     * direction of {@code direction}.
     * @since 1.6
     */
    public static double nextAfter(double start, double direction) {
        /*
         * The cases:
         *
         * nextAfter(+infinity, 0)  == MAX_VALUE
         * nextAfter(+infinity, +infinity)  == +infinity
         * nextAfter(-infinity, 0)  == -MAX_VALUE
         * nextAfter(-infinity, -infinity)  == -infinity
         *
         * are naturally handled without any additional testing
         */

        /*
         * IEEE 754 floating-point numbers are lexicographically
         * ordered if treated as signed-magnitude integers.
         * Since Java's integers are two's complement,
         * incrementing the two's complement representation of a
         * logically negative floating-point value *decrements*
         * the signed-magnitude representation. Therefore, when
         * the integer representation of a floating-point value
         * is negative, the adjustment to the representation is in
         * the opposite direction from what would initially be expected.
         */

        // Branch to descending case first as it is more costly than ascending
        // case due to start != 0.0d conditional.
        if (start > direction) { // descending
            if (start != 0.0d) {
                final long transducer = Double.doubleToRawLongBits(start);
                return Double.longBitsToDouble(transducer + ((transducer > 0L) ? -1L : 1L));
            } else { // start == 0.0d && direction < 0.0d
                return -Double.MIN_VALUE;
            }
        } else if (start < direction) { // ascending
            // Add +0.0 to get rid of a -0.0 (+0.0 + -0.0 => +0.0)
            // then bitwise convert start to integer.
            final long transducer = Double.doubleToRawLongBits(start + 0.0d);
            return Double.longBitsToDouble(transducer + ((transducer >= 0L) ? 1L : -1L));
        } else if (start == direction) {
            return direction;
        } else { // isNaN(start) || isNaN(direction)
            return start + direction;
        }
    }

    /**
     * Returns the floating-point number adjacent to the first
     * argument in the direction of the second argument.  If both
     * arguments compare as equal a value equivalent to the second argument
     * is returned.
     *
     * <p>
     * Special cases:
     * <ul>
     * <li> If either argument is a NaN, then NaN is returned.
     *
     * <li> If both arguments are signed zeros, a value equivalent
     * to {@code direction} is returned.
     *
     * <li> If {@code start} is
     * &plusmn;{@link Float#MIN_VALUE} and {@code direction}
     * has a value such that the result should have a smaller
     * magnitude, then a zero with the same sign as {@code start}
     * is returned.
     *
     * <li> If {@code start} is infinite and
     * {@code direction} has a value such that the result should
     * have a smaller magnitude, {@link Float#MAX_VALUE} with the
     * same sign as {@code start} is returned.
     *
     * <li> If {@code start} is equal to &plusmn;
     * {@link Float#MAX_VALUE} and {@code direction} has a
     * value such that the result should have a larger magnitude, an
     * infinity with same sign as {@code start} is returned.
     * </ul>
     *
     * @param start  starting floating-point value
     * @param direction value indicating which of
     * {@code start}'s neighbors or {@code start} should
     * be returned
     * @return The floating-point number adjacent to {@code start} in the
     * direction of {@code direction}.
     * @since 1.6
     */
    public static float nextAfter(float start, double direction) {
        /*
         * The cases:
         *
         * nextAfter(+infinity, 0)  == MAX_VALUE
         * nextAfter(+infinity, +infinity)  == +infinity
         * nextAfter(-infinity, 0)  == -MAX_VALUE
         * nextAfter(-infinity, -infinity)  == -infinity
         *
         * are naturally handled without any additional testing
         */

        /*
         * IEEE 754 floating-point numbers are lexicographically
         * ordered if treated as signed-magnitude integers.
         * Since Java's integers are two's complement,
         * incrementing the two's complement representation of a
         * logically negative floating-point value *decrements*
         * the signed-magnitude representation. Therefore, when
         * the integer representation of a floating-point value
         * is negative, the adjustment to the representation is in
         * the opposite direction from what would initially be expected.
         */

        // Branch to descending case first as it is more costly than ascending
        // case due to start != 0.0f conditional.
        if (start > direction) { // descending
            if (start != 0.0f) {
                final int transducer = Float.floatToRawIntBits(start);
                return Float.intBitsToFloat(transducer + ((transducer > 0) ? -1 : 1));
            } else { // start == 0.0f && direction < 0.0f
                return -Float.MIN_VALUE;
            }
        } else if (start < direction) { // ascending
            // Add +0.0 to get rid of a -0.0 (+0.0 + -0.0 => +0.0)
            // then bitwise convert start to integer.
            final int transducer = Float.floatToRawIntBits(start + 0.0f);
            return Float.intBitsToFloat(transducer + ((transducer >= 0) ? 1 : -1));
        } else if (start == direction) {
            return (float)direction;
        } else { // isNaN(start) || isNaN(direction)
            return start + (float)direction;
        }
    }

    /**
     * Returns the floating-point value adjacent to {@code d} in
     * the direction of positive infinity.  This method is
     * semantically equivalent to {@code nextAfter(d,
     * Double.POSITIVE_INFINITY)}; however, a {@code nextUp}
     * implementation may run faster than its equivalent
     * {@code nextAfter} call.
     *
     * <p>Special Cases:
     * <ul>
     * <li> If the argument is NaN, the result is NaN.
     *
     * <li> If the argument is positive infinity, the result is
     * positive infinity.
     *
     * <li> If the argument is zero, the result is
     * {@link Double#MIN_VALUE}
     *
     * </ul>
     *
     * @param d starting floating-point value
     * @return The adjacent floating-point value closer to positive
     * infinity.
     * @since 1.6
     */
    public static double nextUp(double d) {
        // Use a single conditional and handle the likely cases first.
        if (d < Double.POSITIVE_INFINITY) {
            // Add +0.0 to get rid of a -0.0 (+0.0 + -0.0 => +0.0).
            final long transducer = Double.doubleToRawLongBits(d + 0.0D);
            return Double.longBitsToDouble(transducer + ((transducer >= 0L) ? 1L : -1L));
        } else { // d is NaN or +Infinity
            return d;
        }
    }

    /**
     * Returns the floating-point value adjacent to {@code f} in
     * the direction of positive infinity.  This method is
     * semantically equivalent to {@code nextAfter(f,
     * Float.POSITIVE_INFINITY)}; however, a {@code nextUp}
     * implementation may run faster than its equivalent
     * {@code nextAfter} call.
     *
     * <p>Special Cases:
     * <ul>
     * <li> If the argument is NaN, the result is NaN.
     *
     * <li> If the argument is positive infinity, the result is
     * positive infinity.
     *
     * <li> If the argument is zero, the result is
     * {@link Float#MIN_VALUE}
     *
     * </ul>
     *
     * @param f starting floating-point value
     * @return The adjacent floating-point value closer to positive
     * infinity.
     * @since 1.6
     */
    public static float nextUp(float f) {
        // Use a single conditional and handle the likely cases first.
        if (f < Float.POSITIVE_INFINITY) {
            // Add +0.0 to get rid of a -0.0 (+0.0 + -0.0 => +0.0).
            final int transducer = Float.floatToRawIntBits(f + 0.0F);
            return Float.intBitsToFloat(transducer + ((transducer >= 0) ? 1 : -1));
        } else { // f is NaN or +Infinity
            return f;
        }
    }

    /**
     * Returns the floating-point value adjacent to {@code d} in
     * the direction of negative infinity.  This method is
     * semantically equivalent to {@code nextAfter(d,
     * Double.NEGATIVE_INFINITY)}; however, a
     * {@code nextDown} implementation may run faster than its
     * equivalent {@code nextAfter} call.
     *
     * <p>Special Cases:
     * <ul>
     * <li> If the argument is NaN, the result is NaN.
     *
     * <li> If the argument is negative infinity, the result is
     * negative infinity.
     *
     * <li> If the argument is zero, the result is
     * {@code -Double.MIN_VALUE}
     *
     * </ul>
     *
     * @param d  starting floating-point value
     * @return The adjacent floating-point value closer to negative
     * infinity.
     * @since 1.8
     */
    public static double nextDown(double d) {
        if (Double.isNaN(d) || d == Double.NEGATIVE_INFINITY)
            return d;
        else {
            if (d == 0.0)
                return -Double.MIN_VALUE;
            else
                return Double.longBitsToDouble(Double.doubleToRawLongBits(d) +
                                               ((d > 0.0d)?-1L:+1L));
        }
    }

    /**
     * Returns the floating-point value adjacent to {@code f} in
     * the direction of negative infinity.  This method is
     * semantically equivalent to {@code nextAfter(f,
     * Float.NEGATIVE_INFINITY)}; however, a
     * {@code nextDown} implementation may run faster than its
     * equivalent {@code nextAfter} call.
     *
     * <p>Special Cases:
     * <ul>
     * <li> If the argument is NaN, the result is NaN.
     *
     * <li> If the argument is negative infinity, the result is
     * negative infinity.
     *
     * <li> If the argument is zero, the result is
     * {@code -Float.MIN_VALUE}
     *
     * </ul>
     *
     * @param f  starting floating-point value
     * @return The adjacent floating-point value closer to negative
     * infinity.
     * @since 1.8
     */
    public static float nextDown(float f) {
        if (Float.isNaN(f) || f == Float.NEGATIVE_INFINITY)
            return f;
        else {
            if (f == 0.0f)
                return -Float.MIN_VALUE;
            else
                return Float.intBitsToFloat(Float.floatToRawIntBits(f) +
                                            ((f > 0.0f)?-1:+1));
        }
    }

    /**
     * Returns {@code d} &times;
     * 2<sup>{@code scaleFactor}</sup> rounded as if performed
     * by a single correctly rounded floating-point multiply to a
     * member of the double value set.  See the Java
     * Language Specification for a discussion of floating-point
     * value sets.  If the exponent of the result is between {@link
     * Double#MIN_EXPONENT} and {@link Double#MAX_EXPONENT}, the
     * answer is calculated exactly.  If the exponent of the result
     * would be larger than {@code Double.MAX_EXPONENT}, an
     * infinity is returned.  Note that if the result is subnormal,
     * precision may be lost; that is, when {@code scalb(x, n)}
     * is subnormal, {@code scalb(scalb(x, n), -n)} may not equal
     * <i>x</i>.  When the result is non-NaN, the result has the same
     * sign as {@code d}.
     *
     * <p>Special cases:
     * <ul>
     * <li> If the first argument is NaN, NaN is returned.
     * <li> If the first argument is infinite, then an infinity of the
     * same sign is returned.
     * <li> If the first argument is zero, then a zero of the same
     * sign is returned.
     * </ul>
     *
     * @param d number to be scaled by a power of two.
     * @param scaleFactor power of 2 used to scale {@code d}
     * @return {@code d} &times; 2<sup>{@code scaleFactor}</sup>
     * @since 1.6
     */
    public static double scalb(double d, int scaleFactor) {
        /*
         * This method does not need to be declared strictfp to
         * compute the same correct result on all platforms.  When
         * scaling up, it does not matter what order the
         * multiply-store operations are done; the result will be
         * finite or overflow regardless of the operation ordering.
         * However, to get the correct result when scaling down, a
         * particular ordering must be used.
         *
         * When scaling down, the multiply-store operations are
         * sequenced so that it is not possible for two consecutive
         * multiply-stores to return subnormal results.  If one
         * multiply-store result is subnormal, the next multiply will
         * round it away to zero.  This is done by first multiplying
         * by 2 ^ (scaleFactor % n) and then multiplying several
         * times by 2^n as needed where n is the exponent of number
         * that is a covenient power of two.  In this way, at most one
         * real rounding error occurs.  If the double value set is
         * being used exclusively, the rounding will occur on a
         * multiply.  If the double-extended-exponent value set is
         * being used, the products will (perhaps) be exact but the
         * stores to d are guaranteed to round to the double value
         * set.
         *
         * It is _not_ a valid implementation to first multiply d by
         * 2^MIN_EXPONENT and then by 2 ^ (scaleFactor %
         * MIN_EXPONENT) since even in a strictfp program double
         * rounding on underflow could occur; e.g. if the scaleFactor
         * argument was (MIN_EXPONENT - n) and the exponent of d was a
         * little less than -(MIN_EXPONENT - n), meaning the final
         * result would be subnormal.
         *
         * Since exact reproducibility of this method can be achieved
         * without any undue performance burden, there is no
         * compelling reason to allow double rounding on underflow in
         * scalb.
         */

        // magnitude of a power of two so large that scaling a finite
        // nonzero value by it would be guaranteed to over or
        // underflow; due to rounding, scaling down takes an
        // additional power of two which is reflected here
        final int MAX_SCALE = Double.MAX_EXPONENT + -Double.MIN_EXPONENT +
                              DoubleConsts.SIGNIFICAND_WIDTH + 1;
        int exp_adjust = 0;
        int scale_increment = 0;
        double exp_delta = Double.NaN;

        // Make sure scaling factor is in a reasonable range

        if(scaleFactor < 0) {
            scaleFactor = Math.max(scaleFactor, -MAX_SCALE);
            scale_increment = -512;
            exp_delta = twoToTheDoubleScaleDown;
        }
        else {
            scaleFactor = Math.min(scaleFactor, MAX_SCALE);
            scale_increment = 512;
            exp_delta = twoToTheDoubleScaleUp;
        }

        // Calculate (scaleFactor % +/-512), 512 = 2^9, using
        // technique from "Hacker's Delight" section 10-2.
        int t = (scaleFactor >> 9-1) >>> 32 - 9;
        exp_adjust = ((scaleFactor + t) & (512 -1)) - t;

        d *= powerOfTwoD(exp_adjust);
        scaleFactor -= exp_adjust;

        while(scaleFactor != 0) {
            d *= exp_delta;
            scaleFactor -= scale_increment;
        }
        return d;
    }

    /**
     * Returns {@code f} &times;
     * 2<sup>{@code scaleFactor}</sup> rounded as if performed
     * by a single correctly rounded floating-point multiply to a
     * member of the float value set.  See the Java
     * Language Specification for a discussion of floating-point
     * value sets.  If the exponent of the result is between {@link
     * Float#MIN_EXPONENT} and {@link Float#MAX_EXPONENT}, the
     * answer is calculated exactly.  If the exponent of the result
     * would be larger than {@code Float.MAX_EXPONENT}, an
     * infinity is returned.  Note that if the result is subnormal,
     * precision may be lost; that is, when {@code scalb(x, n)}
     * is subnormal, {@code scalb(scalb(x, n), -n)} may not equal
     * <i>x</i>.  When the result is non-NaN, the result has the same
     * sign as {@code f}.
     *
     * <p>Special cases:
     * <ul>
     * <li> If the first argument is NaN, NaN is returned.
     * <li> If the first argument is infinite, then an infinity of the
     * same sign is returned.
     * <li> If the first argument is zero, then a zero of the same
     * sign is returned.
     * </ul>
     *
     * @param f number to be scaled by a power of two.
     * @param scaleFactor power of 2 used to scale {@code f}
     * @return {@code f} &times; 2<sup>{@code scaleFactor}</sup>
     * @since 1.6
     */
    public static float scalb(float f, int scaleFactor) {
        // magnitude of a power of two so large that scaling a finite
        // nonzero value by it would be guaranteed to over or
        // underflow; due to rounding, scaling down takes an
        // additional power of two which is reflected here
        final int MAX_SCALE = Float.MAX_EXPONENT + -Float.MIN_EXPONENT +
                              FloatConsts.SIGNIFICAND_WIDTH + 1;

        // Make sure scaling factor is in a reasonable range
        scaleFactor = Math.max(Math.min(scaleFactor, MAX_SCALE), -MAX_SCALE);

        /*
         * Since + MAX_SCALE for float fits well within the double
         * exponent range and + float -> double conversion is exact
         * the multiplication below will be exact. Therefore, the
         * rounding that occurs when the double product is cast to
         * float will be the correctly rounded float result.  Since
         * all operations other than the final multiply will be exact,
         * it is not necessary to declare this method strictfp.
         */
        return (float)((double)f*powerOfTwoD(scaleFactor));
    }

    // Constants used in scalb
    static double twoToTheDoubleScaleUp = powerOfTwoD(512);
    static double twoToTheDoubleScaleDown = powerOfTwoD(-512);

    /**
     * Returns a floating-point power of two in the normal range.
     */
    static double powerOfTwoD(int n) {
        assert(n >= Double.MIN_EXPONENT && n <= Double.MAX_EXPONENT);
        return Double.longBitsToDouble((((long)n + (long)DoubleConsts.EXP_BIAS) <<
                                        (DoubleConsts.SIGNIFICAND_WIDTH-1))
                                       & DoubleConsts.EXP_BIT_MASK);
    }

    /**
     * Returns a floating-point power of two in the normal range.
     */
    static float powerOfTwoF(int n) {
        assert(n >= Float.MIN_EXPONENT && n <= Float.MAX_EXPONENT);
        return Float.intBitsToFloat(((n + FloatConsts.EXP_BIAS) <<
                                     (FloatConsts.SIGNIFICAND_WIDTH-1))
                                    & FloatConsts.EXP_BIT_MASK);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\Configuration.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.module;

import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jdk.internal.module.ModuleReferenceImpl;
import jdk.internal.module.ModuleTarget;

/**
 * A configuration that is the result of <a href="package-summary.html#resolution">
 * resolution</a> or resolution with
 * <a href="{@docRoot}/java.base/java/lang/module/Configuration.html#service-binding">service binding</a>.
 *
 * <p> A configuration encapsulates the <em>readability graph</em> that is the
 * output of resolution. A readability graph is a directed graph whose vertices
 * are of type {@link ResolvedModule} and the edges represent the readability
 * amongst the modules. {@code Configuration} defines the {@link #modules()
 * modules()} method to get the set of resolved modules in the graph. {@code
 * ResolvedModule} defines the {@link ResolvedModule#reads() reads()} method to
 * get the set of modules that a resolved module reads. The modules that are
 * read may be in the same configuration or may be in {@link #parents() parent}
 * configurations. </p>
 *
 * <p> Configuration defines the {@link #resolve(ModuleFinder,List,ModuleFinder,Collection)
 * resolve} method to resolve a collection of root modules, and the {@link
 * #resolveAndBind(ModuleFinder,List,ModuleFinder,Collection) resolveAndBind}
 * method to do resolution with service binding. There are instance and
 * static variants of both methods. The instance methods create a configuration
 * with the receiver as the parent configuration. The static methods are for
 * more advanced cases where there can be more than one parent configuration. </p>
 *
 * <p> Each {@link java.lang.ModuleLayer layer} of modules in the Java virtual
 * machine is created from a configuration. The configuration for the {@link
 * java.lang.ModuleLayer#boot() boot} layer is obtained by invoking {@code
 * ModuleLayer.boot().configuration()}. The configuration for the boot layer
 * will often be the parent when creating new configurations. </p>
 *
 * <h3> Example </h3>
 *
 * <p> The following example uses the {@link
 * #resolve(ModuleFinder,ModuleFinder,Collection) resolve} method to resolve a
 * module named <em>myapp</em> with the configuration for the boot layer as the
 * parent configuration. It prints the name of each resolved module and the
 * names of the modules that each module reads. </p>
 *
 * <pre>{@code
 *    ModuleFinder finder = ModuleFinder.of(dir1, dir2, dir3);
 *
 *    Configuration parent = ModuleLayer.boot().configuration();
 *
 *    Configuration cf = parent.resolve(finder, ModuleFinder.of(), Set.of("myapp"));
 *    cf.modules().forEach(m -> {
 *        System.out.format("%s -> %s%n",
 *            m.name(),
 *            m.reads().stream()
 *                .map(ResolvedModule::name)
 *                .collect(Collectors.joining(", ")));
 *    });
 * }</pre>
 *
 * @since 9
 * @spec JPMS
 * @see java.lang.ModuleLayer
 */
public final class Configuration {

    // @see Configuration#empty()
    private static final Configuration EMPTY_CONFIGURATION = new Configuration();

    // parent configurations, in search order
    private final List<Configuration> parents;

    private final Map<ResolvedModule, Set<ResolvedModule>> graph;
    private final Set<ResolvedModule> modules;
    private final Map<String, ResolvedModule> nameToModule;

    // constraint on target platform
    private final String targetPlatform;

    String targetPlatform() { return targetPlatform; }

    private Configuration() {
        this.parents = Collections.emptyList();
        this.graph = Collections.emptyMap();
        this.modules = Collections.emptySet();
        this.nameToModule = Collections.emptyMap();
        this.targetPlatform = null;
    }

    private Configuration(List<Configuration> parents, Resolver resolver) {
        Map<ResolvedModule, Set<ResolvedModule>> g = resolver.finish(this);

        @SuppressWarnings(value = {"rawtypes", "unchecked"})
        Entry<String, ResolvedModule>[] nameEntries
            = (Entry<String, ResolvedModule>[])new Entry[g.size()];
        ResolvedModule[] moduleArray = new ResolvedModule[g.size()];
        int i = 0;
        for (ResolvedModule resolvedModule : g.keySet()) {
            moduleArray[i] = resolvedModule;
            nameEntries[i] = Map.entry(resolvedModule.name(), resolvedModule);
            i++;
        }

        this.parents = Collections.unmodifiableList(parents);
        this.graph = g;
        this.modules = Set.of(moduleArray);
        this.nameToModule = Map.ofEntries(nameEntries);

        this.targetPlatform = resolver.targetPlatform();
    }

    /**
     * Creates the Configuration for the boot layer from a pre-generated
     * readability graph.
     *
     * @apiNote This method is coded for startup performance.
     */
    Configuration(ModuleFinder finder, Map<String, Set<String>> map) {
        int moduleCount = map.size();

        // create map of name -> ResolvedModule
        @SuppressWarnings(value = {"rawtypes", "unchecked"})
        Entry<String, ResolvedModule>[] nameEntries
            = (Entry<String, ResolvedModule>[])new Entry[moduleCount];
        ResolvedModule[] moduleArray = new ResolvedModule[moduleCount];
        String targetPlatform = null;
        int i = 0;
        for (String name : map.keySet()) {
            ModuleReference mref = finder.find(name).orElse(null);
            assert mref != null;

            if (targetPlatform == null && mref instanceof ModuleReferenceImpl) {
                ModuleTarget target = ((ModuleReferenceImpl)mref).moduleTarget();
                if (target != null) {
                    targetPlatform = target.targetPlatform();
                }
            }

            ResolvedModule resolvedModule = new ResolvedModule(this, mref);
            moduleArray[i] = resolvedModule;
            nameEntries[i] = Map.entry(name, resolvedModule);
            i++;
        }
        Map<String, ResolvedModule> nameToModule = Map.ofEntries(nameEntries);

        // create entries for readability graph
        @SuppressWarnings(value = {"rawtypes", "unchecked"})
        Entry<ResolvedModule, Set<ResolvedModule>>[] moduleEntries
            = (Entry<ResolvedModule, Set<ResolvedModule>>[])new Entry[moduleCount];
        i = 0;
        for (ResolvedModule resolvedModule : moduleArray) {
            Set<String> names = map.get(resolvedModule.name());
            ResolvedModule[] readsArray = new ResolvedModule[names.size()];
            int j = 0;
            for (String name : names) {
                readsArray[j++] = nameToModule.get(name);
            }
            moduleEntries[i++] = Map.entry(resolvedModule, Set.of(readsArray));
        }

        this.parents = List.of(empty());
        this.graph = Map.ofEntries(moduleEntries);
        this.modules = Set.of(moduleArray);
        this.nameToModule = nameToModule;
        this.targetPlatform = targetPlatform;
    }

    /**
     * Resolves a collection of root modules, with this configuration as its
     * parent, to create a new configuration. This method works exactly as
     * specified by the static {@link
     * #resolve(ModuleFinder,List,ModuleFinder,Collection) resolve}
     * method when invoked with this configuration as the parent. In other words,
     * if this configuration is {@code cf} then this method is equivalent to
     * invoking:
     * <pre> {@code
     *     Configuration.resolve(before, List.of(cf), after, roots);
     * }</pre>
     *
     * @param  before
     *         The <em>before</em> module finder to find modules
     * @param  after
     *         The <em>after</em> module finder to locate modules when not
     *         located by the {@code before} module finder or in parent
     *         configurations
     * @param  roots
     *         The possibly-empty collection of module names of the modules
     *         to resolve
     *
     * @return The configuration that is the result of resolving the given
     *         root modules
     *
     * @throws FindException
     *         If resolution fails for any of the observability-related reasons
     *         specified by the static {@code resolve} method
     * @throws ResolutionException
     *         If resolution fails any of the consistency checks specified by
     *         the static {@code resolve} method
     * @throws SecurityException
     *         If locating a module is denied by the security manager
     */
    public Configuration resolve(ModuleFinder before,
                                 ModuleFinder after,
                                 Collection<String> roots)
    {
        return resolve(before, List.of(this), after, roots);
    }


    /**
     * Resolves a collection of root modules, with service binding, and with
     * this configuration as its parent, to create a new configuration.
     * This method works exactly as specified by the static {@link
     * #resolveAndBind(ModuleFinder,List,ModuleFinder,Collection)
     * resolveAndBind} method when invoked with this configuration
     * as the parent. In other words, if this configuration is {@code cf} then
     * this method is equivalent to invoking:
     * <pre> {@code
     *     Configuration.resolveAndBind(before, List.of(cf), after, roots);
     * }</pre>
     *
     *
     * @param  before
     *         The <em>before</em> module finder to find modules
     * @param  after
     *         The <em>after</em> module finder to locate modules when not
     *         located by the {@code before} module finder or in parent
     *         configurations
     * @param  roots
     *         The possibly-empty collection of module names of the modules
     *         to resolve
     *
     * @return The configuration that is the result of resolving, with service
     *         binding, the given root modules
     *
     * @throws FindException
     *         If resolution fails for any of the observability-related reasons
     *         specified by the static {@code resolve} method
     * @throws ResolutionException
     *         If resolution fails any of the consistency checks specified by
     *         the static {@code resolve} method
     * @throws SecurityException
     *         If locating a module is denied by the security manager
     */
    public Configuration resolveAndBind(ModuleFinder before,
                                        ModuleFinder after,
                                        Collection<String> roots)
    {
        return resolveAndBind(before, List.of(this), after, roots);
    }


    /**
     * Resolves a collection of root modules, with service binding, and with
     * the empty configuration as its parent.
     *
     * This method is used to create the configuration for the boot layer.
     */
    static Configuration resolveAndBind(ModuleFinder finder,
                                        Collection<String> roots,
                                        PrintStream traceOutput)
    {
        List<Configuration> parents = List.of(empty());
        Resolver resolver = new Resolver(finder, parents, ModuleFinder.of(), traceOutput);
        resolver.resolve(roots).bind();
        return new Configuration(parents, resolver);
    }

    /**
     * Resolves a collection of root modules to create a configuration.
     *
     * <p> Each root module is located using the given {@code before} module
     * finder. If a module is not found then it is located in the parent
     * configuration as if by invoking the {@link #findModule(String)
     * findModule} method on each parent in iteration order. If not found then
     * the module is located using the given {@code after} module finder. The
     * same search order is used to locate transitive dependences. Root modules
     * or dependences that are located in a parent configuration are resolved
     * no further and are not included in the resulting configuration. </p>
     *
     * <p> When all modules have been enumerated then a readability graph
     * is computed, and in conjunction with the module exports and service use,
     * checked for consistency. </p>
     *
     * <p> Resolution may fail with {@code FindException} for the following
     * <em>observability-related</em> reasons: </p>
     *
     * <ul>
     *
     *     <li><p> A root module, or a direct or transitive dependency, is not
     *     found. </p></li>
     *
     *     <li><p> An error occurs when attempting to find a module.
     *     Possible errors include I/O errors, errors detected parsing a module
     *     descriptor ({@code module-info.class}) or two versions of the same
     *     module are found in the same directory. </p></li>
     *
     * </ul>
     *
     * <p> Resolution may fail with {@code ResolutionException} if any of the
     * following consistency checks fail: </p>
     *
     * <ul>
     *
     *     <li><p> A cycle is detected, say where module {@code m1} requires
     *     module {@code m2} and {@code m2} requires {@code m1}. </p></li>
     *
     *     <li><p> A module reads two or more modules with the same name. This
     *     includes the case where a module reads another with the same name as
     *     itself. </p></li>
     *
     *     <li><p> Two or more modules in the configuration export the same
     *     package to a module that reads both. This includes the case where a
     *     module {@code M} containing package {@code p} reads another module
     *     that exports {@code p} to {@code M}. </p></li>
     *
     *     <li><p> A module {@code M} declares that it "{@code uses p.S}" or
     *     "{@code provides p.S with ...}" but package {@code p} is neither in
     *     module {@code M} nor exported to {@code M} by any module that
     *     {@code M} reads. </p></li>
     *
     * </ul>
     *
     * @implNote In the implementation then observability of modules may depend
     * on referential integrity or other checks that ensure different builds of
     * tightly coupled modules or modules for specific operating systems or
     * architectures are not combined in the same configuration.
     *
     * @param  before
     *         The <em>before</em> module finder to find modules
     * @param  parents
     *         The list parent configurations in search order
     * @param  after
     *         The <em>after</em> module finder to locate modules when not
     *         located by the {@code before} module finder or in parent
     *         configurations
     * @param  roots
     *         The possibly-empty collection of module names of the modules
     *         to resolve
     *
     * @return The configuration that is the result of resolving the given
     *         root modules
     *
     * @throws FindException
     *         If resolution fails for any of observability-related reasons
     *         specified above
     * @throws ResolutionException
     *         If resolution fails for any of the consistency checks specified
     *         above
     * @throws IllegalArgumentException
     *         If the list of parents is empty, or the list has two or more
     *         parents with modules for different target operating systems,
     *         architectures, or versions
     *
     * @throws SecurityException
     *         If locating a module is denied by the security manager
     */
    public static Configuration resolve(ModuleFinder before,
                                        List<Configuration> parents,
                                        ModuleFinder after,
                                        Collection<String> roots)
    {
        Objects.requireNonNull(before);
        Objects.requireNonNull(after);
        Objects.requireNonNull(roots);

        List<Configuration> parentList = new ArrayList<>(parents);
        if (parentList.isEmpty())
            throw new IllegalArgumentException("'parents' is empty");

        Resolver resolver = new Resolver(before, parentList, after, null);
        resolver.resolve(roots);

        return new Configuration(parentList, resolver);
    }

    /**
     * Resolves a collection of root modules, with service binding, to create
     * configuration.
     *
     * <p> This method works exactly as specified by {@link
     * #resolve(ModuleFinder,List,ModuleFinder,Collection)
     * resolve} except that the graph of resolved modules is augmented
     * with modules induced by the service-use dependence relation. </p>
     *
     * <p><a id="service-binding"></a>More specifically, the root modules are
     * resolved as if by calling {@code resolve}. The resolved modules, and
     * all modules in the parent configurations, with {@link ModuleDescriptor#uses()
     * service dependences} are then examined. All modules found by the given
     * module finders that {@link ModuleDescriptor#provides() provide} an
     * implementation of one or more of the service types are added to the
     * module graph and then resolved as if by calling the {@code
     * resolve} method. Adding modules to the module graph may introduce new
     * service-use dependences and so the process works iteratively until no
     * more modules are added. </p>
     *
     * <p> As service binding involves resolution then it may fail with {@code
     * FindException} or {@code ResolutionException} for exactly the same
     * reasons specified in {@code resolve}. </p>
     *
     * @param  before
     *         The <em>before</em> module finder to find modules
     * @param  parents
     *         The list parent configurations in search order
     * @param  after
     *         The <em>after</em> module finder to locate modules when not
     *         located by the {@code before} module finder or in parent
     *         configurations
     * @param  roots
     *         The possibly-empty collection of module names of the modules
     *         to resolve
     *
     * @return The configuration that is the result of resolving, with service
     *         binding, the given root modules
     *
     * @throws FindException
     *         If resolution fails for any of the observability-related reasons
     *         specified by the static {@code resolve} method
     * @throws ResolutionException
     *         If resolution fails any of the consistency checks specified by
     *         the static {@code resolve} method
     * @throws IllegalArgumentException
     *         If the list of parents is empty, or the list has two or more
     *         parents with modules for different target operating systems,
     *         architectures, or versions
     * @throws SecurityException
     *         If locating a module is denied by the security manager
     */
    public static Configuration resolveAndBind(ModuleFinder before,
                                               List<Configuration> parents,
                                               ModuleFinder after,
                                               Collection<String> roots)
    {
        Objects.requireNonNull(before);
        Objects.requireNonNull(after);
        Objects.requireNonNull(roots);

        List<Configuration> parentList = new ArrayList<>(parents);
        if (parentList.isEmpty())
            throw new IllegalArgumentException("'parents' is empty");

        Resolver resolver = new Resolver(before, parentList, after, null);
        resolver.resolve(roots).bind();

        return new Configuration(parentList, resolver);
    }


    /**
     * Returns the <em>empty</em> configuration. There are no modules in the
     * empty configuration. It has no parents.
     *
     * @return The empty configuration
     */
    public static Configuration empty() {
        return EMPTY_CONFIGURATION;
    }


    /**
     * Returns an unmodifiable list of this configuration's parents, in search
     * order. If this is the {@linkplain #empty empty configuration} then an
     * empty list is returned.
     *
     * @return A possibly-empty unmodifiable list of this parent configurations
     */
    public List<Configuration> parents() {
        return parents;
    }


    /**
     * Returns an immutable set of the resolved modules in this configuration.
     *
     * @return A possibly-empty unmodifiable set of the resolved modules
     *         in this configuration
     */
    public Set<ResolvedModule> modules() {
        return modules;
    }


    /**
     * Finds a resolved module in this configuration, or if not in this
     * configuration, the {@linkplain #parents() parent} configurations.
     * Finding a module in parent configurations is equivalent to invoking
     * {@code findModule} on each parent, in search order, until the module
     * is found or all parents have been searched. In a <em>tree of
     * configurations</em> then this is equivalent to a depth-first search.
     *
     * @param  name
     *         The module name of the resolved module to find
     *
     * @return The resolved module with the given name or an empty {@code
     *         Optional} if there isn't a module with this name in this
     *         configuration or any parent configurations
     */
    public Optional<ResolvedModule> findModule(String name) {
        Objects.requireNonNull(name);
        ResolvedModule m = nameToModule.get(name);
        if (m != null)
            return Optional.of(m);

        if (!parents.isEmpty()) {
            return configurations()
                    .skip(1)  // skip this configuration
                    .map(cf -> cf.nameToModule.get(name))
                    .filter(Objects::nonNull)
                    .findFirst();
        }

        return Optional.empty();
    }


    Set<ModuleDescriptor> descriptors() {
        if (modules.isEmpty()) {
            return Collections.emptySet();
        } else {
            return modules.stream()
                    .map(ResolvedModule::reference)
                    .map(ModuleReference::descriptor)
                    .collect(Collectors.toSet());
        }
    }

    Set<ResolvedModule> reads(ResolvedModule m) {
        return Collections.unmodifiableSet(graph.get(m));
    }

    /**
     * Returns an ordered stream of configurations. The first element is this
     * configuration, the remaining elements are the parent configurations
     * in DFS order.
     *
     * @implNote For now, the assumption is that the number of elements will
     * be very low and so this method does not use a specialized spliterator.
     */
    Stream<Configuration> configurations() {
        List<Configuration> allConfigurations = this.allConfigurations;
        if (allConfigurations == null) {
            allConfigurations = new ArrayList<>();
            Set<Configuration> visited = new HashSet<>();
            Deque<Configuration> stack = new ArrayDeque<>();
            visited.add(this);
            stack.push(this);
            while (!stack.isEmpty()) {
                Configuration layer = stack.pop();
                allConfigurations.add(layer);

                // push in reverse order
                for (int i = layer.parents.size() - 1; i >= 0; i--) {
                    Configuration parent = layer.parents.get(i);
                    if (!visited.contains(parent)) {
                        visited.add(parent);
                        stack.push(parent);
                    }
                }
            }
            this.allConfigurations = Collections.unmodifiableList(allConfigurations);
        }
        return allConfigurations.stream();
    }

    private volatile List<Configuration> allConfigurations;


    /**
     * Returns a string describing this configuration.
     *
     * @return A possibly empty string describing this configuration
     */
    @Override
    public String toString() {
        return modules().stream()
                .map(ResolvedModule::name)
                .collect(Collectors.joining(", "));
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\FindException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.module;

/**
 * Thrown by a {@link ModuleFinder ModuleFinder} when an error occurs finding
 * a module. Also thrown by {@link
 * Configuration#resolve(ModuleFinder,java.util.List,ModuleFinder,java.util.Collection)
 * Configuration.resolve} when resolution fails for observability-related
 * reasons.
 *
 * @since 9
 * @spec JPMS
 */

public class FindException extends RuntimeException {
    private static final long serialVersionUID = -5817081036963388391L;

    /**
     * Constructs a {@code FindException} with no detail message.
     */
    public FindException() {
    }

    /**
     * Constructs a {@code FindException} with the given detail
     * message.
     *
     * @param msg
     *        The detail message; can be {@code null}
     */
    public FindException(String msg) {
        super(msg);
    }

    /**
     * Constructs a {@code FindException} with the given cause.
     *
     * @param cause
     *        The cause; can be {@code null}
     */
    public FindException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a {@code FindException} with the given detail message
     * and cause.
     *
     * @param msg
     *        The detail message; can be {@code null}
     * @param cause
     *        The cause; can be {@code null}
     */
    public FindException(String msg, Throwable cause) {
        super(msg, cause);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\InvalidModuleDescriptorException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.module;

/**
 * Thrown when reading a module descriptor and the module descriptor is found
 * to be malformed or otherwise cannot be interpreted as a module descriptor.
 *
 * @see ModuleDescriptor#read
 * @since 9
 * @spec JPMS
 */
public class InvalidModuleDescriptorException extends RuntimeException {
    private static final long serialVersionUID = 4863390386809347380L;

    /**
     * Constructs an {@code InvalidModuleDescriptorException} with no detail
     * message.
     */
    public InvalidModuleDescriptorException() {
    }

    /**
     * Constructs an {@code InvalidModuleDescriptorException} with the
     * specified detail message.
     *
     * @param msg
     *        The detail message; can be {@code null}
     */
    public InvalidModuleDescriptorException(String msg) {
        super(msg);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\ModuleDescriptor.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2009, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.module;

import java.io.InputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static jdk.internal.module.Checks.*;
import static java.util.Objects.*;

import jdk.internal.module.Checks;
import jdk.internal.module.ModuleInfo;


/**
 * A module descriptor.
 *
 * <p> A module descriptor describes a named module and defines methods to
 * obtain each of its components. The module descriptor for a named module
 * in the Java virtual machine is obtained by invoking the {@link
 * java.lang.Module Module}'s {@link java.lang.Module#getDescriptor
 * getDescriptor} method. Module descriptors can also be created using the
 * {@link ModuleDescriptor.Builder} class or by reading the binary form of a
 * module declaration ({@code module-info.class}) using the {@link
 * #read(InputStream,Supplier) read} methods defined here. </p>
 *
 * <p> A module descriptor describes a <em>normal</em>, open, or automatic
 * module. <em>Normal</em> modules and open modules describe their {@link
 * #requires() dependences}, {@link #exports() exported-packages}, the services
 * that they {@link #uses() use} or {@link #provides() provide}, and other
 * components. <em>Normal</em> modules may {@link #opens() open} specific
 * packages. The module descriptor for an open modules does not declare any
 * open packages (its {@code opens} method returns an empty set) but when
 * instantiated in the Java virtual machine then it is treated as if all
 * packages are open. The module descriptor for an automatic module does not
 * declare any dependences (except for the mandatory dependency on {@code
 * java.base}), and does not declare any exported or open packages. Automatic
 * module receive special treatment during resolution so that they read all
 * other modules in the configuration. When an automatic module is instantiated
 * in the Java virtual machine then it reads every unnamed module and is
 * treated as if all packages are exported and open. </p>
 *
 * <p> {@code ModuleDescriptor} objects are immutable and safe for use by
 * multiple concurrent threads.</p>
 *
 * @see java.lang.Module
 * @since 9
 * @spec JPMS
 */

public class ModuleDescriptor
    implements Comparable<ModuleDescriptor>
{

    /**
     * A modifier on a module.
     *
     * @see ModuleDescriptor#modifiers()
     * @since 9
     * @spec JPMS
     */
    public static enum Modifier {
        /**
         * An open module. An open module does not declare any open packages
         * but the resulting module is treated as if all packages are open.
         */
        OPEN,

        /**
         * An automatic module. An automatic module is treated as if it exports
         * and opens all packages.
         *
         * @apiNote This modifier does not correspond to a module flag in the
         * binary form of a module declaration ({@code module-info.class}).
         */
        AUTOMATIC,

        /**
         * The module was not explicitly or implicitly declared.
         */
        SYNTHETIC,

        /**
         * The module was implicitly declared.
         */
        MANDATED;
    }


    /**
     * <p> A dependence upon a module </p>
     *
     * @see ModuleDescriptor#requires()
     * @since 9
     * @spec JPMS
     */

    public final static class Requires
        implements Comparable<Requires>
    {

        /**
         * A modifier on a module dependence.
         *
         * @see Requires#modifiers()
         * @since 9
         * @spec JPMS
         */
        public static enum Modifier {

            /**
             * The dependence causes any module which depends on the <i>current
             * module</i> to have an implicitly declared dependence on the module
             * named by the {@code Requires}.
             */
            TRANSITIVE,

            /**
             * The dependence is mandatory in the static phase, during compilation,
             * but is optional in the dynamic phase, during execution.
             */
            STATIC,

            /**
             * The dependence was not explicitly or implicitly declared in the
             * source of the module declaration.
             */
            SYNTHETIC,

            /**
             * The dependence was implicitly declared in the source of the module
             * declaration.
             */
            MANDATED;

        }

        private final Set<Modifier> mods;
        private final String name;
        private final Version compiledVersion;
        private final String rawCompiledVersion;

        private Requires(Set<Modifier> ms, String mn, Version v, String vs) {
            assert v == null || vs == null;
            if (ms.isEmpty()) {
                ms = Collections.emptySet();
            } else {
                ms = Collections.unmodifiableSet(EnumSet.copyOf(ms));
            }
            this.mods = ms;
            this.name = mn;
            this.compiledVersion = v;
            this.rawCompiledVersion = vs;
        }

        private Requires(Set<Modifier> ms, String mn, Version v, boolean unused) {
            this.mods = ms;
            this.name = mn;
            this.compiledVersion = v;
            this.rawCompiledVersion = null;
        }

        /**
         * Returns the set of modifiers.
         *
         * @return A possibly-empty unmodifiable set of modifiers
         */
        public Set<Modifier> modifiers() {
            return mods;
        }

        /**
         * Return the module name.
         *
         * @return The module name
         */
        public String name() {
            return name;
        }

        /**
         * Returns the version of the module if recorded at compile-time.
         *
         * @return The version of the module if recorded at compile-time,
         *         or an empty {@code Optional} if no version was recorded or
         *         the version string recorded is {@linkplain Version#parse(String)
         *         unparseable}
         */
        public Optional<Version> compiledVersion() {
            return Optional.ofNullable(compiledVersion);
        }

        /**
         * Returns the string with the possibly-unparseable version of the module
         * if recorded at compile-time.
         *
         * @return The string containing the version of the module if recorded
         *         at compile-time, or an empty {@code Optional} if no version
         *         was recorded
         *
         * @see #compiledVersion()
         */
        public Optional<String> rawCompiledVersion() {
            if (compiledVersion != null) {
                return Optional.of(compiledVersion.toString());
            } else {
                return Optional.ofNullable(rawCompiledVersion);
            }
        }

        /**
         * Compares this module dependence to another.
         *
         * <p> Two {@code Requires} objects are compared by comparing their
         * module names lexicographically. Where the module names are equal
         * then the sets of modifiers are compared in the same way that
         * module modifiers are compared (see {@link ModuleDescriptor#compareTo
         * ModuleDescriptor.compareTo}). Where the module names are equal and
         * the set of modifiers are equal then the version of the modules
         * recorded at compile-time are compared. When comparing the versions
         * recorded at compile-time then a dependence that has a recorded
         * version is considered to succeed a dependence that does not have a
         * recorded version. If both recorded versions are {@linkplain
         * Version#parse(String) unparseable} then the {@linkplain
         * #rawCompiledVersion() raw version strings} are compared
         * lexicographically. </p>
         *
         * @param  that
         *         The module dependence to compare
         *
         * @return A negative integer, zero, or a positive integer if this module
         *         dependence is less than, equal to, or greater than the given
         *         module dependence
         */
        @Override
        public int compareTo(Requires that) {
            if (this == that) return 0;

            int c = this.name().compareTo(that.name());
            if (c != 0) return c;

            // modifiers
            long v1 = modsValue(this.modifiers());
            long v2 = modsValue(that.modifiers());
            c = Long.compare(v1, v2);
            if (c != 0) return c;

            // compiledVersion
            c = compare(this.compiledVersion, that.compiledVersion);
            if (c != 0) return c;

            // rawCompiledVersion
            c = compare(this.rawCompiledVersion, that.rawCompiledVersion);
            if (c != 0) return c;

            return 0;
        }

        /**
         * Tests this module dependence for equality with the given object.
         *
         * <p> If the given object is not a {@code Requires} then this method
         * returns {@code false}. Two module dependence objects are equal if
         * the module names are equal, set of modifiers are equal, and the
         * compiled version of both modules is equal or not recorded for
         * both modules. </p>
         *
         * <p> This method satisfies the general contract of the {@link
         * java.lang.Object#equals(Object) Object.equals} method. </p>
         *
         * @param   ob
         *          the object to which this object is to be compared
         *
         * @return  {@code true} if, and only if, the given object is a module
         *          dependence that is equal to this module dependence
         */
        @Override
        public boolean equals(Object ob) {
            if (!(ob instanceof Requires))
                return false;
            Requires that = (Requires)ob;
            return name.equals(that.name) && mods.equals(that.mods)
                    && Objects.equals(compiledVersion, that.compiledVersion)
                    && Objects.equals(rawCompiledVersion, that.rawCompiledVersion);
        }

        /**
         * Computes a hash code for this module dependence.
         *
         * <p> The hash code is based upon the module name, modifiers, and the
         * module version if recorded at compile time. It satisfies the general
         * contract of the {@link Object#hashCode Object.hashCode} method. </p>
         *
         * @return The hash-code value for this module dependence
         */
        @Override
        public int hashCode() {
            int hash = name.hashCode() * 43 + mods.hashCode();
            if (compiledVersion != null)
                hash = hash * 43 + compiledVersion.hashCode();
            if (rawCompiledVersion != null)
                hash = hash * 43 + rawCompiledVersion.hashCode();
            return hash;
        }

        /**
         * Returns a string describing this module dependence.
         *
         * @return A string describing this module dependence
         */
        @Override
        public String toString() {
            String what;
            if (compiledVersion != null) {
                what = name() + " (@" + compiledVersion + ")";
            } else {
                what = name();
            }
            return ModuleDescriptor.toString(mods, what);
        }
    }



    /**
     * <p> A package exported by a module, may be qualified or unqualified. </p>
     *
     * @see ModuleDescriptor#exports()
     * @since 9
     * @spec JPMS
     */

    public final static class Exports
        implements Comparable<Exports>
    {

        /**
         * A modifier on an exported package.
         *
         * @see Exports#modifiers()
         * @since 9
         * @spec JPMS
         */
        public static enum Modifier {

            /**
             * The export was not explicitly or implicitly declared in the
             * source of the module declaration.
             */
            SYNTHETIC,

            /**
             * The export was implicitly declared in the source of the module
             * declaration.
             */
            MANDATED;

        }

        private final Set<Modifier> mods;
        private final String source;
        private final Set<String> targets;  // empty if unqualified export

        /**
         * Constructs an export
         */
        private Exports(Set<Modifier> ms, String source, Set<String> targets) {
            if (ms.isEmpty()) {
                ms = Collections.emptySet();
            } else {
                ms = Collections.unmodifiableSet(EnumSet.copyOf(ms));
            }
            this.mods = ms;
            this.source = source;
            this.targets = emptyOrUnmodifiableSet(targets);
        }

        private Exports(Set<Modifier> ms,
                        String source,
                        Set<String> targets,
                        boolean unused) {
            this.mods = ms;
            this.source = source;
            this.targets = targets;
        }

        /**
         * Returns the set of modifiers.
         *
         * @return A possibly-empty unmodifiable set of modifiers
         */
        public Set<Modifier> modifiers() {
            return mods;
        }

        /**
         * Returns {@code true} if this is a qualified export.
         *
         * @return {@code true} if this is a qualified export
         */
        public boolean isQualified() {
            return !targets.isEmpty();
        }

        /**
         * Returns the package name.
         *
         * @return The package name
         */
        public String source() {
            return source;
        }

        /**
         * For a qualified export, returns the non-empty and immutable set
         * of the module names to which the package is exported. For an
         * unqualified export, returns an empty set.
         *
         * @return The set of target module names or for an unqualified
         *         export, an empty set
         */
        public Set<String> targets() {
            return targets;
        }

        /**
         * Compares this module export to another.
         *
         * <p> Two {@code Exports} objects are compared by comparing the package
         * names lexicographically. Where the packages names are equal then the
         * sets of modifiers are compared in the same way that module modifiers
         * are compared (see {@link ModuleDescriptor#compareTo
         * ModuleDescriptor.compareTo}). Where the package names are equal and
         * the set of modifiers are equal then the set of target modules are
         * compared. This is done by sorting the names of the target modules
         * in ascending order, and according to their natural ordering, and then
         * comparing the corresponding elements lexicographically. Where the
         * sets differ in size, and the larger set contains all elements of the
         * smaller set, then the larger set is considered to succeed the smaller
         * set. </p>
         *
         * @param  that
         *         The module export to compare
         *
         * @return A negative integer, zero, or a positive integer if this module
         *         export is less than, equal to, or greater than the given
         *         export dependence
         */
        @Override
        public int compareTo(Exports that) {
            if (this == that) return 0;

            int c = source.compareTo(that.source);
            if (c != 0)
                return c;

            // modifiers
            long v1 = modsValue(this.modifiers());
            long v2 = modsValue(that.modifiers());
            c = Long.compare(v1, v2);
            if (c != 0)
                return c;

            // targets
            c = compare(targets, that.targets);
            if (c != 0)
                return c;

            return 0;
        }

        /**
         * Computes a hash code for this module export.
         *
         * <p> The hash code is based upon the modifiers, the package name,
         * and for a qualified export, the set of modules names to which the
         * package is exported. It satisfies the general contract of the
         * {@link Object#hashCode Object.hashCode} method.
         *
         * @return The hash-code value for this module export
         */
        @Override
        public int hashCode() {
            int hash = mods.hashCode();
            hash = hash * 43 + source.hashCode();
            return hash * 43 + targets.hashCode();
        }

        /**
         * Tests this module export for equality with the given object.
         *
         * <p> If the given object is not an {@code Exports} then this method
         * returns {@code false}. Two module exports objects are equal if their
         * set of modifiers is equal, the package names are equal and the set
         * of target module names is equal. </p>
         *
         * <p> This method satisfies the general contract of the {@link
         * java.lang.Object#equals(Object) Object.equals} method. </p>
         *
         * @param   ob
         *          the object to which this object is to be compared
         *
         * @return  {@code true} if, and only if, the given object is a module
         *          dependence that is equal to this module dependence
         */
        @Override
        public boolean equals(Object ob) {
            if (!(ob instanceof Exports))
                return false;
            Exports other = (Exports)ob;
            return Objects.equals(this.mods, other.mods)
                    && Objects.equals(this.source, other.source)
                    && Objects.equals(this.targets, other.targets);
        }

        /**
         * Returns a string describing the exported package.
         *
         * @return A string describing the exported package
         */
        @Override
        public String toString() {
            String s = ModuleDescriptor.toString(mods, source);
            if (targets.isEmpty())
                return s;
            else
                return s + " to " + targets;
        }
    }


    /**
     * <p> A package opened by a module, may be qualified or unqualified. </p>
     *
     * <p> The <em>opens</em> directive in a module declaration declares a
     * package to be open to allow all types in the package, and all their
     * members, not just public types and their public members to be reflected
     * on by APIs that support private access or a way to bypass or suppress
     * default Java language access control checks. </p>
     *
     * @see ModuleDescriptor#opens()
     * @since 9
     * @spec JPMS
     */

    public final static class Opens
        implements Comparable<Opens>
    {
        /**
         * A modifier on an open package.
         *
         * @see Opens#modifiers()
         * @since 9
         * @spec JPMS
         */
        public static enum Modifier {

            /**
             * The open package was not explicitly or implicitly declared in
             * the source of the module declaration.
             */
            SYNTHETIC,

            /**
             * The open package was implicitly declared in the source of the
             * module declaration.
             */
            MANDATED;

        }

        private final Set<Modifier> mods;
        private final String source;
        private final Set<String> targets;  // empty if unqualified export

        /**
         * Constructs an Opens
         */
        private Opens(Set<Modifier> ms, String source, Set<String> targets) {
            if (ms.isEmpty()) {
                ms = Collections.emptySet();
            } else {
                ms = Collections.unmodifiableSet(EnumSet.copyOf(ms));
            }
            this.mods = ms;
            this.source = source;
            this.targets = emptyOrUnmodifiableSet(targets);
        }

        private Opens(Set<Modifier> ms,
                      String source,
                      Set<String> targets,
                      boolean unused) {
            this.mods = ms;
            this.source = source;
            this.targets = targets;
        }

        /**
         * Returns the set of modifiers.
         *
         * @return A possibly-empty unmodifiable set of modifiers
         */
        public Set<Modifier> modifiers() {
            return mods;
        }

        /**
         * Returns {@code true} if this is a qualified opens.
         *
         * @return {@code true} if this is a qualified opens
         */
        public boolean isQualified() {
            return !targets.isEmpty();
        }

        /**
         * Returns the package name.
         *
         * @return The package name
         */
        public String source() {
            return source;
        }

        /**
         * For a qualified opens, returns the non-empty and immutable set
         * of the module names to which the package is open. For an
         * unqualified opens, returns an empty set.
         *
         * @return The set of target module names or for an unqualified
         *         opens, an empty set
         */
        public Set<String> targets() {
            return targets;
        }

        /**
         * Compares this module opens to another.
         *
         * <p> Two {@code Opens} objects are compared by comparing the package
         * names lexicographically. Where the packages names are equal then the
         * sets of modifiers are compared in the same way that module modifiers
         * are compared (see {@link ModuleDescriptor#compareTo
         * ModuleDescriptor.compareTo}). Where the package names are equal and
         * the set of modifiers are equal then the set of target modules are
         * compared. This is done by sorting the names of the target modules
         * in ascending order, and according to their natural ordering, and then
         * comparing the corresponding elements lexicographically. Where the
         * sets differ in size, and the larger set contains all elements of the
         * smaller set, then the larger set is considered to succeed the smaller
         * set. </p>
         *
         * @param  that
         *         The module opens to compare
         *
         * @return A negative integer, zero, or a positive integer if this module
         *         opens is less than, equal to, or greater than the given
         *         module opens
         */
        @Override
        public int compareTo(Opens that) {
            if (this == that) return 0;

            int c = source.compareTo(that.source);
            if (c != 0)
                return c;

            // modifiers
            long v1 = modsValue(this.modifiers());
            long v2 = modsValue(that.modifiers());
            c = Long.compare(v1, v2);
            if (c != 0)
                return c;

            // targets
            c = compare(targets, that.targets);
            if (c != 0)
                return c;

            return 0;
        }

        /**
         * Computes a hash code for this module opens.
         *
         * <p> The hash code is based upon the modifiers, the package name,
         * and for a qualified opens, the set of modules names to which the
         * package is opened. It satisfies the general contract of the
         * {@link Object#hashCode Object.hashCode} method.
         *
         * @return The hash-code value for this module opens
         */
        @Override
        public int hashCode() {
            int hash = mods.hashCode();
            hash = hash * 43 + source.hashCode();
            return hash * 43 + targets.hashCode();
        }

        /**
         * Tests this module opens for equality with the given object.
         *
         * <p> If the given object is not an {@code Opens} then this method
         * returns {@code false}. Two {@code Opens} objects are equal if their
         * set of modifiers is equal, the package names are equal and the set
         * of target module names is equal. </p>
         *
         * <p> This method satisfies the general contract of the {@link
         * java.lang.Object#equals(Object) Object.equals} method. </p>
         *
         * @param   ob
         *          the object to which this object is to be compared
         *
         * @return  {@code true} if, and only if, the given object is a module
         *          dependence that is equal to this module dependence
         */
        @Override
        public boolean equals(Object ob) {
            if (!(ob instanceof Opens))
                return false;
            Opens other = (Opens)ob;
            return Objects.equals(this.mods, other.mods)
                    && Objects.equals(this.source, other.source)
                    && Objects.equals(this.targets, other.targets);
        }

        /**
         * Returns a string describing the open package.
         *
         * @return A string describing the open package
         */
        @Override
        public String toString() {
            String s = ModuleDescriptor.toString(mods, source);
            if (targets.isEmpty())
                return s;
            else
                return s + " to " + targets;
        }
    }


    /**
     * <p> A service that a module provides one or more implementations of. </p>
     *
     * @see ModuleDescriptor#provides()
     * @since 9
     * @spec JPMS
     */

    public final static class Provides
        implements Comparable<Provides>
    {
        private final String service;
        private final List<String> providers;

        private Provides(String service, List<String> providers) {
            this.service = service;
            this.providers = Collections.unmodifiableList(providers);
        }

        private Provides(String service, List<String> providers, boolean unused) {
            this.service = service;
            this.providers = providers;
        }

        /**
         * Returns the fully qualified class name of the service type.
         *
         * @return The fully qualified class name of the service type
         */
        public String service() { return service; }

        /**
         * Returns the list of the fully qualified class names of the providers
         * or provider factories.
         *
         * @return A non-empty and unmodifiable list of the fully qualified class
         *         names of the providers or provider factories
         */
        public List<String> providers() { return providers; }

        /**
         * Compares this provides to another.
         *
         * <p> Two {@code Provides} objects are compared by comparing the fully
         * qualified class name of the service type lexicographically. Where the
         * class names are equal then the list of the provider class names are
         * compared by comparing the corresponding elements of both lists
         * lexicographically and in sequence. Where the lists differ in size,
         * {@code N} is the size of the shorter list, and the first {@code N}
         * corresponding elements are equal, then the longer list is considered
         * to succeed the shorter list. </p>
         *
         * @param  that
         *         The {@code Provides} to compare
         *
         * @return A negative integer, zero, or a positive integer if this provides
         *         is less than, equal to, or greater than the given provides
         */
        public int compareTo(Provides that) {
            if (this == that) return 0;

            int c = service.compareTo(that.service);
            if (c != 0) return c;

            // compare provider class names in sequence
            int size1 = this.providers.size();
            int size2 = that.providers.size();
            for (int index=0; index<Math.min(size1, size2); index++) {
                String e1 = this.providers.get(index);
                String e2 = that.providers.get(index);
                c = e1.compareTo(e2);
                if (c != 0) return c;
            }
            if (size1 == size2) {
                return 0;
            } else {
                return (size1 > size2) ? 1 : -1;
            }
        }

        /**
         * Computes a hash code for this provides.
         *
         * <p> The hash code is based upon the service type and the set of
         * providers. It satisfies the general contract of the {@link
         * Object#hashCode Object.hashCode} method. </p>
         *
         * @return The hash-code value for this module provides
         */
        @Override
        public int hashCode() {
            return service.hashCode() * 43 + providers.hashCode();
        }

        /**
         * Tests this provides for equality with the given object.
         *
         * <p> If the given object is not a {@code Provides} then this method
         * returns {@code false}. Two {@code Provides} objects are equal if the
         * service type is equal and the list of providers is equal. </p>
         *
         * <p> This method satisfies the general contract of the {@link
         * java.lang.Object#equals(Object) Object.equals} method. </p>
         *
         * @param   ob
         *          the object to which this object is to be compared
         *
         * @return  {@code true} if, and only if, the given object is a
         *          {@code Provides} that is equal to this {@code Provides}
         */
        @Override
        public boolean equals(Object ob) {
            if (!(ob instanceof Provides))
                return false;
            Provides other = (Provides)ob;
            return Objects.equals(this.service, other.service) &&
                    Objects.equals(this.providers, other.providers);
        }

        /**
         * Returns a string describing this provides.
         *
         * @return A string describing this provides
         */
        @Override
        public String toString() {
            return service + " with " + providers;
        }

    }



    /**
     * A module's version string.
     *
     * <p> A version string has three components: The version number itself, an
     * optional pre-release version, and an optional build version.  Each
     * component is a sequence of tokens; each token is either a non-negative
     * integer or a string.  Tokens are separated by the punctuation characters
     * {@code '.'}, {@code '-'}, or {@code '+'}, or by transitions from a
     * sequence of digits to a sequence of characters that are neither digits
     * nor punctuation characters, or vice versa.
     *
     * <ul>
     *
     *   <li> The <i>version number</i> is a sequence of tokens separated by
     *   {@code '.'} characters, terminated by the first {@code '-'} or {@code
     *   '+'} character. </li>
     *
     *   <li> The <i>pre-release version</i> is a sequence of tokens separated
     *   by {@code '.'} or {@code '-'} characters, terminated by the first
     *   {@code '+'} character. </li>
     *
     *   <li> The <i>build version</i> is a sequence of tokens separated by
     *   {@code '.'}, {@code '-'}, or {@code '+'} characters.
     *
     * </ul>
     *
     * <p> When comparing two version strings, the elements of their
     * corresponding components are compared in pointwise fashion.  If one
     * component is longer than the other, but otherwise equal to it, then the
     * first component is considered the greater of the two; otherwise, if two
     * corresponding elements are integers then they are compared as such;
     * otherwise, at least one of the elements is a string, so the other is
     * converted into a string if it is an integer and the two are compared
     * lexicographically.  Trailing integer elements with the value zero are
     * ignored.
     *
     * <p> Given two version strings, if their version numbers differ then the
     * result of comparing them is the result of comparing their version
     * numbers; otherwise, if one of them has a pre-release version but the
     * other does not then the first is considered to precede the second,
     * otherwise the result of comparing them is the result of comparing their
     * pre-release versions; otherwise, the result of comparing them is the
     * result of comparing their build versions.
     *
     * @see ModuleDescriptor#version()
     * @since 9
     * @spec JPMS
     */

    public final static class Version
        implements Comparable<Version>
    {

        private final String version;

        // If Java had disjunctive types then we'd write List<Integer|String> here
        //
        private final List<Object> sequence;
        private final List<Object> pre;
        private final List<Object> build;

        // Take a numeric token starting at position i
        // Append it to the given list
        // Return the index of the first character not taken
        // Requires: s.charAt(i) is (decimal) numeric
        //
        private static int takeNumber(String s, int i, List<Object> acc) {
            char c = s.charAt(i);
            int d = (c - '0');
            int n = s.length();
            while (++i < n) {
                c = s.charAt(i);
                if (c >= '0' && c <= '9') {
                    d = d * 10 + (c - '0');
                    continue;
                }
                break;
            }
            acc.add(d);
            return i;
        }

        // Take a string token starting at position i
        // Append it to the given list
        // Return the index of the first character not taken
        // Requires: s.charAt(i) is not '.'
        //
        private static int takeString(String s, int i, List<Object> acc) {
            int b = i;
            int n = s.length();
            while (++i < n) {
                char c = s.charAt(i);
                if (c != '.' && c != '-' && c != '+' && !(c >= '0' && c <= '9'))
                    continue;
                break;
            }
            acc.add(s.substring(b, i));
            return i;
        }

        // Syntax: tok+ ( '-' tok+)? ( '+' tok+)?
        // First token string is sequence, second is pre, third is build
        // Tokens are separated by '.' or '-', or by changes between alpha & numeric
        // Numeric tokens are compared as decimal integers
        // Non-numeric tokens are compared lexicographically
        // A version with a non-empty pre is less than a version with same seq but no pre
        // Tokens in build may contain '-' and '+'
        //
        private Version(String v) {

            if (v == null)
                throw new IllegalArgumentException("Null version string");
            int n = v.length();
            if (n == 0)
                throw new IllegalArgumentException("Empty version string");

            int i = 0;
            char c = v.charAt(i);
            if (!(c >= '0' && c <= '9'))
                throw new IllegalArgumentException(v
                                                   + ": Version string does not start"
                                                   + " with a number");

            List<Object> sequence = new ArrayList<>(4);
            List<Object> pre = new ArrayList<>(2);
            List<Object> build = new ArrayList<>(2);

            i = takeNumber(v, i, sequence);

            while (i < n) {
                c = v.charAt(i);
                if (c == '.') {
                    i++;
                    continue;
                }
                if (c == '-' || c == '+') {
                    i++;
                    break;
                }
                if (c >= '0' && c <= '9')
                    i = takeNumber(v, i, sequence);
                else
                    i = takeString(v, i, sequence);
            }

            if (c == '-' && i >= n)
                throw new IllegalArgumentException(v + ": Empty pre-release");

            while (i < n) {
                c = v.charAt(i);
                if (c >= '0' && c <= '9')
                    i = takeNumber(v, i, pre);
                else
                    i = takeString(v, i, pre);
                if (i >= n)
                    break;
                c = v.charAt(i);
                if (c == '.' || c == '-') {
                    i++;
                    continue;
                }
                if (c == '+') {
                    i++;
                    break;
                }
            }

            if (c == '+' && i >= n)
                throw new IllegalArgumentException(v + ": Empty pre-release");

            while (i < n) {
                c = v.charAt(i);
                if (c >= '0' && c <= '9')
                    i = takeNumber(v, i, build);
                else
                    i = takeString(v, i, build);
                if (i >= n)
                    break;
                c = v.charAt(i);
                if (c == '.' || c == '-' || c == '+') {
                    i++;
                    continue;
                }
            }

            this.version = v;
            this.sequence = sequence;
            this.pre = pre;
            this.build = build;
        }

        /**
         * Parses the given string as a version string.
         *
         * @param  v
         *         The string to parse
         *
         * @return The resulting {@code Version}
         *
         * @throws IllegalArgumentException
         *         If {@code v} is {@code null}, an empty string, or cannot be
         *         parsed as a version string
         */
        public static Version parse(String v) {
            return new Version(v);
        }

        @SuppressWarnings("unchecked")
        private int cmp(Object o1, Object o2) {
            return ((Comparable)o1).compareTo(o2);
        }

        private int compareTokens(List<Object> ts1, List<Object> ts2) {
            int n = Math.min(ts1.size(), ts2.size());
            for (int i = 0; i < n; i++) {
                Object o1 = ts1.get(i);
                Object o2 = ts2.get(i);
                if ((o1 instanceof Integer && o2 instanceof Integer)
                    || (o1 instanceof String && o2 instanceof String))
                {
                    int c = cmp(o1, o2);
                    if (c == 0)
                        continue;
                    return c;
                }
                // Types differ, so convert number to string form
                int c = o1.toString().compareTo(o2.toString());
                if (c == 0)
                    continue;
                return c;
            }
            List<Object> rest = ts1.size() > ts2.size() ? ts1 : ts2;
            int e = rest.size();
            for (int i = n; i < e; i++) {
                Object o = rest.get(i);
                if (o instanceof Integer && ((Integer)o) == 0)
                    continue;
                return ts1.size() - ts2.size();
            }
            return 0;
        }

        /**
         * Compares this module version to another module version. Module
         * versions are compared as described in the class description.
         *
         * @param that
         *        The module version to compare
         *
         * @return A negative integer, zero, or a positive integer as this
         *         module version is less than, equal to, or greater than the
         *         given module version
         */
        @Override
        public int compareTo(Version that) {
            int c = compareTokens(this.sequence, that.sequence);
            if (c != 0) return c;
            if (this.pre.isEmpty()) {
                if (!that.pre.isEmpty()) return +1;
            } else {
                if (that.pre.isEmpty()) return -1;
            }
            c = compareTokens(this.pre, that.pre);
            if (c != 0) return c;
            return compareTokens(this.build, that.build);
        }

        /**
         * Tests this module version for equality with the given object.
         *
         * <p> If the given object is not a {@code Version} then this method
         * returns {@code false}. Two module version are equal if their
         * corresponding components are equal. </p>
         *
         * <p> This method satisfies the general contract of the {@link
         * java.lang.Object#equals(Object) Object.equals} method. </p>
         *
         * @param   ob
         *          the object to which this object is to be compared
         *
         * @return  {@code true} if, and only if, the given object is a module
         *          reference that is equal to this module reference
         */
        @Override
        public boolean equals(Object ob) {
            if (!(ob instanceof Version))
                return false;
            return compareTo((Version)ob) == 0;
        }

        /**
         * Computes a hash code for this module version.
         *
         * <p> The hash code is based upon the components of the version and
         * satisfies the general contract of the {@link Object#hashCode
         * Object.hashCode} method. </p>
         *
         * @return The hash-code value for this module version
         */
        @Override
        public int hashCode() {
            return version.hashCode();
        }

        /**
         * Returns the string from which this version was parsed.
         *
         * @return The string from which this version was parsed.
         */
        @Override
        public String toString() {
            return version;
        }

    }


    private final String name;
    private final Version version;
    private final String rawVersionString;
    private final Set<Modifier> modifiers;
    private final boolean open;  // true if modifiers contains OPEN
    private final boolean automatic;  // true if modifiers contains AUTOMATIC
    private final Set<Requires> requires;
    private final Set<Exports> exports;
    private final Set<Opens> opens;
    private final Set<String> uses;
    private final Set<Provides> provides;
    private final Set<String> packages;
    private final String mainClass;

    private ModuleDescriptor(String name,
                             Version version,
                             String rawVersionString,
                             Set<Modifier> modifiers,
                             Set<Requires> requires,
                             Set<Exports> exports,
                             Set<Opens> opens,
                             Set<String> uses,
                             Set<Provides> provides,
                             Set<String> packages,
                             String mainClass)
    {
        assert version == null || rawVersionString == null;
        this.name = name;
        this.version = version;
        this.rawVersionString = rawVersionString;
        this.modifiers = emptyOrUnmodifiableSet(modifiers);
        this.open = modifiers.contains(Modifier.OPEN);
        this.automatic = modifiers.contains(Modifier.AUTOMATIC);
        assert (requires.stream().map(Requires::name).distinct().count()
                == requires.size());
        this.requires = emptyOrUnmodifiableSet(requires);
        this.exports = emptyOrUnmodifiableSet(exports);
        this.opens = emptyOrUnmodifiableSet(opens);
        this.uses = emptyOrUnmodifiableSet(uses);
        this.provides = emptyOrUnmodifiableSet(provides);

        this.packages = emptyOrUnmodifiableSet(packages);
        this.mainClass = mainClass;
    }

    /**
     * Creates a module descriptor from its components.
     * The arguments are pre-validated and sets are unmodifiable sets.
     */
    ModuleDescriptor(String name,
                     Version version,
                     Set<Modifier> modifiers,
                     Set<Requires> requires,
                     Set<Exports> exports,
                     Set<Opens> opens,
                     Set<String> uses,
                     Set<Provides> provides,
                     Set<String> packages,
                     String mainClass,
                     int hashCode,
                     boolean unused) {
        this.name = name;
        this.version = version;
        this.rawVersionString = null;
        this.modifiers = modifiers;
        this.open = modifiers.contains(Modifier.OPEN);
        this.automatic = modifiers.contains(Modifier.AUTOMATIC);
        this.requires = requires;
        this.exports = exports;
        this.opens = opens;
        this.uses = uses;
        this.provides = provides;
        this.packages = packages;
        this.mainClass = mainClass;
        this.hash = hashCode;
    }

    /**
     * <p> Returns the module name. </p>
     *
     * @return The module name
     */
    public String name() {
        return name;
    }

    /**
     * <p> Returns the set of module modifiers. </p>
     *
     * @return A possibly-empty unmodifiable set of modifiers
     */
    public Set<Modifier> modifiers() {
        return modifiers;
    }

    /**
     * <p> Returns {@code true} if this is an open module. </p>
     *
     * <p> This method is equivalent to testing if the set of {@link #modifiers()
     * modifiers} contains the {@link Modifier#OPEN OPEN} modifier. </p>
     *
     * @return  {@code true} if this is an open module
     */
    public boolean isOpen() {
        return open;
    }

    /**
     * <p> Returns {@code true} if this is an automatic module. </p>
     *
     * <p> This method is equivalent to testing if the set of {@link #modifiers()
     * modifiers} contains the {@link Modifier#OPEN AUTOMATIC} modifier. </p>
     *
     * @return  {@code true} if this is an automatic module
     */
    public boolean isAutomatic() {
        return automatic;
    }

    /**
     * <p> Returns the set of {@code Requires} objects representing the module
     * dependences. </p>
     *
     * <p> The set includes a dependency on "{@code java.base}" when this
     * module is not named "{@code java.base}". If this module is an automatic
     * module then it does not have a dependency on any module other than
     * "{@code java.base}". </p>
     *
     * @return  A possibly-empty unmodifiable set of {@link Requires} objects
     */
    public Set<Requires> requires() {
        return requires;
    }

    /**
     * <p> Returns the set of {@code Exports} objects representing the exported
     * packages. </p>
     *
     * <p> If this module is an automatic module then the set of exports
     * is empty. </p>
     *
     * @return  A possibly-empty unmodifiable set of exported packages
     */
    public Set<Exports> exports() {
        return exports;
    }

    /**
     * <p> Returns the set of {@code Opens} objects representing the open
     * packages. </p>
     *
     * <p> If this module is an open module or an automatic module then the
     * set of open packages is empty. </p>
     *
     * @return  A possibly-empty unmodifiable set of open packages
     */
    public Set<Opens> opens() {
        return opens;
    }

    /**
     * <p> Returns the set of service dependences. </p>
     *
     * <p> If this module is an automatic module then the set of service
     * dependences is empty. </p>
     *
     * @return  A possibly-empty unmodifiable set of the fully qualified class
     *          names of the service types used
     */
    public Set<String> uses() {
        return uses;
    }

    /**
     * <p> Returns the set of {@code Provides} objects representing the
     * services that the module provides. </p>
     *
     * @return The possibly-empty unmodifiable set of the services that this
     *         module provides
     */
    public Set<Provides> provides() {
        return provides;
    }

    /**
     * <p> Returns the module version. </p>
     *
     * @return This module's version, or an empty {@code Optional} if the
     *         module does not have a version or the version is
     *         {@linkplain Version#parse(String) unparseable}
     */
    public Optional<Version> version() {
        return Optional.ofNullable(version);
    }

    /**
     * <p> Returns the string with the possibly-unparseable version of the
     * module </p>
     *
     * @return The string containing the version of the module or an empty
     *         {@code Optional} if the module does not have a version
     *
     * @see #version()
     */
    public Optional<String> rawVersion() {
        if (version != null) {
            return Optional.of(version.toString());
        } else {
            return Optional.ofNullable(rawVersionString);
        }
    }

    /**
     * <p> Returns a string containing the module name and, if present, its
     * version. </p>
     *
     * @return A string containing the module name and, if present, its
     *         version
     */
    public String toNameAndVersion() {
        if (version != null) {
            return name() + "@" + version;
        } else {
            return name();
        }
    }

    /**
     * <p> Returns the module main class. </p>
     *
     * @return The fully qualified class name of the module's main class
     */
    public Optional<String> mainClass() {
        return Optional.ofNullable(mainClass);
    }

    /**
     * Returns the set of packages in the module.
     *
     * <p> The set of packages includes all exported and open packages, as well
     * as the packages of any service providers, and the package for the main
     * class. </p>
     *
     * @return A possibly-empty unmodifiable set of the packages in the module
     */
    public Set<String> packages() {
        return packages;
    }


    /**
     * A builder for building {@link ModuleDescriptor} objects.
     *
     * <p> {@code ModuleDescriptor} defines the {@link #newModule newModule},
     * {@link #newOpenModule newOpenModule}, and {@link #newAutomaticModule
     * newAutomaticModule} methods to create builders for building
     * <em>normal</em>, open, and automatic modules. </p>
     *
     * <p> The set of packages in the module are accumulated by the {@code
     * Builder} as the {@link ModuleDescriptor.Builder#exports(String) exports},
     * {@link ModuleDescriptor.Builder#opens(String) opens},
     * {@link ModuleDescriptor.Builder#packages(Set) packages},
     * {@link ModuleDescriptor.Builder#provides(String,List) provides}, and
     * {@link ModuleDescriptor.Builder#mainClass(String) mainClass} methods are
     * invoked. </p>
     *
     * <p> The module names, package names, and class names that are parameters
     * specified to the builder methods are the module names, package names,
     * and qualified names of classes (in named packages) as defined in the
     * <cite>The Java&trade; Language Specification</cite>. </p>
     *
     * <p> Example usage: </p>
     * <pre>{@code    ModuleDescriptor descriptor = ModuleDescriptor.newModule("stats.core")
     *         .requires("java.base")
     *         .exports("org.acme.stats.core.clustering")
     *         .exports("org.acme.stats.core.regression")
     *         .packages(Set.of("org.acme.stats.core.internal"))
     *         .build();
     * }</pre>
     *
     * @apiNote A {@code Builder} checks the components and invariants as
     * components are added to the builder. The rationale for this is to detect
     * errors as early as possible and not defer all validation to the
     * {@link #build build} method.
     *
     * @since 9
     * @spec JPMS
     */
    public static final class Builder {
        final String name;
        final boolean strict;
        final Set<Modifier> modifiers;
        final boolean open;
        final boolean automatic;
        final Set<String> packages = new HashSet<>();
        final Map<String, Requires> requires = new HashMap<>();
        final Map<String, Exports> exports = new HashMap<>();
        final Map<String, Opens> opens = new HashMap<>();
        final Set<String> uses = new HashSet<>();
        final Map<String, Provides> provides = new HashMap<>();
        Version version;
        String rawVersionString;
        String mainClass;

        /**
         * Initializes a new builder with the given module name.
         *
         * If {@code strict} is {@code true} then module, package, and class
         * names are checked to ensure they are legal names. In addition, the
         * {@link #build buid} method will add "{@code requires java.base}" if
         * the dependency is not declared.
         */
        Builder(String name, boolean strict, Set<Modifier> modifiers) {
            this.name = (strict) ? requireModuleName(name) : name;
            this.strict = strict;
            this.modifiers = modifiers;
            this.open = modifiers.contains(Modifier.OPEN);
            this.automatic = modifiers.contains(Modifier.AUTOMATIC);
            assert !open || !automatic;
        }

        /**
         * Returns a snapshot of the packages in the module.
         */
        /* package */ Set<String> packages() {
            return Collections.unmodifiableSet(packages);
        }

        /**
         * Adds a dependence on a module.
         *
         * @param  req
         *         The dependence
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the dependence is on the module that this builder was
         *         initialized to build
         * @throws IllegalStateException
         *         If the dependence on the module has already been declared
         *         or this builder is for an automatic module
         */
        public Builder requires(Requires req) {
            if (automatic)
                throw new IllegalStateException("Automatic modules cannot declare"
                                                + " dependences");
            String mn = req.name();
            if (name.equals(mn))
                throw new IllegalArgumentException("Dependence on self");
            if (requires.containsKey(mn))
                throw new IllegalStateException("Dependence upon " + mn
                                                + " already declared");
            requires.put(mn, req);
            return this;
        }

        /**
         * Adds a dependence on a module with the given (and possibly empty)
         * set of modifiers. The dependence includes the version of the
         * module that was recorded at compile-time.
         *
         * @param  ms
         *         The set of modifiers
         * @param  mn
         *         The module name
         * @param  compiledVersion
         *         The version of the module recorded at compile-time
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the module name is {@code null}, is not a legal module
         *         name, or is equal to the module name that this builder
         *         was initialized to build
         * @throws IllegalStateException
         *         If the dependence on the module has already been declared
         *         or this builder is for an automatic module
         */
        public Builder requires(Set<Requires.Modifier> ms,
                                String mn,
                                Version compiledVersion) {
            Objects.requireNonNull(compiledVersion);
            if (strict)
                mn = requireModuleName(mn);
            return requires(new Requires(ms, mn, compiledVersion, null));
        }

        /* package */Builder requires(Set<Requires.Modifier> ms,
                                      String mn,
                                      String rawCompiledVersion) {
            Requires r;
            try {
                Version v = Version.parse(rawCompiledVersion);
                r = new Requires(ms, mn, v, null);
            } catch (IllegalArgumentException e) {
                if (strict) throw e;
                r = new Requires(ms, mn, null, rawCompiledVersion);
            }
            return requires(r);
        }

        /**
         * Adds a dependence on a module with the given (and possibly empty)
         * set of modifiers.
         *
         * @param  ms
         *         The set of modifiers
         * @param  mn
         *         The module name
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the module name is {@code null}, is not a legal module
         *         name, or is equal to the module name that this builder
         *         was initialized to build
         * @throws IllegalStateException
         *         If the dependence on the module has already been declared
         *         or this builder is for an automatic module
         */
        public Builder requires(Set<Requires.Modifier> ms, String mn) {
            if (strict)
                mn = requireModuleName(mn);
            return requires(new Requires(ms, mn, null, null));
        }

        /**
         * Adds a dependence on a module with an empty set of modifiers.
         *
         * @param  mn
         *         The module name
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the module name is {@code null}, is not a legal module
         *         name, or is equal to the module name that this builder
         *         was initialized to build
         * @throws IllegalStateException
         *         If the dependence on the module has already been declared
         *         or this builder is for an automatic module
         */
        public Builder requires(String mn) {
            return requires(EnumSet.noneOf(Requires.Modifier.class), mn);
        }

        /**
         * Adds an exported package.
         *
         * @param  e
         *         The export
         *
         * @return This builder
         *
         * @throws IllegalStateException
         *         If the {@link Exports#source() package} is already declared as
         *         exported or this builder is for an automatic module
         */
        public Builder exports(Exports e) {
            if (automatic) {
                throw new IllegalStateException("Automatic modules cannot declare"
                                                 + " exported packages");
            }
            String source = e.source();
            if (exports.containsKey(source)) {
                throw new IllegalStateException("Exported package " + source
                                                 + " already declared");
            }
            exports.put(source, e);
            packages.add(source);
            return this;
        }

        /**
         * Adds an exported package with the given (and possibly empty) set of
         * modifiers. The package is exported to a set of target modules.
         *
         * @param  ms
         *         The set of modifiers
         * @param  pn
         *         The package name
         * @param  targets
         *         The set of target modules names
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the package name is {@code null} or is not a legal
         *         package name, the set of target modules is empty, or the set
         *         of target modules contains a name that is not a legal module
         *         name
         * @throws IllegalStateException
         *         If the package is already declared as exported
         *         or this builder is for an automatic module
         */
        public Builder exports(Set<Exports.Modifier> ms,
                               String pn,
                               Set<String> targets)
        {
            Exports e = new Exports(ms, pn, targets);

            // check targets
            targets = e.targets();
            if (targets.isEmpty())
                throw new IllegalArgumentException("Empty target set");
            if (strict) {
                requirePackageName(e.source());
                targets.forEach(Checks::requireModuleName);
            }
            return exports(e);
        }

        /**
         * Adds an exported package with the given (and possibly empty) set of
         * modifiers. The package is exported to all modules.
         *
         * @param  ms
         *         The set of modifiers
         * @param  pn
         *         The package name
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the package name is {@code null} or is not a legal
         *         package name
         * @throws IllegalStateException
         *         If the package is already declared as exported
         *         or this builder is for an automatic module
         */
        public Builder exports(Set<Exports.Modifier> ms, String pn) {
            if (strict) {
                requirePackageName(pn);
            }
            Exports e = new Exports(ms, pn, Collections.emptySet());
            return exports(e);
        }

        /**
         * Adds an exported package. The package is exported to a set of target
         * modules.
         *
         * @param  pn
         *         The package name
         * @param  targets
         *         The set of target modules names
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the package name is {@code null} or is not a legal
         *         package name, the set of target modules is empty, or the set
         *         of target modules contains a name that is not a legal module
         *         name
         * @throws IllegalStateException
         *         If the package is already declared as exported
         *         or this builder is for an automatic module
         */
        public Builder exports(String pn, Set<String> targets) {
            return exports(Collections.emptySet(), pn, targets);
        }

        /**
         * Adds an exported package. The package is exported to all modules.
         *
         * @param  pn
         *         The package name
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the package name is {@code null} or is not a legal
         *         package name
         * @throws IllegalStateException
         *         If the package is already declared as exported
         *         or this builder is for an automatic module
         */
        public Builder exports(String pn) {
            return exports(Collections.emptySet(), pn);
        }

        /**
         * Adds an open package.
         *
         * @param  obj
         *         The {@code Opens} object
         *
         * @return This builder
         *
         * @throws IllegalStateException
         *         If the package is already declared as open, or this is a
         *         builder for an open module or automatic module
         */
        public Builder opens(Opens obj) {
            if (open || automatic) {
                throw new IllegalStateException("Open or automatic modules cannot"
                                                + " declare open packages");
            }
            String source = obj.source();
            if (opens.containsKey(source)) {
                throw new IllegalStateException("Open package " + source
                                                + " already declared");
            }
            opens.put(source, obj);
            packages.add(source);
            return this;
        }


        /**
         * Adds an open package with the given (and possibly empty) set of
         * modifiers. The package is open to a set of target modules.
         *
         * @param  ms
         *         The set of modifiers
         * @param  pn
         *         The package name
         * @param  targets
         *         The set of target modules names
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the package name is {@code null} or is not a legal
         *         package name, the set of target modules is empty, or the set
         *         of target modules contains a name that is not a legal module
         *         name
         * @throws IllegalStateException
         *         If the package is already declared as open, or this is a
         *         builder for an open module or automatic module
         */
        public Builder opens(Set<Opens.Modifier> ms,
                             String pn,
                             Set<String> targets)
        {
            Opens opens = new Opens(ms, pn, targets);

            // check targets
            targets = opens.targets();
            if (targets.isEmpty())
                throw new IllegalArgumentException("Empty target set");
            if (strict) {
                requirePackageName(opens.source());
                targets.forEach(Checks::requireModuleName);
            }
            return opens(opens);
        }

        /**
         * Adds an open package with the given (and possibly empty) set of
         * modifiers. The package is open to all modules.
         *
         * @param  ms
         *         The set of modifiers
         * @param  pn
         *         The package name
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the package name is {@code null} or is not a legal
         *         package name
         * @throws IllegalStateException
         *         If the package is already declared as open, or this is a
         *         builder for an open module or automatic module
         */
        public Builder opens(Set<Opens.Modifier> ms, String pn) {
            if (strict) {
                requirePackageName(pn);
            }
            Opens e = new Opens(ms, pn, Collections.emptySet());
            return opens(e);
        }

        /**
         * Adds an open package. The package is open to a set of target modules.
         *
         * @param  pn
         *         The package name
         * @param  targets
         *         The set of target modules names
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the package name is {@code null} or is not a legal
         *         package name, the set of target modules is empty, or the set
         *         of target modules contains a name that is not a legal module
         *         name
         * @throws IllegalStateException
         *         If the package is already declared as open, or this is a
         *         builder for an open module or automatic module
         */
        public Builder opens(String pn, Set<String> targets) {
            return opens(Collections.emptySet(), pn, targets);
        }

        /**
         * Adds an open package. The package is open to all modules.
         *
         * @param  pn
         *         The package name
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the package name is {@code null} or is not a legal
         *         package name
         * @throws IllegalStateException
         *         If the package is already declared as open, or this is a
         *         builder for an open module or automatic module
         */
        public Builder opens(String pn) {
            return opens(Collections.emptySet(), pn);
        }

        /**
         * Adds a service dependence.
         *
         * @param  service
         *         The service type
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the service type is {@code null} or not a qualified name of
         *         a class in a named package
         * @throws IllegalStateException
         *         If a dependency on the service type has already been declared
         *         or this is a builder for an automatic module
         */
        public Builder uses(String service) {
            if (automatic)
                throw new IllegalStateException("Automatic modules can not declare"
                                                + " service dependences");
            if (uses.contains(requireServiceTypeName(service)))
                throw new IllegalStateException("Dependence upon service "
                                                + service + " already declared");
            uses.add(service);
            return this;
        }

        /**
         * Provides a service with one or more implementations. The package for
         * each {@link Provides#providers provider} (or provider factory) is
         * added to the module if not already added.
         *
         * @param  p
         *         The provides
         *
         * @return This builder
         *
         * @throws IllegalStateException
         *         If the providers for the service type have already been
         *         declared
         */
        public Builder provides(Provides p) {
            String service = p.service();
            if (provides.containsKey(service))
                throw new IllegalStateException("Providers of service "
                                                + service + " already declared");
            provides.put(service, p);
            p.providers().forEach(name -> packages.add(packageName(name)));
            return this;
        }

        /**
         * Provides implementations of a service. The package for each provider
         * (or provider factory) is added to the module if not already added.
         *
         * @param  service
         *         The service type
         * @param  providers
         *         The list of provider or provider factory class names
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If the service type or any of the provider class names is
         *         {@code null} or not a qualified name of a class in a named
         *         package, or the list of provider class names is empty
         * @throws IllegalStateException
         *         If the providers for the service type have already been
         *         declared
         */
        public Builder provides(String service, List<String> providers) {
            Provides p = new Provides(service, providers);

            // check providers after the set has been copied.
            List<String> providerNames = p.providers();
            if (providerNames.isEmpty())
                throw new IllegalArgumentException("Empty providers set");
            if (strict) {
                requireServiceTypeName(p.service());
                providerNames.forEach(Checks::requireServiceProviderName);
            } else {
                // Disallow service/providers in unnamed package
                String pn = packageName(service);
                if (pn.isEmpty()) {
                    throw new IllegalArgumentException(service
                                                       + ": unnamed package");
                }
                for (String name : providerNames) {
                    pn = packageName(name);
                    if (pn.isEmpty()) {
                        throw new IllegalArgumentException(name
                                                           + ": unnamed package");
                    }
                }
            }
            return provides(p);
        }

        /**
         * Adds packages to the module. All packages in the set of package names
         * that are not in the module are added to module.
         *
         * @param  pns
         *         The (possibly empty) set of package names
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If any of the package names is {@code null} or is not a
         *         legal package name
         */
        public Builder packages(Set<String> pns) {
            if (strict) {
                pns = new HashSet<>(pns);
                pns.forEach(Checks::requirePackageName);
            }
            this.packages.addAll(pns);
            return this;
        }

        /**
         * Sets the module version.
         *
         * @param  v
         *         The version
         *
         * @return This builder
         */
        public Builder version(Version v) {
            version = requireNonNull(v);
            rawVersionString = null;
            return this;
        }

        /**
         * Sets the module version.
         *
         * @param  vs
         *         The version string to parse
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If {@code vs} is {@code null} or cannot be parsed as a
         *         version string
         *
         * @see Version#parse(String)
         */
        public Builder version(String vs) {
            try {
                version = Version.parse(vs);
                rawVersionString = null;
            } catch (IllegalArgumentException e) {
                if (strict) throw e;
                version = null;
                rawVersionString = vs;
            }
            return this;
        }

        /**
         * Sets the module main class. The package for the main class is added
         * to the module if not already added. In other words, this method is
         * equivalent to first invoking this builder's {@link #packages(Set)
         * packages} method to add the package name of the main class.
         *
         * @param  mc
         *         The module main class
         *
         * @return This builder
         *
         * @throws IllegalArgumentException
         *         If {@code mainClass} is {@code null} or not a qualified
         *         name of a class in a named package
         */
        public Builder mainClass(String mc) {
            String pn;
            if (strict) {
                mc = requireQualifiedClassName("main class name", mc);
                pn = packageName(mc);
                assert !pn.isEmpty();
            } else {
                // Disallow main class in unnamed package
                pn = packageName(mc);
                if (pn.isEmpty()) {
                    throw new IllegalArgumentException(mc + ": unnamed package");
                }
            }
            packages.add(pn);
            mainClass = mc;
            return this;
        }

        /**
         * Builds and returns a {@code ModuleDescriptor} from its components.
         *
         * <p> The module will require "{@code java.base}" even if the dependence
         * has not been declared (the exception is when building a module named
         * "{@code java.base}" as it cannot require itself). The dependence on
         * "{@code java.base}" will have the {@link
         * java.lang.module.ModuleDescriptor.Requires.Modifier#MANDATED MANDATED}
         * modifier if the dependence was not declared. </p>
         *
         * @return The module descriptor
         */
        public ModuleDescriptor build() {
            Set<Requires> requires = new HashSet<>(this.requires.values());
            Set<Exports> exports = new HashSet<>(this.exports.values());
            Set<Opens> opens = new HashSet<>(this.opens.values());

            // add dependency on java.base
            if (strict
                    && !name.equals("java.base")
                    && !this.requires.containsKey("java.base")) {
                requires.add(new Requires(Set.of(Requires.Modifier.MANDATED),
                                          "java.base",
                                          null,
                                          null));
            }

            Set<Provides> provides = new HashSet<>(this.provides.values());

            return new ModuleDescriptor(name,
                                        version,
                                        rawVersionString,
                                        modifiers,
                                        requires,
                                        exports,
                                        opens,
                                        uses,
                                        provides,
                                        packages,
                                        mainClass);
        }

    }

    /**
     * Compares this module descriptor to another.
     *
     * <p> Two {@code ModuleDescriptor} objects are compared by comparing their
     * module names lexicographically. Where the module names are equal then the
     * module versions are compared. When comparing the module versions then a
     * module descriptor with a version is considered to succeed a module
     * descriptor that does not have a version. If both versions are {@linkplain
     * Version#parse(String) unparseable} then the {@linkplain #rawVersion()
     * raw version strings} are compared lexicographically. Where the module names
     * are equal and the versions are equal (or not present in both), then the
     * set of modifiers are compared. Sets of modifiers are compared by comparing
     * a <em>binary value</em> computed for each set. If a modifier is present
     * in the set then the bit at the position of its ordinal is {@code 1}
     * in the binary value, otherwise {@code 0}. If the two set of modifiers
     * are also equal then the other components of the module descriptors are
     * compared in a manner that is consistent with {@code equals}. </p>
     *
     * @param  that
     *         The module descriptor to compare
     *
     * @return A negative integer, zero, or a positive integer if this module
     *         descriptor is less than, equal to, or greater than the given
     *         module descriptor
     */
    @Override
    public int compareTo(ModuleDescriptor that) {
        if (this == that) return 0;

        int c = this.name().compareTo(that.name());
        if (c != 0) return c;

        c = compare(this.version, that.version);
        if (c != 0) return c;

        c = compare(this.rawVersionString, that.rawVersionString);
        if (c != 0) return c;

        long v1 = modsValue(this.modifiers());
        long v2 = modsValue(that.modifiers());
        c = Long.compare(v1, v2);
        if (c != 0) return c;

        c = compare(this.requires, that.requires);
        if (c != 0) return c;

        c = compare(this.packages, that.packages);
        if (c != 0) return c;

        c = compare(this.exports, that.exports);
        if (c != 0) return c;

        c = compare(this.opens, that.opens);
        if (c != 0) return c;

        c = compare(this.uses, that.uses);
        if (c != 0) return c;

        c = compare(this.provides, that.provides);
        if (c != 0) return c;

        c = compare(this.mainClass, that.mainClass);
        if (c != 0) return c;

        return 0;
    }

    /**
     * Tests this module descriptor for equality with the given object.
     *
     * <p> If the given object is not a {@code ModuleDescriptor} then this
     * method returns {@code false}. Two module descriptors are equal if each
     * of their corresponding components is equal. </p>
     *
     * <p> This method satisfies the general contract of the {@link
     * java.lang.Object#equals(Object) Object.equals} method. </p>
     *
     * @param   ob
     *          the object to which this object is to be compared
     *
     * @return  {@code true} if, and only if, the given object is a module
     *          descriptor that is equal to this module descriptor
     */
    @Override
    public boolean equals(Object ob) {
        if (ob == this)
            return true;
        if (!(ob instanceof ModuleDescriptor))
            return false;
        ModuleDescriptor that = (ModuleDescriptor)ob;
        return (name.equals(that.name)
                && modifiers.equals(that.modifiers)
                && requires.equals(that.requires)
                && Objects.equals(packages, that.packages)
                && exports.equals(that.exports)
                && opens.equals(that.opens)
                && uses.equals(that.uses)
                && provides.equals(that.provides)
                && Objects.equals(version, that.version)
                && Objects.equals(rawVersionString, that.rawVersionString)
                && Objects.equals(mainClass, that.mainClass));
    }

    /**
     * Computes a hash code for this module descriptor.
     *
     * <p> The hash code is based upon the components of the module descriptor,
     * and satisfies the general contract of the {@link Object#hashCode
     * Object.hashCode} method. </p>
     *
     * @return The hash-code value for this module descriptor
     */
    @Override
    public int hashCode() {
        int hc = hash;
        if (hc == 0) {
            hc = name.hashCode();
            hc = hc * 43 + Objects.hashCode(modifiers);
            hc = hc * 43 + requires.hashCode();
            hc = hc * 43 + Objects.hashCode(packages);
            hc = hc * 43 + exports.hashCode();
            hc = hc * 43 + opens.hashCode();
            hc = hc * 43 + uses.hashCode();
            hc = hc * 43 + provides.hashCode();
            hc = hc * 43 + Objects.hashCode(version);
            hc = hc * 43 + Objects.hashCode(rawVersionString);
            hc = hc * 43 + Objects.hashCode(mainClass);
            if (hc == 0)
                hc = -1;
            hash = hc;
        }
        return hc;
    }
    private transient int hash;  // cached hash code

    /**
     * <p> Returns a string describing the module. </p>
     *
     * @return A string describing the module
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (isOpen())
            sb.append("open ");
        sb.append("module { name: ").append(toNameAndVersion());
        if (!requires.isEmpty())
            sb.append(", ").append(requires);
        if (!uses.isEmpty())
            sb.append(", uses: ").append(uses);
        if (!exports.isEmpty())
            sb.append(", exports: ").append(exports);
        if (!opens.isEmpty())
            sb.append(", opens: ").append(opens);
        if (!provides.isEmpty()) {
            sb.append(", provides: ").append(provides);
        }
        sb.append(" }");
        return sb.toString();
    }


    /**
     * Instantiates a builder to build a module descriptor.
     *
     * @param  name
     *         The module name
     * @param  ms
     *         The set of module modifiers
     *
     * @return A new builder
     *
     * @throws IllegalArgumentException
     *         If the module name is {@code null} or is not a legal module
     *         name, or the set of modifiers contains {@link
     *         Modifier#AUTOMATIC AUTOMATIC} with other modifiers
     */
    public static Builder newModule(String name, Set<Modifier> ms) {
        Set<Modifier> mods = new HashSet<>(ms);
        if (mods.contains(Modifier.AUTOMATIC) && mods.size() > 1)
            throw new IllegalArgumentException("AUTOMATIC cannot be used with"
                                               + " other modifiers");

        return new Builder(name, true, mods);
    }

    /**
     * Instantiates a builder to build a module descriptor for a <em>normal</em>
     * module. This method is equivalent to invoking {@link #newModule(String,Set)
     * newModule} with an empty set of {@link ModuleDescriptor.Modifier modifiers}.
     *
     * @param  name
     *         The module name
     *
     * @return A new builder
     *
     * @throws IllegalArgumentException
     *         If the module name is {@code null} or is not a legal module
     *         name
     */
    public static Builder newModule(String name) {
        return new Builder(name, true, Set.of());
    }

    /**
     * Instantiates a builder to build a module descriptor for an open module.
     * This method is equivalent to invoking {@link #newModule(String,Set)
     * newModule} with the {@link ModuleDescriptor.Modifier#OPEN OPEN} modifier.
     *
     * <p> The builder for an open module cannot be used to declare any open
     * packages. </p>
     *
     * @param  name
     *         The module name
     *
     * @return A new builder that builds an open module
     *
     * @throws IllegalArgumentException
     *         If the module name is {@code null} or is not a legal module
     *         name
     */
    public static Builder newOpenModule(String name) {
        return new Builder(name, true, Set.of(Modifier.OPEN));
    }

    /**
     * Instantiates a builder to build a module descriptor for an automatic
     * module. This method is equivalent to invoking {@link #newModule(String,Set)
     * newModule} with the {@link ModuleDescriptor.Modifier#AUTOMATIC AUTOMATIC}
     * modifier.
     *
     * <p> The builder for an automatic module cannot be used to declare module
     * or service dependences. It also cannot be used to declare any exported
     * or open packages. </p>
     *
     * @param  name
     *         The module name
     *
     * @return A new builder that builds an automatic module
     *
     * @throws IllegalArgumentException
     *         If the module name is {@code null} or is not a legal module
     *         name
     *
     * @see ModuleFinder#of(Path[])
     */
    public static Builder newAutomaticModule(String name) {
        return new Builder(name, true, Set.of(Modifier.AUTOMATIC));
    }


    /**
     * Reads the binary form of a module declaration from an input stream
     * as a module descriptor.
     *
     * <p> If the descriptor encoded in the input stream does not indicate a
     * set of packages in the module then the {@code packageFinder} will be
     * invoked. The set of packages that the {@code packageFinder} returns
     * must include all the packages that the module exports, opens, as well
     * as the packages of the service implementations that the module provides,
     * and the package of the main class (if the module has a main class). If
     * the {@code packageFinder} throws an {@link UncheckedIOException} then
     * {@link IOException} cause will be re-thrown. </p>
     *
     * <p> If there are bytes following the module descriptor then it is
     * implementation specific as to whether those bytes are read, ignored,
     * or reported as an {@code InvalidModuleDescriptorException}. If this
     * method fails with an {@code InvalidModuleDescriptorException} or {@code
     * IOException} then it may do so after some, but not all, bytes have
     * been read from the input stream. It is strongly recommended that the
     * stream be promptly closed and discarded if an exception occurs. </p>
     *
     * @apiNote The {@code packageFinder} parameter is for use when reading
     * module descriptors from legacy module-artifact formats that do not
     * record the set of packages in the descriptor itself.
     *
     * @param  in
     *         The input stream
     * @param  packageFinder
     *         A supplier that can produce the set of packages
     *
     * @return The module descriptor
     *
     * @throws InvalidModuleDescriptorException
     *         If an invalid module descriptor is detected or the set of
     *         packages returned by the {@code packageFinder} does not include
     *         all of the packages obtained from the module descriptor
     * @throws IOException
     *         If an I/O error occurs reading from the input stream or {@code
     *         UncheckedIOException} is thrown by the package finder
     */
    public static ModuleDescriptor read(InputStream in,
                                        Supplier<Set<String>> packageFinder)
        throws IOException
    {
        return ModuleInfo.read(in, requireNonNull(packageFinder)).descriptor();
    }

    /**
     * Reads the binary form of a module declaration from an input stream as a
     * module descriptor. This method works exactly as specified by the 2-arg
     * {@link #read(InputStream,Supplier) read} method with the exception that
     * a packager finder is not used to find additional packages when the
     * module descriptor read from the stream does not indicate the set of
     * packages.
     *
     * @param  in
     *         The input stream
     *
     * @return The module descriptor
     *
     * @throws InvalidModuleDescriptorException
     *         If an invalid module descriptor is detected
     * @throws IOException
     *         If an I/O error occurs reading from the input stream
     */
    public static ModuleDescriptor read(InputStream in) throws IOException {
        return ModuleInfo.read(in, null).descriptor();
    }

    /**
     * Reads the binary form of a module declaration from a byte buffer
     * as a module descriptor.
     *
     * <p> If the descriptor encoded in the byte buffer does not indicate a
     * set of packages in the module then the {@code packageFinder} will be
     * invoked. The set of packages that the {@code packageFinder} returns
     * must include all the packages that the module exports, opens, as well
     * as the packages of the service implementations that the module provides,
     * and the package of the main class (if the module has a main class). If
     * the {@code packageFinder} throws an {@link UncheckedIOException} then
     * {@link IOException} cause will be re-thrown. </p>
     *
     * <p> The module descriptor is read from the buffer starting at index
     * {@code p}, where {@code p} is the buffer's {@link ByteBuffer#position()
     * position} when this method is invoked. Upon return the buffer's position
     * will be equal to {@code p + n} where {@code n} is the number of bytes
     * read from the buffer. </p>
     *
     * <p> If there are bytes following the module descriptor then it is
     * implementation specific as to whether those bytes are read, ignored,
     * or reported as an {@code InvalidModuleDescriptorException}. If this
     * method fails with an {@code InvalidModuleDescriptorException} then it
     * may do so after some, but not all, bytes have been read. </p>
     *
     * @apiNote The {@code packageFinder} parameter is for use when reading
     * module descriptors from legacy module-artifact formats that do not
     * record the set of packages in the descriptor itself.
     *
     * @param  bb
     *         The byte buffer
     * @param  packageFinder
     *         A supplier that can produce the set of packages
     *
     * @return The module descriptor
     *
     * @throws InvalidModuleDescriptorException
     *         If an invalid module descriptor is detected or the set of
     *         packages returned by the {@code packageFinder} does not include
     *         all of the packages obtained from the module descriptor
     */
    public static ModuleDescriptor read(ByteBuffer bb,
                                        Supplier<Set<String>> packageFinder)
    {
        return ModuleInfo.read(bb, requireNonNull(packageFinder)).descriptor();
    }

    /**
     * Reads the binary form of a module declaration from a byte buffer as a
     * module descriptor. This method works exactly as specified by the 2-arg
     * {@link #read(ByteBuffer,Supplier) read} method with the exception that a
     * packager finder is not used to find additional packages when the module
     * descriptor encoded in the buffer does not indicate the set of packages.
     *
     * @param  bb
     *         The byte buffer
     *
     * @return The module descriptor
     *
     * @throws InvalidModuleDescriptorException
     *         If an invalid module descriptor is detected
     */
    public static ModuleDescriptor read(ByteBuffer bb) {
        return ModuleInfo.read(bb, null).descriptor();
    }

    private static <K,V> Map<K,V> emptyOrUnmodifiableMap(Map<K,V> map) {
        if (map.isEmpty()) {
            return Collections.emptyMap();
        } else if (map.size() == 1) {
            Map.Entry<K, V> entry = map.entrySet().iterator().next();
            return Collections.singletonMap(entry.getKey(), entry.getValue());
        } else {
            return Collections.unmodifiableMap(map);
        }
    }

    private static <T> Set<T> emptyOrUnmodifiableSet(Set<T> set) {
        if (set.isEmpty()) {
            return Collections.emptySet();
        } else if (set.size() == 1) {
            return Collections.singleton(set.iterator().next());
        } else {
            return Collections.unmodifiableSet(set);
        }
    }

    private static String packageName(String cn) {
        int index = cn.lastIndexOf('.');
        return (index == -1) ? "" : cn.substring(0, index);
    }

    /**
     * Returns a string containing the given set of modifiers and label.
     */
    private static <M> String toString(Set<M> mods, String what) {
        return (Stream.concat(mods.stream().map(e -> e.toString()
                                                      .toLowerCase(Locale.ROOT)),
                              Stream.of(what)))
                .collect(Collectors.joining(" "));
    }

    private static <T extends Object & Comparable<? super T>>
    int compare(T obj1, T obj2) {
        if (obj1 != null) {
            return (obj2 != null) ? obj1.compareTo(obj2) : 1;
        } else {
            return (obj2 == null) ? 0 : -1;
        }
    }

    /**
     * Compares two sets of {@code Comparable} objects.
     */
    @SuppressWarnings("unchecked")
    private static <T extends Object & Comparable<? super T>>
    int compare(Set<T> s1, Set<T> s2) {
        T[] a1 = (T[]) s1.toArray();
        T[] a2 = (T[]) s2.toArray();
        Arrays.sort(a1);
        Arrays.sort(a2);
        return Arrays.compare(a1, a2);
    }

    private static <E extends Enum<E>> long modsValue(Set<E> set) {
        long value = 0;
        for (Enum<E> e : set) {
            value += 1 << e.ordinal();
        }
        return value;
    }

    static {
        /**
         * Setup the shared secret to allow code in other packages access
         * private package methods in java.lang.module.
         */
        jdk.internal.misc.SharedSecrets
            .setJavaLangModuleAccess(new jdk.internal.misc.JavaLangModuleAccess() {
                @Override
                public Builder newModuleBuilder(String mn,
                                                boolean strict,
                                                Set<ModuleDescriptor.Modifier> modifiers) {
                    return new Builder(mn, strict, modifiers);
                }

                @Override
                public Set<String> packages(ModuleDescriptor.Builder builder) {
                    return builder.packages();
                }

                @Override
                public void requires(ModuleDescriptor.Builder builder,
                                     Set<Requires.Modifier> ms,
                                     String mn,
                                     String rawCompiledVersion) {
                    builder.requires(ms, mn, rawCompiledVersion);
                }

                @Override
                public Requires newRequires(Set<Requires.Modifier> ms, String mn, Version v) {
                    return new Requires(ms, mn, v, true);
                }

                @Override
                public Exports newExports(Set<Exports.Modifier> ms, String source) {
                    return new Exports(ms, source, Collections.emptySet(), true);
                }

                @Override
                public Exports newExports(Set<Exports.Modifier> ms,
                                          String source,
                                          Set<String> targets) {
                    return new Exports(ms, source, targets, true);
                }

                @Override
                public Opens newOpens(Set<Opens.Modifier> ms,
                                      String source,
                                      Set<String> targets) {
                    return new Opens(ms, source, targets, true);
                }

                @Override
                public Opens newOpens(Set<Opens.Modifier> ms, String source) {
                    return new Opens(ms, source, Collections.emptySet(), true);
                }

                @Override
                public Provides newProvides(String service, List<String> providers) {
                    return new Provides(service, providers, true);
                }

                @Override
                public ModuleDescriptor newModuleDescriptor(String name,
                                                            Version version,
                                                            Set<ModuleDescriptor.Modifier> modifiers,
                                                            Set<Requires> requires,
                                                            Set<Exports> exports,
                                                            Set<Opens> opens,
                                                            Set<String> uses,
                                                            Set<Provides> provides,
                                                            Set<String> packages,
                                                            String mainClass,
                                                            int hashCode) {
                    return new ModuleDescriptor(name,
                                                version,
                                                modifiers,
                                                requires,
                                                exports,
                                                opens,
                                                uses,
                                                provides,
                                                packages,
                                                mainClass,
                                                hashCode,
                                                false);
                }

                @Override
                public Configuration resolveAndBind(ModuleFinder finder,
                                                    Collection<String> roots,
                                                    PrintStream traceOutput)
                {
                    return Configuration.resolveAndBind(finder, roots, traceOutput);
                }

                @Override
                public Configuration newConfiguration(ModuleFinder finder,
                                                      Map<String, Set<String>> graph) {
                    return new Configuration(finder, graph);
                }
            });
    }

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\ModuleFinder.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.module;

import java.nio.file.Path;
import java.security.AccessController;
import java.security.Permission;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import jdk.internal.module.ModulePath;
import jdk.internal.module.SystemModuleFinders;

/**
 * A finder of modules. A {@code ModuleFinder} is used to find modules during
 * <a href="package-summary.html#resolution">resolution</a> or
 * <a href="Configuration.html#service-binding">service binding</a>.
 *
 * <p> A {@code ModuleFinder} can only find one module with a given name. A
 * {@code ModuleFinder} that finds modules in a sequence of directories, for
 * example, will locate the first occurrence of a module of a given name and
 * will ignore other modules of that name that appear in directories later in
 * the sequence. </p>
 *
 * <p> Example usage: </p>
 *
 * <pre>{@code
 *     Path dir1, dir2, dir3;
 *
 *     ModuleFinder finder = ModuleFinder.of(dir1, dir2, dir3);
 *
 *     Optional<ModuleReference> omref = finder.find("jdk.foo");
 *     omref.ifPresent(mref -> ... );
 *
 * }</pre>
 *
 * <p> The {@link #find(String) find} and {@link #findAll() findAll} methods
 * defined here can fail for several reasons. These include I/O errors, errors
 * detected parsing a module descriptor ({@code module-info.class}), or in the
 * case of {@code ModuleFinder} returned by {@link #of ModuleFinder.of}, that
 * two or more modules with the same name are found in a directory.
 * When an error is detected then these methods throw {@link FindException
 * FindException} with an appropriate {@link Throwable#getCause cause}.
 * The behavior of a {@code ModuleFinder} after a {@code FindException} is
 * thrown is undefined. For example, invoking {@code find} after an exception
 * is thrown may or may not scan the same modules that lead to the exception.
 * It is recommended that a module finder be discarded after an exception is
 * thrown. </p>
 *
 * <p> A {@code ModuleFinder} is not required to be thread safe. </p>
 *
 * @since 9
 * @spec JPMS
 */

public interface ModuleFinder {

    /**
     * Finds a reference to a module of a given name.
     *
     * <p> A {@code ModuleFinder} provides a consistent view of the
     * modules that it locates. If {@code find} is invoked several times to
     * locate the same module (by name) then it will return the same result
     * each time. If a module is located then it is guaranteed to be a member
     * of the set of modules returned by the {@link #findAll() findAll}
     * method. </p>
     *
     * @param  name
     *         The name of the module to find
     *
     * @return A reference to a module with the given name or an empty
     *         {@code Optional} if not found
     *
     * @throws FindException
     *         If an error occurs finding the module
     *
     * @throws SecurityException
     *         If denied by the security manager
     */
    Optional<ModuleReference> find(String name);

    /**
     * Returns the set of all module references that this finder can locate.
     *
     * <p> A {@code ModuleFinder} provides a consistent view of the modules
     * that it locates. If {@link #findAll() findAll} is invoked several times
     * then it will return the same (equals) result each time. For each {@code
     * ModuleReference} element in the returned set then it is guaranteed that
     * {@link #find find} will locate the {@code ModuleReference} if invoked
     * to find that module. </p>
     *
     * @apiNote This is important to have for methods such as {@link
     * Configuration#resolveAndBind resolveAndBind} that need to scan the
     * module path to find modules that provide a specific service.
     *
     * @return The set of all module references that this finder locates
     *
     * @throws FindException
     *         If an error occurs finding all modules
     *
     * @throws SecurityException
     *         If denied by the security manager
     */
    Set<ModuleReference> findAll();

    /**
     * Returns a module finder that locates the <em>system modules</em>. The
     * system modules are the modules in the Java run-time image.
     * The module finder will always find {@code java.base}.
     *
     * <p> If there is a security manager set then its {@link
     * SecurityManager#checkPermission(Permission) checkPermission} method is
     * invoked to check that the caller has been granted
     * {@link RuntimePermission RuntimePermission("accessSystemModules")}
     * to access the system modules. </p>
     *
     * @return A {@code ModuleFinder} that locates the system modules
     *
     * @throws SecurityException
     *         If denied by the security manager
     */
    static ModuleFinder ofSystem() {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new RuntimePermission("accessSystemModules"));
            PrivilegedAction<ModuleFinder> pa = SystemModuleFinders::ofSystem;
            return AccessController.doPrivileged(pa);
        } else {
            return SystemModuleFinders.ofSystem();
        }
    }

    /**
     * Returns a module finder that locates modules on the file system by
     * searching a sequence of directories and/or packaged modules.
     *
     * Each element in the given array is one of:
     * <ol>
     *     <li><p> A path to a directory of modules.</p></li>
     *     <li><p> A path to the <em>top-level</em> directory of an
     *         <em>exploded module</em>. </p></li>
     *     <li><p> A path to a <em>packaged module</em>. </p></li>
     * </ol>
     *
     * The module finder locates modules by searching each directory, exploded
     * module, or packaged module in array index order. It finds the first
     * occurrence of a module with a given name and ignores other modules of
     * that name that appear later in the sequence.
     *
     * <p> If an element is a path to a directory of modules then each entry in
     * the directory is a packaged module or the top-level directory of an
     * exploded module. It is an error if a directory contains more than one
     * module with the same name. If an element is a path to a directory, and
     * that directory contains a file named {@code module-info.class}, then the
     * directory is treated as an exploded module rather than a directory of
     * modules. </p>
     *
     * <p id="automatic-modules"> The module finder returned by this method
     * supports modules packaged as JAR files. A JAR file with a {@code
     * module-info.class} in its top-level directory, or in a versioned entry
     * in a {@linkplain java.util.jar.JarFile#isMultiRelease() multi-release}
     * JAR file, is a modular JAR file and thus defines an <em>explicit</em>
     * module. A JAR file that does not have a {@code module-info.class} in its
     * top-level directory defines an <em>automatic module</em>, as follows:
     * </p>
     *
     * <ul>
     *
     *     <li><p> If the JAR file has the attribute "{@code Automatic-Module-Name}"
     *     in its main manifest then its value is the {@linkplain
     *     ModuleDescriptor#name() module name}. The module name is otherwise
     *     derived from the name of the JAR file. </p></li>
     *
     *     <li><p> The {@link ModuleDescriptor#version() version}, and the
     *     module name when the attribute "{@code Automatic-Module-Name}" is not
     *     present, are derived from the file name of the JAR file as follows: </p>
     *
     *     <ul>
     *
     *         <li><p> The "{@code .jar}" suffix is removed. </p></li>
     *
     *         <li><p> If the name matches the regular expression {@code
     *         "-(\\d+(\\.|$))"} then the module name will be derived from the
     *         subsequence preceding the hyphen of the first occurrence. The
     *         subsequence after the hyphen is parsed as a {@link
     *         ModuleDescriptor.Version Version} and ignored if it cannot be
     *         parsed as a {@code Version}. </p></li>
     *
     *         <li><p> All non-alphanumeric characters ({@code [^A-Za-z0-9]})
     *         in the module name are replaced with a dot ({@code "."}), all
     *         repeating dots are replaced with one dot, and all leading and
     *         trailing dots are removed. </p></li>
     *
     *         <li><p> As an example, a JAR file named "{@code foo-bar.jar}" will
     *         derive a module name "{@code foo.bar}" and no version. A JAR file
     *         named "{@code foo-bar-1.2.3-SNAPSHOT.jar}" will derive a module
     *         name "{@code foo.bar}" and "{@code 1.2.3-SNAPSHOT}" as the version.
     *         </p></li>
     *
     *     </ul></li>
     *
     *     <li><p> The set of packages in the module is derived from the
     *     non-directory entries in the JAR file that have names ending in
     *     "{@code .class}". A candidate package name is derived from the name
     *     using the characters up to, but not including, the last forward slash.
     *     All remaining forward slashes are replaced with dot ({@code "."}). If
     *     the resulting string is a legal package name then it is assumed to be
     *     a package name. For example, if the JAR file contains the entry
     *     "{@code p/q/Foo.class}" then the package name derived is
     *     "{@code p.q}".</p></li>
     *
     *     <li><p> The contents of entries starting with {@code
     *     META-INF/services/} are assumed to be service configuration files
     *     (see {@link java.util.ServiceLoader}). If the name of a file
     *     (that follows {@code META-INF/services/}) is a legal class name
     *     then it is assumed to be the fully-qualified class name of a service
     *     type. The entries in the file are assumed to be the fully-qualified
     *     class names of provider classes. </p></li>
     *
     *     <li><p> If the JAR file has a {@code Main-Class} attribute in its
     *     main manifest, its value is a legal class name, and its package is
     *     in the set of packages derived for the module, then the value is the
     *     module {@linkplain ModuleDescriptor#mainClass() main class}. </p></li>
     *
     * </ul>
     *
     * <p> If a {@code ModuleDescriptor} cannot be created (by means of the
     * {@link ModuleDescriptor.Builder ModuleDescriptor.Builder} API) for an
     * automatic module then {@code FindException} is thrown. This can arise
     * when the value of the "{@code Automatic-Module-Name}" attribute is not a
     * legal module name, a legal module name cannot be derived from the file
     * name of the JAR file, where the JAR file contains a {@code .class} in
     * the top-level directory of the JAR file, where an entry in a service
     * configuration file is not a legal class name or its package name is not
     * in the set of packages derived for the module. </p>
     *
     * <p> In addition to JAR files, an implementation may also support modules
     * that are packaged in other implementation specific module formats. If
     * an element in the array specified to this method is a path to a directory
     * of modules then entries in the directory that not recognized as modules
     * are ignored. If an element in the array is a path to a packaged module
     * that is not recognized then a {@code FindException} is thrown when the
     * file is encountered. Paths to files that do not exist are always ignored.
     * </p>
     *
     * <p> As with automatic modules, the contents of a packaged or exploded
     * module may need to be <em>scanned</em> in order to determine the packages
     * in the module. Whether {@linkplain java.nio.file.Files#isHidden(Path)
     * hidden files} are ignored or not is implementation specific and therefore
     * not specified. If a {@code .class} file (other than {@code
     * module-info.class}) is found in the top-level directory then it is
     * assumed to be a class in the unnamed package and so {@code FindException}
     * is thrown. </p>
     *
     * <p> Finders created by this method are lazy and do not eagerly check
     * that the given file paths are directories or packaged modules.
     * Consequently, the {@code find} or {@code findAll} methods will only
     * fail if invoking these methods results in searching a directory or
     * packaged module and an error is encountered. </p>
     *
     * @param entries
     *        A possibly-empty array of paths to directories of modules
     *        or paths to packaged or exploded modules
     *
     * @return A {@code ModuleFinder} that locates modules on the file system
     */
    static ModuleFinder of(Path... entries) {
        // special case zero entries
        if (entries.length == 0) {
            return new ModuleFinder() {
                @Override
                public Optional<ModuleReference> find(String name) {
                    Objects.requireNonNull(name);
                    return Optional.empty();
                }

                @Override
                public Set<ModuleReference> findAll() {
                    return Collections.emptySet();
                }
            };
        }

        return ModulePath.of(entries);
    }

    /**
     * Returns a module finder that is composed from a sequence of zero or more
     * module finders. The {@link #find(String) find} method of the resulting
     * module finder will locate a module by invoking the {@code find} method
     * of each module finder, in array index order, until either the module is
     * found or all module finders have been searched. The {@link #findAll()
     * findAll} method of the resulting module finder will return a set of
     * modules that includes all modules located by the first module finder.
     * The set of modules will include all modules located by the second or
     * subsequent module finder that are not located by previous module finders
     * in the sequence.
     *
     * <p> When locating modules then any exceptions or errors thrown by the
     * {@code find} or {@code findAll} methods of the underlying module finders
     * will be propagated to the caller of the resulting module finder's
     * {@code find} or {@code findAll} methods. </p>
     *
     * @param finders
     *        The array of module finders
     *
     * @return A {@code ModuleFinder} that composes a sequence of module finders
     */
    static ModuleFinder compose(ModuleFinder... finders) {
        // copy the list and check for nulls
        final List<ModuleFinder> finderList = List.of(finders);

        return new ModuleFinder() {
            private final Map<String, ModuleReference> nameToModule = new HashMap<>();
            private Set<ModuleReference> allModules;

            @Override
            public Optional<ModuleReference> find(String name) {
                // cached?
                ModuleReference mref = nameToModule.get(name);
                if (mref != null)
                    return Optional.of(mref);
                Optional<ModuleReference> omref = finderList.stream()
                        .map(f -> f.find(name))
                        .flatMap(Optional::stream)
                        .findFirst();
                omref.ifPresent(m -> nameToModule.put(name, m));
                return omref;
            }

            @Override
            public Set<ModuleReference> findAll() {
                if (allModules != null)
                    return allModules;
                // seed with modules already found
                Set<ModuleReference> result = new HashSet<>(nameToModule.values());
                finderList.stream()
                          .flatMap(f -> f.findAll().stream())
                          .forEach(mref -> {
                              String name = mref.descriptor().name();
                              if (nameToModule.putIfAbsent(name, mref) == null) {
                                  result.add(mref);
                              }
                          });
                allModules = Collections.unmodifiableSet(result);
                return allModules;
            }
        };
    }

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\ModuleReader.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2015, 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.module;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;


/**
 * Provides access to the content of a module.
 *
 * <p> A module reader is intended for cases where access to the resources in a
 * module is required, regardless of whether the module has been loaded.
 * A framework that scans a collection of packaged modules on the file system,
 * for example, may use a module reader to access a specific resource in each
 * module. A module reader is also intended to be used by {@code ClassLoader}
 * implementations that load classes and resources from modules. </p>
 *
 * <p> A resource in a module is identified by an abstract name that is a
 * '{@code /}'-separated path string. For example, module {@code java.base} may
 * have a resource "{@code java/lang/Object.class}" that, by convention, is the
 * class file for {@code java.lang.Object}. A module reader may treat
 * directories in the module content as resources (whether it does or not is
 * module reader specific). Where the module content contains a directory
 * that can be located as a resource then its name ends with a slash ('/'). The
 * directory can also be located with a name that drops the trailing slash. </p>
 *
 * <p> A {@code ModuleReader} is {@linkplain ModuleReference#open open} upon
 * creation and is closed by invoking the {@link #close close} method.  Failure
 * to close a module reader may result in a resource leak.  The {@code
 * try-with-resources} statement provides a useful construct to ensure that
 * module readers are closed. </p>
 *
 * <p> A {@code ModuleReader} implementation may require permissions to access
 * resources in the module. Consequently the {@link #find find}, {@link #open
 * open}, {@link #read read}, and {@link #list list} methods may throw {@code
 * SecurityException} if access is denied by the security manager. </p>
 *
 * @implSpec Implementations of {@code ModuleReader} should take great care
 * when translating an abstract resource name to the location of a resource in
 * a packaged module or on the file system. Implementations are advised to
 * treat resource names with elements such as '{@code .},  '{@code ..}',
 * elements containing file separators, or empty elements as "not found". More
 * generally, if the resource name is not in the stream of elements that the
 * {@code list} method returns then the resource should be treated as "not
 * found" to avoid inconsistencies.
 *
 * @see ModuleReference
 * @since 9
 * @spec JPMS
 */

public interface ModuleReader extends Closeable {

    /**
     * Finds a resource, returning a URI to the resource in the module.
     *
     * <p> If the module reader can determine that the name locates a directory
     * then the resulting URI will end with a slash ('/'). </p>
     *
     * @param  name
     *         The name of the resource to open for reading
     *
     * @return A URI to the resource; an empty {@code Optional} if the resource
     *         is not found or a URI cannot be constructed to locate the
     *         resource
     *
     * @throws IOException
     *         If an I/O error occurs or the module reader is closed
     * @throws SecurityException
     *         If denied by the security manager
     *
     * @see ClassLoader#getResource(String)
     */
    Optional<URI> find(String name) throws IOException;

    /**
     * Opens a resource, returning an input stream to read the resource in
     * the module.
     *
     * <p> The behavior of the input stream when used after the module reader
     * is closed is implementation specific and therefore not specified. </p>
     *
     * @implSpec The default implementation invokes the {@link #find(String)
     * find} method to get a URI to the resource. If found, then it attempts
     * to construct a {@link java.net.URL URL} and open a connection to the
     * resource.
     *
     * @param  name
     *         The name of the resource to open for reading
     *
     * @return An input stream to read the resource or an empty
     *         {@code Optional} if not found
     *
     * @throws IOException
     *         If an I/O error occurs or the module reader is closed
     * @throws SecurityException
     *         If denied by the security manager
     */
    default Optional<InputStream> open(String name) throws IOException {
        Optional<URI> ouri = find(name);
        if (ouri.isPresent()) {
            return Optional.of(ouri.get().toURL().openStream());
        } else {
            return Optional.empty();
        }
    }

    /**
     * Reads a resource, returning a byte buffer with the contents of the
     * resource.
     *
     * The element at the returned buffer's position is the first byte of the
     * resource, the element at the buffer's limit is the last byte of the
     * resource. Once consumed, the {@link #release(ByteBuffer) release} method
     * must be invoked. Failure to invoke the {@code release} method may result
     * in a resource leak.
     *
     * @apiNote This method is intended for high-performance class loading. It
     * is not capable (or intended) to read arbitrary large resources that
     * could potentially be 2GB or larger. The rationale for using this method
     * in conjunction with the {@code release} method is to allow module reader
     * implementations manage buffers in an efficient manner.
     *
     * @implSpec The default implementation invokes the {@link #open(String)
     * open} method and reads all bytes from the input stream into a byte
     * buffer.
     *
     * @param  name
     *         The name of the resource to read
     *
     * @return A byte buffer containing the contents of the resource or an
     *         empty {@code Optional} if not found
     *
     * @throws IOException
     *         If an I/O error occurs or the module reader is closed
     * @throws SecurityException
     *         If denied by the security manager
     * @throws OutOfMemoryError
     *         If the resource is larger than {@code Integer.MAX_VALUE},
     *         the maximum capacity of a byte buffer
     *
     * @see ClassLoader#defineClass(String, ByteBuffer, java.security.ProtectionDomain)
     */
    default Optional<ByteBuffer> read(String name) throws IOException {
        Optional<InputStream> oin = open(name);
        if (oin.isPresent()) {
            try (InputStream in = oin.get()) {
                return Optional.of(ByteBuffer.wrap(in.readAllBytes()));
            }
        } else {
            return Optional.empty();
        }
    }

    /**
     * Release a byte buffer. This method should be invoked after consuming
     * the contents of the buffer returned by the {@code read} method.
     * The behavior of this method when invoked to release a buffer that has
     * already been released, or the behavior when invoked to release a buffer
     * after a {@code ModuleReader} is closed is implementation specific and
     * therefore not specified.
     *
     * @param  bb
     *         The byte buffer to release
     *
     * @implSpec The default implementation doesn't do anything except check
     * if the byte buffer is null.
     */
    default void release(ByteBuffer bb) {
        Objects.requireNonNull(bb);
    }

    /**
     * Lists the contents of the module, returning a stream of elements that
     * are the names of all resources in the module. Whether the stream of
     * elements includes names corresponding to directories in the module is
     * module reader specific.
     *
     * <p> In lazy implementations then an {@code IOException} may be thrown
     * when using the stream to list the module contents. If this occurs then
     * the {@code IOException} will be wrapped in an {@link
     * java.io.UncheckedIOException} and thrown from the method that caused the
     * access to be attempted. {@code SecurityException} may also be thrown
     * when using the stream to list the module contents and access is denied
     * by the security manager. </p>
     *
     * <p> The behavior of the stream when used after the module reader is
     * closed is implementation specific and therefore not specified. </p>
     *
     * @return A stream of elements that are the names of all resources
     *         in the module
     *
     * @throws IOException
     *         If an I/O error occurs or the module reader is closed
     * @throws SecurityException
     *         If denied by the security manager
     */
    Stream<String> list() throws IOException;

    /**
     * Closes the module reader. Once closed then subsequent calls to locate or
     * read a resource will fail by throwing {@code IOException}.
     *
     * <p> A module reader is not required to be asynchronously closeable. If a
     * thread is reading a resource and another thread invokes the close method,
     * then the second thread may block until the read operation is complete. </p>
     */
    @Override
    void close() throws IOException;

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\ModuleReference.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2014, 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.module;

import java.io.IOException;
import java.net.URI;
import java.util.Objects;
import java.util.Optional;


/**
 * A reference to a module's content.
 *
 * <p> A module reference is a concrete implementation of this class that
 * implements the abstract methods defined by this class. It contains the
 * module's descriptor and its location, if known.  It also has the ability to
 * create a {@link ModuleReader} in order to access the module's content, which
 * may be inside the Java run-time system itself or in an artifact such as a
 * modular JAR file.
 *
 * @see ModuleFinder
 * @see ModuleReader
 * @since 9
 * @spec JPMS
 */

public abstract class ModuleReference {

    private final ModuleDescriptor descriptor;
    private final URI location;

    /**
     * Constructs a new instance of this class.
     *
     * @param descriptor
     *        The module descriptor
     * @param location
     *        The module location or {@code null} if not known
     */
    protected ModuleReference(ModuleDescriptor descriptor, URI location) {
        this.descriptor = Objects.requireNonNull(descriptor);
        this.location = location;
    }

    /**
     * Returns the module descriptor.
     *
     * @return The module descriptor
     */
    public final ModuleDescriptor descriptor() {
        return descriptor;
    }

    /**
     * Returns the location of this module's content, if known.
     *
     * <p> This URI, when present, can be used as the {@linkplain
     * java.security.CodeSource#getLocation location} value of a {@link
     * java.security.CodeSource CodeSource} so that a module's classes can be
     * granted specific permissions when loaded by a {@link
     * java.security.SecureClassLoader SecureClassLoader}.
     *
     * @return The location or an empty {@code Optional} if not known
     */
    public final Optional<URI> location() {
        return Optional.ofNullable(location);
    }

    /**
     * Opens the module content for reading.
     *
     * @return A {@code ModuleReader} to read the module
     *
     * @throws IOException
     *         If an I/O error occurs
     * @throws SecurityException
     *         If denied by the security manager
     */
    public abstract ModuleReader open() throws IOException;
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\package-info.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2013, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/**
 * Classes to support module descriptors and creating configurations of modules
 * by means of resolution and service binding.
 *
 * <p> Unless otherwise noted, passing a {@code null} argument to a constructor
 * or method of any class or interface in this package will cause a {@link
 * java.lang.NullPointerException NullPointerException} to be thrown. Additionally,
 * invoking a method with an array or collection containing a {@code null} element
 * will cause a {@code NullPointerException}, unless otherwise specified. </p>
 *
 *
 * <h1><a id="resolution">{@index "Module Resolution"}</a></h1>
 *
 * <p> Resolution is the process of computing how modules depend on each other.
 * The process occurs at compile time and run time. </p>
 *
 * <p> Resolution is a two-step process. The first step recursively enumerates
 * the 'requires' directives of a set of root modules. If all the enumerated
 * modules are observable, then the second step computes their readability graph.
 * The readability graph embodies how modules depend on each other, which in
 * turn controls access across module boundaries. </p>
 *
 * <h2> Step 1: Recursive enumeration </h2>
 *
 * <p> Recursive enumeration takes a set of module names, looks up each of their
 * module declarations, and for each module declaration, recursively enumerates:
 *
 * <ul>
 *   <li> <p> the module names given by the 'requires' directives with the
 *   'transitive' modifier, and </p></li>
 *   <li> <p> at the discretion of the host system, the module names given by
 *   the 'requires' directives without the 'transitive' modifier. </p></li>
 * </ul>
 *
 * <p> Module declarations are looked up in a set of observable modules. The set
 * of observable modules is determined in an implementation specific manner. The
 * set of observable modules may include modules with explicit declarations
 * (that is, with a {@code module-info.java} source file or {@code module-info.class}
 * file) and modules with implicit declarations (that is,
 * <a href="ModuleFinder.html#automatic-modules">automatic modules</a>).
 * Because an automatic module has no explicit module declaration, it has no
 * 'requires' directives of its own, although its name may be given by a
 * 'requires' directive of an explicit module declaration. </p>

 * <p> The set of root modules, whose names are the initial input to this
 * algorithm, is determined in an implementation specific manner. The set of
 * root modules may include automatic modules. </p>
 *
 * <p> If at least one automatic module is enumerated by this algorithm, then
 * every observable automatic module must be enumerated, regardless of whether
 * any of their names are given by 'requires' directives of explicit module
 * declarations. </p>
 *
 * <p> If any of the following conditions occur, then resolution fails:
 * <ul>
 *   <li><p> Any root module is not observable. </p></li>
 *   <li><p> Any module whose name is given by a 'requires' directive with the
 *   'transitive' modifier is not observable. </p></li>
 *   <li><p> At the discretion of the host system, any module whose name is given
 *   by a 'requires' directive without the 'transitive' modifier is not
 *   observable. </p></li>
 *   <li><p> The algorithm in this step enumerates the same module name twice. This
 *   indicates a cycle in the 'requires' directives, disregarding any 'transitive'
 *   modifiers. </p></li>
 * </ul>
 *
 * <p> Otherwise, resolution proceeds to step 2. </p>
 *
 * <h2> Step 2: Computing the readability graph </h2>
 *
 * <p> A 'requires' directive (irrespective of 'transitive') expresses that
 * one module depends on some other module. The effect of the 'transitive'
 * modifier is to cause additional modules to also depend on the other module.
 * If module M 'requires transitive N', then not only does M depend on N, but
 * any module that depends on M also depends on N. This allows M to be
 * refactored so that some or all of its content can be moved to a new module N
 * without breaking modules that have a 'requires M' directive. </p>
 *
 * <p> Module dependencies are represented by the readability graph. The
 * readability graph is a directed graph whose vertices are the modules
 * enumerated in step 1 and whose edges represent readability between pairs of
 * modules. The edges are specified as follows:
 *
 * <p> First, readability is determined by the 'requires' directives of the
 * enumerated modules, disregarding any 'transitive' modifiers:
 *
 * <ul>
 *   <li><p> For each enumerated module A that 'requires' B: A "reads" B. </p></li>
 *   <li><p> For each enumerated module X that is automatic: X "reads" every
 *   other enumerated module (it is "as if" an automatic module has 'requires'
 *   directives for every other enumerated module). </p></li>
 * </ul>
 *
 * <p> Second, readability is augmented to account for 'transitive' modifiers:
 * <ul>
 *   <li> <p> For each enumerated module A that "reads" B: </p>
 *     <ul>
 *     <li><p> If B 'requires transitive' C, then A "reads" C as well as B. This
 *     augmentation is recursive: since A "reads" C, if C 'requires transitive'
 *     D, then A "reads" D as well as C and B. </p></li>
 *     <li><p> If B is an automatic module, then A "reads" every other enumerated
 *     automatic module. (It is "as if" an automatic module has 'requires transitive'
 *     directives for every other enumerated automatic module).</p> </li>
 *     </ul>
 *   </li>
 * </ul>
 *
 * <p> Finally, every module "reads" itself. </p>
 *
 * <p> If any of the following conditions occur in the readability graph, then
 * resolution fails:
 * <ul>
 *   <li><p> A module "reads" two or more modules with the same name. This includes
 *   the case where a module "reads" another with the same name as itself. </p></li>
 *   <li><p> Two or more modules export a package with the same name to a module
 *   that "reads" both. This includes the case where a module M containing package
 *   p "reads" another module that exports p to M. </p></li>
 *   <li><p> A module M declares that it 'uses p.S' or 'provides p.S with ...' but
 *   package p is neither in module M nor exported to M by any module that M
 *   "reads". </p></li>
 * </ul>
 * <p> Otherwise, resolution succeeds, and the result of resolution is the
 * readability graph.
 *
 * <h2> Root modules </h2>
 *
 * <p> The set of root modules at compile-time is usually the set of modules
 * being compiled. At run-time, the set of root modules is usually the
 * application module specified to the 'java' launcher. When compiling code in
 * the unnamed module, or at run-time when the main application class is loaded
 * from the class path, then the default set of root modules is implementation
 * specific. In the JDK the default set of root modules contains every module
 * that is observable on the upgrade module path or among the system modules,
 * and that exports at least one package without qualification. </p>
 *
 * <h2> Observable modules </h2>
 *
 * <p> The set of observable modules at both compile-time and run-time is
 * determined by searching several different paths, and also by searching
 * the compiled modules built in to the environment. The search order is as
 * follows: </p>
 *
 * <ol>
 *   <li><p> At compile time only, the compilation module path. This path
 *   contains module definitions in source form.  </p></li>
 *
 *   <li><p> The upgrade module path. This path contains compiled definitions of
 *   modules that will be observed in preference to the compiled definitions of
 *   any <i>upgradeable modules</i> that are present in (3) and (4). See the Java
 *   SE Platform for the designation of which standard modules are upgradeable.
 *   </p></li>
 *
 *   <li><p> The system modules, which are the compiled definitions built in to
 *   the environment. </p></li>
 *
 *   <li><p> The application module path. This path contains compiled definitions
 *   of library and application modules. </p></li>
 *
 * </ol>
 *
 * <h2> 'requires' directives with 'static' modifier </h2>
 *
 * <p> 'requires' directives that have the 'static' modifier express an optional
 * dependence at run time. If a module declares that it 'requires static M' then
 * resolution does not search the observable modules for M to satisfy the dependency.
 * However, if M is recursively enumerated at step 1 then all modules that are
 * enumerated and `requires static M` will read M. </p>
 *
 * <h2> Completeness </h2>
 *
 * <p> Resolution may be partial at compile-time in that the complete transitive
 * closure may not be required to compile a set of modules. Minimally, the
 * readability graph that is constructed and validated at compile-time includes
 * the modules being compiled, their direct dependences, and all implicitly
 * declared dependences (requires transitive). </p>
 *
 * <p> At run-time, resolution is an additive process. The recursive enumeration
 * at step 1 may be relative to previous resolutions so that a root module,
 * or a module named in a 'requires' directive, is not enumerated when it was
 * enumerated by a previous (or parent) resolution. The readability graph that
 * is the result of resolution may therefore have a vertex for a module enumerated
 * in step 1 but with an edge to represent that the module reads a module that
 * was enumerated by previous (or parent) resolution. </p>
 *
 * @since 9
 * @spec JPMS
 */

package java.lang.module;

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\ResolutionException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.module;

/**
 * Thrown when resolving a set of modules, or resolving a set of modules with
 * service binding, fails.
 *
 * @see Configuration
 * @since 9
 * @spec JPMS
 */
public class ResolutionException extends RuntimeException {
    private static final long serialVersionUID = -1031186845316729450L;

    /**
     * Constructs a {@code ResolutionException} with no detail message.
     */
    public ResolutionException() { }

    /**
     * Constructs a {@code ResolutionException} with the given detail
     * message.
     *
     * @param msg
     *        The detail message; can be {@code null}
     */
    public ResolutionException(String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of this exception with the given cause.
     *
     * @param cause
     *        The cause; can be {@code null}
     */
    public ResolutionException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a {@code ResolutionException} with the given detail message
     * and cause.
     *
     * @param msg
     *        The detail message; can be {@code null}
     * @param cause
     *        The cause; can be {@code null}
     */
    public ResolutionException(String msg, Throwable cause) {
        super(msg, cause);
    }

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\ResolvedModule.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.module;

import java.util.Objects;
import java.util.Set;

/**
 * A module in a graph of <em>resolved modules</em>.
 *
 * <p> {@code ResolvedModule} defines the {@link #configuration configuration}
 * method to get the configuration that the resolved module is in. It defines
 * the {@link #reference() reference} method to get the reference to the
 * module's content.
 *
 * @since 9
 * @spec JPMS
 * @see Configuration#modules()
 */
public final class ResolvedModule {

    private final Configuration cf;
    private final ModuleReference mref;

    ResolvedModule(Configuration cf, ModuleReference mref) {
        this.cf = Objects.requireNonNull(cf);
        this.mref = Objects.requireNonNull(mref);
    }

    /**
     * Returns the configuration that this resolved module is in.
     *
     * @return The configuration that this resolved module is in
     */
    public Configuration configuration() {
        return cf;
    }

    /**
     * Returns the reference to the module's content.
     *
     * @return The reference to the module's content
     */
    public ModuleReference reference() {
        return mref;
    }

    /**
     * Returns the module descriptor.
     *
     * This convenience method is the equivalent to invoking:
     * <pre> {@code
     *     reference().descriptor()
     * }</pre>
     *
     * @return The module descriptor
     */
    ModuleDescriptor descriptor() {
        return reference().descriptor();
    }

    /**
     * Returns the module name.
     *
     * This convenience method is the equivalent to invoking:
     * <pre> {@code
     *     reference().descriptor().name()
     * }</pre>
     *
     * @return The module name
     */
    public String name() {
        return reference().descriptor().name();
    }

    /**
     * Returns the set of resolved modules that this resolved module reads.
     *
     * @return A possibly-empty unmodifiable set of resolved modules that
     *         this resolved module reads
     */
    public Set<ResolvedModule> reads() {
        return cf.reads(this);
    }

    /**
     * Computes a hash code for this resolved module.
     *
     * <p> The hash code is based upon the components of the resolved module
     * and satisfies the general contract of the {@link Object#hashCode
     * Object.hashCode} method. </p>
     *
     * @return The hash-code value for this resolved module
     */
    @Override
    public int hashCode() {
        return cf.hashCode() ^ mref.hashCode();
    }

    /**
     * Tests this resolved module for equality with the given object.
     *
     * <p> If the given object is not a {@code ResolvedModule} then this
     * method returns {@code false}. Two {@code ResolvedModule} objects are
     * equal if they are in the same configuration and have equal references
     * to the module content. </p>
     *
     * <p> This method satisfies the general contract of the {@link
     * java.lang.Object#equals(Object) Object.equals} method. </p>
     *
     * @param   ob
     *          the object to which this object is to be compared
     *
     * @return  {@code true} if, and only if, the given object is a module
     *          reference that is equal to this module reference
     */
    @Override
    public boolean equals(Object ob) {
        if (!(ob instanceof ResolvedModule))
            return false;

        ResolvedModule that = (ResolvedModule) ob;
        return Objects.equals(this.cf, that.cf)
                && Objects.equals(this.mref, that.mref);
    }

    /**
     * Returns a string describing this resolved module.
     *
     * @return A string describing this resolved module
     */
    @Override
    public String toString() {
        return System.identityHashCode(cf) + "/" + name();
    }

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\module\Resolver.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2013, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang.module;

import java.io.PrintStream;
import java.lang.module.ModuleDescriptor.Provides;
import java.lang.module.ModuleDescriptor.Requires.Modifier;
import java.net.URI;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import jdk.internal.module.ModuleHashes;
import jdk.internal.module.ModuleReferenceImpl;
import jdk.internal.module.ModuleTarget;

/**
 * The resolver used by {@link Configuration#resolve} and {@link
 * Configuration#resolveAndBind}.
 *
 * @implNote The resolver is used at VM startup and so deliberately avoids
 * using lambda and stream usages in code paths used during startup.
 */

final class Resolver {

    private final ModuleFinder beforeFinder;
    private final List<Configuration> parents;
    private final ModuleFinder afterFinder;
    private final PrintStream traceOutput;

    // maps module name to module reference
    private final Map<String, ModuleReference> nameToReference = new HashMap<>();

    // true if all automatic modules have been found
    private boolean haveAllAutomaticModules;

    // constraint on target platform
    private String targetPlatform;

    String targetPlatform() { return targetPlatform; }

    /**
     * @throws IllegalArgumentException if there are more than one parent and
     *         the constraints on the target platform conflict
     */
    Resolver(ModuleFinder beforeFinder,
             List<Configuration> parents,
             ModuleFinder afterFinder,
             PrintStream traceOutput) {
        this.beforeFinder = beforeFinder;
        this.parents = parents;
        this.afterFinder = afterFinder;
        this.traceOutput = traceOutput;

        // record constraint on target platform, checking for conflicts
        for (Configuration parent : parents) {
            String value = parent.targetPlatform();
            if (value != null) {
                if (targetPlatform == null) {
                    targetPlatform = value;
                } else {
                    if (!value.equals(targetPlatform)) {
                        String msg = "Parents have conflicting constraints on target" +
                                     "  platform: " + targetPlatform + ", " + value;
                        throw new IllegalArgumentException(msg);
                    }
                }
            }
        }
    }

    /**
     * Resolves the given named modules.
     *
     * @throws ResolutionException
     */
    Resolver resolve(Collection<String> roots) {

        // create the visit stack to get us started
        Deque<ModuleDescriptor> q = new ArrayDeque<>();
        for (String root : roots) {

            // find root module
            ModuleReference mref = findWithBeforeFinder(root);
            if (mref == null) {

                if (findInParent(root) != null) {
                    // in parent, nothing to do
                    continue;
                }

                mref = findWithAfterFinder(root);
                if (mref == null) {
                    findFail("Module %s not found", root);
                }
            }

            if (isTracing()) {
                trace("root %s", nameAndInfo(mref));
            }

            addFoundModule(mref);
            q.push(mref.descriptor());
        }

        resolve(q);

        return this;
    }

    /**
     * Resolve all modules in the given queue. On completion the queue will be
     * empty and any resolved modules will be added to {@code nameToReference}.
     *
     * @return The set of module resolved by this invocation of resolve
     */
    private Set<ModuleDescriptor> resolve(Deque<ModuleDescriptor> q) {
        Set<ModuleDescriptor> resolved = new HashSet<>();

        while (!q.isEmpty()) {
            ModuleDescriptor descriptor = q.poll();
            assert nameToReference.containsKey(descriptor.name());

            // if the module is an automatic module then all automatic
            // modules need to be resolved
            if (descriptor.isAutomatic() && !haveAllAutomaticModules) {
                addFoundAutomaticModules().forEach(mref -> {
                    ModuleDescriptor other = mref.descriptor();
                    q.offer(other);
                    if (isTracing()) {
                        trace("%s requires %s", descriptor.name(), nameAndInfo(mref));
                    }
                });
                haveAllAutomaticModules = true;
            }

            // process dependences
            for (ModuleDescriptor.Requires requires : descriptor.requires()) {

                // only required at compile-time
                if (requires.modifiers().contains(Modifier.STATIC))
                    continue;

                String dn = requires.name();

                // find dependence
                ModuleReference mref = findWithBeforeFinder(dn);
                if (mref == null) {

                    if (findInParent(dn) != null) {
                        // dependence is in parent
                        continue;
                    }

                    mref = findWithAfterFinder(dn);
                    if (mref == null) {
                        findFail("Module %s not found, required by %s",
                                 dn, descriptor.name());
                    }
                }

                if (isTracing() && !dn.equals("java.base")) {
                    trace("%s requires %s", descriptor.name(), nameAndInfo(mref));
                }

                if (!nameToReference.containsKey(dn)) {
                    addFoundModule(mref);
                    q.offer(mref.descriptor());
                }

            }

            resolved.add(descriptor);
        }

        return resolved;
    }

    /**
     * Augments the set of resolved modules with modules induced by the
     * service-use relation.
     */
    Resolver bind() {

        // Scan the finders for all available service provider modules. As
        // java.base uses services then the module finders will be scanned
        // anyway.
        Map<String, Set<ModuleReference>> availableProviders = new HashMap<>();
        for (ModuleReference mref : findAll()) {
            ModuleDescriptor descriptor = mref.descriptor();
            if (!descriptor.provides().isEmpty()) {

                for (Provides provides :  descriptor.provides()) {
                    String sn = provides.service();

                    // computeIfAbsent
                    Set<ModuleReference> providers = availableProviders.get(sn);
                    if (providers == null) {
                        providers = new HashSet<>();
                        availableProviders.put(sn, providers);
                    }
                    providers.add(mref);
                }

            }
        }

        // create the visit stack
        Deque<ModuleDescriptor> q = new ArrayDeque<>();

        // the initial set of modules that may use services
        Set<ModuleDescriptor> initialConsumers;
        if (ModuleLayer.boot() == null) {
            initialConsumers = new HashSet<>();
        } else {
            initialConsumers = parents.stream()
                    .flatMap(Configuration::configurations)
                    .distinct()
                    .flatMap(c -> c.descriptors().stream())
                    .collect(Collectors.toSet());
        }
        for (ModuleReference mref : nameToReference.values()) {
            initialConsumers.add(mref.descriptor());
        }

        // Where there is a consumer of a service then resolve all modules
        // that provide an implementation of that service
        Set<ModuleDescriptor> candidateConsumers = initialConsumers;
        do {
            for (ModuleDescriptor descriptor : candidateConsumers) {
                if (!descriptor.uses().isEmpty()) {

                    // the modules that provide at least one service
                    Set<ModuleDescriptor> modulesToBind = null;
                    if (isTracing()) {
                        modulesToBind = new HashSet<>();
                    }

                    for (String service : descriptor.uses()) {
                        Set<ModuleReference> mrefs = availableProviders.get(service);
                        if (mrefs != null) {
                            for (ModuleReference mref : mrefs) {
                                ModuleDescriptor provider = mref.descriptor();
                                if (!provider.equals(descriptor)) {

                                    if (isTracing() && modulesToBind.add(provider)) {
                                        trace("%s binds %s", descriptor.name(),
                                                nameAndInfo(mref));
                                    }

                                    String pn = provider.name();
                                    if (!nameToReference.containsKey(pn)) {
                                        addFoundModule(mref);
                                        q.push(provider);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            candidateConsumers = resolve(q);
        } while (!candidateConsumers.isEmpty());

        return this;
    }

    /**
     * Add all automatic modules that have not already been found to the
     * nameToReference map.
     */
    private Set<ModuleReference> addFoundAutomaticModules() {
        Set<ModuleReference> result = new HashSet<>();
        findAll().forEach(mref -> {
            String mn = mref.descriptor().name();
            if (mref.descriptor().isAutomatic() && !nameToReference.containsKey(mn)) {
                addFoundModule(mref);
                result.add(mref);
            }
        });
        return result;
    }

    /**
     * Add the module to the nameToReference map. Also check any constraints on
     * the target platform with the constraints of other modules.
     */
    private void addFoundModule(ModuleReference mref) {
        String mn = mref.descriptor().name();

        if (mref instanceof ModuleReferenceImpl) {
            ModuleTarget target = ((ModuleReferenceImpl)mref).moduleTarget();
            if (target != null)
                checkTargetPlatform(mn, target);
        }

        nameToReference.put(mn, mref);
    }

    /**
     * Check that the module's constraints on the target platform does
     * conflict with the constraint of other modules resolved so far.
     */
    private void checkTargetPlatform(String mn, ModuleTarget target) {
        String value = target.targetPlatform();
        if (value != null) {
            if (targetPlatform == null) {
                targetPlatform = value;
            } else {
                if (!value.equals(targetPlatform)) {
                    findFail("Module %s has constraints on target platform (%s)"
                             + " that conflict with other modules: %s", mn,
                             value, targetPlatform);
                }
            }
        }
    }

    /**
     * Execute post-resolution checks and returns the module graph of resolved
     * modules as a map.
     */
    Map<ResolvedModule, Set<ResolvedModule>> finish(Configuration cf) {
        detectCycles();
        checkHashes();
        Map<ResolvedModule, Set<ResolvedModule>> graph = makeGraph(cf);
        checkExportSuppliers(graph);
        return graph;
    }

    /**
     * Checks the given module graph for cycles.
     *
     * For now the implementation is a simple depth first search on the
     * dependency graph. We'll replace this later, maybe with Tarjan.
     */
    private void detectCycles() {
        visited = new HashSet<>();
        visitPath = new LinkedHashSet<>(); // preserve insertion order
        for (ModuleReference mref : nameToReference.values()) {
            visit(mref.descriptor());
        }
        visited.clear();
    }

    // the modules that were visited
    private Set<ModuleDescriptor> visited;

    // the modules in the current visit path
    private Set<ModuleDescriptor> visitPath;

    private void visit(ModuleDescriptor descriptor) {
        if (!visited.contains(descriptor)) {
            boolean added = visitPath.add(descriptor);
            if (!added) {
                resolveFail("Cycle detected: %s", cycleAsString(descriptor));
            }
            for (ModuleDescriptor.Requires requires : descriptor.requires()) {
                String dn = requires.name();

                ModuleReference mref = nameToReference.get(dn);
                if (mref != null) {
                    ModuleDescriptor other = mref.descriptor();
                    if (other != descriptor) {
                        // dependency is in this configuration
                        visit(other);
                    }
                }
            }
            visitPath.remove(descriptor);
            visited.add(descriptor);
        }
    }

    /**
     * Returns a String with a list of the modules in a detected cycle.
     */
    private String cycleAsString(ModuleDescriptor descriptor) {
        List<ModuleDescriptor> list = new ArrayList<>(visitPath);
        list.add(descriptor);
        int index = list.indexOf(descriptor);
        return list.stream()
                .skip(index)
                .map(ModuleDescriptor::name)
                .collect(Collectors.joining(" -> "));
    }


    /**
     * Checks the hashes in the module descriptor to ensure that they match
     * any recorded hashes.
     */
    private void checkHashes() {
        for (ModuleReference mref : nameToReference.values()) {

            // get the recorded hashes, if any
            if (!(mref instanceof ModuleReferenceImpl))
                continue;
            ModuleHashes hashes = ((ModuleReferenceImpl)mref).recordedHashes();
            if (hashes == null)
                continue;

            ModuleDescriptor descriptor = mref.descriptor();
            String algorithm = hashes.algorithm();
            for (String dn : hashes.names()) {
                ModuleReference mref2 = nameToReference.get(dn);
                if (mref2 == null) {
                    ResolvedModule resolvedModule = findInParent(dn);
                    if (resolvedModule != null)
                        mref2 = resolvedModule.reference();
                }
                if (mref2 == null)
                    continue;

                if (!(mref2 instanceof ModuleReferenceImpl)) {
                    findFail("Unable to compute the hash of module %s", dn);
                }

                ModuleReferenceImpl other = (ModuleReferenceImpl)mref2;
                if (other != null) {
                    byte[] recordedHash = hashes.hashFor(dn);
                    byte[] actualHash = other.computeHash(algorithm);
                    if (actualHash == null)
                        findFail("Unable to compute the hash of module %s", dn);
                    if (!Arrays.equals(recordedHash, actualHash)) {
                        findFail("Hash of %s (%s) differs to expected hash (%s)" +
                                 " recorded in %s", dn, toHexString(actualHash),
                                 toHexString(recordedHash), descriptor.name());
                    }
                }
            }

        }
    }

    private static String toHexString(byte[] ba) {
        StringBuilder sb = new StringBuilder(ba.length * 2);
        for (byte b: ba) {
            sb.append(String.format("%02x", b & 0xff));
        }
        return sb.toString();
    }


    /**
     * Computes the readability graph for the modules in the given Configuration.
     *
     * The readability graph is created by propagating "requires" through the
     * "requires transitive" edges of the module dependence graph. So if the
     * module dependence graph has m1 requires m2 && m2 requires transitive m3
     * then the resulting readability graph will contain m1 reads m2, m1 reads m3,
     * and m2 reads m3.
     */
    private Map<ResolvedModule, Set<ResolvedModule>> makeGraph(Configuration cf) {

        // initial capacity of maps to avoid resizing
        int capacity = 1 + (4 * nameToReference.size())/ 3;

        // the "reads" graph starts as a module dependence graph and
        // is iteratively updated to be the readability graph
        Map<ResolvedModule, Set<ResolvedModule>> g1 = new HashMap<>(capacity);

        // the "requires transitive" graph, contains requires transitive edges only
        Map<ResolvedModule, Set<ResolvedModule>> g2;

        // need "requires transitive" from the modules in parent configurations
        // as there may be selected modules that have a dependency on modules in
        // the parent configuration.
        if (ModuleLayer.boot() == null) {
            g2 = new HashMap<>(capacity);
        } else {
            g2 = parents.stream()
                .flatMap(Configuration::configurations)
                .distinct()
                .flatMap(c ->
                    c.modules().stream().flatMap(m1 ->
                        m1.descriptor().requires().stream()
                            .filter(r -> r.modifiers().contains(Modifier.TRANSITIVE))
                            .flatMap(r -> {
                                Optional<ResolvedModule> m2 = c.findModule(r.name());
                                assert m2.isPresent()
                                        || r.modifiers().contains(Modifier.STATIC);
                                return m2.stream();
                            })
                            .map(m2 -> Map.entry(m1, m2))
                    )
                )
                // stream of m1->m2
                .collect(Collectors.groupingBy(Map.Entry::getKey,
                        HashMap::new,
                        Collectors.mapping(Map.Entry::getValue, Collectors.toSet())
            ));
        }

        // populate g1 and g2 with the dependences from the selected modules

        Map<String, ResolvedModule> nameToResolved = new HashMap<>(capacity);

        for (ModuleReference mref : nameToReference.values()) {
            ModuleDescriptor descriptor = mref.descriptor();
            String name = descriptor.name();

            ResolvedModule m1 = computeIfAbsent(nameToResolved, name, cf, mref);

            Set<ResolvedModule> reads = new HashSet<>();
            Set<ResolvedModule> requiresTransitive = new HashSet<>();

            for (ModuleDescriptor.Requires requires : descriptor.requires()) {
                String dn = requires.name();

                ResolvedModule m2 = null;
                ModuleReference mref2 = nameToReference.get(dn);
                if (mref2 != null) {
                    // same configuration
                    m2 = computeIfAbsent(nameToResolved, dn, cf, mref2);
                } else {
                    // parent configuration
                    m2 = findInParent(dn);
                    if (m2 == null) {
                        assert requires.modifiers().contains(Modifier.STATIC);
                        continue;
                    }
                }

                // m1 requires m2 => m1 reads m2
                reads.add(m2);

                // m1 requires transitive m2
                if (requires.modifiers().contains(Modifier.TRANSITIVE)) {
                    requiresTransitive.add(m2);
                }

            }

            // automatic modules read all selected modules and all modules
            // in parent configurations
            if (descriptor.isAutomatic()) {

                // reads all selected modules
                // `requires transitive` all selected automatic modules
                for (ModuleReference mref2 : nameToReference.values()) {
                    ModuleDescriptor descriptor2 = mref2.descriptor();
                    String name2 = descriptor2.name();

                    if (!name.equals(name2)) {
                        ResolvedModule m2
                            = computeIfAbsent(nameToResolved, name2, cf, mref2);
                        reads.add(m2);
                        if (descriptor2.isAutomatic())
                            requiresTransitive.add(m2);
                    }
                }

                // reads all modules in parent configurations
                // `requires transitive` all automatic modules in parent
                // configurations
                for (Configuration parent : parents) {
                    parent.configurations()
                            .map(Configuration::modules)
                            .flatMap(Set::stream)
                            .forEach(m -> {
                                reads.add(m);
                                if (m.reference().descriptor().isAutomatic())
                                    requiresTransitive.add(m);
                            });
                }
            }

            g1.put(m1, reads);
            g2.put(m1, requiresTransitive);
        }

        // Iteratively update g1 until there are no more requires transitive
        // to propagate
        boolean changed;
        List<ResolvedModule> toAdd = new ArrayList<>();
        do {
            changed = false;
            for (Set<ResolvedModule> m1Reads : g1.values()) {
                for (ResolvedModule m2 : m1Reads) {
                    Set<ResolvedModule> m2RequiresTransitive = g2.get(m2);
                    if (m2RequiresTransitive != null) {
                        for (ResolvedModule m3 : m2RequiresTransitive) {
                            if (!m1Reads.contains(m3)) {
                                // m1 reads m2, m2 requires transitive m3
                                // => need to add m1 reads m3
                                toAdd.add(m3);
                            }
                        }
                    }
                }
                if (!toAdd.isEmpty()) {
                    m1Reads.addAll(toAdd);
                    toAdd.clear();
                    changed = true;
                }
            }
        } while (changed);

        return g1;
    }

    /**
     * Equivalent to
     * <pre>{@code
     *     map.computeIfAbsent(name, k -> new ResolvedModule(cf, mref))
     * </pre>}
     */
    private ResolvedModule computeIfAbsent(Map<String, ResolvedModule> map,
                                           String name,
                                           Configuration cf,
                                           ModuleReference mref)
    {
        ResolvedModule m = map.get(name);
        if (m == null) {
            m = new ResolvedModule(cf, mref);
            map.put(name, m);
        }
        return m;
    }


    /**
     * Checks the readability graph to ensure that
     * <ol>
     *   <li><p> A module does not read two or more modules with the same name.
     *   This includes the case where a module reads another another with the
     *   same name as itself. </p></li>
     *   <li><p> Two or more modules in the configuration don't export the same
     *   package to a module that reads both. This includes the case where a
     *   module {@code M} containing package {@code p} reads another module
     *   that exports {@code p} to {@code M}. </p></li>
     *   <li><p> A module {@code M} doesn't declare that it "{@code uses p.S}"
     *   or "{@code provides p.S with ...}" but package {@code p} is neither
     *   in module {@code M} nor exported to {@code M} by any module that
     *   {@code M} reads. </p></li>
     * </ol>
     */
    private void checkExportSuppliers(Map<ResolvedModule, Set<ResolvedModule>> graph) {

        for (Map.Entry<ResolvedModule, Set<ResolvedModule>> e : graph.entrySet()) {
            ModuleDescriptor descriptor1 = e.getKey().descriptor();
            String name1 = descriptor1.name();

            // the names of the modules that are read (including self)
            Set<String> names = new HashSet<>();
            names.add(name1);

            // the map of packages that are local or exported to descriptor1
            Map<String, ModuleDescriptor> packageToExporter = new HashMap<>();

            // local packages
            Set<String> packages = descriptor1.packages();
            for (String pn : packages) {
                packageToExporter.put(pn, descriptor1);
            }

            // descriptor1 reads descriptor2
            Set<ResolvedModule> reads = e.getValue();
            for (ResolvedModule endpoint : reads) {
                ModuleDescriptor descriptor2 = endpoint.descriptor();

                String name2 = descriptor2.name();
                if (descriptor2 != descriptor1 && !names.add(name2)) {
                    if (name2.equals(name1)) {
                        resolveFail("Module %s reads another module named %s",
                                    name1, name1);
                    } else{
                        resolveFail("Module %s reads more than one module named %s",
                                     name1, name2);
                    }
                }

                if (descriptor2.isAutomatic()) {
                    // automatic modules read self and export all packages
                    if (descriptor2 != descriptor1) {
                        for (String source : descriptor2.packages()) {
                            ModuleDescriptor supplier
                                = packageToExporter.putIfAbsent(source, descriptor2);

                            // descriptor2 and 'supplier' export source to descriptor1
                            if (supplier != null) {
                                failTwoSuppliers(descriptor1, source, descriptor2, supplier);
                            }
                        }

                    }
                } else {
                    for (ModuleDescriptor.Exports export : descriptor2.exports()) {
                        if (export.isQualified()) {
                            if (!export.targets().contains(descriptor1.name()))
                                continue;
                        }

                        // source is exported by descriptor2
                        String source = export.source();
                        ModuleDescriptor supplier
                            = packageToExporter.putIfAbsent(source, descriptor2);

                        // descriptor2 and 'supplier' export source to descriptor1
                        if (supplier != null) {
                            failTwoSuppliers(descriptor1, source, descriptor2, supplier);
                        }
                    }

                }
            }

            // uses/provides checks not applicable to automatic modules
            if (!descriptor1.isAutomatic()) {

                // uses S
                for (String service : descriptor1.uses()) {
                    String pn = packageName(service);
                    if (!packageToExporter.containsKey(pn)) {
                        resolveFail("Module %s does not read a module that exports %s",
                                    descriptor1.name(), pn);
                    }
                }

                // provides S
                for (ModuleDescriptor.Provides provides : descriptor1.provides()) {
                    String pn = packageName(provides.service());
                    if (!packageToExporter.containsKey(pn)) {
                        resolveFail("Module %s does not read a module that exports %s",
                                    descriptor1.name(), pn);
                    }
                }

            }

        }

    }

    /**
     * Fail because a module in the configuration exports the same package to
     * a module that reads both. This includes the case where a module M
     * containing a package p reads another module that exports p to at least
     * module M.
     */
    private void failTwoSuppliers(ModuleDescriptor descriptor,
                                  String source,
                                  ModuleDescriptor supplier1,
                                  ModuleDescriptor supplier2) {

        if (supplier2 == descriptor) {
            ModuleDescriptor tmp = supplier1;
            supplier1 = supplier2;
            supplier2 = tmp;
        }

        if (supplier1 == descriptor) {
            resolveFail("Module %s contains package %s"
                         + ", module %s exports package %s to %s",
                    descriptor.name(),
                    source,
                    supplier2.name(),
                    source,
                    descriptor.name());
        } else {
            resolveFail("Modules %s and %s export package %s to module %s",
                    supplier1.name(),
                    supplier2.name(),
                    source,
                    descriptor.name());
        }

    }


    /**
     * Find a module of the given name in the parent configurations
     */
    private ResolvedModule findInParent(String mn) {
        for (Configuration parent : parents) {
            Optional<ResolvedModule> om = parent.findModule(mn);
            if (om.isPresent())
                return om.get();
        }
        return null;
    }


    /**
     * Invokes the beforeFinder to find method to find the given module.
     */
    private ModuleReference findWithBeforeFinder(String mn) {

        return beforeFinder.find(mn).orElse(null);

    }

    /**
     * Invokes the afterFinder to find method to find the given module.
     */
    private ModuleReference findWithAfterFinder(String mn) {
        return afterFinder.find(mn).orElse(null);
    }

    /**
     * Returns the set of all modules that are observable with the before
     * and after ModuleFinders.
     */
    private Set<ModuleReference> findAll() {
        Set<ModuleReference> beforeModules = beforeFinder.findAll();
        Set<ModuleReference> afterModules = afterFinder.findAll();

        if (afterModules.isEmpty())
            return beforeModules;

        if (beforeModules.isEmpty()
                && parents.size() == 1
                && parents.get(0) == Configuration.empty())
            return afterModules;

        Set<ModuleReference> result = new HashSet<>(beforeModules);
        for (ModuleReference mref : afterModules) {
            String name = mref.descriptor().name();
            if (!beforeFinder.find(name).isPresent()
                    && findInParent(name) == null) {
                result.add(mref);
            }
        }

        return result;
    }

    /**
     * Returns the package name
     */
    private static String packageName(String cn) {
        int index = cn.lastIndexOf(".");
        return (index == -1) ? "" : cn.substring(0, index);
    }

    /**
     * Throw FindException with the given format string and arguments
     */
    private static void findFail(String fmt, Object ... args) {
        String msg = String.format(fmt, args);
        throw new FindException(msg);
    }

    /**
     * Throw ResolutionException with the given format string and arguments
     */
    private static void resolveFail(String fmt, Object ... args) {
        String msg = String.format(fmt, args);
        throw new ResolutionException(msg);
    }

    /**
     * Tracing support
     */

    private boolean isTracing() {
        return traceOutput != null;
    }

    private void trace(String fmt, Object ... args) {
        if (traceOutput != null) {
            traceOutput.format(fmt, args);
            traceOutput.println();
        }
    }

    private String nameAndInfo(ModuleReference mref) {
        ModuleDescriptor descriptor = mref.descriptor();
        StringBuilder sb = new StringBuilder(descriptor.name());
        mref.location().ifPresent(uri -> sb.append(" " + uri));
        if (descriptor.isAutomatic())
            sb.append(" automatic");
        return sb.toString();
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\Module.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.module.Configuration;
import java.lang.module.ModuleReference;
import java.lang.module.ModuleDescriptor;
import java.lang.module.ModuleDescriptor.Exports;
import java.lang.module.ModuleDescriptor.Opens;
import java.lang.module.ModuleDescriptor.Version;
import java.lang.module.ResolvedModule;
import java.lang.reflect.AnnotatedElement;
import java.net.URI;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jdk.internal.loader.BuiltinClassLoader;
import jdk.internal.loader.BootLoader;
import jdk.internal.loader.ClassLoaders;
import jdk.internal.module.IllegalAccessLogger;
import jdk.internal.module.ModuleLoaderMap;
import jdk.internal.module.ServicesCatalog;
import jdk.internal.module.Resources;
import jdk.internal.org.objectweb.asm.AnnotationVisitor;
import jdk.internal.org.objectweb.asm.Attribute;
import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.ClassVisitor;
import jdk.internal.org.objectweb.asm.ClassWriter;
import jdk.internal.org.objectweb.asm.ModuleVisitor;
import jdk.internal.org.objectweb.asm.Opcodes;
import jdk.internal.reflect.CallerSensitive;
import jdk.internal.reflect.Reflection;
import sun.security.util.SecurityConstants;

/**
 * Represents a run-time module, either {@link #isNamed() named} or unnamed.
 *
 * <p> Named modules have a {@link #getName() name} and are constructed by the
 * Java Virtual Machine when a graph of modules is defined to the Java virtual
 * machine to create a {@linkplain ModuleLayer module layer}. </p>
 *
 * <p> An unnamed module does not have a name. There is an unnamed module for
 * each {@link ClassLoader ClassLoader}, obtained by invoking its {@link
 * ClassLoader#getUnnamedModule() getUnnamedModule} method. All types that are
 * not in a named module are members of their defining class loader's unnamed
 * module. </p>
 *
 * <p> The package names that are parameters or returned by methods defined in
 * this class are the fully-qualified names of the packages as defined in
 * section 6.5.3 of <cite>The Java&trade; Language Specification</cite>, for
 * example, {@code "java.lang"}. </p>
 *
 * <p> Unless otherwise specified, passing a {@code null} argument to a method
 * in this class causes a {@link NullPointerException NullPointerException} to
 * be thrown. </p>
 *
 * @since 9
 * @spec JPMS
 * @see Class#getModule()
 */

public final class Module implements AnnotatedElement {

    // the layer that contains this module, can be null
    private final ModuleLayer layer;

    // module name and loader, these fields are read by VM
    private final String name;
    private final ClassLoader loader;

    // the module descriptor
    private final ModuleDescriptor descriptor;


    /**
     * Creates a new named Module. The resulting Module will be defined to the
     * VM but will not read any other modules, will not have any exports setup
     * and will not be registered in the service catalog.
     */
    Module(ModuleLayer layer,
           ClassLoader loader,
           ModuleDescriptor descriptor,
           URI uri)
    {
        this.layer = layer;
        this.name = descriptor.name();
        this.loader = loader;
        this.descriptor = descriptor;

        // define module to VM

        boolean isOpen = descriptor.isOpen() || descriptor.isAutomatic();
        Version version = descriptor.version().orElse(null);
        String vs = Objects.toString(version, null);
        String loc = Objects.toString(uri, null);
        String[] packages = descriptor.packages().toArray(new String[0]);
        defineModule0(this, isOpen, vs, loc, packages);
    }


    /**
     * Create the unnamed Module for the given ClassLoader.
     *
     * @see ClassLoader#getUnnamedModule
     */
    Module(ClassLoader loader) {
        this.layer = null;
        this.name = null;
        this.loader = loader;
        this.descriptor = null;
    }


    /**
     * Creates a named module but without defining the module to the VM.
     *
     * @apiNote This constructor is for VM white-box testing.
     */
    Module(ClassLoader loader, ModuleDescriptor descriptor) {
        this.layer = null;
        this.name = descriptor.name();
        this.loader = loader;
        this.descriptor = descriptor;
    }


    /**
     * Returns {@code true} if this module is a named module.
     *
     * @return {@code true} if this is a named module
     *
     * @see ClassLoader#getUnnamedModule()
     */
    public boolean isNamed() {
        return name != null;
    }

    /**
     * Returns the module name or {@code null} if this module is an unnamed
     * module.
     *
     * @return The module name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the {@code ClassLoader} for this module.
     *
     * <p> If there is a security manager then its {@code checkPermission}
     * method if first called with a {@code RuntimePermission("getClassLoader")}
     * permission to check that the caller is allowed to get access to the
     * class loader. </p>
     *
     * @return The class loader for this module
     *
     * @throws SecurityException
     *         If denied by the security manager
     */
    public ClassLoader getClassLoader() {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(SecurityConstants.GET_CLASSLOADER_PERMISSION);
        }
        return loader;
    }

    /**
     * Returns the module descriptor for this module or {@code null} if this
     * module is an unnamed module.
     *
     * @return The module descriptor for this module
     */
    public ModuleDescriptor getDescriptor() {
        return descriptor;
    }

    /**
     * Returns the module layer that contains this module or {@code null} if
     * this module is not in a module layer.
     *
     * A module layer contains named modules and therefore this method always
     * returns {@code null} when invoked on an unnamed module.
     *
     * <p> <a href="reflect/Proxy.html#dynamicmodule">Dynamic modules</a> are
     * named modules that are generated at runtime. A dynamic module may or may
     * not be in a module layer. </p>
     *
     * @return The module layer that contains this module
     *
     * @see java.lang.reflect.Proxy
     */
    public ModuleLayer getLayer() {
        if (isNamed()) {
            ModuleLayer layer = this.layer;
            if (layer != null)
                return layer;

            // special-case java.base as it is created before the boot layer
            if (loader == null && name.equals("java.base")) {
                return ModuleLayer.boot();
            }
        }
        return null;
    }

    // --

    // special Module to mean "all unnamed modules"
    private static final Module ALL_UNNAMED_MODULE = new Module(null);
    private static final Set<Module> ALL_UNNAMED_MODULE_SET = Set.of(ALL_UNNAMED_MODULE);

    // special Module to mean "everyone"
    private static final Module EVERYONE_MODULE = new Module(null);
    private static final Set<Module> EVERYONE_SET = Set.of(EVERYONE_MODULE);

    /**
     * The holder of data structures to support readability, exports, and
     * service use added at runtime with the reflective APIs.
     */
    private static class ReflectionData {
        /**
         * A module (1st key) reads another module (2nd key)
         */
        static final WeakPairMap<Module, Module, Boolean> reads =
            new WeakPairMap<>();

        /**
         * A module (1st key) exports or opens a package to another module
         * (2nd key). The map value is a map of package name to a boolean
         * that indicates if the package is opened.
         */
        static final WeakPairMap<Module, Module, Map<String, Boolean>> exports =
            new WeakPairMap<>();

        /**
         * A module (1st key) uses a service (2nd key)
         */
        static final WeakPairMap<Module, Class<?>, Boolean> uses =
            new WeakPairMap<>();
    }


    // -- readability --

    // the modules that this module reads
    private volatile Set<Module> reads;

    /**
     * Indicates if this module reads the given module. This method returns
     * {@code true} if invoked to test if this module reads itself. It also
     * returns {@code true} if invoked on an unnamed module (as unnamed
     * modules read all modules).
     *
     * @param  other
     *         The other module
     *
     * @return {@code true} if this module reads {@code other}
     *
     * @see #addReads(Module)
     */
    public boolean canRead(Module other) {
        Objects.requireNonNull(other);

        // an unnamed module reads all modules
        if (!this.isNamed())
            return true;

        // all modules read themselves
        if (other == this)
            return true;

        // check if this module reads other
        if (other.isNamed()) {
            Set<Module> reads = this.reads; // volatile read
            if (reads != null && reads.contains(other))
                return true;
        }

        // check if this module reads the other module reflectively
        if (ReflectionData.reads.containsKeyPair(this, other))
            return true;

        // if other is an unnamed module then check if this module reads
        // all unnamed modules
        if (!other.isNamed()
            && ReflectionData.reads.containsKeyPair(this, ALL_UNNAMED_MODULE))
            return true;

        return false;
    }

    /**
     * If the caller's module is this module then update this module to read
     * the given module.
     *
     * This method is a no-op if {@code other} is this module (all modules read
     * themselves), this module is an unnamed module (as unnamed modules read
     * all modules), or this module already reads {@code other}.
     *
     * @implNote <em>Read edges</em> added by this method are <em>weak</em> and
     * do not prevent {@code other} from being GC'ed when this module is
     * strongly reachable.
     *
     * @param  other
     *         The other module
     *
     * @return this module
     *
     * @throws IllegalCallerException
     *         If this is a named module and the caller's module is not this
     *         module
     *
     * @see #canRead
     */
    @CallerSensitive
    public Module addReads(Module other) {
        Objects.requireNonNull(other);
        if (this.isNamed()) {
            Module caller = getCallerModule(Reflection.getCallerClass());
            if (caller != this) {
                throw new IllegalCallerException(caller + " != " + this);
            }
            implAddReads(other, true);
        }
        return this;
    }

    /**
     * Updates this module to read another module.
     *
     * @apiNote Used by the --add-reads command line option.
     */
    void implAddReads(Module other) {
        implAddReads(other, true);
    }

    /**
     * Updates this module to read all unnamed modules.
     *
     * @apiNote Used by the --add-reads command line option.
     */
    void implAddReadsAllUnnamed() {
        implAddReads(Module.ALL_UNNAMED_MODULE, true);
    }

    /**
     * Updates this module to read another module without notifying the VM.
     *
     * @apiNote This method is for VM white-box testing.
     */
    void implAddReadsNoSync(Module other) {
        implAddReads(other, false);
    }

    /**
     * Makes the given {@code Module} readable to this module.
     *
     * If {@code syncVM} is {@code true} then the VM is notified.
     */
    private void implAddReads(Module other, boolean syncVM) {
        Objects.requireNonNull(other);
        if (!canRead(other)) {
            // update VM first, just in case it fails
            if (syncVM) {
                if (other == ALL_UNNAMED_MODULE) {
                    addReads0(this, null);
                } else {
                    addReads0(this, other);
                }
            }

            // add reflective read
            ReflectionData.reads.putIfAbsent(this, other, Boolean.TRUE);
        }
    }


    // -- exported and open packages --

    // the packages are open to other modules, can be null
    // if the value contains EVERYONE_MODULE then the package is open to all
    private volatile Map<String, Set<Module>> openPackages;

    // the packages that are exported, can be null
    // if the value contains EVERYONE_MODULE then the package is exported to all
    private volatile Map<String, Set<Module>> exportedPackages;

    /**
     * Returns {@code true} if this module exports the given package to at
     * least the given module.
     *
     * <p> This method returns {@code true} if invoked to test if a package in
     * this module is exported to itself. It always returns {@code true} when
     * invoked on an unnamed module. A package that is {@link #isOpen open} to
     * the given module is considered exported to that module at run-time and
     * so this method returns {@code true} if the package is open to the given
     * module. </p>
     *
     * <p> This method does not check if the given module reads this module. </p>
     *
     * @param  pn
     *         The package name
     * @param  other
     *         The other module
     *
     * @return {@code true} if this module exports the package to at least the
     *         given module
     *
     * @see ModuleDescriptor#exports()
     * @see #addExports(String,Module)
     */
    public boolean isExported(String pn, Module other) {
        Objects.requireNonNull(pn);
        Objects.requireNonNull(other);
        return implIsExportedOrOpen(pn, other, /*open*/false);
    }

    /**
     * Returns {@code true} if this module has <em>opened</em> a package to at
     * least the given module.
     *
     * <p> This method returns {@code true} if invoked to test if a package in
     * this module is open to itself. It returns {@code true} when invoked on an
     * {@link ModuleDescriptor#isOpen open} module with a package in the module.
     * It always returns {@code true} when invoked on an unnamed module. </p>
     *
     * <p> This method does not check if the given module reads this module. </p>
     *
     * @param  pn
     *         The package name
     * @param  other
     *         The other module
     *
     * @return {@code true} if this module has <em>opened</em> the package
     *         to at least the given module
     *
     * @see ModuleDescriptor#opens()
     * @see #addOpens(String,Module)
     * @see AccessibleObject#setAccessible(boolean)
     * @see java.lang.invoke.MethodHandles#privateLookupIn
     */
    public boolean isOpen(String pn, Module other) {
        Objects.requireNonNull(pn);
        Objects.requireNonNull(other);
        return implIsExportedOrOpen(pn, other, /*open*/true);
    }

    /**
     * Returns {@code true} if this module exports the given package
     * unconditionally.
     *
     * <p> This method always returns {@code true} when invoked on an unnamed
     * module. A package that is {@link #isOpen(String) opened} unconditionally
     * is considered exported unconditionally at run-time and so this method
     * returns {@code true} if the package is opened unconditionally. </p>
     *
     * <p> This method does not check if the given module reads this module. </p>
     *
     * @param  pn
     *         The package name
     *
     * @return {@code true} if this module exports the package unconditionally
     *
     * @see ModuleDescriptor#exports()
     */
    public boolean isExported(String pn) {
        Objects.requireNonNull(pn);
        return implIsExportedOrOpen(pn, EVERYONE_MODULE, /*open*/false);
    }

    /**
     * Returns {@code true} if this module has <em>opened</em> a package
     * unconditionally.
     *
     * <p> This method always returns {@code true} when invoked on an unnamed
     * module. Additionally, it always returns {@code true} when invoked on an
     * {@link ModuleDescriptor#isOpen open} module with a package in the
     * module. </p>
     *
     * <p> This method does not check if the given module reads this module. </p>
     *
     * @param  pn
     *         The package name
     *
     * @return {@code true} if this module has <em>opened</em> the package
     *         unconditionally
     *
     * @see ModuleDescriptor#opens()
     */
    public boolean isOpen(String pn) {
        Objects.requireNonNull(pn);
        return implIsExportedOrOpen(pn, EVERYONE_MODULE, /*open*/true);
    }


    /**
     * Returns {@code true} if this module exports or opens the given package
     * to the given module. If the other module is {@code EVERYONE_MODULE} then
     * this method tests if the package is exported or opened unconditionally.
     */
    private boolean implIsExportedOrOpen(String pn, Module other, boolean open) {
        // all packages in unnamed modules are open
        if (!isNamed())
            return true;

        // all packages are exported/open to self
        if (other == this && descriptor.packages().contains(pn))
            return true;

        // all packages in open and automatic modules are open
        if (descriptor.isOpen() || descriptor.isAutomatic())
            return descriptor.packages().contains(pn);

        // exported/opened via module declaration/descriptor
        if (isStaticallyExportedOrOpen(pn, other, open))
            return true;

        // exported via addExports/addOpens
        if (isReflectivelyExportedOrOpen(pn, other, open))
            return true;

        // not exported or open to other
        return false;
    }

    /**
     * Returns {@code true} if this module exports or opens a package to
     * the given module via its module declaration or CLI options.
     */
    private boolean isStaticallyExportedOrOpen(String pn, Module other, boolean open) {
        // test if package is open to everyone or <other>
        Map<String, Set<Module>> openPackages = this.openPackages;
        if (openPackages != null && allows(openPackages.get(pn), other)) {
            return true;
        }

        if (!open) {
            // test package is exported to everyone or <other>
            Map<String, Set<Module>> exportedPackages = this.exportedPackages;
            if (exportedPackages != null && allows(exportedPackages.get(pn), other)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns {@code true} if targets is non-null and contains EVERYONE_MODULE
     * or the given module. Also returns true if the given module is an unnamed
     * module and targets contains ALL_UNNAMED_MODULE.
     */
    private boolean allows(Set<Module> targets, Module module) {
       if (targets != null) {
           if (targets.contains(EVERYONE_MODULE))
               return true;
           if (module != EVERYONE_MODULE) {
               if (targets.contains(module))
                   return true;
               if (!module.isNamed() && targets.contains(ALL_UNNAMED_MODULE))
                   return true;
           }
        }
        return false;
    }

    /**
     * Returns {@code true} if this module reflectively exports or opens the
     * given package to the given module.
     */
    private boolean isReflectivelyExportedOrOpen(String pn, Module other, boolean open) {
        // exported or open to all modules
        Map<String, Boolean> exports = ReflectionData.exports.get(this, EVERYONE_MODULE);
        if (exports != null) {
            Boolean b = exports.get(pn);
            if (b != null) {
                boolean isOpen = b.booleanValue();
                if (!open || isOpen) return true;
            }
        }

        if (other != EVERYONE_MODULE) {

            // exported or open to other
            exports = ReflectionData.exports.get(this, other);
            if (exports != null) {
                Boolean b = exports.get(pn);
                if (b != null) {
                    boolean isOpen = b.booleanValue();
                    if (!open || isOpen) return true;
                }
            }

            // other is an unnamed module && exported or open to all unnamed
            if (!other.isNamed()) {
                exports = ReflectionData.exports.get(this, ALL_UNNAMED_MODULE);
                if (exports != null) {
                    Boolean b = exports.get(pn);
                    if (b != null) {
                        boolean isOpen = b.booleanValue();
                        if (!open || isOpen) return true;
                    }
                }
            }

        }

        return false;
    }

    /**
     * Returns {@code true} if this module reflectively exports the
     * given package to the given module.
     */
    boolean isReflectivelyExported(String pn, Module other) {
        return isReflectivelyExportedOrOpen(pn, other, false);
    }

    /**
     * Returns {@code true} if this module reflectively opens the
     * given package to the given module.
     */
    boolean isReflectivelyOpened(String pn, Module other) {
        return isReflectivelyExportedOrOpen(pn, other, true);
    }


    /**
     * If the caller's module is this module then update this module to export
     * the given package to the given module.
     *
     * <p> This method has no effect if the package is already exported (or
     * <em>open</em>) to the given module. </p>
     *
     * @apiNote As specified in section 5.4.3 of the <cite>The Java&trade;
     * Virtual Machine Specification </cite>, if an attempt to resolve a
     * symbolic reference fails because of a linkage error, then subsequent
     * attempts to resolve the reference always fail with the same error that
     * was thrown as a result of the initial resolution attempt.
     *
     * @param  pn
     *         The package name
     * @param  other
     *         The module
     *
     * @return this module
     *
     * @throws IllegalArgumentException
     *         If {@code pn} is {@code null}, or this is a named module and the
     *         package {@code pn} is not a package in this module
     * @throws IllegalCallerException
     *         If this is a named module and the caller's module is not this
     *         module
     *
     * @jvms 5.4.3 Resolution
     * @see #isExported(String,Module)
     */
    @CallerSensitive
    public Module addExports(String pn, Module other) {
        if (pn == null)
            throw new IllegalArgumentException("package is null");
        Objects.requireNonNull(other);

        if (isNamed()) {
            Module caller = getCallerModule(Reflection.getCallerClass());
            if (caller != this) {
                throw new IllegalCallerException(caller + " != " + this);
            }
            implAddExportsOrOpens(pn, other, /*open*/false, /*syncVM*/true);
        }

        return this;
    }

    /**
     * If this module has <em>opened</em> a package to at least the caller
     * module then update this module to open the package to the given module.
     * Opening a package with this method allows all types in the package,
     * and all their members, not just public types and their public members,
     * to be reflected on by the given module when using APIs that support
     * private access or a way to bypass or suppress default Java language
     * access control checks.
     *
     * <p> This method has no effect if the package is already <em>open</em>
     * to the given module. </p>
     *
     * @apiNote This method can be used for cases where a <em>consumer
     * module</em> uses a qualified opens to open a package to an <em>API
     * module</em> but where the reflective access to the members of classes in
     * the consumer module is delegated to code in another module. Code in the
     * API module can use this method to open the package in the consumer module
     * to the other module.
     *
     * @param  pn
     *         The package name
     * @param  other
     *         The module
     *
     * @return this module
     *
     * @throws IllegalArgumentException
     *         If {@code pn} is {@code null}, or this is a named module and the
     *         package {@code pn} is not a package in this module
     * @throws IllegalCallerException
     *         If this is a named module and this module has not opened the
     *         package to at least the caller's module
     *
     * @see #isOpen(String,Module)
     * @see AccessibleObject#setAccessible(boolean)
     * @see java.lang.invoke.MethodHandles#privateLookupIn
     */
    @CallerSensitive
    public Module addOpens(String pn, Module other) {
        if (pn == null)
            throw new IllegalArgumentException("package is null");
        Objects.requireNonNull(other);

        if (isNamed()) {
            Module caller = getCallerModule(Reflection.getCallerClass());
            if (caller != this && (caller == null || !isOpen(pn, caller)))
                throw new IllegalCallerException(pn + " is not open to " + caller);
            implAddExportsOrOpens(pn, other, /*open*/true, /*syncVM*/true);
        }

        return this;
    }


    /**
     * Updates this module to export a package unconditionally.
     *
     * @apiNote This method is for JDK tests only.
     */
    void implAddExports(String pn) {
        implAddExportsOrOpens(pn, Module.EVERYONE_MODULE, false, true);
    }

    /**
     * Updates this module to export a package to another module.
     *
     * @apiNote Used by Instrumentation::redefineModule and --add-exports
     */
    void implAddExports(String pn, Module other) {
        implAddExportsOrOpens(pn, other, false, true);
    }

    /**
     * Updates this module to export a package to all unnamed modules.
     *
     * @apiNote Used by the --add-exports command line option.
     */
    void implAddExportsToAllUnnamed(String pn) {
        implAddExportsOrOpens(pn, Module.ALL_UNNAMED_MODULE, false, true);
    }

    /**
     * Updates this export to export a package unconditionally without
     * notifying the VM.
     *
     * @apiNote This method is for VM white-box testing.
     */
    void implAddExportsNoSync(String pn) {
        implAddExportsOrOpens(pn.replace('/', '.'), Module.EVERYONE_MODULE, false, false);
    }

    /**
     * Updates a module to export a package to another module without
     * notifying the VM.
     *
     * @apiNote This method is for VM white-box testing.
     */
    void implAddExportsNoSync(String pn, Module other) {
        implAddExportsOrOpens(pn.replace('/', '.'), other, false, false);
    }

    /**
     * Updates this module to open a package unconditionally.
     *
     * @apiNote This method is for JDK tests only.
     */
    void implAddOpens(String pn) {
        implAddExportsOrOpens(pn, Module.EVERYONE_MODULE, true, true);
    }

    /**
     * Updates this module to open a package to another module.
     *
     * @apiNote Used by Instrumentation::redefineModule and --add-opens
     */
    void implAddOpens(String pn, Module other) {
        implAddExportsOrOpens(pn, other, true, true);
    }

    /**
     * Updates this module to open a package to all unnamed modules.
     *
     * @apiNote Used by the --add-opens command line option.
     */
    void implAddOpensToAllUnnamed(String pn) {
        implAddExportsOrOpens(pn, Module.ALL_UNNAMED_MODULE, true, true);
    }

    /**
     * Updates a module to export or open a module to another module.
     *
     * If {@code syncVM} is {@code true} then the VM is notified.
     */
    private void implAddExportsOrOpens(String pn,
                                       Module other,
                                       boolean open,
                                       boolean syncVM) {
        Objects.requireNonNull(other);
        Objects.requireNonNull(pn);

        // all packages are open in unnamed, open, and automatic modules
        if (!isNamed() || descriptor.isOpen() || descriptor.isAutomatic())
            return;

        // check if the package is already exported/open to other
        if (implIsExportedOrOpen(pn, other, open)) {

            // if the package is exported/open for illegal access then we need
            // to record that it has also been exported/opened reflectively so
            // that the IllegalAccessLogger doesn't emit a warning.
            boolean needToAdd = false;
            if (!other.isNamed()) {
                IllegalAccessLogger l = IllegalAccessLogger.illegalAccessLogger();
                if (l != null) {
                    if (open) {
                        needToAdd = l.isOpenForIllegalAccess(this, pn);
                    } else {
                        needToAdd = l.isExportedForIllegalAccess(this, pn);
                    }
                }
            }
            if (!needToAdd) {
                // nothing to do
                return;
            }
        }

        // can only export a package in the module
        if (!descriptor.packages().contains(pn)) {
            throw new IllegalArgumentException("package " + pn
                                               + " not in contents");
        }

        // update VM first, just in case it fails
        if (syncVM) {
            if (other == EVERYONE_MODULE) {
                addExportsToAll0(this, pn);
            } else if (other == ALL_UNNAMED_MODULE) {
                addExportsToAllUnnamed0(this, pn);
            } else {
                addExports0(this, pn, other);
            }
        }

        // add package name to exports if absent
        Map<String, Boolean> map = ReflectionData.exports
            .computeIfAbsent(this, other,
                             (m1, m2) -> new ConcurrentHashMap<>());
        if (open) {
            map.put(pn, Boolean.TRUE);  // may need to promote from FALSE to TRUE
        } else {
            map.putIfAbsent(pn, Boolean.FALSE);
        }
    }

    /**
     * Updates a module to open all packages returned by the given iterator to
     * all unnamed modules.
     *
     * @apiNote Used during startup to open packages for illegal access.
     */
    void implAddOpensToAllUnnamed(Iterator<String> iterator) {
        if (jdk.internal.misc.VM.isModuleSystemInited()) {
            throw new IllegalStateException("Module system already initialized");
        }

        // replace this module's openPackages map with a new map that opens
        // the packages to all unnamed modules.
        Map<String, Set<Module>> openPackages = this.openPackages;
        if (openPackages == null) {
            openPackages = new HashMap<>();
        } else {
            openPackages = new HashMap<>(openPackages);
        }
        while (iterator.hasNext()) {
            String pn = iterator.next();
            Set<Module> prev = openPackages.putIfAbsent(pn, ALL_UNNAMED_MODULE_SET);
            if (prev != null) {
                prev.add(ALL_UNNAMED_MODULE);
            }

            // update VM to export the package
            addExportsToAllUnnamed0(this, pn);
        }
        this.openPackages = openPackages;
    }


    // -- services --

    /**
     * If the caller's module is this module then update this module to add a
     * service dependence on the given service type. This method is intended
     * for use by frameworks that invoke {@link java.util.ServiceLoader
     * ServiceLoader} on behalf of other modules or where the framework is
     * passed a reference to the service type by other code. This method is
     * a no-op when invoked on an unnamed module or an automatic module.
     *
     * <p> This method does not cause {@link Configuration#resolveAndBind
     * resolveAndBind} to be re-run. </p>
     *
     * @param  service
     *         The service type
     *
     * @return this module
     *
     * @throws IllegalCallerException
     *         If this is a named module and the caller's module is not this
     *         module
     *
     * @see #canUse(Class)
     * @see ModuleDescriptor#uses()
     */
    @CallerSensitive
    public Module addUses(Class<?> service) {
        Objects.requireNonNull(service);

        if (isNamed() && !descriptor.isAutomatic()) {
            Module caller = getCallerModule(Reflection.getCallerClass());
            if (caller != this) {
                throw new IllegalCallerException(caller + " != " + this);
            }
            implAddUses(service);
        }

        return this;
    }

    /**
     * Update this module to add a service dependence on the given service
     * type.
     */
    void implAddUses(Class<?> service) {
        if (!canUse(service)) {
            ReflectionData.uses.putIfAbsent(this, service, Boolean.TRUE);
        }
    }


    /**
     * Indicates if this module has a service dependence on the given service
     * type. This method always returns {@code true} when invoked on an unnamed
     * module or an automatic module.
     *
     * @param  service
     *         The service type
     *
     * @return {@code true} if this module uses service type {@code st}
     *
     * @see #addUses(Class)
     */
    public boolean canUse(Class<?> service) {
        Objects.requireNonNull(service);

        if (!isNamed())
            return true;

        if (descriptor.isAutomatic())
            return true;

        // uses was declared
        if (descriptor.uses().contains(service.getName()))
            return true;

        // uses added via addUses
        return ReflectionData.uses.containsKeyPair(this, service);
    }



    // -- packages --

    /**
     * Returns the set of package names for the packages in this module.
     *
     * <p> For named modules, the returned set contains an element for each
     * package in the module. </p>
     *
     * <p> For unnamed modules, this method is the equivalent to invoking the
     * {@link ClassLoader#getDefinedPackages() getDefinedPackages} method of
     * this module's class loader and returning the set of package names. </p>
     *
     * @return the set of the package names of the packages in this module
     */
    public Set<String> getPackages() {
        if (isNamed()) {
            return descriptor.packages();
        } else {
            // unnamed module
            Stream<Package> packages;
            if (loader == null) {
                packages = BootLoader.packages();
            } else {
                packages = loader.packages();
            }
            return packages.map(Package::getName).collect(Collectors.toSet());
        }
    }


    // -- creating Module objects --

    /**
     * Defines all module in a configuration to the runtime.
     *
     * @return a map of module name to runtime {@code Module}
     *
     * @throws IllegalArgumentException
     *         If defining any of the modules to the VM fails
     */
    static Map<String, Module> defineModules(Configuration cf,
                                             Function<String, ClassLoader> clf,
                                             ModuleLayer layer)
    {
        boolean isBootLayer = (ModuleLayer.boot() == null);

        int cap = (int)(cf.modules().size() / 0.75f + 1.0f);
        Map<String, Module> nameToModule = new HashMap<>(cap);
        Map<String, ClassLoader> nameToLoader = new HashMap<>(cap);

        Set<ClassLoader> loaders = new HashSet<>();
        boolean hasPlatformModules = false;

        // map each module to a class loader
        for (ResolvedModule resolvedModule : cf.modules()) {
            String name = resolvedModule.name();
            ClassLoader loader = clf.apply(name);
            nameToLoader.put(name, loader);
            if (loader == null || loader == ClassLoaders.platformClassLoader()) {
                if (!(clf instanceof ModuleLoaderMap.Mapper)) {
                    throw new IllegalArgumentException("loader can't be 'null'"
                            + " or the platform class loader");
                }
                hasPlatformModules = true;
            } else {
                loaders.add(loader);
            }
        }

        // define each module in the configuration to the VM
        for (ResolvedModule resolvedModule : cf.modules()) {
            ModuleReference mref = resolvedModule.reference();
            ModuleDescriptor descriptor = mref.descriptor();
            String name = descriptor.name();
            ClassLoader loader = nameToLoader.get(name);
            Module m;
            if (loader == null && name.equals("java.base")) {
                // java.base is already defined to the VM
                m = Object.class.getModule();
            } else {
                URI uri = mref.location().orElse(null);
                m = new Module(layer, loader, descriptor, uri);
            }
            nameToModule.put(name, m);
        }

        // setup readability and exports/opens
        for (ResolvedModule resolvedModule : cf.modules()) {
            ModuleReference mref = resolvedModule.reference();
            ModuleDescriptor descriptor = mref.descriptor();

            String mn = descriptor.name();
            Module m = nameToModule.get(mn);
            assert m != null;

            // reads
            Set<Module> reads = new HashSet<>();

            // name -> source Module when in parent layer
            Map<String, Module> nameToSource = Collections.emptyMap();

            for (ResolvedModule other : resolvedModule.reads()) {
                Module m2 = null;
                if (other.configuration() == cf) {
                    // this configuration
                    m2 = nameToModule.get(other.name());
                    assert m2 != null;
                } else {
                    // parent layer
                    for (ModuleLayer parent: layer.parents()) {
                        m2 = findModule(parent, other);
                        if (m2 != null)
                            break;
                    }
                    assert m2 != null;
                    if (nameToSource.isEmpty())
                        nameToSource = new HashMap<>();
                    nameToSource.put(other.name(), m2);
                }
                reads.add(m2);

                // update VM view
                addReads0(m, m2);
            }
            m.reads = reads;

            // automatic modules read all unnamed modules
            if (descriptor.isAutomatic()) {
                m.implAddReads(ALL_UNNAMED_MODULE, true);
            }

            // exports and opens, skipped for open and automatic
            if (!descriptor.isOpen() && !descriptor.isAutomatic()) {
                if (isBootLayer && descriptor.opens().isEmpty()) {
                    // no open packages, no qualified exports to modules in parent layers
                    initExports(m, nameToModule);
                } else {
                    initExportsAndOpens(m, nameToSource, nameToModule, layer.parents());
                }
            }
        }

        // if there are modules defined to the boot or platform class loaders
        // then register the modules in the class loader's services catalog
        if (hasPlatformModules) {
            ClassLoader pcl = ClassLoaders.platformClassLoader();
            ServicesCatalog bootCatalog = BootLoader.getServicesCatalog();
            ServicesCatalog pclCatalog = ServicesCatalog.getServicesCatalog(pcl);
            for (ResolvedModule resolvedModule : cf.modules()) {
                ModuleReference mref = resolvedModule.reference();
                ModuleDescriptor descriptor = mref.descriptor();
                if (!descriptor.provides().isEmpty()) {
                    String name = descriptor.name();
                    Module m = nameToModule.get(name);
                    ClassLoader loader = nameToLoader.get(name);
                    if (loader == null) {
                        bootCatalog.register(m);
                    } else if (loader == pcl) {
                        pclCatalog.register(m);
                    }
                }
            }
        }

        // record that there is a layer with modules defined to the class loader
        for (ClassLoader loader : loaders) {
            layer.bindToLoader(loader);
        }

        return nameToModule;
    }

    /**
     * Find the runtime Module corresponding to the given ResolvedModule
     * in the given parent layer (or its parents).
     */
    private static Module findModule(ModuleLayer parent,
                                     ResolvedModule resolvedModule) {
        Configuration cf = resolvedModule.configuration();
        String dn = resolvedModule.name();
        return parent.layers()
                .filter(l -> l.configuration() == cf)
                .findAny()
                .map(layer -> {
                    Optional<Module> om = layer.findModule(dn);
                    assert om.isPresent() : dn + " not found in layer";
                    Module m = om.get();
                    assert m.getLayer() == layer : m + " not in expected layer";
                    return m;
                })
                .orElse(null);
    }

    /**
     * Initialize/setup a module's exports.
     *
     * @param m the module
     * @param nameToModule map of module name to Module (for qualified exports)
     */
    private static void initExports(Module m, Map<String, Module> nameToModule) {
        Map<String, Set<Module>> exportedPackages = new HashMap<>();

        for (Exports exports : m.getDescriptor().exports()) {
            String source = exports.source();
            if (exports.isQualified()) {
                // qualified exports
                Set<Module> targets = new HashSet<>();
                for (String target : exports.targets()) {
                    Module m2 = nameToModule.get(target);
                    if (m2 != null) {
                        addExports0(m, source, m2);
                        targets.add(m2);
                    }
                }
                if (!targets.isEmpty()) {
                    exportedPackages.put(source, targets);
                }
            } else {
                // unqualified exports
                addExportsToAll0(m, source);
                exportedPackages.put(source, EVERYONE_SET);
            }
        }

        if (!exportedPackages.isEmpty())
            m.exportedPackages = exportedPackages;
    }

    /**
     * Initialize/setup a module's exports.
     *
     * @param m the module
     * @param nameToSource map of module name to Module for modules that m reads
     * @param nameToModule map of module name to Module for modules in the layer
     *                     under construction
     * @param parents the parent layers
     */
    private static void initExportsAndOpens(Module m,
                                            Map<String, Module> nameToSource,
                                            Map<String, Module> nameToModule,
                                            List<ModuleLayer> parents) {
        ModuleDescriptor descriptor = m.getDescriptor();
        Map<String, Set<Module>> openPackages = new HashMap<>();
        Map<String, Set<Module>> exportedPackages = new HashMap<>();

        // process the open packages first
        for (Opens opens : descriptor.opens()) {
            String source = opens.source();

            if (opens.isQualified()) {
                // qualified opens
                Set<Module> targets = new HashSet<>();
                for (String target : opens.targets()) {
                    Module m2 = findModule(target, nameToSource, nameToModule, parents);
                    if (m2 != null) {
                        addExports0(m, source, m2);
                        targets.add(m2);
                    }
                }
                if (!targets.isEmpty()) {
                    openPackages.put(source, targets);
                }
            } else {
                // unqualified opens
                addExportsToAll0(m, source);
                openPackages.put(source, EVERYONE_SET);
            }
        }

        // next the exports, skipping exports when the package is open
        for (Exports exports : descriptor.exports()) {
            String source = exports.source();

            // skip export if package is already open to everyone
            Set<Module> openToTargets = openPackages.get(source);
            if (openToTargets != null && openToTargets.contains(EVERYONE_MODULE))
                continue;

            if (exports.isQualified()) {
                // qualified exports
                Set<Module> targets = new HashSet<>();
                for (String target : exports.targets()) {
                    Module m2 = findModule(target, nameToSource, nameToModule, parents);
                    if (m2 != null) {
                        // skip qualified export if already open to m2
                        if (openToTargets == null || !openToTargets.contains(m2)) {
                            addExports0(m, source, m2);
                            targets.add(m2);
                        }
                    }
                }
                if (!targets.isEmpty()) {
                    exportedPackages.put(source, targets);
                }
            } else {
                // unqualified exports
                addExportsToAll0(m, source);
                exportedPackages.put(source, EVERYONE_SET);
            }
        }

        if (!openPackages.isEmpty())
            m.openPackages = openPackages;
        if (!exportedPackages.isEmpty())
            m.exportedPackages = exportedPackages;
    }

    /**
     * Find the runtime Module with the given name. The module name is the
     * name of a target module in a qualified exports or opens directive.
     *
     * @param target The target module to find
     * @param nameToSource The modules in parent layers that are read
     * @param nameToModule The modules in the layer under construction
     * @param parents The parent layers
     */
    private static Module findModule(String target,
                                     Map<String, Module> nameToSource,
                                     Map<String, Module> nameToModule,
                                     List<ModuleLayer> parents) {
        Module m = nameToSource.get(target);
        if (m == null) {
            m = nameToModule.get(target);
            if (m == null) {
                for (ModuleLayer parent : parents) {
                    m = parent.findModule(target).orElse(null);
                    if (m != null) break;
                }
            }
        }
        return m;
    }


    // -- annotations --

    /**
     * {@inheritDoc}
     * This method returns {@code null} when invoked on an unnamed module.
     */
    @Override
    public <T extends Annotation> T getAnnotation(Class<T> annotationClass) {
        return moduleInfoClass().getDeclaredAnnotation(annotationClass);
    }

    /**
     * {@inheritDoc}
     * This method returns an empty array when invoked on an unnamed module.
     */
    @Override
    public Annotation[] getAnnotations() {
        return moduleInfoClass().getAnnotations();
    }

    /**
     * {@inheritDoc}
     * This method returns an empty array when invoked on an unnamed module.
     */
    @Override
    public Annotation[] getDeclaredAnnotations() {
        return moduleInfoClass().getDeclaredAnnotations();
    }

    // cached class file with annotations
    private volatile Class<?> moduleInfoClass;

    private Class<?> moduleInfoClass() {
        Class<?> clazz = this.moduleInfoClass;
        if (clazz != null)
            return clazz;

        synchronized (this) {
            clazz = this.moduleInfoClass;
            if (clazz == null) {
                if (isNamed()) {
                    PrivilegedAction<Class<?>> pa = this::loadModuleInfoClass;
                    clazz = AccessController.doPrivileged(pa);
                }
                if (clazz == null) {
                    class DummyModuleInfo { }
                    clazz = DummyModuleInfo.class;
                }
                this.moduleInfoClass = clazz;
            }
            return clazz;
        }
    }

    private Class<?> loadModuleInfoClass() {
        Class<?> clazz = null;
        try (InputStream in = getResourceAsStream("module-info.class")) {
            if (in != null)
                clazz = loadModuleInfoClass(in);
        } catch (Exception ignore) { }
        return clazz;
    }

    /**
     * Loads module-info.class as a package-private interface in a class loader
     * that is a child of this module's class loader.
     */
    private Class<?> loadModuleInfoClass(InputStream in) throws IOException {
        final String MODULE_INFO = "module-info";

        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS
                                         + ClassWriter.COMPUTE_FRAMES);

        ClassVisitor cv = new ClassVisitor(Opcodes.ASM6, cw) {
            @Override
            public void visit(int version,
                              int access,
                              String name,
                              String signature,
                              String superName,
                              String[] interfaces) {
                cw.visit(version,
                        Opcodes.ACC_INTERFACE
                            + Opcodes.ACC_ABSTRACT
                            + Opcodes.ACC_SYNTHETIC,
                        MODULE_INFO,
                        null,
                        "java/lang/Object",
                        null);
            }
            @Override
            public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
                // keep annotations
                return super.visitAnnotation(desc, visible);
            }
            @Override
            public void visitAttribute(Attribute attr) {
                // drop non-annotation attributes
            }
            @Override
            public ModuleVisitor visitModule(String name, int flags, String version) {
                // drop Module attribute
                return null;
            }
        };

        ClassReader cr = new ClassReader(in);
        cr.accept(cv, 0);
        byte[] bytes = cw.toByteArray();

        ClassLoader cl = new ClassLoader(loader) {
            @Override
            protected Class<?> findClass(String cn)throws ClassNotFoundException {
                if (cn.equals(MODULE_INFO)) {
                    return super.defineClass(cn, bytes, 0, bytes.length);
                } else {
                    throw new ClassNotFoundException(cn);
                }
            }
        };

        try {
            return cl.loadClass(MODULE_INFO);
        } catch (ClassNotFoundException e) {
            throw new InternalError(e);
        }
    }


    // -- misc --


    /**
     * Returns an input stream for reading a resource in this module.
     * The {@code name} parameter is a {@code '/'}-separated path name that
     * identifies the resource. As with {@link Class#getResourceAsStream
     * Class.getResourceAsStream}, this method delegates to the module's class
     * loader {@link ClassLoader#findResource(String,String)
     * findResource(String,String)} method, invoking it with the module name
     * (or {@code null} when the module is unnamed) and the name of the
     * resource. If the resource name has a leading slash then it is dropped
     * before delegation.
     *
     * <p> A resource in a named module may be <em>encapsulated</em> so that
     * it cannot be located by code in other modules. Whether a resource can be
     * located or not is determined as follows: </p>
     *
     * <ul>
     *     <li> If the resource name ends with  "{@code .class}" then it is not
     *     encapsulated. </li>
     *
     *     <li> A <em>package name</em> is derived from the resource name. If
     *     the package name is a {@linkplain #getPackages() package} in the
     *     module then the resource can only be located by the caller of this
     *     method when the package is {@linkplain #isOpen(String,Module) open}
     *     to at least the caller's module. If the resource is not in a
     *     package in the module then the resource is not encapsulated. </li>
     * </ul>
     *
     * <p> In the above, the <em>package name</em> for a resource is derived
     * from the subsequence of characters that precedes the last {@code '/'} in
     * the name and then replacing each {@code '/'} character in the subsequence
     * with {@code '.'}. A leading slash is ignored when deriving the package
     * name. As an example, the package name derived for a resource named
     * "{@code a/b/c/foo.properties}" is "{@code a.b.c}". A resource name
     * with the name "{@code META-INF/MANIFEST.MF}" is never encapsulated
     * because "{@code META-INF}" is not a legal package name. </p>
     *
     * <p> This method returns {@code null} if the resource is not in this
     * module, the resource is encapsulated and cannot be located by the caller,
     * or access to the resource is denied by the security manager. </p>
     *
     * @param  name
     *         The resource name
     *
     * @return An input stream for reading the resource or {@code null}
     *
     * @throws IOException
     *         If an I/O error occurs
     *
     * @see Class#getResourceAsStream(String)
     */
    @CallerSensitive
    public InputStream getResourceAsStream(String name) throws IOException {
        if (name.startsWith("/")) {
            name = name.substring(1);
        }

        if (isNamed() && Resources.canEncapsulate(name)) {
            Module caller = getCallerModule(Reflection.getCallerClass());
            if (caller != this && caller != Object.class.getModule()) {
                String pn = Resources.toPackageName(name);
                if (getPackages().contains(pn)) {
                    if (caller == null && !isOpen(pn)) {
                        // no caller, package not open
                        return null;
                    }
                    if (!isOpen(pn, caller)) {
                        // package not open to caller
                        return null;
                    }
                }
            }
        }

        String mn = this.name;

        // special-case built-in class loaders to avoid URL connection
        if (loader == null) {
            return BootLoader.findResourceAsStream(mn, name);
        } else if (loader instanceof BuiltinClassLoader) {
            return ((BuiltinClassLoader) loader).findResourceAsStream(mn, name);
        }

        // locate resource in module
        URL url = loader.findResource(mn, name);
        if (url != null) {
            try {
                return url.openStream();
            } catch (SecurityException e) { }
        }

        return null;
    }

    /**
     * Returns the string representation of this module. For a named module,
     * the representation is the string {@code "module"}, followed by a space,
     * and then the module name. For an unnamed module, the representation is
     * the string {@code "unnamed module"}, followed by a space, and then an
     * implementation specific string that identifies the unnamed module.
     *
     * @return The string representation of this module
     */
    @Override
    public String toString() {
        if (isNamed()) {
            return "module " + name;
        } else {
            String id = Integer.toHexString(System.identityHashCode(this));
            return "unnamed module @" + id;
        }
    }

    /**
     * Returns the module that a given caller class is a member of. Returns
     * {@code null} if the caller is {@code null}.
     */
    private Module getCallerModule(Class<?> caller) {
        return (caller != null) ? caller.getModule() : null;
    }


    // -- native methods --

    // JVM_DefineModule
    private static native void defineModule0(Module module,
                                             boolean isOpen,
                                             String version,
                                             String location,
                                             String[] pns);

    // JVM_AddReadsModule
    private static native void addReads0(Module from, Module to);

    // JVM_AddModuleExports
    private static native void addExports0(Module from, String pn, Module to);

    // JVM_AddModuleExportsToAll
    private static native void addExportsToAll0(Module from, String pn);

    // JVM_AddModuleExportsToAllUnnamed
    private static native void addExportsToAllUnnamed0(Module from, String pn);
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\ModuleLayer.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.lang.module.Configuration;
import java.lang.module.ModuleDescriptor;
import java.lang.module.ResolvedModule;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jdk.internal.loader.ClassLoaderValue;
import jdk.internal.loader.Loader;
import jdk.internal.loader.LoaderPool;
import jdk.internal.module.ServicesCatalog;
import sun.security.util.SecurityConstants;


/**
 * A layer of modules in the Java virtual machine.
 *
 * <p> A layer is created from a graph of modules in a {@link Configuration}
 * and a function that maps each module to a {@link ClassLoader}.
 * Creating a layer informs the Java virtual machine about the classes that
 * may be loaded from the modules so that the Java virtual machine knows which
 * module that each class is a member of. </p>
 *
 * <p> Creating a layer creates a {@link Module} object for each {@link
 * ResolvedModule} in the configuration. For each resolved module that is
 * {@link ResolvedModule#reads() read}, the {@code Module} {@link
 * Module#canRead reads} the corresponding run-time {@code Module}, which may
 * be in the same layer or a {@link #parents() parent} layer. </p>
 *
 * <p> The {@link #defineModulesWithOneLoader defineModulesWithOneLoader} and
 * {@link #defineModulesWithManyLoaders defineModulesWithManyLoaders} methods
 * provide convenient ways to create a module layer where all modules are
 * mapped to a single class loader or where each module is mapped to its own
 * class loader. The {@link #defineModules defineModules} method is for more
 * advanced cases where modules are mapped to custom class loaders by means of
 * a function specified to the method. Each of these methods has an instance
 * and static variant. The instance methods create a layer with the receiver
 * as the parent layer. The static methods are for more advanced cases where
 * there can be more than one parent layer or where a {@link
 * ModuleLayer.Controller Controller} is needed to control modules in the layer
 * </p>
 *
 * <p> A Java virtual machine has at least one non-empty layer, the {@link
 * #boot() boot} layer, that is created when the Java virtual machine is
 * started. The boot layer contains module {@code java.base} and is the only
 * layer in the Java virtual machine with a module named "{@code java.base}".
 * The modules in the boot layer are mapped to the bootstrap class loader and
 * other class loaders that are <a href="ClassLoader.html#builtinLoaders">
 * built-in</a> into the Java virtual machine. The boot layer will often be
 * the {@link #parents() parent} when creating additional layers. </p>
 *
 * <p> Each {@code Module} in a layer is created so that it {@link
 * Module#isExported(String) exports} and {@link Module#isOpen(String) opens}
 * the packages described by its {@link ModuleDescriptor}. Qualified exports
 * (where a package is exported to a set of target modules rather than all
 * modules) are reified when creating the layer as follows: </p>
 * <ul>
 *     <li> If module {@code X} exports a package to {@code Y}, and if the
 *     runtime {@code Module} {@code X} reads {@code Module} {@code Y}, then
 *     the package is exported to {@code Module} {@code Y} (which may be in
 *     the same layer as {@code X} or a parent layer). </li>
 *
 *     <li> If module {@code X} exports a package to {@code Y}, and if the
 *     runtime {@code Module} {@code X} does not read {@code Y} then target
 *     {@code Y} is located as if by invoking {@link #findModule(String)
 *     findModule} to find the module in the layer or its parent layers. If
 *     {@code Y} is found then the package is exported to the instance of
 *     {@code Y} that was found. If {@code Y} is not found then the qualified
 *     export is ignored. </li>
 * </ul>
 *
 * <p> Qualified opens are handled in same way as qualified exports. </p>
 *
 * <p> As when creating a {@code Configuration},
 * {@link ModuleDescriptor#isAutomatic() automatic} modules receive special
 * treatment when creating a layer. An automatic module is created in the
 * Java virtual machine as a {@code Module} that reads every unnamed {@code
 * Module} in the Java virtual machine. </p>
 *
 * <p> Unless otherwise specified, passing a {@code null} argument to a method
 * in this class causes a {@link NullPointerException NullPointerException} to
 * be thrown. </p>
 *
 * <h3> Example usage: </h3>
 *
 * <p> This example creates a configuration by resolving a module named
 * "{@code myapp}" with the configuration for the boot layer as the parent. It
 * then creates a new layer with the modules in this configuration. All modules
 * are defined to the same class loader. </p>
 *
 * <pre>{@code
 *     ModuleFinder finder = ModuleFinder.of(dir1, dir2, dir3);
 *
 *     ModuleLayer parent = ModuleLayer.boot();
 *
 *     Configuration cf = parent.configuration().resolve(finder, ModuleFinder.of(), Set.of("myapp"));
 *
 *     ClassLoader scl = ClassLoader.getSystemClassLoader();
 *
 *     ModuleLayer layer = parent.defineModulesWithOneLoader(cf, scl);
 *
 *     Class<?> c = layer.findLoader("myapp").loadClass("app.Main");
 * }</pre>
 *
 * @since 9
 * @spec JPMS
 * @see Module#getLayer()
 */

public final class ModuleLayer {

    // the empty layer
    private static final ModuleLayer EMPTY_LAYER
        = new ModuleLayer(Configuration.empty(), List.of(), null);

    // the configuration from which this layer was created
    private final Configuration cf;

    // parent layers, empty in the case of the empty layer
    private final List<ModuleLayer> parents;

    // maps module name to jlr.Module
    private final Map<String, Module> nameToModule;

    /**
     * Creates a new module layer from the modules in the given configuration.
     */
    private ModuleLayer(Configuration cf,
                        List<ModuleLayer> parents,
                        Function<String, ClassLoader> clf)
    {
        this.cf = cf;
        this.parents = parents; // no need to do defensive copy

        Map<String, Module> map;
        if (parents.isEmpty()) {
            map = Collections.emptyMap();
        } else {
            map = Module.defineModules(cf, clf, this);
        }
        this.nameToModule = map; // no need to do defensive copy
    }

    /**
     * Controls a module layer. The static methods defined by {@link ModuleLayer}
     * to create module layers return a {@code Controller} that can be used to
     * control modules in the layer.
     *
     * <p> Unless otherwise specified, passing a {@code null} argument to a
     * method in this class causes a {@link NullPointerException
     * NullPointerException} to be thrown. </p>
     *
     * @apiNote Care should be taken with {@code Controller} objects, they
     * should never be shared with untrusted code.
     *
     * @since 9
     * @spec JPMS
     */
    public static final class Controller {
        private final ModuleLayer layer;

        Controller(ModuleLayer layer) {
            this.layer = layer;
        }

        /**
         * Returns the layer that this object controls.
         *
         * @return the module layer
         */
        public ModuleLayer layer() {
            return layer;
        }

        private void ensureInLayer(Module source) {
            if (source.getLayer() != layer)
                throw new IllegalArgumentException(source + " not in layer");
        }


        /**
         * Updates module {@code source} in the layer to read module
         * {@code target}. This method is a no-op if {@code source} already
         * reads {@code target}.
         *
         * @implNote <em>Read edges</em> added by this method are <em>weak</em>
         * and do not prevent {@code target} from being GC'ed when {@code source}
         * is strongly reachable.
         *
         * @param  source
         *         The source module
         * @param  target
         *         The target module to read
         *
         * @return This controller
         *
         * @throws IllegalArgumentException
         *         If {@code source} is not in the module layer
         *
         * @see Module#addReads
         */
        public Controller addReads(Module source, Module target) {
            ensureInLayer(source);
            source.implAddReads(target);
            return this;
        }

        /**
         * Updates module {@code source} in the layer to export a package to
         * module {@code target}. This method is a no-op if {@code source}
         * already exports the package to at least {@code target}.
         *
         * @param  source
         *         The source module
         * @param  pn
         *         The package name
         * @param  target
         *         The target module
         *
         * @return This controller
         *
         * @throws IllegalArgumentException
         *         If {@code source} is not in the module layer or the package
         *         is not in the source module
         *
         * @see Module#addExports
         */
        public Controller addExports(Module source, String pn, Module target) {
            ensureInLayer(source);
            source.implAddExports(pn, target);
            return this;
        }

        /**
         * Updates module {@code source} in the layer to open a package to
         * module {@code target}. This method is a no-op if {@code source}
         * already opens the package to at least {@code target}.
         *
         * @param  source
         *         The source module
         * @param  pn
         *         The package name
         * @param  target
         *         The target module
         *
         * @return This controller
         *
         * @throws IllegalArgumentException
         *         If {@code source} is not in the module layer or the package
         *         is not in the source module
         *
         * @see Module#addOpens
         */
        public Controller addOpens(Module source, String pn, Module target) {
            ensureInLayer(source);
            source.implAddOpens(pn, target);
            return this;
        }
    }


    /**
     * Creates a new module layer, with this layer as its parent, by defining the
     * modules in the given {@code Configuration} to the Java virtual machine.
     * This method creates one class loader and defines all modules to that
     * class loader. The {@link ClassLoader#getParent() parent} of each class
     * loader is the given parent class loader. This method works exactly as
     * specified by the static {@link
     * #defineModulesWithOneLoader(Configuration,List,ClassLoader)
     * defineModulesWithOneLoader} method when invoked with this layer as the
     * parent. In other words, if this layer is {@code thisLayer} then this
     * method is equivalent to invoking:
     * <pre> {@code
     *     ModuleLayer.defineModulesWithOneLoader(cf, List.of(thisLayer), parentLoader).layer();
     * }</pre>
     *
     * @param  cf
     *         The configuration for the layer
     * @param  parentLoader
     *         The parent class loader for the class loader created by this
     *         method; may be {@code null} for the bootstrap class loader
     *
     * @return The newly created layer
     *
     * @throws IllegalArgumentException
     *         If the given configuration has more than one parent or the parent
     *         of the configuration is not the configuration for this layer
     * @throws LayerInstantiationException
     *         If the layer cannot be created for any of the reasons specified
     *         by the static {@code defineModulesWithOneLoader} method
     * @throws SecurityException
     *         If {@code RuntimePermission("createClassLoader")} or
     *         {@code RuntimePermission("getClassLoader")} is denied by
     *         the security manager
     *
     * @see #findLoader
     */
    public ModuleLayer defineModulesWithOneLoader(Configuration cf,
                                                  ClassLoader parentLoader) {
        return defineModulesWithOneLoader(cf, List.of(this), parentLoader).layer();
    }


    /**
     * Creates a new module layer, with this layer as its parent, by defining the
     * modules in the given {@code Configuration} to the Java virtual machine.
     * Each module is defined to its own {@link ClassLoader} created by this
     * method. The {@link ClassLoader#getParent() parent} of each class loader
     * is the given parent class loader. This method works exactly as specified
     * by the static {@link
     * #defineModulesWithManyLoaders(Configuration,List,ClassLoader)
     * defineModulesWithManyLoaders} method when invoked with this layer as the
     * parent. In other words, if this layer is {@code thisLayer} then this
     * method is equivalent to invoking:
     * <pre> {@code
     *     ModuleLayer.defineModulesWithManyLoaders(cf, List.of(thisLayer), parentLoader).layer();
     * }</pre>
     *
     * @param  cf
     *         The configuration for the layer
     * @param  parentLoader
     *         The parent class loader for each of the class loaders created by
     *         this method; may be {@code null} for the bootstrap class loader
     *
     * @return The newly created layer
     *
     * @throws IllegalArgumentException
     *         If the given configuration has more than one parent or the parent
     *         of the configuration is not the configuration for this layer
     * @throws LayerInstantiationException
     *         If the layer cannot be created for any of the reasons specified
     *         by the static {@code defineModulesWithManyLoaders} method
     * @throws SecurityException
     *         If {@code RuntimePermission("createClassLoader")} or
     *         {@code RuntimePermission("getClassLoader")} is denied by
     *         the security manager
     *
     * @see #findLoader
     */
    public ModuleLayer defineModulesWithManyLoaders(Configuration cf,
                                                    ClassLoader parentLoader) {
        return defineModulesWithManyLoaders(cf, List.of(this), parentLoader).layer();
    }


    /**
     * Creates a new module layer, with this layer as its parent, by defining the
     * modules in the given {@code Configuration} to the Java virtual machine.
     * Each module is mapped, by name, to its class loader by means of the
     * given function. This method works exactly as specified by the static
     * {@link #defineModules(Configuration,List,Function) defineModules}
     * method when invoked with this layer as the parent. In other words, if
     * this layer is {@code thisLayer} then this method is equivalent to
     * invoking:
     * <pre> {@code
     *     ModuleLayer.defineModules(cf, List.of(thisLayer), clf).layer();
     * }</pre>
     *
     * @param  cf
     *         The configuration for the layer
     * @param  clf
     *         The function to map a module name to a class loader
     *
     * @return The newly created layer
     *
     * @throws IllegalArgumentException
     *         If the given configuration has more than one parent or the parent
     *         of the configuration is not the configuration for this layer
     * @throws LayerInstantiationException
     *         If the layer cannot be created for any of the reasons specified
     *         by the static {@code defineModules} method
     * @throws SecurityException
     *         If {@code RuntimePermission("getClassLoader")} is denied by
     *         the security manager
     */
    public ModuleLayer defineModules(Configuration cf,
                                     Function<String, ClassLoader> clf) {
        return defineModules(cf, List.of(this), clf).layer();
    }

    /**
     * Creates a new module layer by defining the modules in the given {@code
     * Configuration} to the Java virtual machine. This method creates one
     * class loader and defines all modules to that class loader.
     *
     * <p> The class loader created by this method implements <em>direct
     * delegation</em> when loading classes from modules. If the {@link
     * ClassLoader#loadClass(String, boolean) loadClass} method is invoked to
     * load a class then it uses the package name of the class to map it to a
     * module. This may be a module in this layer and hence defined to the same
     * class loader. It may be a package in a module in a parent layer that is
     * exported to one or more of the modules in this layer. The class
     * loader delegates to the class loader of the module, throwing {@code
     * ClassNotFoundException} if not found by that class loader.
     * When {@code loadClass} is invoked to load classes that do not map to a
     * module then it delegates to the parent class loader. </p>
     *
     * <p> The class loader created by this method locates resources
     * ({@link ClassLoader#getResource(String) getResource}, {@link
     * ClassLoader#getResources(String) getResources}, and other resource
     * methods) in all modules in the layer before searching the parent class
     * loader. </p>
     *
     * <p> Attempting to create a layer with all modules defined to the same
     * class loader can fail for the following reasons:
     *
     * <ul>
     *
     *     <li><p> <em>Overlapping packages</em>: Two or more modules in the
     *     configuration have the same package. </p></li>
     *
     *     <li><p> <em>Split delegation</em>: The resulting class loader would
     *     need to delegate to more than one class loader in order to load
     *     classes in a specific package. </p></li>
     *
     * </ul>
     *
     * <p> In addition, a layer cannot be created if the configuration contains
     * a module named "{@code java.base}", or a module contains a package named
     * "{@code java}" or a package with a name starting with "{@code java.}". </p>
     *
     * <p> If there is a security manager then the class loader created by
     * this method will load classes and resources with privileges that are
     * restricted by the calling context of this method. </p>
     *
     * @param  cf
     *         The configuration for the layer
     * @param  parentLayers
     *         The list of parent layers in search order
     * @param  parentLoader
     *         The parent class loader for the class loader created by this
     *         method; may be {@code null} for the bootstrap class loader
     *
     * @return A controller that controls the newly created layer
     *
     * @throws IllegalArgumentException
     *         If the parent(s) of the given configuration do not match the
     *         configuration of the parent layers, including order
     * @throws LayerInstantiationException
     *         If all modules cannot be defined to the same class loader for any
     *         of the reasons listed above
     * @throws SecurityException
     *         If {@code RuntimePermission("createClassLoader")} or
     *         {@code RuntimePermission("getClassLoader")} is denied by
     *         the security manager
     *
     * @see #findLoader
     */
    public static Controller defineModulesWithOneLoader(Configuration cf,
                                                        List<ModuleLayer> parentLayers,
                                                        ClassLoader parentLoader)
    {
        List<ModuleLayer> parents = new ArrayList<>(parentLayers);
        checkConfiguration(cf, parents);

        checkCreateClassLoaderPermission();
        checkGetClassLoaderPermission();

        try {
            Loader loader = new Loader(cf.modules(), parentLoader);
            loader.initRemotePackageMap(cf, parents);
            ModuleLayer layer = new ModuleLayer(cf, parents, mn -> loader);
            return new Controller(layer);
        } catch (IllegalArgumentException | IllegalStateException e) {
            throw new LayerInstantiationException(e.getMessage());
        }
    }

    /**
     * Creates a new module layer by defining the modules in the given {@code
     * Configuration} to the Java virtual machine. Each module is defined to
     * its own {@link ClassLoader} created by this method. The {@link
     * ClassLoader#getParent() parent} of each class loader is the given parent
     * class loader.
     *
     * <p> The class loaders created by this method implement <em>direct
     * delegation</em> when loading classes from modules. If the {@link
     * ClassLoader#loadClass(String, boolean) loadClass} method is invoked to
     * load a class then it uses the package name of the class to map it to a
     * module. The package may be in the module defined to the class loader.
     * The package may be exported by another module in this layer to the
     * module defined to the class loader. It may be in a package exported by a
     * module in a parent layer. The class loader delegates to the class loader
     * of the module, throwing {@code ClassNotFoundException} if not found by
     * that class loader. When {@code loadClass} is invoked to load a class
     * that does not map to a module then it delegates to the parent class
     * loader. </p>
     *
     * <p> The class loaders created by this method locate resources
     * ({@link ClassLoader#getResource(String) getResource}, {@link
     * ClassLoader#getResources(String) getResources}, and other resource
     * methods) in the module defined to the class loader before searching
     * the parent class loader. </p>
     *
     * <p> If there is a security manager then the class loaders created by
     * this method will load classes and resources with privileges that are
     * restricted by the calling context of this method. </p>
     *
     * @param  cf
     *         The configuration for the layer
     * @param  parentLayers
     *         The list of parent layers in search order
     * @param  parentLoader
     *         The parent class loader for each of the class loaders created by
     *         this method; may be {@code null} for the bootstrap class loader
     *
     * @return A controller that controls the newly created layer
     *
     * @throws IllegalArgumentException
     *         If the parent(s) of the given configuration do not match the
     *         configuration of the parent layers, including order
     * @throws LayerInstantiationException
     *         If the layer cannot be created because the configuration contains
     *         a module named "{@code java.base}" or a module contains a package
     *         named "{@code java}" or a package with a name starting with
     *         "{@code java.}"
     *
     * @throws SecurityException
     *         If {@code RuntimePermission("createClassLoader")} or
     *         {@code RuntimePermission("getClassLoader")} is denied by
     *         the security manager
     *
     * @see #findLoader
     */
    public static Controller defineModulesWithManyLoaders(Configuration cf,
                                                          List<ModuleLayer> parentLayers,
                                                          ClassLoader parentLoader)
    {
        List<ModuleLayer> parents = new ArrayList<>(parentLayers);
        checkConfiguration(cf, parents);

        checkCreateClassLoaderPermission();
        checkGetClassLoaderPermission();

        LoaderPool pool = new LoaderPool(cf, parents, parentLoader);
        try {
            ModuleLayer layer = new ModuleLayer(cf, parents, pool::loaderFor);
            return new Controller(layer);
        } catch (IllegalArgumentException | IllegalStateException e) {
            throw new LayerInstantiationException(e.getMessage());
        }
    }

    /**
     * Creates a new module layer by defining the modules in the given {@code
     * Configuration} to the Java virtual machine. The given function maps each
     * module in the configuration, by name, to a class loader. Creating the
     * layer informs the Java virtual machine about the classes that may be
     * loaded so that the Java virtual machine knows which module that each
     * class is a member of.
     *
     * <p> The class loader delegation implemented by the class loaders must
     * respect module readability. The class loaders should be
     * {@link ClassLoader#registerAsParallelCapable parallel-capable} so as to
     * avoid deadlocks during class loading. In addition, the entity creating
     * a new layer with this method should arrange that the class loaders be
     * ready to load from these modules before there are any attempts to load
     * classes or resources. </p>
     *
     * <p> Creating a layer can fail for the following reasons: </p>
     *
     * <ul>
     *
     *     <li><p> Two or more modules with the same package are mapped to the
     *     same class loader. </p></li>
     *
     *     <li><p> A module is mapped to a class loader that already has a
     *     module of the same name defined to it. </p></li>
     *
     *     <li><p> A module is mapped to a class loader that has already
     *     defined types in any of the packages in the module. </p></li>
     *
     * </ul>
     *
     * <p> In addition, a layer cannot be created if the configuration contains
     * a module named "{@code java.base}", a configuration contains a module
     * with a package named "{@code java}" or a package name starting with
     * "{@code java.}", or the function to map a module name to a class loader
     * returns {@code null} or the {@linkplain ClassLoader#getPlatformClassLoader()
     * platform class loader}. </p>
     *
     * <p> If the function to map a module name to class loader throws an error
     * or runtime exception then it is propagated to the caller of this method.
     * </p>
     *
     * @apiNote It is implementation specific as to whether creating a layer
     * with this method is an atomic operation or not. Consequentially it is
     * possible for this method to fail with some modules, but not all, defined
     * to the Java virtual machine.
     *
     * @param  cf
     *         The configuration for the layer
     * @param  parentLayers
     *         The list of parent layers in search order
     * @param  clf
     *         The function to map a module name to a class loader
     *
     * @return A controller that controls the newly created layer
     *
     * @throws IllegalArgumentException
     *         If the parent(s) of the given configuration do not match the
     *         configuration of the parent layers, including order
     * @throws LayerInstantiationException
     *         If creating the layer fails for any of the reasons listed above
     * @throws SecurityException
     *         If {@code RuntimePermission("getClassLoader")} is denied by
     *         the security manager
     */
    public static Controller defineModules(Configuration cf,
                                           List<ModuleLayer> parentLayers,
                                           Function<String, ClassLoader> clf)
    {
        List<ModuleLayer> parents = new ArrayList<>(parentLayers);
        checkConfiguration(cf, parents);
        Objects.requireNonNull(clf);

        checkGetClassLoaderPermission();

        // The boot layer is checked during module system initialization
        if (boot() != null) {
            checkForDuplicatePkgs(cf, clf);
        }

        try {
            ModuleLayer layer = new ModuleLayer(cf, parents, clf);
            return new Controller(layer);
        } catch (IllegalArgumentException | IllegalStateException e) {
            throw new LayerInstantiationException(e.getMessage());
        }
    }


    /**
     * Checks that the parent configurations match the configuration of
     * the parent layers.
     */
    private static void checkConfiguration(Configuration cf,
                                           List<ModuleLayer> parentLayers)
    {
        Objects.requireNonNull(cf);

        List<Configuration> parentConfigurations = cf.parents();
        if (parentLayers.size() != parentConfigurations.size())
            throw new IllegalArgumentException("wrong number of parents");

        int index = 0;
        for (ModuleLayer parent : parentLayers) {
            if (parent.configuration() != parentConfigurations.get(index)) {
                throw new IllegalArgumentException(
                        "Parent of configuration != configuration of this Layer");
            }
            index++;
        }
    }

    private static void checkCreateClassLoaderPermission() {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null)
            sm.checkPermission(SecurityConstants.CREATE_CLASSLOADER_PERMISSION);
    }

    private static void checkGetClassLoaderPermission() {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null)
            sm.checkPermission(SecurityConstants.GET_CLASSLOADER_PERMISSION);
    }

    /**
     * Checks a configuration and the module-to-loader mapping to ensure that
     * no two modules mapped to the same class loader have the same package.
     * It also checks that no two automatic modules have the same package.
     *
     * @throws LayerInstantiationException
     */
    private static void checkForDuplicatePkgs(Configuration cf,
                                              Function<String, ClassLoader> clf)
    {
        // HashMap allows null keys
        Map<ClassLoader, Set<String>> loaderToPackages = new HashMap<>();
        for (ResolvedModule resolvedModule : cf.modules()) {
            ModuleDescriptor descriptor = resolvedModule.reference().descriptor();
            ClassLoader loader = clf.apply(descriptor.name());

            Set<String> loaderPackages
                = loaderToPackages.computeIfAbsent(loader, k -> new HashSet<>());

            for (String pkg : descriptor.packages()) {
                boolean added = loaderPackages.add(pkg);
                if (!added) {
                    throw fail("More than one module with package %s mapped" +
                               " to the same class loader", pkg);
                }
            }
        }
    }

    /**
     * Creates a LayerInstantiationException with the a message formatted from
     * the given format string and arguments.
     */
    private static LayerInstantiationException fail(String fmt, Object ... args) {
        String msg = String.format(fmt, args);
        return new LayerInstantiationException(msg);
    }


    /**
     * Returns the configuration for this layer.
     *
     * @return The configuration for this layer
     */
    public Configuration configuration() {
        return cf;
    }


    /**
     * Returns the list of this layer's parents unless this is the
     * {@linkplain #empty empty layer}, which has no parents and so an
     * empty list is returned.
     *
     * @return The list of this layer's parents
     */
    public List<ModuleLayer> parents() {
        return parents;
    }


    /**
     * Returns an ordered stream of layers. The first element is this layer,
     * the remaining elements are the parent layers in DFS order.
     *
     * @implNote For now, the assumption is that the number of elements will
     * be very low and so this method does not use a specialized spliterator.
     */
    Stream<ModuleLayer> layers() {
        List<ModuleLayer> allLayers = this.allLayers;
        if (allLayers != null)
            return allLayers.stream();

        allLayers = new ArrayList<>();
        Set<ModuleLayer> visited = new HashSet<>();
        Deque<ModuleLayer> stack = new ArrayDeque<>();
        visited.add(this);
        stack.push(this);

        while (!stack.isEmpty()) {
            ModuleLayer layer = stack.pop();
            allLayers.add(layer);

            // push in reverse order
            for (int i = layer.parents.size() - 1; i >= 0; i--) {
                ModuleLayer parent = layer.parents.get(i);
                if (!visited.contains(parent)) {
                    visited.add(parent);
                    stack.push(parent);
                }
            }
        }

        this.allLayers = allLayers = Collections.unmodifiableList(allLayers);
        return allLayers.stream();
    }

    private volatile List<ModuleLayer> allLayers;

    /**
     * Returns the set of the modules in this layer.
     *
     * @return A possibly-empty unmodifiable set of the modules in this layer
     */
    public Set<Module> modules() {
        Set<Module> modules = this.modules;
        if (modules == null) {
            this.modules = modules =
                Collections.unmodifiableSet(new HashSet<>(nameToModule.values()));
        }
        return modules;
    }

    private volatile Set<Module> modules;


    /**
     * Returns the module with the given name in this layer, or if not in this
     * layer, the {@linkplain #parents() parent} layers. Finding a module in
     * parent layers is equivalent to invoking {@code findModule} on each
     * parent, in search order, until the module is found or all parents have
     * been searched. In a <em>tree of layers</em>  then this is equivalent to
     * a depth-first search.
     *
     * @param  name
     *         The name of the module to find
     *
     * @return The module with the given name or an empty {@code Optional}
     *         if there isn't a module with this name in this layer or any
     *         parent layer
     */
    public Optional<Module> findModule(String name) {
        Objects.requireNonNull(name);
        if (this == EMPTY_LAYER)
            return Optional.empty();
        Module m = nameToModule.get(name);
        if (m != null)
            return Optional.of(m);

        return layers()
                .skip(1)  // skip this layer
                .map(l -> l.nameToModule.get(name))
                .filter(Objects::nonNull)
                .findAny();
    }


    /**
     * Returns the {@code ClassLoader} for the module with the given name. If
     * a module of the given name is not in this layer then the {@link #parents()
     * parent} layers are searched in the manner specified by {@link
     * #findModule(String) findModule}.
     *
     * <p> If there is a security manager then its {@code checkPermission}
     * method is called with a {@code RuntimePermission("getClassLoader")}
     * permission to check that the caller is allowed to get access to the
     * class loader. </p>
     *
     * @apiNote This method does not return an {@code Optional<ClassLoader>}
     * because `null` must be used to represent the bootstrap class loader.
     *
     * @param  name
     *         The name of the module to find
     *
     * @return The ClassLoader that the module is defined to
     *
     * @throws IllegalArgumentException if a module of the given name is not
     *         defined in this layer or any parent of this layer
     *
     * @throws SecurityException if denied by the security manager
     */
    public ClassLoader findLoader(String name) {
        Optional<Module> om = findModule(name);

        // can't use map(Module::getClassLoader) as class loader can be null
        if (om.isPresent()) {
            return om.get().getClassLoader();
        } else {
            throw new IllegalArgumentException("Module " + name
                                               + " not known to this layer");
        }
    }

    /**
     * Returns a string describing this module layer.
     *
     * @return A possibly empty string describing this module layer
     */
    @Override
    public String toString() {
        return modules().stream()
                .map(Module::getName)
                .collect(Collectors.joining(", "));
    }

    /**
     * Returns the <em>empty</em> layer. There are no modules in the empty
     * layer. It has no parents.
     *
     * @return The empty layer
     */
    public static ModuleLayer empty() {
        return EMPTY_LAYER;
    }


    /**
     * Returns the boot layer. The boot layer contains at least one module,
     * {@code java.base}. Its parent is the {@link #empty() empty} layer.
     *
     * @apiNote This method returns {@code null} during startup and before
     *          the boot layer is fully initialized.
     *
     * @return The boot layer
     */
    public static ModuleLayer boot() {
        return System.bootLayer;
    }

    /**
     * Returns the ServicesCatalog for this Layer, creating it if not
     * already created.
     */
    ServicesCatalog getServicesCatalog() {
        ServicesCatalog servicesCatalog = this.servicesCatalog;
        if (servicesCatalog != null)
            return servicesCatalog;

        synchronized (this) {
            servicesCatalog = this.servicesCatalog;
            if (servicesCatalog == null) {
                servicesCatalog = ServicesCatalog.create();
                nameToModule.values().forEach(servicesCatalog::register);
                this.servicesCatalog = servicesCatalog;
            }
        }

        return servicesCatalog;
    }

    private volatile ServicesCatalog servicesCatalog;


    /**
     * Record that this layer has at least one module defined to the given
     * class loader.
     */
    void bindToLoader(ClassLoader loader) {
        // CLV.computeIfAbsent(loader, (cl, clv) -> new CopyOnWriteArrayList<>())
        List<ModuleLayer> list = CLV.get(loader);
        if (list == null) {
            list = new CopyOnWriteArrayList<>();
            List<ModuleLayer> previous = CLV.putIfAbsent(loader, list);
            if (previous != null) list = previous;
        }
        list.add(this);
    }

    /**
     * Returns a stream of the layers that have at least one module defined to
     * the given class loader.
     */
    static Stream<ModuleLayer> layers(ClassLoader loader) {
        List<ModuleLayer> list = CLV.get(loader);
        if (list != null) {
            return list.stream();
        } else {
            return Stream.empty();
        }
    }

    // the list of layers with modules defined to a class loader
    private static final ClassLoaderValue<List<ModuleLayer>> CLV = new ClassLoaderValue<>();
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\NamedPackage.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang;

import java.lang.module.Configuration;
import java.lang.module.ModuleReference;
import java.net.URI;

/**
 * A NamedPackage represents a package by name in a specific module.
 *
 * A class loader will automatically create NamedPackage for each
 * package when a class is defined.  Package object is lazily
 * defined until Class::getPackage, Package::getPackage(s), or
 * ClassLoader::getDefinedPackage(s) method is called.
 *
 * NamedPackage allows ClassLoader to keep track of the runtime
 * packages with minimal footprint and avoid constructing Package
 * object.
 */
class NamedPackage {
    private final String name;
    private final Module module;

    NamedPackage(String pn, Module module) {
        if (pn.isEmpty() && module.isNamed()) {
            throw new InternalError("unnamed package in  " + module);
        }
        this.name = pn.intern();
        this.module = module;
    }

    /**
     * Returns the name of this package.
     */
    String packageName() {
        return name;
    }

    /**
     * Returns the module of this named package.
     */
    Module module() {
        return module;
    }

    /**
     * Returns the location of the module if this named package is in
     * a named module; otherwise, returns null.
     */
    URI location() {
        if (module.isNamed() && module.getLayer() != null) {
            Configuration cf = module.getLayer().configuration();
            ModuleReference mref
                = cf.findModule(module.getName()).get().reference();
            return mref.location().orElse(null);
        }
        return null;
    }

    /**
     * Creates a Package object of the given name and module.
     */
    static Package toPackage(String name, Module module) {
        return new Package(name, module);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\NegativeArraySizeException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1994, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown if an application tries to create an array with negative size.
 *
 * @author  unascribed
 * @since   1.0
 */
public
class NegativeArraySizeException extends RuntimeException {
    private static final long serialVersionUID = -8960118058596991861L;

    /**
     * Constructs a <code>NegativeArraySizeException</code> with no
     * detail message.
     */
    public NegativeArraySizeException() {
        super();
    }

    /**
     * Constructs a <code>NegativeArraySizeException</code> with the
     * specified detail message.
     *
     * @param   s   the detail message.
     */
    public NegativeArraySizeException(String s) {
        super(s);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\NoClassDefFoundError.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1994, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown if the Java Virtual Machine or a <code>ClassLoader</code> instance
 * tries to load in the definition of a class (as part of a normal method call
 * or as part of creating a new instance using the <code>new</code> expression)
 * and no definition of the class could be found.
 * <p>
 * The searched-for class definition existed when the currently
 * executing class was compiled, but the definition can no longer be
 * found.
 *
 * @author  unascribed
 * @since   1.0
 */
public
class NoClassDefFoundError extends LinkageError {
    private static final long serialVersionUID = 9095859863287012458L;

    /**
     * Constructs a <code>NoClassDefFoundError</code> with no detail message.
     */
    public NoClassDefFoundError() {
        super();
    }

    /**
     * Constructs a <code>NoClassDefFoundError</code> with the specified
     * detail message.
     *
     * @param   s   the detail message.
     */
    public NoClassDefFoundError(String s) {
        super(s);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\NoSuchFieldError.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown if an application tries to access or modify a specified
 * field of an object, and that object no longer has that field.
 * <p>
 * Normally, this error is caught by the compiler; this error can
 * only occur at run time if the definition of a class has
 * incompatibly changed.
 *
 * @author  unascribed
 * @since   1.0
 */
public
class NoSuchFieldError extends IncompatibleClassChangeError {
    private static final long serialVersionUID = -3456430195886129035L;

    /**
     * Constructs a <code>NoSuchFieldError</code> with no detail message.
     */
    public NoSuchFieldError() {
        super();
    }

    /**
     * Constructs a <code>NoSuchFieldError</code> with the specified
     * detail message.
     *
     * @param   s   the detail message.
     */
    public NoSuchFieldError(String s) {
        super(s);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\NoSuchFieldException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1996, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Signals that the class doesn't have a field of a specified name.
 *
 * @author  unascribed
 * @since   1.1
 */
public class NoSuchFieldException extends ReflectiveOperationException {
    private static final long serialVersionUID = -6143714805279938260L;

    /**
     * Constructor.
     */
    public NoSuchFieldException() {
        super();
    }

    /**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public NoSuchFieldException(String s) {
        super(s);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\NoSuchMethodError.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1994, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown if an application tries to call a specified method of a
 * class (either static or instance), and that class no longer has a
 * definition of that method.
 * <p>
 * Normally, this error is caught by the compiler; this error can
 * only occur at run time if the definition of a class has
 * incompatibly changed.
 *
 * @author  unascribed
 * @since   1.0
 */
public
class NoSuchMethodError extends IncompatibleClassChangeError {
    private static final long serialVersionUID = -3765521442372831335L;

    /**
     * Constructs a <code>NoSuchMethodError</code> with no detail message.
     */
    public NoSuchMethodError() {
        super();
    }

    /**
     * Constructs a <code>NoSuchMethodError</code> with the
     * specified detail message.
     *
     * @param   s   the detail message.
     */
    public NoSuchMethodError(String s) {
        super(s);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\NoSuchMethodException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown when a particular method cannot be found.
 *
 * @author     unascribed
 * @since      1.0
 */
public
class NoSuchMethodException extends ReflectiveOperationException {
    private static final long serialVersionUID = 5034388446362600923L;

    /**
     * Constructs a <code>NoSuchMethodException</code> without a detail message.
     */
    public NoSuchMethodException() {
        super();
    }

    /**
     * Constructs a <code>NoSuchMethodException</code> with a detail message.
     *
     * @param      s   the detail message.
     */
    public NoSuchMethodException(String s) {
        super(s);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\NullPointerException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1994, 2011, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
