     *     <td>{@code CARRIAGE RETURN}</td></tr>
     * <tr><th scope="row">{@code ' '}</th>  <td>{@code U+0020}</td>
     *     <td>{@code SPACE}</td></tr>
     * </tbody>
     * </table>
     *
     * @param      ch   the character to be tested.
     * @return     {@code true} if the character is ISO-LATIN-1 white
     *             space; {@code false} otherwise.
     * @see        Character#isSpaceChar(char)
     * @see        Character#isWhitespace(char)
     * @deprecated Replaced by isWhitespace(char).
     */
    @Deprecated(since="1.1")
    public static boolean isSpace(char ch) {
        return (ch <= 0x0020) &&
            (((((1L << 0x0009) |
            (1L << 0x000A) |
            (1L << 0x000C) |
            (1L << 0x000D) |
            (1L << 0x0020)) >> ch) & 1L) != 0);
    }


    /**
     * Determines if the specified character is a Unicode space character.
     * A character is considered to be a space character if and only if
     * it is specified to be a space character by the Unicode Standard. This
     * method returns true if the character's general category type is any of
     * the following:
     * <ul>
     * <li> {@code SPACE_SEPARATOR}
     * <li> {@code LINE_SEPARATOR}
     * <li> {@code PARAGRAPH_SEPARATOR}
     * </ul>
     *
     * <p><b>Note:</b> This method cannot handle <a
     * href="#supplementary"> supplementary characters</a>. To support
     * all Unicode characters, including supplementary characters, use
     * the {@link #isSpaceChar(int)} method.
     *
     * @param   ch      the character to be tested.
     * @return  {@code true} if the character is a space character;
     *          {@code false} otherwise.
     * @see     Character#isWhitespace(char)
     * @since   1.1
     */
    public static boolean isSpaceChar(char ch) {
        return isSpaceChar((int)ch);
    }

    /**
     * Determines if the specified character (Unicode code point) is a
     * Unicode space character.  A character is considered to be a
     * space character if and only if it is specified to be a space
     * character by the Unicode Standard. This method returns true if
     * the character's general category type is any of the following:
     *
     * <ul>
     * <li> {@link #SPACE_SEPARATOR}
     * <li> {@link #LINE_SEPARATOR}
     * <li> {@link #PARAGRAPH_SEPARATOR}
     * </ul>
     *
     * @param   codePoint the character (Unicode code point) to be tested.
     * @return  {@code true} if the character is a space character;
     *          {@code false} otherwise.
     * @see     Character#isWhitespace(int)
     * @since   1.5
     */
    public static boolean isSpaceChar(int codePoint) {
        return ((((1 << Character.SPACE_SEPARATOR) |
                  (1 << Character.LINE_SEPARATOR) |
                  (1 << Character.PARAGRAPH_SEPARATOR)) >> getType(codePoint)) & 1)
            != 0;
    }

    /**
     * Determines if the specified character is white space according to Java.
     * A character is a Java whitespace character if and only if it satisfies
     * one of the following criteria:
     * <ul>
     * <li> It is a Unicode space character ({@code SPACE_SEPARATOR},
     *      {@code LINE_SEPARATOR}, or {@code PARAGRAPH_SEPARATOR})
     *      but is not also a non-breaking space ({@code '\u005Cu00A0'},
     *      {@code '\u005Cu2007'}, {@code '\u005Cu202F'}).
     * <li> It is {@code '\u005Ct'}, U+0009 HORIZONTAL TABULATION.
     * <li> It is {@code '\u005Cn'}, U+000A LINE FEED.
     * <li> It is {@code '\u005Cu000B'}, U+000B VERTICAL TABULATION.
     * <li> It is {@code '\u005Cf'}, U+000C FORM FEED.
     * <li> It is {@code '\u005Cr'}, U+000D CARRIAGE RETURN.
     * <li> It is {@code '\u005Cu001C'}, U+001C FILE SEPARATOR.
     * <li> It is {@code '\u005Cu001D'}, U+001D GROUP SEPARATOR.
     * <li> It is {@code '\u005Cu001E'}, U+001E RECORD SEPARATOR.
     * <li> It is {@code '\u005Cu001F'}, U+001F UNIT SEPARATOR.
     * </ul>
     *
     * <p><b>Note:</b> This method cannot handle <a
     * href="#supplementary"> supplementary characters</a>. To support
     * all Unicode characters, including supplementary characters, use
     * the {@link #isWhitespace(int)} method.
     *
     * @param   ch the character to be tested.
     * @return  {@code true} if the character is a Java whitespace
     *          character; {@code false} otherwise.
     * @see     Character#isSpaceChar(char)
     * @since   1.1
     */
    public static boolean isWhitespace(char ch) {
        return isWhitespace((int)ch);
    }

    /**
     * Determines if the specified character (Unicode code point) is
     * white space according to Java.  A character is a Java
     * whitespace character if and only if it satisfies one of the
     * following criteria:
     * <ul>
     * <li> It is a Unicode space character ({@link #SPACE_SEPARATOR},
     *      {@link #LINE_SEPARATOR}, or {@link #PARAGRAPH_SEPARATOR})
     *      but is not also a non-breaking space ({@code '\u005Cu00A0'},
     *      {@code '\u005Cu2007'}, {@code '\u005Cu202F'}).
     * <li> It is {@code '\u005Ct'}, U+0009 HORIZONTAL TABULATION.
     * <li> It is {@code '\u005Cn'}, U+000A LINE FEED.
     * <li> It is {@code '\u005Cu000B'}, U+000B VERTICAL TABULATION.
     * <li> It is {@code '\u005Cf'}, U+000C FORM FEED.
     * <li> It is {@code '\u005Cr'}, U+000D CARRIAGE RETURN.
     * <li> It is {@code '\u005Cu001C'}, U+001C FILE SEPARATOR.
     * <li> It is {@code '\u005Cu001D'}, U+001D GROUP SEPARATOR.
     * <li> It is {@code '\u005Cu001E'}, U+001E RECORD SEPARATOR.
     * <li> It is {@code '\u005Cu001F'}, U+001F UNIT SEPARATOR.
     * </ul>
     *
     * @param   codePoint the character (Unicode code point) to be tested.
     * @return  {@code true} if the character is a Java whitespace
     *          character; {@code false} otherwise.
     * @see     Character#isSpaceChar(int)
     * @since   1.5
     */
    public static boolean isWhitespace(int codePoint) {
        return CharacterData.of(codePoint).isWhitespace(codePoint);
    }

    /**
     * Determines if the specified character is an ISO control
     * character.  A character is considered to be an ISO control
     * character if its code is in the range {@code '\u005Cu0000'}
     * through {@code '\u005Cu001F'} or in the range
     * {@code '\u005Cu007F'} through {@code '\u005Cu009F'}.
     *
     * <p><b>Note:</b> This method cannot handle <a
     * href="#supplementary"> supplementary characters</a>. To support
     * all Unicode characters, including supplementary characters, use
     * the {@link #isISOControl(int)} method.
     *
     * @param   ch      the character to be tested.
     * @return  {@code true} if the character is an ISO control character;
     *          {@code false} otherwise.
     *
     * @see     Character#isSpaceChar(char)
     * @see     Character#isWhitespace(char)
     * @since   1.1
     */
    public static boolean isISOControl(char ch) {
        return isISOControl((int)ch);
    }

    /**
     * Determines if the referenced character (Unicode code point) is an ISO control
     * character.  A character is considered to be an ISO control
     * character if its code is in the range {@code '\u005Cu0000'}
     * through {@code '\u005Cu001F'} or in the range
     * {@code '\u005Cu007F'} through {@code '\u005Cu009F'}.
     *
     * @param   codePoint the character (Unicode code point) to be tested.
     * @return  {@code true} if the character is an ISO control character;
     *          {@code false} otherwise.
     * @see     Character#isSpaceChar(int)
     * @see     Character#isWhitespace(int)
     * @since   1.5
     */
    public static boolean isISOControl(int codePoint) {
        // Optimized form of:
        //     (codePoint >= 0x00 && codePoint <= 0x1F) ||
        //     (codePoint >= 0x7F && codePoint <= 0x9F);
        return codePoint <= 0x9F &&
            (codePoint >= 0x7F || (codePoint >>> 5 == 0));
    }

    /**
     * Returns a value indicating a character's general category.
     *
     * <p><b>Note:</b> This method cannot handle <a
     * href="#supplementary"> supplementary characters</a>. To support
     * all Unicode characters, including supplementary characters, use
     * the {@link #getType(int)} method.
     *
     * @param   ch      the character to be tested.
     * @return  a value of type {@code int} representing the
     *          character's general category.
     * @see     Character#COMBINING_SPACING_MARK
     * @see     Character#CONNECTOR_PUNCTUATION
     * @see     Character#CONTROL
     * @see     Character#CURRENCY_SYMBOL
     * @see     Character#DASH_PUNCTUATION
     * @see     Character#DECIMAL_DIGIT_NUMBER
     * @see     Character#ENCLOSING_MARK
     * @see     Character#END_PUNCTUATION
     * @see     Character#FINAL_QUOTE_PUNCTUATION
     * @see     Character#FORMAT
     * @see     Character#INITIAL_QUOTE_PUNCTUATION
     * @see     Character#LETTER_NUMBER
     * @see     Character#LINE_SEPARATOR
     * @see     Character#LOWERCASE_LETTER
     * @see     Character#MATH_SYMBOL
     * @see     Character#MODIFIER_LETTER
     * @see     Character#MODIFIER_SYMBOL
     * @see     Character#NON_SPACING_MARK
     * @see     Character#OTHER_LETTER
     * @see     Character#OTHER_NUMBER
     * @see     Character#OTHER_PUNCTUATION
     * @see     Character#OTHER_SYMBOL
     * @see     Character#PARAGRAPH_SEPARATOR
     * @see     Character#PRIVATE_USE
     * @see     Character#SPACE_SEPARATOR
     * @see     Character#START_PUNCTUATION
     * @see     Character#SURROGATE
     * @see     Character#TITLECASE_LETTER
     * @see     Character#UNASSIGNED
     * @see     Character#UPPERCASE_LETTER
     * @since   1.1
     */
    public static int getType(char ch) {
        return getType((int)ch);
    }

    /**
     * Returns a value indicating a character's general category.
     *
     * @param   codePoint the character (Unicode code point) to be tested.
     * @return  a value of type {@code int} representing the
     *          character's general category.
     * @see     Character#COMBINING_SPACING_MARK COMBINING_SPACING_MARK
     * @see     Character#CONNECTOR_PUNCTUATION CONNECTOR_PUNCTUATION
     * @see     Character#CONTROL CONTROL
     * @see     Character#CURRENCY_SYMBOL CURRENCY_SYMBOL
     * @see     Character#DASH_PUNCTUATION DASH_PUNCTUATION
     * @see     Character#DECIMAL_DIGIT_NUMBER DECIMAL_DIGIT_NUMBER
     * @see     Character#ENCLOSING_MARK ENCLOSING_MARK
     * @see     Character#END_PUNCTUATION END_PUNCTUATION
     * @see     Character#FINAL_QUOTE_PUNCTUATION FINAL_QUOTE_PUNCTUATION
     * @see     Character#FORMAT FORMAT
     * @see     Character#INITIAL_QUOTE_PUNCTUATION INITIAL_QUOTE_PUNCTUATION
     * @see     Character#LETTER_NUMBER LETTER_NUMBER
     * @see     Character#LINE_SEPARATOR LINE_SEPARATOR
     * @see     Character#LOWERCASE_LETTER LOWERCASE_LETTER
     * @see     Character#MATH_SYMBOL MATH_SYMBOL
     * @see     Character#MODIFIER_LETTER MODIFIER_LETTER
     * @see     Character#MODIFIER_SYMBOL MODIFIER_SYMBOL
     * @see     Character#NON_SPACING_MARK NON_SPACING_MARK
     * @see     Character#OTHER_LETTER OTHER_LETTER
     * @see     Character#OTHER_NUMBER OTHER_NUMBER
     * @see     Character#OTHER_PUNCTUATION OTHER_PUNCTUATION
     * @see     Character#OTHER_SYMBOL OTHER_SYMBOL
     * @see     Character#PARAGRAPH_SEPARATOR PARAGRAPH_SEPARATOR
     * @see     Character#PRIVATE_USE PRIVATE_USE
     * @see     Character#SPACE_SEPARATOR SPACE_SEPARATOR
     * @see     Character#START_PUNCTUATION START_PUNCTUATION
     * @see     Character#SURROGATE SURROGATE
     * @see     Character#TITLECASE_LETTER TITLECASE_LETTER
     * @see     Character#UNASSIGNED UNASSIGNED
     * @see     Character#UPPERCASE_LETTER UPPERCASE_LETTER
     * @since   1.5
     */
    public static int getType(int codePoint) {
        return CharacterData.of(codePoint).getType(codePoint);
    }

    /**
     * Determines the character representation for a specific digit in
     * the specified radix. If the value of {@code radix} is not a
     * valid radix, or the value of {@code digit} is not a valid
     * digit in the specified radix, the null character
     * ({@code '\u005Cu0000'}) is returned.
     * <p>
     * The {@code radix} argument is valid if it is greater than or
     * equal to {@code MIN_RADIX} and less than or equal to
     * {@code MAX_RADIX}. The {@code digit} argument is valid if
     * {@code 0 <= digit < radix}.
     * <p>
     * If the digit is less than 10, then
     * {@code '0' + digit} is returned. Otherwise, the value
     * {@code 'a' + digit - 10} is returned.
     *
     * @param   digit   the number to convert to a character.
     * @param   radix   the radix.
     * @return  the {@code char} representation of the specified digit
     *          in the specified radix.
     * @see     Character#MIN_RADIX
     * @see     Character#MAX_RADIX
     * @see     Character#digit(char, int)
     */
    public static char forDigit(int digit, int radix) {
        if ((digit >= radix) || (digit < 0)) {
            return '\0';
        }
        if ((radix < Character.MIN_RADIX) || (radix > Character.MAX_RADIX)) {
            return '\0';
        }
        if (digit < 10) {
            return (char)('0' + digit);
        }
        return (char)('a' - 10 + digit);
    }

    /**
     * Returns the Unicode directionality property for the given
     * character.  Character directionality is used to calculate the
     * visual ordering of text. The directionality value of undefined
     * {@code char} values is {@code DIRECTIONALITY_UNDEFINED}.
     *
     * <p><b>Note:</b> This method cannot handle <a
     * href="#supplementary"> supplementary characters</a>. To support
     * all Unicode characters, including supplementary characters, use
     * the {@link #getDirectionality(int)} method.
     *
     * @param  ch {@code char} for which the directionality property
     *            is requested.
     * @return the directionality property of the {@code char} value.
     *
     * @see Character#DIRECTIONALITY_UNDEFINED
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR
     * @see Character#DIRECTIONALITY_ARABIC_NUMBER
     * @see Character#DIRECTIONALITY_COMMON_NUMBER_SEPARATOR
     * @see Character#DIRECTIONALITY_NONSPACING_MARK
     * @see Character#DIRECTIONALITY_BOUNDARY_NEUTRAL
     * @see Character#DIRECTIONALITY_PARAGRAPH_SEPARATOR
     * @see Character#DIRECTIONALITY_SEGMENT_SEPARATOR
     * @see Character#DIRECTIONALITY_WHITESPACE
     * @see Character#DIRECTIONALITY_OTHER_NEUTRALS
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE
     * @see Character#DIRECTIONALITY_POP_DIRECTIONAL_FORMAT
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT_ISOLATE
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_ISOLATE
     * @see Character#DIRECTIONALITY_FIRST_STRONG_ISOLATE
     * @see Character#DIRECTIONALITY_POP_DIRECTIONAL_ISOLATE
     * @since 1.4
     */
    public static byte getDirectionality(char ch) {
        return getDirectionality((int)ch);
    }

    /**
     * Returns the Unicode directionality property for the given
     * character (Unicode code point).  Character directionality is
     * used to calculate the visual ordering of text. The
     * directionality value of undefined character is {@link
     * #DIRECTIONALITY_UNDEFINED}.
     *
     * @param   codePoint the character (Unicode code point) for which
     *          the directionality property is requested.
     * @return the directionality property of the character.
     *
     * @see Character#DIRECTIONALITY_UNDEFINED DIRECTIONALITY_UNDEFINED
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT DIRECTIONALITY_LEFT_TO_RIGHT
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT DIRECTIONALITY_RIGHT_TO_LEFT
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER DIRECTIONALITY_EUROPEAN_NUMBER
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR DIRECTIONALITY_EUROPEAN_NUMBER_SEPARATOR
     * @see Character#DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR DIRECTIONALITY_EUROPEAN_NUMBER_TERMINATOR
     * @see Character#DIRECTIONALITY_ARABIC_NUMBER DIRECTIONALITY_ARABIC_NUMBER
     * @see Character#DIRECTIONALITY_COMMON_NUMBER_SEPARATOR DIRECTIONALITY_COMMON_NUMBER_SEPARATOR
     * @see Character#DIRECTIONALITY_NONSPACING_MARK DIRECTIONALITY_NONSPACING_MARK
     * @see Character#DIRECTIONALITY_BOUNDARY_NEUTRAL DIRECTIONALITY_BOUNDARY_NEUTRAL
     * @see Character#DIRECTIONALITY_PARAGRAPH_SEPARATOR DIRECTIONALITY_PARAGRAPH_SEPARATOR
     * @see Character#DIRECTIONALITY_SEGMENT_SEPARATOR DIRECTIONALITY_SEGMENT_SEPARATOR
     * @see Character#DIRECTIONALITY_WHITESPACE DIRECTIONALITY_WHITESPACE
     * @see Character#DIRECTIONALITY_OTHER_NEUTRALS DIRECTIONALITY_OTHER_NEUTRALS
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE
     * @see Character#DIRECTIONALITY_POP_DIRECTIONAL_FORMAT DIRECTIONALITY_POP_DIRECTIONAL_FORMAT
     * @see Character#DIRECTIONALITY_LEFT_TO_RIGHT_ISOLATE DIRECTIONALITY_LEFT_TO_RIGHT_ISOLATE
     * @see Character#DIRECTIONALITY_RIGHT_TO_LEFT_ISOLATE DIRECTIONALITY_RIGHT_TO_LEFT_ISOLATE
     * @see Character#DIRECTIONALITY_FIRST_STRONG_ISOLATE DIRECTIONALITY_FIRST_STRONG_ISOLATE
     * @see Character#DIRECTIONALITY_POP_DIRECTIONAL_ISOLATE DIRECTIONALITY_POP_DIRECTIONAL_ISOLATE
     * @since    1.5
     */
    public static byte getDirectionality(int codePoint) {
        return CharacterData.of(codePoint).getDirectionality(codePoint);
    }

    /**
     * Determines whether the character is mirrored according to the
     * Unicode specification.  Mirrored characters should have their
     * glyphs horizontally mirrored when displayed in text that is
     * right-to-left.  For example, {@code '\u005Cu0028'} LEFT
     * PARENTHESIS is semantically defined to be an <i>opening
     * parenthesis</i>.  This will appear as a "(" in text that is
     * left-to-right but as a ")" in text that is right-to-left.
     *
     * <p><b>Note:</b> This method cannot handle <a
     * href="#supplementary"> supplementary characters</a>. To support
     * all Unicode characters, including supplementary characters, use
     * the {@link #isMirrored(int)} method.
     *
     * @param  ch {@code char} for which the mirrored property is requested
     * @return {@code true} if the char is mirrored, {@code false}
     *         if the {@code char} is not mirrored or is not defined.
     * @since 1.4
     */
    public static boolean isMirrored(char ch) {
        return isMirrored((int)ch);
    }

    /**
     * Determines whether the specified character (Unicode code point)
     * is mirrored according to the Unicode specification.  Mirrored
     * characters should have their glyphs horizontally mirrored when
     * displayed in text that is right-to-left.  For example,
     * {@code '\u005Cu0028'} LEFT PARENTHESIS is semantically
     * defined to be an <i>opening parenthesis</i>.  This will appear
     * as a "(" in text that is left-to-right but as a ")" in text
     * that is right-to-left.
     *
     * @param   codePoint the character (Unicode code point) to be tested.
     * @return  {@code true} if the character is mirrored, {@code false}
     *          if the character is not mirrored or is not defined.
     * @since   1.5
     */
    public static boolean isMirrored(int codePoint) {
        return CharacterData.of(codePoint).isMirrored(codePoint);
    }

    /**
     * Compares two {@code Character} objects numerically.
     *
     * @param   anotherCharacter   the {@code Character} to be compared.

     * @return  the value {@code 0} if the argument {@code Character}
     *          is equal to this {@code Character}; a value less than
     *          {@code 0} if this {@code Character} is numerically less
     *          than the {@code Character} argument; and a value greater than
     *          {@code 0} if this {@code Character} is numerically greater
     *          than the {@code Character} argument (unsigned comparison).
     *          Note that this is strictly a numerical comparison; it is not
     *          locale-dependent.
     * @since   1.2
     */
    public int compareTo(Character anotherCharacter) {
        return compare(this.value, anotherCharacter.value);
    }

    /**
     * Compares two {@code char} values numerically.
     * The value returned is identical to what would be returned by:
     * <pre>
     *    Character.valueOf(x).compareTo(Character.valueOf(y))
     * </pre>
     *
     * @param  x the first {@code char} to compare
     * @param  y the second {@code char} to compare
     * @return the value {@code 0} if {@code x == y};
     *         a value less than {@code 0} if {@code x < y}; and
     *         a value greater than {@code 0} if {@code x > y}
     * @since 1.7
     */
    public static int compare(char x, char y) {
        return x - y;
    }

    /**
     * Converts the character (Unicode code point) argument to uppercase using
     * information from the UnicodeData file.
     *
     * @param   codePoint   the character (Unicode code point) to be converted.
     * @return  either the uppercase equivalent of the character, if
     *          any, or an error flag ({@code Character.ERROR})
     *          that indicates that a 1:M {@code char} mapping exists.
     * @see     Character#isLowerCase(char)
     * @see     Character#isUpperCase(char)
     * @see     Character#toLowerCase(char)
     * @see     Character#toTitleCase(char)
     * @since 1.4
     */
    static int toUpperCaseEx(int codePoint) {
        assert isValidCodePoint(codePoint);
        return CharacterData.of(codePoint).toUpperCaseEx(codePoint);
    }

    /**
     * Converts the character (Unicode code point) argument to uppercase using case
     * mapping information from the SpecialCasing file in the Unicode
     * specification. If a character has no explicit uppercase
     * mapping, then the {@code char} itself is returned in the
     * {@code char[]}.
     *
     * @param   codePoint   the character (Unicode code point) to be converted.
     * @return a {@code char[]} with the uppercased character.
     * @since 1.4
     */
    static char[] toUpperCaseCharArray(int codePoint) {
        // As of Unicode 6.0, 1:M uppercasings only happen in the BMP.
        assert isBmpCodePoint(codePoint);
        return CharacterData.of(codePoint).toUpperCaseCharArray(codePoint);
    }

    /**
     * The number of bits used to represent a {@code char} value in unsigned
     * binary form, constant {@code 16}.
     *
     * @since 1.5
     */
    public static final int SIZE = 16;

    /**
     * The number of bytes used to represent a {@code char} value in unsigned
     * binary form.
     *
     * @since 1.8
     */
    public static final int BYTES = SIZE / Byte.SIZE;

    /**
     * Returns the value obtained by reversing the order of the bytes in the
     * specified {@code char} value.
     *
     * @param ch The {@code char} of which to reverse the byte order.
     * @return the value obtained by reversing (or, equivalently, swapping)
     *     the bytes in the specified {@code char} value.
     * @since 1.5
     */
    @HotSpotIntrinsicCandidate
    public static char reverseBytes(char ch) {
        return (char) (((ch & 0xFF00) >> 8) | (ch << 8));
    }

    /**
     * Returns the Unicode name of the specified character
     * {@code codePoint}, or null if the code point is
     * {@link #UNASSIGNED unassigned}.
     * <p>
     * Note: if the specified character is not assigned a name by
     * the <i>UnicodeData</i> file (part of the Unicode Character
     * Database maintained by the Unicode Consortium), the returned
     * name is the same as the result of expression.
     *
     * <blockquote>{@code
     *     Character.UnicodeBlock.of(codePoint).toString().replace('_', ' ')
     *     + " "
     *     + Integer.toHexString(codePoint).toUpperCase(Locale.ROOT);
     *
     * }</blockquote>
     *
     * @param  codePoint the character (Unicode code point)
     *
     * @return the Unicode name of the specified character, or null if
     *         the code point is unassigned.
     *
     * @throws IllegalArgumentException if the specified
     *            {@code codePoint} is not a valid Unicode
     *            code point.
     *
     * @since 1.7
     */
    public static String getName(int codePoint) {
        if (!isValidCodePoint(codePoint)) {
            throw new IllegalArgumentException(
                String.format("Not a valid Unicode code point: 0x%X", codePoint));
        }
        String name = CharacterName.getInstance().getName(codePoint);
        if (name != null)
            return name;
        if (getType(codePoint) == UNASSIGNED)
            return null;
        UnicodeBlock block = UnicodeBlock.of(codePoint);
        if (block != null)
            return block.toString().replace('_', ' ') + " "
                   + Integer.toHexString(codePoint).toUpperCase(Locale.ROOT);
        // should never come here
        return Integer.toHexString(codePoint).toUpperCase(Locale.ROOT);
    }

    /**
     * Returns the code point value of the Unicode character specified by
     * the given Unicode character name.
     * <p>
     * Note: if a character is not assigned a name by the <i>UnicodeData</i>
     * file (part of the Unicode Character Database maintained by the Unicode
     * Consortium), its name is defined as the result of expression
     *
     * <blockquote>{@code
     *     Character.UnicodeBlock.of(codePoint).toString().replace('_', ' ')
     *     + " "
     *     + Integer.toHexString(codePoint).toUpperCase(Locale.ROOT);
     *
     * }</blockquote>
     * <p>
     * The {@code name} matching is case insensitive, with any leading and
     * trailing whitespace character removed.
     *
     * @param  name the Unicode character name
     *
     * @return the code point value of the character specified by its name.
     *
     * @throws IllegalArgumentException if the specified {@code name}
     *         is not a valid Unicode character name.
     * @throws NullPointerException if {@code name} is {@code null}
     *
     * @since 9
     */
    public static int codePointOf(String name) {
        name = name.trim().toUpperCase(Locale.ROOT);
        int cp = CharacterName.getInstance().getCodePoint(name);
        if (cp != -1)
            return cp;
        try {
            int off = name.lastIndexOf(' ');
            if (off != -1) {
                cp = Integer.parseInt(name, off + 1, name.length(), 16);
                if (isValidCodePoint(cp) && name.equals(getName(cp)))
                    return cp;
            }
        } catch (Exception x) {}
        throw new IllegalArgumentException("Unrecognized character name :" + name);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\CharacterData.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2006, 2011, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

abstract class CharacterData {
    abstract int getProperties(int ch);
    abstract int getType(int ch);
    abstract boolean isWhitespace(int ch);
    abstract boolean isMirrored(int ch);
    abstract boolean isJavaIdentifierStart(int ch);
    abstract boolean isJavaIdentifierPart(int ch);
    abstract boolean isUnicodeIdentifierStart(int ch);
    abstract boolean isUnicodeIdentifierPart(int ch);
    abstract boolean isIdentifierIgnorable(int ch);
    abstract int toLowerCase(int ch);
    abstract int toUpperCase(int ch);
    abstract int toTitleCase(int ch);
    abstract int digit(int ch, int radix);
    abstract int getNumericValue(int ch);
    abstract byte getDirectionality(int ch);

    //need to implement for JSR204
    int toUpperCaseEx(int ch) {
        return toUpperCase(ch);
    }

    char[] toUpperCaseCharArray(int ch) {
        return null;
    }

    boolean isOtherLowercase(int ch) {
        return false;
    }

    boolean isOtherUppercase(int ch) {
        return false;
    }

    boolean isOtherAlphabetic(int ch) {
        return false;
    }

    boolean isIdeographic(int ch) {
        return false;
    }

    // Character <= 0xff (basic latin) is handled by internal fast-path
    // to avoid initializing large tables.
    // Note: performance of this "fast-path" code may be sub-optimal
    // in negative cases for some accessors due to complicated ranges.
    // Should revisit after optimization of table initialization.

    static final CharacterData of(int ch) {
        if (ch >>> 8 == 0) {     // fast-path
            return CharacterDataLatin1.instance;
        } else {
            switch(ch >>> 16) {  //plane 00-16
            case(0):
                return CharacterData00.instance;
            case(1):
                return CharacterData01.instance;
            case(2):
                return CharacterData02.instance;
            case(14):
                return CharacterData0E.instance;
            case(15):   // Private Use
            case(16):   // Private Use
                return CharacterDataPrivateUse.instance;
            default:
                return CharacterDataUndefined.instance;
            }
        }
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\CharacterData00.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
// This file was generated AUTOMATICALLY from a template file Thu Jan 17 21:22:20 PST 2019
/*
 * Copyright (c) 2003, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/** 
 * The CharacterData00 class encapsulates the large tables once found in
 * java.lang.Character
*/

class CharacterData00 extends CharacterData {
    /* The character properties are currently encoded into 32 bits in the following manner:
        1 bit   mirrored property
        4 bits  directionality property
        9 bits  signed offset used for converting case
        1 bit   if 1, adding the signed offset converts the character to lowercase
        1 bit   if 1, subtracting the signed offset converts the character to uppercase
        1 bit   if 1, this character has a titlecase equivalent (possibly itself)
        3 bits  0  may not be part of an identifier
                1  ignorable control; may continue a Unicode identifier or Java identifier
                2  may continue a Java identifier but not a Unicode identifier (unused)
                3  may continue a Unicode identifier or Java identifier
                4  is a Java whitespace character
                5  may start or continue a Java identifier;
                   may continue but not start a Unicode identifier (underscores)
                6  may start or continue a Java identifier but not a Unicode identifier ($)
                7  may start or continue a Unicode identifier or Java identifier
                Thus:
                   5, 6, 7 may start a Java identifier
                   1, 2, 3, 5, 6, 7 may continue a Java identifier
                   7 may start a Unicode identifier
                   1, 3, 5, 7 may continue a Unicode identifier
                   1 is ignorable within an identifier
                   4 is Java whitespace
        2 bits  0  this character has no numeric property
                1  adding the digit offset to the character code and then
                   masking with 0x1F will produce the desired numeric value
                2  this character has a "strange" numeric value
                3  a Java supradecimal digit: adding the digit offset to the
                   character code, then masking with 0x1F, then adding 10
                   will produce the desired numeric value
        5 bits  digit offset
        5 bits  character type

        The encoding of character properties is subject to change at any time.
     */

    int getProperties(int ch) {
        char offset = (char)ch;
        int props = A[Y[X[offset>>5]|((offset>>1)&0xF)]|(offset&0x1)];
        return props;
    }

    int getPropertiesEx(int ch) {
        char offset = (char)ch;
        int props = B[Y[X[offset>>5]|((offset>>1)&0xF)]|(offset&0x1)];
        return props;
    }

    int getType(int ch) {
        int props = getProperties(ch);
        return (props & 0x1F);
    }

    boolean isOtherLowercase(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0001) != 0;
    }

    boolean isOtherUppercase(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0002) != 0;
    }

    boolean isOtherAlphabetic(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0004) != 0;
    }

    boolean isIdeographic(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0010) != 0;
    }

    boolean isJavaIdentifierStart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) >= 0x00005000);
    }

    boolean isJavaIdentifierPart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00003000) != 0);
    }

    boolean isUnicodeIdentifierStart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00007000);
    }

    boolean isUnicodeIdentifierPart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00001000) != 0);
    }

    boolean isIdentifierIgnorable(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00001000);
    }

    int toLowerCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00020000) != 0) {
          if ((val & 0x07FC0000) == 0x07FC0000) {
            switch(ch) {
            case 0x0130: mapChar = 0x0069; break;
            case 0x023A: mapChar = 0x2C65; break;
            case 0x023E: mapChar = 0x2C66; break;
            case 0x0412: mapChar = 0x1C80; break;
            case 0x0414: mapChar = 0x1C81; break;
            case 0x041E: mapChar = 0x1C82; break;
            case 0x0421: mapChar = 0x1C83; break;
            case 0x042A: mapChar = 0x1C86; break;
            case 0x0462: mapChar = 0x1C87; break;
            case 0x10A0: mapChar = 0x2D00; break;
            case 0x10A1: mapChar = 0x2D01; break;
            case 0x10A2: mapChar = 0x2D02; break;
            case 0x10A3: mapChar = 0x2D03; break;
            case 0x10A4: mapChar = 0x2D04; break;
            case 0x10A5: mapChar = 0x2D05; break;
            case 0x10A6: mapChar = 0x2D06; break;
            case 0x10A7: mapChar = 0x2D07; break;
            case 0x10A8: mapChar = 0x2D08; break;
            case 0x10A9: mapChar = 0x2D09; break;
            case 0x10AA: mapChar = 0x2D0A; break;
            case 0x10AB: mapChar = 0x2D0B; break;
            case 0x10AC: mapChar = 0x2D0C; break;
            case 0x10AD: mapChar = 0x2D0D; break;
            case 0x10AE: mapChar = 0x2D0E; break;
            case 0x10AF: mapChar = 0x2D0F; break;
            case 0x10B0: mapChar = 0x2D10; break;
            case 0x10B1: mapChar = 0x2D11; break;
            case 0x10B2: mapChar = 0x2D12; break;
            case 0x10B3: mapChar = 0x2D13; break;
            case 0x10B4: mapChar = 0x2D14; break;
            case 0x10B5: mapChar = 0x2D15; break;
            case 0x10B6: mapChar = 0x2D16; break;
            case 0x10B7: mapChar = 0x2D17; break;
            case 0x10B8: mapChar = 0x2D18; break;
            case 0x10B9: mapChar = 0x2D19; break;
            case 0x10BA: mapChar = 0x2D1A; break;
            case 0x10BB: mapChar = 0x2D1B; break;
            case 0x10BC: mapChar = 0x2D1C; break;
            case 0x10BD: mapChar = 0x2D1D; break;
            case 0x10BE: mapChar = 0x2D1E; break;
            case 0x10BF: mapChar = 0x2D1F; break;
            case 0x10C0: mapChar = 0x2D20; break;
            case 0x10C1: mapChar = 0x2D21; break;
            case 0x10C2: mapChar = 0x2D22; break;
            case 0x10C3: mapChar = 0x2D23; break;
            case 0x10C4: mapChar = 0x2D24; break;
            case 0x10C5: mapChar = 0x2D25; break;
            case 0x10C7: mapChar = 0x2D27; break;
            case 0x10CD: mapChar = 0x2D2D; break;
            case 0x13A0: mapChar = 0xAB70; break;
            case 0x13A1: mapChar = 0xAB71; break;
            case 0x13A2: mapChar = 0xAB72; break;
            case 0x13A3: mapChar = 0xAB73; break;
            case 0x13A4: mapChar = 0xAB74; break;
            case 0x13A5: mapChar = 0xAB75; break;
            case 0x13A6: mapChar = 0xAB76; break;
            case 0x13A7: mapChar = 0xAB77; break;
            case 0x13A8: mapChar = 0xAB78; break;
            case 0x13A9: mapChar = 0xAB79; break;
            case 0x13AA: mapChar = 0xAB7A; break;
            case 0x13AB: mapChar = 0xAB7B; break;
            case 0x13AC: mapChar = 0xAB7C; break;
            case 0x13AD: mapChar = 0xAB7D; break;
            case 0x13AE: mapChar = 0xAB7E; break;
            case 0x13AF: mapChar = 0xAB7F; break;
            case 0x13B0: mapChar = 0xAB80; break;
            case 0x13B1: mapChar = 0xAB81; break;
            case 0x13B2: mapChar = 0xAB82; break;
            case 0x13B3: mapChar = 0xAB83; break;
            case 0x13B4: mapChar = 0xAB84; break;
            case 0x13B5: mapChar = 0xAB85; break;
            case 0x13B6: mapChar = 0xAB86; break;
            case 0x13B7: mapChar = 0xAB87; break;
            case 0x13B8: mapChar = 0xAB88; break;
            case 0x13B9: mapChar = 0xAB89; break;
            case 0x13BA: mapChar = 0xAB8A; break;
            case 0x13BB: mapChar = 0xAB8B; break;
            case 0x13BC: mapChar = 0xAB8C; break;
            case 0x13BD: mapChar = 0xAB8D; break;
            case 0x13BE: mapChar = 0xAB8E; break;
            case 0x13BF: mapChar = 0xAB8F; break;
            case 0x13C0: mapChar = 0xAB90; break;
            case 0x13C1: mapChar = 0xAB91; break;
            case 0x13C2: mapChar = 0xAB92; break;
            case 0x13C3: mapChar = 0xAB93; break;
            case 0x13C4: mapChar = 0xAB94; break;
            case 0x13C5: mapChar = 0xAB95; break;
            case 0x13C6: mapChar = 0xAB96; break;
            case 0x13C7: mapChar = 0xAB97; break;
            case 0x13C8: mapChar = 0xAB98; break;
            case 0x13C9: mapChar = 0xAB99; break;
            case 0x13CA: mapChar = 0xAB9A; break;
            case 0x13CB: mapChar = 0xAB9B; break;
            case 0x13CC: mapChar = 0xAB9C; break;
            case 0x13CD: mapChar = 0xAB9D; break;
            case 0x13CE: mapChar = 0xAB9E; break;
            case 0x13CF: mapChar = 0xAB9F; break;
            case 0x13D0: mapChar = 0xABA0; break;
            case 0x13D1: mapChar = 0xABA1; break;
            case 0x13D2: mapChar = 0xABA2; break;
            case 0x13D3: mapChar = 0xABA3; break;
            case 0x13D4: mapChar = 0xABA4; break;
            case 0x13D5: mapChar = 0xABA5; break;
            case 0x13D6: mapChar = 0xABA6; break;
            case 0x13D7: mapChar = 0xABA7; break;
            case 0x13D8: mapChar = 0xABA8; break;
            case 0x13D9: mapChar = 0xABA9; break;
            case 0x13DA: mapChar = 0xABAA; break;
            case 0x13DB: mapChar = 0xABAB; break;
            case 0x13DC: mapChar = 0xABAC; break;
            case 0x13DD: mapChar = 0xABAD; break;
            case 0x13DE: mapChar = 0xABAE; break;
            case 0x13DF: mapChar = 0xABAF; break;
            case 0x13E0: mapChar = 0xABB0; break;
            case 0x13E1: mapChar = 0xABB1; break;
            case 0x13E2: mapChar = 0xABB2; break;
            case 0x13E3: mapChar = 0xABB3; break;
            case 0x13E4: mapChar = 0xABB4; break;
            case 0x13E5: mapChar = 0xABB5; break;
            case 0x13E6: mapChar = 0xABB6; break;
            case 0x13E7: mapChar = 0xABB7; break;
            case 0x13E8: mapChar = 0xABB8; break;
            case 0x13E9: mapChar = 0xABB9; break;
            case 0x13EA: mapChar = 0xABBA; break;
            case 0x13EB: mapChar = 0xABBB; break;
            case 0x13EC: mapChar = 0xABBC; break;
            case 0x13ED: mapChar = 0xABBD; break;
            case 0x13EE: mapChar = 0xABBE; break;
            case 0x13EF: mapChar = 0xABBF; break;
            case 0x1E9E: mapChar = 0x00DF; break;
            case 0x1F88: mapChar = 0x1F80; break;
            case 0x1F89: mapChar = 0x1F81; break;
            case 0x1F8A: mapChar = 0x1F82; break;
            case 0x1F8B: mapChar = 0x1F83; break;
            case 0x1F8C: mapChar = 0x1F84; break;
            case 0x1F8D: mapChar = 0x1F85; break;
            case 0x1F8E: mapChar = 0x1F86; break;
            case 0x1F8F: mapChar = 0x1F87; break;
            case 0x1F98: mapChar = 0x1F90; break;
            case 0x1F99: mapChar = 0x1F91; break;
            case 0x1F9A: mapChar = 0x1F92; break;
            case 0x1F9B: mapChar = 0x1F93; break;
            case 0x1F9C: mapChar = 0x1F94; break;
            case 0x1F9D: mapChar = 0x1F95; break;
            case 0x1F9E: mapChar = 0x1F96; break;
            case 0x1F9F: mapChar = 0x1F97; break;
            case 0x1FA8: mapChar = 0x1FA0; break;
            case 0x1FA9: mapChar = 0x1FA1; break;
            case 0x1FAA: mapChar = 0x1FA2; break;
            case 0x1FAB: mapChar = 0x1FA3; break;
            case 0x1FAC: mapChar = 0x1FA4; break;
            case 0x1FAD: mapChar = 0x1FA5; break;
            case 0x1FAE: mapChar = 0x1FA6; break;
            case 0x1FAF: mapChar = 0x1FA7; break;
            case 0x1FBC: mapChar = 0x1FB3; break;
            case 0x1FCC: mapChar = 0x1FC3; break;
            case 0x1FFC: mapChar = 0x1FF3; break;
            case 0x2126: mapChar = 0x03C9; break;
            case 0x212A: mapChar = 0x006B; break;
            case 0x212B: mapChar = 0x00E5; break;
            case 0x2C62: mapChar = 0x026B; break;
            case 0x2C63: mapChar = 0x1D7D; break;
            case 0x2C64: mapChar = 0x027D; break;
            case 0x2C6D: mapChar = 0x0251; break;
            case 0x2C6E: mapChar = 0x0271; break;
            case 0x2C6F: mapChar = 0x0250; break;
            case 0x2C70: mapChar = 0x0252; break;
            case 0x2C7E: mapChar = 0x023F; break;
            case 0x2C7F: mapChar = 0x0240; break;
            case 0xA64A: mapChar = 0x1C88; break;
            case 0xA77D: mapChar = 0x1D79; break;
            case 0xA78D: mapChar = 0x0265; break;
            case 0xA7AA: mapChar = 0x0266; break;
            case 0xA7AB: mapChar = 0x025C; break;
            case 0xA7AC: mapChar = 0x0261; break;
            case 0xA7AD: mapChar = 0x026C; break;
            case 0xA7AE: mapChar = 0x026A; break;
            case 0xA7B0: mapChar = 0x029E; break;
            case 0xA7B1: mapChar = 0x0287; break;
            case 0xA7B2: mapChar = 0x029D; break;
            case 0xA7B3: mapChar = 0xAB53; break;
              // default mapChar is already set, so no
              // need to redo it here.
              // default       : mapChar = ch;
            }
          }
          else {
            int offset = val << 5 >> (5+18);
            mapChar = ch + offset;
          }
        }
        return mapChar;
    }

    int toUpperCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00010000) != 0) {
          if ((val & 0x07FC0000) == 0x07FC0000) {
            switch(ch) {
            case 0x017F: mapChar = 0x0053; break;
            case 0x023F: mapChar = 0x2C7E; break;
            case 0x0240: mapChar = 0x2C7F; break;
            case 0x0250: mapChar = 0x2C6F; break;
            case 0x0251: mapChar = 0x2C6D; break;
            case 0x0252: mapChar = 0x2C70; break;
            case 0x025C: mapChar = 0xA7AB; break;
            case 0x0261: mapChar = 0xA7AC; break;
            case 0x0265: mapChar = 0xA78D; break;
            case 0x0266: mapChar = 0xA7AA; break;
            case 0x026A: mapChar = 0xA7AE; break;
            case 0x026B: mapChar = 0x2C62; break;
            case 0x026C: mapChar = 0xA7AD; break;
            case 0x0271: mapChar = 0x2C6E; break;
            case 0x027D: mapChar = 0x2C64; break;
            case 0x0287: mapChar = 0xA7B1; break;
            case 0x029D: mapChar = 0xA7B2; break;
            case 0x029E: mapChar = 0xA7B0; break;
            case 0x1C80: mapChar = 0x0412; break;
            case 0x1C81: mapChar = 0x0414; break;
            case 0x1C82: mapChar = 0x041E; break;
            case 0x1C83: mapChar = 0x0421; break;
            case 0x1C84: mapChar = 0x0422; break;
            case 0x1C85: mapChar = 0x0422; break;
            case 0x1C86: mapChar = 0x042A; break;
            case 0x1C87: mapChar = 0x0462; break;
            case 0x1C88: mapChar = 0xA64A; break;
            case 0x1D79: mapChar = 0xA77D; break;
            case 0x1D7D: mapChar = 0x2C63; break;
            case 0x1F80: mapChar = 0x1F88; break;
            case 0x1F81: mapChar = 0x1F89; break;
            case 0x1F82: mapChar = 0x1F8A; break;
            case 0x1F83: mapChar = 0x1F8B; break;
            case 0x1F84: mapChar = 0x1F8C; break;
            case 0x1F85: mapChar = 0x1F8D; break;
            case 0x1F86: mapChar = 0x1F8E; break;
            case 0x1F87: mapChar = 0x1F8F; break;
            case 0x1F90: mapChar = 0x1F98; break;
            case 0x1F91: mapChar = 0x1F99; break;
            case 0x1F92: mapChar = 0x1F9A; break;
            case 0x1F93: mapChar = 0x1F9B; break;
            case 0x1F94: mapChar = 0x1F9C; break;
            case 0x1F95: mapChar = 0x1F9D; break;
            case 0x1F96: mapChar = 0x1F9E; break;
            case 0x1F97: mapChar = 0x1F9F; break;
            case 0x1FA0: mapChar = 0x1FA8; break;
            case 0x1FA1: mapChar = 0x1FA9; break;
            case 0x1FA2: mapChar = 0x1FAA; break;
            case 0x1FA3: mapChar = 0x1FAB; break;
            case 0x1FA4: mapChar = 0x1FAC; break;
            case 0x1FA5: mapChar = 0x1FAD; break;
            case 0x1FA6: mapChar = 0x1FAE; break;
            case 0x1FA7: mapChar = 0x1FAF; break;
            case 0x1FB3: mapChar = 0x1FBC; break;
            case 0x1FBE: mapChar = 0x0399; break;
            case 0x1FC3: mapChar = 0x1FCC; break;
            case 0x1FF3: mapChar = 0x1FFC; break;
            case 0x2C65: mapChar = 0x023A; break;
            case 0x2C66: mapChar = 0x023E; break;
            case 0x2D00: mapChar = 0x10A0; break;
            case 0x2D01: mapChar = 0x10A1; break;
            case 0x2D02: mapChar = 0x10A2; break;
            case 0x2D03: mapChar = 0x10A3; break;
            case 0x2D04: mapChar = 0x10A4; break;
            case 0x2D05: mapChar = 0x10A5; break;
            case 0x2D06: mapChar = 0x10A6; break;
            case 0x2D07: mapChar = 0x10A7; break;
            case 0x2D08: mapChar = 0x10A8; break;
            case 0x2D09: mapChar = 0x10A9; break;
            case 0x2D0A: mapChar = 0x10AA; break;
            case 0x2D0B: mapChar = 0x10AB; break;
            case 0x2D0C: mapChar = 0x10AC; break;
            case 0x2D0D: mapChar = 0x10AD; break;
            case 0x2D0E: mapChar = 0x10AE; break;
            case 0x2D0F: mapChar = 0x10AF; break;
            case 0x2D10: mapChar = 0x10B0; break;
            case 0x2D11: mapChar = 0x10B1; break;
            case 0x2D12: mapChar = 0x10B2; break;
            case 0x2D13: mapChar = 0x10B3; break;
            case 0x2D14: mapChar = 0x10B4; break;
            case 0x2D15: mapChar = 0x10B5; break;
            case 0x2D16: mapChar = 0x10B6; break;
            case 0x2D17: mapChar = 0x10B7; break;
            case 0x2D18: mapChar = 0x10B8; break;
            case 0x2D19: mapChar = 0x10B9; break;
            case 0x2D1A: mapChar = 0x10BA; break;
            case 0x2D1B: mapChar = 0x10BB; break;
            case 0x2D1C: mapChar = 0x10BC; break;
            case 0x2D1D: mapChar = 0x10BD; break;
            case 0x2D1E: mapChar = 0x10BE; break;
            case 0x2D1F: mapChar = 0x10BF; break;
            case 0x2D20: mapChar = 0x10C0; break;
            case 0x2D21: mapChar = 0x10C1; break;
            case 0x2D22: mapChar = 0x10C2; break;
            case 0x2D23: mapChar = 0x10C3; break;
            case 0x2D24: mapChar = 0x10C4; break;
            case 0x2D25: mapChar = 0x10C5; break;
            case 0x2D27: mapChar = 0x10C7; break;
            case 0x2D2D: mapChar = 0x10CD; break;
            case 0xAB53: mapChar = 0xA7B3; break;
            case 0xAB70: mapChar = 0x13A0; break;
            case 0xAB71: mapChar = 0x13A1; break;
            case 0xAB72: mapChar = 0x13A2; break;
            case 0xAB73: mapChar = 0x13A3; break;
            case 0xAB74: mapChar = 0x13A4; break;
            case 0xAB75: mapChar = 0x13A5; break;
            case 0xAB76: mapChar = 0x13A6; break;
            case 0xAB77: mapChar = 0x13A7; break;
            case 0xAB78: mapChar = 0x13A8; break;
            case 0xAB79: mapChar = 0x13A9; break;
            case 0xAB7A: mapChar = 0x13AA; break;
            case 0xAB7B: mapChar = 0x13AB; break;
            case 0xAB7C: mapChar = 0x13AC; break;
            case 0xAB7D: mapChar = 0x13AD; break;
            case 0xAB7E: mapChar = 0x13AE; break;
            case 0xAB7F: mapChar = 0x13AF; break;
            case 0xAB80: mapChar = 0x13B0; break;
            case 0xAB81: mapChar = 0x13B1; break;
            case 0xAB82: mapChar = 0x13B2; break;
            case 0xAB83: mapChar = 0x13B3; break;
            case 0xAB84: mapChar = 0x13B4; break;
            case 0xAB85: mapChar = 0x13B5; break;
            case 0xAB86: mapChar = 0x13B6; break;
            case 0xAB87: mapChar = 0x13B7; break;
            case 0xAB88: mapChar = 0x13B8; break;
            case 0xAB89: mapChar = 0x13B9; break;
            case 0xAB8A: mapChar = 0x13BA; break;
            case 0xAB8B: mapChar = 0x13BB; break;
            case 0xAB8C: mapChar = 0x13BC; break;
            case 0xAB8D: mapChar = 0x13BD; break;
            case 0xAB8E: mapChar = 0x13BE; break;
            case 0xAB8F: mapChar = 0x13BF; break;
            case 0xAB90: mapChar = 0x13C0; break;
            case 0xAB91: mapChar = 0x13C1; break;
            case 0xAB92: mapChar = 0x13C2; break;
            case 0xAB93: mapChar = 0x13C3; break;
            case 0xAB94: mapChar = 0x13C4; break;
            case 0xAB95: mapChar = 0x13C5; break;
            case 0xAB96: mapChar = 0x13C6; break;
            case 0xAB97: mapChar = 0x13C7; break;
            case 0xAB98: mapChar = 0x13C8; break;
            case 0xAB99: mapChar = 0x13C9; break;
            case 0xAB9A: mapChar = 0x13CA; break;
            case 0xAB9B: mapChar = 0x13CB; break;
            case 0xAB9C: mapChar = 0x13CC; break;
            case 0xAB9D: mapChar = 0x13CD; break;
            case 0xAB9E: mapChar = 0x13CE; break;
            case 0xAB9F: mapChar = 0x13CF; break;
            case 0xABA0: mapChar = 0x13D0; break;
            case 0xABA1: mapChar = 0x13D1; break;
            case 0xABA2: mapChar = 0x13D2; break;
            case 0xABA3: mapChar = 0x13D3; break;
            case 0xABA4: mapChar = 0x13D4; break;
            case 0xABA5: mapChar = 0x13D5; break;
            case 0xABA6: mapChar = 0x13D6; break;
            case 0xABA7: mapChar = 0x13D7; break;
            case 0xABA8: mapChar = 0x13D8; break;
            case 0xABA9: mapChar = 0x13D9; break;
            case 0xABAA: mapChar = 0x13DA; break;
            case 0xABAB: mapChar = 0x13DB; break;
            case 0xABAC: mapChar = 0x13DC; break;
            case 0xABAD: mapChar = 0x13DD; break;
            case 0xABAE: mapChar = 0x13DE; break;
            case 0xABAF: mapChar = 0x13DF; break;
            case 0xABB0: mapChar = 0x13E0; break;
            case 0xABB1: mapChar = 0x13E1; break;
            case 0xABB2: mapChar = 0x13E2; break;
            case 0xABB3: mapChar = 0x13E3; break;
            case 0xABB4: mapChar = 0x13E4; break;
            case 0xABB5: mapChar = 0x13E5; break;
            case 0xABB6: mapChar = 0x13E6; break;
            case 0xABB7: mapChar = 0x13E7; break;
            case 0xABB8: mapChar = 0x13E8; break;
            case 0xABB9: mapChar = 0x13E9; break;
            case 0xABBA: mapChar = 0x13EA; break;
            case 0xABBB: mapChar = 0x13EB; break;
            case 0xABBC: mapChar = 0x13EC; break;
            case 0xABBD: mapChar = 0x13ED; break;
            case 0xABBE: mapChar = 0x13EE; break;
            case 0xABBF: mapChar = 0x13EF; break;
              // ch must have a 1:M case mapping, but we
              // can't handle it here. Return ch.
              // since mapChar is already set, no need
              // to redo it here.
              //default       : mapChar = ch;
            }
          }
          else {
            int offset = val  << 5 >> (5+18);
            mapChar =  ch - offset;
          }
        }
        return mapChar;
    }

    int toTitleCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00008000) != 0) {
            // There is a titlecase equivalent.  Perform further checks:
            if ((val & 0x00010000) == 0) {
                // The character does not have an uppercase equivalent, so it must
                // already be uppercase; so add 1 to get the titlecase form.
                mapChar = ch + 1;
            }
            else if ((val & 0x00020000) == 0) {
                // The character does not have a lowercase equivalent, so it must
                // already be lowercase; so subtract 1 to get the titlecase form.
                mapChar = ch - 1;
            }
            // else {
            // The character has both an uppercase equivalent and a lowercase
            // equivalent, so it must itself be a titlecase form; return it.
            // return ch;
            //}
        }
        else if ((val & 0x00010000) != 0) {
            // This character has no titlecase equivalent but it does have an
            // uppercase equivalent, so use that (subtract the signed case offset).
            mapChar = toUpperCase(ch);
        }
        return mapChar;
    }

    int digit(int ch, int radix) {
        int value = -1;
        if (radix >= Character.MIN_RADIX && radix <= Character.MAX_RADIX) {
            int val = getProperties(ch);
            int kind = val & 0x1F;
            if (kind == Character.DECIMAL_DIGIT_NUMBER) {
                value = ch + ((val & 0x3E0) >> 5) & 0x1F;
            }
            else if ((val & 0xC00) == 0x00000C00) {
                // Java supradecimal digit
                value = (ch + ((val & 0x3E0) >> 5) & 0x1F) + 10;
            }
        }
        return (value < radix) ? value : -1;
    }

    int getNumericValue(int ch) {
        int val = getProperties(ch);
        int retval = -1;

        switch (val & 0xC00) {
        default: // cannot occur
        case (0x00000000):         // not numeric
            retval = -1;
            break;
        case (0x00000400):              // simple numeric
            retval = ch + ((val & 0x3E0) >> 5) & 0x1F;
            break;
        case (0x00000800)      :       // "strange" numeric
            switch (ch) {
                case 0x0BF1: retval = 100; break;         // TAMIL NUMBER ONE HUNDRED
                case 0x0BF2: retval = 1000; break;        // TAMIL NUMBER ONE THOUSAND
                case 0x0D71: retval = 100; break;         // MALAYALAM NUMBER ONE HUNDRED
                case 0x0D72: retval = 1000; break;        // MALAYALAM NUMBER ONE THOUSAND
                case 0x1375: retval = 40; break;          // ETHIOPIC NUMBER FORTY
                case 0x1376: retval = 50; break;          // ETHIOPIC NUMBER FIFTY
                case 0x1377: retval = 60; break;          // ETHIOPIC NUMBER SIXTY
                case 0x1378: retval = 70; break;          // ETHIOPIC NUMBER SEVENTY
                case 0x1379: retval = 80; break;          // ETHIOPIC NUMBER EIGHTY
                case 0x137A: retval = 90; break;          // ETHIOPIC NUMBER NINETY
                case 0x137B: retval = 100; break;         // ETHIOPIC NUMBER HUNDRED
                case 0x137C: retval = 10000; break;       // ETHIOPIC NUMBER TEN THOUSAND
                case 0x215F: retval = 1; break;           // FRACTION NUMERATOR ONE
                case 0x216C: retval = 50; break;          // ROMAN NUMERAL FIFTY
                case 0x216D: retval = 100; break;         // ROMAN NUMERAL ONE HUNDRED
                case 0x216E: retval = 500; break;         // ROMAN NUMERAL FIVE HUNDRED
                case 0x216F: retval = 1000; break;        // ROMAN NUMERAL ONE THOUSAND
                case 0x217C: retval = 50; break;          // SMALL ROMAN NUMERAL FIFTY
                case 0x217D: retval = 100; break;         // SMALL ROMAN NUMERAL ONE HUNDRED
                case 0x217E: retval = 500; break;         // SMALL ROMAN NUMERAL FIVE HUNDRED
                case 0x217F: retval = 1000; break;        // SMALL ROMAN NUMERAL ONE THOUSAND
                case 0x2180: retval = 1000; break;        // ROMAN NUMERAL ONE THOUSAND C D
                case 0x2181: retval = 5000; break;        // ROMAN NUMERAL FIVE THOUSAND
                case 0x2182: retval = 10000; break;       // ROMAN NUMERAL TEN THOUSAND
                case 0x2186: retval = 50; break;          // ROMAN NUMERAL FIFTY EARLY FORM
                case 0x2187: retval = 50000; break;       // ROMAN NUMERAL FIFTY THOUSAND
                case 0x2188: retval = 100000; break;      // ROMAN NUMERAL ONE HUNDRED THOUSAND
                case 0x324B: retval = 40; break;          // CIRCLED NUMBER FORTY ON BLACK SQUARE
                case 0x324C: retval = 50; break;          // CIRCLED NUMBER FIFTY ON BLACK SQUARE
                case 0x324D: retval = 60; break;          // CIRCLED NUMBER SIXTY ON BLACK SQUARE
                case 0x324E: retval = 70; break;          // CIRCLED NUMBER SEVENTY ON BLACK SQUARE
                case 0x324F: retval = 80; break;          // CIRCLED NUMBER EIGHTY ON BLACK SQUARE
                case 0x325C: retval = 32; break;          // CIRCLED NUMBER THIRTY TWO
                case 0x325D: retval = 33; break;          // CIRCLED NUMBER THIRTY THREE
                case 0x325E: retval = 34; break;          // CIRCLED NUMBER THIRTY FOUR
                case 0x325F: retval = 35; break;          // CIRCLED NUMBER THIRTY FIVE
                case 0x32B1: retval = 36; break;          // CIRCLED NUMBER THIRTY SIX
                case 0x32B2: retval = 37; break;          // CIRCLED NUMBER THIRTY SEVEN
                case 0x32B3: retval = 38; break;          // CIRCLED NUMBER THIRTY EIGHT
                case 0x32B4: retval = 39; break;          // CIRCLED NUMBER THIRTY NINE
                case 0x32B5: retval = 40; break;          // CIRCLED NUMBER FORTY
                case 0x32B6: retval = 41; break;          // CIRCLED NUMBER FORTY ONE
                case 0x32B7: retval = 42; break;          // CIRCLED NUMBER FORTY TWO
                case 0x32B8: retval = 43; break;          // CIRCLED NUMBER FORTY THREE
                case 0x32B9: retval = 44; break;          // CIRCLED NUMBER FORTY FOUR
                case 0x32BA: retval = 45; break;          // CIRCLED NUMBER FORTY FIVE
                case 0x32BB: retval = 46; break;          // CIRCLED NUMBER FORTY SIX
                case 0x32BC: retval = 47; break;          // CIRCLED NUMBER FORTY SEVEN
                case 0x32BD: retval = 48; break;          // CIRCLED NUMBER FORTY EIGHT
                case 0x32BE: retval = 49; break;          // CIRCLED NUMBER FORTY NINE
                case 0x32BF: retval = 50; break;          // CIRCLED NUMBER FIFTY
                default:       retval = -2; break;
            }
            break;
        case (0x00000C00):           // Java supradecimal
            retval = (ch + ((val & 0x3E0) >> 5) & 0x1F) + 10;
            break;
        }
        return retval;
    }

    boolean isWhitespace(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00004000);
    }

    byte getDirectionality(int ch) {
        int val = getProperties(ch);
        byte directionality = (byte)((val & 0x78000000) >> 27);
        if (directionality == 0xF ) {
            switch(ch) {
                case 0x202A :
                    // This is the only char with LRE
                    directionality = Character.DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING;
                    break;
                case 0x202B :
                    // This is the only char with RLE
                    directionality = Character.DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING;
                    break;
                case 0x202C :
                    // This is the only char with PDF
                    directionality = Character.DIRECTIONALITY_POP_DIRECTIONAL_FORMAT;
                    break;
                case 0x202D :
                    // This is the only char with LRO
                    directionality = Character.DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE;
                    break;
                case 0x202E :
                    // This is the only char with RLO
                    directionality = Character.DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE;
                    break;
                case 0x2066 :
                    // This is the only char with LRI
                    directionality = Character.DIRECTIONALITY_LEFT_TO_RIGHT_ISOLATE;
                    break;
                case 0x2067 :
                    // This is the only char with RLI
                    directionality = Character.DIRECTIONALITY_RIGHT_TO_LEFT_ISOLATE;
                    break;
                case 0x2068 :
                    // This is the only char with FSI
                    directionality = Character.DIRECTIONALITY_FIRST_STRONG_ISOLATE;
                    break;
                case 0x2069 :
                    // This is the only char with PDI
                    directionality = Character.DIRECTIONALITY_POP_DIRECTIONAL_ISOLATE;
                    break;
                default :
                    directionality = Character.DIRECTIONALITY_UNDEFINED;
                    break;
            }
        }
        return directionality;
    }

    boolean isMirrored(int ch) {
        int props = getProperties(ch);
        return ((props & 0x80000000) != 0);
    }

    int toUpperCaseEx(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00010000) != 0) {
            if ((val & 0x07FC0000) != 0x07FC0000) {
                int offset = val  << 5 >> (5+18);
                mapChar =  ch - offset;
            }
            else {
                switch(ch) {
                    case 0x017F: mapChar = 0x0053; break;
                    case 0x023F: mapChar = 0x2C7E; break;
                    case 0x0240: mapChar = 0x2C7F; break;
                    case 0x0250: mapChar = 0x2C6F; break;
                    case 0x0251: mapChar = 0x2C6D; break;
                    case 0x0252: mapChar = 0x2C70; break;
                    case 0x025C: mapChar = 0xA7AB; break;
                    case 0x0261: mapChar = 0xA7AC; break;
                    case 0x0265: mapChar = 0xA78D; break;
                    case 0x0266: mapChar = 0xA7AA; break;
                    case 0x026A: mapChar = 0xA7AE; break;
                    case 0x026B: mapChar = 0x2C62; break;
                    case 0x026C: mapChar = 0xA7AD; break;
                    case 0x0271: mapChar = 0x2C6E; break;
                    case 0x027D: mapChar = 0x2C64; break;
                    case 0x0287: mapChar = 0xA7B1; break;
                    case 0x029D: mapChar = 0xA7B2; break;
                    case 0x029E: mapChar = 0xA7B0; break;
                    case 0x1C80: mapChar = 0x0412; break;
                    case 0x1C81: mapChar = 0x0414; break;
                    case 0x1C82: mapChar = 0x041E; break;
                    case 0x1C83: mapChar = 0x0421; break;
                    case 0x1C84: mapChar = 0x0422; break;
                    case 0x1C85: mapChar = 0x0422; break;
                    case 0x1C86: mapChar = 0x042A; break;
                    case 0x1C87: mapChar = 0x0462; break;
                    case 0x1C88: mapChar = 0xA64A; break;
                    case 0x1D79: mapChar = 0xA77D; break;
                    case 0x1D7D: mapChar = 0x2C63; break;
                    case 0x1FBE: mapChar = 0x0399; break;
                    case 0x2C65: mapChar = 0x023A; break;
                    case 0x2C66: mapChar = 0x023E; break;
                    case 0x2D00: mapChar = 0x10A0; break;
                    case 0x2D01: mapChar = 0x10A1; break;
                    case 0x2D02: mapChar = 0x10A2; break;
                    case 0x2D03: mapChar = 0x10A3; break;
                    case 0x2D04: mapChar = 0x10A4; break;
                    case 0x2D05: mapChar = 0x10A5; break;
                    case 0x2D06: mapChar = 0x10A6; break;
                    case 0x2D07: mapChar = 0x10A7; break;
                    case 0x2D08: mapChar = 0x10A8; break;
                    case 0x2D09: mapChar = 0x10A9; break;
                    case 0x2D0A: mapChar = 0x10AA; break;
                    case 0x2D0B: mapChar = 0x10AB; break;
                    case 0x2D0C: mapChar = 0x10AC; break;
                    case 0x2D0D: mapChar = 0x10AD; break;
                    case 0x2D0E: mapChar = 0x10AE; break;
                    case 0x2D0F: mapChar = 0x10AF; break;
                    case 0x2D10: mapChar = 0x10B0; break;
                    case 0x2D11: mapChar = 0x10B1; break;
                    case 0x2D12: mapChar = 0x10B2; break;
                    case 0x2D13: mapChar = 0x10B3; break;
                    case 0x2D14: mapChar = 0x10B4; break;
                    case 0x2D15: mapChar = 0x10B5; break;
                    case 0x2D16: mapChar = 0x10B6; break;
                    case 0x2D17: mapChar = 0x10B7; break;
                    case 0x2D18: mapChar = 0x10B8; break;
                    case 0x2D19: mapChar = 0x10B9; break;
                    case 0x2D1A: mapChar = 0x10BA; break;
                    case 0x2D1B: mapChar = 0x10BB; break;
                    case 0x2D1C: mapChar = 0x10BC; break;
                    case 0x2D1D: mapChar = 0x10BD; break;
                    case 0x2D1E: mapChar = 0x10BE; break;
                    case 0x2D1F: mapChar = 0x10BF; break;
                    case 0x2D20: mapChar = 0x10C0; break;
                    case 0x2D21: mapChar = 0x10C1; break;
                    case 0x2D22: mapChar = 0x10C2; break;
                    case 0x2D23: mapChar = 0x10C3; break;
                    case 0x2D24: mapChar = 0x10C4; break;
                    case 0x2D25: mapChar = 0x10C5; break;
                    case 0x2D27: mapChar = 0x10C7; break;
                    case 0x2D2D: mapChar = 0x10CD; break;
                    case 0xAB53: mapChar = 0xA7B3; break;
                    case 0xAB70: mapChar = 0x13A0; break;
                    case 0xAB71: mapChar = 0x13A1; break;
                    case 0xAB72: mapChar = 0x13A2; break;
                    case 0xAB73: mapChar = 0x13A3; break;
                    case 0xAB74: mapChar = 0x13A4; break;
                    case 0xAB75: mapChar = 0x13A5; break;
                    case 0xAB76: mapChar = 0x13A6; break;
                    case 0xAB77: mapChar = 0x13A7; break;
                    case 0xAB78: mapChar = 0x13A8; break;
                    case 0xAB79: mapChar = 0x13A9; break;
                    case 0xAB7A: mapChar = 0x13AA; break;
                    case 0xAB7B: mapChar = 0x13AB; break;
                    case 0xAB7C: mapChar = 0x13AC; break;
                    case 0xAB7D: mapChar = 0x13AD; break;
                    case 0xAB7E: mapChar = 0x13AE; break;
                    case 0xAB7F: mapChar = 0x13AF; break;
                    case 0xAB80: mapChar = 0x13B0; break;
                    case 0xAB81: mapChar = 0x13B1; break;
                    case 0xAB82: mapChar = 0x13B2; break;
                    case 0xAB83: mapChar = 0x13B3; break;
                    case 0xAB84: mapChar = 0x13B4; break;
                    case 0xAB85: mapChar = 0x13B5; break;
                    case 0xAB86: mapChar = 0x13B6; break;
                    case 0xAB87: mapChar = 0x13B7; break;
                    case 0xAB88: mapChar = 0x13B8; break;
                    case 0xAB89: mapChar = 0x13B9; break;
                    case 0xAB8A: mapChar = 0x13BA; break;
                    case 0xAB8B: mapChar = 0x13BB; break;
                    case 0xAB8C: mapChar = 0x13BC; break;
                    case 0xAB8D: mapChar = 0x13BD; break;
                    case 0xAB8E: mapChar = 0x13BE; break;
                    case 0xAB8F: mapChar = 0x13BF; break;
                    case 0xAB90: mapChar = 0x13C0; break;
                    case 0xAB91: mapChar = 0x13C1; break;
                    case 0xAB92: mapChar = 0x13C2; break;
                    case 0xAB93: mapChar = 0x13C3; break;
                    case 0xAB94: mapChar = 0x13C4; break;
                    case 0xAB95: mapChar = 0x13C5; break;
                    case 0xAB96: mapChar = 0x13C6; break;
                    case 0xAB97: mapChar = 0x13C7; break;
                    case 0xAB98: mapChar = 0x13C8; break;
                    case 0xAB99: mapChar = 0x13C9; break;
                    case 0xAB9A: mapChar = 0x13CA; break;
                    case 0xAB9B: mapChar = 0x13CB; break;
                    case 0xAB9C: mapChar = 0x13CC; break;
                    case 0xAB9D: mapChar = 0x13CD; break;
                    case 0xAB9E: mapChar = 0x13CE; break;
                    case 0xAB9F: mapChar = 0x13CF; break;
                    case 0xABA0: mapChar = 0x13D0; break;
                    case 0xABA1: mapChar = 0x13D1; break;
                    case 0xABA2: mapChar = 0x13D2; break;
                    case 0xABA3: mapChar = 0x13D3; break;
                    case 0xABA4: mapChar = 0x13D4; break;
                    case 0xABA5: mapChar = 0x13D5; break;
                    case 0xABA6: mapChar = 0x13D6; break;
                    case 0xABA7: mapChar = 0x13D7; break;
                    case 0xABA8: mapChar = 0x13D8; break;
                    case 0xABA9: mapChar = 0x13D9; break;
                    case 0xABAA: mapChar = 0x13DA; break;
                    case 0xABAB: mapChar = 0x13DB; break;
                    case 0xABAC: mapChar = 0x13DC; break;
                    case 0xABAD: mapChar = 0x13DD; break;
                    case 0xABAE: mapChar = 0x13DE; break;
                    case 0xABAF: mapChar = 0x13DF; break;
                    case 0xABB0: mapChar = 0x13E0; break;
                    case 0xABB1: mapChar = 0x13E1; break;
                    case 0xABB2: mapChar = 0x13E2; break;
                    case 0xABB3: mapChar = 0x13E3; break;
                    case 0xABB4: mapChar = 0x13E4; break;
                    case 0xABB5: mapChar = 0x13E5; break;
                    case 0xABB6: mapChar = 0x13E6; break;
                    case 0xABB7: mapChar = 0x13E7; break;
                    case 0xABB8: mapChar = 0x13E8; break;
                    case 0xABB9: mapChar = 0x13E9; break;
                    case 0xABBA: mapChar = 0x13EA; break;
                    case 0xABBB: mapChar = 0x13EB; break;
                    case 0xABBC: mapChar = 0x13EC; break;
                    case 0xABBD: mapChar = 0x13ED; break;
                    case 0xABBE: mapChar = 0x13EE; break;
                    case 0xABBF: mapChar = 0x13EF; break;
                    default       : mapChar = Character.ERROR; break;
                }
            }
        }
        return mapChar;
    }

    char[] toUpperCaseCharArray(int ch) {
        char[] upperMap = {(char)ch};
        int location = findInCharMap(ch);
        if (location != -1) {
            upperMap = charMap[location][1];
        }
        return upperMap;
    }


    /**
     * Finds the character in the uppercase mapping table.
     *
     * @param ch the <code>char</code> to search
     * @return the index location ch in the table or -1 if not found
     * @since 1.4
     */
     int findInCharMap(int ch) {
        if (charMap == null || charMap.length == 0) {
            return -1;
        }
        int top, bottom, current;
        bottom = 0;
        top = charMap.length;
        current = top/2;
        // invariant: top > current >= bottom && ch >= CharacterData.charMap[bottom][0]
        while (top - bottom > 1) {
            if (ch >= charMap[current][0][0]) {
                bottom = current;
            } else {
                top = current;
            }
            current = (top + bottom) / 2;
        }
        if (ch == charMap[current][0][0]) return current;
        else return -1;
    }

    static final CharacterData00 instance = new CharacterData00();
    private CharacterData00() {};

    // The following tables and code generated using:
  // java GenerateCharacter -string -plane 0 -template t:/workspace/open/make/data/characterdata/CharacterData00.java.template -spec t:/workspace/open/make/data/unicodedata/UnicodeData.txt -specialcasing t:/workspace/open/make/data/unicodedata/SpecialCasing.txt -proplist t:/workspace/open/make/data/unicodedata/PropList.txt -o t:/workspace/build/windows-x64/support/gensrc/java.base/java/lang/CharacterData00.java -usecharforbyte 11 4 1
      static final char[][][] charMap;
// The X table has 2048 entries for a total of 4096 bytes.

  static final char X[] = (
    "\000\020\040\060\100\120\140\160\200\220\240\260\300\320\340\360\200\u0100"+
    "\u0110\u0120\u0130\u0140\u0150\u0160\u0170\u0170\u0180\u0190\u01A0\u01B0\u01C0"+
    "\u01D0\u01E0\u01F0\u0200\200\u0210\200\u0220\200\200\u0230\u0240\u0250\u0260"+
    "\u0270\u0280\u0290\u02A0\u02B0\u02C0\u02D0\u02B0\u02B0\u02E0\u02F0\u0300\u0310"+
    "\u0320\u02B0\u02B0\u0330\u0340\u0350\u0360\u0370\u0380\u0390\u03A0\u03B0\u03C0"+
    "\u03D0\u03E0\u03F0\u0400\u0410\u0420\u0430\u0440\u0450\u0460\u0470\u0480\u0490"+
    "\u04A0\u04B0\u04C0\u04D0\u04E0\u04F0\u0500\u0510\u0520\u0530\u0540\u0550\u0560"+
    "\u0570\u0580\u0590\u05A0\u05B0\u05C0\u05D0\u05E0\u05F0\u0600\u0610\u0620\u0630"+
    "\u0640\u0650\u0660\u0670\u0680\u03A0\u0690\u06A0\u06B0\u03A0\u06C0\u06D0\u06E0"+
    "\u06F0\u0700\u0710\u0720\u03A0\u0730\u0740\u0750\u0760\u0770\u0780\u0790\u07A0"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u07B0\u0730\u07C0"+
    "\u07D0\u07E0\u0730\u07F0\u0730\u0800\u0810\u0820\u0780\u0780\u0830\u0840\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0850\u0860\u0730\u0730\u0870\u0880\u0890\u08A0\u08B0"+
    "\u0730\u08C0\u08D0\u08E0\u08F0\u0730\u0900\u0910\u0920\u0930\u0730\u0940\u0950"+
    "\u0960\u0970\u0980\u0730\u0990\u09A0\u09B0\u09C0\u0730\u09D0\u09E0\u09F0\u0A00"+
    "\u03A0\u03A0\u0A10\u0A20\u0A30\u0A40\u0A50\u0A60\u0730\u0A70\u0730\u0A80\u0A90"+
    "\u0AA0\u0AB0\u03A0\u0AC0\u0AD0\u0AE0\u0AF0\u0B00\u0B10\u0B20\u0B00\u0170\u0B30"+
    "\200\200\200\200\u0B40\200\200\200\u0B50\u0B60\u0B70\u0B80\u0B90\u0BA0\u0BB0"+
    "\u0BC0\u0BD0\u0BE0\u0BF0\u0C00\u0C10\u0C20\u0C30\u0C40\u0C50\u0C60\u0C70\u0C80"+
    "\u0C90\u0CA0\u0CB0\u0CC0\u0CD0\u0CE0\u0CF0\u0D00\u0D10\u0D20\u0D30\u0D40\u0D50"+
    "\u0D60\u0D70\u0D80\u0D90\u0DA0\u0DB0\u0DC0\u09B0\u0DD0\u0DE0\u0DF0\u0E00\u0E10"+
    "\u0E20\u0E30\u09B0\u09B0\u09B0\u09B0\u09B0\u0E40\u0E50\u0E60\u09B0\u09B0\u09B0"+
    "\u0E70\u09B0\u0E80\u09B0\u09B0\u09B0\u09B0\u09B0\u0E90\u0EA0\u09B0\u0EB0\u0EC0"+
    "\u0D70\u0D70\u0D70\u0D70\u0D70\u0D70\u0D70\u0D70\u0ED0\u0ED0\u0ED0\u0ED0\u0EE0"+
    "\u0EF0\u0F00\u0F10\u0F20\u0F30\u0F40\u0F50\u0F60\u0F70\u0F80\u0F90\u09B0\u0FA0"+
    "\u0FB0\u0FC0\u0FD0\u0FE0\u0FF0\u1000\u1010\u1020\u1030\u1040\200\200\200\u1050"+
    "\u1060\u1070\u0730\u1080\u1090\u10A0\u10A0\u10B0\u10C0\u10D0\u10E0\u03A0\u10F0"+
    "\u09B0\u09B0\u1100\u09B0\u09B0\u09B0\u09B0\u09B0\u09B0\u1110\u1120\u1130\u1140"+
    "\u0660\u0730\u1150\u0840\u0730\u1160\u1170\u1180\u0730\u0730\u1190\u11A0\u09B0"+
    "\u11B0\u11C0\u11D0\u11E0\u11F0\u11D0\u1200\u1210\u1220\u0D70\u0D70\u0D70\u1230"+
    "\u0D70\u0D70\u1240\u1250\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1270\u09B0\u09B0\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260"+
    "\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1260\u1280\u1290\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u12A0\u09B0\u12B0\u0AA0\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u12C0\u12D0\200\u12E0\u12F0\u0730\u0730"+
    "\u1300\u1310\u1320\200\u1330\u1340\u1350\u03A0\u1360\u1370\u1380\u0730\u1390"+
    "\u13A0\u13B0\u13C0\u13D0\u13E0\u13F0\u1400\u1410\u03E0\u1420\u1430\u1440\u0730"+
    "\u1450\u1460\u1470\u0730\u1480\u1490\u14A0\u14B0\u14C0\u14D0\u14E0\u1060\u1060"+
    "\u0730\u14F0\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730\u0730"+
    "\u1500\u1510\u1520\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530"+
    "\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530"+
    "\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530"+
    "\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530"+
    "\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530\u1530"+
    "\u1530\u1530\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1540"+
    "\u1540\u1540\u1540\u1540\u1540\u1540\u1540\u1260\u1260\u1260\u1550\u1260\u1560"+
    "\u1570\u1580\u1260\u1260\u1260\u1590\u1260\u1260\u15A0\u03A0\u15B0\u15C0\u15D0"+
    "\u02B0\u02B0\u15E0\u15F0\u02B0\u02B0\u02B0\u02B0\u02B0\u02B0\u02B0\u02B0\u02B0"+
    "\u02B0\u1600\u1610\u02B0\u1620\u02B0\u1630\u1640\u1650\u1660\u1670\u1680\u02B0"+
    "\u02B0\u02B0\u1690\u16A0\040\u16B0\u16C0\u16D0\u0950\u16E0\u16F0").toCharArray();

  // The Y table has 5888 entries for a total of 11776 bytes.

  static final char Y[] = (
    "\000\000\000\000\002\004\006\000\000\000\000\000\000\000\010\004\012\014\016"+
    "\020\022\024\026\030\032\032\032\032\032\034\036\040\042\044\044\044\044\044"+
    "\044\044\044\044\044\044\044\046\050\052\054\056\056\056\056\056\056\056\056"+
    "\056\056\056\056\060\062\064\000\000\066\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\070\072\072\074\076\100\102\104\106\110\112\020\114\116\120"+
    "\122\124\124\124\124\124\124\124\124\124\124\124\126\124\124\124\130\132\132"+
    "\132\132\132\132\132\132\132\132\132\134\132\132\132\136\140\140\140\140\140"+
    "\140\140\140\140\140\140\140\140\140\140\140\140\140\140\140\140\140\140\140"+
    "\142\140\140\140\144\146\146\146\146\146\146\146\150\140\140\140\140\140\140"+
    "\140\140\140\140\140\140\140\140\140\140\140\140\140\140\140\140\140\152\146"+
    "\146\150\154\140\140\156\160\162\164\166\170\160\172\174\140\176\200\202\140"+
    "\140\140\204\206\210\140\204\212\214\146\216\140\220\140\222\224\224\226\230"+
    "\232\226\234\146\146\146\146\146\146\146\236\140\140\140\140\140\140\140\140"+
    "\140\240\232\140\242\140\140\140\140\244\140\140\140\140\140\140\140\140\140"+
    "\210\210\210\246\250\252\254\256\260\140\140\140\140\140\262\264\266\270\272"+
    "\274\276\210\300\302\304\276\306\262\276\310\304\312\314\210\210\210\304\210"+
    "\316\320\210\304\322\324\326\210\210\330\332\210\210\210\304\276\210\210\210"+
    "\210\210\210\210\210\334\334\334\334\336\340\342\342\334\344\344\346\346\346"+
    "\346\346\342\344\344\344\344\344\344\344\334\334\350\344\344\344\352\354\344"+
    "\344\344\344\344\344\344\344\356\356\356\356\356\356\356\356\356\356\356\356"+
    "\356\356\356\356\356\356\360\356\356\356\356\356\356\356\356\356\356\356\356"+
    "\356\356\356\356\356\356\356\356\356\140\140\352\140\362\364\366\370\362\362"+
    "\344\372\374\376\u0100\u0102\u0104\124\124\124\124\124\124\124\124\u0106\124"+
    "\124\124\124\u0108\u010A\u010C\132\132\132\132\132\132\132\132\u010E\132\132"+
    "\132\132\u0110\u0112\u0114\u0116\u0118\u011A\140\140\140\140\140\140\140\140"+
    "\140\140\140\140\u011C\u011E\u0120\u0122\u0124\140\u0126\u0128\u012A\u012A"+
    "\u012A\u012A\u012A\u012A\u012A\u012A\124\124\124\124\124\124\124\124\124\124"+
    "\124\124\124\124\124\124\132\132\132\132\132\132\132\132\132\132\132\132\132"+
    "\132\132\132\u012C\u012C\u012C\u012C\u012C\u012C\u012C\u012C\140\u012E\356"+
    "\356\u0130\140\140\140\140\140\140\140\140\140\140\140\u0132\146\146\146\146"+
    "\146\146\u0134\140\140\140\140\140\140\140\140\140\140\140\140\140\140\140"+
    "\140\u0136\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138"+
    "\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u013A\u013C\u013E\u013E\u013E\u0140"+
    "\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142"+
    "\u0142\u0142\u0142\u0142\u0142\u0144\u0146\u0148\u014A\u014C\u014E\356\356"+
    "\356\356\356\356\356\356\356\356\356\356\356\356\356\u0150\u0150\u0150\u0150"+
    "\u0150\u0150\u0150\u0152\u0154\u0156\u0150\u0154\362\362\362\362\u0158\u0158"+
    "\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u015A\362"+
    "\362\u0158\u015C\u015E\362\362\362\362\362\u0160\u0160\u0160\u0162\u0164\u0166"+
    "\u0168\u016A\u0150\u0150\u0150\u0150\u0150\u016C\u016E\u0170\u0172\u0172\u0172"+
    "\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172"+
    "\u0174\u0172\u0172\u0172\u0172\u0176\u0150\u0150\u0150\u0150\u0150\u0150\u0178"+
    "\u0150\u0150\u0150\u017A\u017A\u017A\u017A\u017A\u017C\u017E\u0172\u0180\u0172"+
    "\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172"+
    "\u0172\u0172\u0172\u0182\u0150\u0150\u0150\u0184\u0186\u0178\u0150\u0188\u018A"+
    "\u018C\356\u0178\u0172\032\032\032\032\032\u0172\u018E\u0190\u0170\u0170\u0170"+
    "\u0170\u0170\u0170\u0170\u0192\u0176\u0172\u0172\u0172\u0172\u0172\u0172\u0172"+
    "\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0150\u0150\u0150\u0150\u0150"+
    "\u0150\u0150\u0150\356\356\356\356\356\u0194\u0196\u0172\u0172\u0172\u0172"+
    "\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0150\u0150\u0150\u0150\u0150"+
    "\u0180\362\362\362\362\362\362\362\u0198\u0198\u0198\u0198\u0198\u0158\u0158"+
    "\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158"+
    "\u0158\u019A\356\356\356\356\u019C\074\020\u019E\362\362\u0158\u0158\u0158"+
    "\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0150\356\u01A0\u0150\u0150"+
    "\u0150\u0150\u01A0\u0150\u01A0\u0150\u01A2\362\u01A4\u01A4\u01A4\u01A4\u01A4"+
    "\u01A4\u01A4\u015E\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158\u0158"+
    "\u0158\u0158\u019A\356\362\u015E\u0172\u0172\u0172\u0172\u0172\u01A6\362\362"+
    "\362\362\362\362\362\362\362\362\362\362\362\362\362\362\362\362\362\362\362"+
    "\362\362\362\362\362\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172"+
    "\u0172\u01A6\u0172\u0172\u0172\u0172\362\362\362\362\362\362\362\362\362\362"+
    "\362\u0150\u0150\u0150\u0150\u0150\u0150\356\u01A8\u0150\u0150\u0150\356\356"+
    "\356\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u01AA\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\u01AA\u01AC\u01AE\u01B0\u0150\u0150\u0150\u01AA\u01AE"+
    "\u01B2\u01AE\u01B4\356\u0178\u0150\224\224\224\224\224\u0150\u013E\u01B6\u01B6"+
    "\u01B6\u01B6\u01B6\u01B8\224\224\224\224\224\224\224\u01BA\u01AE\u01BC\224"+
    "\224\224\u01BE\u01BC\u01BE\u01BC\224\224\224\224\224\224\224\224\224\224\u01BE"+
    "\224\224\224\u01BE\u01BE\362\224\224\362\u01AC\u01AE\u01B0\u0150\u01C0\u01C2"+
    "\u01C4\u01C2\u01B2\u01BE\362\362\362\u01C2\362\362\224\u01BC\224\u0150\362"+
    "\u01B6\u01B6\u01B6\u01B6\u01B6\224\072\u01C6\u01C6\u01C8\u01CA\u01CC\362\u01CE"+
    "\u01AA\u01BC\224\224\u01BE\362\u01BC\u01BE\u01BC\224\224\224\224\224\224\224"+
    "\224\224\224\u01BE\224\224\224\u01BE\224\u01BC\u01BE\224\362\u0194\u01AE\u01B0"+
    "\u01C0\362\u01CE\u01C0\u01CE\u01A2\362\u01CE\362\362\362\u01BC\224\u01BE\u01BE"+
    "\362\362\362\u01B6\u01B6\u01B6\u01B6\u01B6\u0150\224\u01BA\362\362\362\362"+
    "\362\u01CE\u01AA\u01BC\224\224\224\224\u01BC\224\u01BC\224\224\224\224\224"+
    "\224\224\224\224\224\u01BE\224\224\224\u01BE\224\u01BC\224\224\362\u01AC\u01AE"+
    "\u01B0\u0150\u0150\u01CE\u01AA\u01C2\u01B2\362\u01BE\362\362\362\362\362\362"+
    "\362\224\u0150\362\u01B6\u01B6\u01B6\u01B6\u01B6\u01D0\362\362\362\u01BC\u0150"+
    "\u01A2\356\u01CE\u01AE\u01BC\224\224\224\u01BE\u01BC\u01BE\u01BC\224\224\224"+
    "\224\224\224\224\224\224\224\u01BE\224\224\224\u01BE\224\u01BC\224\224\362"+
    "\u01AC\u01B0\u01B0\u0150\u01C0\u01C2\u01C4\u01C2\u01B2\362\362\362\362\u01AA"+
    "\362\362\224\u01BC\224\u0150\362\u01B6\u01B6\u01B6\u01B6\u01B6\u01D2\u01C6"+
    "\u01C6\u01C6\362\362\362\362\362\u01D4\u01BC\224\224\u01BE\362\224\u01BE\224"+
    "\224\362\u01BC\u01BE\u01BE\224\362\u01BC\u01BE\362\224\u01BE\362\224\224\224"+
    "\224\224\224\362\362\u01AE\u01AA\u01C4\362\u01AE\u01C4\u01AE\u01B2\362\u01BE"+
    "\362\362\u01C2\362\362\362\362\362\362\362\u01B6\u01B6\u01B6\u01B6\u01B6\u01D6"+
    "\u01D8\u016A\u016A\u014C\u01DA\362\362\u01AA\u01AE\u01BC\224\224\224\u01BE"+
    "\224\u01BE\224\224\224\224\224\224\224\224\224\224\224\u01BE\224\224\224\224"+
    "\224\224\224\224\362\u01BC\u0150\u01AA\u01AE\u01C4\u0150\u01C0\u0150\u01A2"+
    "\362\362\362\u01CE\u01C0\224\u01BE\362\362\224\u0150\362\u01B6\u01B6\u01B6"+
    "\u01B6\u01B6\362\362\362\362\u01DC\u01DC\u01DE\u01E0\u01BA\u01AE\u01BC\224"+
    "\224\224\u01BE\224\u01BE\224\224\224\224\224\224\224\224\224\224\224\u01BE"+
    "\224\224\224\224\224\u01BC\224\224\362\u01AC\u01E2\u01AE\u01AE\u01C4\u01E4"+
    "\u01C4\u01AE\u01A2\362\362\362\u01C2\u01C4\362\362\362\u01BE\224\u0150\362"+
    "\u01B6\u01B6\u01B6\u01B6\u01B6\u01BC\u01BE\362\362\362\362\362\362\u0150\u01AE"+
    "\u01BC\224\224\224\u01BE\224\u01BE\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\u01B4\u01AC\u01AE\u01B0\u0150\u01C0"+
    "\u01AE\u01C4\u01AE\u01B2\u01E6\362\362\224\u01E8\u01C6\u01C6\u01C6\u01EA\224"+
    "\u0150\362\u01B6\u01B6\u01B6\u01B6\u01B6\u01D6\u01C6\u01C6\u01C6\u01EC\224"+
    "\224\224\362\u01AE\u01BC\224\224\224\224\224\224\224\224\u01BE\362\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\u01BC\224\224\224\224\u01BC\362\224"+
    "\224\224\u01BE\362\u0194\362\u01C2\u01AE\u0150\u01C0\u01C0\u01AE\u01AE\u01AE"+
    "\u01AE\362\362\362\u01B6\u01B6\u01B6\u01B6\u01B6\362\u01AE\u01EE\362\362\362"+
    "\362\362\u01BC\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\224\224\224\u01BA\224\u0150\u0150\u0150\u01C0\362\u01F0"+
    "\224\224\224\u01F2\356\356\u0178\u01F4\u01F6\u01F6\u01F6\u01F6\u01F6\u013E"+
    "\362\362\u01BC\u01BE\u01BE\u01BC\u01BE\u01BE\u01BC\362\362\362\224\224\u01BC"+
    "\224\224\224\u01BC\224\u01BC\u01BC\362\224\u01BC\224\u01BA\224\u0150\u0150"+
    "\u0150\u01CE\u01D4\362\224\224\u01BE\u01F8\356\356\u0178\362\u01F6\u01F6\u01F6"+
    "\u01F6\u01F6\362\224\224\u01E6\u01FA\u013E\u013E\u013E\u013E\u013E\u013E\u013E"+
    "\u01FC\u01FC\u01FA\356\u01FA\u01FA\u01FA\u01FE\u01FE\u01FE\u01FE\u01FE\u01C6"+
    "\u01C6\u01C6\u01C6\u01C6\u012E\u012E\u012E\022\022\u0200\224\224\224\224\u01BC"+
    "\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\u01BE"+
    "\362\u01CE\u0150\u0150\u0150\u0150\u0150\u0150\u01AA\u0150\356\u01F4\356\224"+
    "\224\u01BA\u0150\u0150\u0150\u0150\u0150\u01CE\u0150\u0150\u0150\u0150\u0150"+
    "\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u01C0"+
    "\u01FA\u01FA\u01FA\u01FA\u0202\u01FA\u01FA\u0204\u01FA\u013E\u013E\u01FC\u01FA"+
    "\u0206\u01EE\362\362\224\224\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\224\224\u01E8\u01B0\u0150\u01AA\u0150\u0150\u01A2\u01B2"+
    "\u0208\u01B0\u01D4\u01FE\u01FE\u01FE\u01FE\u01FE\u013E\u013E\u013E\224\224"+
    "\224\u01AE\u0150\224\224\u0150\u01D4\u020A\u020C\u01E8\u020A\u0200\u0200\224"+
    "\u01BA\u0150\u01D4\224\224\224\224\224\224\u01AA\u01B0\u020E\u0200\u0200\u0210"+
    "\u0212\u01F6\u01F6\u01F6\u01F6\u01F6\u0200\u01B0\u01FA\u0214\u0214\u0214\u0214"+
    "\u0214\u0214\u0214\u0214\u0214\u0214\u0214\u0214\u0214\u0214\u0214\u0214\u0214"+
    "\u0214\u0214\u0216\362\362\u0216\362\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\224\u01CC\u0218\224\224\224\224\224"+
    "\u01BE\224\224\362\224\224\224\u01BE\u01BE\224\224\362\224\224\224\224\u01BE"+
    "\224\224\362\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\u01BE\224\224\362\224\224\224\u01BE\u01BE\224\224\362\224\224\224\224\224"+
    "\224\224\u01BE\224\224\224\224\224\224\224\224\224\224\224\224\u01BE\224\224"+
    "\362\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\u01BE"+
    "\u014E\u0178\u013E\u013E\u013E\u013E\u021A\u021C\u021C\u021C\u021C\u021E\u0220"+
    "\u01C6\u01C6\u01C6\u0222\362\224\224\224\224\224\224\224\224\u016A\u016A\u016A"+
    "\u016A\u016A\362\362\362\u0214\u0214\u0214\u0214\u0214\u0214\u0214\u0214\u0224"+
    "\u0224\u0224\362\u0226\u0226\u0226\362\u0228\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\224\224\224\u01CC\u022A\224\224\224"+
    "\224\224\224\224\224\u022C\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\u022E\u0230\362\224\224\224\224\224\u01CC\u013E\u0232\u0234\224\224\224\u01BE"+
    "\362\362\362\224\224\224\224\224\224\u01BE\224\224\u0150\u0194\362\362\362"+
    "\362\362\224\224\224\224\224\224\224\224\224\u0150\u01F4\u01EE\362\362\362"+
    "\362\224\224\224\224\224\224\224\224\224\u0150\362\362\362\362\362\362\224"+
    "\224\224\224\224\224\u01BE\224\u01BE\u0150\362\362\362\362\362\362\224\224"+
    "\224\224\224\224\224\224\224\224\356\u01B0\u0150\u0150\u0150\u01AE\u01AE\u01AE"+
    "\u01AE\u01AA\u01B2\356\356\356\356\356\u013E\u01B8\u013E\u01D0\u01B4\362\u01FE"+
    "\u01FE\u01FE\u01FE\u01FE\362\362\362\u0236\u0236\u0236\u0236\u0236\362\362"+
    "\362\020\020\020\u0238\020\u023A\356\u023C\u01F6\u01F6\u01F6\u01F6\u01F6\362"+
    "\362\362\224\u023E\224\224\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\224\224\362\362\362\362\224\224\u01BA"+
    "\u01D4\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\u01BA"+
    "\u01BE\362\362\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\362\362\362\362\362\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\u01BE\u0150\u01AA\u01AE\u01B0\u01AA\u01AE\362\362\u01AE"+
    "\u01AA\u01AE\u01AE\u01B2\356\362\362\u01DA\362\020\u01B6\u01B6\u01B6\u01B6"+
    "\u01B6\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\362\224"+
    "\224\u01BE\362\362\362\362\362\224\224\224\224\224\224\362\362\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\362\362\362\u01F6\u01F6\u01F6\u01F6"+
    "\u01F6\u0240\362\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\224\224\224\224\224\224\224\224"+
    "\224\224\224\u01BA\u01AA\u01B0\362\u013E\224\224\224\224\224\224\224\224\224"+
    "\224\u01E8\u01AA\u0150\u0150\u0150\u01C0\u0208\u01AA\u01B0\u0150\u0150\u0150"+
    "\u01AA\u01AE\u01AE\u01B0\u01A2\356\356\356\u0194\u014E\u01FE\u01FE\u01FE\u01FE"+
    "\u01FE\362\362\362\u01F6\u01F6\u01F6\u01F6\u01F6\362\362\362\u013E\u013E\u013E"+
    "\u01B8\u013E\u013E\u013E\362\356\356\356\356\356\356\356\u0242\u0150\u0150"+
    "\u0244\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\224\u0208\u0150\u0150\u01AA\u01AA\u01AE\u01AE\u01AA\u020C"+
    "\224\224\224\362\362\u01F6\u01F6\u01F6\u01F6\u01F6\u013E\u013E\u013E\u01FC"+
    "\u01FA\u01FA\u01FA\u01FA\u012E\356\356\356\356\u01FA\u01FA\u01FA\u01FA\u0204"+
    "\362\u0150\u0244\224\224\224\224\224\224\224\224\224\224\224\224\224\224\u01E8"+
    "\u0150\u0150\u01AE\u0150\u0210\u0150\224\u01F6\u01F6\u01F6\u01F6\u01F6\224"+
    "\224\224\224\224\224\u0208\u0150\u01AE\u01B0\u01B0\u0150\u0200\362\362\362"+
    "\362\u013E\u013E\224\224\u01AE\u01AE\u01AE\u01AE\u0150\u0150\u0150\u0150\u01AE"+
    "\356\362\u0146\u013E\u013E\u01FE\u01FE\u01FE\u01FE\u01FE\362\u01BC\224\u01F6"+
    "\u01F6\u01F6\u01F6\u01F6\224\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\342\342\342\u013E\262\262\262\262\u0246\362\362\362\362\362\362\362"+
    "\362\362\362\362\u013E\u013E\u013E\u013E\362\362\362\362\356\u01F4\356\356"+
    "\356\356\356\356\u0248\356\356\356\u01AC\224\u01B4\224\224\u01AE\u01AC\u0212"+
    "\356\362\362\362\210\210\210\210\210\210\210\210\210\210\210\210\210\210\210"+
    "\210\210\210\210\210\210\210\334\334\334\334\334\334\334\334\334\334\334\334"+
    "\334\334\334\334\334\334\334\334\334\334\334\334\334\334\334\334\334\334\334"+
    "\u024A\210\210\210\210\210\210\u024C\210\304\210\210\210\210\210\210\210\210"+
    "\210\210\210\210\210\210\u024E\334\334\356\356\356\u0178\u0150\u0150\u0150"+
    "\u0150\u0150\u0150\u01A2\356\356\u014E\356\356\140\140\140\140\140\140\140"+
    "\140\140\140\140\262\262\u0250\210\u0252\u0254\u0254\u0254\u0254\u0256\u0256"+
    "\u0256\u0256\u0254\u0254\u0254\362\u0256\u0256\u0256\362\u0254\u0254\u0254"+
    "\u0254\u0256\u0256\u0256\u0256\u0254\u0254\u0254\u0254\u0256\u0256\u0256\u0256"+
    "\u0254\u0254\u0254\362\u0256\u0256\u0256\362\u0258\u0258\u0258\u0258\u025A"+
    "\u025A\u025A\u025A\u0254\u0254\u0254\u0254\u0256\u0256\u0256\u0256\u025C\u025E"+
    "\u025E\u0260\u0262\u0264\u0266\362\262\262\262\262\u0268\u0268\u0268\u0268"+
    "\262\262\262\262\u0268\u0268\u0268\u0268\262\262\262\262\u0268\u0268\u0268"+
    "\u0268\u0254\262\u0246\262\u0256\u026A\u026C\u026E\344\262\u0246\262\u0270"+
    "\u0270\u026C\344\u0254\262\362\262\u0256\u0272\u0274\344\u0254\262\u0276\262"+
    "\u0256\u0278\u027A\344\362\262\u0246\262\u027C\u027E\u026C\u0280\u0282\u0282"+
    "\u0282\u0284\u0282\u0286\u0288\u028A\u028C\u028C\u028C\020\u028E\u0290\u028E"+
    "\u0290\020\020\020\020\u0292\u0294\u0294\u0296\u0298\u0298\u029A\020\u029C"+
    "\u029E\020\u02A0\u02A2\020\u02A4\u02A6\020\020\020\020\020\u02A8\u02A2\020"+
    "\020\020\020\u02AA\u0288\u0288\u023C\u0294\u0294\u0288\u0288\u0288\u02AC\362"+
    "\110\110\110\u02AE\u02B0\u02B2\u02B4\u02B4\u02B4\u02B4\u02B4\u02AE\u02B0\u0230"+
    "\334\334\334\334\334\334\u02B6\362\072\072\072\072\072\072\072\072\072\072"+
    "\072\072\072\072\072\072\362\362\362\362\362\362\362\362\356\356\356\356\356"+
    "\356\u02B8\u0130\u02BA\u0130\u02BA\356\356\356\356\356\u0194\362\362\362\362"+
    "\362\362\362\u016A\u02BC\u016A\u02BE\u016A\u02C0\u0116\210\u0116\u02C2\u02BE"+
    "\u016A\u02C4\u0116\u0116\u016A\u016A\u016A\u02BC\u02C6\u02BC\u0214\u0116\u02C8"+
    "\u0116\u02CA\220\224\332\u016A\210\u0116\036\u0162\u02C4\210\210\u02CC\u016A"+
    "\u02CE\120\120\120\120\120\120\120\120\u02D0\u02D0\u02D0\u02D0\u02D0\u02D0"+
    "\u02D2\u02D2\u02D4\u02D4\u02D4\u02D4\u02D4\u02D4\u02D6\u02D6\u02D8\u02DA\u02DC"+
    "\u02D8\u02DE\u016A\362\362\u0162\u0162\u02E0\u016A\u016A\u0162\u016A\u016A"+
    "\u02E0\u02CC\u016A\u02E0\u016A\u016A\u016A\u02E0\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u0162\u016A\u02E0"+
    "\u02E0\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u0162\u0162\u0162\u0162\u0162\u0162\u02E2\u02E4\036\u0162"+
    "\u02E4\u02E4\u02E4\u0162\u02E2\u02E6\u02E2\036\u0162\u02E4\u02E4\u02E2\u02E4"+
    "\036\036\036\u0162\u02E2\u02E4\u02E4\u02E4\u02E4\u0162\u0162\u02E2\u02E2\u02E4"+
    "\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\036\u0162\u0162\u02E4\u02E4\u0162"+
    "\u0162\u0162\u0162\u02E2\036\036\u02E4\u02E4\u02E4\u02E4\u0162\u02E4\u02E4"+
    "\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4"+
    "\036\u02E2\u02E4\036\u0162\u0162\036\u0162\u0162\u0162\u0162\u02E4\u0162\u02E4"+
    "\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\036\u0162\u0162\u02E4\u0162"+
    "\u0162\u0162\u0162\u02E2\u02E4\u02E4\u0162\u02E4\u0162\u0162\u02E4\u02E4\u02E4"+
    "\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u0162\u02E4\u02E4\u02E4"+
    "\u02E4\u02E4\u02E4\u02E4\u02E4\u016A\u016A\u016A\u016A\022\022\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u02E4\u016A\u016A\u016A\u02E8"+
    "\u02EA\u016A\u016A\u016A\u016A\u016A\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u02EC\u02E0\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u02EE\u016A\u016A\u02CC\u0162\u0162\u0162\u0162\u0162\u0162\u0162\u0162"+
    "\u0162\u0162\u0162\u0162\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u0162\u0162"+
    "\u0162\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u01DA\362\362\362\362\362\362\362\362"+
    "\362\362\362\362\u016A\u016A\u016A\u016A\u016A\u01DA\362\362\362\362\362\362"+
    "\362\362\362\362\u02F0\u02F0\u02F0\u02F0\u02F0\u02F0\u02F0\u02F0\u02F0\u02F0"+
    "\u02F2\u02F2\u02F2\u02F2\u02F2\u02F2\u02F2\u02F2\u02F2\u02F2\u02F4\u02F4\u02F4"+
    "\u02F4\u02F4\u02F4\u02F4\u02F4\u02F4\u02F4\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u02F6\u02F6\u02F6\u02F6\u02F6\u02F6"+
    "\u02F6\u02F6\u02F6\u02F6\u02F6\u02F6\u02F6\u02F8\u02F8\u02F8\u02F8\u02F8\u02F8"+
    "\u02F8\u02F8\u02F8\u02F8\u02F8\u02F8\u02F8\u02FA\u02FC\u02FC\u02FC\u02FC\u02FE"+
    "\u0300\u0300\u0300\u0300\u0302\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u02CC\u016A\u016A\u016A\u016A\u02CC\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u0162\u0162\u0162"+
    "\u0162\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u02CC\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u02EC\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\022\022"+
    "\022\022\022\022\022\u0304\u0304\u0304\u0304\u0304\u02F0\u02F0\u02F0\u02F0"+
    "\u02F0\u0306\u0306\u0306\u0306\u0306\u016A\u016A\u016A\u016A\u016A\u016A\036"+
    "\u02E2\u0308\u030A\u02E4\u02E2\u02E4\u0162\u0162\u02E2\u02E4\036\u0162\u0162"+
    "\u02E4\036\u0162\u02E4\u02E4\022\022\022\022\022\u0162\u0162\u0162\u0162\u0162"+
    "\u0162\u0162\u0162\u0162\u0162\u0162\u0162\u0162\u0162\u0162\u0162\u0162\u0162"+
    "\u0162\u0162\u0162\u0162\u0162\u0162\u0162\u02B0\u030C\u030C\u030C\u030C\u030C"+
    "\u030C\u030C\u030C\u030C\u030C\u030A\u02E2\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4"+
    "\u02E4\u02E4\u02E4\u02E4\u0162\u0162\u0162\u0162\036\u0162\u0162\u0162\u02E4"+
    "\u02E4\u02E4\u0162\u02E2\u0162\u0162\u02E4\u02E4\036\u02E4\u0162\022\022\036"+
    "\u0162\u02E2\u02E2\u02E4\u0162\u02E4\u0162\u0162\u0162\u0162\u0162\u02E4\u02E4"+
    "\u02E4\u0162\022\u0162\u0162\u0162\u0162\u0162\u0162\u02E4\u02E4\u02E4\u02E4"+
    "\u02E4\u02E4\u02E4\u02E4\u02E4\036\u02E4\u02E4\u0162\036\036\u02E2\u02E2\u02E4"+
    "\036\u0162\u0162\u02E4\u0162\u0162\u0162\u02E4\036\u0162\u0162\u0162\u0162"+
    "\u0162\u0162\u0162\u0162\u0162\u0162\u0162\u02E2\036\u0162\u0162\u0162\u0162"+
    "\u0162\u02E4\u0162\u0162\u02E4\u02E4\u02E2\036\u02E2\036\u0162\u02E2\u02E4"+
    "\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4"+
    "\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u0162\u02E4\u02E4\u02E4\u02E4\u02E2"+
    "\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4"+
    "\u02E4\u02E4\u02E4\u02E4\u02E4\u02E4\036\u0162\u0162\036\036\u0162\u02E4\u02E4"+
    "\036\u0162\u0162\u02E4\036\u0162\u02E2\u0162\u02E2\u02E4\u02E4\u02E2\u0162"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u0162\u0162\u0162\u0162\u0162"+
    "\u0162\u0162\u0162\u0162\u0162\u02E0\u02CC\u0162\u0162\u02E0\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\362\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\362\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\362"+
    "\u014A\u016A\u016A\u016A\u016A\u016A\u01DA\u016A\u016A\u016A\u016A\u01DA\362"+
    "\362\362\362\362\362\362\362\362\362\362\362\u016A\u016A\362\362\362\362\362"+
    "\362\362\362\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138"+
    "\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u0138\u013A"+
    "\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142"+
    "\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u0142\u030E\140\u0214"+
    "\252\254\146\146\u0310\u0214\u0252\140\144\164\210\210\334\u0214\140\140\u0312"+
    "\u016A\u016A\u0314\146\u0316\356\140\362\362\u0318\020\u031A\020\262\262\262"+
    "\262\262\262\262\262\262\262\262\262\262\262\262\262\262\262\262\u031C\362"+
    "\362\u031C\362\224\224\224\224\224\224\224\224\224\224\224\224\362\362\362"+
    "\u013C\u01EE\362\362\362\362\362\362\u014E\224\224\224\224\224\224\224\224"+
    "\224\224\224\u01BE\362\362\362\362\224\224\224\u01BE\224\224\224\u01BE\224"+
    "\224\224\u01BE\224\224\224\u01BE\u0150\u0150\u0150\u0150\u0150\u0150\u0150"+
    "\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u0150\u0150\020\u031E\u031E\020"+
    "\u029C\u029E\u031E\020\020\020\020\u0320\020\u0238\u031E\020\u031E\022\022"+
    "\022\022\020\020\u0322\020\020\020\020\020\u028C\020\020\u0238\u0324\020\020"+
    "\020\362\362\362\362\362\362\362\362\362\362\362\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u014A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\362\362\362\362\362\362\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\u016A\362\362\362\362"+
    "\362\362\362\362\362\362\362\362\362\u016A\u016A\u016A\u016A\u016A\u016A\362"+
    "\362\012\020\u0326\u0328\022\022\022\022\022\u016A\022\022\022\022\u032A\u032C"+
    "\u032E\u0330\u0330\u0330\u0330\356\356\u0200\u0332\342\342\u016A\u0334\u0336"+
    "\u0338\u016A\224\224\224\224\224\224\224\224\224\224\224\u01BE\u014E\u033A"+
    "\u033C\u0218\224\224\224\224\224\224\224\224\224\224\224\224\224\u0338\342"+
    "\u0218\362\362\u01BC\224\224\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\224\u01BE\u01BC\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\u01BE\u01FA\u033E\u033E\u01FA\u01FA\u01FA\u01FA\u01FA\224"+
    "\224\224\224\224\224\224\224\224\224\224\224\224\u01BE\362\362\u016A\u016A"+
    "\362\362\362\362\362\362\224\224\224\224\224\224\224\224\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u02EC\u01DA"+
    "\u0340\u0340\u0340\u0340\u0340\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u0342\u0344\u01C6\u01C6\u0346\u0348"+
    "\u0348\u0348\u0348\u0348\120\120\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u016A\u02EE\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u01FA\u034A\120\120\120\120\120\120\120\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u016A\u016A\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u0204\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u02EC\u016A\u02EE\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u016A\u01FA\u01FA\u01FA\u01FA"+
    "\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u01FA\u02EC\u034C"+
    "\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C"+
    "\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C"+
    "\362\362\362\362\362\u034C\u034C\u034C\u034C\u034C\u034E\362\362\362\362\362"+
    "\362\362\362\362\362\224\224\224\224\224\224\224\224\224\224\u023E\224\224"+
    "\224\224\224\224\224\224\224\224\224\u01BE\362\u016A\u016A\u016A\u016A\u016A"+
    "\u016A\u016A\u016A\u016A\u016A\u016A\u01DA\362\362\362\362\224\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\u0350\020\224\224\224\224\224\224"+
    "\224\224\u01FE\u01FE\u01FE\u01FE\u01FE\224\362\362\362\362\362\362\362\362"+
    "\362\362\140\140\140\140\140\140\140\u01B4\u0130\u0352\u0150\u0150\u0150\u0150"+
    "\356\u0322\140\140\140\140\140\140\140\140\140\140\140\140\140\140\334\u0150"+
    "\224\224\224\u0354\u0354\u0354\u0354\u0356\356\u013E\u013E\u013E\362\362\362"+
    "\362\344\344\344\344\344\344\344\344\344\344\344\u0358\346\346\346\346\344"+
    "\140\140\140\140\140\140\140\210\140\140\140\140\140\140\140\140\140\140\140"+
    "\140\140\140\140\u024A\210\210\210\144\146\u0310\140\140\140\140\140\u035A"+
    "\u035C\u0310\220\140\140\210\140\140\140\140\140\140\140\140\140\140\u0214"+
    "\u0214\u035E\u0214\u0214\140\140\362\362\362\362\362\362\362\362\362\362\362"+
    "\362\362\362\362\u01BC\334\220\224\224\224\u01AC\224\u01AC\224\u01B4\224\224"+
    "\224\224\224\224\224\224\224\224\224\u01E8\u01B0\u01AA\u016A\u016A\362\362"+
    "\u01C6\u01C6\u01C6\u01FA\u0360\362\362\362\224\224\224\224\224\224\224\224"+
    "\224\224\020\020\362\362\362\362\u01AE\224\224\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\u01AE\u01AE"+
    "\u01AE\u01AE\u01AE\u01AE\u01AE\u01AE\u0178\362\362\362\362\u013E\u01F6\u01F6"+
    "\u01F6\u01F6\u01F6\362\362\362\356\356\356\356\356\356\356\356\356\224\224"+
    "\224\u013E\u022A\u022A\362\u01FE\u01FE\u01FE\u01FE\u01FE\224\224\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\u0150\u0150\u01A2\356\u013E\224\224\224"+
    "\224\224\224\224\224\224\224\224\u01BA\u0150\u0150\u0150\u0150\u0150\u020A"+
    "\362\362\362\362\362\u0146\224\224\224\224\224\224\224\224\224\224\224\224"+
    "\224\224\u01BE\362\224\224\224\224\224\224\224\224\224\u01B4\u01AE\u0150\u0150"+
    "\u01AE\u01AA\u01AE\u0362\u013E\u013E\u013E\u013E\u013E\u013E\u013C\u01F6\u01F6"+
    "\u01F6\u01F6\u01F6\362\362\u013E\224\224\u01B4\u0218\224\224\224\224\u01F6"+
    "\u01F6\u01F6\u01F6\u01F6\224\224\u01BE\224\224\224\224\u01BA\u0150\u0150\u01AA"+
    "\u01B0\u01AA\u01B0\u01C0\362\362\362\362\224\u01BA\224\224\224\224\u01AA\362"+
    "\u01F6\u01F6\u01F6\u01F6\u01F6\362\u013E\u013E\224\224\224\224\224\224\224"+
    "\224\u0218\224\224\u01E6\u01FA\u0212\u0248\224\224\224\224\224\224\224\224"+
    "\224\u01D4\u0150\u01D4\u01BA\u01D4\224\224\u01A2\u01B4\u01BE\362\362\362\362"+
    "\362\362\362\362\362\362\362\u01BC\u023E\u013E\224\224\224\224\224\u01E8\u0150"+
    "\u01AE\u013E\u023E\u0364\u0194\362\362\362\362\u01BC\224\224\u01BE\u01BC\224"+
    "\224\u01BE\u01BC\224\224\u01BE\362\362\362\362\224\224\224\u01BE\224\224\224"+
    "\u01BE\210\210\210\210\210\210\210\210\210\210\210\210\210\210\210\210\210"+
    "\304\210\210\210\u0366\334\334\210\210\210\362\362\362\362\362\262\262\262"+
    "\262\262\262\262\262\224\u01E8\u01B0\u01AE\u01AA\u0368\u0210\362\u01F6\u01F6"+
    "\u01F6\u01F6\u01F6\362\362\362\224\224\362\362\362\362\362\362\224\224\224"+
    "\224\224\224\224\224\224\224\224\u01BE\362\u01BC\224\224\224\224\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\362\362"+
    "\u036A\u036A\u036A\u036A\u036A\u036A\u036A\u036A\u036A\u036A\u036A\u036A\u036A"+
    "\u036A\u036A\u036A\u036C\u036C\u036C\u036C\u036C\u036C\u036C\u036C\u036C\u036C"+
    "\u036C\u036C\u036C\u036C\u036C\u036C\u034C\u034C\u034C\u034C\u034C\u036E\u034C"+
    "\u034C\u034C\u0370\u034C\u034C\u0372\u034C\u034C\u034C\u034C\u034C\u034C\u034C"+
    "\u034C\u034C\u034C\u034C\u034C\u0374\u034C\u034C\u034C\u034C\u034C\u034C\u034C"+
    "\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u0376\u0378\u034C\u034C\u034C\u034C"+
    "\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C"+
    "\u034C\u034C\u034C\u037A\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\362"+
    "\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C"+
    "\u034C\u034C\u034C\u034C\u034C\u034C\u034C\u034C\362\362\362\262\262\262\u0246"+
    "\362\362\362\362\362\u031C\262\262\362\362\u037C\u037E\u0158\u0158\u0158\u0158"+
    "\u0380\u0158\u0158\u0158\u0158\u0158\u0158\u015A\u0158\u0158\u015A\u015A\u0158"+
    "\u037C\u015A\u0158\u0158\u0158\u0158\u0158\u0172\u0172\u0172\u0172\u0172\u0172"+
    "\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0382\u0382"+
    "\u0382\u0382\u0382\u0382\u0382\u0382\362\362\362\362\362\362\362\362\u0196"+
    "\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172"+
    "\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0384\362\362\362\362\362"+
    "\362\362\362\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172"+
    "\u0172\u0172\u0172\u0172\u0172\362\u0172\u0172\u0172\u0172\u0172\u0172\u0172"+
    "\u0172\u0172\u0172\u0172\362\362\362\362\362\362\362\362\362\362\362\362\362"+
    "\362\362\362\362\362\362\362\u0172\u0172\u0172\u0172\u0172\u0172\u0386\362"+
    "\356\356\356\356\356\356\356\356\020\020\020\u0388\u038A\362\362\362\356\356"+
    "\356\356\356\356\356\356\u0320\u038C\u038E\u0384\u0384\u0384\u0384\u0384\u0384"+
    "\u0384\u038A\u0388\u038A\020\u02A0\u0390\034\u0392\u0394\020\u0396\u030C\u030C"+
    "\u0398\020\u039A\u02E4\u039C\u039E\u029A\362\362\u0172\u0172\u01A6\u0172\u0172"+
    "\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172\u0172"+
    "\u0172\u0172\u0172\u0172\u01A6\u03A0\u0318\014\016\020\022\024\026\030\032"+
    "\032\032\032\032\034\036\040\054\056\056\056\056\056\056\056\056\056\056\056"+
    "\056\060\062\u02B0\u02A6\022\020\224\224\224\224\224\u0218\224\224\224\224"+
    "\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\224\342"+
    "\362\224\224\224\362\224\224\224\362\224\224\224\362\224\u01BE\362\072\u03A2"+
    "\u014C\u03A4\u02CC\u0162\u02E0\u01DA\362\362\362\362\u03A6\u03A8\u016A\362").toCharArray();

  // The A table has 938 entries for a total of 3752 bytes.

  static final int A[] = new int[938];
  static final String A_DATA =
    "\u4800\u100F\u4800\u100F\u4800\u100F\u5800\u400F\u5000\u400F\u5800\u400F\u6000"+
    "\u400F\u5000\u400F\u5000\u400F\u5000\u400F\u6000\u400C\u6800\030\u6800\030"+
    "\u2800\030\u2800\u601A\u2800\030\u6800\030\u6800\030\uE800\025\uE800\026\u6800"+
    "\030\u2000\031\u3800\030\u2000\024\u3800\030\u3800\030\u1800\u3609\u1800\u3609"+
    "\u3800\030\u6800\030\uE800\031\u6800\031\uE800\031\u6800\030\u6800\030\202"+
    "\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\uE800\025\u6800\030\uE800\026\u6800\033"+
    "\u6800\u5017\u6800\033\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\uE800\025\u6800"+
    "\031\uE800\026\u6800\031\u4800\u100F\u4800\u100F\u5000\u100F\u3800\014\u6800"+
    "\030\u2800\u601A\u2800\u601A\u6800\034\u6800\030\u6800\033\u6800\034\000\u7005"+
    "\uE800\035\u6800\031\u4800\u1010\u6800\034\u6800\033\u2800\034\u2800\031\u1800"+
    "\u060B\u1800\u060B\u6800\033\u07FD\u7002\u6800\033\u1800\u050B\000\u7005\uE800"+
    "\036\u6800\u080B\u6800\u080B\u6800\u080B\u6800\030\202\u7001\202\u7001\202"+
    "\u7001\u6800\031\202\u7001\u07FD\u7002\201\u7002\201\u7002\201\u7002\u6800"+
    "\031\201\u7002\u061D\u7002\006\u7001\005\u7002\u07FF\uF001\u03A1\u7002\000"+
    "\u7002\006\u7001\005\u7002\006\u7001\005\u7002\u07FD\u7002\u061E\u7001\006"+
    "\u7001\u04F5\u7002\u034A\u7001\u033A\u7001\006\u7001\005\u7002\u0336\u7001"+
    "\u0336\u7001\006\u7001\005\u7002\000\u7002\u013E\u7001\u032A\u7001\u032E\u7001"+
    "\006\u7001\u033E\u7001\u067D\u7002\u034E\u7001\u0346\u7001\u0575\u7002\000"+
    "\u7002\u034E\u7001\u0356\u7001\u05F9\u7002\u035A\u7001\u036A\u7001\006\u7001"+
    "\005\u7002\u036A\u7001\000\u7002\000\u7002\005\u7002\u0366\u7001\u0366\u7001"+
    "\006\u7001\005\u7002\u036E\u7001\000\u7002\000\u7005\000\u7002\u0721\u7002"+
    "\000\u7005\000\u7005\012\uF001\007\uF003\011\uF002\012\uF001\007\uF003\011"+
    "\uF002\011\uF002\006\u7001\005\u7002\u013D\u7002\u07FD\u7002\012\uF001\u067E"+
    "\u7001\u0722\u7001\u05FA\u7001\000\u7002\u07FE\u7001\006\u7001\005\u7002\u0576"+
    "\u7001\u07FE\u7001\u07FD\u7002\u07FD\u7002\006\u7001\005\u7002\u04F6\u7001"+
    "\u0116\u7001\u011E\u7001\u07FD\u7002\u07FD\u7002\u07FD\u7002\u0349\u7002\u0339"+
    "\u7002\000\u7002\u0335\u7002\u0335\u7002\000\u7002\u0329\u7002\000\u7002\u032D"+
    "\u7002\u07FD\u7002\000\u7002\u0335\u7002\u07FD\u7002\000\u7002\u033D\u7002"+
    "\000\u7002\u07FD\u7002\u0345\u7002\u034D\u7002\000\u7002\u034D\u7002\u0355"+
    "\u7002\000\u7002\000\u7002\u0359\u7002\u0369\u7002\000\u7002\000\u7002\u0369"+
    "\u7002\u0369\u7002\u0115\u7002\u0365\u7002\u0365\u7002\u011D\u7002\000\u7002"+
    "\u036D\u7002\000\u7002\000\u7005\000\u7002\000\u7004\000\u7004\000\u7004\u6800"+
    "\u7004\u6800\u7004\000\u7004\000\u7004\000\u7004\u6800\033\u6800\033\u6800"+
    "\u7004\u6800\u7004\000\u7004\u6800\033\u6800\u7004\u6800\033\000\u7004\u6800"+
    "\033\u4000\u3006\u4000\u3006\u4000\u3006\u46B1\u3006\u7800\000\u7800\000\000"+
    "\u7004\u05F9\u7002\u05F9\u7002\u05F9\u7002\u6800\030\u01D2\u7001\232\u7001"+
    "\u6800\030\226\u7001\226\u7001\226\u7001\u7800\000\u0102\u7001\u7800\000\376"+
    "\u7001\376\u7001\u07FD\u7002\202\u7001\u7800\000\202\u7001\231\u7002\225\u7002"+
    "\225\u7002\225\u7002\u07FD\u7002\201\u7002\175\u7002\201\u7002\u0101\u7002"+
    "\375\u7002\375\u7002\042\u7001\371\u7002\345\u7002\000\u7001\000\u7001\000"+
    "\u7001\275\u7002\331\u7002\041\u7002\u0159\u7002\u0141\u7002\u07E5\u7002\u01D1"+
    "\u7002\u0712\u7001\u0181\u7002\u6800\031\006\u7001\005\u7002\u07E6\u7001\000"+
    "\u7002\u05FA\u7001\u05FA\u7001\u05FA\u7001\u0142\u7001\u0142\u7001\u0141\u7002"+
    "\u0141\u7002\000\034\u4000\u3006\u4000\007\u4000\007\076\u7001\006\u7001\005"+
    "\u7002\075\u7002\u7800\000\302\u7001\302\u7001\302\u7001\302\u7001\u7800\000"+
    "\u7800\000\000\u7004\000\030\000\030\u7800\000\301\u7002\301\u7002\301\u7002"+
    "\301\u7002\u07FD\u7002\u7800\000\000\030\u6800\024\u7800\000\u7800\000\u6800"+
    "\034\u6800\034\u2800\u601A\u7800\000\u4000\u3006\u4000\u3006\u4000\u3006\u0800"+
    "\024\u4000\u3006\u0800\030\u4000\u3006\u4000\u3006\u0800\030\u0800\u7005\u0800"+
    "\u7005\u0800\u7005\u7800\000\u0800\u7005\u0800\030\u0800\030\u7800\000\u3000"+
    "\u1010\u3000\u1010\u6800\031\u6800\031\u1000\031\u2800\030\u2800\030\u1000"+
    "\u601A\u3800\030\u1000\030\u6800\034\u6800\034\u4000\u3006\u1000\030\u1000"+
    "\u1010\u7800\000\u1000\030\u1000\030\u1000\u7005\u1000\u7005\u1000\u7004\u1000"+
    "\u7005\u1000\u7005\u4000\u3006\u4000\u3006\u4000\u3006\u3000\u3409\u3000\u3409"+
    "\u2800\030\u3000\030\u3000\030\u1000\030\u4000\u3006\u1000\u7005\u1000\030"+
    "\u1000\u7005\u4000\u3006\u3000\u1010\u6800\034\u4000\u3006\u4000\u3006\u1000"+
    "\u7004\u1000\u7004\u4000\u3006\u4000\u3006\u6800\034\u1000\u7005\u1000\034"+
    "\u1000\034\u1000\u7005\u7800\000\u1000\u1010\u4000\u3006\u7800\000\u7800\000"+
    "\u1000\u7005\u0800\u3409\u0800\u3409\u0800\u7005\u4000\u3006\u0800\u7004\u0800"+
    "\u7004\u0800\u7004\u7800\000\u0800\u7004\u4000\u3006\u4000\u3006\u4000\u3006"+
    "\u0800\030\u0800\030\u1000\u7005\u7800\000\u3000\u1010\u4000\u3006\u4000\u3006"+
    "\000\u3008\u4000\u3006\000\u7005\000\u3008\000\u3008\000\u3008\u4000\u3006"+
    "\000\u3008\u4000\u3006\000\u7005\u4000\u3006\000\u3749\000\u3749\000\030\000"+
    "\u7004\000\u7005\u4000\u3006\u7800\000\000\u7005\000\u7005\u7800\000\u4000"+
    "\u3006\u7800\000\u7800\000\000\u3008\000\u3008\u7800\000\000\u080B\000\u080B"+
    "\000\u080B\000\u06EB\000\034\u2800\u601A\000\u7005\000\030\u7800\000\u4000"+
    "\u3006\000\030\u2800\u601A\000\034\000\u7005\u4000\u3006\000\u7005\000\u074B"+
    "\000\u080B\000\u080B\u6800\034\u6800\034\u7800\000\u6800\u050B\u6800\u050B"+
    "\u6800\u04AB\u6800\u04AB\u6800\u04AB\000\034\000\u3008\000\u3006\000\u3006"+
    "\000\u3008\000\u7005\000\034\000\u7005\000\u3008\000\u080B\000\u7005\000\u080B"+
    "\000\034\000\030\u7800\000\u7800\000\u2800\u601A\000\u7004\u4000\u3006\u4000"+
    "\u3006\000\030\000\u3609\000\u3609\000\u7004\u7800\000\000\034\000\034\000"+
    "\030\000\034\000\u3409\000\u3409\000\u3008\000\u3008\u4000\u3006\000\034\000"+
    "\034\u7800\000\000\034\000\030\u4000\u3006\000\u3008\000\u3008\000\u3008\000"+
    "\u3008\000\u7005\u4000\u3006\000\u3008\000\u3008\u4000\u3006\000\u7005\000"+
    "\u3008\u07FE\u7001\u07FE\u7001\u7800\000\u07FE\u7001\000\u7004\000\u7005\000"+
    "\030\000\u070B\000\u070B\000\u070B\000\u070B\000\u042B\000\u054B\000\u080B"+
    "\000\u080B\u7800\000\042\u7001\042\u7001\041\u7002\041\u7002\u6800\024\000"+
    "\u7005\000\030\000\u7005\u6000\u400C\000\u7005\000\u7005\uE800\025\uE800\026"+
    "\u7800\000\000\u746A\000\u746A\000\u746A\000\u7005\u6800\u060B\u6800\u060B"+
    "\u6800\024\u6800\030\u6800\030\u4000\u3006\u4800\u1010\u7800\000\000\u7005"+
    "\000\u7004\000\u04EB\u7800\000\u4000\007\u7800\000\000\u3008\000\u7005\u07FD"+
    "\u7002\u7800\000\u4000\u3006\000\u3008\000\u7004\000\u7002\000\u7004\u07FD"+
    "\u7002\000\u7002\000\u7004\u07FD\u7002\355\u7002\u07FE\u7001\000\u7002\u07E1"+
    "\u7002\u07E1\u7002\u07E2\u7001\u07E2\u7001\u07FD\u7002\u07E1\u7002\u7800\000"+
    "\u07E2\u7001\u06D9\u7002\u06D9\u7002\u06A9\u7002\u06A9\u7002\u0671\u7002\u0671"+
    "\u7002\u0601\u7002\u0601\u7002\u0641\u7002\u0641\u7002\u0609\u7002\u0609\u7002"+
    "\u07FF\uF003\u07FF\uF003\u06DA\u7001\u06DA\u7001\u07FF\uF003\u6800\033\u07FD"+
    "\u7002\u6800\033\u06AA\u7001\u06AA\u7001\u0672\u7001\u0672\u7001\u7800\000"+
    "\u6800\033\u07FD\u7002\u07E5\u7002\u0642\u7001\u0642\u7001\u07E6\u7001\u6800"+
    "\033\u0602\u7001\u0602\u7001\u060A\u7001\u060A\u7001\u6800\033\u7800\000\u6000"+
    "\u400C\u6000\u400C\u6000\u400C\u6000\014\u6000\u400C\u4800\u1010\u4800\u1010"+
    "\u4800\u1010\000\u1010\u0800\u1010\u6800\024\u6800\024\u6800\035\u6800\036"+
    "\u6800\025\u6800\035\u6000\u400D\u5000\u400E\u7800\u1010\u7800\u1010\u7800"+
    "\u1010\u3800\014\u2800\030\u2800\030\u2800\030\u6800\030\u6800\030\uE800\035"+
    "\uE800\036\u6800\030\u6800\030\u6800\u5017\u6800\u5017\u6800\030\u3800\031"+
    "\uE800\025\uE800\026\u6800\030\u6800\031\u6800\030\u6800\030\u6000\u400C\u1800"+
    "\u060B\000\u7004\u2000\031\u2000\031\u6800\031\uE800\025\uE800\026\000\u7004"+
    "\u1800\u040B\u1800\u040B\000\u7004\u7800\000\u4000\u3006\u4000\007\u4000\007"+
    "\u4000\u3006\000\u7001\u6800\034\u6800\034\000\u7001\000\u7002\000\u7001\000"+
    "\u7001\000\u7002\u6800\031\000\u7001\u07FE\u7001\u6800\034\u2800\034\000\u7002"+
    "\162\u7001\000\u7001\u6800\034\u6800\031\161\u7002\000\034\102\u742A\102\u742A"+
    "\102\u780A\102\u780A\101\u762A\101\u762A\101\u780A\101\u780A\000\u780A\000"+
    "\u780A\000\u780A\006\u7001\005\u7002\000\u742A\000\u780A\u6800\u06EB\u6800"+
    "\031\u6800\034\u6800\031\uE800\031\uE800\031\uE800\031\u2000\031\u2800\031"+
    "\u6800\034\uE800\025\uE800\026\u6800\034\000\034\u6800\034\u6800\034\000\034"+
    "\u6800\u042B\u6800\u042B\u6800\u05AB\u6800\u05AB\u1800\u072B\u1800\u072B\152"+
    "\034\152\034\151\034\151\034\u6800\u06CB\u6800\u040B\u6800\u040B\u6800\u040B"+
    "\u6800\u040B\u6800\u058B\u6800\u058B\u6800\u058B\u6800\u058B\u6800\u042B\u6800"+
    "\u056B\u6800\u056B\u6800\u06EB\u6800\u06EB\uE800\031\uE800\025\uE800\026\u6800"+
    "\031\uE800\026\uE800\025\301\u7002\u7800\000\005\u7002\u07FE\u7001\000\u7002"+
    "\u6800\034\u6800\034\006\u7001\005\u7002\u4000\u3006\u7800\000\u6800\030\u6800"+
    "\030\u6800\u080B\u7800\000\u07FD\u7002\uE800\035\uE800\036\u6800\030\u6800"+
    "\024\u6800\030\u6800\u7004\u6800\025\u6800\030\u6800\034\000\u7004\000\u7005"+
    "\000\u772A\u6800\024\u6800\025\u6800\026\u6800\026\u6800\034\000\u740A\000"+
    "\u740A\000\u740A\u6800\024\000\u7004\000\u764A\000\u776A\000\u748A\000\u7004"+
    "\000\u7005\u6800\030\u4000\u3006\u6800\033\u6800\033\000\u7004\000\u05EB\000"+
    "\u05EB\000\u042B\000\u042B\000\u044B\000\u056B\000\u068B\000\u080B\u6800\034"+
    "\u6800\u048B\u6800\u048B\u6800\u048B\000\034\u6800\u080B\000\u7005\000\u7005"+
    "\000\u7005\u7800\000\000\u7004\u6800\030\u4000\007\u6800\030\000\u776A\000"+
    "\u776A\000\u776A\000\u762A\u6800\033\u6800\u7004\u6800\u7004\000\033\000\033"+
    "\006\u7001\u07FE\u7001\u7800\000\u2800\u601A\u2800\034\000\u3008\000\030\000"+
    "\u7004\000\u3008\000\u7002\000\033\000\u3008\000\030\000\023\000\023\000\022"+
    "\000\022\000\u7005\000\u7705\000\u7005\000\u76E5\000\u7545\000\u7005\000\u75C5"+
    "\000\u7005\000\u7005\000\u76A5\000\u7005\000\u7665\000\u7005\000\u75A5\u7800"+
    "\000\u0800\u7005\u4000\u3006\u0800\u7005\u0800\u7005\u2000\031\u1000\033\u1000"+
    "\033\u6800\026\u6800\025\u1000\u601A\u6800\034\u6800\030\u6800\025\u6800\026"+
    "\u6800\030\u6800\024\u6800\u5017\u6800\u5017\u6800\025\u6800\u5017\u6800\u5017"+
    "\u3800\030\u7800\000\u6800\030\u3800\030\u6800\024\uE800\025\uE800\026\u2800"+
    "\030\u2000\031\u2000\024\u6800\031\u7800\000\u6800\030\u2800\u601A\u7800\000"+
    "\u4800\u1010\u6800\031\u6800\033\u2800\u601A\u7800\000\u7800\000\u6800\u1010"+
    "\u6800\u1010\u6800\u1010";

  // The B table has 938 entries for a total of 1876 bytes.

  static final char B[] = (
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\001\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\001\001\001\000\000\000\000\000"+
    "\000\000\000\000\001\000\000\000\000\000\000\000\000\005\000\000\001\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\004\004\000\004\000\004"+
    "\004\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\004\000\000\000\000\000\000\000\000\000\000\004\000\004\000\000"+
    "\000\000\000\000\004\000\000\000\004\000\000\000\004\000\000\004\004\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\004"+
    "\004\000\000\000\000\000\000\004\004\004\000\000\004\004\004\004\004\000\000"+
    "\000\000\000\000\000\000\004\000\000\000\000\004\000\000\004\004\000\000\000"+
    "\000\000\000\000\000\000\000\004\000\000\000\000\004\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\004\004\004\004\000\000\000\004\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\004\004\000\000\000\004\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\004\000\000\000\000\000\001\000\001"+
    "\000\000\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\001\000\000\000\000\000\001\000\000\001\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\002\002"+
    "\002\002\001\001\001\001\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\006\006"+
    "\005\005\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\020\020\000\000\000\000\000\020\020"+
    "\020\000\000\020\020\020\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\020\020\020\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\004\000\000\004\000"+
    "\000\000\000\000\020\020\020\020\020\020\020\020\020\020\020\020\020\020\000"+
    "\000\004\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000").toCharArray();

  // In all, the character property tables require 19624 bytes.

    static {
            charMap = new char[][][] {
        { {'\u00DF'}, {'\u0053', '\u0053', } },
        { {'\u0130'}, {'\u0130', } },
        { {'\u0149'}, {'\u02BC', '\u004E', } },
        { {'\u01F0'}, {'\u004A', '\u030C', } },
        { {'\u0390'}, {'\u0399', '\u0308', '\u0301', } },
        { {'\u03B0'}, {'\u03A5', '\u0308', '\u0301', } },
        { {'\u0587'}, {'\u0535', '\u0552', } },
        { {'\u1E96'}, {'\u0048', '\u0331', } },
        { {'\u1E97'}, {'\u0054', '\u0308', } },
        { {'\u1E98'}, {'\u0057', '\u030A', } },
        { {'\u1E99'}, {'\u0059', '\u030A', } },
        { {'\u1E9A'}, {'\u0041', '\u02BE', } },
        { {'\u1F50'}, {'\u03A5', '\u0313', } },
        { {'\u1F52'}, {'\u03A5', '\u0313', '\u0300', } },
        { {'\u1F54'}, {'\u03A5', '\u0313', '\u0301', } },
        { {'\u1F56'}, {'\u03A5', '\u0313', '\u0342', } },
        { {'\u1F80'}, {'\u1F08', '\u0399', } },
        { {'\u1F81'}, {'\u1F09', '\u0399', } },
        { {'\u1F82'}, {'\u1F0A', '\u0399', } },
        { {'\u1F83'}, {'\u1F0B', '\u0399', } },
        { {'\u1F84'}, {'\u1F0C', '\u0399', } },
        { {'\u1F85'}, {'\u1F0D', '\u0399', } },
        { {'\u1F86'}, {'\u1F0E', '\u0399', } },
        { {'\u1F87'}, {'\u1F0F', '\u0399', } },
        { {'\u1F88'}, {'\u1F08', '\u0399', } },
        { {'\u1F89'}, {'\u1F09', '\u0399', } },
        { {'\u1F8A'}, {'\u1F0A', '\u0399', } },
        { {'\u1F8B'}, {'\u1F0B', '\u0399', } },
        { {'\u1F8C'}, {'\u1F0C', '\u0399', } },
        { {'\u1F8D'}, {'\u1F0D', '\u0399', } },
        { {'\u1F8E'}, {'\u1F0E', '\u0399', } },
        { {'\u1F8F'}, {'\u1F0F', '\u0399', } },
        { {'\u1F90'}, {'\u1F28', '\u0399', } },
        { {'\u1F91'}, {'\u1F29', '\u0399', } },
        { {'\u1F92'}, {'\u1F2A', '\u0399', } },
        { {'\u1F93'}, {'\u1F2B', '\u0399', } },
        { {'\u1F94'}, {'\u1F2C', '\u0399', } },
        { {'\u1F95'}, {'\u1F2D', '\u0399', } },
        { {'\u1F96'}, {'\u1F2E', '\u0399', } },
        { {'\u1F97'}, {'\u1F2F', '\u0399', } },
        { {'\u1F98'}, {'\u1F28', '\u0399', } },
        { {'\u1F99'}, {'\u1F29', '\u0399', } },
        { {'\u1F9A'}, {'\u1F2A', '\u0399', } },
        { {'\u1F9B'}, {'\u1F2B', '\u0399', } },
        { {'\u1F9C'}, {'\u1F2C', '\u0399', } },
        { {'\u1F9D'}, {'\u1F2D', '\u0399', } },
        { {'\u1F9E'}, {'\u1F2E', '\u0399', } },
        { {'\u1F9F'}, {'\u1F2F', '\u0399', } },
        { {'\u1FA0'}, {'\u1F68', '\u0399', } },
        { {'\u1FA1'}, {'\u1F69', '\u0399', } },
        { {'\u1FA2'}, {'\u1F6A', '\u0399', } },
        { {'\u1FA3'}, {'\u1F6B', '\u0399', } },
        { {'\u1FA4'}, {'\u1F6C', '\u0399', } },
        { {'\u1FA5'}, {'\u1F6D', '\u0399', } },
        { {'\u1FA6'}, {'\u1F6E', '\u0399', } },
        { {'\u1FA7'}, {'\u1F6F', '\u0399', } },
        { {'\u1FA8'}, {'\u1F68', '\u0399', } },
        { {'\u1FA9'}, {'\u1F69', '\u0399', } },
        { {'\u1FAA'}, {'\u1F6A', '\u0399', } },
        { {'\u1FAB'}, {'\u1F6B', '\u0399', } },
        { {'\u1FAC'}, {'\u1F6C', '\u0399', } },
        { {'\u1FAD'}, {'\u1F6D', '\u0399', } },
        { {'\u1FAE'}, {'\u1F6E', '\u0399', } },
        { {'\u1FAF'}, {'\u1F6F', '\u0399', } },
        { {'\u1FB2'}, {'\u1FBA', '\u0399', } },
        { {'\u1FB3'}, {'\u0391', '\u0399', } },
        { {'\u1FB4'}, {'\u0386', '\u0399', } },
        { {'\u1FB6'}, {'\u0391', '\u0342', } },
        { {'\u1FB7'}, {'\u0391', '\u0342', '\u0399', } },
        { {'\u1FBC'}, {'\u0391', '\u0399', } },
        { {'\u1FC2'}, {'\u1FCA', '\u0399', } },
        { {'\u1FC3'}, {'\u0397', '\u0399', } },
        { {'\u1FC4'}, {'\u0389', '\u0399', } },
        { {'\u1FC6'}, {'\u0397', '\u0342', } },
        { {'\u1FC7'}, {'\u0397', '\u0342', '\u0399', } },
        { {'\u1FCC'}, {'\u0397', '\u0399', } },
        { {'\u1FD2'}, {'\u0399', '\u0308', '\u0300', } },
        { {'\u1FD3'}, {'\u0399', '\u0308', '\u0301', } },
        { {'\u1FD6'}, {'\u0399', '\u0342', } },
        { {'\u1FD7'}, {'\u0399', '\u0308', '\u0342', } },
        { {'\u1FE2'}, {'\u03A5', '\u0308', '\u0300', } },
        { {'\u1FE3'}, {'\u03A5', '\u0308', '\u0301', } },
        { {'\u1FE4'}, {'\u03A1', '\u0313', } },
        { {'\u1FE6'}, {'\u03A5', '\u0342', } },
        { {'\u1FE7'}, {'\u03A5', '\u0308', '\u0342', } },
        { {'\u1FF2'}, {'\u1FFA', '\u0399', } },
        { {'\u1FF3'}, {'\u03A9', '\u0399', } },
        { {'\u1FF4'}, {'\u038F', '\u0399', } },
        { {'\u1FF6'}, {'\u03A9', '\u0342', } },
        { {'\u1FF7'}, {'\u03A9', '\u0342', '\u0399', } },
        { {'\u1FFC'}, {'\u03A9', '\u0399', } },
        { {'\uFB00'}, {'\u0046', '\u0046', } },
        { {'\uFB01'}, {'\u0046', '\u0049', } },
        { {'\uFB02'}, {'\u0046', '\u004C', } },
        { {'\uFB03'}, {'\u0046', '\u0046', '\u0049', } },
        { {'\uFB04'}, {'\u0046', '\u0046', '\u004C', } },
        { {'\uFB05'}, {'\u0053', '\u0054', } },
        { {'\uFB06'}, {'\u0053', '\u0054', } },
        { {'\uFB13'}, {'\u0544', '\u0546', } },
        { {'\uFB14'}, {'\u0544', '\u0535', } },
        { {'\uFB15'}, {'\u0544', '\u053B', } },
        { {'\uFB16'}, {'\u054E', '\u0546', } },
        { {'\uFB17'}, {'\u0544', '\u053D', } },
    };
        { // THIS CODE WAS AUTOMATICALLY CREATED BY GenerateCharacter:
            char[] data = A_DATA.toCharArray();
            assert (data.length == (938 * 2));
            int i = 0, j = 0;
            while (i < (938 * 2)) {
                int entry = data[i++] << 16;
                A[j++] = entry | data[i++];
            }
        }

    }        
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\CharacterData01.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
// This file was generated AUTOMATICALLY from a template file Thu Jan 17 21:22:20 PST 2019
/*
 * Copyright (c) 2003, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/** The CharacterData class encapsulates the large tables once found in
 *  java.lang.Character. 
 */

class CharacterData01 extends CharacterData {
    /* The character properties are currently encoded into 32 bits in the following manner:
        1 bit   mirrored property
        4 bits  directionality property
        9 bits  signed offset used for converting case
        1 bit   if 1, adding the signed offset converts the character to lowercase
        1 bit   if 1, subtracting the signed offset converts the character to uppercase
        1 bit   if 1, this character has a titlecase equivalent (possibly itself)
        3 bits  0  may not be part of an identifier
                1  ignorable control; may continue a Unicode identifier or Java identifier
                2  may continue a Java identifier but not a Unicode identifier (unused)
                3  may continue a Unicode identifier or Java identifier
                4  is a Java whitespace character
                5  may start or continue a Java identifier;
                   may continue but not start a Unicode identifier (underscores)
                6  may start or continue a Java identifier but not a Unicode identifier ($)
                7  may start or continue a Unicode identifier or Java identifier
                Thus:
                   5, 6, 7 may start a Java identifier
                   1, 2, 3, 5, 6, 7 may continue a Java identifier
                   7 may start a Unicode identifier
                   1, 3, 5, 7 may continue a Unicode identifier
                   1 is ignorable within an identifier
                   4 is Java whitespace
        2 bits  0  this character has no numeric property
                1  adding the digit offset to the character code and then
                   masking with 0x1F will produce the desired numeric value
                2  this character has a "strange" numeric value
                3  a Java supradecimal digit: adding the digit offset to the
                   character code, then masking with 0x1F, then adding 10
                   will produce the desired numeric value
        5 bits  digit offset
        5 bits  character type

        The encoding of character properties is subject to change at any time.
     */

    int getProperties(int ch) {
        char offset = (char)ch;
        int props = A[(Y[(X[offset>>5]<<4)|((offset>>1)&0xF)]<<1)|(offset&0x1)];
        return props;
    }

    int getPropertiesEx(int ch) {
        char offset = (char)ch;
        int props = B[(Y[(X[offset>>5]<<4)|((offset>>1)&0xF)]<<1)|(offset&0x1)];
        return props;
    }

    int getType(int ch) {
        int props = getProperties(ch);
        return (props & 0x1F);
    }

    boolean isOtherLowercase(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0001) != 0;
    }

    boolean isOtherUppercase(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0002) != 0;
    }
 
    boolean isOtherAlphabetic(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0004) != 0;
    }

    boolean isIdeographic(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0010) != 0;
    }

    boolean isJavaIdentifierStart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) >= 0x00005000);
    }

    boolean isJavaIdentifierPart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00003000) != 0);
    }

    boolean isUnicodeIdentifierStart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00007000);
    }

    boolean isUnicodeIdentifierPart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00001000) != 0);
    }

    boolean isIdentifierIgnorable(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00001000);
    }

    int toLowerCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00020000) != 0) {
            int offset = val << 5 >> (5+18);
            mapChar = ch + offset;
        }
        return  mapChar;
    }

    int toUpperCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00010000) != 0) {
            int offset = val  << 5 >> (5+18);
            mapChar =  ch - offset;
        }
        return  mapChar;
    }

    int toTitleCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00008000) != 0) {
            // There is a titlecase equivalent.  Perform further checks:
            if ((val & 0x00010000) == 0) {
                // The character does not have an uppercase equivalent, so it must
                // already be uppercase; so add 1 to get the titlecase form.
                mapChar = ch + 1;
            }
            else if ((val & 0x00020000) == 0) {
                // The character does not have a lowercase equivalent, so it must
                // already be lowercase; so subtract 1 to get the titlecase form.
                mapChar = ch - 1;
            }
            // else {
            // The character has both an uppercase equivalent and a lowercase
            // equivalent, so it must itself be a titlecase form; return it.
            // return ch;
            //}
        }
        else if ((val & 0x00010000) != 0) {
            // This character has no titlecase equivalent but it does have an
            // uppercase equivalent, so use that (subtract the signed case offset).
            mapChar = toUpperCase(ch);
        }
        return  mapChar;
    }

    int digit(int ch, int radix) {
        int value = -1;
        if (radix >= Character.MIN_RADIX && radix <= Character.MAX_RADIX) {
            int val = getProperties(ch);
            int kind = val & 0x1F;
            if (kind == Character.DECIMAL_DIGIT_NUMBER) {
                value = ch + ((val & 0x3E0) >> 5) & 0x1F;
            }
            else if ((val & 0xC00) == 0x00000C00) {
                // Java supradecimal digit
                value = (ch + ((val & 0x3E0) >> 5) & 0x1F) + 10;
            }
        }
        return (value < radix) ? value : -1;
    }

    int getNumericValue(int ch) {
        int val = getProperties(ch);
        int retval = -1;

        switch (val & 0xC00) {
        default: // cannot occur
        case (0x00000000):         // not numeric
            retval = -1;
            break;
        case (0x00000400):              // simple numeric
            retval = ch + ((val & 0x3E0) >> 5) & 0x1F;
            break;
        case (0x00000800)      :       // "strange" numeric
            switch(ch) {
            case 0x10113: retval = 40; break;      // AEGEAN NUMBER FORTY
            case 0x10114: retval = 50; break;      // AEGEAN NUMBER FIFTY
            case 0x10115: retval = 60; break;      // AEGEAN NUMBER SIXTY
            case 0x10116: retval = 70; break;      // AEGEAN NUMBER SEVENTY
            case 0x10117: retval = 80; break;      // AEGEAN NUMBER EIGHTY
            case 0x10118: retval = 90; break;      // AEGEAN NUMBER NINETY
            case 0x10119: retval = 100; break;     // AEGEAN NUMBER ONE HUNDRED
            case 0x1011A: retval = 200; break;     // AEGEAN NUMBER TWO HUNDRED
            case 0x1011B: retval = 300; break;     // AEGEAN NUMBER THREE HUNDRED
            case 0x1011C: retval = 400; break;     // AEGEAN NUMBER FOUR HUNDRED
            case 0x1011D: retval = 500; break;     // AEGEAN NUMBER FIVE HUNDRED
            case 0x1011E: retval = 600; break;     // AEGEAN NUMBER SIX HUNDRED
            case 0x1011F: retval = 700; break;     // AEGEAN NUMBER SEVEN HUNDRED
            case 0x10120: retval = 800; break;     // AEGEAN NUMBER EIGHT HUNDRED
            case 0x10121: retval = 900; break;     // AEGEAN NUMBER NINE HUNDRED
            case 0x10122: retval = 1000; break;    // AEGEAN NUMBER ONE THOUSAND
            case 0x10123: retval = 2000; break;    // AEGEAN NUMBER TWO THOUSAND
            case 0x10124: retval = 3000; break;    // AEGEAN NUMBER THREE THOUSAND
            case 0x10125: retval = 4000; break;    // AEGEAN NUMBER FOUR THOUSAND
            case 0x10126: retval = 5000; break;    // AEGEAN NUMBER FIVE THOUSAND
            case 0x10127: retval = 6000; break;    // AEGEAN NUMBER SIX THOUSAND
            case 0x10128: retval = 7000; break;    // AEGEAN NUMBER SEVEN THOUSAND
            case 0x10129: retval = 8000; break;    // AEGEAN NUMBER EIGHT THOUSAND
            case 0x1012A: retval = 9000; break;    // AEGEAN NUMBER NINE THOUSAND
            case 0x1012B: retval = 10000; break;   // AEGEAN NUMBER TEN THOUSAND
            case 0x1012C: retval = 20000; break;   // AEGEAN NUMBER TWENTY THOUSAND
            case 0x1012D: retval = 30000; break;   // AEGEAN NUMBER THIRTY THOUSAND
            case 0x1012E: retval = 40000; break;   // AEGEAN NUMBER FORTY THOUSAND
            case 0x1012F: retval = 50000; break;   // AEGEAN NUMBER FIFTY THOUSAND
            case 0x10130: retval = 60000; break;   // AEGEAN NUMBER SIXTY THOUSAND
            case 0x10131: retval = 70000; break;   // AEGEAN NUMBER SEVENTY THOUSAND
            case 0x10132: retval = 80000; break;   // AEGEAN NUMBER EIGHTY THOUSAND
            case 0x10133: retval = 90000; break;   // AEGEAN NUMBER NINETY THOUSAND
            case 0x10144: retval = 50; break;      // GREEK ACROPHONIC ATTIC FIFTY
            case 0x10145: retval = 500; break;     // GREEK ACROPHONIC ATTIC FIVE HUNDRED
            case 0x10146: retval = 5000; break;    // GREEK ACROPHONIC ATTIC FIVE THOUSAND
            case 0x10147: retval = 50000; break;   // GREEK ACROPHONIC ATTIC FIFTY THOUSAND
            case 0x1014A: retval = 50; break;      // GREEK ACROPHONIC ATTIC FIFTY TALENTS
            case 0x1014B: retval = 100; break;     // GREEK ACROPHONIC ATTIC ONE HUNDRED TALENTS
            case 0x1014C: retval = 500; break;     // GREEK ACROPHONIC ATTIC FIVE HUNDRED TALENTS
            case 0x1014D: retval = 1000; break;    // GREEK ACROPHONIC ATTIC ONE THOUSAND TALENTS
            case 0x1014E: retval = 5000; break;    // GREEK ACROPHONIC ATTIC FIVE THOUSAND TALENTS
            case 0x10151: retval = 50; break;      // GREEK ACROPHONIC ATTIC FIFTY STATERS
            case 0x10152: retval = 100; break;     // GREEK ACROPHONIC ATTIC ONE HUNDRED STATERS
            case 0x10153: retval = 500; break;     // GREEK ACROPHONIC ATTIC FIVE HUNDRED STATERS
            case 0x10154: retval = 1000; break;    // GREEK ACROPHONIC ATTIC ONE THOUSAND STATERS
            case 0x10155: retval = 10000; break;   // GREEK ACROPHONIC ATTIC TEN THOUSAND STATERS
            case 0x10156: retval = 50000; break;   // GREEK ACROPHONIC ATTIC FIFTY THOUSAND STATERS
            case 0x10166: retval = 50; break;      // GREEK ACROPHONIC TROEZENIAN FIFTY
            case 0x10167: retval = 50; break;      // GREEK ACROPHONIC TROEZENIAN FIFTY ALTERNATE FORM
            case 0x10168: retval = 50; break;      // GREEK ACROPHONIC HERMIONIAN FIFTY
            case 0x10169: retval = 50; break;      // GREEK ACROPHONIC THESPIAN FIFTY
            case 0x1016A: retval = 100; break;     // GREEK ACROPHONIC THESPIAN ONE HUNDRED
            case 0x1016B: retval = 300; break;     // GREEK ACROPHONIC THESPIAN THREE HUNDRED
            case 0x1016C: retval = 500; break;     // GREEK ACROPHONIC EPIDAUREAN FIVE HUNDRED
            case 0x1016D: retval = 500; break;     // GREEK ACROPHONIC TROEZENIAN FIVE HUNDRED
            case 0x1016E: retval = 500; break;     // GREEK ACROPHONIC THESPIAN FIVE HUNDRED
            case 0x1016F: retval = 500; break;     // GREEK ACROPHONIC CARYSTIAN FIVE HUNDRED
            case 0x10170: retval = 500; break;     // GREEK ACROPHONIC NAXIAN FIVE HUNDRED
            case 0x10171: retval = 1000; break;    // GREEK ACROPHONIC THESPIAN ONE THOUSAND
            case 0x10172: retval = 5000; break;    // GREEK ACROPHONIC THESPIAN FIVE THOUSAND
            case 0x10174: retval = 50; break;      // GREEK ACROPHONIC STRATIAN FIFTY MNAS
            case 0x102ED: retval = 40; break;      // COPTIC EPACT NUMBER FORTY
            case 0x102EE: retval = 50; break;      // COPTIC EPACT NUMBER FIFTY
            case 0x102EF: retval = 60; break;      // COPTIC EPACT NUMBER SIXTY
            case 0x102F0: retval = 70; break;      // COPTIC EPACT NUMBER SEVENTY
            case 0x102F1: retval = 80; break;      // COPTIC EPACT NUMBER EIGHTY
            case 0x102F2: retval = 90; break;      // COPTIC EPACT NUMBER NINETY
            case 0x102F3: retval = 100; break;     // COPTIC EPACT NUMBER ONE HUNDRED
            case 0x102F4: retval = 200; break;     // COPTIC EPACT NUMBER TWO HUNDRED
            case 0x102F5: retval = 300; break;     // COPTIC EPACT NUMBER THREE HUNDRED
            case 0x102F6: retval = 400; break;     // COPTIC EPACT NUMBER FOUR HUNDRED
            case 0x102F7: retval = 500; break;     // COPTIC EPACT NUMBER FIVE HUNDRED
            case 0x102F8: retval = 600; break;     // COPTIC EPACT NUMBER SIX HUNDRED
            case 0x102F9: retval = 700; break;     // COPTIC EPACT NUMBER SEVEN HUNDRED
            case 0x102FA: retval = 800; break;     // COPTIC EPACT NUMBER EIGHT HUNDRED
            case 0x102FB: retval = 900; break;     // COPTIC EPACT NUMBER NINE HUNDRED
            case 0x10323: retval = 50; break;      // OLD ITALIC NUMERAL FIFTY
            case 0x10341: retval = 90; break;      // GOTHIC LETTER NINETY
            case 0x1034A: retval = 900; break;     // GOTHIC LETTER NINE HUNDRED
            case 0x103D5: retval = 100; break;     // OLD PERSIAN NUMBER HUNDRED
            case 0x1085D: retval = 100; break;     // IMPERIAL ARAMAIC NUMBER ONE HUNDRED
            case 0x1085E: retval = 1000; break;    // IMPERIAL ARAMAIC NUMBER ONE THOUSAND
            case 0x1085F: retval = 10000; break;   // IMPERIAL ARAMAIC NUMBER TEN THOUSAND
            case 0x108AF: retval = 100; break;     // NABATAEAN NUMBER ONE HUNDRED
            case 0x108FF: retval = 100; break;     // HATRAN NUMBER ONE HUNDRED
            case 0x10919: retval = 100; break;     // PHOENICIAN NUMBER ONE HUNDRED
            case 0x109CC: retval = 40; break;      // MEROITIC CURSIVE NUMBER FORTY
            case 0x109CD: retval = 50; break;      // MEROITIC CURSIVE NUMBER FIFTY
            case 0x109CE: retval = 60; break;      // MEROITIC CURSIVE NUMBER SIXTY
            case 0x109CF: retval = 70; break;      // MEROITIC CURSIVE NUMBER SEVENTY
            case 0x109D2: retval = 100; break;     // MEROITIC CURSIVE NUMBER ONE HUNDRED
            case 0x109D3: retval = 200; break;     // MEROITIC CURSIVE NUMBER TWO HUNDRED
            case 0x109D4: retval = 300; break;     // MEROITIC CURSIVE NUMBER THREE HUNDRED
            case 0x109D5: retval = 400; break;     // MEROITIC CURSIVE NUMBER FOUR HUNDRED
            case 0x109D6: retval = 500; break;     // MEROITIC CURSIVE NUMBER FIVE HUNDRED
            case 0x109D7: retval = 600; break;     // MEROITIC CURSIVE NUMBER SIX HUNDRED
            case 0x109D8: retval = 700; break;     // MEROITIC CURSIVE NUMBER SEVEN HUNDRED
            case 0x109D9: retval = 800; break;     // MEROITIC CURSIVE NUMBER EIGHT HUNDRED
            case 0x109DA: retval = 900; break;     // MEROITIC CURSIVE NUMBER NINE HUNDRED
            case 0x109DB: retval = 1000; break;    // MEROITIC CURSIVE NUMBER ONE THOUSAND
            case 0x109DC: retval = 2000; break;    // MEROITIC CURSIVE NUMBER TWO THOUSAND
            case 0x109DD: retval = 3000; break;    // MEROITIC CURSIVE NUMBER THREE THOUSAND
            case 0x109DE: retval = 4000; break;    // MEROITIC CURSIVE NUMBER FOUR THOUSAND
            case 0x109DF: retval = 5000; break;    // MEROITIC CURSIVE NUMBER FIVE THOUSAND
            case 0x109E0: retval = 6000; break;    // MEROITIC CURSIVE NUMBER SIX THOUSAND
            case 0x109E1: retval = 7000; break;    // MEROITIC CURSIVE NUMBER SEVEN THOUSAND
            case 0x109E2: retval = 8000; break;    // MEROITIC CURSIVE NUMBER EIGHT THOUSAND
            case 0x109E3: retval = 9000; break;    // MEROITIC CURSIVE NUMBER NINE THOUSAND
            case 0x109E4: retval = 10000; break;   // MEROITIC CURSIVE NUMBER TEN THOUSAND
            case 0x109E5: retval = 20000; break;   // MEROITIC CURSIVE NUMBER TWENTY THOUSAND
            case 0x109E6: retval = 30000; break;   // MEROITIC CURSIVE NUMBER THIRTY THOUSAND
            case 0x109E7: retval = 40000; break;   // MEROITIC CURSIVE NUMBER FORTY THOUSAND
            case 0x109E8: retval = 50000; break;   // MEROITIC CURSIVE NUMBER FIFTY THOUSAND
            case 0x109E9: retval = 60000; break;   // MEROITIC CURSIVE NUMBER SIXTY THOUSAND
            case 0x109EA: retval = 70000; break;   // MEROITIC CURSIVE NUMBER SEVENTY THOUSAND
            case 0x109EB: retval = 80000; break;   // MEROITIC CURSIVE NUMBER EIGHTY THOUSAND
            case 0x109EC: retval = 90000; break;   // MEROITIC CURSIVE NUMBER NINETY THOUSAND
            case 0x109ED: retval = 100000; break;  // MEROITIC CURSIVE NUMBER ONE HUNDRED THOUSAND
            case 0x109EE: retval = 200000; break;  // MEROITIC CURSIVE NUMBER TWO HUNDRED THOUSAND
            case 0x109EF: retval = 300000; break;  // MEROITIC CURSIVE NUMBER THREE HUNDRED THOUSAND
            case 0x109F0: retval = 400000; break;  // MEROITIC CURSIVE NUMBER FOUR HUNDRED THOUSAND
            case 0x109F1: retval = 500000; break;  // MEROITIC CURSIVE NUMBER FIVE HUNDRED THOUSAND
            case 0x109F2: retval = 600000; break;  // MEROITIC CURSIVE NUMBER SIX HUNDRED THOUSAND
            case 0x109F3: retval = 700000; break;  // MEROITIC CURSIVE NUMBER SEVEN HUNDRED THOUSAND
            case 0x109F4: retval = 800000; break;  // MEROITIC CURSIVE NUMBER EIGHT HUNDRED THOUSAND
            case 0x109F5: retval = 900000; break;  // MEROITIC CURSIVE NUMBER NINE HUNDRED THOUSAND
            case 0x10A46: retval = 100; break;     // KHAROSHTHI NUMBER ONE HUNDRED
            case 0x10A47: retval = 1000; break;    // KHAROSHTHI NUMBER ONE THOUSAND
            case 0x10A7E: retval = 50; break;      // OLD SOUTH ARABIAN NUMBER FIFTY
            case 0x10AEF: retval = 100; break;     // MANICHAEAN NUMBER ONE HUNDRED
            case 0x10B5E: retval = 100; break;     // INSCRIPTIONAL PARTHIAN NUMBER ONE HUNDRED
            case 0x10B5F: retval = 1000; break;    // INSCRIPTIONAL PARTHIAN NUMBER ONE THOUSAND
            case 0x10B7E: retval = 100; break;     // INSCRIPTIONAL PAHLAVI NUMBER ONE HUNDRED
            case 0x10B7F: retval = 1000; break;    // INSCRIPTIONAL PAHLAVI NUMBER ONE THOUSAND
            case 0x10BAF: retval = 100; break;     // PSALTER PAHLAVI NUMBER ONE HUNDRED
            case 0x10CFD: retval = 50; break;      // OLD HUNGARIAN NUMBER FIFTY
            case 0x10CFE: retval = 100; break;     // OLD HUNGARIAN NUMBER ONE HUNDRED
            case 0x10CFF: retval = 1000; break;    // OLD HUNGARIAN NUMBER ONE THOUSAND
            case 0x10E6C: retval = 40; break;      // RUMI NUMBER FORTY
            case 0x10E6D: retval = 50; break;      // RUMI NUMBER FIFTY
            case 0x10E6E: retval = 60; break;      // RUMI NUMBER SIXTY
            case 0x10E6F: retval = 70; break;      // RUMI NUMBER SEVENTY
            case 0x10E70: retval = 80; break;      // RUMI NUMBER EIGHTY
            case 0x10E71: retval = 90; break;      // RUMI NUMBER NINETY
            case 0x10E72: retval = 100; break;     // RUMI NUMBER ONE HUNDRED
            case 0x10E73: retval = 200; break;     // RUMI NUMBER TWO HUNDRED
            case 0x10E74: retval = 300; break;     // RUMI NUMBER THREE HUNDRED
            case 0x10E75: retval = 400; break;     // RUMI NUMBER FOUR HUNDRED
            case 0x10E76: retval = 500; break;     // RUMI NUMBER FIVE HUNDRED
            case 0x10E77: retval = 600; break;     // RUMI NUMBER SIX HUNDRED
            case 0x10E78: retval = 700; break;     // RUMI NUMBER SEVEN HUNDRED
            case 0x10E79: retval = 800; break;     // RUMI NUMBER EIGHT HUNDRED
            case 0x10E7A: retval = 900; break;     // RUMI NUMBER NINE HUNDRED
            case 0x1105E: retval = 40; break;      // BRAHMI NUMBER FORTY
            case 0x1105F: retval = 50; break;      // BRAHMI NUMBER FIFTY
            case 0x11060: retval = 60; break;      // BRAHMI NUMBER SIXTY
            case 0x11061: retval = 70; break;      // BRAHMI NUMBER SEVENTY
            case 0x11062: retval = 80; break;      // BRAHMI NUMBER EIGHTY
            case 0x11063: retval = 90; break;      // BRAHMI NUMBER NINETY
            case 0x11064: retval = 100; break;     // BRAHMI NUMBER ONE HUNDRED
            case 0x11065: retval = 1000; break;    // BRAHMI NUMBER ONE THOUSAND
            case 0x11C66: retval = 40; break;      // BHAIKSUKI NUMBER FORTY
            case 0x11C67: retval = 50; break;      // BHAIKSUKI NUMBER FIFTY
            case 0x11C68: retval = 60; break;      // BHAIKSUKI NUMBER SIXTY
            case 0x11C69: retval = 70; break;      // BHAIKSUKI NUMBER SEVENTY
            case 0x11C6A: retval = 80; break;      // BHAIKSUKI NUMBER EIGHTY
            case 0x11C6B: retval = 90; break;      // BHAIKSUKI NUMBER NINETY
            case 0x11C6C: retval = 100; break;     // BHAIKSUKI HUNDREDS UNIT MARK
            case 0x111ED: retval = 40; break;      // SINHALA ARCHAIC NUMBER FORTY
            case 0x111EE: retval = 50; break;      // SINHALA ARCHAIC NUMBER FIFTY
            case 0x111EF: retval = 60; break;      // SINHALA ARCHAIC NUMBER SIXTY
            case 0x111F0: retval = 70; break;      // SINHALA ARCHAIC NUMBER SEVENTY
            case 0x111F1: retval = 80; break;      // SINHALA ARCHAIC NUMBER EIGHTY
            case 0x111F2: retval = 90; break;      // SINHALA ARCHAIC NUMBER NINETY
            case 0x111F3: retval = 100; break;     // SINHALA ARCHAIC NUMBER ONE HUNDRED
            case 0x111F4: retval = 1000; break;    // SINHALA ARCHAIC NUMBER ONE THOUSAND
            case 0x118ED: retval = 40; break;      // WARANG CITI NUMBER FORTY
            case 0x118EE: retval = 50; break;      // WARANG CITI NUMBER FIFTY
            case 0x118EF: retval = 60; break;      // WARANG CITI NUMBER SIXTY
            case 0x118F0: retval = 70; break;      // WARANG CITI NUMBER SEVENTY
            case 0x118F1: retval = 80; break;      // WARANG CITI NUMBER EIGHTY
            case 0x118F2: retval = 90; break;      // WARANG CITI NUMBER NINETY
            case 0x12432: retval = 216000; break;  // CUNEIFORM NUMERIC SIGN SHAR2 TIMES GAL PLUS DISH
            case 0x12433: retval = 432000; break;  // CUNEIFORM NUMERIC SIGN SHAR2 TIMES GAL PLUS MIN
            case 0x12467: retval = 40; break;      // CUNEIFORM NUMERIC SIGN ELAMITE FORTY
            case 0x12468: retval = 50; break;      // CUNEIFORM NUMERIC SIGN ELAMITE FIFTY
            case 0x16B5C: retval = 100; break;     // PAHAWH HMONG NUMBER HUNDREDS
            case 0x16B5D: retval = 10000; break;   // PAHAWH HMONG NUMBER TEN THOUSANDS
            case 0x16B5E: retval = 1000000; break; // PAHAWH HMONG NUMBER MILLIONS
            case 0x16B5F: retval = 100000000; break;// PAHAWH HMONG NUMBER HUNDRED MILLIONS
            case 0x1D36C: retval = 40; break;      // COUNTING ROD TENS DIGIT FOUR
            case 0x1D36D: retval = 50; break;      // COUNTING ROD TENS DIGIT FIVE
            case 0x1D36E: retval = 60; break;      // COUNTING ROD TENS DIGIT SIX
            case 0x1D36F: retval = 70; break;      // COUNTING ROD TENS DIGIT SEVEN
            case 0x1D370: retval = 80; break;      // COUNTING ROD TENS DIGIT EIGHT
            case 0x1D371: retval = 90; break;      // COUNTING ROD TENS DIGIT NINE
            default: retval = -2; break;
            }
            
            break;
        case (0x00000C00):           // Java supradecimal
            retval = (ch + ((val & 0x3E0) >> 5) & 0x1F) + 10;
            break;
        }
        return retval;
    }

    boolean isWhitespace(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00004000);
    }

    byte getDirectionality(int ch) {
        int val = getProperties(ch);
        byte directionality = (byte)((val & 0x78000000) >> 27);
        if (directionality == 0xF ) {
            directionality = Character.DIRECTIONALITY_UNDEFINED;
        }
        return directionality;
    }

    boolean isMirrored(int ch) {
        int props = getProperties(ch);
        return ((props & 0x80000000) != 0);
    }

    static final CharacterData instance = new CharacterData01();
    private CharacterData01() {};

    // The following tables and code generated using:
  // java GenerateCharacter -string -plane 1 -template t:/workspace/open/make/data/characterdata/CharacterData01.java.template -spec t:/workspace/open/make/data/unicodedata/UnicodeData.txt -specialcasing t:/workspace/open/make/data/unicodedata/SpecialCasing.txt -proplist t:/workspace/open/make/data/unicodedata/PropList.txt -o t:/workspace/build/windows-x64/support/gensrc/java.base/java/lang/CharacterData01.java -usecharforbyte 11 4 1
  // The X table has 2048 entries for a total of 4096 bytes.

  static final char X[] = (
    "\000\001\002\003\004\004\004\005\006\007\010\011\012\013\014\015\003\003\003"+
    "\003\016\004\017\020\004\021\022\023\024\004\025\003\026\027\030\004\031\032"+
    "\033\034\004\035\004\036\003\003\003\003\004\004\004\004\004\004\004\004\004"+
    "\037\040\041\003\003\003\003\042\043\044\045\046\047\003\050\051\052\003\003"+
    "\053\054\055\056\057\060\061\062\063\003\064\065\053\066\067\070\071\072\003"+
    "\003\053\053\073\003\074\075\076\077\003\003\003\003\003\003\003\003\003\003"+
    "\003\100\003\003\003\003\003\003\003\003\003\003\003\003\101\102\103\104\105"+
    "\106\107\110\111\112\113\114\115\116\117\120\121\122\003\003\123\124\125\126"+
    "\127\130\131\132\003\003\003\003\004\133\134\003\004\135\136\003\003\003\003"+
    "\003\004\137\140\003\004\141\142\143\004\144\145\003\146\147\003\003\003\003"+
    "\003\003\003\003\003\003\003\150\151\152\003\003\003\003\003\003\003\003\153"+
    "\154\155\004\156\157\004\160\003\003\003\003\003\003\003\003\161\162\163\164"+
    "\165\166\003\003\167\170\171\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\004\004\004\004\004\004\004\004\004\004"+
    "\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\172"+
    "\003\003\003\173\174\175\176\004\004\004\004\004\004\177\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004"+
    "\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\200"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\004\004\004\004\004\004\004"+
    "\004\004\004\004\004\004\004\004\004\004\004\201\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\004\004\004\004"+
    "\004\004\004\004\004\004\004\004\004\004\004\004\004\160\202\203\003\003\204"+
    "\205\004\206\207\210\211\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\004\004\212\213\214"+
    "\003\003\215\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\217\216\216\216\216\216\216\216\216\216\216\216\216\216\216"+
    "\216\216\216\216\216\216\216\216\216\220\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\004\004\004\004\004\004\004\004\202\003\003\221\216\216\216\216\216"+
    "\216\216\216\216\216\216\222\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\004\004\004\223\224\225\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\226\226\226\226\226\226\226\227"+
    "\226\230\226\231\232\233\226\234\235\235\236\003\003\003\003\003\235\235\237"+
    "\240\003\003\003\003\241\242\243\244\245\246\247\250\251\252\253\254\255\241"+
    "\242\256\244\257\260\261\250\262\263\264\265\266\267\270\271\272\273\274\226"+
    "\226\226\226\226\226\226\226\226\226\226\226\226\226\226\226\275\276\275\277"+
    "\300\301\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\302\303\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\053\053\053\053\053\053"+
    "\304\003\305\306\307\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\310\311\312\313\314\315\003\316\003\003\003\003\003\003\003"+
    "\003\235\317\235\235\320\321\322\323\324\325\326\327\330\331\003\332\333\334"+
    "\335\336\003\003\003\003\235\235\235\235\235\235\235\337\235\235\235\235\235"+
    "\235\235\235\235\235\235\235\235\235\235\235\235\235\235\235\235\235\340\341"+
    "\235\235\235\320\235\235\340\003\317\235\342\235\343\344\003\003\317\345\346"+
    "\347\350\003\351\352\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003").toCharArray();

  // The Y table has 3760 entries for a total of 7520 bytes.

  static final char Y[] = (
    "\000\000\000\000\000\000\001\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\002\000\000\000\000\000\000\000\000\000\002\000\001\000\000\000\000\000\000"+
    "\000\003\000\000\000\000\000\000\000\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\002\003"+
    "\003\004\005\003\006\007\007\007\007\010\011\012\012\012\012\012\012\012\012"+
    "\012\012\012\012\012\012\012\012\003\013\014\014\014\014\015\016\015\015\017"+
    "\015\015\020\021\015\015\022\023\024\025\026\027\030\031\015\015\015\015\015"+
    "\015\032\033\034\035\036\036\036\036\036\036\036\036\037\040\041\036\036\036"+
    "\036\036\036\003\003\042\003\003\003\003\003\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\014\014\014\014\014\014\014\014\014"+
    "\014\014\014\014\014\014\014\014\014\014\014\014\014\043\003\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\002\003\000\000\000\000\000\000\000"+
    "\000\002\003\003\003\003\003\003\003\044\045\045\045\045\046\047\050\050\050"+
    "\050\050\050\050\003\003\051\052\003\003\003\003\001\000\000\000\000\000\000"+
    "\000\000\000\053\000\000\000\000\054\003\003\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\055\055\056\003\003\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\057\000\000\003\003\000\000"+
    "\000\000\060\061\062\003\003\003\003\003\063\063\063\063\063\063\063\063\063"+
    "\063\063\063\063\063\063\063\063\063\063\063\064\064\064\064\064\064\064\064"+
    "\064\064\064\064\064\064\064\064\064\064\064\064\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\003\065\065"+
    "\065\065\065\003\003\003\063\063\063\063\063\063\063\063\063\063\063\063\063"+
    "\063\063\063\063\063\003\003\064\064\064\064\064\064\064\064\064\064\064\064"+
    "\064\064\064\064\064\064\003\003\000\000\000\000\003\003\003\003\000\000\000"+
    "\000\000\000\000\000\000\000\003\003\003\003\003\057\003\003\003\003\003\003"+
    "\003\003\000\000\000\000\000\000\000\000\000\000\000\002\003\003\003\003\000"+
    "\000\000\000\000\000\000\000\000\000\000\003\003\003\003\003\000\000\000\000"+
    "\003\003\003\003\003\003\003\003\003\003\003\003\066\066\066\003\067\066\066"+
    "\066\066\066\066\066\066\066\066\066\066\066\066\066\066\066\066\066\066\066"+
    "\066\070\067\003\067\070\066\066\066\066\066\066\066\066\066\066\066\071\072"+
    "\073\074\075\066\066\066\066\066\066\066\066\066\066\066\076\077\100\100\101"+
    "\066\066\066\066\066\066\066\066\066\066\066\066\066\066\066\067\003\003\003"+
    "\102\103\104\105\106\003\003\003\003\003\003\003\003\066\066\066\066\066\066"+
    "\066\066\066\067\066\003\003\107\110\111\066\066\066\066\066\066\066\066\066"+
    "\066\066\112\113\100\003\114\066\066\066\066\066\066\066\066\066\066\066\066"+
    "\066\003\003\071\066\066\066\066\066\066\066\066\066\066\066\066\066\066\066"+
    "\066\066\066\066\066\066\066\066\066\066\066\066\066\003\003\075\066\115\115"+
    "\115\115\115\116\075\075\003\075\075\075\075\075\075\075\075\075\075\075\075"+
    "\075\075\075\075\075\075\075\075\075\075\075\117\055\120\056\003\003\055\055"+
    "\066\066\070\066\070\066\066\066\066\066\066\066\066\066\066\066\066\066\003"+
    "\003\121\122\003\123\115\115\124\075\003\003\003\003\125\125\125\125\126\003"+
    "\003\003\066\066\066\066\066\066\066\066\066\066\066\066\066\066\127\130\066"+
    "\066\066\066\066\066\066\066\066\066\066\066\066\066\127\101\066\066\066\066"+
    "\131\066\066\066\066\066\066\066\066\066\066\066\066\066\132\122\003\133\105"+
    "\106\125\125\125\126\003\003\003\003\066\066\066\066\066\066\066\066\066\066"+
    "\066\003\114\134\134\134\066\066\066\066\066\066\066\066\066\066\066\003\072"+
    "\072\135\075\066\066\066\066\066\066\066\066\066\067\003\003\072\072\135\075"+
    "\066\066\066\066\066\066\066\066\066\003\003\003\071\125\126\003\003\003\003"+
    "\003\136\137\140\106\003\003\003\003\003\003\003\003\066\066\066\066\067\003"+
    "\003\003\003\003\003\003\003\003\003\003\141\141\141\141\141\141\141\141\141"+
    "\141\141\141\141\141\141\141\141\141\141\141\141\141\141\141\141\142\003\003"+
    "\003\003\003\003\143\143\143\143\143\143\143\143\143\143\143\143\143\143\143"+
    "\143\143\143\143\143\143\143\143\143\143\144\003\003\003\145\146\075\147\147"+
    "\147\147\147\150\151\151\151\151\151\151\151\151\151\152\153\154\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\055\055\055\055\055\055\055\155\156\156\156\003\003\157\157"+
    "\157\157\157\160\034\034\034\034\161\161\161\161\161\003\003\003\003\003\003"+
    "\003\123\121\154\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\162\153\055\163\164\155\165\156\156\003\003\003"+
    "\003\003\003\003\000\000\000\000\000\000\000\000\000\000\000\000\002\003\003"+
    "\003\166\166\166\166\166\003\003\003\055\167\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\170\055\055\153\055\055\171\122\172\172"+
    "\172\172\172\156\156\003\003\003\003\003\003\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\173\156\002\003\003\003\003\055\154\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\174\162\055\055\055\055\163\175\000\176\156\156\121\155\003\166"+
    "\166\166\166\166\176\176\156\177\200\200\200\200\201\202\012\012\012\203\003"+
    "\003\003\003\003\000\000\000\000\000\000\000\000\000\001\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\162\153\055\162\204\205\156\156\156\056\000\000"+
    "\000\002\002\000\000\001\000\000\000\000\000\000\000\001\000\000\000\000\176"+
    "\003\003\003\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\170\162\153\055\055\171\122\003\003\166\166\166"+
    "\166\166\003\003\003\055\162\001\000\000\000\002\001\002\001\000\000\000\000"+
    "\000\000\000\000\000\000\002\000\000\000\002\000\001\000\000\003\206\162\163"+
    "\162\207\210\207\210\211\003\002\003\003\210\003\003\001\000\000\162\003\121"+
    "\121\121\122\003\121\121\122\003\003\003\003\003\000\000\000\000\000\000\000"+
    "\000\000\000\174\162\055\055\055\055\162\205\163\206\000\176\156\156\166\166"+
    "\166\166\166\057\057\003\000\000\000\000\000\000\000\000\162\153\055\055\163"+
    "\163\162\153\163\121\000\212\003\003\003\003\166\166\166\166\166\003\003\003"+
    "\000\000\000\000\000\000\000\174\162\055\055\003\162\162\055\164\155\156\156"+
    "\156\156\156\156\156\156\156\156\156\000\000\055\003\000\000\000\000\000\000"+
    "\000\000\162\153\055\055\055\163\153\164\213\156\002\003\003\003\003\003\166"+
    "\166\166\166\166\003\003\003\134\134\134\134\134\134\214\003\003\003\003\003"+
    "\003\003\003\003\000\000\000\000\000\170\153\162\055\055\055\215\003\003\003"+
    "\003\065\065\065\065\065\003\003\003\003\003\003\003\003\003\003\003\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\003\120\055\162\055\055\153\055"+
    "\171\003\003\166\166\166\166\166\216\156\217\220\220\220\220\220\220\220\220"+
    "\220\220\220\220\220\220\220\220\221\221\221\221\221\221\221\221\221\221\221"+
    "\221\221\221\221\221\065\065\065\065\065\201\202\012\012\203\003\003\003\003"+
    "\003\001\170\055\055\163\153\167\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\173\205\055\163\170\055\213\156\156\156\222"+
    "\003\003\003\003\170\055\055\163\153\055\000\000\000\000\003\000\000\055\055"+
    "\055\055\055\055\163\121\156\005\156\156\005\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\000\000\000\000\000\000\000\000\000\000\000\000\002"+
    "\003\003\003\000\000\000\000\002\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\174\055\055\055\056\055\055\055\223\176\156\156"+
    "\003\003\003\003\003\166\166\166\166\166\224\224\224\224\224\216\012\012\012"+
    "\203\003\156\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\003"+
    "\055\055\055\055\055\055\055\055\055\055\055\210\055\055\055\163\055\153\056"+
    "\003\003\003\003\000\000\000\002\000\001\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\170\055\055\056\003\056\055\120\055\205"+
    "\121\170\003\003\003\003\166\166\166\166\166\003\003\003\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\003\003\003\225\225\225\225\226\226\226\227"+
    "\230\230\231\232\232\232\232\233\233\234\235\236\236\236\230\237\240\241\242"+
    "\243\232\244\245\246\247\250\251\252\253\254\254\255\256\257\260\232\261\241"+
    "\241\241\241\241\241\241\262\226\226\263\156\156\005\003\003\003\003\003\000"+
    "\000\003\003\003\003\003\003\003\003\003\003\003\003\003\003\000\000\000\000"+
    "\000\000\000\002\003\003\003\003\003\003\003\003\000\000\000\002\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\002\065\065\065\065\065\003\003\156\003\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\003\121\121\155\003\003\003\003\003\000\000\000"+
    "\000\000\000\000\000\055\055\055\213\156\156\014\014\264\264\217\003\003\003"+
    "\003\003\166\166\166\166\166\265\012\012\012\001\000\000\000\000\000\000\000"+
    "\000\000\000\003\003\001\000\000\000\000\000\000\000\000\000\003\003\003\003"+
    "\003\003\003\003\000\000\002\003\003\003\003\003\174\162\162\162\162\162\162"+
    "\162\162\162\162\162\162\162\162\162\162\162\162\162\162\162\162\207\003\003"+
    "\003\003\003\003\003\123\121\266\264\264\264\264\264\264\264\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\003\267\267\267\267\267\267\267\267"+
    "\267\267\267\267\267\267\267\267\267\267\267\267\267\267\270\003\003\003\003"+
    "\003\003\003\003\003\267\267\267\267\267\267\267\267\267\270\003\003\003\003"+
    "\003\003\003\003\003\003\003\003\003\003\267\267\267\267\267\267\267\267\267"+
    "\267\267\267\267\267\267\267\267\267\267\267\267\267\003\003\000\000\000\000"+
    "\000\002\003\003\000\000\000\000\000\000\002\003\000\000\000\000\002\003\003"+
    "\003\000\000\000\000\000\003\043\213\271\271\003\003\003\003\003\003\003\003"+
    "\003\003\003\003\003\003\014\014\014\014\014\014\014\014\014\014\014\014\014"+
    "\014\014\014\014\014\014\014\014\014\014\014\014\014\014\003\003\003\003\003"+
    "\014\014\014\041\013\014\014\014\014\014\014\014\014\014\014\014\014\014\272"+
    "\215\121\014\272\273\273\274\271\271\271\275\121\121\121\276\043\121\121\121"+
    "\014\014\014\014\014\014\014\014\014\014\014\014\014\014\014\121\121\014\014"+
    "\014\014\014\014\014\014\014\014\014\014\014\041\003\003\003\003\003\003\003"+
    "\003\003\003\003\036\036\036\036\036\036\036\036\036\036\036\036\036\036\036"+
    "\036\036\121\277\003\003\003\003\003\003\003\003\003\003\003\003\003\036\036"+
    "\036\036\036\036\036\036\036\036\036\042\003\003\003\003\300\300\300\300\300"+
    "\301\012\012\012\003\003\003\003\003\003\003\302\302\302\302\302\302\302\302"+
    "\302\302\302\302\302\303\303\303\303\303\303\303\303\303\303\303\303\303\302"+
    "\302\302\302\302\302\302\302\302\302\302\302\302\303\303\303\304\303\303\303"+
    "\303\303\303\303\303\303\302\302\302\302\302\302\302\302\302\302\302\302\302"+
    "\303\303\303\303\303\303\303\303\303\303\303\303\303\305\302\003\305\306\305"+
    "\306\302\305\302\302\302\302\303\303\307\307\303\303\303\307\303\303\303\303"+
    "\303\302\302\302\302\302\302\302\302\302\302\302\302\302\303\303\303\303\303"+
    "\303\303\303\303\303\303\303\303\302\306\302\305\306\302\302\302\305\302\302"+
    "\302\305\303\303\303\303\303\303\303\303\303\303\303\303\303\302\306\302\305"+
    "\302\302\305\305\003\302\302\302\305\303\303\303\303\303\303\303\303\303\303"+
    "\303\303\303\302\302\302\302\302\302\302\302\302\302\302\302\302\303\303\303"+
    "\303\303\303\303\303\303\303\303\303\303\302\302\302\302\302\302\302\303\303"+
    "\303\303\303\303\303\303\303\302\303\303\303\303\303\303\303\303\303\303\303"+
    "\303\303\302\302\302\302\302\302\302\302\302\302\302\302\302\303\303\303\303"+
    "\303\303\303\303\303\303\303\303\303\302\302\302\302\302\302\302\302\303\303"+
    "\303\003\302\302\302\302\302\302\302\302\302\302\302\302\310\303\303\303\303"+
    "\303\303\303\303\303\303\303\303\311\303\303\303\302\302\302\302\302\302\302"+
    "\302\302\302\302\302\310\303\303\303\303\303\303\303\303\303\303\303\303\311"+
    "\303\303\303\302\302\302\302\302\302\302\302\302\302\302\302\310\303\303\303"+
    "\303\303\303\303\303\303\303\303\303\311\303\303\303\302\302\302\302\302\302"+
    "\302\302\302\302\302\302\310\303\303\303\303\303\303\303\303\303\303\303\303"+
    "\311\303\303\303\302\302\302\302\302\302\302\302\302\302\302\302\310\303\303"+
    "\303\303\303\303\303\303\303\303\303\303\311\303\303\303\312\003\313\313\313"+
    "\313\313\314\314\314\314\314\315\315\315\315\315\316\316\316\316\316\317\317"+
    "\317\317\317\121\121\121\121\121\121\121\121\121\121\121\121\121\121\121\121"+
    "\121\121\121\121\121\121\121\121\121\121\121\276\014\043\121\121\121\121\121"+
    "\121\121\121\276\014\014\014\043\014\014\014\014\014\014\014\276\320\156\156"+
    "\003\003\003\003\003\003\003\123\121\121\123\121\121\121\121\121\121\121\003"+
    "\003\003\003\003\003\003\003\055\055\055\056\055\055\055\055\055\055\055\055"+
    "\056\120\055\055\055\120\056\055\055\056\003\003\003\003\003\003\003\003\003"+
    "\003\066\066\067\102\103\103\103\103\121\121\121\122\003\003\003\003\321\321"+
    "\321\321\321\321\321\321\321\321\321\321\321\321\321\321\321\322\322\322\322"+
    "\322\322\322\322\322\322\322\322\322\322\322\322\322\121\205\121\122\003\003"+
    "\323\323\323\323\323\003\003\125\324\324\325\324\324\324\324\324\324\324\324"+
    "\324\324\324\324\324\325\326\326\325\325\324\324\324\324\326\324\324\325\325"+
    "\003\003\003\326\003\325\325\325\325\324\325\326\326\325\325\325\325\325\325"+
    "\326\326\325\324\326\324\324\324\326\324\324\325\324\326\326\324\324\324\324"+
    "\324\325\324\324\324\324\324\324\324\324\003\003\325\324\325\324\324\325\324"+
    "\324\324\324\324\324\324\324\003\003\003\003\003\003\003\003\003\003\327\003"+
    "\003\003\003\003\003\003\036\036\036\036\036\036\003\003\036\036\036\036\036"+
    "\036\036\036\036\036\036\036\036\036\036\036\036\036\003\003\003\003\003\003"+
    "\036\036\036\036\036\036\036\042\330\036\036\036\036\036\036\036\330\036\036"+
    "\036\036\036\036\036\330\036\036\036\036\036\036\036\036\036\036\036\036\036"+
    "\036\036\036\036\036\003\003\003\003\003\331\332\332\332\332\333\334\003\014"+
    "\014\014\014\014\014\014\014\014\014\014\014\014\014\014\041\335\335\335\335"+
    "\335\335\335\335\335\335\335\335\335\014\014\014\335\335\335\335\335\335\335"+
    "\335\335\335\335\335\335\036\003\003\335\335\335\335\335\335\335\335\335\335"+
    "\335\335\335\014\014\014\014\014\014\014\014\014\014\014\014\014\014\014\014"+
    "\014\041\003\003\003\003\003\003\003\003\003\003\003\003\014\014\014\014\014"+
    "\014\014\014\014\014\014\014\014\014\041\003\003\003\003\003\003\014\014\014"+
    "\014\014\014\014\014\014\014\014\014\014\014\014\014\014\014\014\014\014\014"+
    "\003\003\014\014\014\014\041\003\003\003\014\003\003\003\003\003\003\003\036"+
    "\036\036\003\003\003\003\003\003\003\003\003\003\003\003\003\036\036\036\036"+
    "\036\036\036\036\036\036\036\036\036\336\337\337\036\036\036\036\036\036\036"+
    "\036\036\036\042\003\003\003\003\003\036\036\036\036\036\036\042\003\036\036"+
    "\036\036\042\003\003\003\036\036\036\036\003\003\003\003\036\036\036\036\036"+
    "\003\003\003\036\036\036\036\003\003\003\003\036\036\036\036\036\036\036\036"+
    "\036\036\036\036\036\036\036\003\003\003\003\003\003\003\003\003\036\036\036"+
    "\036\036\036\036\036\036\036\036\036\036\036\036\042\036\036\036\036\036\036"+
    "\042\003\036\036\036\036\036\036\036\036\036\036\036\036\036\036\003\003\003"+
    "\003\003\003\003\003\003\003\036\036\036\036\036\036\036\036\036\036\036\036"+
    "\003\003\003\003\042\003\003\003\003\003\003\003\036\036\036\036\036\036\036"+
    "\036\036\036\036\042\003\003\003\003\003\003\003\003\003\003\003\003").toCharArray();

  // The A table has 448 entries for a total of 1792 bytes.

  static final int A[] = new int[448];
  static final String A_DATA =
    "\000\u7005\000\u7005\u7800\000\000\u7005\000\u7005\u7800\000\u7800\000\u7800"+
    "\000\000\030\u6800\030\000\030\u7800\000\u7800\000\000\u074B\000\u074B\000"+
    "\u074B\000\u074B\000\u046B\000\u058B\000\u080B\000\u080B\000\u080B\u7800\000"+
    "\000\034\000\034\000\034\u6800\u780A\u6800\u780A\u6800\u77EA\u6800\u744A\u6800"+
    "\u77AA\u6800\u742A\u6800\u780A\u6800\u76CA\u6800\u774A\u6800\u780A\u6800\u780A"+
    "\u6800\u766A\u6800\u752A\u6800\u750A\u6800\u74EA\u6800\u74EA\u6800\u74CA\u6800"+
    "\u74AA\u6800\u748A\u6800\u74CA\u6800\u754A\u6800\u752A\u6800\u750A\u6800\u74EA"+
    "\u6800\u74CA\u6800\u772A\u6800\u780A\u6800\u764A\u6800\u780A\u6800\u080B\u6800"+
    "\u080B\u6800\u080B\u6800\u080B\u6800\034\u6800\034\u6800\034\u6800\u06CB\u6800"+
    "\u080B\u6800\034\000\034\000\034\u7800\000\u6800\034\u7800\000\000\034\u4000"+
    "\u3006\u4000\u3006\u1800\u040B\u1800\u040B\u1800\u040B\u1800\u040B\u1800\u052B"+
    "\u1800\u064B\u1800\u080B\u1800\u080B\u1800\u080B\000\u042B\000\u048B\000\u050B"+
    "\000\u080B\000\u7005\000\u780A\000\u780A\u7800\000\u4000\u3006\u4000\u3006"+
    "\u4000\u3006\u7800\000\u7800\000\000\030\000\030\000\u760A\000\u760A\000\u76EA"+
    "\000\u740A\000\u780A\242\u7001\242\u7001\241\u7002\241\u7002\000\u3409\000"+
    "\u3409\u0800\u7005\u0800\u7005\u0800\u7005\u7800\000\u7800\000\u0800\u7005"+
    "\u7800\000\u0800\030\u0800\u052B\u0800\u052B\u0800\u052B\u0800\u05EB\u0800"+
    "\u070B\u0800\u080B\u0800\u080B\u0800\u080B\u0800\u7005\u0800\034\u0800\034"+
    "\u0800\u050B\u0800\u050B\u0800\u050B\u0800\u058B\u0800\u06AB\u7800\000\u0800"+
    "\u074B\u0800\u074B\u0800\u074B\u0800\u074B\u0800\u072B\u0800\u072B\u0800\u07AB"+
    "\u0800\u04CB\u0800\u080B\u7800\000\u0800\u04CB\u0800\u052B\u0800\u05AB\u0800"+
    "\u06CB\u0800\u080B\u0800\u056B\u0800\u066B\u0800\u078B\u0800\u080B\u7800\000"+
    "\u6800\030\u0800\u042B\u0800\u042B\u0800\u054B\u0800\u066B\u0800\u7005\u4000"+
    "\u3006\u7800\000\u4000\u3006\u4000\u3006\u4000\u3006\u4000\u3006\u7800\000"+
    "\u7800\000\u4000\u3006\u0800\u04CB\u0800\u05EB\u0800\030\u0800\030\u0800\030"+
    "\u7800\000\u0800\u7005\u0800\u048B\u0800\u080B\u0800\030\u0800\034\u0800\u7005"+
    "\u0800\u7005\u4000\u3006\u7800\000\u0800\u06CB\u6800\030\u6800\030\u0800\u05CB"+
    "\u0800\u06EB\u7800\000\u0800\u070B\u0800\u070B\u0800\u070B\u0800\u070B\u0800"+
    "\u07AB\u0902\u7001\u0902\u7001\u0902\u7001\u7800\000\u0901\u7002\u0901\u7002"+
    "\u0901\u7002\u7800\000\u0800\u04EB\u0800\u054B\u0800\u05CB\u0800\u080B\u3000"+
    "\u042B\u3000\u042B\u3000\u054B\u3000\u066B\u3000\u080B\u3000\u080B\u3000\u080B"+
    "\u7800\000\000\u3008\u4000\u3006\000\u3008\000\u7005\u4000\u3006\000\030\000"+
    "\030\000\030\u6800\u05EB\u6800\u05EB\u6800\u070B\u6800\u042B\000\u3749\000"+
    "\u3749\000\u3008\000\u3008\u4000\u3006\000\u3008\000\u3008\u4000\u3006\000"+
    "\030\000\u1010\000\u3609\000\u3609\u4000\u3006\000\u7005\000\u7005\u4000\u3006"+
    "\u4000\u3006\u4000\u3006\000\u3549\000\u3549\000\u7005\u4000\u3006\000\u7005"+
    "\000\u3008\000\u3008\000\u7005\000\u7005\000\030\u7800\000\000\u040B\000\u040B"+
    "\000\u040B\000\u040B\000\u052B\000\u064B\000\u080B\000\u080B\u7800\000\u4000"+
    "\u3006\000\u3008\u4000\u3006\u4000\u3006\u4000\u3006\000\u7005\000\u3008\u7800"+
    "\000\u7800\000\000\u3008\000\u3008\000\u3008\000\030\000\u7005\u4000\u3006"+
    "\000\030\u6800\030\u7800\000\000\u3008\u4000\u3006\000\u060B\000\u072B\000"+
    "\030\000\034\202\u7001\202\u7001\201\u7002\201\u7002\000\030\u4000\u3006\000"+
    "\u3008\000\u3006\000\u04EB\000\u04EB\000\u744A\000\u744A\000\u776A\000\u776A"+
    "\000\u776A\000\u76AA\000\u76AA\000\u76AA\000\u76AA\000\u758A\000\u758A\000"+
    "\u758A\000\u746A\000\u746A\000\u746A\000\u77EA\000\u77EA\000\u77CA\000\u77CA"+
    "\000\u77CA\000\u76AA\000\u768A\000\u768A\000\u768A\000\u780A\000\u780A\000"+
    "\u75AA\000\u75AA\000\u75AA\000\u758A\000\u752A\000\u750A\000\u750A\000\u74EA"+
    "\000\u74CA\000\u74AA\000\u74CA\000\u74CA\000\u74AA\000\u748A\000\u748A\000"+
    "\u746A\000\u746A\000\u744A\000\u742A\000\u740A\000\u770A\000\u770A\000\u770A"+
    "\000\u764A\000\u764A\000\u764A\000\u764A\000\u762A\000\u762A\000\u760A\000"+
    "\u752A\000\u752A\000\u780A\000\u776A\000\u776A\u7800\000\000\u7004\000\u7004"+
    "\u7800\000\000\u05EB\u4000\u3006\000\u7004\000\u7005\000\u7005\000\u7005\u7800"+
    "\000\u4800\u1010\u4800\u1010\000\034\000\u3008\000\u3008\000\u3008\000\u3008"+
    "\u4800\u1010\u4800\u1010\u4000\u3006\u4000\u3006\000\034\u4000\u3006\u6800"+
    "\034\000\u042B\000\u042B\000\u054B\000\u066B\000\u7001\000\u7001\000\u7002"+
    "\000\u7002\000\u7002\u7800\000\000\u7001\u7800\000\u7800\000\000\u7001\u7800"+
    "\000\000\u7002\000\u7001\000\031\000\u7002\uE800\031\000\u7001\000\u7002\u1800"+
    "\u3649\u1800\u3649\u1800\u3509\u1800\u3509\u1800\u37C9\u1800\u37C9\u1800\u3689"+
    "\u1800\u3689\u1800\u3549\u1800\u3549\000\034\000\030\u088A\u7001\u088A\u7001"+
    "\u0889\u7002\u0889\u7002\u0800\u3609\u0800\u3609\u1000\u7005\u1000\u7005\u7800"+
    "\000\u1000\u7005\u1000\u7005\u7800\000\u6800\031\u6800\031\u7800\000\u6800"+
    "\034\u1800\u040B\u1800\u07EB\u1800\u07EB\u1800\u07EB\u1800\u07EB\u6800\u06AB"+
    "\u6800\u068B\u7800\000\000\034\000\034\u6800\034\u6800\033\u6800\033\u6800"+
    "\033";

  // The B table has 448 entries for a total of 896 bytes.

  static final char B[] = (
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\004\004\004\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\004\000\004\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\004\004\004\000\000\000\000\000\000\000\000\000\000\000"+
    "\004\004\004\004\004\000\000\000\000\000\004\000\000\004\004\000\000\000\000"+
    "\000\000\004\000\000\000\000\000\000\000\000\000\000\000\000\000\000\004\000"+
    "\000\004\000\000\004\000\000\004\004\000\000\000\004\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\004\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\020\020\020\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\006\006\000\000\000\000").toCharArray();

  // In all, the character property tables require 13408 bytes.

    static {
                { // THIS CODE WAS AUTOMATICALLY CREATED BY GenerateCharacter:
            char[] data = A_DATA.toCharArray();
            assert (data.length == (448 * 2));
            int i = 0, j = 0;
            while (i < (448 * 2)) {
                int entry = data[i++] << 16;
                A[j++] = entry | data[i++];
            }
        }

    }        
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\CharacterData02.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
// This file was generated AUTOMATICALLY from a template file Thu Jan 17 21:22:20 PST 2019
/*
 * Copyright (c) 2003, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/** The CharacterData class encapsulates the large tables found in
    Java.lang.Character. */

class CharacterData02 extends CharacterData {
    /* The character properties are currently encoded into 32 bits in the following manner:
        1 bit   mirrored property
        4 bits  directionality property
        9 bits  signed offset used for converting case
        1 bit   if 1, adding the signed offset converts the character to lowercase
        1 bit   if 1, subtracting the signed offset converts the character to uppercase
        1 bit   if 1, this character has a titlecase equivalent (possibly itself)
        3 bits  0  may not be part of an identifier
                1  ignorable control; may continue a Unicode identifier or Java identifier
                2  may continue a Java identifier but not a Unicode identifier (unused)
                3  may continue a Unicode identifier or Java identifier
                4  is a Java whitespace character
                5  may start or continue a Java identifier;
                   may continue but not start a Unicode identifier (underscores)
                6  may start or continue a Java identifier but not a Unicode identifier ($)
                7  may start or continue a Unicode identifier or Java identifier
                Thus:
                   5, 6, 7 may start a Java identifier
                   1, 2, 3, 5, 6, 7 may continue a Java identifier
                   7 may start a Unicode identifier
                   1, 3, 5, 7 may continue a Unicode identifier
                   1 is ignorable within an identifier
                   4 is Java whitespace
        2 bits  0  this character has no numeric property
                1  adding the digit offset to the character code and then
                   masking with 0x1F will produce the desired numeric value
                2  this character has a "strange" numeric value
                3  a Java supradecimal digit: adding the digit offset to the
                   character code, then masking with 0x1F, then adding 10
                   will produce the desired numeric value
        5 bits  digit offset
        5 bits  character type

        The encoding of character properties is subject to change at any time.
     */

    int getProperties(int ch) {
	char offset = (char)ch;
        int props = A[Y[X[offset>>5]|((offset>>1)&0xF)]|(offset&0x1)];
        return props;
    }

    int getPropertiesEx(int ch) {
        char offset = (char)ch;
        int props = B[Y[X[offset>>5]|((offset>>1)&0xF)]|(offset&0x1)];
        return props;
    }

    boolean isOtherLowercase(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0001) != 0;
    }

    boolean isOtherUppercase(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0002) != 0;
    }

    boolean isOtherAlphabetic(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0004) != 0;
    }

    boolean isIdeographic(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0010) != 0;
    }

    int getType(int ch) {
        int props = getProperties(ch);
        return (props & 0x1F);
    }

    boolean isJavaIdentifierStart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) >= 0x00005000);
    }

    boolean isJavaIdentifierPart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00003000) != 0);
    }

    boolean isUnicodeIdentifierStart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00007000);
    }

    boolean isUnicodeIdentifierPart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00001000) != 0);
    }

    boolean isIdentifierIgnorable(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00001000);
    }

    int toLowerCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00020000) != 0) {
            int offset = val << 5 >> (5+18);
            mapChar = ch + offset;
        }
        return mapChar;
    }

    int toUpperCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00010000) != 0) {
            int offset = val  << 5 >> (5+18);
            mapChar =  ch - offset;
        }
        return mapChar;
    }

    int toTitleCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00008000) != 0) {
            // There is a titlecase equivalent.  Perform further checks:
            if ((val & 0x00010000) == 0) {
                // The character does not have an uppercase equivalent, so it must
                // already be uppercase; so add 1 to get the titlecase form.
                mapChar = ch + 1;
            }
            else if ((val & 0x00020000) == 0) {
                // The character does not have a lowercase equivalent, so it must
                // already be lowercase; so subtract 1 to get the titlecase form.
                mapChar = ch - 1;
            }
            // else {
            // The character has both an uppercase equivalent and a lowercase
            // equivalent, so it must itself be a titlecase form; return it.
            // return ch;
            //}
        }
        else if ((val & 0x00010000) != 0) {
            // This character has no titlecase equivalent but it does have an
            // uppercase equivalent, so use that (subtract the signed case offset).
            mapChar = toUpperCase(ch);
        }
        return mapChar;
    }

    int digit(int ch, int radix) {
        int value = -1;
        if (radix >= Character.MIN_RADIX && radix <= Character.MAX_RADIX) {
            int val = getProperties(ch);
            int kind = val & 0x1F;
            if (kind == Character.DECIMAL_DIGIT_NUMBER) {
                value = ch + ((val & 0x3E0) >> 5) & 0x1F;
            }
            else if ((val & 0xC00) == 0x00000C00) {
                // Java supradecimal digit
                value = (ch + ((val & 0x3E0) >> 5) & 0x1F) + 10;
            }
        }
        return (value < radix) ? value : -1;
    }

    int getNumericValue(int ch) {
        int val = getProperties(ch);
        int retval = -1;

        switch (val & 0xC00) {
        default: // cannot occur
        case (0x00000000):         // not numeric
            retval = -1;
            break;
        case (0x00000400):              // simple numeric
            retval = ch + ((val & 0x3E0) >> 5) & 0x1F;
            break;
        case (0x00000800)      :       // "strange" numeric
            retval = -2;
            break;
        case (0x00000C00):           // Java supradecimal
            retval = (ch + ((val & 0x3E0) >> 5) & 0x1F) + 10;
            break;
        }
        return retval;
    }

    boolean isWhitespace(int ch) {
        return (getProperties(ch) & 0x00007000) == 0x00004000;
    }

    byte getDirectionality(int ch) {
        int val = getProperties(ch);
        byte directionality = (byte)((val & 0x78000000) >> 27);
        if (directionality == 0xF ) {
	        directionality = Character.DIRECTIONALITY_UNDEFINED;
        }
        return directionality;
    }

    boolean isMirrored(int ch) {
        return (getProperties(ch) & 0x80000000) != 0;
    }

    static final CharacterData instance = new CharacterData02();
    private CharacterData02() {};

    // The following tables and code generated using:
  // java GenerateCharacter -string -plane 2 -template t:/workspace/open/make/data/characterdata/CharacterData02.java.template -spec t:/workspace/open/make/data/unicodedata/UnicodeData.txt -specialcasing t:/workspace/open/make/data/unicodedata/SpecialCasing.txt -proplist t:/workspace/open/make/data/unicodedata/PropList.txt -o t:/workspace/build/windows-x64/support/gensrc/java.base/java/lang/CharacterData02.java -usecharforbyte 11 4 1
  // The X table has 2048 entries for a total of 4096 bytes.

  static final char X[] = (
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\020\040\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\060\000\000\000\000\000\000\100\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\120\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\140\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\000\000\000\000\160\000\000\000\000\000\000"+
    "\000\000\000\000\000\100\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040").toCharArray();

  // The Y table has 128 entries for a total of 256 bytes.

  static final char Y[] = (
    "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\000\000\000\000\000\002\004\004\004\004\004\004\004\004\004\004"+
    "\004\004\004\004\004\004\004\004\004\004\000\000\000\000\000\000\000\000\000"+
    "\000\002\004\004\004\004\004\000\000\000\000\000\000\000\000\000\000\000\000"+
    "\000\000\000\004\000\004\004\004\004\004\004\004\000\000\000\000\000\000\000"+
    "\000\002\004\004\004\004\004\004\004\004\004\004\004\004\004\004\004\000\000"+
    "\000\000\000\000\000\000\006\000\000\000\000\000\000\000").toCharArray();

  // The A table has 8 entries for a total of 32 bytes.

  static final int A[] = new int[8];
  static final String A_DATA =
    "\000\u7005\000\u7005\000\u7005\u7800\000\u7800\000\u7800\000\000\u7725\000"+
    "\u7005";

  // The B table has 8 entries for a total of 16 bytes.

  static final char B[] = (
    "\020\020\020\000\000\000\020\020").toCharArray();

  // In all, the character property tables require 4384 bytes.

    static {
                { // THIS CODE WAS AUTOMATICALLY CREATED BY GenerateCharacter:
            char[] data = A_DATA.toCharArray();
            assert (data.length == (8 * 2));
            int i = 0, j = 0;
            while (i < (8 * 2)) {
                int entry = data[i++] << 16;
                A[j++] = entry | data[i++];
            }
        }

    }        
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\CharacterData0E.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
// This file was generated AUTOMATICALLY from a template file Thu Jan 17 21:22:20 PST 2019
/*
 * Copyright (c) 2003, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/** The CharacterData class encapsulates the large tables found in
    Java.lang.Character. */

class CharacterData0E extends CharacterData {
    /* The character properties are currently encoded into 32 bits in the following manner:
        1 bit   mirrored property
        4 bits  directionality property
        9 bits  signed offset used for converting case
        1 bit   if 1, adding the signed offset converts the character to lowercase
        1 bit   if 1, subtracting the signed offset converts the character to uppercase
        1 bit   if 1, this character has a titlecase equivalent (possibly itself)
        3 bits  0  may not be part of an identifier
                1  ignorable control; may continue a Unicode identifier or Java identifier
                2  may continue a Java identifier but not a Unicode identifier (unused)
                3  may continue a Unicode identifier or Java identifier
                4  is a Java whitespace character
                5  may start or continue a Java identifier;
                   may continue but not start a Unicode identifier (underscores)
                6  may start or continue a Java identifier but not a Unicode identifier ($)
                7  may start or continue a Unicode identifier or Java identifier
                Thus:
                   5, 6, 7 may start a Java identifier
                   1, 2, 3, 5, 6, 7 may continue a Java identifier
                   7 may start a Unicode identifier
                   1, 3, 5, 7 may continue a Unicode identifier
                   1 is ignorable within an identifier
                   4 is Java whitespace
        2 bits  0  this character has no numeric property
                1  adding the digit offset to the character code and then
                   masking with 0x1F will produce the desired numeric value
                2  this character has a "strange" numeric value
                3  a Java supradecimal digit: adding the digit offset to the
                   character code, then masking with 0x1F, then adding 10
                   will produce the desired numeric value
        5 bits  digit offset
        5 bits  character type

        The encoding of character properties is subject to change at any time.
     */

    int getProperties(int ch) {
        char offset = (char)ch;
        int props = A[Y[X[offset>>5]|((offset>>1)&0xF)]|(offset&0x1)];
        return props;
    }

    int getPropertiesEx(int ch) {
        char offset = (char)ch;
        int props = B[Y[X[offset>>5]|((offset>>1)&0xF)]|(offset&0x1)];
        return props;
    }

    boolean isOtherLowercase(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0001) != 0;
    }

    boolean isOtherUppercase(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0002) != 0;
    }

    boolean isOtherAlphabetic(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0004) != 0;
    }

    boolean isIdeographic(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0010) != 0;
    }

    int getType(int ch) {
        int props = getProperties(ch);
        return (props & 0x1F);
    }

    boolean isJavaIdentifierStart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) >= 0x00005000);
    }

    boolean isJavaIdentifierPart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00003000) != 0);
    }

    boolean isUnicodeIdentifierStart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00007000);
    }

    boolean isUnicodeIdentifierPart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00001000) != 0);
    }

    boolean isIdentifierIgnorable(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00001000);
    }

    int toLowerCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00020000) != 0) {
            int offset = val << 5 >> (5+18);
            mapChar = ch + offset;
        }
        return mapChar;
    }

    int toUpperCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00010000) != 0) {
            int offset = val  << 5 >> (5+18);
            mapChar =  ch - offset;
        }
        return mapChar;
    }

    int toTitleCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00008000) != 0) {
            // There is a titlecase equivalent.  Perform further checks:
            if ((val & 0x00010000) == 0) {
                // The character does not have an uppercase equivalent, so it must
                // already be uppercase; so add 1 to get the titlecase form.
                mapChar = ch + 1;
            }
            else if ((val & 0x00020000) == 0) {
                // The character does not have a lowercase equivalent, so it must
                // already be lowercase; so subtract 1 to get the titlecase form.
                mapChar = ch - 1;
            }
            // else {
            // The character has both an uppercase equivalent and a lowercase
            // equivalent, so it must itself be a titlecase form; return it.
            // return ch;
            //}
        }
        else if ((val & 0x00010000) != 0) {
            // This character has no titlecase equivalent but it does have an
            // uppercase equivalent, so use that (subtract the signed case offset).
            mapChar = toUpperCase(ch);
        }
        return mapChar;
    }

    int digit(int ch, int radix) {
        int value = -1;
        if (radix >= Character.MIN_RADIX && radix <= Character.MAX_RADIX) {
            int val = getProperties(ch);
            int kind = val & 0x1F;
            if (kind == Character.DECIMAL_DIGIT_NUMBER) {
                value = ch + ((val & 0x3E0) >> 5) & 0x1F;
            }
            else if ((val & 0xC00) == 0x00000C00) {
                // Java supradecimal digit
                value = (ch + ((val & 0x3E0) >> 5) & 0x1F) + 10;
            }
        }
        return (value < radix) ? value : -1;
    }

    int getNumericValue(int ch) {
        int val = getProperties(ch);
        int retval = -1;

        switch (val & 0xC00) {
        default: // cannot occur
        case (0x00000000):         // not numeric
            retval = -1;
            break;
        case (0x00000400):              // simple numeric
            retval = ch + ((val & 0x3E0) >> 5) & 0x1F;
            break;
        case (0x00000800)      :       // "strange" numeric
            retval = -2;
            break;
        case (0x00000C00):           // Java supradecimal
            retval = (ch + ((val & 0x3E0) >> 5) & 0x1F) + 10;
            break;
        }
        return retval;
    }

    boolean isWhitespace(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00004000);
    }

    byte getDirectionality(int ch) {
        int val = getProperties(ch);
        byte directionality = (byte)((val & 0x78000000) >> 27);
        if (directionality == 0xF ) {
	        directionality = Character.DIRECTIONALITY_UNDEFINED;
        }
        return directionality;
    }

    boolean isMirrored(int ch) {
        int props = getProperties(ch);
        return ((props & 0x80000000) != 0);
    }

    static final CharacterData instance = new CharacterData0E();
    private CharacterData0E() {};

    // The following tables and code generated using:
  // java GenerateCharacter -string -plane 14 -template t:/workspace/open/make/data/characterdata/CharacterData0E.java.template -spec t:/workspace/open/make/data/unicodedata/UnicodeData.txt -specialcasing t:/workspace/open/make/data/unicodedata/SpecialCasing.txt -proplist t:/workspace/open/make/data/unicodedata/PropList.txt -o t:/workspace/build/windows-x64/support/gensrc/java.base/java/lang/CharacterData0E.java -usecharforbyte 11 4 1
  // The X table has 2048 entries for a total of 4096 bytes.

  static final char X[] = (
    "\000\020\020\020\040\040\040\040\060\060\060\060\060\060\060\100\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040"+
    "\040\040\040\040\040\040\040\040\040\040\040\040\040\040\040").toCharArray();

  // The Y table has 80 entries for a total of 160 bytes.

  static final char Y[] = (
    "\000\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\004\004\004"+
    "\004\004\004\004\004\004\004\004\004\004\004\004\004\002\002\002\002\002\002"+
    "\002\002\002\002\002\002\002\002\002\002\006\006\006\006\006\006\006\006\006"+
    "\006\006\006\006\006\006\006\006\006\006\006\006\006\006\006\002\002\002\002"+
    "\002\002\002\002").toCharArray();

  // The A table has 8 entries for a total of 32 bytes.

  static final int A[] = new int[8];
  static final String A_DATA =
    "\u7800\000\u4800\u1010\u7800\000\u7800\000\u4800\u1010\u4800\u1010\u4000\u3006"+
    "\u4000\u3006";

  // The B table has 8 entries for a total of 16 bytes.

  static final char B[] = (
    "\000\000\000\000\000\000\000\000").toCharArray();

  // In all, the character property tables require 4288 bytes.

    static {
                { // THIS CODE WAS AUTOMATICALLY CREATED BY GenerateCharacter:
            char[] data = A_DATA.toCharArray();
            assert (data.length == (8 * 2));
            int i = 0, j = 0;
            while (i < (8 * 2)) {
                int entry = data[i++] << 16;
                A[j++] = entry | data[i++];
            }
        }

    }        
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\CharacterDataLatin1.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
// This file was generated AUTOMATICALLY from a template file Thu Jan 17 21:22:19 PST 2019
/*
 * Copyright (c) 2002, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/** The CharacterData class encapsulates the large tables found in
    Java.lang.Character. */

class CharacterDataLatin1 extends CharacterData {

    /* The character properties are currently encoded into 32 bits in the following manner:
        1 bit   mirrored property
        4 bits  directionality property
        9 bits  signed offset used for converting case
        1 bit   if 1, adding the signed offset converts the character to lowercase
        1 bit   if 1, subtracting the signed offset converts the character to uppercase
        1 bit   if 1, this character has a titlecase equivalent (possibly itself)
        3 bits  0  may not be part of an identifier
                1  ignorable control; may continue a Unicode identifier or Java identifier
                2  may continue a Java identifier but not a Unicode identifier (unused)
                3  may continue a Unicode identifier or Java identifier
                4  is a Java whitespace character
                5  may start or continue a Java identifier;
                   may continue but not start a Unicode identifier (underscores)
                6  may start or continue a Java identifier but not a Unicode identifier ($)
                7  may start or continue a Unicode identifier or Java identifier
                Thus:
                   5, 6, 7 may start a Java identifier
                   1, 2, 3, 5, 6, 7 may continue a Java identifier
                   7 may start a Unicode identifier
                   1, 3, 5, 7 may continue a Unicode identifier
                   1 is ignorable within an identifier
                   4 is Java whitespace
        2 bits  0  this character has no numeric property
                1  adding the digit offset to the character code and then
                   masking with 0x1F will produce the desired numeric value
                2  this character has a "strange" numeric value
                3  a Java supradecimal digit: adding the digit offset to the
                   character code, then masking with 0x1F, then adding 10
                   will produce the desired numeric value
        5 bits  digit offset
        5 bits  character type

        The encoding of character properties is subject to change at any time.
     */

    int getProperties(int ch) {
        char offset = (char)ch;
        int props = A[offset];
        return props;
    }

    int getPropertiesEx(int ch) {
        char offset = (char)ch;
        int props = B[offset];
        return props;
    }

    boolean isOtherLowercase(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0001) != 0;
    }

    boolean isOtherUppercase(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0002) != 0;
    }

    boolean isOtherAlphabetic(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0004) != 0;
    }

    boolean isIdeographic(int ch) {
        int props = getPropertiesEx(ch);
        return (props & 0x0010) != 0;
    }

    int getType(int ch) {
        int props = getProperties(ch);
        return (props & 0x1F);
    }

    boolean isJavaIdentifierStart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) >= 0x00005000);
    }

    boolean isJavaIdentifierPart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00003000) != 0);
    }

    boolean isUnicodeIdentifierStart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00007000);
    }

    boolean isUnicodeIdentifierPart(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00001000) != 0);
    }

    boolean isIdentifierIgnorable(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00001000);
    }

    int toLowerCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if (((val & 0x00020000) != 0) && 
                ((val & 0x07FC0000) != 0x07FC0000)) { 
            int offset = val << 5 >> (5+18);
            mapChar = ch + offset;
        }
        return mapChar;
    }

    int toUpperCase(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00010000) != 0) {
            if ((val & 0x07FC0000) != 0x07FC0000) {
                int offset = val  << 5 >> (5+18);
                mapChar =  ch - offset;
            } else if (ch == 0x00B5) {
                mapChar = 0x039C;
            }
        }
        return mapChar;
    }

    int toTitleCase(int ch) {
        return toUpperCase(ch);
    }

    // Digit values for codePoints in the 0-255 range. Contents generated using:
    // for (char i = 0; i < 256; i++) {
    //     int v = -1;
    //     if (i >= '0' && i <= '9') { v = i - '0'; } 
    //     else if (i >= 'A' && i <= 'Z') { v = i - 'A' + 10; }
    //     else if (i >= 'a' && i <= 'z') { v = i - 'a' + 10; }
    //     if (i % 20 == 0) System.out.println();
    //     System.out.printf("%2d, ", v);
    // }
    //
    // Analysis has shown that generating the whole array allows the JIT to generate
    // better code compared to a slimmed down array, such as one cutting off after 'z'
    private static final byte[] DIGITS = new byte[] {
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, -1, -1,
        -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
        25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1, -1, 10, 11, 12,
        13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
        33, 34, 35, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };

    int digit(int ch, int radix) {
        int value = DIGITS[ch];
        return (value >= 0 && value < radix && radix >= Character.MIN_RADIX
                && radix <= Character.MAX_RADIX) ? value : -1;
    }

    int getNumericValue(int ch) {
        int val = getProperties(ch);
        int retval = -1;

        switch (val & 0xC00) {
            default: // cannot occur
            case (0x00000000):         // not numeric
                retval = -1;
                break;
            case (0x00000400):              // simple numeric
                retval = ch + ((val & 0x3E0) >> 5) & 0x1F;
                break;
            case (0x00000800)      :       // "strange" numeric
                 retval = -2; 
                 break;
            case (0x00000C00):           // Java supradecimal
                retval = (ch + ((val & 0x3E0) >> 5) & 0x1F) + 10;
                break;
        }
        return retval;
    }

    boolean isWhitespace(int ch) {
        int props = getProperties(ch);
        return ((props & 0x00007000) == 0x00004000);
    }

    byte getDirectionality(int ch) {
        int val = getProperties(ch);
        byte directionality = (byte)((val & 0x78000000) >> 27);

        if (directionality == 0xF ) {
            directionality = -1;
        }
        return directionality;
    }

    boolean isMirrored(int ch) {
        int props = getProperties(ch);
        return ((props & 0x80000000) != 0);
    }

    int toUpperCaseEx(int ch) {
        int mapChar = ch;
        int val = getProperties(ch);

        if ((val & 0x00010000) != 0) {
            if ((val & 0x07FC0000) != 0x07FC0000) {
                int offset = val  << 5 >> (5+18);
                mapChar =  ch - offset;
            }
            else {
                switch(ch) {
                    // map overflow characters
                    case 0x00B5 : mapChar = 0x039C; break;
                    default       : mapChar = Character.ERROR; break;
                }
            }
        }
        return mapChar;
    }

    static char[] sharpsMap = new char[] {'S', 'S'};

    char[] toUpperCaseCharArray(int ch) {
        char[] upperMap = {(char)ch};
        if (ch == 0x00DF) {
            upperMap = sharpsMap;
        }
        return upperMap;
    }

    static final CharacterDataLatin1 instance = new CharacterDataLatin1();
    private CharacterDataLatin1() {};

    // The following tables and code generated using:
  // java GenerateCharacter -template t:/workspace/open/make/data/characterdata/CharacterDataLatin1.java.template -spec t:/workspace/open/make/data/unicodedata/UnicodeData.txt -specialcasing t:/workspace/open/make/data/unicodedata/SpecialCasing.txt -proplist t:/workspace/open/make/data/unicodedata/PropList.txt -o t:/workspace/build/windows-x64/support/gensrc/java.base/java/lang/CharacterDataLatin1.java -usecharforbyte -latin1 8
  // The A table has 256 entries for a total of 1024 bytes.

  static final int A[] = {
    0x4800100F,  //   0   Cc, ignorable
    0x4800100F,  //   1   Cc, ignorable
    0x4800100F,  //   2   Cc, ignorable
    0x4800100F,  //   3   Cc, ignorable
    0x4800100F,  //   4   Cc, ignorable
    0x4800100F,  //   5   Cc, ignorable
    0x4800100F,  //   6   Cc, ignorable
    0x4800100F,  //   7   Cc, ignorable
    0x4800100F,  //   8   Cc, ignorable
    0x5800400F,  //   9   Cc, S, whitespace
    0x5000400F,  //  10   Cc, B, whitespace
    0x5800400F,  //  11   Cc, S, whitespace
    0x6000400F,  //  12   Cc, WS, whitespace
    0x5000400F,  //  13   Cc, B, whitespace
    0x4800100F,  //  14   Cc, ignorable
    0x4800100F,  //  15   Cc, ignorable
    0x4800100F,  //  16   Cc, ignorable
    0x4800100F,  //  17   Cc, ignorable
    0x4800100F,  //  18   Cc, ignorable
    0x4800100F,  //  19   Cc, ignorable
    0x4800100F,  //  20   Cc, ignorable
    0x4800100F,  //  21   Cc, ignorable
    0x4800100F,  //  22   Cc, ignorable
    0x4800100F,  //  23   Cc, ignorable
    0x4800100F,  //  24   Cc, ignorable
    0x4800100F,  //  25   Cc, ignorable
    0x4800100F,  //  26   Cc, ignorable
    0x4800100F,  //  27   Cc, ignorable
    0x5000400F,  //  28   Cc, B, whitespace
    0x5000400F,  //  29   Cc, B, whitespace
    0x5000400F,  //  30   Cc, B, whitespace
    0x5800400F,  //  31   Cc, S, whitespace
    0x6000400C,  //  32   Zs, WS, whitespace
    0x68000018,  //  33   Po, ON
    0x68000018,  //  34   Po, ON
    0x28000018,  //  35   Po, ET
    0x2800601A,  //  36   Sc, ET, currency
    0x28000018,  //  37   Po, ET
    0x68000018,  //  38   Po, ON
    0x68000018,  //  39   Po, ON
    -0x17FFFFEB,  //  40   No, hasUpper (subtract 511), hasLower (add 511), hasTitle, identifier start, supradecimal 31
    -0x17FFFFEA,  //  41   Nl, hasUpper (subtract 511), hasLower (add 511), hasTitle, identifier start, supradecimal 31
    0x68000018,  //  42   Po, ON
    0x20000019,  //  43   Sm, ES
    0x38000018,  //  44   Po, CS
    0x20000014,  //  45   Pd, ES
    0x38000018,  //  46   Po, CS
    0x38000018,  //  47   Po, CS
    0x18003609,  //  48   Nd, EN, identifier part, decimal 16
    0x18003609,  //  49   Nd, EN, identifier part, decimal 16
    0x18003609,  //  50   Nd, EN, identifier part, decimal 16
    0x18003609,  //  51   Nd, EN, identifier part, decimal 16
    0x18003609,  //  52   Nd, EN, identifier part, decimal 16
    0x18003609,  //  53   Nd, EN, identifier part, decimal 16
    0x18003609,  //  54   Nd, EN, identifier part, decimal 16
    0x18003609,  //  55   Nd, EN, identifier part, decimal 16
    0x18003609,  //  56   Nd, EN, identifier part, decimal 16
    0x18003609,  //  57   Nd, EN, identifier part, decimal 16
    0x38000018,  //  58   Po, CS
    0x68000018,  //  59   Po, ON
    -0x17FFFFE7,  //  60   Me, hasUpper (subtract 511), hasLower (add 511), hasTitle, identifier start, supradecimal 31
    0x68000019,  //  61   Sm, ON
    -0x17FFFFE7,  //  62   Me, hasUpper (subtract 511), hasLower (add 511), hasTitle, identifier start, supradecimal 31
    0x68000018,  //  63   Po, ON
    0x68000018,  //  64   Po, ON
    0x00827FE1,  //  65   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  66   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  67   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  68   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  69   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  70   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  71   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  72   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  73   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  74   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  75   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  76   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  77   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  78   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  79   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  80   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  81   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  82   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  83   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  84   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  85   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  86   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  87   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  88   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  89   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    0x00827FE1,  //  90   Lu, L, hasLower (add 32), identifier start, supradecimal 31
    -0x17FFFFEB,  //  91   No, hasUpper (subtract 511), hasLower (add 511), hasTitle, identifier start, supradecimal 31
    0x68000018,  //  92   Po, ON
    -0x17FFFFEA,  //  93   Nl, hasUpper (subtract 511), hasLower (add 511), hasTitle, identifier start, supradecimal 31
    0x6800001B,  //  94   Sk, ON
    0x68005017,  //  95   Pc, ON, underscore
    0x6800001B,  //  96   Sk, ON
    0x00817FE2,  //  97   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  //  98   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  //  99   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 100   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 101   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 102   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 103   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 104   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 105   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 106   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 107   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 108   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 109   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 110   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 111   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 112   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 113   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 114   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 115   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 116   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 117   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 118   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 119   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 120   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 121   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    0x00817FE2,  // 122   Ll, L, hasUpper (subtract 32), identifier start, supradecimal 31
    -0x17FFFFEB,  // 123   No, hasUpper (subtract 511), hasLower (add 511), hasTitle, identifier start, supradecimal 31
    0x68000019,  // 124   Sm, ON
    -0x17FFFFEA,  // 125   Nl, hasUpper (subtract 511), hasLower (add 511), hasTitle, identifier start, supradecimal 31
    0x68000019,  // 126   Sm, ON
    0x4800100F,  // 127   Cc, ignorable
    0x4800100F,  // 128   Cc, ignorable
    0x4800100F,  // 129   Cc, ignorable
    0x4800100F,  // 130   Cc, ignorable
    0x4800100F,  // 131   Cc, ignorable
    0x4800100F,  // 132   Cc, ignorable
    0x5000100F,  // 133   Cc, B, ignorable
    0x4800100F,  // 134   Cc, ignorable
    0x4800100F,  // 135   Cc, ignorable
    0x4800100F,  // 136   Cc, ignorable
    0x4800100F,  // 137   Cc, ignorable
    0x4800100F,  // 138   Cc, ignorable
    0x4800100F,  // 139   Cc, ignorable
    0x4800100F,  // 140   Cc, ignorable
    0x4800100F,  // 141   Cc, ignorable
    0x4800100F,  // 142   Cc, ignorable
    0x4800100F,  // 143   Cc, ignorable
    0x4800100F,  // 144   Cc, ignorable
    0x4800100F,  // 145   Cc, ignorable
    0x4800100F,  // 146   Cc, ignorable
    0x4800100F,  // 147   Cc, ignorable
    0x4800100F,  // 148   Cc, ignorable
    0x4800100F,  // 149   Cc, ignorable
    0x4800100F,  // 150   Cc, ignorable
    0x4800100F,  // 151   Cc, ignorable
    0x4800100F,  // 152   Cc, ignorable
    0x4800100F,  // 153   Cc, ignorable
    0x4800100F,  // 154   Cc, ignorable
    0x4800100F,  // 155   Cc, ignorable
    0x4800100F,  // 156   Cc, ignorable
    0x4800100F,  // 157   Cc, ignorable
    0x4800100F,  // 158   Cc, ignorable
    0x4800100F,  // 159   Cc, ignorable
    0x3800000C,  // 160   Zs, CS
    0x68000018,  // 161   Po, ON
    0x2800601A,  // 162   Sc, ET, currency
    0x2800601A,  // 163   Sc, ET, currency
    0x2800601A,  // 164   Sc, ET, currency
    0x2800601A,  // 165   Sc, ET, currency
    0x6800001C,  // 166   So, ON
    0x68000018,  // 167   Po, ON
    0x6800001B,  // 168   Sk, ON
    0x6800001C,  // 169   So, ON
    -0xFFFF8FFB,  // 170   Sk, hasUpper (subtract 511), hasLower (add 511), hasTitle, supradecimal 31
    -0x17FFFFE3,  // 171   Lt, hasUpper (subtract 511), hasLower (add 511), hasTitle, identifier start, supradecimal 31
    0x68000019,  // 172   Sm, ON
    0x48001010,  // 173   Cf, ignorable
    0x6800001C,  // 174   So, ON
    0x6800001B,  // 175   Sk, ON
    0x2800001C,  // 176   So, ET
    0x28000019,  // 177   Sm, ET
    0x1800060B,  // 178   No, EN, decimal 16
    0x1800060B,  // 179   No, EN, decimal 16
    0x6800001B,  // 180   Sk, ON
    0x07FD7002,  // 181   Ll, L, hasUpper (subtract 511), identifier start
    0x68000018,  // 182   Po, ON
    0x68000018,  // 183   Po, ON
    0x6800001B,  // 184   Sk, ON
    0x1800050B,  // 185   No, EN, decimal 8
    -0xFFFF8FFB,  // 186   Sk, hasUpper (subtract 511), hasLower (add 511), hasTitle, supradecimal 31
    -0x17FFFFE2,  // 187   Ll, hasUpper (subtract 511), hasLower (add 511), hasTitle, identifier start, supradecimal 31
    0x6800080B,  // 188   No, ON, strange
    0x6800080B,  // 189   No, ON, strange
    0x6800080B,  // 190   No, ON, strange
    0x68000018,  // 191   Po, ON
    0x00827001,  // 192   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 193   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 194   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 195   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 196   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 197   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 198   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 199   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 200   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 201   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 202   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 203   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 204   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 205   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 206   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 207   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 208   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 209   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 210   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 211   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 212   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 213   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 214   Lu, L, hasLower (add 32), identifier start
    0x68000019,  // 215   Sm, ON
    0x00827001,  // 216   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 217   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 218   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 219   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 220   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 221   Lu, L, hasLower (add 32), identifier start
    0x00827001,  // 222   Lu, L, hasLower (add 32), identifier start
    0x07FD7002,  // 223   Ll, L, hasUpper (subtract 511), identifier start
    0x00817002,  // 224   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 225   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 226   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 227   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 228   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 229   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 230   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 231   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 232   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 233   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 234   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 235   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 236   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 237   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 238   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 239   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 240   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 241   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 242   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 243   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 244   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 245   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 246   Ll, L, hasUpper (subtract 32), identifier start
    0x68000019,  // 247   Sm, ON
    0x00817002,  // 248   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 249   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 250   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 251   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 252   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 253   Ll, L, hasUpper (subtract 32), identifier start
    0x00817002,  // 254   Ll, L, hasUpper (subtract 32), identifier start
    0x061D7002   // 255   Ll, L, hasUpper (subtract 391), identifier start
  };

  // The B table has 256 entries for a total of 512 bytes.

  static final char B[] = {
    0x0000,  //   0   unassigned, L
    0x0000,  //   1   unassigned, L
    0x0000,  //   2   unassigned, L
    0x0000,  //   3   unassigned, L
    0x0000,  //   4   unassigned, L
    0x0000,  //   5   unassigned, L
    0x0000,  //   6   unassigned, L
    0x0000,  //   7   unassigned, L
    0x0000,  //   8   unassigned, L
    0x0000,  //   9   unassigned, L
    0x0000,  //  10   unassigned, L
    0x0000,  //  11   unassigned, L
    0x0000,  //  12   unassigned, L
    0x0000,  //  13   unassigned, L
    0x0000,  //  14   unassigned, L
    0x0000,  //  15   unassigned, L
    0x0000,  //  16   unassigned, L
    0x0000,  //  17   unassigned, L
    0x0000,  //  18   unassigned, L
    0x0000,  //  19   unassigned, L
    0x0000,  //  20   unassigned, L
    0x0000,  //  21   unassigned, L
    0x0000,  //  22   unassigned, L
    0x0000,  //  23   unassigned, L
    0x0000,  //  24   unassigned, L
    0x0000,  //  25   unassigned, L
    0x0000,  //  26   unassigned, L
    0x0000,  //  27   unassigned, L
    0x0000,  //  28   unassigned, L
    0x0000,  //  29   unassigned, L
    0x0000,  //  30   unassigned, L
    0x0000,  //  31   unassigned, L
    0x0000,  //  32   unassigned, L
    0x0000,  //  33   unassigned, L
    0x0000,  //  34   unassigned, L
    0x0000,  //  35   unassigned, L
    0x0000,  //  36   unassigned, L
    0x0000,  //  37   unassigned, L
    0x0000,  //  38   unassigned, L
    0x0000,  //  39   unassigned, L
    0x0000,  //  40   unassigned, L
    0x0000,  //  41   unassigned, L
    0x0000,  //  42   unassigned, L
    0x0000,  //  43   unassigned, L
    0x0000,  //  44   unassigned, L
    0x0000,  //  45   unassigned, L
    0x0000,  //  46   unassigned, L
    0x0000,  //  47   unassigned, L
    0x0000,  //  48   unassigned, L
    0x0000,  //  49   unassigned, L
    0x0000,  //  50   unassigned, L
    0x0000,  //  51   unassigned, L
    0x0000,  //  52   unassigned, L
    0x0000,  //  53   unassigned, L
    0x0000,  //  54   unassigned, L
    0x0000,  //  55   unassigned, L
    0x0000,  //  56   unassigned, L
    0x0000,  //  57   unassigned, L
    0x0000,  //  58   unassigned, L
    0x0000,  //  59   unassigned, L
    0x0000,  //  60   unassigned, L
    0x0000,  //  61   unassigned, L
    0x0000,  //  62   unassigned, L
    0x0000,  //  63   unassigned, L
    0x0000,  //  64   unassigned, L
    0x0000,  //  65   unassigned, L
    0x0000,  //  66   unassigned, L
    0x0000,  //  67   unassigned, L
    0x0000,  //  68   unassigned, L
    0x0000,  //  69   unassigned, L
    0x0000,  //  70   unassigned, L
    0x0000,  //  71   unassigned, L
    0x0000,  //  72   unassigned, L
    0x0000,  //  73   unassigned, L
    0x0000,  //  74   unassigned, L
    0x0000,  //  75   unassigned, L
    0x0000,  //  76   unassigned, L
    0x0000,  //  77   unassigned, L
    0x0000,  //  78   unassigned, L
    0x0000,  //  79   unassigned, L
    0x0000,  //  80   unassigned, L
    0x0000,  //  81   unassigned, L
    0x0000,  //  82   unassigned, L
    0x0000,  //  83   unassigned, L
    0x0000,  //  84   unassigned, L
    0x0000,  //  85   unassigned, L
    0x0000,  //  86   unassigned, L
    0x0000,  //  87   unassigned, L
    0x0000,  //  88   unassigned, L
    0x0000,  //  89   unassigned, L
    0x0000,  //  90   unassigned, L
    0x0000,  //  91   unassigned, L
    0x0000,  //  92   unassigned, L
    0x0000,  //  93   unassigned, L
    0x0000,  //  94   unassigned, L
    0x0000,  //  95   unassigned, L
    0x0000,  //  96   unassigned, L
    0x0000,  //  97   unassigned, L
    0x0000,  //  98   unassigned, L
    0x0000,  //  99   unassigned, L
    0x0000,  // 100   unassigned, L
    0x0000,  // 101   unassigned, L
    0x0000,  // 102   unassigned, L
    0x0000,  // 103   unassigned, L
    0x0000,  // 104   unassigned, L
    0x0000,  // 105   unassigned, L
    0x0000,  // 106   unassigned, L
    0x0000,  // 107   unassigned, L
    0x0000,  // 108   unassigned, L
    0x0000,  // 109   unassigned, L
    0x0000,  // 110   unassigned, L
    0x0000,  // 111   unassigned, L
    0x0000,  // 112   unassigned, L
    0x0000,  // 113   unassigned, L
    0x0000,  // 114   unassigned, L
    0x0000,  // 115   unassigned, L
    0x0000,  // 116   unassigned, L
    0x0000,  // 117   unassigned, L
    0x0000,  // 118   unassigned, L
    0x0000,  // 119   unassigned, L
    0x0000,  // 120   unassigned, L
    0x0000,  // 121   unassigned, L
    0x0000,  // 122   unassigned, L
    0x0000,  // 123   unassigned, L
    0x0000,  // 124   unassigned, L
    0x0000,  // 125   unassigned, L
    0x0000,  // 126   unassigned, L
    0x0000,  // 127   unassigned, L
    0x0000,  // 128   unassigned, L
    0x0000,  // 129   unassigned, L
    0x0000,  // 130   unassigned, L
    0x0000,  // 131   unassigned, L
    0x0000,  // 132   unassigned, L
    0x0000,  // 133   unassigned, L
    0x0000,  // 134   unassigned, L
    0x0000,  // 135   unassigned, L
    0x0000,  // 136   unassigned, L
    0x0000,  // 137   unassigned, L
    0x0000,  // 138   unassigned, L
    0x0000,  // 139   unassigned, L
    0x0000,  // 140   unassigned, L
    0x0000,  // 141   unassigned, L
    0x0000,  // 142   unassigned, L
    0x0000,  // 143   unassigned, L
    0x0000,  // 144   unassigned, L
    0x0000,  // 145   unassigned, L
    0x0000,  // 146   unassigned, L
    0x0000,  // 147   unassigned, L
    0x0000,  // 148   unassigned, L
    0x0000,  // 149   unassigned, L
    0x0000,  // 150   unassigned, L
    0x0000,  // 151   unassigned, L
    0x0000,  // 152   unassigned, L
    0x0000,  // 153   unassigned, L
    0x0000,  // 154   unassigned, L
    0x0000,  // 155   unassigned, L
    0x0000,  // 156   unassigned, L
    0x0000,  // 157   unassigned, L
    0x0000,  // 158   unassigned, L
    0x0000,  // 159   unassigned, L
    0x0000,  // 160   unassigned, L
    0x0000,  // 161   unassigned, L
    0x0000,  // 162   unassigned, L
    0x0000,  // 163   unassigned, L
    0x0000,  // 164   unassigned, L
    0x0000,  // 165   unassigned, L
    0x0000,  // 166   unassigned, L
    0x0000,  // 167   unassigned, L
    0x0000,  // 168   unassigned, L
    0x0000,  // 169   unassigned, L
    0x0001,  // 170   Lu, L
    0x0000,  // 171   unassigned, L
    0x0000,  // 172   unassigned, L
    0x0000,  // 173   unassigned, L
    0x0000,  // 174   unassigned, L
    0x0000,  // 175   unassigned, L
    0x0000,  // 176   unassigned, L
    0x0000,  // 177   unassigned, L
    0x0000,  // 178   unassigned, L
    0x0000,  // 179   unassigned, L
    0x0000,  // 180   unassigned, L
    0x0000,  // 181   unassigned, L
    0x0000,  // 182   unassigned, L
    0x0000,  // 183   unassigned, L
    0x0000,  // 184   unassigned, L
    0x0000,  // 185   unassigned, L
    0x0001,  // 186   Lu, L
    0x0000,  // 187   unassigned, L
    0x0000,  // 188   unassigned, L
    0x0000,  // 189   unassigned, L
    0x0000,  // 190   unassigned, L
    0x0000,  // 191   unassigned, L
    0x0000,  // 192   unassigned, L
    0x0000,  // 193   unassigned, L
    0x0000,  // 194   unassigned, L
    0x0000,  // 195   unassigned, L
    0x0000,  // 196   unassigned, L
    0x0000,  // 197   unassigned, L
    0x0000,  // 198   unassigned, L
    0x0000,  // 199   unassigned, L
    0x0000,  // 200   unassigned, L
    0x0000,  // 201   unassigned, L
    0x0000,  // 202   unassigned, L
    0x0000,  // 203   unassigned, L
    0x0000,  // 204   unassigned, L
    0x0000,  // 205   unassigned, L
    0x0000,  // 206   unassigned, L
    0x0000,  // 207   unassigned, L
    0x0000,  // 208   unassigned, L
    0x0000,  // 209   unassigned, L
    0x0000,  // 210   unassigned, L
    0x0000,  // 211   unassigned, L
    0x0000,  // 212   unassigned, L
    0x0000,  // 213   unassigned, L
    0x0000,  // 214   unassigned, L
    0x0000,  // 215   unassigned, L
    0x0000,  // 216   unassigned, L
    0x0000,  // 217   unassigned, L
    0x0000,  // 218   unassigned, L
    0x0000,  // 219   unassigned, L
    0x0000,  // 220   unassigned, L
    0x0000,  // 221   unassigned, L
    0x0000,  // 222   unassigned, L
    0x0000,  // 223   unassigned, L
    0x0000,  // 224   unassigned, L
    0x0000,  // 225   unassigned, L
    0x0000,  // 226   unassigned, L
    0x0000,  // 227   unassigned, L
    0x0000,  // 228   unassigned, L
    0x0000,  // 229   unassigned, L
    0x0000,  // 230   unassigned, L
    0x0000,  // 231   unassigned, L
    0x0000,  // 232   unassigned, L
    0x0000,  // 233   unassigned, L
    0x0000,  // 234   unassigned, L
    0x0000,  // 235   unassigned, L
    0x0000,  // 236   unassigned, L
    0x0000,  // 237   unassigned, L
    0x0000,  // 238   unassigned, L
    0x0000,  // 239   unassigned, L
    0x0000,  // 240   unassigned, L
    0x0000,  // 241   unassigned, L
    0x0000,  // 242   unassigned, L
    0x0000,  // 243   unassigned, L
    0x0000,  // 244   unassigned, L
    0x0000,  // 245   unassigned, L
    0x0000,  // 246   unassigned, L
    0x0000,  // 247   unassigned, L
    0x0000,  // 248   unassigned, L
    0x0000,  // 249   unassigned, L
    0x0000,  // 250   unassigned, L
    0x0000,  // 251   unassigned, L
    0x0000,  // 252   unassigned, L
    0x0000,  // 253   unassigned, L
    0x0000,  // 254   unassigned, L
    0x0000   // 255   unassigned, L
  };

  // In all, the character property tables require 1024 bytes.

    static {
        
    }        
}


【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\CharacterDataPrivateUse.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2003, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/** The CharacterData class encapsulates the large tables found in
    Java.lang.Character. */

class CharacterDataPrivateUse extends CharacterData {

    int getProperties(int ch) {
        return 0;
    }

    int getType(int ch) {
	return (ch & 0xFFFE) == 0xFFFE
	    ? Character.UNASSIGNED
	    : Character.PRIVATE_USE;
    }

    boolean isJavaIdentifierStart(int ch) {
		return false;
    }

    boolean isJavaIdentifierPart(int ch) {
		return false;
    }

    boolean isUnicodeIdentifierStart(int ch) {
		return false;
    }

    boolean isUnicodeIdentifierPart(int ch) {
		return false;
    }

    boolean isIdentifierIgnorable(int ch) {
		return false;
    }

    int toLowerCase(int ch) {
		return ch;
    }

    int toUpperCase(int ch) {
		return ch;
    }

    int toTitleCase(int ch) {
		return ch;
    }

    int digit(int ch, int radix) {
		return -1;
    }

    int getNumericValue(int ch) {
		return -1;
    }

    boolean isWhitespace(int ch) {
		return false;
    }

    byte getDirectionality(int ch) {
	return (ch & 0xFFFE) == 0xFFFE
	    ? Character.DIRECTIONALITY_UNDEFINED
	    : Character.DIRECTIONALITY_LEFT_TO_RIGHT;
    }

    boolean isMirrored(int ch) {
		return false;
    }

    static final CharacterData instance = new CharacterDataPrivateUse();
    private CharacterDataPrivateUse() {};
}

	

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\CharacterDataUndefined.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2003, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/** The CharacterData class encapsulates the large tables found in
    Java.lang.Character. */

class CharacterDataUndefined extends CharacterData {

    int getProperties(int ch) {
        return 0;
    }

    int getType(int ch) {
	return Character.UNASSIGNED;
    }

    boolean isJavaIdentifierStart(int ch) {
		return false;
    }

    boolean isJavaIdentifierPart(int ch) {
		return false;
    }

    boolean isUnicodeIdentifierStart(int ch) {
		return false;
    }

    boolean isUnicodeIdentifierPart(int ch) {
		return false;
    }

    boolean isIdentifierIgnorable(int ch) {
		return false;
    }

    int toLowerCase(int ch) {
		return ch;
    }

    int toUpperCase(int ch) {
		return ch;
    }

    int toTitleCase(int ch) {
		return ch;
    }

    int digit(int ch, int radix) {
		return -1;
    }

    int getNumericValue(int ch) {
		return -1;
    }

    boolean isWhitespace(int ch) {
		return false;
    }

    byte getDirectionality(int ch) {
		return Character.DIRECTIONALITY_UNDEFINED;
    }

    boolean isMirrored(int ch) {
		return false;
    }

    static final CharacterData instance = new CharacterDataUndefined();
    private CharacterDataUndefined() {};
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\CharacterName.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2010, 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.io.DataInputStream;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.Arrays;
import java.util.Locale;
import java.util.zip.InflaterInputStream;
import java.security.AccessController;
import java.security.PrivilegedAction;

class CharacterName {

    private static SoftReference<CharacterName> refCharName;

    // codepoint -> bkIndex -> lookup -> offset/len
    private final byte[] strPool;
    private final int[] lookup;      // code point -> offset/len in strPool
    private final int[] bkIndices;   // code point -> lookup index

    // name -> hash -> hsIndices -> cpEntries -> code point
    private final int[] cpEntries;   // code points that have name in strPool
    private final int[] hsIndices;   // chain heads, hash indices into "cps"

    private CharacterName()  {
        try (DataInputStream dis = new DataInputStream(new InflaterInputStream(
            AccessController.doPrivileged(new PrivilegedAction<>() {
                public InputStream run() {
                    return getClass().getResourceAsStream("uniName.dat");
                }
            })))) {

            int total = dis.readInt();
            int bkNum = dis.readInt();
            int cpNum = dis.readInt();
            int cpEnd = dis.readInt();
            byte ba[] = new byte[cpEnd];
            lookup = new int[bkNum * 256];
            bkIndices = new int[(Character.MAX_CODE_POINT + 1) >> 8];
            strPool = new byte[total - cpEnd];
            cpEntries = new int[cpNum * 3];
            hsIndices = new int[(cpNum / 2) | 1];
            Arrays.fill(bkIndices, -1);
            Arrays.fill(hsIndices, -1);
            dis.readFully(ba);
            dis.readFully(strPool);

            int nameOff = 0;
            int cpOff = 0;
            int cp = 0;
            int bk = -1;
            int prevBk = -1;   // prev bkNo;
            int idx = 0;
            int next = -1;
            int hash = 0;
            int hsh = 0;
            do {
                int len = ba[cpOff++] & 0xff;
                if (len == 0) {
                    len = ba[cpOff++] & 0xff;
                    // always big-endian
                    cp = ((ba[cpOff++] & 0xff) << 16) |
                         ((ba[cpOff++] & 0xff) <<  8) |
                         ((ba[cpOff++] & 0xff));
                }  else {
                    cp++;
                }
                // cp -> name
                int hi = cp >> 8;
                if (prevBk != hi) {
                    bk++;
                    bkIndices[hi] = bk;
                    prevBk = hi;
                }
                lookup[(bk << 8) + (cp & 0xff)] = (nameOff << 8) | len;
                // name -> cp
                hash = hashN(strPool, nameOff, len);
                hsh = (hash & 0x7fffffff) % hsIndices.length;
                next = hsIndices[hsh];
                hsIndices[hsh] = idx;
                idx = addCp(idx, hash, next, cp);
                nameOff += len;
            } while (cpOff < cpEnd);
        } catch (Exception x) {
            throw new InternalError(x.getMessage(), x);
        }
    }

    private static final int hashN(byte[] a, int off, int len) {
        int h = 1;
        while (len-- > 0) {
            h = 31 * h + a[off++];
        }
        return h;
    }

    private int addCp(int idx, int hash, int next, int cp) {
        cpEntries[idx++] = hash;
        cpEntries[idx++] = next;
        cpEntries[idx++] = cp;
        return idx;
    }

    private int getCpHash(int idx) { return cpEntries[idx]; }
    private int getCpNext(int idx) { return cpEntries[idx + 1]; }
    private int getCp(int idx)  { return cpEntries[idx + 2]; }

    public static CharacterName getInstance() {
        SoftReference<CharacterName> ref = refCharName;
        CharacterName cname = null;
        if (ref == null || (cname = ref.get()) == null) {
            cname = new CharacterName();
            refCharName = new SoftReference<>(cname);
        }
        return cname;
    }

    public String getName(int cp) {
        int off = 0;
        int bk = bkIndices[cp >> 8];
        if (bk == -1 || (off = lookup[(bk << 8) + (cp & 0xff)]) == 0)
            return null;
        @SuppressWarnings("deprecation")
        String result = new String(strPool, 0, off >>> 8, off & 0xff);  // ASCII
        return result;
    }

    public int getCodePoint(String name) {
        byte[] bname = name.getBytes(java.nio.charset.StandardCharsets.ISO_8859_1);
        int hsh = hashN(bname, 0, bname.length);
        int idx = hsIndices[(hsh & 0x7fffffff) % hsIndices.length];
        while (idx != -1) {
            if (getCpHash(idx) == hsh) {
                int cp = getCp(idx);
                int off = -1;
                int bk = bkIndices[cp >> 8];
                if (bk != -1 && (off = lookup[(bk << 8) + (cp & 0xff)]) != 0) {
                    int len = off & 0xff;
                    off = off >>> 8;
                    if (bname.length == len) {
                        int i = 0;
                        while (i < len && bname[i] == strPool[off++]) {
                            i++;
                        }
                        if (i == len) {
                            return cp;
                        }
                    }
                 }
            }
            idx = getCpNext(idx);
        }
        return -1;
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\CharSequence.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.PrimitiveIterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

/**
 * A {@code CharSequence} is a readable sequence of {@code char} values. This
 * interface provides uniform, read-only access to many different kinds of
 * {@code char} sequences.
 * A {@code char} value represents a character in the <i>Basic
 * Multilingual Plane (BMP)</i> or a surrogate. Refer to <a
 * href="Character.html#unicode">Unicode Character Representation</a> for details.
 *
 * <p> This interface does not refine the general contracts of the {@link
 * java.lang.Object#equals(java.lang.Object) equals} and {@link
 * java.lang.Object#hashCode() hashCode} methods. The result of testing two objects
 * that implement {@code CharSequence} for equality is therefore, in general, undefined.
 * Each object may be implemented by a different class, and there
 * is no guarantee that each class will be capable of testing its instances
 * for equality with those of the other.  It is therefore inappropriate to use
 * arbitrary {@code CharSequence} instances as elements in a set or as keys in
 * a map. </p>
 *
 * @author Mike McCloskey
 * @since 1.4
 * @spec JSR-51
 */

public interface CharSequence {

    /**
     * Returns the length of this character sequence.  The length is the number
     * of 16-bit {@code char}s in the sequence.
     *
     * @return  the number of {@code char}s in this sequence
     */
    int length();

    /**
     * Returns the {@code char} value at the specified index.  An index ranges from zero
     * to {@code length() - 1}.  The first {@code char} value of the sequence is at
     * index zero, the next at index one, and so on, as for array
     * indexing.
     *
     * <p>If the {@code char} value specified by the index is a
     * <a href="{@docRoot}/java.base/java/lang/Character.html#unicode">surrogate</a>, the surrogate
     * value is returned.
     *
     * @param   index   the index of the {@code char} value to be returned
     *
     * @return  the specified {@code char} value
     *
     * @throws  IndexOutOfBoundsException
     *          if the {@code index} argument is negative or not less than
     *          {@code length()}
     */
    char charAt(int index);

    /**
     * Returns a {@code CharSequence} that is a subsequence of this sequence.
     * The subsequence starts with the {@code char} value at the specified index and
     * ends with the {@code char} value at index {@code end - 1}.  The length
     * (in {@code char}s) of the
     * returned sequence is {@code end - start}, so if {@code start == end}
     * then an empty sequence is returned.
     *
     * @param   start   the start index, inclusive
     * @param   end     the end index, exclusive
     *
     * @return  the specified subsequence
     *
     * @throws  IndexOutOfBoundsException
     *          if {@code start} or {@code end} are negative,
     *          if {@code end} is greater than {@code length()},
     *          or if {@code start} is greater than {@code end}
     */
    CharSequence subSequence(int start, int end);

    /**
     * Returns a string containing the characters in this sequence in the same
     * order as this sequence.  The length of the string will be the length of
     * this sequence.
     *
     * @return  a string consisting of exactly this sequence of characters
     */
    public String toString();

    /**
     * Returns a stream of {@code int} zero-extending the {@code char} values
     * from this sequence.  Any char which maps to a <a
     * href="{@docRoot}/java.base/java/lang/Character.html#unicode">surrogate code
     * point</a> is passed through uninterpreted.
     *
     * <p>The stream binds to this sequence when the terminal stream operation
     * commences (specifically, for mutable sequences the spliterator for the
     * stream is <a href="../util/Spliterator.html#binding"><em>late-binding</em></a>).
     * If the sequence is modified during that operation then the result is
     * undefined.
     *
     * @return an IntStream of char values from this sequence
     * @since 1.8
     */
    public default IntStream chars() {
        class CharIterator implements PrimitiveIterator.OfInt {
            int cur = 0;

            public boolean hasNext() {
                return cur < length();
            }

            public int nextInt() {
                if (hasNext()) {
                    return charAt(cur++);
                } else {
                    throw new NoSuchElementException();
                }
            }

            @Override
            public void forEachRemaining(IntConsumer block) {
                for (; cur < length(); cur++) {
                    block.accept(charAt(cur));
                }
            }
        }

        return StreamSupport.intStream(() ->
                Spliterators.spliterator(
                        new CharIterator(),
                        length(),
                        Spliterator.ORDERED),
                Spliterator.SUBSIZED | Spliterator.SIZED | Spliterator.ORDERED,
                false);
    }

    /**
     * Returns a stream of code point values from this sequence.  Any surrogate
     * pairs encountered in the sequence are combined as if by {@linkplain
     * Character#toCodePoint Character.toCodePoint} and the result is passed
     * to the stream. Any other code units, including ordinary BMP characters,
     * unpaired surrogates, and undefined code units, are zero-extended to
     * {@code int} values which are then passed to the stream.
     *
     * <p>The stream binds to this sequence when the terminal stream operation
     * commences (specifically, for mutable sequences the spliterator for the
     * stream is <a href="../util/Spliterator.html#binding"><em>late-binding</em></a>).
     * If the sequence is modified during that operation then the result is
     * undefined.
     *
     * @return an IntStream of Unicode code points from this sequence
     * @since 1.8
     */
    public default IntStream codePoints() {
        class CodePointIterator implements PrimitiveIterator.OfInt {
            int cur = 0;

            @Override
            public void forEachRemaining(IntConsumer block) {
                final int length = length();
                int i = cur;
                try {
                    while (i < length) {
                        char c1 = charAt(i++);
                        if (!Character.isHighSurrogate(c1) || i >= length) {
                            block.accept(c1);
                        } else {
                            char c2 = charAt(i);
                            if (Character.isLowSurrogate(c2)) {
                                i++;
                                block.accept(Character.toCodePoint(c1, c2));
                            } else {
                                block.accept(c1);
                            }
                        }
                    }
                } finally {
                    cur = i;
                }
            }

            public boolean hasNext() {
                return cur < length();
            }

            public int nextInt() {
                final int length = length();

                if (cur >= length) {
                    throw new NoSuchElementException();
                }
                char c1 = charAt(cur++);
                if (Character.isHighSurrogate(c1) && cur < length) {
                    char c2 = charAt(cur);
                    if (Character.isLowSurrogate(c2)) {
                        cur++;
                        return Character.toCodePoint(c1, c2);
                    }
                }
                return c1;
            }
        }

        return StreamSupport.intStream(() ->
                Spliterators.spliteratorUnknownSize(
                        new CodePointIterator(),
                        Spliterator.ORDERED),
                Spliterator.ORDERED,
                false);
    }

    /**
     * Compares two {@code CharSequence} instances lexicographically. Returns a
     * negative value, zero, or a positive value if the first sequence is lexicographically
     * less than, equal to, or greater than the second, respectively.
     *
     * <p>
     * The lexicographical ordering of {@code CharSequence} is defined as follows.
     * Consider a {@code CharSequence} <i>cs</i> of length <i>len</i> to be a
     * sequence of char values, <i>cs[0]</i> to <i>cs[len-1]</i>. Suppose <i>k</i>
     * is the lowest index at which the corresponding char values from each sequence
     * differ. The lexicographic ordering of the sequences is determined by a numeric
     * comparison of the char values <i>cs1[k]</i> with <i>cs2[k]</i>. If there is
     * no such index <i>k</i>, the shorter sequence is considered lexicographically
     * less than the other. If the sequences have the same length, the sequences are
     * considered lexicographically equal.
     *
     *
     * @param cs1 the first {@code CharSequence}
     * @param cs2 the second {@code CharSequence}
     *
     * @return  the value {@code 0} if the two {@code CharSequence} are equal;
     *          a negative integer if the first {@code CharSequence}
     *          is lexicographically less than the second; or a
     *          positive integer if the first {@code CharSequence} is
     *          lexicographically greater than the second.
     *
     * @since 11
     */
    @SuppressWarnings("unchecked")
    public static int compare(CharSequence cs1, CharSequence cs2) {
        if (Objects.requireNonNull(cs1) == Objects.requireNonNull(cs2)) {
            return 0;
        }

        if (cs1.getClass() == cs2.getClass() && cs1 instanceof Comparable) {
            return ((Comparable<Object>) cs1).compareTo(cs2);
        }

        for (int i = 0, len = Math.min(cs1.length(), cs2.length()); i < len; i++) {
            char a = cs1.charAt(i);
            char b = cs2.charAt(i);
            if (a != b) {
                return a - b;
            }
        }

        return cs1.length() - cs2.length();
    }

}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\Class.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1994, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.lang.annotation.Annotation;
import java.lang.module.ModuleReader;
import java.lang.ref.SoftReference;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectStreamField;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.AnnotatedType;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

import jdk.internal.HotSpotIntrinsicCandidate;
import jdk.internal.loader.BootLoader;
import jdk.internal.loader.BuiltinClassLoader;
import jdk.internal.misc.Unsafe;
import jdk.internal.misc.VM;
import jdk.internal.module.Resources;
import jdk.internal.reflect.CallerSensitive;
import jdk.internal.reflect.ConstantPool;
import jdk.internal.reflect.Reflection;
import jdk.internal.reflect.ReflectionFactory;
import jdk.internal.vm.annotation.ForceInline;
import sun.reflect.generics.factory.CoreReflectionFactory;
import sun.reflect.generics.factory.GenericsFactory;
import sun.reflect.generics.repository.ClassRepository;
import sun.reflect.generics.repository.MethodRepository;
import sun.reflect.generics.repository.ConstructorRepository;
import sun.reflect.generics.scope.ClassScope;
import sun.security.util.SecurityConstants;
import sun.reflect.annotation.*;
import sun.reflect.misc.ReflectUtil;

/**
 * Instances of the class {@code Class} represent classes and interfaces
 * in a running Java application. An enum type is a kind of class and an
 * annotation type is a kind of interface. Every array also
 * belongs to a class that is reflected as a {@code Class} object
 * that is shared by all arrays with the same element type and number
 * of dimensions.  The primitive Java types ({@code boolean},
 * {@code byte}, {@code char}, {@code short},
 * {@code int}, {@code long}, {@code float}, and
 * {@code double}), and the keyword {@code void} are also
 * represented as {@code Class} objects.
 *
 * <p> {@code Class} has no public constructor. Instead a {@code Class}
 * object is constructed automatically by the Java Virtual Machine
 * when a class loader invokes one of the
 * {@link ClassLoader#defineClass(String,byte[], int,int) defineClass} methods
 * and passes the bytes of a {@code class} file.
 *
 * <p> The methods of class {@code Class} expose many characteristics of a
 * class or interface. Most characteristics are derived from the {@code class}
 * file that the class loader passed to the Java Virtual Machine. A few
 * characteristics are determined by the class loading environment at run time,
 * such as the module returned by {@link #getModule() getModule()}.
 *
 * <p> Some methods of class {@code Class} expose whether the declaration of
 * a class or interface in Java source code was <em>enclosed</em> within
 * another declaration. Other methods describe how a class or interface
 * is situated in a <em>nest</em>. A <a id="nest">nest</a> is a set of
 * classes and interfaces, in the same run-time package, that
 * allow mutual access to their {@code private} members.
 * The classes and interfaces are known as <em>nestmates</em>.
 * One nestmate acts as the
 * <em>nest host</em>, and enumerates the other nestmates which
 * belong to the nest; each of them in turn records it as the nest host.
 * The classes and interfaces which belong to a nest, including its host, are
 * determined when
 * {@code class} files are generated, for example, a Java compiler
 * will typically record a top-level class as the host of a nest where the
 * other members are the classes and interfaces whose declarations are
 * enclosed within the top-level class declaration.
 *
 * <p> The following example uses a {@code Class} object to print the
 * class name of an object:
 *
 * <blockquote><pre>
 *     void printClassName(Object obj) {
 *         System.out.println("The class of " + obj +
 *                            " is " + obj.getClass().getName());
 *     }
 * </pre></blockquote>
 *
 * <p> It is also possible to get the {@code Class} object for a named
 * type (or for void) using a class literal.  See Section 15.8.2 of
 * <cite>The Java&trade; Language Specification</cite>.
 * For example:
 *
 * <blockquote>
 *     {@code System.out.println("The name of class Foo is: "+Foo.class.getName());}
 * </blockquote>
 *
 * @param <T> the type of the class modeled by this {@code Class}
 * object.  For example, the type of {@code String.class} is {@code
 * Class<String>}.  Use {@code Class<?>} if the class being modeled is
 * unknown.
 *
 * @author  unascribed
 * @see     java.lang.ClassLoader#defineClass(byte[], int, int)
 * @since   1.0
 */
public final class Class<T> implements java.io.Serializable,
                              GenericDeclaration,
                              Type,
                              AnnotatedElement {
    private static final int ANNOTATION= 0x00002000;
    private static final int ENUM      = 0x00004000;
    private static final int SYNTHETIC = 0x00001000;

    private static native void registerNatives();
    static {
        registerNatives();
    }

    /*
     * Private constructor. Only the Java Virtual Machine creates Class objects.
     * This constructor is not used and prevents the default constructor being
     * generated.
     */
    private Class(ClassLoader loader, Class<?> arrayComponentType) {
        // Initialize final field for classLoader.  The initialization value of non-null
        // prevents future JIT optimizations from assuming this final field is null.
        classLoader = loader;
        componentType = arrayComponentType;
    }

    /**
     * Converts the object to a string. The string representation is the
     * string "class" or "interface", followed by a space, and then by the
     * fully qualified name of the class in the format returned by
     * {@code getName}.  If this {@code Class} object represents a
     * primitive type, this method returns the name of the primitive type.  If
     * this {@code Class} object represents void this method returns
     * "void". If this {@code Class} object represents an array type,
     * this method returns "class " followed by {@code getName}.
     *
     * @return a string representation of this class object.
     */
    public String toString() {
        return (isInterface() ? "interface " : (isPrimitive() ? "" : "class "))
            + getName();
    }

    /**
     * Returns a string describing this {@code Class}, including
     * information about modifiers and type parameters.
     *
     * The string is formatted as a list of type modifiers, if any,
     * followed by the kind of type (empty string for primitive types
     * and {@code class}, {@code enum}, {@code interface}, or
     * <code>&#64;</code>{@code interface}, as appropriate), followed
     * by the type's name, followed by an angle-bracketed
     * comma-separated list of the type's type parameters, if any.
     *
     * A space is used to separate modifiers from one another and to
     * separate any modifiers from the kind of type. The modifiers
     * occur in canonical order. If there are no type parameters, the
     * type parameter list is elided.
     *
     * For an array type, the string starts with the type name,
     * followed by an angle-bracketed comma-separated list of the
     * type's type parameters, if any, followed by a sequence of
     * {@code []} characters, one set of brackets per dimension of
     * the array.
     *
     * <p>Note that since information about the runtime representation
     * of a type is being generated, modifiers not present on the
     * originating source code or illegal on the originating source
     * code may be present.
     *
     * @return a string describing this {@code Class}, including
     * information about modifiers and type parameters
     *
     * @since 1.8
     */
    public String toGenericString() {
        if (isPrimitive()) {
            return toString();
        } else {
            StringBuilder sb = new StringBuilder();
            Class<?> component = this;
            int arrayDepth = 0;

            if (isArray()) {
                do {
                    arrayDepth++;
                    component = component.getComponentType();
                } while (component.isArray());
                sb.append(component.getName());
            } else {
                // Class modifiers are a superset of interface modifiers
                int modifiers = getModifiers() & Modifier.classModifiers();
                if (modifiers != 0) {
                    sb.append(Modifier.toString(modifiers));
                    sb.append(' ');
                }

                if (isAnnotation()) {
                    sb.append('@');
                }
                if (isInterface()) { // Note: all annotation types are interfaces
                    sb.append("interface");
                } else {
                    if (isEnum())
                        sb.append("enum");
                    else
                        sb.append("class");
                }
                sb.append(' ');
                sb.append(getName());
            }

            TypeVariable<?>[] typeparms = component.getTypeParameters();
            if (typeparms.length > 0) {
                StringJoiner sj = new StringJoiner(",", "<", ">");
                for(TypeVariable<?> typeparm: typeparms) {
                    sj.add(typeparm.getTypeName());
                }
                sb.append(sj.toString());
            }

            for (int i = 0; i < arrayDepth; i++)
                sb.append("[]");

            return sb.toString();
        }
    }

    /**
     * Returns the {@code Class} object associated with the class or
     * interface with the given string name.  Invoking this method is
     * equivalent to:
     *
     * <blockquote>
     *  {@code Class.forName(className, true, currentLoader)}
     * </blockquote>
     *
     * where {@code currentLoader} denotes the defining class loader of
     * the current class.
     *
     * <p> For example, the following code fragment returns the
     * runtime {@code Class} descriptor for the class named
     * {@code java.lang.Thread}:
     *
     * <blockquote>
     *   {@code Class t = Class.forName("java.lang.Thread")}
     * </blockquote>
     * <p>
     * A call to {@code forName("X")} causes the class named
     * {@code X} to be initialized.
     *
     * @param      className   the fully qualified name of the desired class.
     * @return     the {@code Class} object for the class with the
     *             specified name.
     * @exception LinkageError if the linkage fails
     * @exception ExceptionInInitializerError if the initialization provoked
     *            by this method fails
     * @exception ClassNotFoundException if the class cannot be located
     */
    @CallerSensitive
    public static Class<?> forName(String className)
                throws ClassNotFoundException {
        Class<?> caller = Reflection.getCallerClass();
        return forName0(className, true, ClassLoader.getClassLoader(caller), caller);
    }


    /**
     * Returns the {@code Class} object associated with the class or
     * interface with the given string name, using the given class loader.
     * Given the fully qualified name for a class or interface (in the same
     * format returned by {@code getName}) this method attempts to
     * locate, load, and link the class or interface.  The specified class
     * loader is used to load the class or interface.  If the parameter
     * {@code loader} is null, the class is loaded through the bootstrap
     * class loader.  The class is initialized only if the
     * {@code initialize} parameter is {@code true} and if it has
     * not been initialized earlier.
     *
     * <p> If {@code name} denotes a primitive type or void, an attempt
     * will be made to locate a user-defined class in the unnamed package whose
     * name is {@code name}. Therefore, this method cannot be used to
     * obtain any of the {@code Class} objects representing primitive
     * types or void.
     *
     * <p> If {@code name} denotes an array class, the component type of
     * the array class is loaded but not initialized.
     *
     * <p> For example, in an instance method the expression:
     *
     * <blockquote>
     *  {@code Class.forName("Foo")}
     * </blockquote>
     *
     * is equivalent to:
     *
     * <blockquote>
     *  {@code Class.forName("Foo", true, this.getClass().getClassLoader())}
     * </blockquote>
     *
     * Note that this method throws errors related to loading, linking or
     * initializing as specified in Sections 12.2, 12.3 and 12.4 of <em>The
     * Java Language Specification</em>.
     * Note that this method does not check whether the requested class
     * is accessible to its caller.
     *
     * @param name       fully qualified name of the desired class
     * @param initialize if {@code true} the class will be initialized.
     *                   See Section 12.4 of <em>The Java Language Specification</em>.
     * @param loader     class loader from which the class must be loaded
     * @return           class object representing the desired class
     *
     * @exception LinkageError if the linkage fails
     * @exception ExceptionInInitializerError if the initialization provoked
     *            by this method fails
     * @exception ClassNotFoundException if the class cannot be located by
     *            the specified class loader
     * @exception SecurityException
     *            if a security manager is present, and the {@code loader} is
     *            {@code null}, and the caller's class loader is not
     *            {@code null}, and the caller does not have the
     *            {@link RuntimePermission}{@code ("getClassLoader")}
     *
     * @see       java.lang.Class#forName(String)
     * @see       java.lang.ClassLoader
     * @since     1.2
     */
    @CallerSensitive
    public static Class<?> forName(String name, boolean initialize,
                                   ClassLoader loader)
        throws ClassNotFoundException
    {
        Class<?> caller = null;
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            // Reflective call to get caller class is only needed if a security manager
            // is present.  Avoid the overhead of making this call otherwise.
            caller = Reflection.getCallerClass();
            if (loader == null) {
                ClassLoader ccl = ClassLoader.getClassLoader(caller);
                if (ccl != null) {
                    sm.checkPermission(
                        SecurityConstants.GET_CLASSLOADER_PERMISSION);
                }
            }
        }
        return forName0(name, initialize, loader, caller);
    }

    /** Called after security check for system loader access checks have been made. */
    private static native Class<?> forName0(String name, boolean initialize,
                                            ClassLoader loader,
                                            Class<?> caller)
        throws ClassNotFoundException;


    /**
     * Returns the {@code Class} with the given <a href="ClassLoader.html#name">
     * binary name</a> in the given module.
     *
     * <p> This method attempts to locate, load, and link the class or interface.
     * It does not run the class initializer.  If the class is not found, this
     * method returns {@code null}. </p>
     *
     * <p> If the class loader of the given module defines other modules and
     * the given name is a class defined in a different module, this method
     * returns {@code null} after the class is loaded. </p>
     *
     * <p> This method does not check whether the requested class is
     * accessible to its caller. </p>
     *
     * @apiNote
     * This method returns {@code null} on failure rather than
     * throwing a {@link ClassNotFoundException}, as is done by
     * the {@link #forName(String, boolean, ClassLoader)} method.
     * The security check is a stack-based permission check if the caller
     * loads a class in another module.
     *
     * @param  module   A module
     * @param  name     The <a href="ClassLoader.html#name">binary name</a>
     *                  of the class
     * @return {@code Class} object of the given name defined in the given module;
     *         {@code null} if not found.
     *
     * @throws NullPointerException if the given module or name is {@code null}
     *
     * @throws LinkageError if the linkage fails
     *
     * @throws SecurityException
     *         <ul>
     *         <li> if the caller is not the specified module and
     *         {@code RuntimePermission("getClassLoader")} permission is denied; or</li>
     *         <li> access to the module content is denied. For example,
     *         permission check will be performed when a class loader calls
     *         {@link ModuleReader#open(String)} to read the bytes of a class file
     *         in a module.</li>
     *         </ul>
     *
     * @since 9
     * @spec JPMS
     */
    @CallerSensitive
    public static Class<?> forName(Module module, String name) {
        Objects.requireNonNull(module);
        Objects.requireNonNull(name);

        ClassLoader cl;
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            Class<?> caller = Reflection.getCallerClass();
            if (caller != null && caller.getModule() != module) {
                // if caller is null, Class.forName is the last java frame on the stack.
                // java.base has all permissions
                sm.checkPermission(SecurityConstants.GET_CLASSLOADER_PERMISSION);
            }
            PrivilegedAction<ClassLoader> pa = module::getClassLoader;
            cl = AccessController.doPrivileged(pa);
        } else {
            cl = module.getClassLoader();
        }

        if (cl != null) {
            return cl.loadClass(module, name);
        } else {
            return BootLoader.loadClass(module, name);
        }
    }

    /**
     * Creates a new instance of the class represented by this {@code Class}
     * object.  The class is instantiated as if by a {@code new}
     * expression with an empty argument list.  The class is initialized if it
     * has not already been initialized.
     *
     * @deprecated This method propagates any exception thrown by the
     * nullary constructor, including a checked exception.  Use of
     * this method effectively bypasses the compile-time exception
     * checking that would otherwise be performed by the compiler.
     * The {@link
     * java.lang.reflect.Constructor#newInstance(java.lang.Object...)
     * Constructor.newInstance} method avoids this problem by wrapping
     * any exception thrown by the constructor in a (checked) {@link
     * java.lang.reflect.InvocationTargetException}.
     *
     * <p>The call
     *
     * <pre>{@code
     * clazz.newInstance()
     * }</pre>
     *
     * can be replaced by
     *
     * <pre>{@code
     * clazz.getDeclaredConstructor().newInstance()
     * }</pre>
     *
     * The latter sequence of calls is inferred to be able to throw
     * the additional exception types {@link
     * InvocationTargetException} and {@link
     * NoSuchMethodException}. Both of these exception types are
     * subclasses of {@link ReflectiveOperationException}.
     *
     * @return  a newly allocated instance of the class represented by this
     *          object.
     * @throws  IllegalAccessException  if the class or its nullary
     *          constructor is not accessible.
     * @throws  InstantiationException
     *          if this {@code Class} represents an abstract class,
     *          an interface, an array class, a primitive type, or void;
     *          or if the class has no nullary constructor;
     *          or if the instantiation fails for some other reason.
     * @throws  ExceptionInInitializerError if the initialization
     *          provoked by this method fails.
     * @throws  SecurityException
     *          If a security manager, <i>s</i>, is present and
     *          the caller's class loader is not the same as or an
     *          ancestor of the class loader for the current class and
     *          invocation of {@link SecurityManager#checkPackageAccess
     *          s.checkPackageAccess()} denies access to the package
     *          of this class.
     */
    @CallerSensitive
    @Deprecated(since="9")
    public T newInstance()
        throws InstantiationException, IllegalAccessException
    {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.PUBLIC, Reflection.getCallerClass(), false);
        }

        // NOTE: the following code may not be strictly correct under
        // the current Java memory model.

        // Constructor lookup
        if (cachedConstructor == null) {
            if (this == Class.class) {
                throw new IllegalAccessException(
                    "Can not call newInstance() on the Class for java.lang.Class"
                );
            }
            try {
                Class<?>[] empty = {};
                final Constructor<T> c = getReflectionFactory().copyConstructor(
                    getConstructor0(empty, Member.DECLARED));
                // Disable accessibility checks on the constructor
                // since we have to do the security check here anyway
                // (the stack depth is wrong for the Constructor's
                // security check to work)
                java.security.AccessController.doPrivileged(
                    new java.security.PrivilegedAction<>() {
                        public Void run() {
                                c.setAccessible(true);
                                return null;
                            }
                        });
                cachedConstructor = c;
            } catch (NoSuchMethodException e) {
                throw (InstantiationException)
                    new InstantiationException(getName()).initCause(e);
            }
        }
        Constructor<T> tmpConstructor = cachedConstructor;
        // Security check (same as in java.lang.reflect.Constructor)
        Class<?> caller = Reflection.getCallerClass();
        if (newInstanceCallerCache != caller) {
            int modifiers = tmpConstructor.getModifiers();
            Reflection.ensureMemberAccess(caller, this, this, modifiers);
            newInstanceCallerCache = caller;
        }
        // Run constructor
        try {
            return tmpConstructor.newInstance((Object[])null);
        } catch (InvocationTargetException e) {
            Unsafe.getUnsafe().throwException(e.getTargetException());
            // Not reached
            return null;
        }
    }
    private transient volatile Constructor<T> cachedConstructor;
    private transient volatile Class<?>       newInstanceCallerCache;


    /**
     * Determines if the specified {@code Object} is assignment-compatible
     * with the object represented by this {@code Class}.  This method is
     * the dynamic equivalent of the Java language {@code instanceof}
     * operator. The method returns {@code true} if the specified
     * {@code Object} argument is non-null and can be cast to the
     * reference type represented by this {@code Class} object without
     * raising a {@code ClassCastException.} It returns {@code false}
     * otherwise.
     *
     * <p> Specifically, if this {@code Class} object represents a
     * declared class, this method returns {@code true} if the specified
     * {@code Object} argument is an instance of the represented class (or
     * of any of its subclasses); it returns {@code false} otherwise. If
     * this {@code Class} object represents an array class, this method
     * returns {@code true} if the specified {@code Object} argument
     * can be converted to an object of the array class by an identity
     * conversion or by a widening reference conversion; it returns
     * {@code false} otherwise. If this {@code Class} object
     * represents an interface, this method returns {@code true} if the
     * class or any superclass of the specified {@code Object} argument
     * implements this interface; it returns {@code false} otherwise. If
     * this {@code Class} object represents a primitive type, this method
     * returns {@code false}.
     *
     * @param   obj the object to check
     * @return  true if {@code obj} is an instance of this class
     *
     * @since 1.1
     */
    @HotSpotIntrinsicCandidate
    public native boolean isInstance(Object obj);


    /**
     * Determines if the class or interface represented by this
     * {@code Class} object is either the same as, or is a superclass or
     * superinterface of, the class or interface represented by the specified
     * {@code Class} parameter. It returns {@code true} if so;
     * otherwise it returns {@code false}. If this {@code Class}
     * object represents a primitive type, this method returns
     * {@code true} if the specified {@code Class} parameter is
     * exactly this {@code Class} object; otherwise it returns
     * {@code false}.
     *
     * <p> Specifically, this method tests whether the type represented by the
     * specified {@code Class} parameter can be converted to the type
     * represented by this {@code Class} object via an identity conversion
     * or via a widening reference conversion. See <em>The Java Language
     * Specification</em>, sections 5.1.1 and 5.1.4 , for details.
     *
     * @param cls the {@code Class} object to be checked
     * @return the {@code boolean} value indicating whether objects of the
     * type {@code cls} can be assigned to objects of this class
     * @exception NullPointerException if the specified Class parameter is
     *            null.
     * @since 1.1
     */
    @HotSpotIntrinsicCandidate
    public native boolean isAssignableFrom(Class<?> cls);


    /**
     * Determines if the specified {@code Class} object represents an
     * interface type.
     *
     * @return  {@code true} if this object represents an interface;
     *          {@code false} otherwise.
     */
    @HotSpotIntrinsicCandidate
    public native boolean isInterface();


    /**
     * Determines if this {@code Class} object represents an array class.
     *
     * @return  {@code true} if this object represents an array class;
     *          {@code false} otherwise.
     * @since   1.1
     */
    @HotSpotIntrinsicCandidate
    public native boolean isArray();


    /**
     * Determines if the specified {@code Class} object represents a
     * primitive type.
     *
     * <p> There are nine predefined {@code Class} objects to represent
     * the eight primitive types and void.  These are created by the Java
     * Virtual Machine, and have the same names as the primitive types that
     * they represent, namely {@code boolean}, {@code byte},
     * {@code char}, {@code short}, {@code int},
     * {@code long}, {@code float}, and {@code double}.
     *
     * <p> These objects may only be accessed via the following public static
     * final variables, and are the only {@code Class} objects for which
     * this method returns {@code true}.
     *
     * @return true if and only if this class represents a primitive type
     *
     * @see     java.lang.Boolean#TYPE
     * @see     java.lang.Character#TYPE
     * @see     java.lang.Byte#TYPE
     * @see     java.lang.Short#TYPE
     * @see     java.lang.Integer#TYPE
     * @see     java.lang.Long#TYPE
     * @see     java.lang.Float#TYPE
     * @see     java.lang.Double#TYPE
     * @see     java.lang.Void#TYPE
     * @since 1.1
     */
    @HotSpotIntrinsicCandidate
    public native boolean isPrimitive();

    /**
     * Returns true if this {@code Class} object represents an annotation
     * type.  Note that if this method returns true, {@link #isInterface()}
     * would also return true, as all annotation types are also interfaces.
     *
     * @return {@code true} if this class object represents an annotation
     *      type; {@code false} otherwise
     * @since 1.5
     */
    public boolean isAnnotation() {
        return (getModifiers() & ANNOTATION) != 0;
    }

    /**
     * Returns {@code true} if this class is a synthetic class;
     * returns {@code false} otherwise.
     * @return {@code true} if and only if this class is a synthetic class as
     *         defined by the Java Language Specification.
     * @jls 13.1 The Form of a Binary
     * @since 1.5
     */
    public boolean isSynthetic() {
        return (getModifiers() & SYNTHETIC) != 0;
    }

    /**
     * Returns the  name of the entity (class, interface, array class,
     * primitive type, or void) represented by this {@code Class} object,
     * as a {@code String}.
     *
     * <p> If this class object represents a reference type that is not an
     * array type then the binary name of the class is returned, as specified
     * by
     * <cite>The Java&trade; Language Specification</cite>.
     *
     * <p> If this class object represents a primitive type or void, then the
     * name returned is a {@code String} equal to the Java language
     * keyword corresponding to the primitive type or void.
     *
     * <p> If this class object represents a class of arrays, then the internal
     * form of the name consists of the name of the element type preceded by
     * one or more '{@code [}' characters representing the depth of the array
     * nesting.  The encoding of element type names is as follows:
     *
     * <blockquote><table class="striped">
     * <caption style="display:none">Element types and encodings</caption>
     * <thead>
     * <tr><th scope="col"> Element Type <th scope="col"> Encoding
     * </thead>
     * <tbody style="text-align:left">
     * <tr><th scope="row"> boolean      <td style="text-align:center"> Z
     * <tr><th scope="row"> byte         <td style="text-align:center"> B
     * <tr><th scope="row"> char         <td style="text-align:center"> C
     * <tr><th scope="row"> class or interface
     *                                   <td style="text-align:center"> L<i>classname</i>;
     * <tr><th scope="row"> double       <td style="text-align:center"> D
     * <tr><th scope="row"> float        <td style="text-align:center"> F
     * <tr><th scope="row"> int          <td style="text-align:center"> I
     * <tr><th scope="row"> long         <td style="text-align:center"> J
     * <tr><th scope="row"> short        <td style="text-align:center"> S
     * </tbody>
     * </table></blockquote>
     *
     * <p> The class or interface name <i>classname</i> is the binary name of
     * the class specified above.
     *
     * <p> Examples:
     * <blockquote><pre>
     * String.class.getName()
     *     returns "java.lang.String"
     * byte.class.getName()
     *     returns "byte"
     * (new Object[3]).getClass().getName()
     *     returns "[Ljava.lang.Object;"
     * (new int[3][4][5][6][7][8][9]).getClass().getName()
     *     returns "[[[[[[[I"
     * </pre></blockquote>
     *
     * @return  the name of the class or interface
     *          represented by this object.
     */
    public String getName() {
        String name = this.name;
        if (name == null)
            this.name = name = getName0();
        return name;
    }

    // cache the name to reduce the number of calls into the VM
    private transient String name;
    private native String getName0();

    /**
     * Returns the class loader for the class.  Some implementations may use
     * null to represent the bootstrap class loader. This method will return
     * null in such implementations if this class was loaded by the bootstrap
     * class loader.
     *
     * <p>If this object
     * represents a primitive type or void, null is returned.
     *
     * @return  the class loader that loaded the class or interface
     *          represented by this object.
     * @throws  SecurityException
     *          if a security manager is present, and the caller's class loader
     *          is not {@code null} and is not the same as or an ancestor of the
     *          class loader for the class whose class loader is requested,
     *          and the caller does not have the
     *          {@link RuntimePermission}{@code ("getClassLoader")}
     * @see java.lang.ClassLoader
     * @see SecurityManager#checkPermission
     * @see java.lang.RuntimePermission
     */
    @CallerSensitive
    @ForceInline // to ensure Reflection.getCallerClass optimization
    public ClassLoader getClassLoader() {
        ClassLoader cl = getClassLoader0();
        if (cl == null)
            return null;
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            ClassLoader.checkClassLoaderPermission(cl, Reflection.getCallerClass());
        }
        return cl;
    }

    // Package-private to allow ClassLoader access
    ClassLoader getClassLoader0() { return classLoader; }

    /**
     * Returns the module that this class or interface is a member of.
     *
     * If this class represents an array type then this method returns the
     * {@code Module} for the element type. If this class represents a
     * primitive type or void, then the {@code Module} object for the
     * {@code java.base} module is returned.
     *
     * If this class is in an unnamed module then the {@linkplain
     * ClassLoader#getUnnamedModule() unnamed} {@code Module} of the class
     * loader for this class is returned.
     *
     * @return the module that this class or interface is a member of
     *
     * @since 9
     * @spec JPMS
     */
    public Module getModule() {
        return module;
    }

    // set by VM
    private transient Module module;

    // Initialized in JVM not by private constructor
    // This field is filtered from reflection access, i.e. getDeclaredField
    // will throw NoSuchFieldException
    private final ClassLoader classLoader;

    /**
     * Returns an array of {@code TypeVariable} objects that represent the
     * type variables declared by the generic declaration represented by this
     * {@code GenericDeclaration} object, in declaration order.  Returns an
     * array of length 0 if the underlying generic declaration declares no type
     * variables.
     *
     * @return an array of {@code TypeVariable} objects that represent
     *     the type variables declared by this generic declaration
     * @throws java.lang.reflect.GenericSignatureFormatError if the generic
     *     signature of this generic declaration does not conform to
     *     the format specified in
     *     <cite>The Java&trade; Virtual Machine Specification</cite>
     * @since 1.5
     */
    @SuppressWarnings("unchecked")
    public TypeVariable<Class<T>>[] getTypeParameters() {
        ClassRepository info = getGenericInfo();
        if (info != null)
            return (TypeVariable<Class<T>>[])info.getTypeParameters();
        else
            return (TypeVariable<Class<T>>[])new TypeVariable<?>[0];
    }


    /**
     * Returns the {@code Class} representing the direct superclass of the
     * entity (class, interface, primitive type or void) represented by
     * this {@code Class}.  If this {@code Class} represents either the
     * {@code Object} class, an interface, a primitive type, or void, then
     * null is returned.  If this object represents an array class then the
     * {@code Class} object representing the {@code Object} class is
     * returned.
     *
     * @return the direct superclass of the class represented by this object
     */
    @HotSpotIntrinsicCandidate
    public native Class<? super T> getSuperclass();


    /**
     * Returns the {@code Type} representing the direct superclass of
     * the entity (class, interface, primitive type or void) represented by
     * this {@code Class}.
     *
     * <p>If the superclass is a parameterized type, the {@code Type}
     * object returned must accurately reflect the actual type
     * parameters used in the source code. The parameterized type
     * representing the superclass is created if it had not been
     * created before. See the declaration of {@link
     * java.lang.reflect.ParameterizedType ParameterizedType} for the
     * semantics of the creation process for parameterized types.  If
     * this {@code Class} represents either the {@code Object}
     * class, an interface, a primitive type, or void, then null is
     * returned.  If this object represents an array class then the
     * {@code Class} object representing the {@code Object} class is
     * returned.
     *
     * @throws java.lang.reflect.GenericSignatureFormatError if the generic
     *     class signature does not conform to the format specified in
     *     <cite>The Java&trade; Virtual Machine Specification</cite>
     * @throws TypeNotPresentException if the generic superclass
     *     refers to a non-existent type declaration
     * @throws java.lang.reflect.MalformedParameterizedTypeException if the
     *     generic superclass refers to a parameterized type that cannot be
     *     instantiated  for any reason
     * @return the direct superclass of the class represented by this object
     * @since 1.5
     */
    public Type getGenericSuperclass() {
        ClassRepository info = getGenericInfo();
        if (info == null) {
            return getSuperclass();
        }

        // Historical irregularity:
        // Generic signature marks interfaces with superclass = Object
        // but this API returns null for interfaces
        if (isInterface()) {
            return null;
        }

        return info.getSuperclass();
    }

    /**
     * Gets the package of this class.
     *
     * <p>If this class represents an array type, a primitive type or void,
     * this method returns {@code null}.
     *
     * @return the package of this class.
     * @revised 9
     * @spec JPMS
     */
    public Package getPackage() {
        if (isPrimitive() || isArray()) {
            return null;
        }
        ClassLoader cl = getClassLoader0();
        return cl != null ? cl.definePackage(this)
                          : BootLoader.definePackage(this);
    }

    /**
     * Returns the fully qualified package name.
     *
     * <p> If this class is a top level class, then this method returns the fully
     * qualified name of the package that the class is a member of, or the
     * empty string if the class is in an unnamed package.
     *
     * <p> If this class is a member class, then this method is equivalent to
     * invoking {@code getPackageName()} on the {@linkplain #getEnclosingClass
     * enclosing class}.
     *
     * <p> If this class is a {@linkplain #isLocalClass local class} or an {@linkplain
     * #isAnonymousClass() anonymous class}, then this method is equivalent to
     * invoking {@code getPackageName()} on the {@linkplain #getDeclaringClass
     * declaring class} of the {@linkplain #getEnclosingMethod enclosing method} or
     * {@linkplain #getEnclosingConstructor enclosing constructor}.
     *
     * <p> If this class represents an array type then this method returns the
     * package name of the element type. If this class represents a primitive
     * type or void then the package name "{@code java.lang}" is returned.
     *
     * @return the fully qualified package name
     *
     * @since 9
     * @spec JPMS
     * @jls 6.7  Fully Qualified Names
     */
    public String getPackageName() {
        String pn = this.packageName;
        if (pn == null) {
            Class<?> c = this;
            while (c.isArray()) {
                c = c.getComponentType();
            }
            if (c.isPrimitive()) {
                pn = "java.lang";
            } else {
                String cn = c.getName();
                int dot = cn.lastIndexOf('.');
                pn = (dot != -1) ? cn.substring(0, dot).intern() : "";
            }
            this.packageName = pn;
        }
        return pn;
    }

    // cached package name
    private transient String packageName;

    /**
     * Returns the interfaces directly implemented by the class or interface
     * represented by this object.
     *
     * <p>If this object represents a class, the return value is an array
     * containing objects representing all interfaces directly implemented by
     * the class.  The order of the interface objects in the array corresponds
     * to the order of the interface names in the {@code implements} clause of
     * the declaration of the class represented by this object.  For example,
     * given the declaration:
     * <blockquote>
     * {@code class Shimmer implements FloorWax, DessertTopping { ... }}
     * </blockquote>
     * suppose the value of {@code s} is an instance of
     * {@code Shimmer}; the value of the expression:
     * <blockquote>
     * {@code s.getClass().getInterfaces()[0]}
     * </blockquote>
     * is the {@code Class} object that represents interface
     * {@code FloorWax}; and the value of:
     * <blockquote>
     * {@code s.getClass().getInterfaces()[1]}
     * </blockquote>
     * is the {@code Class} object that represents interface
     * {@code DessertTopping}.
     *
     * <p>If this object represents an interface, the array contains objects
     * representing all interfaces directly extended by the interface.  The
     * order of the interface objects in the array corresponds to the order of
     * the interface names in the {@code extends} clause of the declaration of
     * the interface represented by this object.
     *
     * <p>If this object represents a class or interface that implements no
     * interfaces, the method returns an array of length 0.
     *
     * <p>If this object represents a primitive type or void, the method
     * returns an array of length 0.
     *
     * <p>If this {@code Class} object represents an array type, the
     * interfaces {@code Cloneable} and {@code java.io.Serializable} are
     * returned in that order.
     *
     * @return an array of interfaces directly implemented by this class
     */
    public Class<?>[] getInterfaces() {
        // defensively copy before handing over to user code
        return getInterfaces(true);
    }

    private Class<?>[] getInterfaces(boolean cloneArray) {
        ReflectionData<T> rd = reflectionData();
        if (rd == null) {
            // no cloning required
            return getInterfaces0();
        } else {
            Class<?>[] interfaces = rd.interfaces;
            if (interfaces == null) {
                interfaces = getInterfaces0();
                rd.interfaces = interfaces;
            }
            // defensively copy if requested
            return cloneArray ? interfaces.clone() : interfaces;
        }
    }

    private native Class<?>[] getInterfaces0();

    /**
     * Returns the {@code Type}s representing the interfaces
     * directly implemented by the class or interface represented by
     * this object.
     *
     * <p>If a superinterface is a parameterized type, the
     * {@code Type} object returned for it must accurately reflect
     * the actual type parameters used in the source code. The
     * parameterized type representing each superinterface is created
     * if it had not been created before. See the declaration of
     * {@link java.lang.reflect.ParameterizedType ParameterizedType}
     * for the semantics of the creation process for parameterized
     * types.
     *
     * <p>If this object represents a class, the return value is an array
     * containing objects representing all interfaces directly implemented by
     * the class.  The order of the interface objects in the array corresponds
     * to the order of the interface names in the {@code implements} clause of
     * the declaration of the class represented by this object.
     *
     * <p>If this object represents an interface, the array contains objects
     * representing all interfaces directly extended by the interface.  The
     * order of the interface objects in the array corresponds to the order of
     * the interface names in the {@code extends} clause of the declaration of
     * the interface represented by this object.
     *
     * <p>If this object represents a class or interface that implements no
     * interfaces, the method returns an array of length 0.
     *
     * <p>If this object represents a primitive type or void, the method
     * returns an array of length 0.
     *
     * <p>If this {@code Class} object represents an array type, the
     * interfaces {@code Cloneable} and {@code java.io.Serializable} are
     * returned in that order.
     *
     * @throws java.lang.reflect.GenericSignatureFormatError
     *     if the generic class signature does not conform to the format
     *     specified in
     *     <cite>The Java&trade; Virtual Machine Specification</cite>
     * @throws TypeNotPresentException if any of the generic
     *     superinterfaces refers to a non-existent type declaration
     * @throws java.lang.reflect.MalformedParameterizedTypeException
     *     if any of the generic superinterfaces refer to a parameterized
     *     type that cannot be instantiated for any reason
     * @return an array of interfaces directly implemented by this class
     * @since 1.5
     */
    public Type[] getGenericInterfaces() {
        ClassRepository info = getGenericInfo();
        return (info == null) ?  getInterfaces() : info.getSuperInterfaces();
    }


    /**
     * Returns the {@code Class} representing the component type of an
     * array.  If this class does not represent an array class this method
     * returns null.
     *
     * @return the {@code Class} representing the component type of this
     * class if this class is an array
     * @see     java.lang.reflect.Array
     * @since 1.1
     */
    public Class<?> getComponentType() {
        // Only return for array types. Storage may be reused for Class for instance types.
        if (isArray()) {
            return componentType;
        } else {
            return null;
        }
    }

    private final Class<?> componentType;


    /**
     * Returns the Java language modifiers for this class or interface, encoded
     * in an integer. The modifiers consist of the Java Virtual Machine's
     * constants for {@code public}, {@code protected},
     * {@code private}, {@code final}, {@code static},
     * {@code abstract} and {@code interface}; they should be decoded
     * using the methods of class {@code Modifier}.
     *
     * <p> If the underlying class is an array class, then its
     * {@code public}, {@code private} and {@code protected}
     * modifiers are the same as those of its component type.  If this
     * {@code Class} represents a primitive type or void, its
     * {@code public} modifier is always {@code true}, and its
     * {@code protected} and {@code private} modifiers are always
     * {@code false}. If this object represents an array class, a
     * primitive type or void, then its {@code final} modifier is always
     * {@code true} and its interface modifier is always
     * {@code false}. The values of its other modifiers are not determined
     * by this specification.
     *
     * <p> The modifier encodings are defined in <em>The Java Virtual Machine
     * Specification</em>, table 4.1.
     *
     * @return the {@code int} representing the modifiers for this class
     * @see     java.lang.reflect.Modifier
     * @since 1.1
     */
    @HotSpotIntrinsicCandidate
    public native int getModifiers();


    /**
     * Gets the signers of this class.
     *
     * @return  the signers of this class, or null if there are no signers.  In
     *          particular, this method returns null if this object represents
     *          a primitive type or void.
     * @since   1.1
     */
    public native Object[] getSigners();


    /**
     * Set the signers of this class.
     */
    native void setSigners(Object[] signers);


    /**
     * If this {@code Class} object represents a local or anonymous
     * class within a method, returns a {@link
     * java.lang.reflect.Method Method} object representing the
     * immediately enclosing method of the underlying class. Returns
     * {@code null} otherwise.
     *
     * In particular, this method returns {@code null} if the underlying
     * class is a local or anonymous class immediately enclosed by a type
     * declaration, instance initializer or static initializer.
     *
     * @return the immediately enclosing method of the underlying class, if
     *     that class is a local or anonymous class; otherwise {@code null}.
     *
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and any of the
     *         following conditions is met:
     *
     *         <ul>
     *
     *         <li> the caller's class loader is not the same as the
     *         class loader of the enclosing class and invocation of
     *         {@link SecurityManager#checkPermission
     *         s.checkPermission} method with
     *         {@code RuntimePermission("accessDeclaredMembers")}
     *         denies access to the methods within the enclosing class
     *
     *         <li> the caller's class loader is not the same as or an
     *         ancestor of the class loader for the enclosing class and
     *         invocation of {@link SecurityManager#checkPackageAccess
     *         s.checkPackageAccess()} denies access to the package
     *         of the enclosing class
     *
     *         </ul>
     * @since 1.5
     */
    @CallerSensitive
    public Method getEnclosingMethod() throws SecurityException {
        EnclosingMethodInfo enclosingInfo = getEnclosingMethodInfo();

        if (enclosingInfo == null)
            return null;
        else {
            if (!enclosingInfo.isMethod())
                return null;

            MethodRepository typeInfo = MethodRepository.make(enclosingInfo.getDescriptor(),
                                                              getFactory());
            Class<?>   returnType       = toClass(typeInfo.getReturnType());
            Type []    parameterTypes   = typeInfo.getParameterTypes();
            Class<?>[] parameterClasses = new Class<?>[parameterTypes.length];

            // Convert Types to Classes; returned types *should*
            // be class objects since the methodDescriptor's used
            // don't have generics information
            for(int i = 0; i < parameterClasses.length; i++)
                parameterClasses[i] = toClass(parameterTypes[i]);

            // Perform access check
            final Class<?> enclosingCandidate = enclosingInfo.getEnclosingClass();
            SecurityManager sm = System.getSecurityManager();
            if (sm != null) {
                enclosingCandidate.checkMemberAccess(sm, Member.DECLARED,
                                                     Reflection.getCallerClass(), true);
            }
            Method[] candidates = enclosingCandidate.privateGetDeclaredMethods(false);

            /*
             * Loop over all declared methods; match method name,
             * number of and type of parameters, *and* return
             * type.  Matching return type is also necessary
             * because of covariant returns, etc.
             */
            ReflectionFactory fact = getReflectionFactory();
            for (Method m : candidates) {
                if (m.getName().equals(enclosingInfo.getName()) &&
                    arrayContentsEq(parameterClasses,
                                    fact.getExecutableSharedParameterTypes(m))) {
                    // finally, check return type
                    if (m.getReturnType().equals(returnType)) {
                        return fact.copyMethod(m);
                    }
                }
            }

            throw new InternalError("Enclosing method not found");
        }
    }

    private native Object[] getEnclosingMethod0();

    private EnclosingMethodInfo getEnclosingMethodInfo() {
        Object[] enclosingInfo = getEnclosingMethod0();
        if (enclosingInfo == null)
            return null;
        else {
            return new EnclosingMethodInfo(enclosingInfo);
        }
    }

    private static final class EnclosingMethodInfo {
        private final Class<?> enclosingClass;
        private final String name;
        private final String descriptor;

        static void validate(Object[] enclosingInfo) {
            if (enclosingInfo.length != 3)
                throw new InternalError("Malformed enclosing method information");
            try {
                // The array is expected to have three elements:

                // the immediately enclosing class
                Class<?> enclosingClass = (Class<?>)enclosingInfo[0];
                assert(enclosingClass != null);

                // the immediately enclosing method or constructor's
                // name (can be null).
                String name = (String)enclosingInfo[1];

                // the immediately enclosing method or constructor's
                // descriptor (null iff name is).
                String descriptor = (String)enclosingInfo[2];
                assert((name != null && descriptor != null) || name == descriptor);
            } catch (ClassCastException cce) {
                throw new InternalError("Invalid type in enclosing method information", cce);
            }
        }

        EnclosingMethodInfo(Object[] enclosingInfo) {
            validate(enclosingInfo);
            this.enclosingClass = (Class<?>)enclosingInfo[0];
            this.name = (String)enclosingInfo[1];
            this.descriptor = (String)enclosingInfo[2];
        }

        boolean isPartial() {
            return enclosingClass == null || name == null || descriptor == null;
        }

        boolean isConstructor() { return !isPartial() && "<init>".equals(name); }

        boolean isMethod() { return !isPartial() && !isConstructor() && !"<clinit>".equals(name); }

        Class<?> getEnclosingClass() { return enclosingClass; }

        String getName() { return name; }

        String getDescriptor() { return descriptor; }

    }

    private static Class<?> toClass(Type o) {
        if (o instanceof GenericArrayType)
            return Array.newInstance(toClass(((GenericArrayType)o).getGenericComponentType()),
                                     0)
                .getClass();
        return (Class<?>)o;
     }

    /**
     * If this {@code Class} object represents a local or anonymous
     * class within a constructor, returns a {@link
     * java.lang.reflect.Constructor Constructor} object representing
     * the immediately enclosing constructor of the underlying
     * class. Returns {@code null} otherwise.  In particular, this
     * method returns {@code null} if the underlying class is a local
     * or anonymous class immediately enclosed by a type declaration,
     * instance initializer or static initializer.
     *
     * @return the immediately enclosing constructor of the underlying class, if
     *     that class is a local or anonymous class; otherwise {@code null}.
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and any of the
     *         following conditions is met:
     *
     *         <ul>
     *
     *         <li> the caller's class loader is not the same as the
     *         class loader of the enclosing class and invocation of
     *         {@link SecurityManager#checkPermission
     *         s.checkPermission} method with
     *         {@code RuntimePermission("accessDeclaredMembers")}
     *         denies access to the constructors within the enclosing class
     *
     *         <li> the caller's class loader is not the same as or an
     *         ancestor of the class loader for the enclosing class and
     *         invocation of {@link SecurityManager#checkPackageAccess
     *         s.checkPackageAccess()} denies access to the package
     *         of the enclosing class
     *
     *         </ul>
     * @since 1.5
     */
    @CallerSensitive
    public Constructor<?> getEnclosingConstructor() throws SecurityException {
        EnclosingMethodInfo enclosingInfo = getEnclosingMethodInfo();

        if (enclosingInfo == null)
            return null;
        else {
            if (!enclosingInfo.isConstructor())
                return null;

            ConstructorRepository typeInfo = ConstructorRepository.make(enclosingInfo.getDescriptor(),
                                                                        getFactory());
            Type []    parameterTypes   = typeInfo.getParameterTypes();
            Class<?>[] parameterClasses = new Class<?>[parameterTypes.length];

            // Convert Types to Classes; returned types *should*
            // be class objects since the methodDescriptor's used
            // don't have generics information
            for(int i = 0; i < parameterClasses.length; i++)
                parameterClasses[i] = toClass(parameterTypes[i]);

            // Perform access check
            final Class<?> enclosingCandidate = enclosingInfo.getEnclosingClass();
            SecurityManager sm = System.getSecurityManager();
            if (sm != null) {
                enclosingCandidate.checkMemberAccess(sm, Member.DECLARED,
                                                     Reflection.getCallerClass(), true);
            }

            Constructor<?>[] candidates = enclosingCandidate
                    .privateGetDeclaredConstructors(false);
            /*
             * Loop over all declared constructors; match number
             * of and type of parameters.
             */
            ReflectionFactory fact = getReflectionFactory();
            for (Constructor<?> c : candidates) {
                if (arrayContentsEq(parameterClasses,
                                    fact.getExecutableSharedParameterTypes(c))) {
                    return fact.copyConstructor(c);
                }
            }

            throw new InternalError("Enclosing constructor not found");
        }
    }


    /**
     * If the class or interface represented by this {@code Class} object
     * is a member of another class, returns the {@code Class} object
     * representing the class in which it was declared.  This method returns
     * null if this class or interface is not a member of any other class.  If
     * this {@code Class} object represents an array class, a primitive
     * type, or void,then this method returns null.
     *
     * @return the declaring class for this class
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and the caller's
     *         class loader is not the same as or an ancestor of the class
     *         loader for the declaring class and invocation of {@link
     *         SecurityManager#checkPackageAccess s.checkPackageAccess()}
     *         denies access to the package of the declaring class
     * @since 1.1
     */
    @CallerSensitive
    public Class<?> getDeclaringClass() throws SecurityException {
        final Class<?> candidate = getDeclaringClass0();

        if (candidate != null) {
            SecurityManager sm = System.getSecurityManager();
            if (sm != null) {
                candidate.checkPackageAccess(sm,
                    ClassLoader.getClassLoader(Reflection.getCallerClass()), true);
            }
        }
        return candidate;
    }

    private native Class<?> getDeclaringClass0();


    /**
     * Returns the immediately enclosing class of the underlying
     * class.  If the underlying class is a top level class this
     * method returns {@code null}.
     * @return the immediately enclosing class of the underlying class
     * @exception  SecurityException
     *             If a security manager, <i>s</i>, is present and the caller's
     *             class loader is not the same as or an ancestor of the class
     *             loader for the enclosing class and invocation of {@link
     *             SecurityManager#checkPackageAccess s.checkPackageAccess()}
     *             denies access to the package of the enclosing class
     * @since 1.5
     */
    @CallerSensitive
    public Class<?> getEnclosingClass() throws SecurityException {
        // There are five kinds of classes (or interfaces):
        // a) Top level classes
        // b) Nested classes (static member classes)
        // c) Inner classes (non-static member classes)
        // d) Local classes (named classes declared within a method)
        // e) Anonymous classes


        // JVM Spec 4.7.7: A class must have an EnclosingMethod
        // attribute if and only if it is a local class or an
        // anonymous class.
        EnclosingMethodInfo enclosingInfo = getEnclosingMethodInfo();
        Class<?> enclosingCandidate;

        if (enclosingInfo == null) {
            // This is a top level or a nested class or an inner class (a, b, or c)
            enclosingCandidate = getDeclaringClass0();
        } else {
            Class<?> enclosingClass = enclosingInfo.getEnclosingClass();
            // This is a local class or an anonymous class (d or e)
            if (enclosingClass == this || enclosingClass == null)
                throw new InternalError("Malformed enclosing method information");
            else
                enclosingCandidate = enclosingClass;
        }

        if (enclosingCandidate != null) {
            SecurityManager sm = System.getSecurityManager();
            if (sm != null) {
                enclosingCandidate.checkPackageAccess(sm,
                    ClassLoader.getClassLoader(Reflection.getCallerClass()), true);
            }
        }
        return enclosingCandidate;
    }

    /**
     * Returns the simple name of the underlying class as given in the
     * source code. Returns an empty string if the underlying class is
     * anonymous.
     *
     * <p>The simple name of an array is the simple name of the
     * component type with "[]" appended.  In particular the simple
     * name of an array whose component type is anonymous is "[]".
     *
     * @return the simple name of the underlying class
     * @since 1.5
     */
    public String getSimpleName() {
        ReflectionData<T> rd = reflectionData();
        String simpleName = rd.simpleName;
        if (simpleName == null) {
            rd.simpleName = simpleName = getSimpleName0();
        }
        return simpleName;
    }

    private String getSimpleName0() {
        if (isArray()) {
            return getComponentType().getSimpleName() + "[]";
        }
        String simpleName = getSimpleBinaryName();
        if (simpleName == null) { // top level class
            simpleName = getName();
            simpleName = simpleName.substring(simpleName.lastIndexOf('.') + 1); // strip the package name
        }
        return simpleName;
    }

    /**
     * Return an informative string for the name of this type.
     *
     * @return an informative string for the name of this type
     * @since 1.8
     */
    public String getTypeName() {
        if (isArray()) {
            try {
                Class<?> cl = this;
                int dimensions = 0;
                do {
                    dimensions++;
                    cl = cl.getComponentType();
                } while (cl.isArray());
                StringBuilder sb = new StringBuilder();
                sb.append(cl.getName());
                for (int i = 0; i < dimensions; i++) {
                    sb.append("[]");
                }
                return sb.toString();
            } catch (Throwable e) { /*FALLTHRU*/ }
        }
        return getName();
    }

    /**
     * Returns the canonical name of the underlying class as
     * defined by the Java Language Specification.  Returns null if
     * the underlying class does not have a canonical name (i.e., if
     * it is a local or anonymous class or an array whose component
     * type does not have a canonical name).
     * @return the canonical name of the underlying class if it exists, and
     * {@code null} otherwise.
     * @since 1.5
     */
    public String getCanonicalName() {
        ReflectionData<T> rd = reflectionData();
        String canonicalName = rd.canonicalName;
        if (canonicalName == null) {
            rd.canonicalName = canonicalName = getCanonicalName0();
        }
        return canonicalName == ReflectionData.NULL_SENTINEL? null : canonicalName;
    }

    private String getCanonicalName0() {
        if (isArray()) {
            String canonicalName = getComponentType().getCanonicalName();
            if (canonicalName != null)
                return canonicalName + "[]";
            else
                return ReflectionData.NULL_SENTINEL;
        }
        if (isLocalOrAnonymousClass())
            return ReflectionData.NULL_SENTINEL;
        Class<?> enclosingClass = getEnclosingClass();
        if (enclosingClass == null) { // top level class
            return getName();
        } else {
            String enclosingName = enclosingClass.getCanonicalName();
            if (enclosingName == null)
                return ReflectionData.NULL_SENTINEL;
            return enclosingName + "." + getSimpleName();
        }
    }

    /**
     * Returns {@code true} if and only if the underlying class
     * is an anonymous class.
     *
     * @return {@code true} if and only if this class is an anonymous class.
     * @since 1.5
     */
    public boolean isAnonymousClass() {
        return !isArray() && isLocalOrAnonymousClass() &&
                getSimpleBinaryName0() == null;
    }

    /**
     * Returns {@code true} if and only if the underlying class
     * is a local class.
     *
     * @return {@code true} if and only if this class is a local class.
     * @since 1.5
     */
    public boolean isLocalClass() {
        return isLocalOrAnonymousClass() &&
                (isArray() || getSimpleBinaryName0() != null);
    }

    /**
     * Returns {@code true} if and only if the underlying class
     * is a member class.
     *
     * @return {@code true} if and only if this class is a member class.
     * @since 1.5
     */
    public boolean isMemberClass() {
        return !isLocalOrAnonymousClass() && getDeclaringClass0() != null;
    }

    /**
     * Returns the "simple binary name" of the underlying class, i.e.,
     * the binary name without the leading enclosing class name.
     * Returns {@code null} if the underlying class is a top level
     * class.
     */
    private String getSimpleBinaryName() {
        if (isTopLevelClass())
            return null;
        String name = getSimpleBinaryName0();
        if (name == null) // anonymous class
            return "";
        return name;
    }

    private native String getSimpleBinaryName0();

    /**
     * Returns {@code true} if this is a top level class.  Returns {@code false}
     * otherwise.
     */
    private boolean isTopLevelClass() {
        return !isLocalOrAnonymousClass() && getDeclaringClass0() == null;
    }

    /**
     * Returns {@code true} if this is a local class or an anonymous
     * class.  Returns {@code false} otherwise.
     */
    private boolean isLocalOrAnonymousClass() {
        // JVM Spec 4.7.7: A class must have an EnclosingMethod
        // attribute if and only if it is a local class or an
        // anonymous class.
        return hasEnclosingMethodInfo();
    }

    private boolean hasEnclosingMethodInfo() {
        Object[] enclosingInfo = getEnclosingMethod0();
        if (enclosingInfo != null) {
            EnclosingMethodInfo.validate(enclosingInfo);
            return true;
        }
        return false;
    }

    /**
     * Returns an array containing {@code Class} objects representing all
     * the public classes and interfaces that are members of the class
     * represented by this {@code Class} object.  This includes public
     * class and interface members inherited from superclasses and public class
     * and interface members declared by the class.  This method returns an
     * array of length 0 if this {@code Class} object has no public member
     * classes or interfaces.  This method also returns an array of length 0 if
     * this {@code Class} object represents a primitive type, an array
     * class, or void.
     *
     * @return the array of {@code Class} objects representing the public
     *         members of this class
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and
     *         the caller's class loader is not the same as or an
     *         ancestor of the class loader for the current class and
     *         invocation of {@link SecurityManager#checkPackageAccess
     *         s.checkPackageAccess()} denies access to the package
     *         of this class.
     *
     * @since 1.1
     */
    @CallerSensitive
    public Class<?>[] getClasses() {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.PUBLIC, Reflection.getCallerClass(), false);
        }

        // Privileged so this implementation can look at DECLARED classes,
        // something the caller might not have privilege to do.  The code here
        // is allowed to look at DECLARED classes because (1) it does not hand
        // out anything other than public members and (2) public member access
        // has already been ok'd by the SecurityManager.

        return java.security.AccessController.doPrivileged(
            new java.security.PrivilegedAction<>() {
                public Class<?>[] run() {
                    List<Class<?>> list = new ArrayList<>();
                    Class<?> currentClass = Class.this;
                    while (currentClass != null) {
                        for (Class<?> m : currentClass.getDeclaredClasses()) {
                            if (Modifier.isPublic(m.getModifiers())) {
                                list.add(m);
                            }
                        }
                        currentClass = currentClass.getSuperclass();
                    }
                    return list.toArray(new Class<?>[0]);
                }
            });
    }


    /**
     * Returns an array containing {@code Field} objects reflecting all
     * the accessible public fields of the class or interface represented by
     * this {@code Class} object.
     *
     * <p> If this {@code Class} object represents a class or interface with
     * no accessible public fields, then this method returns an array of length
     * 0.
     *
     * <p> If this {@code Class} object represents a class, then this method
     * returns the public fields of the class and of all its superclasses and
     * superinterfaces.
     *
     * <p> If this {@code Class} object represents an interface, then this
     * method returns the fields of the interface and of all its
     * superinterfaces.
     *
     * <p> If this {@code Class} object represents an array type, a primitive
     * type, or void, then this method returns an array of length 0.
     *
     * <p> The elements in the returned array are not sorted and are not in any
     * particular order.
     *
     * @return the array of {@code Field} objects representing the
     *         public fields
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and
     *         the caller's class loader is not the same as or an
     *         ancestor of the class loader for the current class and
     *         invocation of {@link SecurityManager#checkPackageAccess
     *         s.checkPackageAccess()} denies access to the package
     *         of this class.
     *
     * @since 1.1
     * @jls 8.2 Class Members
     * @jls 8.3 Field Declarations
     */
    @CallerSensitive
    public Field[] getFields() throws SecurityException {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.PUBLIC, Reflection.getCallerClass(), true);
        }
        return copyFields(privateGetPublicFields());
    }


    /**
     * Returns an array containing {@code Method} objects reflecting all the
     * public methods of the class or interface represented by this {@code
     * Class} object, including those declared by the class or interface and
     * those inherited from superclasses and superinterfaces.
     *
     * <p> If this {@code Class} object represents an array type, then the
     * returned array has a {@code Method} object for each of the public
     * methods inherited by the array type from {@code Object}. It does not
     * contain a {@code Method} object for {@code clone()}.
     *
     * <p> If this {@code Class} object represents an interface then the
     * returned array does not contain any implicitly declared methods from
     * {@code Object}. Therefore, if no methods are explicitly declared in
     * this interface or any of its superinterfaces then the returned array
     * has length 0. (Note that a {@code Class} object which represents a class
     * always has public methods, inherited from {@code Object}.)
     *
     * <p> The returned array never contains methods with names "{@code <init>}"
     * or "{@code <clinit>}".
     *
     * <p> The elements in the returned array are not sorted and are not in any
     * particular order.
     *
     * <p> Generally, the result is computed as with the following 4 step algorithm.
     * Let C be the class or interface represented by this {@code Class} object:
     * <ol>
     * <li> A union of methods is composed of:
     *   <ol type="a">
     *   <li> C's declared public instance and static methods as returned by
     *        {@link #getDeclaredMethods()} and filtered to include only public
     *        methods.</li>
     *   <li> If C is a class other than {@code Object}, then include the result
     *        of invoking this algorithm recursively on the superclass of C.</li>
     *   <li> Include the results of invoking this algorithm recursively on all
     *        direct superinterfaces of C, but include only instance methods.</li>
     *   </ol></li>
     * <li> Union from step 1 is partitioned into subsets of methods with same
     *      signature (name, parameter types) and return type.</li>
     * <li> Within each such subset only the most specific methods are selected.
     *      Let method M be a method from a set of methods with same signature
     *      and return type. M is most specific if there is no such method
     *      N != M from the same set, such that N is more specific than M.
     *      N is more specific than M if:
     *   <ol type="a">
     *   <li> N is declared by a class and M is declared by an interface; or</li>
     *   <li> N and M are both declared by classes or both by interfaces and
     *        N's declaring type is the same as or a subtype of M's declaring type
     *        (clearly, if M's and N's declaring types are the same type, then
     *        M and N are the same method).</li>
     *   </ol></li>
     * <li> The result of this algorithm is the union of all selected methods from
     *      step 3.</li>
     * </ol>
     *
     * @apiNote There may be more than one method with a particular name
     * and parameter types in a class because while the Java language forbids a
     * class to declare multiple methods with the same signature but different
     * return types, the Java virtual machine does not.  This
     * increased flexibility in the virtual machine can be used to
     * implement various language features.  For example, covariant
     * returns can be implemented with {@linkplain
     * java.lang.reflect.Method#isBridge bridge methods}; the bridge
     * method and the overriding method would have the same
     * signature but different return types.
     *
     * @return the array of {@code Method} objects representing the
     *         public methods of this class
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and
     *         the caller's class loader is not the same as or an
     *         ancestor of the class loader for the current class and
     *         invocation of {@link SecurityManager#checkPackageAccess
     *         s.checkPackageAccess()} denies access to the package
     *         of this class.
     *
     * @jls 8.2 Class Members
     * @jls 8.4 Method Declarations
     * @since 1.1
     */
    @CallerSensitive
    public Method[] getMethods() throws SecurityException {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.PUBLIC, Reflection.getCallerClass(), true);
        }
        return copyMethods(privateGetPublicMethods());
    }


    /**
     * Returns an array containing {@code Constructor} objects reflecting
     * all the public constructors of the class represented by this
     * {@code Class} object.  An array of length 0 is returned if the
     * class has no public constructors, or if the class is an array class, or
     * if the class reflects a primitive type or void.
     *
     * Note that while this method returns an array of {@code
     * Constructor<T>} objects (that is an array of constructors from
     * this class), the return type of this method is {@code
     * Constructor<?>[]} and <em>not</em> {@code Constructor<T>[]} as
     * might be expected.  This less informative return type is
     * necessary since after being returned from this method, the
     * array could be modified to hold {@code Constructor} objects for
     * different classes, which would violate the type guarantees of
     * {@code Constructor<T>[]}.
     *
     * @return the array of {@code Constructor} objects representing the
     *         public constructors of this class
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and
     *         the caller's class loader is not the same as or an
     *         ancestor of the class loader for the current class and
     *         invocation of {@link SecurityManager#checkPackageAccess
     *         s.checkPackageAccess()} denies access to the package
     *         of this class.
     *
     * @since 1.1
     */
    @CallerSensitive
    public Constructor<?>[] getConstructors() throws SecurityException {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.PUBLIC, Reflection.getCallerClass(), true);
        }
        return copyConstructors(privateGetDeclaredConstructors(true));
    }


    /**
     * Returns a {@code Field} object that reflects the specified public member
     * field of the class or interface represented by this {@code Class}
     * object. The {@code name} parameter is a {@code String} specifying the
     * simple name of the desired field.
     *
     * <p> The field to be reflected is determined by the algorithm that
     * follows.  Let C be the class or interface represented by this object:
     *
     * <OL>
     * <LI> If C declares a public field with the name specified, that is the
     *      field to be reflected.</LI>
     * <LI> If no field was found in step 1 above, this algorithm is applied
     *      recursively to each direct superinterface of C. The direct
     *      superinterfaces are searched in the order they were declared.</LI>
     * <LI> If no field was found in steps 1 and 2 above, and C has a
     *      superclass S, then this algorithm is invoked recursively upon S.
     *      If C has no superclass, then a {@code NoSuchFieldException}
     *      is thrown.</LI>
     * </OL>
     *
     * <p> If this {@code Class} object represents an array type, then this
     * method does not find the {@code length} field of the array type.
     *
     * @param name the field name
     * @return the {@code Field} object of this class specified by
     *         {@code name}
     * @throws NoSuchFieldException if a field with the specified name is
     *         not found.
     * @throws NullPointerException if {@code name} is {@code null}
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and
     *         the caller's class loader is not the same as or an
     *         ancestor of the class loader for the current class and
     *         invocation of {@link SecurityManager#checkPackageAccess
     *         s.checkPackageAccess()} denies access to the package
     *         of this class.
     *
     * @since 1.1
     * @jls 8.2 Class Members
     * @jls 8.3 Field Declarations
     */
    @CallerSensitive
    public Field getField(String name)
        throws NoSuchFieldException, SecurityException {
        Objects.requireNonNull(name);
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.PUBLIC, Reflection.getCallerClass(), true);
        }
        Field field = getField0(name);
        if (field == null) {
            throw new NoSuchFieldException(name);
        }
        return getReflectionFactory().copyField(field);
    }


    /**
     * Returns a {@code Method} object that reflects the specified public
     * member method of the class or interface represented by this
     * {@code Class} object. The {@code name} parameter is a
     * {@code String} specifying the simple name of the desired method. The
     * {@code parameterTypes} parameter is an array of {@code Class}
     * objects that identify the method's formal parameter types, in declared
     * order. If {@code parameterTypes} is {@code null}, it is
     * treated as if it were an empty array.
     *
     * <p> If this {@code Class} object represents an array type, then this
     * method finds any public method inherited by the array type from
     * {@code Object} except method {@code clone()}.
     *
     * <p> If this {@code Class} object represents an interface then this
     * method does not find any implicitly declared method from
     * {@code Object}. Therefore, if no methods are explicitly declared in
     * this interface or any of its superinterfaces, then this method does not
     * find any method.
     *
     * <p> This method does not find any method with name "{@code <init>}" or
     * "{@code <clinit>}".
     *
     * <p> Generally, the method to be reflected is determined by the 4 step
     * algorithm that follows.
     * Let C be the class or interface represented by this {@code Class} object:
     * <ol>
     * <li> A union of methods is composed of:
     *   <ol type="a">
     *   <li> C's declared public instance and static methods as returned by
     *        {@link #getDeclaredMethods()} and filtered to include only public
     *        methods that match given {@code name} and {@code parameterTypes}</li>
     *   <li> If C is a class other than {@code Object}, then include the result
     *        of invoking this algorithm recursively on the superclass of C.</li>
     *   <li> Include the results of invoking this algorithm recursively on all
     *        direct superinterfaces of C, but include only instance methods.</li>
     *   </ol></li>
     * <li> This union is partitioned into subsets of methods with same
     *      return type (the selection of methods from step 1 also guarantees that
     *      they have the same method name and parameter types).</li>
     * <li> Within each such subset only the most specific methods are selected.
     *      Let method M be a method from a set of methods with same VM
     *      signature (return type, name, parameter types).
     *      M is most specific if there is no such method N != M from the same
     *      set, such that N is more specific than M. N is more specific than M
     *      if:
     *   <ol type="a">
     *   <li> N is declared by a class and M is declared by an interface; or</li>
     *   <li> N and M are both declared by classes or both by interfaces and
     *        N's declaring type is the same as or a subtype of M's declaring type
     *        (clearly, if M's and N's declaring types are the same type, then
     *        M and N are the same method).</li>
     *   </ol></li>
     * <li> The result of this algorithm is chosen arbitrarily from the methods
     *      with most specific return type among all selected methods from step 3.
     *      Let R be a return type of a method M from the set of all selected methods
     *      from step 3. M is a method with most specific return type if there is
     *      no such method N != M from the same set, having return type S != R,
     *      such that S is a subtype of R as determined by
     *      R.class.{@link #isAssignableFrom}(S.class).
     * </ol>
     *
     * @apiNote There may be more than one method with matching name and
     * parameter types in a class because while the Java language forbids a
     * class to declare multiple methods with the same signature but different
     * return types, the Java virtual machine does not.  This
     * increased flexibility in the virtual machine can be used to
     * implement various language features.  For example, covariant
     * returns can be implemented with {@linkplain
     * java.lang.reflect.Method#isBridge bridge methods}; the bridge
     * method and the overriding method would have the same
     * signature but different return types. This method would return the
     * overriding method as it would have a more specific return type.
     *
     * @param name the name of the method
     * @param parameterTypes the list of parameters
     * @return the {@code Method} object that matches the specified
     *         {@code name} and {@code parameterTypes}
     * @throws NoSuchMethodException if a matching method is not found
     *         or if the name is "&lt;init&gt;"or "&lt;clinit&gt;".
     * @throws NullPointerException if {@code name} is {@code null}
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and
     *         the caller's class loader is not the same as or an
     *         ancestor of the class loader for the current class and
     *         invocation of {@link SecurityManager#checkPackageAccess
     *         s.checkPackageAccess()} denies access to the package
     *         of this class.
     *
     * @jls 8.2 Class Members
     * @jls 8.4 Method Declarations
     * @since 1.1
     */
    @CallerSensitive
    public Method getMethod(String name, Class<?>... parameterTypes)
        throws NoSuchMethodException, SecurityException {
        Objects.requireNonNull(name);
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.PUBLIC, Reflection.getCallerClass(), true);
        }
        Method method = getMethod0(name, parameterTypes);
        if (method == null) {
            throw new NoSuchMethodException(methodToString(name, parameterTypes));
        }
        return getReflectionFactory().copyMethod(method);
    }

    /**
     * Returns a {@code Constructor} object that reflects the specified
     * public constructor of the class represented by this {@code Class}
     * object. The {@code parameterTypes} parameter is an array of
     * {@code Class} objects that identify the constructor's formal
     * parameter types, in declared order.
     *
     * If this {@code Class} object represents an inner class
     * declared in a non-static context, the formal parameter types
     * include the explicit enclosing instance as the first parameter.
     *
     * <p> The constructor to reflect is the public constructor of the class
     * represented by this {@code Class} object whose formal parameter
     * types match those specified by {@code parameterTypes}.
     *
     * @param parameterTypes the parameter array
     * @return the {@code Constructor} object of the public constructor that
     *         matches the specified {@code parameterTypes}
     * @throws NoSuchMethodException if a matching method is not found.
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and
     *         the caller's class loader is not the same as or an
     *         ancestor of the class loader for the current class and
     *         invocation of {@link SecurityManager#checkPackageAccess
     *         s.checkPackageAccess()} denies access to the package
     *         of this class.
     *
     * @since 1.1
     */
    @CallerSensitive
    public Constructor<T> getConstructor(Class<?>... parameterTypes)
        throws NoSuchMethodException, SecurityException
    {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.PUBLIC, Reflection.getCallerClass(), true);
        }
        return getReflectionFactory().copyConstructor(
            getConstructor0(parameterTypes, Member.PUBLIC));
    }


    /**
     * Returns an array of {@code Class} objects reflecting all the
     * classes and interfaces declared as members of the class represented by
     * this {@code Class} object. This includes public, protected, default
     * (package) access, and private classes and interfaces declared by the
     * class, but excludes inherited classes and interfaces.  This method
     * returns an array of length 0 if the class declares no classes or
     * interfaces as members, or if this {@code Class} object represents a
     * primitive type, an array class, or void.
     *
     * @return the array of {@code Class} objects representing all the
     *         declared members of this class
     * @throws SecurityException
     *         If a security manager, <i>s</i>, is present and any of the
     *         following conditions is met:
     *
     *         <ul>
     *
     *         <li> the caller's class loader is not the same as the
     *         class loader of this class and invocation of
     *         {@link SecurityManager#checkPermission
     *         s.checkPermission} method with
     *         {@code RuntimePermission("accessDeclaredMembers")}
     *         denies access to the declared classes within this class
     *
     *         <li> the caller's class loader is not the same as or an
     *         ancestor of the class loader for the current class and
     *         invocation of {@link SecurityManager#checkPackageAccess
     *         s.checkPackageAccess()} denies access to the package
     *         of this class
     *
     *         </ul>
     *
     * @since 1.1
     */
    @CallerSensitive
    public Class<?>[] getDeclaredClasses() throws SecurityException {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.DECLARED, Reflection.getCallerClass(), false);
        }
        return getDeclaredClasses0();
    }


    /**
     * Returns an array of {@code Field} objects reflecting all the fields
     * declared by the class or interface represented by this
     * {@code Class} object. This includes public, protected, default
     * (package) access, and private fields, but excludes inherited fields.
     *
     * <p> If this {@code Class} object represents a class or interface with no
     * declared fields, then this method returns an array of length 0.
     *
     * <p> If this {@code Class} object represents an array type, a primitive
     * type, or void, then this method returns an array of length 0.
     *
     * <p> The elements in the returned array are not sorted and are not in any
     * particular order.
     *
     * @return  the array of {@code Field} objects representing all the
     *          declared fields of this class
     * @throws  SecurityException
     *          If a security manager, <i>s</i>, is present and any of the
     *          following conditions is met:
     *
     *          <ul>
     *
     *          <li> the caller's class loader is not the same as the
     *          class loader of this class and invocation of
     *          {@link SecurityManager#checkPermission
     *          s.checkPermission} method with
     *          {@code RuntimePermission("accessDeclaredMembers")}
     *          denies access to the declared fields within this class
     *
     *          <li> the caller's class loader is not the same as or an
     *          ancestor of the class loader for the current class and
     *          invocation of {@link SecurityManager#checkPackageAccess
     *          s.checkPackageAccess()} denies access to the package
     *          of this class
     *
     *          </ul>
     *
     * @since 1.1
     * @jls 8.2 Class Members
     * @jls 8.3 Field Declarations
     */
    @CallerSensitive
    public Field[] getDeclaredFields() throws SecurityException {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.DECLARED, Reflection.getCallerClass(), true);
        }
        return copyFields(privateGetDeclaredFields(false));
    }


    /**
     * Returns an array containing {@code Method} objects reflecting all the
     * declared methods of the class or interface represented by this {@code
     * Class} object, including public, protected, default (package)
     * access, and private methods, but excluding inherited methods.
     *
     * <p> If this {@code Class} object represents a type that has multiple
     * declared methods with the same name and parameter types, but different
     * return types, then the returned array has a {@code Method} object for
     * each such method.
     *
     * <p> If this {@code Class} object represents a type that has a class
     * initialization method {@code <clinit>}, then the returned array does
     * <em>not</em> have a corresponding {@code Method} object.
     *
     * <p> If this {@code Class} object represents a class or interface with no
     * declared methods, then the returned array has length 0.
     *
     * <p> If this {@code Class} object represents an array type, a primitive
     * type, or void, then the returned array has length 0.
     *
     * <p> The elements in the returned array are not sorted and are not in any
     * particular order.
     *
     * @return  the array of {@code Method} objects representing all the
     *          declared methods of this class
     * @throws  SecurityException
     *          If a security manager, <i>s</i>, is present and any of the
     *          following conditions is met:
     *
     *          <ul>
     *
     *          <li> the caller's class loader is not the same as the
     *          class loader of this class and invocation of
     *          {@link SecurityManager#checkPermission
     *          s.checkPermission} method with
     *          {@code RuntimePermission("accessDeclaredMembers")}
     *          denies access to the declared methods within this class
     *
     *          <li> the caller's class loader is not the same as or an
     *          ancestor of the class loader for the current class and
     *          invocation of {@link SecurityManager#checkPackageAccess
     *          s.checkPackageAccess()} denies access to the package
     *          of this class
     *
     *          </ul>
     *
     * @jls 8.2 Class Members
     * @jls 8.4 Method Declarations
     * @since 1.1
     */
    @CallerSensitive
    public Method[] getDeclaredMethods() throws SecurityException {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.DECLARED, Reflection.getCallerClass(), true);
        }
        return copyMethods(privateGetDeclaredMethods(false));
    }


    /**
     * Returns an array of {@code Constructor} objects reflecting all the
     * constructors declared by the class represented by this
     * {@code Class} object. These are public, protected, default
     * (package) access, and private constructors.  The elements in the array
     * returned are not sorted and are not in any particular order.  If the
     * class has a default constructor, it is included in the returned array.
     * This method returns an array of length 0 if this {@code Class}
     * object represents an interface, a primitive type, an array class, or
     * void.
     *
     * <p> See <em>The Java Language Specification</em>, section 8.2.
     *
     * @return  the array of {@code Constructor} objects representing all the
     *          declared constructors of this class
     * @throws  SecurityException
     *          If a security manager, <i>s</i>, is present and any of the
     *          following conditions is met:
     *
     *          <ul>
     *
     *          <li> the caller's class loader is not the same as the
     *          class loader of this class and invocation of
     *          {@link SecurityManager#checkPermission
     *          s.checkPermission} method with
     *          {@code RuntimePermission("accessDeclaredMembers")}
     *          denies access to the declared constructors within this class
     *
     *          <li> the caller's class loader is not the same as or an
     *          ancestor of the class loader for the current class and
     *          invocation of {@link SecurityManager#checkPackageAccess
     *          s.checkPackageAccess()} denies access to the package
     *          of this class
     *
     *          </ul>
     *
     * @since 1.1
     */
    @CallerSensitive
    public Constructor<?>[] getDeclaredConstructors() throws SecurityException {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.DECLARED, Reflection.getCallerClass(), true);
        }
        return copyConstructors(privateGetDeclaredConstructors(false));
    }


    /**
     * Returns a {@code Field} object that reflects the specified declared
     * field of the class or interface represented by this {@code Class}
     * object. The {@code name} parameter is a {@code String} that specifies
     * the simple name of the desired field.
     *
     * <p> If this {@code Class} object represents an array type, then this
     * method does not find the {@code length} field of the array type.
     *
     * @param name the name of the field
     * @return  the {@code Field} object for the specified field in this
     *          class
     * @throws  NoSuchFieldException if a field with the specified name is
     *          not found.
     * @throws  NullPointerException if {@code name} is {@code null}
     * @throws  SecurityException
     *          If a security manager, <i>s</i>, is present and any of the
     *          following conditions is met:
     *
     *          <ul>
     *
     *          <li> the caller's class loader is not the same as the
     *          class loader of this class and invocation of
     *          {@link SecurityManager#checkPermission
     *          s.checkPermission} method with
     *          {@code RuntimePermission("accessDeclaredMembers")}
     *          denies access to the declared field
     *
     *          <li> the caller's class loader is not the same as or an
     *          ancestor of the class loader for the current class and
     *          invocation of {@link SecurityManager#checkPackageAccess
     *          s.checkPackageAccess()} denies access to the package
     *          of this class
     *
     *          </ul>
     *
     * @since 1.1
     * @jls 8.2 Class Members
     * @jls 8.3 Field Declarations
     */
    @CallerSensitive
    public Field getDeclaredField(String name)
        throws NoSuchFieldException, SecurityException {
        Objects.requireNonNull(name);
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.DECLARED, Reflection.getCallerClass(), true);
        }
        Field field = searchFields(privateGetDeclaredFields(false), name);
        if (field == null) {
            throw new NoSuchFieldException(name);
        }
        return getReflectionFactory().copyField(field);
    }


    /**
     * Returns a {@code Method} object that reflects the specified
     * declared method of the class or interface represented by this
     * {@code Class} object. The {@code name} parameter is a
     * {@code String} that specifies the simple name of the desired
     * method, and the {@code parameterTypes} parameter is an array of
     * {@code Class} objects that identify the method's formal parameter
     * types, in declared order.  If more than one method with the same
     * parameter types is declared in a class, and one of these methods has a
     * return type that is more specific than any of the others, that method is
     * returned; otherwise one of the methods is chosen arbitrarily.  If the
     * name is "&lt;init&gt;"or "&lt;clinit&gt;" a {@code NoSuchMethodException}
     * is raised.
     *
     * <p> If this {@code Class} object represents an array type, then this
     * method does not find the {@code clone()} method.
     *
     * @param name the name of the method
     * @param parameterTypes the parameter array
     * @return  the {@code Method} object for the method of this class
     *          matching the specified name and parameters
     * @throws  NoSuchMethodException if a matching method is not found.
     * @throws  NullPointerException if {@code name} is {@code null}
     * @throws  SecurityException
     *          If a security manager, <i>s</i>, is present and any of the
     *          following conditions is met:
     *
     *          <ul>
     *
     *          <li> the caller's class loader is not the same as the
     *          class loader of this class and invocation of
     *          {@link SecurityManager#checkPermission
     *          s.checkPermission} method with
     *          {@code RuntimePermission("accessDeclaredMembers")}
     *          denies access to the declared method
     *
     *          <li> the caller's class loader is not the same as or an
     *          ancestor of the class loader for the current class and
     *          invocation of {@link SecurityManager#checkPackageAccess
     *          s.checkPackageAccess()} denies access to the package
     *          of this class
     *
     *          </ul>
     *
     * @jls 8.2 Class Members
     * @jls 8.4 Method Declarations
     * @since 1.1
     */
    @CallerSensitive
    public Method getDeclaredMethod(String name, Class<?>... parameterTypes)
        throws NoSuchMethodException, SecurityException {
        Objects.requireNonNull(name);
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.DECLARED, Reflection.getCallerClass(), true);
        }
        Method method = searchMethods(privateGetDeclaredMethods(false), name, parameterTypes);
        if (method == null) {
            throw new NoSuchMethodException(methodToString(name, parameterTypes));
        }
        return getReflectionFactory().copyMethod(method);
    }

    /**
     * Returns the list of {@code Method} objects for the declared public
     * methods of this class or interface that have the specified method name
     * and parameter types.
     *
     * @param name the name of the method
     * @param parameterTypes the parameter array
     * @return the list of {@code Method} objects for the public methods of
     *         this class matching the specified name and parameters
     */
    List<Method> getDeclaredPublicMethods(String name, Class<?>... parameterTypes) {
        Method[] methods = privateGetDeclaredMethods(/* publicOnly */ true);
        ReflectionFactory factory = getReflectionFactory();
        List<Method> result = new ArrayList<>();
        for (Method method : methods) {
            if (method.getName().equals(name)
                && Arrays.equals(
                    factory.getExecutableSharedParameterTypes(method),
                    parameterTypes)) {
                result.add(factory.copyMethod(method));
            }
        }
        return result;
    }

    /**
     * Returns a {@code Constructor} object that reflects the specified
     * constructor of the class or interface represented by this
     * {@code Class} object.  The {@code parameterTypes} parameter is
     * an array of {@code Class} objects that identify the constructor's
     * formal parameter types, in declared order.
     *
     * If this {@code Class} object represents an inner class
     * declared in a non-static context, the formal parameter types
     * include the explicit enclosing instance as the first parameter.
     *
     * @param parameterTypes the parameter array
     * @return  The {@code Constructor} object for the constructor with the
     *          specified parameter list
     * @throws  NoSuchMethodException if a matching method is not found.
     * @throws  SecurityException
     *          If a security manager, <i>s</i>, is present and any of the
     *          following conditions is met:
     *
     *          <ul>
     *
     *          <li> the caller's class loader is not the same as the
     *          class loader of this class and invocation of
     *          {@link SecurityManager#checkPermission
     *          s.checkPermission} method with
     *          {@code RuntimePermission("accessDeclaredMembers")}
     *          denies access to the declared constructor
     *
     *          <li> the caller's class loader is not the same as or an
     *          ancestor of the class loader for the current class and
     *          invocation of {@link SecurityManager#checkPackageAccess
     *          s.checkPackageAccess()} denies access to the package
     *          of this class
     *
     *          </ul>
     *
     * @since 1.1
     */
    @CallerSensitive
    public Constructor<T> getDeclaredConstructor(Class<?>... parameterTypes)
        throws NoSuchMethodException, SecurityException
    {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkMemberAccess(sm, Member.DECLARED, Reflection.getCallerClass(), true);
        }

        return getReflectionFactory().copyConstructor(
            getConstructor0(parameterTypes, Member.DECLARED));
    }

    /**
     * Finds a resource with a given name.
     *
     * <p> If this class is in a named {@link Module Module} then this method
     * will attempt to find the resource in the module. This is done by
     * delegating to the module's class loader {@link
     * ClassLoader#findResource(String,String) findResource(String,String)}
     * method, invoking it with the module name and the absolute name of the
     * resource. Resources in named modules are subject to the rules for
     * encapsulation specified in the {@code Module} {@link
     * Module#getResourceAsStream getResourceAsStream} method and so this
     * method returns {@code null} when the resource is a
     * non-"{@code .class}" resource in a package that is not open to the
     * caller's module.
     *
     * <p> Otherwise, if this class is not in a named module then the rules for
     * searching resources associated with a given class are implemented by the
     * defining {@linkplain ClassLoader class loader} of the class.  This method
     * delegates to this object's class loader.  If this object was loaded by
     * the bootstrap class loader, the method delegates to {@link
     * ClassLoader#getSystemResourceAsStream}.
     *
     * <p> Before delegation, an absolute resource name is constructed from the
     * given resource name using this algorithm:
     *
     * <ul>
     *
     * <li> If the {@code name} begins with a {@code '/'}
     * (<code>'&#92;u002f'</code>), then the absolute name of the resource is the
     * portion of the {@code name} following the {@code '/'}.
     *
     * <li> Otherwise, the absolute name is of the following form:
     *
     * <blockquote>
     *   {@code modified_package_name/name}
     * </blockquote>
     *
     * <p> Where the {@code modified_package_name} is the package name of this
     * object with {@code '/'} substituted for {@code '.'}
     * (<code>'&#92;u002e'</code>).
     *
     * </ul>
     *
     * @param  name name of the desired resource
     * @return  A {@link java.io.InputStream} object; {@code null} if no
     *          resource with this name is found, the resource is in a package
     *          that is not {@linkplain Module#isOpen(String, Module) open} to at
     *          least the caller module, or access to the resource is denied
     *          by the security manager.
     * @throws  NullPointerException If {@code name} is {@code null}
     *
     * @see Module#getResourceAsStream(String)
     * @since  1.1
     * @revised 9
     * @spec JPMS
     */
    @CallerSensitive
    public InputStream getResourceAsStream(String name) {
        name = resolveName(name);

        Module thisModule = getModule();
        if (thisModule.isNamed()) {
            // check if resource can be located by caller
            if (Resources.canEncapsulate(name)
                && !isOpenToCaller(name, Reflection.getCallerClass())) {
                return null;
            }

            // resource not encapsulated or in package open to caller
            String mn = thisModule.getName();
            ClassLoader cl = getClassLoader0();
            try {

                // special-case built-in class loaders to avoid the
                // need for a URL connection
                if (cl == null) {
                    return BootLoader.findResourceAsStream(mn, name);
                } else if (cl instanceof BuiltinClassLoader) {
                    return ((BuiltinClassLoader) cl).findResourceAsStream(mn, name);
                } else {
                    URL url = cl.findResource(mn, name);
                    return (url != null) ? url.openStream() : null;
                }

            } catch (IOException | SecurityException e) {
                return null;
            }
        }

        // unnamed module
        ClassLoader cl = getClassLoader0();
        if (cl == null) {
            return ClassLoader.getSystemResourceAsStream(name);
        } else {
            return cl.getResourceAsStream(name);
        }
    }

    /**
     * Finds a resource with a given name.
     *
     * <p> If this class is in a named {@link Module Module} then this method
     * will attempt to find the resource in the module. This is done by
     * delegating to the module's class loader {@link
     * ClassLoader#findResource(String,String) findResource(String,String)}
     * method, invoking it with the module name and the absolute name of the
     * resource. Resources in named modules are subject to the rules for
     * encapsulation specified in the {@code Module} {@link
     * Module#getResourceAsStream getResourceAsStream} method and so this
     * method returns {@code null} when the resource is a
     * non-"{@code .class}" resource in a package that is not open to the
     * caller's module.
     *
     * <p> Otherwise, if this class is not in a named module then the rules for
     * searching resources associated with a given class are implemented by the
     * defining {@linkplain ClassLoader class loader} of the class.  This method
     * delegates to this object's class loader. If this object was loaded by
     * the bootstrap class loader, the method delegates to {@link
     * ClassLoader#getSystemResource}.
     *
     * <p> Before delegation, an absolute resource name is constructed from the
     * given resource name using this algorithm:
     *
     * <ul>
     *
     * <li> If the {@code name} begins with a {@code '/'}
     * (<code>'&#92;u002f'</code>), then the absolute name of the resource is the
     * portion of the {@code name} following the {@code '/'}.
     *
     * <li> Otherwise, the absolute name is of the following form:
     *
     * <blockquote>
     *   {@code modified_package_name/name}
     * </blockquote>
     *
     * <p> Where the {@code modified_package_name} is the package name of this
     * object with {@code '/'} substituted for {@code '.'}
     * (<code>'&#92;u002e'</code>).
     *
     * </ul>
     *
     * @param  name name of the desired resource
     * @return A {@link java.net.URL} object; {@code null} if no resource with
     *         this name is found, the resource cannot be located by a URL, the
     *         resource is in a package that is not
     *         {@linkplain Module#isOpen(String, Module) open} to at least the caller
     *         module, or access to the resource is denied by the security
     *         manager.
     * @throws NullPointerException If {@code name} is {@code null}
     * @since  1.1
     * @revised 9
     * @spec JPMS
     */
    @CallerSensitive
    public URL getResource(String name) {
        name = resolveName(name);

        Module thisModule = getModule();
        if (thisModule.isNamed()) {
            // check if resource can be located by caller
            if (Resources.canEncapsulate(name)
                && !isOpenToCaller(name, Reflection.getCallerClass())) {
                return null;
            }

            // resource not encapsulated or in package open to caller
            String mn = thisModule.getName();
            ClassLoader cl = getClassLoader0();
            try {
                if (cl == null) {
                    return BootLoader.findResource(mn, name);
                } else {
                    return cl.findResource(mn, name);
                }
            } catch (IOException ioe) {
                return null;
            }
        }

        // unnamed module
        ClassLoader cl = getClassLoader0();
        if (cl == null) {
            return ClassLoader.getSystemResource(name);
        } else {
            return cl.getResource(name);
        }
    }

    /**
     * Returns true if a resource with the given name can be located by the
     * given caller. All resources in a module can be located by code in
     * the module. For other callers, then the package needs to be open to
     * the caller.
     */
    private boolean isOpenToCaller(String name, Class<?> caller) {
        // assert getModule().isNamed();
        Module thisModule = getModule();
        Module callerModule = (caller != null) ? caller.getModule() : null;
        if (callerModule != thisModule) {
            String pn = Resources.toPackageName(name);
            if (thisModule.getDescriptor().packages().contains(pn)) {
                if (callerModule == null && !thisModule.isOpen(pn)) {
                    // no caller, package not open
                    return false;
                }
                if (!thisModule.isOpen(pn, callerModule)) {
                    // package not open to caller
                    return false;
                }
            }
        }
        return true;
    }


    /** protection domain returned when the internal domain is null */
    private static java.security.ProtectionDomain allPermDomain;

    /**
     * Returns the {@code ProtectionDomain} of this class.  If there is a
     * security manager installed, this method first calls the security
     * manager's {@code checkPermission} method with a
     * {@code RuntimePermission("getProtectionDomain")} permission to
     * ensure it's ok to get the
     * {@code ProtectionDomain}.
     *
     * @return the ProtectionDomain of this class
     *
     * @throws SecurityException
     *        if a security manager exists and its
     *        {@code checkPermission} method doesn't allow
     *        getting the ProtectionDomain.
     *
     * @see java.security.ProtectionDomain
     * @see SecurityManager#checkPermission
     * @see java.lang.RuntimePermission
     * @since 1.2
     */
    public java.security.ProtectionDomain getProtectionDomain() {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(SecurityConstants.GET_PD_PERMISSION);
        }
        java.security.ProtectionDomain pd = getProtectionDomain0();
        if (pd == null) {
            if (allPermDomain == null) {
                java.security.Permissions perms =
                    new java.security.Permissions();
                perms.add(SecurityConstants.ALL_PERMISSION);
                allPermDomain =
                    new java.security.ProtectionDomain(null, perms);
            }
            pd = allPermDomain;
        }
        return pd;
    }


    /**
     * Returns the ProtectionDomain of this class.
     */
    private native java.security.ProtectionDomain getProtectionDomain0();

    /*
     * Return the Virtual Machine's Class object for the named
     * primitive type.
     */
    static native Class<?> getPrimitiveClass(String name);

    /*
     * Check if client is allowed to access members.  If access is denied,
     * throw a SecurityException.
     *
     * This method also enforces package access.
     *
     * <p> Default policy: allow all clients access with normal Java access
     * control.
     *
     * <p> NOTE: should only be called if a SecurityManager is installed
     */
    private void checkMemberAccess(SecurityManager sm, int which,
                                   Class<?> caller, boolean checkProxyInterfaces) {
        /* Default policy allows access to all {@link Member#PUBLIC} members,
         * as well as access to classes that have the same class loader as the caller.
         * In all other cases, it requires RuntimePermission("accessDeclaredMembers")
         * permission.
         */
        final ClassLoader ccl = ClassLoader.getClassLoader(caller);
        if (which != Member.PUBLIC) {
            final ClassLoader cl = getClassLoader0();
            if (ccl != cl) {
                sm.checkPermission(SecurityConstants.CHECK_MEMBER_ACCESS_PERMISSION);
            }
        }
        this.checkPackageAccess(sm, ccl, checkProxyInterfaces);
    }

    /*
     * Checks if a client loaded in ClassLoader ccl is allowed to access this
     * class under the current package access policy. If access is denied,
     * throw a SecurityException.
     *
     * NOTE: this method should only be called if a SecurityManager is active
     */
    private void checkPackageAccess(SecurityManager sm, final ClassLoader ccl,
                                    boolean checkProxyInterfaces) {
        final ClassLoader cl = getClassLoader0();

        if (ReflectUtil.needsPackageAccessCheck(ccl, cl)) {
            String pkg = this.getPackageName();
            if (pkg != null && !pkg.isEmpty()) {
                // skip the package access check on a proxy class in default proxy package
                if (!Proxy.isProxyClass(this) || ReflectUtil.isNonPublicProxyClass(this)) {
                    sm.checkPackageAccess(pkg);
                }
            }
        }
        // check package access on the proxy interfaces
        if (checkProxyInterfaces && Proxy.isProxyClass(this)) {
            ReflectUtil.checkProxyPackageAccess(ccl, this.getInterfaces());
        }
    }

    /**
     * Add a package name prefix if the name is not absolute Remove leading "/"
     * if name is absolute
     */
    private String resolveName(String name) {
        if (!name.startsWith("/")) {
            Class<?> c = this;
            while (c.isArray()) {
                c = c.getComponentType();
            }
            String baseName = c.getPackageName();
            if (baseName != null && !baseName.isEmpty()) {
                name = baseName.replace('.', '/') + "/" + name;
            }
        } else {
            name = name.substring(1);
        }
        return name;
    }

    /**
     * Atomic operations support.
     */
    private static class Atomic {
        // initialize Unsafe machinery here, since we need to call Class.class instance method
        // and have to avoid calling it in the static initializer of the Class class...
        private static final Unsafe unsafe = Unsafe.getUnsafe();
        // offset of Class.reflectionData instance field
        private static final long reflectionDataOffset
                = unsafe.objectFieldOffset(Class.class, "reflectionData");
        // offset of Class.annotationType instance field
        private static final long annotationTypeOffset
                = unsafe.objectFieldOffset(Class.class, "annotationType");
        // offset of Class.annotationData instance field
        private static final long annotationDataOffset
                = unsafe.objectFieldOffset(Class.class, "annotationData");

        static <T> boolean casReflectionData(Class<?> clazz,
                                             SoftReference<ReflectionData<T>> oldData,
                                             SoftReference<ReflectionData<T>> newData) {
            return unsafe.compareAndSetObject(clazz, reflectionDataOffset, oldData, newData);
        }

        static <T> boolean casAnnotationType(Class<?> clazz,
                                             AnnotationType oldType,
                                             AnnotationType newType) {
            return unsafe.compareAndSetObject(clazz, annotationTypeOffset, oldType, newType);
        }

        static <T> boolean casAnnotationData(Class<?> clazz,
                                             AnnotationData oldData,
                                             AnnotationData newData) {
            return unsafe.compareAndSetObject(clazz, annotationDataOffset, oldData, newData);
        }
    }

    /**
     * Reflection support.
     */

    // Reflection data caches various derived names and reflective members. Cached
    // values may be invalidated when JVM TI RedefineClasses() is called
    private static class ReflectionData<T> {
        volatile Field[] declaredFields;
        volatile Field[] publicFields;
        volatile Method[] declaredMethods;
        volatile Method[] publicMethods;
        volatile Constructor<T>[] declaredConstructors;
        volatile Constructor<T>[] publicConstructors;
        // Intermediate results for getFields and getMethods
        volatile Field[] declaredPublicFields;
        volatile Method[] declaredPublicMethods;
        volatile Class<?>[] interfaces;

        // Cached names
        String simpleName;
        String canonicalName;
        static final String NULL_SENTINEL = new String();

        // Value of classRedefinedCount when we created this ReflectionData instance
        final int redefinedCount;

        ReflectionData(int redefinedCount) {
            this.redefinedCount = redefinedCount;
        }
    }

    private transient volatile SoftReference<ReflectionData<T>> reflectionData;

    // Incremented by the VM on each call to JVM TI RedefineClasses()
    // that redefines this class or a superclass.
    private transient volatile int classRedefinedCount;

    // Lazily create and cache ReflectionData
    private ReflectionData<T> reflectionData() {
        SoftReference<ReflectionData<T>> reflectionData = this.reflectionData;
        int classRedefinedCount = this.classRedefinedCount;
        ReflectionData<T> rd;
        if (reflectionData != null &&
            (rd = reflectionData.get()) != null &&
            rd.redefinedCount == classRedefinedCount) {
            return rd;
        }
        // else no SoftReference or cleared SoftReference or stale ReflectionData
        // -> create and replace new instance
        return newReflectionData(reflectionData, classRedefinedCount);
    }

    private ReflectionData<T> newReflectionData(SoftReference<ReflectionData<T>> oldReflectionData,
                                                int classRedefinedCount) {
        while (true) {
            ReflectionData<T> rd = new ReflectionData<>(classRedefinedCount);
            // try to CAS it...
            if (Atomic.casReflectionData(this, oldReflectionData, new SoftReference<>(rd))) {
                return rd;
            }
            // else retry
            oldReflectionData = this.reflectionData;
            classRedefinedCount = this.classRedefinedCount;
            if (oldReflectionData != null &&
                (rd = oldReflectionData.get()) != null &&
                rd.redefinedCount == classRedefinedCount) {
                return rd;
            }
        }
    }

    // Generic signature handling
    private native String getGenericSignature0();

    // Generic info repository; lazily initialized
    private transient volatile ClassRepository genericInfo;

    // accessor for factory
    private GenericsFactory getFactory() {
        // create scope and factory
        return CoreReflectionFactory.make(this, ClassScope.make(this));
    }

    // accessor for generic info repository;
    // generic info is lazily initialized
    private ClassRepository getGenericInfo() {
        ClassRepository genericInfo = this.genericInfo;
        if (genericInfo == null) {
            String signature = getGenericSignature0();
            if (signature == null) {
                genericInfo = ClassRepository.NONE;
            } else {
                genericInfo = ClassRepository.make(signature, getFactory());
            }
            this.genericInfo = genericInfo;
        }
        return (genericInfo != ClassRepository.NONE) ? genericInfo : null;
    }

    // Annotations handling
    native byte[] getRawAnnotations();
    // Since 1.8
    native byte[] getRawTypeAnnotations();
    static byte[] getExecutableTypeAnnotationBytes(Executable ex) {
        return getReflectionFactory().getExecutableTypeAnnotationBytes(ex);
    }

    native ConstantPool getConstantPool();

    //
    //
    // java.lang.reflect.Field handling
    //
    //

    // Returns an array of "root" fields. These Field objects must NOT
    // be propagated to the outside world, but must instead be copied
    // via ReflectionFactory.copyField.
    private Field[] privateGetDeclaredFields(boolean publicOnly) {
        Field[] res;
        ReflectionData<T> rd = reflectionData();
        if (rd != null) {
            res = publicOnly ? rd.declaredPublicFields : rd.declaredFields;
            if (res != null) return res;
        }
        // No cached value available; request value from VM
        res = Reflection.filterFields(this, getDeclaredFields0(publicOnly));
        if (rd != null) {
            if (publicOnly) {
                rd.declaredPublicFields = res;
            } else {
                rd.declaredFields = res;
            }
        }
        return res;
    }

    // Returns an array of "root" fields. These Field objects must NOT
    // be propagated to the outside world, but must instead be copied
    // via ReflectionFactory.copyField.
    private Field[] privateGetPublicFields() {
        Field[] res;
        ReflectionData<T> rd = reflectionData();
        if (rd != null) {
            res = rd.publicFields;
            if (res != null) return res;
        }

        // Use a linked hash set to ensure order is preserved and
        // fields from common super interfaces are not duplicated
        LinkedHashSet<Field> fields = new LinkedHashSet<>();

        // Local fields
        addAll(fields, privateGetDeclaredFields(true));

        // Direct superinterfaces, recursively
        for (Class<?> si : getInterfaces()) {
            addAll(fields, si.privateGetPublicFields());
        }

        // Direct superclass, recursively
        Class<?> sc = getSuperclass();
        if (sc != null) {
            addAll(fields, sc.privateGetPublicFields());
        }

        res = fields.toArray(new Field[0]);
        if (rd != null) {
            rd.publicFields = res;
        }
        return res;
    }

    private static void addAll(Collection<Field> c, Field[] o) {
        for (Field f : o) {
            c.add(f);
        }
    }


    //
    //
    // java.lang.reflect.Constructor handling
    //
    //

    // Returns an array of "root" constructors. These Constructor
    // objects must NOT be propagated to the outside world, but must
    // instead be copied via ReflectionFactory.copyConstructor.
    private Constructor<T>[] privateGetDeclaredConstructors(boolean publicOnly) {
        Constructor<T>[] res;
        ReflectionData<T> rd = reflectionData();
        if (rd != null) {
            res = publicOnly ? rd.publicConstructors : rd.declaredConstructors;
            if (res != null) return res;
        }
        // No cached value available; request value from VM
        if (isInterface()) {
            @SuppressWarnings("unchecked")
            Constructor<T>[] temporaryRes = (Constructor<T>[]) new Constructor<?>[0];
            res = temporaryRes;
        } else {
            res = getDeclaredConstructors0(publicOnly);
        }
        if (rd != null) {
            if (publicOnly) {
                rd.publicConstructors = res;
            } else {
                rd.declaredConstructors = res;
            }
        }
        return res;
    }

    //
    //
    // java.lang.reflect.Method handling
    //
    //

    // Returns an array of "root" methods. These Method objects must NOT
    // be propagated to the outside world, but must instead be copied
    // via ReflectionFactory.copyMethod.
    private Method[] privateGetDeclaredMethods(boolean publicOnly) {
        Method[] res;
        ReflectionData<T> rd = reflectionData();
        if (rd != null) {
            res = publicOnly ? rd.declaredPublicMethods : rd.declaredMethods;
            if (res != null) return res;
        }
        // No cached value available; request value from VM
        res = Reflection.filterMethods(this, getDeclaredMethods0(publicOnly));
        if (rd != null) {
            if (publicOnly) {
                rd.declaredPublicMethods = res;
            } else {
                rd.declaredMethods = res;
            }
        }
        return res;
    }

    // Returns an array of "root" methods. These Method objects must NOT
    // be propagated to the outside world, but must instead be copied
    // via ReflectionFactory.copyMethod.
    private Method[] privateGetPublicMethods() {
        Method[] res;
        ReflectionData<T> rd = reflectionData();
        if (rd != null) {
            res = rd.publicMethods;
            if (res != null) return res;
        }

        // No cached value available; compute value recursively.
        // Start by fetching public declared methods...
        PublicMethods pms = new PublicMethods();
        for (Method m : privateGetDeclaredMethods(/* publicOnly */ true)) {
            pms.merge(m);
        }
        // ...then recur over superclass methods...
        Class<?> sc = getSuperclass();
        if (sc != null) {
            for (Method m : sc.privateGetPublicMethods()) {
                pms.merge(m);
            }
        }
        // ...and finally over direct superinterfaces.
        for (Class<?> intf : getInterfaces(/* cloneArray */ false)) {
            for (Method m : intf.privateGetPublicMethods()) {
                // static interface methods are not inherited
                if (!Modifier.isStatic(m.getModifiers())) {
                    pms.merge(m);
                }
            }
        }

        res = pms.toArray();
        if (rd != null) {
            rd.publicMethods = res;
        }
        return res;
    }


    //
    // Helpers for fetchers of one field, method, or constructor
    //

    // This method does not copy the returned Field object!
    private static Field searchFields(Field[] fields, String name) {
        for (Field field : fields) {
            if (field.getName().equals(name)) {
                return field;
            }
        }
        return null;
    }

    // Returns a "root" Field object. This Field object must NOT
    // be propagated to the outside world, but must instead be copied
    // via ReflectionFactory.copyField.
    private Field getField0(String name) {
        // Note: the intent is that the search algorithm this routine
        // uses be equivalent to the ordering imposed by
        // privateGetPublicFields(). It fetches only the declared
        // public fields for each class, however, to reduce the number
        // of Field objects which have to be created for the common
        // case where the field being requested is declared in the
        // class which is being queried.
        Field res;
        // Search declared public fields
        if ((res = searchFields(privateGetDeclaredFields(true), name)) != null) {
            return res;
        }
        // Direct superinterfaces, recursively
        Class<?>[] interfaces = getInterfaces(/* cloneArray */ false);
        for (Class<?> c : interfaces) {
            if ((res = c.getField0(name)) != null) {
                return res;
            }
        }
        // Direct superclass, recursively
        if (!isInterface()) {
            Class<?> c = getSuperclass();
            if (c != null) {
                if ((res = c.getField0(name)) != null) {
                    return res;
                }
            }
        }
        return null;
    }

    // This method does not copy the returned Method object!
    private static Method searchMethods(Method[] methods,
                                        String name,
                                        Class<?>[] parameterTypes)
    {
        ReflectionFactory fact = getReflectionFactory();
        Method res = null;
        for (Method m : methods) {
            if (m.getName().equals(name)
                && arrayContentsEq(parameterTypes,
                                   fact.getExecutableSharedParameterTypes(m))
                && (res == null
                    || (res.getReturnType() != m.getReturnType()
                        && res.getReturnType().isAssignableFrom(m.getReturnType()))))
                res = m;
        }
        return res;
    }

    private static final Class<?>[] EMPTY_CLASS_ARRAY = new Class<?>[0];

    // Returns a "root" Method object. This Method object must NOT
    // be propagated to the outside world, but must instead be copied
    // via ReflectionFactory.copyMethod.
    private Method getMethod0(String name, Class<?>[] parameterTypes) {
        PublicMethods.MethodList res = getMethodsRecursive(
            name,
            parameterTypes == null ? EMPTY_CLASS_ARRAY : parameterTypes,
            /* includeStatic */ true);
        return res == null ? null : res.getMostSpecific();
    }

    // Returns a list of "root" Method objects. These Method objects must NOT
    // be propagated to the outside world, but must instead be copied
    // via ReflectionFactory.copyMethod.
    private PublicMethods.MethodList getMethodsRecursive(String name,
                                                         Class<?>[] parameterTypes,
                                                         boolean includeStatic) {
        // 1st check declared public methods
        Method[] methods = privateGetDeclaredMethods(/* publicOnly */ true);
        PublicMethods.MethodList res = PublicMethods.MethodList
            .filter(methods, name, parameterTypes, includeStatic);
        // if there is at least one match among declared methods, we need not
        // search any further as such match surely overrides matching methods
        // declared in superclass(es) or interface(s).
        if (res != null) {
            return res;
        }

        // if there was no match among declared methods,
        // we must consult the superclass (if any) recursively...
        Class<?> sc = getSuperclass();
        if (sc != null) {
            res = sc.getMethodsRecursive(name, parameterTypes, includeStatic);
        }

        // ...and coalesce the superclass methods with methods obtained
        // from directly implemented interfaces excluding static methods...
        for (Class<?> intf : getInterfaces(/* cloneArray */ false)) {
            res = PublicMethods.MethodList.merge(
                res, intf.getMethodsRecursive(name, parameterTypes,
                                              /* includeStatic */ false));
        }

        return res;
    }

    // Returns a "root" Constructor object. This Constructor object must NOT
    // be propagated to the outside world, but must instead be copied
    // via ReflectionFactory.copyConstructor.
    private Constructor<T> getConstructor0(Class<?>[] parameterTypes,
                                        int which) throws NoSuchMethodException
    {
        ReflectionFactory fact = getReflectionFactory();
        Constructor<T>[] constructors = privateGetDeclaredConstructors((which == Member.PUBLIC));
        for (Constructor<T> constructor : constructors) {
            if (arrayContentsEq(parameterTypes,
                                fact.getExecutableSharedParameterTypes(constructor))) {
                return constructor;
            }
        }
        throw new NoSuchMethodException(methodToString("<init>", parameterTypes));
    }

    //
    // Other helpers and base implementation
    //

    private static boolean arrayContentsEq(Object[] a1, Object[] a2) {
        if (a1 == null) {
            return a2 == null || a2.length == 0;
        }

        if (a2 == null) {
            return a1.length == 0;
        }

        if (a1.length != a2.length) {
            return false;
        }

        for (int i = 0; i < a1.length; i++) {
            if (a1[i] != a2[i]) {
                return false;
            }
        }

        return true;
    }

    private static Field[] copyFields(Field[] arg) {
        Field[] out = new Field[arg.length];
        ReflectionFactory fact = getReflectionFactory();
        for (int i = 0; i < arg.length; i++) {
            out[i] = fact.copyField(arg[i]);
        }
        return out;
    }

    private static Method[] copyMethods(Method[] arg) {
        Method[] out = new Method[arg.length];
        ReflectionFactory fact = getReflectionFactory();
        for (int i = 0; i < arg.length; i++) {
            out[i] = fact.copyMethod(arg[i]);
        }
        return out;
    }

    private static <U> Constructor<U>[] copyConstructors(Constructor<U>[] arg) {
        Constructor<U>[] out = arg.clone();
        ReflectionFactory fact = getReflectionFactory();
        for (int i = 0; i < out.length; i++) {
            out[i] = fact.copyConstructor(out[i]);
        }
        return out;
    }

    private native Field[]       getDeclaredFields0(boolean publicOnly);
    private native Method[]      getDeclaredMethods0(boolean publicOnly);
    private native Constructor<T>[] getDeclaredConstructors0(boolean publicOnly);
    private native Class<?>[]   getDeclaredClasses0();

    /**
     * Helper method to get the method name from arguments.
     */
    private String methodToString(String name, Class<?>[] argTypes) {
        StringJoiner sj = new StringJoiner(", ", getName() + "." + name + "(", ")");
        if (argTypes != null) {
            for (int i = 0; i < argTypes.length; i++) {
                Class<?> c = argTypes[i];
                sj.add((c == null) ? "null" : c.getName());
            }
        }
        return sj.toString();
    }

    /** use serialVersionUID from JDK 1.1 for interoperability */
    private static final long serialVersionUID = 3206093459760846163L;


    /**
     * Class Class is special cased within the Serialization Stream Protocol.
     *
     * A Class instance is written initially into an ObjectOutputStream in the
     * following format:
     * <pre>
     *      {@code TC_CLASS} ClassDescriptor
     *      A ClassDescriptor is a special cased serialization of
     *      a {@code java.io.ObjectStreamClass} instance.
     * </pre>
     * A new handle is generated for the initial time the class descriptor
     * is written into the stream. Future references to the class descriptor
     * are written as references to the initial class descriptor instance.
     *
     * @see java.io.ObjectStreamClass
     */
    private static final ObjectStreamField[] serialPersistentFields =
        new ObjectStreamField[0];


    /**
     * Returns the assertion status that would be assigned to this
     * class if it were to be initialized at the time this method is invoked.
     * If this class has had its assertion status set, the most recent
     * setting will be returned; otherwise, if any package default assertion
     * status pertains to this class, the most recent setting for the most
     * specific pertinent package default assertion status is returned;
     * otherwise, if this class is not a system class (i.e., it has a
     * class loader) its class loader's default assertion status is returned;
     * otherwise, the system class default assertion status is returned.
     * <p>
     * Few programmers will have any need for this method; it is provided
     * for the benefit of the JRE itself.  (It allows a class to determine at
     * the time that it is initialized whether assertions should be enabled.)
     * Note that this method is not guaranteed to return the actual
     * assertion status that was (or will be) associated with the specified
     * class when it was (or will be) initialized.
     *
     * @return the desired assertion status of the specified class.
     * @see    java.lang.ClassLoader#setClassAssertionStatus
     * @see    java.lang.ClassLoader#setPackageAssertionStatus
     * @see    java.lang.ClassLoader#setDefaultAssertionStatus
     * @since  1.4
     */
    public boolean desiredAssertionStatus() {
        ClassLoader loader = getClassLoader0();
        // If the loader is null this is a system class, so ask the VM
        if (loader == null)
            return desiredAssertionStatus0(this);

        // If the classloader has been initialized with the assertion
        // directives, ask it. Otherwise, ask the VM.
        synchronized(loader.assertionLock) {
            if (loader.classAssertionStatus != null) {
                return loader.desiredAssertionStatus(getName());
            }
        }
        return desiredAssertionStatus0(this);
    }

    // Retrieves the desired assertion status of this class from the VM
    private static native boolean desiredAssertionStatus0(Class<?> clazz);

    /**
     * Returns true if and only if this class was declared as an enum in the
     * source code.
     *
     * @return true if and only if this class was declared as an enum in the
     *     source code
     * @since 1.5
     */
    public boolean isEnum() {
        // An enum must both directly extend java.lang.Enum and have
        // the ENUM bit set; classes for specialized enum constants
        // don't do the former.
        return (this.getModifiers() & ENUM) != 0 &&
        this.getSuperclass() == java.lang.Enum.class;
    }

    // Fetches the factory for reflective objects
    private static ReflectionFactory getReflectionFactory() {
        if (reflectionFactory == null) {
            reflectionFactory =
                java.security.AccessController.doPrivileged
                    (new ReflectionFactory.GetReflectionFactoryAction());
        }
        return reflectionFactory;
    }
    private static ReflectionFactory reflectionFactory;

    /**
     * Returns the elements of this enum class or null if this
     * Class object does not represent an enum type.
     *
     * @return an array containing the values comprising the enum class
     *     represented by this Class object in the order they're
     *     declared, or null if this Class object does not
     *     represent an enum type
     * @since 1.5
     */
    public T[] getEnumConstants() {
        T[] values = getEnumConstantsShared();
        return (values != null) ? values.clone() : null;
    }

    /**
     * Returns the elements of this enum class or null if this
     * Class object does not represent an enum type;
     * identical to getEnumConstants except that the result is
     * uncloned, cached, and shared by all callers.
     */
    T[] getEnumConstantsShared() {
        T[] constants = enumConstants;
        if (constants == null) {
            if (!isEnum()) return null;
            try {
                final Method values = getMethod("values");
                java.security.AccessController.doPrivileged(
                    new java.security.PrivilegedAction<>() {
                        public Void run() {
                                values.setAccessible(true);
                                return null;
                            }
                        });
                @SuppressWarnings("unchecked")
                T[] temporaryConstants = (T[])values.invoke(null);
                enumConstants = constants = temporaryConstants;
            }
            // These can happen when users concoct enum-like classes
            // that don't comply with the enum spec.
            catch (InvocationTargetException | NoSuchMethodException |
                   IllegalAccessException ex) { return null; }
        }
        return constants;
    }
    private transient volatile T[] enumConstants;

    /**
     * Returns a map from simple name to enum constant.  This package-private
     * method is used internally by Enum to implement
     * {@code public static <T extends Enum<T>> T valueOf(Class<T>, String)}
     * efficiently.  Note that the map is returned by this method is
     * created lazily on first use.  Typically it won't ever get created.
     */
    Map<String, T> enumConstantDirectory() {
        Map<String, T> directory = enumConstantDirectory;
        if (directory == null) {
            T[] universe = getEnumConstantsShared();
            if (universe == null)
                throw new IllegalArgumentException(
                    getName() + " is not an enum type");
            directory = new HashMap<>((int)(universe.length / 0.75f) + 1);
            for (T constant : universe) {
                directory.put(((Enum<?>)constant).name(), constant);
            }
            enumConstantDirectory = directory;
        }
        return directory;
    }
    private transient volatile Map<String, T> enumConstantDirectory;

    /**
     * Casts an object to the class or interface represented
     * by this {@code Class} object.
     *
     * @param obj the object to be cast
     * @return the object after casting, or null if obj is null
     *
     * @throws ClassCastException if the object is not
     * null and is not assignable to the type T.
     *
     * @since 1.5
     */
    @SuppressWarnings("unchecked")
    @HotSpotIntrinsicCandidate
    public T cast(Object obj) {
        if (obj != null && !isInstance(obj))
            throw new ClassCastException(cannotCastMsg(obj));
        return (T) obj;
    }

    private String cannotCastMsg(Object obj) {
        return "Cannot cast " + obj.getClass().getName() + " to " + getName();
    }

    /**
     * Casts this {@code Class} object to represent a subclass of the class
     * represented by the specified class object.  Checks that the cast
     * is valid, and throws a {@code ClassCastException} if it is not.  If
     * this method succeeds, it always returns a reference to this class object.
     *
     * <p>This method is useful when a client needs to "narrow" the type of
     * a {@code Class} object to pass it to an API that restricts the
     * {@code Class} objects that it is willing to accept.  A cast would
     * generate a compile-time warning, as the correctness of the cast
     * could not be checked at runtime (because generic types are implemented
     * by erasure).
     *
     * @param <U> the type to cast this class object to
     * @param clazz the class of the type to cast this class object to
     * @return this {@code Class} object, cast to represent a subclass of
     *    the specified class object.
     * @throws ClassCastException if this {@code Class} object does not
     *    represent a subclass of the specified class (here "subclass" includes
     *    the class itself).
     * @since 1.5
     */
    @SuppressWarnings("unchecked")
    public <U> Class<? extends U> asSubclass(Class<U> clazz) {
        if (clazz.isAssignableFrom(this))
            return (Class<? extends U>) this;
        else
            throw new ClassCastException(this.toString());
    }

    /**
     * @throws NullPointerException {@inheritDoc}
     * @since 1.5
     */
    @SuppressWarnings("unchecked")
    public <A extends Annotation> A getAnnotation(Class<A> annotationClass) {
        Objects.requireNonNull(annotationClass);

        return (A) annotationData().annotations.get(annotationClass);
    }

    /**
     * {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     * @since 1.5
     */
    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
        return GenericDeclaration.super.isAnnotationPresent(annotationClass);
    }

    /**
     * @throws NullPointerException {@inheritDoc}
     * @since 1.8
     */
    @Override
    public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationClass) {
        Objects.requireNonNull(annotationClass);

        AnnotationData annotationData = annotationData();
        return AnnotationSupport.getAssociatedAnnotations(annotationData.declaredAnnotations,
                                                          this,
                                                          annotationClass);
    }

    /**
     * @since 1.5
     */
    public Annotation[] getAnnotations() {
        return AnnotationParser.toArray(annotationData().annotations);
    }

    /**
     * @throws NullPointerException {@inheritDoc}
     * @since 1.8
     */
    @Override
    @SuppressWarnings("unchecked")
    public <A extends Annotation> A getDeclaredAnnotation(Class<A> annotationClass) {
        Objects.requireNonNull(annotationClass);

        return (A) annotationData().declaredAnnotations.get(annotationClass);
    }

    /**
     * @throws NullPointerException {@inheritDoc}
     * @since 1.8
     */
    @Override
    public <A extends Annotation> A[] getDeclaredAnnotationsByType(Class<A> annotationClass) {
        Objects.requireNonNull(annotationClass);

        return AnnotationSupport.getDirectlyAndIndirectlyPresent(annotationData().declaredAnnotations,
                                                                 annotationClass);
    }

    /**
     * @since 1.5
     */
    public Annotation[] getDeclaredAnnotations()  {
        return AnnotationParser.toArray(annotationData().declaredAnnotations);
    }

    // annotation data that might get invalidated when JVM TI RedefineClasses() is called
    private static class AnnotationData {
        final Map<Class<? extends Annotation>, Annotation> annotations;
        final Map<Class<? extends Annotation>, Annotation> declaredAnnotations;

        // Value of classRedefinedCount when we created this AnnotationData instance
        final int redefinedCount;

        AnnotationData(Map<Class<? extends Annotation>, Annotation> annotations,
                       Map<Class<? extends Annotation>, Annotation> declaredAnnotations,
                       int redefinedCount) {
            this.annotations = annotations;
            this.declaredAnnotations = declaredAnnotations;
            this.redefinedCount = redefinedCount;
        }
    }

    // Annotations cache
    @SuppressWarnings("UnusedDeclaration")
    private transient volatile AnnotationData annotationData;

    private AnnotationData annotationData() {
        while (true) { // retry loop
            AnnotationData annotationData = this.annotationData;
            int classRedefinedCount = this.classRedefinedCount;
            if (annotationData != null &&
                annotationData.redefinedCount == classRedefinedCount) {
                return annotationData;
            }
            // null or stale annotationData -> optimistically create new instance
            AnnotationData newAnnotationData = createAnnotationData(classRedefinedCount);
            // try to install it
            if (Atomic.casAnnotationData(this, annotationData, newAnnotationData)) {
                // successfully installed new AnnotationData
                return newAnnotationData;
            }
        }
    }

    private AnnotationData createAnnotationData(int classRedefinedCount) {
        Map<Class<? extends Annotation>, Annotation> declaredAnnotations =
            AnnotationParser.parseAnnotations(getRawAnnotations(), getConstantPool(), this);
        Class<?> superClass = getSuperclass();
        Map<Class<? extends Annotation>, Annotation> annotations = null;
        if (superClass != null) {
            Map<Class<? extends Annotation>, Annotation> superAnnotations =
                superClass.annotationData().annotations;
            for (Map.Entry<Class<? extends Annotation>, Annotation> e : superAnnotations.entrySet()) {
                Class<? extends Annotation> annotationClass = e.getKey();
                if (AnnotationType.getInstance(annotationClass).isInherited()) {
                    if (annotations == null) { // lazy construction
                        annotations = new LinkedHashMap<>((Math.max(
                                declaredAnnotations.size(),
                                Math.min(12, declaredAnnotations.size() + superAnnotations.size())
                            ) * 4 + 2) / 3
                        );
                    }
                    annotations.put(annotationClass, e.getValue());
                }
            }
        }
        if (annotations == null) {
            // no inherited annotations -> share the Map with declaredAnnotations
            annotations = declaredAnnotations;
        } else {
            // at least one inherited annotation -> declared may override inherited
            annotations.putAll(declaredAnnotations);
        }
        return new AnnotationData(annotations, declaredAnnotations, classRedefinedCount);
    }

    // Annotation types cache their internal (AnnotationType) form

    @SuppressWarnings("UnusedDeclaration")
    private transient volatile AnnotationType annotationType;

    boolean casAnnotationType(AnnotationType oldType, AnnotationType newType) {
        return Atomic.casAnnotationType(this, oldType, newType);
    }

    AnnotationType getAnnotationType() {
        return annotationType;
    }

    Map<Class<? extends Annotation>, Annotation> getDeclaredAnnotationMap() {
        return annotationData().declaredAnnotations;
    }

    /* Backing store of user-defined values pertaining to this class.
     * Maintained by the ClassValue class.
     */
    transient ClassValue.ClassValueMap classValueMap;

    /**
     * Returns an {@code AnnotatedType} object that represents the use of a
     * type to specify the superclass of the entity represented by this {@code
     * Class} object. (The <em>use</em> of type Foo to specify the superclass
     * in '...  extends Foo' is distinct from the <em>declaration</em> of type
     * Foo.)
     *
     * <p> If this {@code Class} object represents a type whose declaration
     * does not explicitly indicate an annotated superclass, then the return
     * value is an {@code AnnotatedType} object representing an element with no
     * annotations.
     *
     * <p> If this {@code Class} represents either the {@code Object} class, an
     * interface type, an array type, a primitive type, or void, the return
     * value is {@code null}.
     *
     * @return an object representing the superclass
     * @since 1.8
     */
    public AnnotatedType getAnnotatedSuperclass() {
        if (this == Object.class ||
                isInterface() ||
                isArray() ||
                isPrimitive() ||
                this == Void.TYPE) {
            return null;
        }

        return TypeAnnotationParser.buildAnnotatedSuperclass(getRawTypeAnnotations(), getConstantPool(), this);
    }

    /**
     * Returns an array of {@code AnnotatedType} objects that represent the use
     * of types to specify superinterfaces of the entity represented by this
     * {@code Class} object. (The <em>use</em> of type Foo to specify a
     * superinterface in '... implements Foo' is distinct from the
     * <em>declaration</em> of type Foo.)
     *
     * <p> If this {@code Class} object represents a class, the return value is
     * an array containing objects representing the uses of interface types to
     * specify interfaces implemented by the class. The order of the objects in
     * the array corresponds to the order of the interface types used in the
     * 'implements' clause of the declaration of this {@code Class} object.
     *
     * <p> If this {@code Class} object represents an interface, the return
     * value is an array containing objects representing the uses of interface
     * types to specify interfaces directly extended by the interface. The
     * order of the objects in the array corresponds to the order of the
     * interface types used in the 'extends' clause of the declaration of this
     * {@code Class} object.
     *
     * <p> If this {@code Class} object represents a class or interface whose
     * declaration does not explicitly indicate any annotated superinterfaces,
     * the return value is an array of length 0.
     *
     * <p> If this {@code Class} object represents either the {@code Object}
     * class, an array type, a primitive type, or void, the return value is an
     * array of length 0.
     *
     * @return an array representing the superinterfaces
     * @since 1.8
     */
    public AnnotatedType[] getAnnotatedInterfaces() {
         return TypeAnnotationParser.buildAnnotatedInterfaces(getRawTypeAnnotations(), getConstantPool(), this);
    }

    private native Class<?> getNestHost0();

    /**
     * Returns the nest host of the <a href=#nest>nest</a> to which the class
     * or interface represented by this {@code Class} object belongs.
     * Every class and interface is a member of exactly one nest.
     * A class or interface that is not recorded as belonging to a nest
     * belongs to the nest consisting only of itself, and is the nest
     * host.
     *
     * <p>Each of the {@code Class} objects representing array types,
     * primitive types, and {@code void} returns {@code this} to indicate
     * that the represented entity belongs to the nest consisting only of
     * itself, and is the nest host.
     *
     * <p>If there is a {@linkplain LinkageError linkage error} accessing
     * the nest host, or if this class or interface is not enumerated as
     * a member of the nest by the nest host, then it is considered to belong
     * to its own nest and {@code this} is returned as the host.
     *
     * @apiNote A {@code class} file of version 55.0 or greater may record the
     * host of the nest to which it belongs by using the {@code NestHost}
     * attribute (JVMS 4.7.28). Alternatively, a {@code class} file of
     * version 55.0 or greater may act as a nest host by enumerating the nest's
     * other members with the
     * {@code NestMembers} attribute (JVMS 4.7.29).
     * A {@code class} file of version 54.0 or lower does not use these
     * attributes.
     *
     * @return the nest host of this class or interface
     *
     * @throws SecurityException
     *         If the returned class is not the current class, and
     *         if a security manager, <i>s</i>, is present and the caller's
     *         class loader is not the same as or an ancestor of the class
     *         loader for the returned class and invocation of {@link
     *         SecurityManager#checkPackageAccess s.checkPackageAccess()}
     *         denies access to the package of the returned class
     * @since 11
     * @jvms 4.7.28 and 4.7.29 NestHost and NestMembers attributes
     * @jvms 5.4.4 Access Control
     */
    @CallerSensitive
    public Class<?> getNestHost() {
        if (isPrimitive() || isArray()) {
            return this;
        }
        Class<?> host;
        try {
            host = getNestHost0();
        } catch (LinkageError e) {
            // if we couldn't load our nest-host then we
            // act as-if we have no nest-host attribute
            return this;
        }
        // if null then nest membership validation failed, so we
        // act as-if we have no nest-host attribute
        if (host == null || host == this) {
            return this;
        }
        // returning a different class requires a security check
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            checkPackageAccess(sm,
                               ClassLoader.getClassLoader(Reflection.getCallerClass()), true);
        }
        return host;
    }

    /**
     * Determines if the given {@code Class} is a nestmate of the
     * class or interface represented by this {@code Class} object.
     * Two classes or interfaces are nestmates
     * if they have the same {@linkplain #getNestHost() nest host}.
     *
     * @param c the class to check
     * @return {@code true} if this class and {@code c} are members of
     * the same nest; and {@code false} otherwise.
     *
     * @since 11
     */
    public boolean isNestmateOf(Class<?> c) {
        if (this == c) {
            return true;
        }
        if (isPrimitive() || isArray() ||
            c.isPrimitive() || c.isArray()) {
            return false;
        }
        try {
            return getNestHost0() == c.getNestHost0();
        } catch (LinkageError e) {
            return false;
        }
    }

    private native Class<?>[] getNestMembers0();

    /**
     * Returns an array containing {@code Class} objects representing all the
     * classes and interfaces that are members of the nest to which the class
     * or interface represented by this {@code Class} object belongs.
     * The {@linkplain #getNestHost() nest host} of that nest is the zeroth
     * element of the array. Subsequent elements represent any classes or
     * interfaces that are recorded by the nest host as being members of
     * the nest; the order of such elements is unspecified. Duplicates are
     * permitted.
     * If the nest host of that nest does not enumerate any members, then the
     * array has a single element containing {@code this}.
     *
     * <p>Each of the {@code Class} objects representing array types,
     * primitive types, and {@code void} returns an array containing only
     * {@code this}.
     *
     * <p>This method validates that, for each class or interface which is
     * recorded as a member of the nest by the nest host, that class or
     * interface records itself as a member of that same nest. Any exceptions
     * that occur during this validation are rethrown by this method.
     *
     * @return an array of all classes and interfaces in the same nest as
     * this class
     *
     * @throws LinkageError
     *         If there is any problem loading or validating a nest member or
     *         its nest host
     * @throws SecurityException
     *         If any returned class is not the current class, and
     *         if a security manager, <i>s</i>, is present and the caller's
     *         class loader is not the same as or an ancestor of the class
     *         loader for that returned class and invocation of {@link
     *         SecurityManager#checkPackageAccess s.checkPackageAccess()}
     *         denies access to the package of that returned class
     *
     * @since 11
     * @see #getNestHost()
     */
    @CallerSensitive
    public Class<?>[] getNestMembers() {
        if (isPrimitive() || isArray()) {
            return new Class<?>[] { this };
        }
        Class<?>[] members = getNestMembers0();
        // Can't actually enable this due to bootstrapping issues
        // assert(members.length != 1 || members[0] == this); // expected invariant from VM

        if (members.length > 1) {
            // If we return anything other than the current class we need
            // a security check
            SecurityManager sm = System.getSecurityManager();
            if (sm != null) {
                checkPackageAccess(sm,
                                   ClassLoader.getClassLoader(Reflection.getCallerClass()), true);
            }
        }
        return members;
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\ClassCastException.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1994, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown to indicate that the code has attempted to cast an object
 * to a subclass of which it is not an instance. For example, the
 * following code generates a <code>ClassCastException</code>:
 * <blockquote><pre>
 *     Object x = new Integer(0);
 *     System.out.println((String)x);
 * </pre></blockquote>
 *
 * @author  unascribed
 * @since   1.0
 */
public
class ClassCastException extends RuntimeException {
    private static final long serialVersionUID = -9223365651070458532L;

    /**
     * Constructs a <code>ClassCastException</code> with no detail message.
     */
    public ClassCastException() {
        super();
    }

    /**
     * Constructs a <code>ClassCastException</code> with the specified
     * detail message.
     *
     * @param   s   the detail message.
     */
    public ClassCastException(String s) {
        super(s);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\ClassCircularityError.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown when the Java Virtual Machine detects a circularity in the
 * superclass hierarchy of a class being loaded.
 *
 * @author     unascribed
 * @since      1.0
 */
public class ClassCircularityError extends LinkageError {
    private static final long serialVersionUID = 1054362542914539689L;

    /**
     * Constructs a {@code ClassCircularityError} with no detail message.
     */
    public ClassCircularityError() {
        super();
    }

    /**
     * Constructs a {@code ClassCircularityError} with the specified detail
     * message.
     *
     * @param  s
     *         The detail message
     */
    public ClassCircularityError(String s) {
        super(s);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\ClassFormatError.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 1994, 2008, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Thrown when the Java Virtual Machine attempts to read a class
 * file and determines that the file is malformed or otherwise cannot
 * be interpreted as a class file.
 *
 * @author  unascribed
 * @since   1.0
 */
public
class ClassFormatError extends LinkageError {
    private static final long serialVersionUID = -8420114879011949195L;

    /**
     * Constructs a <code>ClassFormatError</code> with no detail message.
     */
    public ClassFormatError() {
        super();
    }

    /**
     * Constructs a <code>ClassFormatError</code> with the specified
     * detail message.
     *
     * @param   s   the detail message.
     */
    public ClassFormatError(String s) {
        super(s);
    }
}

【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【【
E:\AllProjects\GitProjects\JavaProjects\jdk11.0.2lib.src.java.base.java\lang\ClassLoader.java
】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】】
/*
 * Copyright (c) 2013, 2018, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.io.InputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.security.AccessController;
import java.security.AccessControlContext;
import java.security.CodeSource;
import java.security.PrivilegedAction;
import java.security.ProtectionDomain;
import java.security.cert.Certificate;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.Vector;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import jdk.internal.loader.BuiltinClassLoader;
import jdk.internal.perf.PerfCounter;
import jdk.internal.loader.BootLoader;
import jdk.internal.loader.ClassLoaders;
import jdk.internal.misc.Unsafe;
import jdk.internal.misc.VM;
import jdk.internal.ref.CleanerFactory;
import jdk.internal.reflect.CallerSensitive;
import jdk.internal.reflect.Reflection;
import sun.reflect.misc.ReflectUtil;
import sun.security.util.SecurityConstants;

/**
 * A class loader is an object that is responsible for loading classes. The
 * class {@code ClassLoader} is an abstract class.  Given the <a
 * href="#binary-name">binary name</a> of a class, a class loader should attempt to
 * locate or generate data that constitutes a definition for the class.  A
 * typical strategy is to transform the name into a file name and then read a
 * "class file" of that name from a file system.
 *
 * <p> Every {@link java.lang.Class Class} object contains a {@link
 * Class#getClassLoader() reference} to the {@code ClassLoader} that defined
 * it.
 *
 * <p> {@code Class} objects for array classes are not created by class
 * loaders, but are created automatically as required by the Java runtime.
